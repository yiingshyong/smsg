 @echo off
 cd @sgw.base@\Batch\crsmsg_log_archive
 setLocal EnableDelayedExpansion
 set CLASSPATH=.\classes"
 for /R ./lib %%a in (*.jar) do (
   set CLASSPATH=!CLASSPATH!;%%a
 )
 set CLASSPATH=!CLASSPATH!"
 echo !CLASSPATH!

java -cp %CLASSPATH% SystemLogArchival 
