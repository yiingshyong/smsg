@echo off
cd @basedir@/Batch/crsmsg_log_archive
setLocal EnableDelayedExpansion
set cp=.\classes
for /R .\lib %%a in (*.jar) do (
   set cp=!cp!;%%a
)
echo %cp%
java -classpath %cp% UsageLogMaintenance >> @batch.log.path@/cron_db_archival.log
