import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class SystemLogArchival {

	private static final Log log = LogFactory.getLog(SystemLogArchival.class);
	private static ResourceBundle CONFIG = ResourceBundle.getBundle("log_maintenance");
	private static final String FILE_SEP = System.getProperty("file.separator");
	private String engineLogsPath;
	private String webLogsPath;
	private String archivalPath;
	private String archivalSuffixDf;
	private String dbDriverClass;
	private String dbUrl;
	private String dbUser;
	private String dbPassword;
	private Connection conn;
	private int nonArchiveDuration;
	
	public SystemLogArchival(){
		init();
	}
	
	private void init(){
		this.engineLogsPath = CONFIG.getString("ENGINE_LOG_PATH");
		this.webLogsPath = CONFIG.getString("TOMCAT_LOG_PATH");
		this.archivalPath = CONFIG.getString("SYSTEM_LOG_PATH");
		this.archivalSuffixDf = CONFIG.getString("ARCHIVE_LOG_DF");
		this.dbDriverClass = CONFIG.getString("MYSQLDRIVER");
		this.dbUser = CONFIG.getString("USER");
		this.dbPassword = CONFIG.getString("PASSWORD");
		this.dbUrl = CONFIG.getString("HOST");
		try {
			Class.forName(this.dbDriverClass);
			this.conn = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
			this.nonArchiveDuration = getNonArchivedDuration(conn);
		} catch (Exception e){
			log.error(e.getMessage(), e);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Running System Log Archival ...");
		log.info("Running System Log Archival ...");
		SystemLogArchival instance = new SystemLogArchival();
		if(instance.getNonArchiveDuration() == 0){
			log.warn("Archival was disabled. Exiting.");
			return;
		}
		instance.archive(instance.getEngineLogsPath(), instance.getArchivalPath(), instance.getNonArchiveDuration(), "engine-archive");	
		instance.archive(instance.getWebLogsPath(), instance.getArchivalPath(), instance.getNonArchiveDuration(), "web-archive");
		log.info("Done.");
		System.out.println("Finished.");
	}
	
	private void archive(String logDirectory, String targetArchiveFolder, int nonArchiveDuration, String archivalFileName){
		log.info("Archiving logs in " + logDirectory);
		File logsFolder = new File(logDirectory);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -nonArchiveDuration);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		
		log.info("File older than " + cal.getTime() + " will be archived");
		IOFileFilter fileFilter = FileFilterUtils.ageFileFilter(cal.getTime(), true);
		List<File> filesToBeArchived = new ArrayList<File>(FileUtils.listFiles(logsFolder, fileFilter, null));
		
		if(filesToBeArchived == null || filesToBeArchived.isEmpty()){
			log.warn("No files found to be archived for today.");
			return;
		}
		
		String runDate = new SimpleDateFormat(archivalSuffixDf).format(new Date());
		String archiveFileName = archivalFileName + "-" + runDate + ".zip";
		String archiveFilePath = targetArchiveFolder + FILE_SEP + archiveFileName;
		
		try{
			new File(targetArchiveFolder).mkdirs();
			File archiveFile = new File(archiveFilePath);
			
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(archiveFile));
			
			for(File f : filesToBeArchived){
				FileInputStream fis = new FileInputStream(f);
				zos.putNextEntry(new ZipEntry(f.getName()));
				IOUtils.copy(fis, zos);
				fis.close();
				zos.closeEntry();					
			}
			zos.flush();
			zos.close();
			for(File f : filesToBeArchived){
				f.delete();
			}
		}catch(Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
	
	private int getNonArchivedDuration(Connection conn) throws Exception {
		int archiveDuration = 0;
		PreparedStatement selectStmt = null;
		ResultSet rs = null;
		String sql = new StringBuffer("SELECT SYSTEM_LOG_DURATION")
					.append(" FROM sms_cfg")
					.toString();
		
		try {
			selectStmt = conn.prepareStatement(sql);
			rs = selectStmt.executeQuery();
			
			if(rs.next()){
				archiveDuration = rs.getInt("SYSTEM_LOG_DURATION");
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(rs != null)
				rs.close();
			if(selectStmt != null)
				selectStmt.close();
		}
		
		return archiveDuration;
	}

	public String getEngineLogsPath() {
		return engineLogsPath;
	}

	public void setEngineLogsPath(String engineLogsPath) {
		this.engineLogsPath = engineLogsPath;
	}

	public String getWebLogsPath() {
		return webLogsPath;
	}

	public void setWebLogsPath(String webLogsPath) {
		this.webLogsPath = webLogsPath;
	}

	public String getArchivalPath() {
		return archivalPath;
	}

	public void setArchivalPath(String archivalPath) {
		this.archivalPath = archivalPath;
	}

	public String getArchivalSuffixDf() {
		return archivalSuffixDf;
	}

	public void setArchivalSuffixDf(String archivalSuffixDf) {
		this.archivalSuffixDf = archivalSuffixDf;
	}

	public String getDbDriverClass() {
		return dbDriverClass;
	}

	public void setDbDriverClass(String dbDriverClass) {
		this.dbDriverClass = dbDriverClass;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public int getNonArchiveDuration() {
		return nonArchiveDuration;
	}

	public void setNonArchiveDuration(int nonArchiveDuration) {
		this.nonArchiveDuration = nonArchiveDuration;
	}

	public static ResourceBundle getConfig() {
		return CONFIG;
	}

	public static String getFileSep() {
		return FILE_SEP;
	}
	
}
