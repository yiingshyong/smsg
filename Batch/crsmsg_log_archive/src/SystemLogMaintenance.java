

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;


public class SystemLogMaintenance {
	private static Logger logger = Logger.getLogger(SystemLogMaintenance.class.getName());
	private static final String RESOURCE_FILE = "log_maintenance";
	private static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";
	
	private static final String TOMCAT_LOG_ARCHIVE = "tomcat_log";
	private static final String ENGINE_LOG_ARCHIVE = "SgwCore_log";
	
	private static final String ENVIRONMENT_WINDOWS = "Windows";
	private static final String ENVIRONMENT_SOLARIS = "Solaris";
	
	private static String getKey(String KEY){
		ResourceBundle rb;
		try{
			rb = ResourceBundle.getBundle(RESOURCE_FILE);
		}catch (MissingResourceException m){
			return "";
		}
		return rb.getString(KEY);
	}
	
	private static Connection CreateConnection() 
	{	
	    Connection MySQLConn = null;

	    String MYSQLDRIVER = getKey("MYSQLDRIVER");
	    String HOST = getKey("HOST");
	    String USER = getKey("USER");
	    String PASSWORD = getKey("PASSWORD");

		try 
		{
			Class.forName(MYSQLDRIVER);
			MySQLConn = DriverManager.getConnection(HOST,USER,PASSWORD);
		} 
		catch (Exception e)
		{
			logger.info("Error SystemLogMaintenance CreateConnection: " + e);
		}

	     return MySQLConn;
	}
	
	private static String stringByDayMonthYear(java.util.Date date){
		SimpleDateFormat _dateFormatter;
		_dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
    	if (date == null){
    		return "";
    	}
		
		return _dateFormatter.format(date);
	}
	
	private static int getArchiveDuration(Connection conn) throws Exception {
		int archiveDuration = 0;
		PreparedStatement selectStmt = null;
		ResultSet rs = null;
		String sql = new StringBuffer("SELECT SYSTEM_LOG_DURATION")
					.append(" FROM sms_cfg")
					.toString();
		
		try {
			selectStmt = conn.prepareStatement(sql);
			rs = selectStmt.executeQuery();
			
			if(rs.next()){
				archiveDuration = rs.getInt("SYSTEM_LOG_DURATION");
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(rs != null)
				rs.close();
			if(selectStmt != null)
				selectStmt.close();
		}
		
		return archiveDuration;
	}
	
	private static void prepareLogFolder(String activeFolderName) throws Exception {
		try {
			File active_log_folder = new File(activeFolderName);  
			System.out.println(active_log_folder.toString());
			if(!active_log_folder.isDirectory()) {
				active_log_folder.mkdirs();
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static boolean checkEmptyFolder(String pathStr) {
		boolean empty = true;
		File path = new File(pathStr);
		
		if(path.list() != null && path.list().length > 0) {
			empty = false;
		}
		
		return empty;
	}
	
	private static void addToZip(String path, String srcFile, ZipOutputStream zip) throws Exception{

		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip);
		}
		else {
			if(path != null && path.trim().length() > 0)
				path += "/";
			
			byte[] buf = new byte[1024];
			int len;
			try {
				FileInputStream in = new FileInputStream(srcFile);
				zip.putNextEntry(new ZipEntry(path + folder.getName()));
				while ((len = in.read(buf)) > 0) {
					zip.write(buf, 0, len);
				}
				
				in.close();
			}
			catch (Exception e){
				throw e;
			}
		}
	}
	
	private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws Exception {
		File folder = new File(srcFolder);
		String fileListe[] = folder.list();
		try {
			if(path != null && path.trim().length() > 0)
				path += "/";
			
			for (int i = 0; i < fileListe.length; i++) {
				addToZip(path + folder.getName(), srcFolder + "/" + fileListe[i], zip);
			}
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	private static void zipLogFolder(String srcFolder, String destZipFile) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;
		
		try {
			fileWriter = new FileOutputStream(destZipFile);
			zip = new ZipOutputStream(fileWriter);
		}
		catch (Exception e){
			throw e;
		}

		addFolderToZip("", srcFolder, zip);
		
		try {
			zip.flush();
			zip.close();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	private static void cleanupFolder(File path, boolean deletePath) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i = 0; i < files.length; i++) {
				if(files[i].isDirectory()) {
					cleanupFolder(files[i], deletePath);
				}
				else {
					files[i].delete();
				}
			}
		}
		
		if(deletePath)
			path.delete();
	}
	
	private static boolean startTomcat(String environment) {
		boolean success = true;
		String[] command = null;
		
		if(environment.equalsIgnoreCase(ENVIRONMENT_WINDOWS)) {
			command = new String[]{"NET start Tomcat5"};
		} else if(environment.equalsIgnoreCase(ENVIRONMENT_SOLARIS)) {
			command = new String[]{"/export/home/b000sm1/tomcat5.5.26/bin/startup.sh"};
		}
		
		try
		{
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(command);
			int exitVal = proc.waitFor();

		} catch(Exception ex) {
			success = false;
			System.out.println("Solaris Runtime Error - Start Tomcat Service");
			ex.printStackTrace();
		}

		return success;
	}
	
	private static boolean shutdownTomcat(String environment) {
		boolean success = true;
		String[] command = null;
		
		if(environment.equalsIgnoreCase(ENVIRONMENT_WINDOWS)) {
			command = new String[]{"NET stop Tomcat5"};
		} else if(environment.equalsIgnoreCase(ENVIRONMENT_SOLARIS)) {
			command = new String[]{"/export/home/b000sm1/tomcat5.5.26/bin/shutdown.sh"};
		}
		
		try
		{
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(command);
			int exitVal = proc.waitFor();

		} catch(Exception ex) {
			success = false;
			System.out.println("Solaris Runtime Error - Stop Tomcat Service");
			ex.printStackTrace();
		}
		
		try
		{
			//Pause 10secs
			Thread.sleep(10000L);
		}
		catch(InterruptedException e)
		{
			success = false;
			e.printStackTrace();
		}

		return success;
	}
	
	private static boolean startEngine(String environment) {
		boolean success = true;
		
		if(environment.equalsIgnoreCase(ENVIRONMENT_WINDOWS)) {
			try
			{
				Runtime.getRuntime().exec("NET start qfp");

			} catch(Exception ex) {
				success = false;
				System.out.println("Windows Runtime Error - Start qfp Service");
				ex.printStackTrace();
			}

			try
			{
				Runtime.getRuntime().exec("NET start sp");

			} catch(Exception ex) {
				success = false;
				System.out.println("Windows Runtime Error - Start sp Service");
				ex.printStackTrace();
			}
		} else if(environment.equalsIgnoreCase(ENVIRONMENT_SOLARIS)) {
			try
			{
				Runtime.getRuntime().exec("/export/home/b000sm1/crsmsg/SgwCore/bin/qfp start");

			} catch(Exception ex) {
				success = false;
				System.out.println("Solaris Runtime Error - Start qfp Service");
				ex.printStackTrace();
			}

			try
			{
				Runtime.getRuntime().exec("/export/home/b000sm1/crsmsg/SgwCore/bin/sp start");

			} catch(Exception ex) {
				success = false;
				System.out.println("Solaris Runtime Error - Start sp Service");
				ex.printStackTrace();
			}
		}
		
		return success;
	}
	
	private static boolean shutdownEngine(String environment) {
		boolean success = true;
		
		if(environment.equalsIgnoreCase(ENVIRONMENT_WINDOWS)) {
			try
			{
				Runtime.getRuntime().exec("NET stop qfp");

			} catch(Exception ex) {
				success = false;
				System.out.println("Windows Runtime Error - Stop qfp Service");
				ex.printStackTrace();
			}

			try
			{
				Runtime.getRuntime().exec("NET stop sp");

			} catch(Exception ex) {
				success = false;
				System.out.println("Windows Runtime Error - Stop sp Service");
				ex.printStackTrace();
			}
		} else if(environment.equalsIgnoreCase(ENVIRONMENT_SOLARIS)) {
			try
			{
				Runtime.getRuntime().exec("/export/home/b000sm1/crsmsg/SgwCore/bin/qfp stop");

			} catch(Exception ex) {
				success = false;
				System.out.println("Solaris Runtime Error - Stop qfp Service");
				ex.printStackTrace();
			}

			try
			{
				Runtime.getRuntime().exec("/export/home/b000sm1/crsmsg/SgwCore/bin/sp stop");

			} catch(Exception ex) {
				success = false;
				System.out.println("Solaris Runtime Error - Stop sp Service");
				ex.printStackTrace();
			}
		}

		try
		{
			//Pause 10secs
			Thread.sleep(10000L);
		}
		catch(InterruptedException e)
		{
			success = false;
			e.printStackTrace();
		}
		
		return success;
	}
	
	private static Date checkLogsDate() {
		String tomcatDir = getKey("TOMCAT_LOG_PATH");
		File tomcatLog = new File(tomcatDir);
		String[] fileName = null;
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		String logName = "catalina.out";
		String logPrefix = "catalina";
		String catalinaStr = null;
		String dateFormatter = "MMM d, yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatter);
		Date logDate = null;
		File catalinaLog = new File(tomcatDir + logName);
		
		if(catalinaLog != null && catalinaLog.exists()) {
			try {
				logger.info("SystemLogMaintenance.checkLogsDate found catalina.out");
				fstream = new FileInputStream(catalinaLog);
				// Get the object of DataInputStream
				in = new DataInputStream(fstream);
				br = new BufferedReader(new InputStreamReader(in));

				catalinaStr = br.readLine();
				if(catalinaStr != null) {
					logDate = simpleDateFormat.parse(catalinaStr);
					System.out.println(logDate.toString());
				}
			} catch (FileNotFoundException fe) {
				fe.printStackTrace();
				logger.info("Error SystemLogMaintenance.checkLogsDate " + fe);
			} catch (IOException ie) {
				ie.printStackTrace();
				logger.info("Error SystemLogMaintenance.checkLogsDate " + ie);
			} catch (ParseException pe) {
				pe.printStackTrace();
				logger.info("Error SystemLogMaintenance.checkLogsDate " + pe);
			}
		} else if(tomcatLog != null && tomcatLog.list().length > 0) {
			fileName = tomcatLog.list();
			for (int i = 0; i < fileName.length; i++) {
				if(fileName[i] != null && fileName[i].startsWith(logPrefix)) {
					try {
						fstream = new FileInputStream(tomcatDir + fileName[i]);
						// Get the object of DataInputStream
						in = new DataInputStream(fstream);
						br = new BufferedReader(new InputStreamReader(in));

						catalinaStr = br.readLine();
						if(catalinaStr != null) {
							logDate = simpleDateFormat.parse(catalinaStr);
							System.out.println(logDate.toString());
							break;
						}
					} catch (FileNotFoundException fe) {
						fe.printStackTrace();
						logger.info("Error SystemLogMaintenance.checkLogsDate " + fe);
					} catch (IOException ie) {
						ie.printStackTrace();
						logger.info("Error SystemLogMaintenance.checkLogsDate " + ie);
					} catch (ParseException pe) {
						pe.printStackTrace();
						logger.info("Error SystemLogMaintenance.checkLogsDate " + pe);
					}

				}
			}
		} else
			logger.info("SystemLogMaintenance: Tomcat Log is empty.");
		
		return logDate;
	}
	
	public static void main(String[] args) {
		logger.info("Running SystemLogMaintenance.");
		
		Connection conn = CreateConnection();
		int archiveDuration = 0;
		Calendar archiveCal = null;
		Date logDate = null;
		String activeFolderName = null;
		String environment = null;
		try {
			archiveDuration = getArchiveDuration(conn);
			
			if(archiveDuration <= 0) {
				logger.info("SystemLogMaintenance: System Log Maintenance is disable.");
			} else {			
				environment = getKey("ENVIRONMENT");
				archiveCal = Calendar.getInstance();
				archiveCal.add(Calendar.MONTH, -archiveDuration);
				
				logDate = checkLogsDate();
				//Check Tomcat Log date
				if(logDate != null && logDate.before(archiveCal.getTime())) {
					//Preparing Archive Folder
					activeFolderName = getKey("SYSTEM_LOG_PATH") + "/" + stringByDayMonthYear(new java.util.Date()) + "/";
					prepareLogFolder(activeFolderName + TOMCAT_LOG_ARCHIVE + "/");
					prepareLogFolder(activeFolderName + ENGINE_LOG_ARCHIVE + "/");

					//Stop service
					shutdownTomcat(environment);
					shutdownEngine(environment);

					//Zip logs folder
					if(!checkEmptyFolder(getKey("TOMCAT_LOG_PATH")))
						zipLogFolder(getKey("TOMCAT_LOG_PATH"), activeFolderName + TOMCAT_LOG_ARCHIVE + ".zip");
					if(!checkEmptyFolder(getKey("ENGINE_LOG_PATH")))
						zipLogFolder(getKey("ENGINE_LOG_PATH"), activeFolderName + ENGINE_LOG_ARCHIVE + ".zip");

					logger.info("SystemLogMaintenance: Zip done.");

					//Cleanup logs folder
					cleanupFolder(new File(getKey("TOMCAT_LOG_PATH")), false);
					cleanupFolder(new File(getKey("ENGINE_LOG_PATH")), false);
					cleanupFolder(new File(activeFolderName + TOMCAT_LOG_ARCHIVE), true);
					cleanupFolder(new File(activeFolderName + ENGINE_LOG_ARCHIVE), true);

					logger.info("SystemLogMaintenance: Cleanup log folder done.");

					//Start service
					startTomcat(environment);
					startEngine(environment);

					logger.info("SystemLogMaintenance: Start services done.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error SystemLogMaintenance " + e);
		}
	}
}
