import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;


public class UsageLogMaintenance {
	private static Logger logger = Logger.getLogger(UsageLogMaintenance.class.getName());
	private static final String RESOURCE_FILE = "log_maintenance";
	private static final String LOG_FOLDER = "usage_log";
	private static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";
	private static final boolean DELETE_RECORD = true;
	
	private static final String FILE_NAME_SMS = "SMS.CSV";
	private static final String FILE_NAME_SMS_RECEIVED = "SMS_RECEIVED.CSV";
	private static final String FILE_NAME_FTP_REPORT = "FTP_REPORT.CSV";
	private static final String FILE_NAME_AUDIT_TRAIL = "AUDIT_TRAIL.CSV";
	private static final String FILE_NAME_BULK_SMS_SETUP = "BULK_SMS_SETUP.CSV";
	private static final String FILE_NAME_CHGPWD_HIST = "CHGPWD_HIST.CSV";
	private static final String FILE_NAME_LOGON_HIST = "LOGON_HIST.CSV";
	private static final String FILE_NAME_SMS_AUDIT_TRAIL = "SMS_AUDIT_TRAIL.CSV";
	private static final String FILE_NAME_SMS_HISTORY = "SMS_HISTORY.CSV";
	private static final String FILE_NAME_SMS_RECEIVED_DELETE = "SMS_RECEIVED_DELETE.CSV";
	private static final String FILE_NAME_CREDIT_CARD_ACTIVATION = "CC_ACTIVATION.CSV";
	private static Connection connection;
	
	private static String getKey(String KEY){
		ResourceBundle rb;
		try{
			rb = ResourceBundle.getBundle(RESOURCE_FILE);
		}catch (MissingResourceException m){
			return "";
		}
		return rb.getString(KEY);
	}
	
	private static Connection CreateConnection() throws Exception{	
		logger.info("Creating database connection");
	    String MYSQLDRIVER = getKey("MYSQLDRIVER");
	    String HOST = getKey("HOST");
	    String USER = getKey("USER");
	    String PASSWORD = getKey("PASSWORD");

		try {
			Class.forName(MYSQLDRIVER);
			connection = DriverManager.getConnection(HOST,USER,PASSWORD);
			return connection;
		} catch (Exception e){
			logger.error("Error creating database connection. Reason: " + e.getMessage(), e);
			throw e;
		}
	}
	
	public static String stringByDayMonthYear(java.util.Date date){
		SimpleDateFormat _dateFormatter;
		_dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
    	if (date == null){
    		return "";
    	}
		
		return _dateFormatter.format(date);
	}
	
	private static int getArchiveDuration(Connection conn) throws Exception {
		int archiveDuration = 0;
		PreparedStatement selectStmt = null;
		ResultSet rs = null;
		String sql = new StringBuffer("SELECT USAGE_LOG_DURATION")
					.append(" FROM sms_cfg")
					.toString();
		
		try {
			selectStmt = conn.prepareStatement(sql);
			rs = selectStmt.executeQuery();
			
			if(rs.next()){
				archiveDuration = rs.getInt("USAGE_LOG_DURATION");
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(rs != null)
				rs.close();
			if(selectStmt != null)
				selectStmt.close();
		}
		
		return archiveDuration;
	}
	
	private static int getCreditCardArchiveDuration(Connection conn) throws Exception {
		int archiveDuration = 0;
		PreparedStatement selectStmt = null;
		ResultSet rs = null;
		String sql = new StringBuffer("SELECT CCA_DATA_AGE")
					.append(" FROM sms_cfg")
					.toString();
		
		try {
			selectStmt = conn.prepareStatement(sql);
			rs = selectStmt.executeQuery();
			
			if(rs.next()){
				archiveDuration = rs.getInt("CCA_DATA_AGE");
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(rs != null)
				rs.close();
			if(selectStmt != null)
				selectStmt.close();
		}
		
		return archiveDuration;
	}
	
	private static void prepareLogFolder(String activeFolderName) throws Exception {
		try {
			File active_log_folder = new File(activeFolderName);  
			if(!active_log_folder.isDirectory()) {
				logger.info("Creating archive folder -> '" + active_log_folder.toString() + "'");
				active_log_folder.mkdirs();
			}
			
			try
			{
				Runtime.getRuntime().exec("chmod 777 " + active_log_folder);

			}
			catch (Exception ex)
			{
				System.out.println("Solaris Runtime Error - chmod 777 " + active_log_folder);
				ex.printStackTrace();
			}
			
		} catch (Exception e) {
			logger.error("Error creating archive folder. Reason: " + e.getMessage(), e);
			throw e;
		}
	}
	
	private static void addToZip(String path, String srcFile, ZipOutputStream zip) throws Exception{

		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip);
		}
		else {
			if(path != null && path.trim().length() > 0)
				path += "/";
			
			byte[] buf = new byte[1024];
			int len;
			try {
				FileInputStream in = new FileInputStream(srcFile);
				zip.putNextEntry(new ZipEntry(path + folder.getName()));
				while ((len = in.read(buf)) > 0) {
					zip.write(buf, 0, len);
				}
				
				in.close();
			}
			catch (Exception e){
				throw e;
			}
		}
	}
	
	private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws Exception {
		File folder = new File(srcFolder);
		String fileListe[] = folder.list();
		try {
			if(path != null && path.trim().length() > 0)
				path += "/";
			
			for (int i = 0; i < fileListe.length; i++) {
				addToZip(path + folder.getName(), srcFolder + "/" + fileListe[i], zip);
			}
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	private static void zipLogFolder(String srcFolder, String destZipFile) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;
		
		try {
			fileWriter = new FileOutputStream(destZipFile);
			zip = new ZipOutputStream(fileWriter);
		}
		catch (Exception e){
			throw e;
		}

		addFolderToZip("", srcFolder, zip);
		
		try {
			zip.flush();
			zip.close();
		}
		catch (Exception e) {
			throw e;
		}
	}
	
	public static boolean deleteWholeFolder(File path) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i = 0; i < files.length; i++) {
				if(files[i].isDirectory()) {
					deleteWholeFolder(files[i]);
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}
	
	private static void archiveSMS(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM sms")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND SMS_STATUS_DATETIME <= ").append("'"+archiveDate+"')")
					.append(" AND `SMS_STATUS` IN ('S', 'U')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM sms")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND SMS_STATUS_DATETIME <= ").append("'"+archiveDate+"')")
					.append(" AND `SMS_STATUS` IN ('S', 'U')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveSMS: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveSMSReceived(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM sms_received")
					.append(" WHERE (RECEIVED_DATETIME IS NOT NULL AND RECEIVED_DATETIME <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM sms_received")
					.append(" WHERE (RECEIVED_DATETIME IS NOT NULL AND RECEIVED_DATETIME <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveSMSReceived: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveFTPReport(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM ftp_report")
					.append(" WHERE (START_DATE IS NOT NULL AND START_DATE <= ").append("'"+archiveDate+"')")
					.append(" AND `TYPE` IN ('B', 'F')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM ftp_report")
					.append(" WHERE (START_DATE IS NOT NULL AND START_DATE <= ").append("'"+archiveDate+"')")
					.append(" AND `TYPE` IN ('B', 'F')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveFTPReport: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveAuditTrail(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM audit_trail")
					.append(" WHERE (AUDIT_DATE IS NOT NULL AND AUDIT_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM audit_trail")
					.append(" WHERE (AUDIT_DATE IS NOT NULL AND AUDIT_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveAuditTrail: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveBulkSmsSetup(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM bulk_sms_setup")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND DATE(SMS_STATUS_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM bulk_sms_setup")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND DATE(SMS_STATUS_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveBulkSmsSetup: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveChgpwdHist(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM chgpwd_hist")
					.append(" WHERE (CHGPWD_DATE IS NOT NULL AND CHGPWD_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM chgpwd_hist")
					.append(" WHERE (CHGPWD_DATE IS NOT NULL AND CHGPWD_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveChgpwdHist: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveLogonHist(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM logon_hist")
					.append(" WHERE (LOGON_DATE IS NOT NULL AND LOGON_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM logon_hist")
					.append(" WHERE (LOGON_DATE IS NOT NULL AND LOGON_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveLogonHist: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveSmsAuditTrail(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM sms_audit_trail")
					.append(" WHERE (AUDIT_DATE IS NOT NULL AND AUDIT_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM sms_audit_trail")
					.append(" WHERE (AUDIT_DATE IS NOT NULL AND AUDIT_DATE <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveSmsAuditTrail: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveSmsHistory(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM sms_history")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND DATE(SMS_STATUS_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM sms_history")
					.append(" WHERE (SMS_STATUS_DATETIME IS NOT NULL AND DATE(SMS_STATUS_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveSmsHistory: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveSmsReceivedDelete(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM sms_received_delete")
					.append(" WHERE (RECEIVED_DATETIME IS NOT NULL AND DATE(RECEIVED_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM sms_received_delete")
					.append(" WHERE (RECEIVED_DATETIME IS NOT NULL AND DATE(RECEIVED_DATETIME) <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveSmsReceivedDelete: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	private static void archiveCreditCardLogs(Connection conn, String fileName, Date archiveDate) throws Exception {
		PreparedStatement exportStmt = null, deleteStmt = null;
		String exportSql = new StringBuffer("SELECT *")
					.append(" INTO OUTFILE ?")
					.append(" FIELDS TERMINATED BY ','")
					.append(" ENCLOSED BY '\"'")
					.append(" ESCAPED BY '\\\\'")
					.append(" LINES TERMINATED BY '\\n'")
					.append(" FROM cc_activation")
					.append(" WHERE ( CREATED_DATETIME IS NOT NULL AND CREATED_DATETIME <= ").append("'"+archiveDate+"')")
					.toString();
		String deleteSql = new StringBuffer("DELETE ")
					.append(" FROM cc_activation")
					.append(" WHERE (CREATED_DATETIME IS NOT NULL AND CREATED_DATETIME <= ").append("'"+archiveDate+"')")
					.toString();
		
		try {
			exportStmt = conn.prepareStatement(exportSql);
			exportStmt.setString(1, fileName);
			exportStmt.executeQuery();
			
			if(DELETE_RECORD) {
				deleteStmt = conn.prepareStatement(deleteSql);
				deleteStmt.executeUpdate();
			}
			
		} catch (SQLException sqlex) {
			if(sqlex.getErrorCode() != 1086)
				sqlex.printStackTrace();
			else
				logger.info("UsageLogMaintenance: archiveCreditCardLogs: file exists.");
		} catch (Exception e) {
			throw e;
		} finally {
			if(exportStmt != null)
				exportStmt.close();
		}
	}
	
	public static void main(String[] args) {
		logger.info("Starting " + UsageLogMaintenance.class);		
		try {
			Connection conn = CreateConnection();
			int archiveDuration = 0;
			int creditCardArchiveDuration = 0;		
			Calendar archiveCal = null;
			Calendar creditCardArchiveCal = null;
			String activeFolderName = null;
	
			archiveDuration = getArchiveDuration(conn);
			creditCardArchiveDuration = getCreditCardArchiveDuration(conn);
			
			if(creditCardArchiveDuration <= 0){
				logger.warn("Credit Card Log Maintenance is disable.");				
			}			
			if(archiveDuration <= 0) {
				logger.warn("Usage Log Maintenance is disable.");
			}
			
			if(creditCardArchiveDuration > 0 || archiveDuration > 0){
				activeFolderName = getKey("USAGE_LOG_PATH") + LOG_FOLDER + "/" + stringByDayMonthYear(new java.util.Date()) + "/";
				prepareLogFolder(activeFolderName);
				
				if(creditCardArchiveDuration > 0){
					creditCardArchiveCal = Calendar.getInstance();
					creditCardArchiveCal.add(Calendar.MONTH, -creditCardArchiveDuration);
					Date archiveDate = new Date(creditCardArchiveCal.getTime().getTime());					
					logger.info("Archiving Credit Card Logs older than " + archiveDate);
					archiveCreditCardLogs(conn, activeFolderName + FILE_NAME_CREDIT_CARD_ACTIVATION, archiveDate);
					logger.info("Credit Card Archive done.");
				}
				if(archiveDuration > 0){
					archiveCal = Calendar.getInstance();
					archiveCal.add(Calendar.MONTH, -archiveDuration);
					Date archiveDate = new Date(archiveCal.getTime().getTime());					
					logger.info("Archiving Usage Logs older than " + archiveDate);
					archiveSMS(conn, activeFolderName + FILE_NAME_SMS, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveSMSReceived(conn, activeFolderName + FILE_NAME_SMS_RECEIVED, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveFTPReport(conn, activeFolderName + FILE_NAME_FTP_REPORT, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveAuditTrail(conn, activeFolderName + FILE_NAME_AUDIT_TRAIL, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveBulkSmsSetup(conn, activeFolderName + FILE_NAME_BULK_SMS_SETUP, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveChgpwdHist(conn, activeFolderName + FILE_NAME_CHGPWD_HIST, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveLogonHist(conn, activeFolderName + FILE_NAME_LOGON_HIST, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveSmsAuditTrail(conn, activeFolderName + FILE_NAME_SMS_AUDIT_TRAIL, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveSmsHistory(conn, activeFolderName + FILE_NAME_SMS_HISTORY, new java.sql.Date(archiveCal.getTime().getTime()));
					archiveSmsReceivedDelete(conn, activeFolderName + FILE_NAME_SMS_RECEIVED_DELETE, new java.sql.Date(archiveCal.getTime().getTime()));
					logger.info("All Usage Log Archive done.");
				}
				
				activeFolderName = activeFolderName.substring(0, activeFolderName.length() - 1);
				String zipDest = activeFolderName + ".zip";
				zipLogFolder(activeFolderName, zipDest);
				logger.info("Zipped archive folder -> '" + zipDest + "'");
				deleteWholeFolder(new File(activeFolderName));
				logger.info("Removed archive folder.");
			}
			
		} catch (Exception e) {
			logger.error("Program exiting. Caught unexpected exception. Reason: " + e.getMessage(), e);
		}finally{
			try {
				logger.info("Closing database connection");
				connection.close();
				logger.info("Exiting program");
			} catch (SQLException e) {
				logger.error("Error closing database connection. Reason: " + e.getMessage(), e);
			}
		}
	}
}
