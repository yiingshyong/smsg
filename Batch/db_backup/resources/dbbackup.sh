#!/usr//bin/bash

################################################################
#CONFIGURATION. PLEASE MODIFY 

#number of backup before old backup is overwritten 
# if value > 1 is set, then the binlog after mysqldump for today will be backup on the next day 
# if value = 1, there will be no bin log backup, bin log will remain in the mysql directory
backup_days=3
basedir=@dbbackup.dir@
dbuser=@db.user@
dbpass=@db.password@
dbname=@dbbackup.mysql.dbname@
#mysql bin log directory
binlogfiles=@dbbackup.master_bin_logs@
################################################################

echo "Current date `date`" 

backup_index=`date +%Y%m%d`

#today directory
today_dir="backup-"$backup_index
backup_dir=$basedir"/"$today_dir
mkdir -p $backup_dir
cd $backup_dir

echo "backup bin logs"
cp $binlogfiles . 

echo "dumping database"
@dbbackup.mysql.basedir@/bin/mysqldump -u"$dbuser" -p"$dbpass" "$dbname" --master-data=2 --single-transaction --flush-logs |/usr/bin/gzip -9 > backup_`date +%Y%m%d`.sql.gz

cd ..

echo "zipping backup files for today"
zip -r "$backup_dir.zip" $today_dir

echo "remove backup files for today"
rm -rf $today_dir

baksize=$(ls -l | grep "backup-" | wc -l)
echo "backup file size is $baksize" 
if [ "$baksize" -gt "$backup_days" ] ; then
        oldbak=$(ls -l | head -2)
	echo "remove expried backup $oldbak"
        rm -rf $oldbak
fi

echo "Backup file for today located at $backup_dir.zip"

