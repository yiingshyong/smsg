#!/usr//bin/bash

################################################################
#mysql slave offline backup
#CONFIGURATION. PLEASE MODIFY 

#number of backup before old backup is overwritten 
# if value > 1 is set, then the binlog after mysqldump for today will be backup on the next day 
# if value = 1, there will be no bin log backup, bin log will remain in the mysql directory
backup_days=3
basedir=@dbbackup.dir@
dbuser=@db.user@
dbpass=@db.password@
dbname=@dbbackup.mysql.dbname@
#mysql bin log directory
slave_relay_logs=@dbbackup.slave_relay_logs@
slave_info_logs=@dbbackup.slave.info@
################################################################

echo "Current date `date`" 

backup_index=`date +%Y%m%d`

#today directory
today_dir="backup-"$backup_index
backup_dir=$basedir"/"$today_dir
mkdir -p $backup_dir
cd $backup_dir

echo "backup relay logs"
cp $slave_relay_logs . 
cp $slave_info_logs . 

echo "Stopping slave connection"
@dbbackup.mysql.basedir@/bin/mysql -u"$dbuser" -p"$dbpass" -e"stop slave;"

echo "dumping database"
@dbbackup.mysql.basedir@/bin/mysqldump -u"$dbuser" -p"$dbpass" "$dbname" |/usr/bin/gzip -9 > backup_`date +%Y%m%d`.sql.gz

echo "Start slave connection"
@dbbackup.mysql.basedir@/bin/mysql -u"$dbuser" -p"$dbpass" -e"start slave;"

cd ..

echo "zipping backup files for today"
zip -r "$backup_dir.zip" $today_dir

echo "remove backup files for today"
rm -rf $today_dir

baksize=$(ls -l | grep "backup-" | wc -l)
echo "backup file size is $baksize" 
if [ "$baksize" -gt "$backup_days" ] ; then
        oldbak=$(ls -l | head -2)
	echo "remove expried backup $oldbak"
        rm -rf $oldbak
fi

echo "Backup file for today located at $backup_dir.zip"

