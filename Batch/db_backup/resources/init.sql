use @db.name@;
SET @max_id = (select max(id)+1 from (select max(a.sms_id) as id from sms a union select max(b.sms_id) as id from sms_queue b) c);
insert into sms_queue(sms_id,version) values(@max_id,1);
delete from sms_queue where sms_id=@max_id;
