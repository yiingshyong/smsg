import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class BackupEmailAlert {
	private static final Log log = LogFactory.getLog(BackupEmailAlert.class);
	private static final String MAIL_STATUS_SUCCESS = "SUCCESS";
	private static final String NEW_LINE = System.getProperty("line.separator");  
		
	public static void main(String[] args) {
		log.info("Start program .." );
		new BackupEmailAlert().emailDbBackupResult();
	}
		
	public void emailDbBackupResult(){
		try{				
			ResourceBundle rb = ResourceBundle.getBundle("settings");
			
			String subject = rb.getString("dbbackup.fail.email.subject");
			
			//The email body header
			StringBuffer mailContentSB = new StringBuffer();			
			mailContentSB.append(rb.getString("dbbackup.fail.email.message"));
			mailContentSB.append(NEW_LINE);
			mailContentSB.append("Server IP: ").append(NEW_LINE);
			mailContentSB.append(getServerInfo());
			mailContentSB.append(NEW_LINE).append(NEW_LINE);
			
			//the error log file
			File errorLog = new File(rb.getString("errorlog"));
			File generalLog = new File(rb.getString("generalLog"));
			
			if(errorLog.exists() && errorLog.length() > 0){				
				log.info("Found error log.");
				subject = rb.getString("dbbackup.fail.email.subject");				
				mailContentSB.append("Error log shows: ").append(NEW_LINE);
				BufferedReader br = new BufferedReader(new FileReader(errorLog));
				String line = "";
				while( (line=br.readLine()) != null){
					mailContentSB.append(line);
				}
				br.close();
			}else if(generalLog.exists() && (System.currentTimeMillis()-generalLog.lastModified()) > (2*60*60*1000) ){
				subject = rb.getString("dbbackup.notRunning.email.subject");				
				String message = "Abnormal log file date -> " + new Date(generalLog.lastModified()) + ". Db backup might not have run. Please check.";
				log.info(message);
				mailContentSB.append(message);
			}else{
				subject = rb.getString("dbbackup.success.email.subject");				
				String message = rb.getString("dbbackup.success.email.message");
				log.info(message);
				mailContentSB.append(message);							
			}
			
			String[] tos = StringUtils.split(rb.getString("dbbackup.email.recipients"),",");
			String from = rb.getString("dbbackup.email.from");
			String smtpHost = rb.getString("smtp.host");
			
			for(String to : tos){
				String mailStatus = send(from, to, subject, mailContentSB.toString(), smtpHost);
				if(mailStatus.equals(MAIL_STATUS_SUCCESS)){
				}else{
					log.info("Email alert failed to send. Error: " + mailStatus);
				}
			}
			log.info("Email sent, stopping job.");
		}catch(Exception e){
			log.error("Error while checking db backup status.Due to: " + e.getMessage(), e);
		}
	}
	
	public String send(String sender, String recipient, String subject, String message, String smtpHost) 
	{
		
		String status = "";
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", smtpHost);
		Session session = Session.getDefaultInstance(properties, null);		
		session.setDebug(true); 
		
		try {
			
			MimeMessage mimeMessage = new MimeMessage(session); // Create a New message
			
			mimeMessage.setFrom(new InternetAddress(sender)); // Set the From address
			
			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
						
			mimeMessage.setSubject(subject); 
			
			MimeBodyPart mbp = new MimeBodyPart();
			mbp.setText(message);
			
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp);					
			mimeMessage.setContent(mp);
			mimeMessage.setSentDate(new Date());
			
			Transport.send(mimeMessage);
			
			status = MAIL_STATUS_SUCCESS;
		} catch (Exception e) {
			status = e.toString();
			log.error("Error sending email. Reason: " + e.getMessage(), e);			
		}
		return status;
	} 

	private String getServerInfo() throws Exception{
		StringBuffer sb = new StringBuffer();
		Enumeration e = NetworkInterface.getNetworkInterfaces();
		
        while(e.hasMoreElements()) {
           NetworkInterface ni = (NetworkInterface) e.nextElement();
           sb.append("Network interface: "+ni.getName());
           sb.append(NEW_LINE);

           Enumeration e2 = ni.getInetAddresses();

           while (e2.hasMoreElements()){
              InetAddress ip = (InetAddress) e2.nextElement();
              sb.append("IP address: "+ ip.toString()).append(NEW_LINE);
           }		
        }
        return sb.toString();
	}
	
}
