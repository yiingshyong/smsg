#!/usr/bin/bash

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo "Load master cron jobs"

runExec "crontab @basedir@/Batch/services/common/cron-db-master"

echo "Execute sql command to convert slave to master"
@db.bin@ -u@db.user@ -p@db.password@ < @basedir@/Batch/services/common/slave_to_master.sql

echo "Converted as slave master"

