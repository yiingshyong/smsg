#!/usr/bin/bash

#JAVA_HOME=/usr/jdk/instances/jdk1.5.0
#export JAVA_HOME

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo "Load primary cron jobs"

runExec "crontab @basedir@/Batch/services/common/cron-primary"

echo "Start Passive Services"

#Hardcoded application path, change when necessary
runExec "/app/smsg_my/Batch/services/passive-services-start.sh"
runExec "/app/smsg_sg/Batch/services/passive-services-start.sh"

echo "Link start script to primary script"

rm -r @common.script.dir@/*

runExec "ln -s /app/smsg_my/Batch/services/primary-start.sh @common.script.dir@/sstart-my"
runExec "ln -s /app/smsg_sg/Batch/services/primary-start.sh @common.script.dir@/sstart-sg"

runExec "ln -s /app/smsg_my/Batch/services/primary-stop.sh @common.script.dir@/send-my"
runExec "ln -s /app/smsg_sg/Batch/services/primary-stop.sh @common.script.dir@/send-sg"

echo "Converted as primary server."
echo "Total Java Process Running: " `ps -ef|grep java|wc -l`