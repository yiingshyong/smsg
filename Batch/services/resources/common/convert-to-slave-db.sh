#!/usr/bin/bash

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo "Load slave cron jobs"

runExec "crontab @basedir@/Batch/services/common/cron-db-slave"

echo "Execute sql command to convert master to slave"
@db.bin@ -u@db.user@ -p@db.password@ < @basedir@/Batch/services/common/master_to_slave.sql

echo "Converted as slave database"