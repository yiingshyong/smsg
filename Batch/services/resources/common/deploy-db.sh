#!/usr/bin/bash

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su @app_user_id@ -c "$1"
fi
}

basedir=/app/deployment
current=$basedir/current
backup=$basedir/backup
archive=$basedir/archive
binary=$basedir/tmp

cd $basedir

echo "Executing $0 at `date +%Y-%m-%d_%H:%M:%S`"

echo "Moving previous backup at $backup to archive folder at $archive"
runExec "cp -r $backup/* $archive/"
rm -r $backup/*

echo "Backup current version at $current to backup folder at $backup"
runExec "cp -r $current/* $backup/"

echo "Stopping services"
#@common.script.dir@/send-my
#@common.script.dir@/send-sg

echo "Remove current binaries"
rm -r $current/*

echo "Deploying application"
echo "1. Unzip deployment files at $binary"
cd $binary
for i in `ls $binary`
do
chown @app_user_id@:@app_user_id@ $i
chmod 755 $i
echo "Unzip $binary/$i"
runExec "unzip -q -o $binary/$i"
done

echo "Moving zip file to archive"
mv $binary/*.zip $archive/

echo "2. Moving deployment folder to destination at $current"
mv $binary/* $current/

echo "3. Removing existing links"
rm -rf /app/smsg_my
rm -rf /app/smsg_sg

echo "4. Linking path to current binary at $current"
runExec "ln -s $current/`ls $current|grep _my` /app/smsg_my"
runExec "ln -s $current/`ls $current|grep _sg` /app/smsg_sg"

echo "5. Start services"
#@common.script.dir@/sstart-my
#@common.script.dir@/sstart-sg

echo "Deployment done at `date +%Y-%m-%d_%H:%M:%S`"