#!/usr/bin/bash

echo "Executing $0 at : `date +%Y-%m-%d_%H:%M:%S`"


runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su @app_user_id@ -c "$1"
fi
}

basedir=/app/deployment
current=$basedir/current
backup=$basedir/backup
archive=$basedir/archive
tmp=$basedir/tmp

cd $basedir

echo "Script will restore the binaries from $backup which contains `ls -lort $backup`"

echo "1. Stopping services"
#@common.script.dir@/send-my
#@common.script.dir@/send-sg

echo "2. Remove current files"
rm -r $current/*

echo "3. Move backup to current"
mv $backup/* $current/

echo "4. Removing existing links"
rm -rf /app/smsg_my
rm -rf /app/smsg_sg

echo "5. Linking path to current binary at $current"
runExec "ln -s $current/`ls $current|grep _my` /app/smsg_my"
runExec "ln -s $current/`ls $current|grep _sg` /app/smsg_sg"

echo "6. Start services"
#@common.script.dir@/sstart-my
#@common.script.dir@/sstart-sg


echo "Fallback done at `date +%Y-%m-%d_%H:%M:%S`"