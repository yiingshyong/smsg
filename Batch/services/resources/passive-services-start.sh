#!/usr/bin/bash

#JAVA_HOME=/usr/jdk/instances/jdk1.5.0
#export JAVA_HOME

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo Starting @appname@ Passive Services

echo 1. Starting ActiveMQ Server
runExec "@activemq.home@/bin/activemq start"
/bin/sleep 5

echo 2. Starting QFP
runExec "@basedir@/SgwCore/bin/qfp start"
/bin/sleep 5

echo 3. Starting SP
runExec "@basedir@/SgwCore/bin/sp start"
/bin/sleep 5

echo 4. Starting BP
runExec "@basedir@/SgwCore/bin/bp start"
/bin/sleep 5

echo 5. Starting Monitoring Engine
runExec "@basedir@/SgwCore/bin/monitor start"
/bin/sleep 5

echo ..Finished

