#!/usr/bin/bash

#JAVA_HOME=/usr/jdk/instances/jdk1.5.0
#export JAVA_HOME

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo Stopping @appname@ Passive Services

echo 1. Shutting Down QFP
runExec "@basedir@/SgwCore/bin/qfp stop"
/bin/sleep 5

echo 2. Shutting Down SP
runExec "@basedir@/SgwCore/bin/sp stop"
/bin/sleep 5

echo 3. Shutting Down BP
runExec "@basedir@/SgwCore/bin/bp stop"
/bin/sleep 5

echo 4. Shutting Down Monitoring Engine
runExec "@basedir@/SgwCore/bin/monitor stop"
/bin/sleep 5

echo 5. Shutting Down ActiveMQ Server
runExec "@activemq.home@/bin/activemq stop"
/bin/sleep 5

echo ..Finished
