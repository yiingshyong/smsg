#!/usr/bin/bash

echo Stopping @appname@ Services

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo 1. Shutting Down Tomcat
runExec "@tomcat.home@/bin/shutdown.sh"
/bin/sleep 5

echo 2. Shutting Down QFP
runExec "@basedir@/SgwCore/bin/qfp stop"
/bin/sleep 5

echo 3. Shutting Down SP
runExec "@basedir@/SgwCore/bin/sp stop"
/bin/sleep 5

echo 4. Shutting Down BP
runExec "@basedir@/SgwCore/bin/bp stop"
/bin/sleep 5

echo 5. Shutting Down TCPLISTENER
runExec "@basedir@/SgwCore/bin/tcplistener stop"
/bin/sleep 5

echo 6. Shutting Down Monitoring Engine
runExec "@basedir@/SgwCore/bin/monitor stop"
/bin/sleep 5

echo 7. Shutting Down SIBS TCPLISTENER
runExec "@basedir@/SgwCore/bin/sibs stop"
/bin/sleep 5

echo 8. Shutting Down SOCKET TCPLISTENER
runExec "@basedir@/SgwCore/bin/socket stop"
/bin/sleep 5

echo 9. Shutting Down ActiveMQ Server
runExec "@activemq.home@/bin/activemq stop"
/bin/sleep 5

#echo ---------- Shutting Down MySQL ----------
#@db.mysql.basedir@/bin/mysqladmin -u@db.user@ -p@db.password@ shutdown
#echo ---------- Finish Shutting Down MySQL ----------
#/bin/sleep 5

echo ..Finished
