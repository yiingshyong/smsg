#!/usr/bin/bash

#JAVA_HOME=/usr/jdk/instances/jdk1.5.0
#export JAVA_HOME

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo Starting @appname@ Services

echo 1. Starting Tomcat
runExec "@tomcat.home@/bin/startup.sh"
/bin/sleep 5

echo 2. Starting TCPLISTENER
runExec "@basedir@/SgwCore/bin/tcplistener start"
/bin/sleep 5

echo 3. Starting SIBS TCPLISTENER
runExec "@basedir@/SgwCore/bin/sibs start"
/bin/sleep 5

echo 4. Starting SOCKET TCPLISTENER
runExec "@basedir@/SgwCore/bin/socket start"
/bin/sleep 5

echo ..Finished

