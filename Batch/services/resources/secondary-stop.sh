#!/usr/bin/bash

echo Stopping @appname@ Services

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo 1. Shutting Down Tomcat
runExec "@tomcat.home@/bin/shutdown.sh"
/bin/sleep 5

echo 2. Shutting Down TCPLISTENER
runExec "@basedir@/SgwCore/bin/tcplistener stop"
/bin/sleep 5

echo 2. Shutting Down SIBS TCPLISTENER
runExec "@basedir@/SgwCore/bin/sibs stop"
/bin/sleep 5

echo 3. Shutting Down SOCKET TCPLISTENER
runExec "@basedir@/SgwCore/bin/socket stop"
/bin/sleep 5

echo ..Finished
