#!/usr/bin/bash

#JAVA_HOME=/usr/jdk/instances/jdk1.5.0
#export JAVA_HOME

runExec(){
if [ -n "$EUID" -a "$EUID" != "0" ]; then
        $1
else
        su - @app_user_id@ -c "$1"
fi
}

echo Starting @appname@ Services

#echo ---------- Starting MySQL ----------
#@db.mysql.basedir@/bin/mysqld --user=@db.user@ --datadir=@db.mysql.datadir@ &
#echo ---------- Finish Starting MySQL ----------
#/bin/sleep 10

echo 1. Starting ActiveMQ Server
runExec "@activemq.home@/bin/activemq start"
/bin/sleep 5

echo 2. Starting Tomcat
runExec "@tomcat.home@/bin/startup.sh"
/bin/sleep 5

echo 3. Starting QFP
runExec "@basedir@/SgwCore/bin/qfp start"
/bin/sleep 5

echo 4. Starting SP
runExec "@basedir@/SgwCore/bin/sp start"
/bin/sleep 5

echo 5. Starting TCPLISTENER
runExec "@basedir@/SgwCore/bin/tcplistener start"
/bin/sleep 5

echo 6. Starting BP
runExec "@basedir@/SgwCore/bin/bp start"
/bin/sleep 5

echo 7. Starting Monitoring Engine
runExec "@basedir@/SgwCore/bin/monitor start"
/bin/sleep 5

echo 8. Starting SIBS TCPLISTENER
runExec "@basedir@/SgwCore/bin/sibs start"
/bin/sleep 5

echo 9. Starting SOCKET TCPLISTENER
runExec "@basedir@/SgwCore/bin/socket start"
/bin/sleep 5

echo ..Finished

