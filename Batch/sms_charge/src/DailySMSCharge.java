import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;


public class DailySMSCharge {
	private static final Log log = LogFactory.getLog(DailySMSCharge.class);
	private static final String MAIL_STATUS_SUCCESS = "SUCCESS";
	private static final String NEW_LINE = System.getProperty("line.separator");
	static final String RESOURCE_FILE= "dailysmscharge";
	static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	private static SimpleDateFormat _dateFormatter;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean status=true;
		// TODO Auto-generated method stub
		try {
			runbatchOutgoingSMS();
			runbatchIncomingSMS();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			status=false;
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			status=false;
			e.printStackTrace();
		}
		emailResult(status);
	}
	
	private static void runbatchOutgoingSMS() throws ClassNotFoundException, SQLException{
		
		Class.forName(getKey("JdbcClassName"));
		String url =
			getKey("JdbcURL");
		Statement stmt;
		
		Connection con =
          DriverManager.getConnection(
                      url,getKey("DBUSERNAME"), getDecrypted(getKey("DBPASSWORD")));
		
		stmt = con.createStatement();
		int reverseDay = Integer.parseInt(getKey("REVERSEDAY"));
		
		String startTime = "";
		String endTime = "";
		for (int i =0; i < reverseDay; i ++){
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE,i * -1);
			String chargeDate = stringByDayMonthYear(c.getTime());
			startTime = chargeDate + " 00:00:01";
			endTime = chargeDate + " 23:59:59"; 
			System.out.println("reverse: " + i*-1 + " "+ stringByDayMonthYear(c.getTime()));
			ResultSet rs;
//			rs = 
//				stmt.executeQuery("select sum(sms_charge) as total, " +
//						"sum(total_sms) as count, dept, t.telco_id as telco_id from sms s, telco t " +
//						"where t.telco_id = s.telco and s.sms_status='S' and s.sms_status_datetime between " + "'"+ startTime +"'" +
//						"and '"+ endTime +"'" +
//						" group by dept, telco order by dept");
			
			rs = 
			stmt.executeQuery("select sum(sms_charge) as total, sum(total_sms) as count, " +
					"dept, t.telco_id as telco_id, " +
					"t.svc_pvd_id as svc_pvd_id, s.created_by as user_id, " +
					"s.channel_id as channel_id, s.sms_rate_id as rate_id " +
					"from sms s, telco t " +
					"where t.telco_id = s.telco and s.sms_status='S' and s.sms_status_datetime between " + "'"+ startTime +"' " +
					"and '"+ endTime +"'" +
					" group by dept,s.created_by,channel_id, svc_pvd_id,rate_id order by dept,user_id,channel_id,svc_pvd_id,rate_id");
			while(rs.next()){
				
		        int telco_id= rs.getInt("telco_id");
		        String dept = rs.getString("dept");
		        double total= rs.getDouble("total");
		        int count= rs.getInt("count");
		        String userId = rs.getString("user_id");
		        String channelId = rs.getString("channel_id");
		        String svcProviderId = rs.getString("svc_pvd_id");
		        String rateId = rs.getString("rate_id");
		        
		        Double gst = getOutgoingGst(con, total,svcProviderId);		        
		        saveDataOutgoing(con, dept, telco_id, total, count, chargeDate, userId, channelId, svcProviderId, rateId, gst);
		        System.out.println("\telco_id= " + telco_id
		                             + " dept = " + dept);
		    }//end while loop
		}
		
		con.close();
	}
	
	
	private static void runbatchIncomingSMS() throws ClassNotFoundException, SQLException{
		
		Class.forName(getKey("JdbcClassName"));
		String url =
			getKey("JdbcURL");
		Statement stmt;
		
		Connection con =
          DriverManager.getConnection(
                      url,getKey("DBUSERNAME"), getKey("DBPASSWORD"));
		
		stmt = con.createStatement();
		int reverseDay = Integer.parseInt(getKey("REVERSEDAY"));
		
		String startTime = "";
		String endTime = "";
		for (int i =0; i < reverseDay; i ++){
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE,i * -1);
			String chargeDate = stringByDayMonthYear(c.getTime());
			startTime = chargeDate + " 00:00:01";
			endTime = chargeDate + " 23:59:59";
			System.out.println("reverse: " + i*-1 + " "+ stringByDayMonthYear(c.getTime()));
			ResultSet rs;
			rs = 
				stmt.executeQuery("select sum(sms_charge) as total, " +
						"count(sms_id) as count, t.telco_id as telco_id, s.dept as dept from sms_received s, telco t " +
						"where t.telco_id = s.telco and s.received_datetime between " + "'"+ startTime +"'" +
						"and '"+ endTime +"'" +
						" group by dept, telco order by dept");
			
			while(rs.next()){
				
		        int telco_id= rs.getInt("telco_id");
		        String dept = rs.getString("dept");
		        int total= rs.getInt("total");
		        int count= rs.getInt("count");
		        
		        Double gst =  getIncomingGst(con, total,telco_id); // to be check later - done!		        
		        saveDataIncoming(con, dept, telco_id, total, count, chargeDate, gst);
		        System.out.println("\telco_id= " + telco_id + " dept = " + dept);
		    }//end while loop
		}
		
		con.close();
	}

	private static void saveDataOutgoing(Connection con, String dept, int telco_id, double totalCharge, int count, String chargeDate,
			String userId, String channelId, String svcProviderId, String rateId, Double gst) throws SQLException{
				
		Statement stmt = con.createStatement();
		boolean rcFound = false;
		ResultSet rs;
		rs = 
			stmt.executeQuery("Select count(id) as count from SMS_CHARGE_DAILY " +
					"where CHARGE_DATE = '" + chargeDate + "' " +
					"and TELCO_ID = " + telco_id + " and dept = '" + dept + "' " +
					"and user_id = '" + userId + "' and channel_id = '" + channelId + "' " +
					"and svc_provider_id = " + svcProviderId + " and sms_rate_id =" + rateId + " " +
					"and SMS = 'O'");
		while(rs.next()){
			int rccount= rs.getInt("count");
			if (rccount > 0)
				rcFound = true;
		}
		
		if (rcFound){
			System.out.println("update ");
			stmt.executeUpdate("Update SMS_CHARGE_DAILY set TOTAL_CHARGE = " + totalCharge + " , TOTAL_COUNT = " + count + " , GST_CHARGE =" + gst + " " +
								"where CHARGE_DATE = '" + chargeDate + "' " +
								"and TELCO_ID = " + telco_id + " and dept = '" + dept + "' " +
								"and user_id = '" + userId + "' and channel_id = '" + channelId + "' " +
								"and svc_provider_id = " + svcProviderId + " and sms_rate_id =" + rateId + " " +
								//"and GST_CHARGE =" + gst + " " +
								"and SMS = 'O'");
		}else{
			System.out.println("insert ");
			stmt.executeUpdate(
                "INSERT INTO SMS_CHARGE_DAILY(dept, CHARGE_DATE, CREATED_DATETIME" +
                ", TELCO_ID, TOTAL_CHARGE, TOTAL_COUNT, sms" +
                ", USER_ID, CHANNEL_ID, SVC_PROVIDER_ID, SMS_RATE_ID" +
                ", GST_CHARGE) " +
                " VALUES ( '" + dept + "', '" + chargeDate + 
                "', CURDATE(), " + telco_id +", " + totalCharge + 
                ", " + count + ", 'O', '" + userId + "', '" + channelId + 
                "', " + svcProviderId + ", " + rateId + ", " + gst +
                ")"
                );
		}
	}
	
	private static void saveDataIncoming(Connection con, String dept, int telco_id, int totalCharge, int count, String chargeDate, Double gst) throws SQLException{

		System.out.println("test incoming ");
				Statement stmt = con.createStatement();
		boolean rcFound = false;
		ResultSet rs;
		rs = 
			stmt.executeQuery("Select count(id) as count from SMS_CHARGE_DAILY where CHARGE_DATE = '" + chargeDate + "' " +
					" and TELCO_ID = " + telco_id + " and dept = '" + dept + "' and SMS = 'I'");
		while(rs.next()){
			int rccount= rs.getInt("count");
			if (rccount > 0)
				rcFound = true;
		}
		
		if (rcFound){
			System.out.println("update ");
			stmt.executeUpdate(" Update SMS_CHARGE_DAILY set TOTAL_CHARGE = " + totalCharge + " , TOTAL_COUNT = " + count + " , GST_CHARGE = " + gst + " " +
				" where TELCO_ID = " + telco_id + " and dept = '" + dept + "' and CHARGE_DATE = '" + chargeDate + "' and sms = 'I'");
		}else{
			System.out.println("insert ");
			stmt.executeUpdate(
                "INSERT INTO SMS_CHARGE_DAILY(dept, CHARGE_DATE, CREATED_DATETIME, TELCO_ID, TOTAL_CHARGE, TOTAL_COUNT, GST_CHARGE, SMS) " +
                " VALUES ( '" + dept + "', '" + chargeDate + "', CURDATE(), " + telco_id +", " + totalCharge + ", " + count + ", " + gst+ ", 'I')");
		}

	}
	
	
	
	public static String getKey(String KEY){
		
		ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_FILE);

		return rb.getString(KEY);
	}
	
	private static String stringByDayMonthYear(Date date){
		 _dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
    	if (date == null){
    		return "";
    	}
		
		return _dateFormatter.format(date);
	}
	
	private static Double getOutgoingGst(Connection con, double cost, String svc_provider){
		double costStr = roundTwoDecimals(cost); 
		Double calcGst = 0.00000000; 
		try{			

			Statement stmt = con.createStatement();			
			ResultSet rs;
			rs = stmt.executeQuery("select outgoing_gst_charge, id, name from service_provider "+
				 "where id = "+Integer.valueOf(svc_provider));
			
			if (rs.next()){	
				
				BigDecimal charges = rs.getBigDecimal("outgoing_gst_charge");			
				//BigDecimal charges = (BigDecimal) sp.getOutgoingGstCharge();
				BigDecimal percent = new BigDecimal(100);				
				percent = charges.divide(percent, 6, RoundingMode.FLOOR); 		
				BigDecimal total = (BigDecimal) BigDecimal.valueOf(costStr);  
				BigDecimal getCalc = total.multiply(percent); 
				//Double roundVal = getCalc.setScale(4,RoundingMode.HALF_UP).doubleValue();
				calcGst = getCalc.setScale(8).doubleValue(); log.info("calcGstOUT:"+calcGst);
    		}		
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			ex.printStackTrace();
		}
		return calcGst;
	}
	
	private static Double getIncomingGst(Connection con, int cost, int telco){
		//String costStr= String.valueOf(cost);
		Double calcGst = 0.00000000;
		
		try{			
			Statement stmt = con.createStatement();			
			ResultSet rs;
			rs = stmt.executeQuery("select svc_pvd_id from telco "+
				 "where telco_id = "+Integer.valueOf(telco));
						
			if (rs.next()) {	
				
				Statement stmtSp = con.createStatement();			
				ResultSet rsSp;
				rsSp = stmtSp.executeQuery("select incoming_gst_charge from service_provider "+
					 "where id = "+rs.getInt("svc_pvd_id"));
				
				if (rsSp.next()){
					
					BigDecimal charges = rsSp.getBigDecimal("incoming_gst_charge");						
					BigDecimal percent = new BigDecimal(100);				
					percent = charges.divide(percent, 6, RoundingMode.FLOOR);
					BigDecimal total = (BigDecimal) BigDecimal.valueOf(cost);
					BigDecimal getCalc = total.multiply(percent); 
					//calcGst = FormatUtil.getInstance().formatDecimal(getCalc, 4) ;
					//Double roundVal = getCalc.setScale(4,RoundingMode.HALF_UP).doubleValue();
					calcGst = getCalc.setScale(8).doubleValue(); log.info("calcGstIN:"+calcGst);
					//calcGst = roundVal; //FormatUtil.getInstance().formatNumber(roundVal,4); 
	    		}
    		}
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			ex.printStackTrace();
		}
		return calcGst;
	}
	
	public static void emailResult(boolean status){ 
		try{				
			ResourceBundle ch = ResourceBundle.getBundle(RESOURCE_FILE);
			
			String subject = ch.getString("dailybatch.success.email");
			
			//The email body header
			StringBuffer mailContentSB = new StringBuffer();			
			//mailContentSB.append(rb.getString("dailybatch.success.email.my"));
					
			if(status == true){
				subject = ch.getString("dailybatch.success.email");				
				String message = ch.getString("dailybatch.success.email");
				log.info(message);
				mailContentSB.append(message);
			}else if(status == false){
				subject = ch.getString("dailybatch.fail.email");				
				String message = ch.getString("dailybatch.fail.email");
				log.info(message);
				mailContentSB.append(message);		
			}

			String[] tos = StringUtils.split(ch.getString("dbbackup.email.recipients"),",");
			String from = ch.getString("dbbackup.email.from");
			String smtpHost = ch.getString("smtp.host");
			for(String to : tos){ 
				String mailStatus = send(from, to, subject, mailContentSB.toString(), smtpHost);
				if(mailStatus.equals(MAIL_STATUS_SUCCESS)){
				}else{
					log.info("Email alert failed to send. Error: " + mailStatus);
				}
			}
			log.info("Email sent, stopping job.");
		}catch(Exception e){
			log.error("Error while checking db backup status.Due to: " + e.getMessage(), e);
		}
	}
	
	public static String send(String sender, String recipient, String subject, String message, String smtpHost) 
	{
		
		String status = "";
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", smtpHost);
		Session session = Session.getDefaultInstance(properties, null);		
		session.setDebug(true); 
		
		try {
			
			/*MimeMessage mimeMessage = new MimeMessage(session); // Create a New message
			
			mimeMessage.setFrom(new InternetAddress(sender)); // Set the From address
			
			mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
						
			mimeMessage.setSubject(subject); 
			
			MimeBodyPart mbp = new MimeBodyPart();
			mbp.setText(message);
			
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp);					
			mimeMessage.setContent(mp);
			mimeMessage.setSentDate(new Date());
			
			Transport.send(mimeMessage);*/
			
			status = MAIL_STATUS_SUCCESS;
		} catch (Exception e) {
			status = e.toString();
			log.error("Error sending email. Reason: " + e.getMessage(), e);			
		}
		return status;
	} 
	
	 public static double roundTwoDecimals(double d) {
	      DecimalFormat twoDForm = new DecimalFormat("#.##");
	      return Double.valueOf(twoDForm.format(d));
	 }
	 
	 public static String getDecrypted(String encrypted)
		{
			String encryptionKey = "mysql";
			String algorithm = "PBEWithMD5AndDES";
			
			StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
			
			if (algorithm != "") {
			  encryptor.setAlgorithm(algorithm);
			} else {
			  System.out.println("----------------------------------------------------------------------------");
			  System.out.println("WARNING: No encryption/decryption algorithm define, default to JASPYT preset algorithm!");
			  //log4jUtil.info("WARNING: No encryption/decryption algorithm define, default to JASPYT preset algorithm!");
			}

			if (encryptionKey != "") {
			  encryptor.setPassword(encryptionKey);
			} else {
			  System.out.println("--------------------------------------------------------");
			  System.out.println("ERROR: No encryption/decryption key define, fail to process! Abort!");
			  //log4jUtil.info("ERROR: No encryption/decryption key define, fail to process! Abort!");
			  return "";
			}
			return encryptor.decrypt(encrypted);
		}
}
