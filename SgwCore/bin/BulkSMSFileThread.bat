 @echo off
 setLocal EnableDelayedExpansion
 set CLASSPATH=..\classes"
 for /R ./../lib %%a in (*.jar) do (
   set CLASSPATH=!CLASSPATH!;%%a
 )
 set CLASSPATH=!CLASSPATH!"
 echo !CLASSPATH!

java -cp %CLASSPATH% com/bcb/sgwcore/senderprocessor/BulkSmsFileThread