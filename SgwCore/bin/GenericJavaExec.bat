 @echo off
 setLocal EnableDelayedExpansion
 set CLASSPATH=..\classes"
 for /R ./../lib %%a in (*.jar) do (
   set CLASSPATH=!CLASSPATH!;%%a
 )
 set CLASSPATH=!CLASSPATH!"
 echo !CLASSPATH!

echo "Usage Example: %~nx0 com/cimb/sibs/TcpClientSimulator"
java -cp %CLASSPATH% %*
