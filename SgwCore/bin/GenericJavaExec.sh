#!/usr/bin/bash

cp=../classes

for i in `ls ../lib/*.jar` 
do
cp=$cp:$i
done

echo $cp

echo "Usage: $0 <javaMainClass> <param1> <param2> <paramn>" 
echo "example $0 com/cimb/sibs/TcpClientSimulator"
java -cp $cp -Dsgw.home=/tmp/ $@ 
