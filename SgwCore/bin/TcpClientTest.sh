#!/usr/bin/bash

cp=../classes

for i in `ls ../lib/*.jar` 
do
cp=$cp:$i
done

echo $cp

echo "Usage: ./TcpClientTest.sh <mobileNum> <smsMode: 1-text/2-utf> <priority:1-9> <senderUserId> <message>" 
echo example ./TcpClientTest.sh 0126900868 1 9 012345 \"Testing from tcpclient\"
java -cp $cp -Dsgw.home=/tmp/ com/cimb/tcplistener/processor/TcpClientTest $1 $2 $3 $4 $5 $6 $7 $8 $9
