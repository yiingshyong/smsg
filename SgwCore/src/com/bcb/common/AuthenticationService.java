package com.bcb.common;

import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import com.bcb.common.modal.User;
import com.bcb.common.util.SHAUtil;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class AuthenticationService {
	
	private static final ResourceBundle RB = ResourceBundle.getBundle("com.bcb.common.util.MessageResources");
	
	public static String authenticate(String userId, String password, int permId) throws Exception{
		if(StringUtils.isEmpty(userId)) return RB.getString("errors.logon.userid.blank");
		if(StringUtils.isEmpty(password)) return RB.getString("errors.logon.password.blank");
		
		User user = DaoBeanFactory.getUserDao().findByUserId(userId);
		if(user == null) return RB.getString("errors.logon.notexist");
		else{
			if(!user.getPassword().equals(SecurityManager.getInstance().encryptSHA2(password))) return RB.getString("errors.logon.password");
			if(!user.getUserStatus().equals("Active")) return RB.getString("errors.logon.disabled");
			if(user.getFailedLogon() > 2) return RB.getString("errors.logon.suspend");
		}
			
		if(!DaoBeanFactory.getUserDao().hasPermission(userId, permId)){
			return RB.getString("errors.logon.accessdenied");
		}
		return null;
	}
	
		
}
