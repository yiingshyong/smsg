package com.bcb.common;

import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.remote.JMXAuthenticator;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXPrincipal;
import javax.management.remote.JMXServiceURL;
import javax.security.auth.Subject;

import com.bcb.crsmsg.util.Constants;

/*
 This CustomAgent will start an RMI COnnector Server using only port "custom.rmi.agent.port".
*/

public class CustomJmxAgent {

   private CustomJmxAgent() { }

   //The Instrument argument should be taken off if using JAVA 6
   public static void premain(String agentArgs, Instrumentation instrument) throws IOException {

       // Ensure cryptographically strong random number generator used
       // to choose the object number - see java.rmi.server.ObjID
       //
       System.setProperty("java.rmi.server.randomIDs", "true");

       // Start an RMI registry on port specified by example.rmi.agent.port
       // (default 3000).
       //
       final int port= Integer.parseInt(System.getProperty("custom.rmi.agent.port","3000"));
       System.out.println("Create RMI registry on port "+port);
       LocateRegistry.createRegistry(port);

       // Retrieve the PlatformMBeanServer.
       //
       System.out.println("Get the platform's MBean server");
       MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

       // Environment map.
       //
       System.out.println("Initialize the environment map");
       HashMap<String,Object> env = new HashMap<String,Object>();
       String javaHome = System.getenv("JAVA_HOME");
       if(javaHome == null || javaHome.trim().length() == 0){
    	   throw new RuntimeException("JAVA_HOME enviroment variable is not set.");
       }
//       env.put("jmx.remote.x.password.file", "C:/Java/jdk1.5.0_06/jre/lib/management/jmxremote.password");
//       env.put("jmx.remote.x.access.file", "C:/Java/jdk1.5.0_06/jre/lib/management/jmxremote.access");
//     env.put("jmx.remote.x.password.file", javaHome + "/jre/lib/management/jmxremote.password");
//     env.put("jmx.remote.x.access.file", javaHome + "/jre/lib/management/jmxremote.access");
       
       // This where we would enable security - left out of this
       // for the sake of the example....
       //

       // Create an RMI connector server.
       //
       // As specified in the JMXServiceURL the RMIServer stub will be
       // registered in the RMI registry running in the local host on
       // port 3000 with the name "jmxrmi". This is the same name the
       // out-of-the-box management agent uses to register the RMIServer
       // stub too.
       //
       // The port specified in "service:jmx:rmi://"+hostname+":"+port
       // is the second port, where RMI connection objects will be exported.
       // Here we use the same port as that we choose for the RMI registry. 
       // The port for the RMI registry is specified in the second part
       // of the URL, in "rmi://"+hostname+":"+port
       //
       env.put(JMXConnectorServer.AUTHENTICATOR, createAuthenticator());
       System.out.println("Create an RMI connector server");
       final String hostname = InetAddress.getLocalHost().getHostName();
       JMXServiceURL url = new JMXServiceURL("service:jmx:rmi://" + hostname + ":" + port + "/jndi/rmi://" + hostname + ":" + port + "/jmxrmi");
       
       // Now create the server from the JMXServiceURL
       JMXConnectorServer cs =
           JMXConnectorServerFactory.newJMXConnectorServer(url, env, mbs);

       // Start the RMI connector server.
       System.out.println("Start the RMI connector server on port " + port);
       cs.start();
   }
   
   private static JMXAuthenticator createAuthenticator(){
	   JMXAuthenticator auth = new JMXAuthenticator() {
		
		public Subject authenticate(Object credentials) {
			try{
				System.out.println("Authenticating user ...");
	            if (!(credentials instanceof String[]))
	                throw new SecurityException("Bad Input");
	            String[] creds = (String[]) credentials;
	            if (creds.length != 2)
	                throw new SecurityException("Information not sufficient");
	
	            String userId = creds[0];
	            String password = creds[1];
	
	            if (password == null)
	                password = "";
	            
	            String result = AuthenticationService.authenticate(userId, password, Integer.parseInt(Constants.PERM_JMX)); 
	            if(result == null){
		            Set principals = new HashSet();
		            principals.add(new JMXPrincipal(userId));
		            return new Subject(false, principals, Collections.EMPTY_SET, Collections.EMPTY_SET);	            	
	            }else{
	            	throw new SecurityException(result);
	            }
	            
			}catch(Exception e){
				if(e instanceof SecurityException){
					throw new SecurityException(e.getMessage());
				}
				e.printStackTrace();				
	            throw new SecurityException("Exception While Authenticating User");
			}
		}
	};
	   return auth;
   }
}