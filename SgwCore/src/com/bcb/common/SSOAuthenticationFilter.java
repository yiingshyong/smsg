package com.bcb.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.util.Constants;



public class SSOAuthenticationFilter implements Filter {
	
	private static String SSO_AUTH_URL_VALUE = ""; 
	private static String SSO_LOGIN_URL_VALUE = "";
	private static final Log log = LogFactory.getLog(SSOAuthenticationFilter.class);
	
	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain filterChain) throws IOException, ServletException {
		
		try{			
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			String ssoToken = null;
			
			Cookie[] cookies = httpRequest.getCookies();
			if(cookies != null){
				for(Cookie c : cookies){
					if(c.getName().equals(Constants.SSO_AMQ_COOKIE_NAME)){
						ssoToken = c.getValue();
						break;
					}
				}
			}
			
			if(ssoToken == null || ssoToken.length() == 0){
				failAuth(response,request);
				return;
			}
			
		    String postData = URLEncoder.encode(Constants.SSO_TOKEN_PARAM, Constants.CHARSET_UNICODE) + "=" + URLEncoder.encode(ssoToken, Constants.CHARSET_UNICODE);
		    SSO_AUTH_URL_VALUE = MessageFormat.format(SSO_AUTH_URL_VALUE, request.getServerName());
		    URL url = new URL(SSO_AUTH_URL_VALUE + "?" + postData);
		    URLConnection conn = url.openConnection();
		    conn.setDoOutput(true);
		    // Get the response
		    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    String line = null;
		    StringBuffer sb = new StringBuffer();
		    while ((line = br.readLine()) != null) {
		        sb.append(line);
		        sb.append(Constants.LINE_SEPARATOR);
		    }	    	    
		    br.close();
		    
		    
		    if(sb.toString().contains(Constants.SSO_TOKEN_VALID)){
		    	filterChain.doFilter(request, response);
		    	return;
		    }else{
		    	failAuth(response, request);
		    	return;
		    }
		}catch(Exception e){
			log.error("Error while authenticating with SSO Server.", e);
			failAuth(response, request);
			return;
		}
	}

	private void failAuth(ServletResponse response, ServletRequest request) throws IOException{
    	if(SSO_LOGIN_URL_VALUE == null || SSO_LOGIN_URL_VALUE.trim().length() == 0){
    		response.setContentType( "text/html" );
    		PrintWriter writer = response.getWriter();
    		writer.write("You are not login.");
    		writer.flush();
    		writer.close();
    	}
    	((HttpServletResponse)response).sendRedirect(MessageFormat.format(SSO_LOGIN_URL_VALUE, request.getServerName()));		
	}
	
	public void init(FilterConfig arg0) throws ServletException {
		SSO_AUTH_URL_VALUE = arg0.getInitParameter(Constants.SSO_AUTH_URL_PARAM);
		SSO_LOGIN_URL_VALUE = arg0.getInitParameter(Constants.SSO_LOGIN_URL_PARAM);
		
		log.info("Setting up sso auth url - > " + SSO_AUTH_URL_VALUE);
		if(SSO_AUTH_URL_VALUE == null || SSO_AUTH_URL_VALUE.trim().length() == 0 ) throw new RuntimeException("SSO Authentication URL is not set. Please check web.xml");
		
		log.info("Setting up sso login url - > " + SSO_LOGIN_URL_VALUE);
		if(SSO_LOGIN_URL_VALUE == null || SSO_LOGIN_URL_VALUE.trim().length() == 0 ){
			log.warn("The SSO Failed Login URL is not configured.");
		}
		
	}

}
