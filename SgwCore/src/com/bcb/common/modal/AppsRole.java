package com.bcb.common.modal;
import java.util.ArrayList;
public class AppsRole {

	private String appsName;
	private ArrayList permList;

	public String getAppsName() {
		return appsName;
	}
	public void setAppsName(String appsName) {
		this.appsName = appsName;
	}
	public ArrayList getPermList() {
		return permList;
	}
	public void setPermList(ArrayList permList) {
		this.permList = permList;
	}



}
