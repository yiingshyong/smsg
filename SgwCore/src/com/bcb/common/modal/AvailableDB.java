package com.bcb.common.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class AvailableDB implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2828870352545167634L;

	/** identifier field */
    private Integer availableDBId;

    /** nullable persistent field */
    private String name;
    
    /** nullable persistent field */
    private String hibernateValue;
    
    /** nullable persistent field */
    private String availableDBStatus;
    
    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDateTime;






	public AvailableDB(Integer availableDBId, String name, String hibernateValue, String availableDBStatus,
			String createdBy, Date createdDateTime) {
		super();
		this.availableDBId = availableDBId;
		this.name = name;
		this.hibernateValue = hibernateValue;
		this.availableDBStatus = availableDBStatus;
		this.createdBy = createdBy;
		this.createdDateTime = createdDateTime;
	}



	/** default constructor */
    public AvailableDB() {
    }



	public Integer getAvailableDBId() {
		return availableDBId;
	}



	public void setAvailableDBId(Integer availableDBId) {
		this.availableDBId = availableDBId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getHibernateValue() {
		return hibernateValue;
	}



	public void setHibernateValue(String hibernateValue) {
		this.hibernateValue = hibernateValue;
	}



	public String getAvailableDBStatus() {
		return availableDBStatus;
	}



	public void setAvailableDBStatus(String availableDBStatus) {
		this.availableDBStatus = availableDBStatus;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public Date getCreatedDateTime() {
		return createdDateTime;
	}



	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}


}
