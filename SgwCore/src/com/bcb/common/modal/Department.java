package com.bcb.common.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Department implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2727890359545760654L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDate;

    /** nullable persistent field */
    private Date createdTime;

    /** nullable persistent field */
    private String deptDescription;

    /** nullable persistent field */
    private String deptLeader;

    /** nullable persistent field */
    private String deptName;

    /** nullable persistent field */
    private String deptStatus;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDate;

    /** nullable persistent field */
    private Date modifiedTime;
    
//    private int smsType;
//    private TblRef smsTypeObj;

//    public Department(Department dept, TblRef smsTypeObj){
//    	try{
//    		PropertyUtils.copyProperties(this, dept);
//        	this.setSmsTypeObj(smsTypeObj);
//    	}catch(Exception e){
//    		throw new RuntimeException("Error while creating Department Object. Reason: " + e.getMessage(), e);
//    	}
//    }
//    /** full constructor */
//    public Department(Integer id, String createdBy, Date createdDate, Date createdTime, String deptDescription, String deptLeader, String deptName, String deptStatus, String modifiedBy, Date modifiedDate, Date modifiedTime, int smsType) {
//        this.id = id;
//        this.createdBy = createdBy;
//        this.createdDate = createdDate;
//        this.createdTime = createdTime;
//        this.deptDescription = deptDescription;
//        this.deptLeader = deptLeader;
//        this.deptName = deptName;
//        this.deptStatus = deptStatus;
//        this.modifiedBy = modifiedBy;
//        this.modifiedDate = modifiedDate;
//        this.modifiedTime = modifiedTime;
//        this.smsType = smsType;
//    }
    public Department(Integer id, String createdBy, Date createdDate, Date createdTime, String deptDescription, String deptLeader, String deptName, String deptStatus, String modifiedBy, Date modifiedDate, Date modifiedTime) {
        this.id = id;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.deptDescription = deptDescription;
        this.deptLeader = deptLeader;
        this.deptName = deptName;
        this.deptStatus = deptStatus;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
    }

    /** default constructor */
    public Department() {
    }

    /** minimal constructor */
    public Department(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedTime() {
        return this.createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getDeptDescription() {
        return this.deptDescription;
    }

    public void setDeptDescription(String deptDescription) {
        this.deptDescription = deptDescription;
    }

    public String getDeptLeader() {
        return this.deptLeader;
    }

    public void setDeptLeader(String deptLeader) {
        this.deptLeader = deptLeader;
    }

    public String getDeptName() {
        return this.deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptStatus() {
        return this.deptStatus;
    }

    public void setDeptStatus(String deptStatus) {
        this.deptStatus = deptStatus;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedTime() {
        return this.modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

//	public int getSmsType() {
//		return smsType;
//	}
//
//	public void setSmsType(int smsType) {
//		this.smsType = smsType;
//	}
//
//	public TblRef getSmsTypeObj() {
//		return smsTypeObj;
//	}
//
//	public void setSmsTypeObj(TblRef smsTypeObj) {
//		this.smsTypeObj = smsTypeObj;
//	}

}
