package com.bcb.common.modal;
import java.util.ArrayList;
import java.util.HashMap;
public class LogonCredential implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6601840294340874936L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	private String userId;
	private String userName;
	private String userGroup;
	private String branchCode;
	private HashMap credential;
	private ArrayList<String> rights;
	
	public HashMap getCredential() {
		return credential;
	}
	public void setCredential(HashMap credential) {
		this.credential = credential;
	}
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public boolean allowed (String Apps,String modId)
	{
		if ( getCredential()!=null&&!getCredential().isEmpty()&&getCredential().containsKey(Apps))
		{
			HashMap appsmap = (HashMap) getCredential().get(Apps);
			if ( appsmap.containsKey(modId))
				return true;
		};
		
		return false;	
	}
	public void addCredential (String modId)
	{
		if (getCredential() == null)
			setCredential (new HashMap());
		
		getCredential().put(modId,modId);
	}
	public void addCredential (String Apps, String modId)
	{
		if (getCredential() == null)
			setCredential (new HashMap());
		if ( getCredential().containsKey(Apps))
		{
			// retrieve hash map
			HashMap appsmap = (HashMap) getCredential().get(Apps);
			appsmap.put(modId,modId);
		}
		else
		{
			HashMap appsmap = new HashMap();
			appsmap.put(modId,modId);
			getCredential().put(Apps,appsmap);
		};
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public ArrayList<String> getRights() {
		return rights;
	}
	public void setRights(ArrayList<String> rights) {
		this.rights = rights;
	}
}


