package com.bcb.common.util;

import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.sgwcore.util.CommonUtil;
 
public class Constants {

	/**SSO Constants**/
	public static final String SSO_AMQ_COOKIE_NAME="AMQCS";
	public static final String SSO_AMQ_COOKIE_PATH="/CRSMSG-AMQCS";
	public static final String SSO_TOKEN_PARAM =  "SSO_TOKEN";
	public static final String SSO_TOKEN_VALID = "SSO_AUTH_OK";
	public static final String SSO_AUTH_URL_PARAM = "SSO-AUTH-URL";
	public static final String SSO_LOGIN_URL_PARAM = "SSO-LOGIN-URL";
	
	public static final String CHARSET_UNICODE = "UTF-8";
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");	
	public static final String MAPPINGVALUE_SUCCESS = "success";
	public static final String MAPPINGVALUE_ERRORS = "error";
	public static final String MAPPINGVALUE_NOTLOGON = "notlogon";
	public static final String MAPPINGVALUE_ACCDENY =  "accdeny";
    public static final String BRANCH_KEY = "branch";
    public static final String APPS_KEY="apps";    
    public static final String DEPT_KEY = "dept";
    public static final String USER_STATUS_ACTIVE="Active";
    public static final String ROLE_KEY = "role";
	public static final String LOGONCREDENTIAL_KEY = "logoncredential";
    public static final String SLOGON_KEY = "slogon";
    public static final String FLOGON_KEY = "flogon";
    public static final String MENU_KEY = "menu";
    public static final String USER_KEY = "user";
    public static final String DORMANTREPORT_KEY = "dormantreport";
    public static final String PERM_KEY = "perm";
    public static final String APPSOFROLE_KEY="appsofrole";
    public static final String PERMSOFROLE_KEY="permsofrole";
    public static final String APPSID= "1";
    public static final String APPSNAME= "Admin";  
    public static final String VIEW_ONLY_KEY = "viewonly";
    public static final String VIEW_ONLY_ROLE_NAME = "View Only";
    public static final String ROLE_OPERATOR = CommonUtils.getPropValue("crsmsg", "role.operator");
    public static final String ROLE_OPERATOR_KEY = "operatorrole";
    
    public static final String ERROR_LOGONACTION =  "logon";
    public static final String ERROR_SAVEADDUSERACTION =  "SaveAddUser";
    public static final String ERROR_EDITUSERACTION = "EditUser";
    public static final String ERROR_SAVEEDITUSERACTION = "SaveEditUser";
    public static final String ERROR_USERSEARCHACTION = "UserSearch";
    
    public static final String ERRMSG_NOTLOGON = "errors.logon.notlogon";
    public static final String ERRMSG_SYSTEM = "errors.system";
    public static final String ERRMSG_USERALREADYEXIST = "errors.user.alreadyexist";
    public static final String ERRMSG_USERSAVESUCCESS = "errors.user.save.success";
    public static final String ERRMSG_ACTION_FAIL_DUPLICATE="errors.action.fail.duplicate";
    
    public static final String SEGMENTMGMT_PERM = "3";  
    public static final String BRANCHMGMT_PERM = "4";    
    public static final String SOURCEMGMT_PERM = "5";  
    public static final String CHANNELMGMT_PERM = "6";
    public static final String FTPSETUP_PERM = "7";
    public static final String DEPTMGMT_PERM = "2";
    public static final String USERMGMT_PERM = "9";  
    public static final String AREAMGMT_PERM = "10";    
    public static final String ROLEMGMT_PERM = "1";  
    public static final String TYPEMGMT_PERM = "14";  
    public static final String REGIONMGMT_PERM = "16";  
    public static final String CREDITISSUEMGMT_PERM = "12";
    public static final String GREGIONMGMT_PERM = "17";
    public static final String STATEMGMT_PERM = "18";
    public static final String HOLIDAYMGMT_PERM = "19";
    public static final String RPTAUDIT_PERM = "20";
    public static final String PRODUCTGROUPMGMT_PERM = "21";
	public static final String CICS_PERM = "22";
	public static final String RPTDORMANTUSER_PERM = "23";

    public static final String STRING_ZERO = "0"; 
    public static final String STRING_ALL = "All"; 
	
    // Audit Action String
    public static final String SYS_LOGOFF = "User Logoff";
    public static final String SYS_LOGON = "User Logon";
    public static final String BRANCH_ADD = "Branch Add";
    public static final String BRANCH_EDIT = "Branch Edit";
    public static final String DEPT_ADD = "Dept Add";
    public static final String DEPT_EDIT= "Dept Edit";
    public static final String SEGMENT_ADD = "Segment Add";
    public static final String SEGMENT_EDIT= "Segment Edit";
    public static final String SOURCE_ADD = "Segment Add";
    public static final String SOURCE_EDIT= "Segment Edit";
    public static final String GREGION_ADD = "Geo Region Add";
    public static final String GREGION_EDIT= "Geo Region Edit";
    public static final String REGION_ADD = "Region Add";
    public static final String REGION_EDIT= "Region Edit";
    public static final String HOLIDAY_ADD = "Holiday Add";
    public static final String HOLIDAY_EDIT= "Holiday Edit";
    public static final String STATE_ADD = "State Add";
    public static final String STATE_EDIT = "State Edit";
    public static final String ROLE_ADD = "Role Add";
    public static final String USER_ROLE_ADD = "User Role Add";
    public static final String USER_ROLE_EDIT = "User Role Edit";
    public static final String ROLE_EDIT = "Role Edit";
    public static final String USER_ADD = "User Add";
    public static final String USER_EDIT = "User Edit";
    public static final String AREA_ADD = "Area Add";
    public static final String AREA_EDIT = "Area Edit";
    public static final String PWD_CHG = "Password Change";
    public static final String CHANNEL_ADD = "Channel Add";
    public static final String CHANNEL_EDIT = "Channel Edit";
    public static final String CI_ADD = "Credit Add";
    public static final String CI_EDIT = "Credit Edit";
    public static final String TYPE_ADD = "TYPE Add";
    public static final String TYPE_EDIT = "Type Edit";
    public static final String GENERATE_REPORT = "Generate Report";
    
	public static final String CONTENTTYPE_CSV = "text/csv";
	public static final String STR_FILENAME = "filename=";
	public static final String HEADER = "Content-disposition";
	public static final String CSV_STRSTART = "\"";
	public static final String CSV_STREND = "\",";
	public static final String CSV_STREMPTY = "\"\",";
	public static final String CSVFILENAME_RPTDORMENTUSER = "ReportDormentUser.csv";
	    
	public static final String HEAD_BRANCHNAME = "Branch Code";
	public static final String HEAD_USERID = "User Id";
	public static final String HEAD_USERNAME = "User Name";
	public static final String HEAD_USERSTATUS = "User Status";
	public static final String HEAD_LASTLOGINDATE = "Last Login Date";
	public static final String FORM_FIELD_DEPTNAME = "Department Name";
	public static final String FORM_FIELD_ROLENAME = "Role Name";
	
    //others
    public static final String DATEFORMAT_DAYBYMONTHYEAR = "dd/MM/yyyy";
    public static final String DATEFORMAT_MONTHDAYYEAR = "MM/dd/yyyy";
    
    
	public static final String DATEFORMAT_YEARBYMONTHDAY = "yyMMdd";
	public static final String DATEFORMAT_HOURBYMINUTE = "HH:mm";
	public static final String DATEFORMAT_HOURBYMINUTESECOND = "hh:mm:ss";
	
	public static final String STATUS_ACTIVE_LABEL = "Active";
	public static final String STATUS_DISABLED_LABEL = "Disabled";
	public static final String STATUS_CLOSED_LABEL = "Closed";
	public static final String STATUS_RESIGN_LABEL = "Resign";	
	
	public static final String PARAM_USERID = "userid";
	
	public static final String STR_ALL = "All";
	public static final String STR_ACTIVE = "Active";
	public static final String STR_CLOSED = "Closed";
	public static final String STR_RESIGN = "Resign";
	public static final String STR_USERID = "User Id";
	public static final String STR_USERNAME = "User Name";
	public static final String STR_PERCENT = "%";
	public static final String STR_SLASH = "/";
	public static final String STR_BRANCHCODE = "Branch Code";
	public static final String STR_EMPTYSTRING = "";
	public static final String STR_LOAD = "Load";
	public static final String STR_SEARCH = "Search";
	public static final String STR_HTML = "HTML";
	public static final String STR_CSV = "CSV";
		
	public static final String STR_BRANCHNAMEVALUE = "BranchName";
	public static final String STR_BRANCHNAMELABEL = "Branch Name";
	public static final String STR_BRANCHCODEVALUE = "BranchCode";
	public static final String STR_BRANCHCODELABEL = "Branch Code";
	public static final String STR_FOUR = "4";
	public static final String STR_ACTION_SUCCESS = "S";
	
	public static final String QUERY_APPS = "from Apps where matchString = ?";
	
	public static final int DEFAULT_TIMEOUT = 600;
	
	public static final String WELCOME_PATH = "/common/Welcome.do";
	public static final String LOGON_PATH = "/common/Logon.do";
	public static final String LOGOFF_PATH = "common/Logoff.do";
	
	public static final String AUTO_LOGON = "autologon";

	public final static String REQUEST_USERID = "userId";
	public final static String REQUEST_PASSWD = "passwd";
	public final static String USER_STATUS = "ACTIVE";
	
	public static final String QUEUE_MK_TXN = "MK.TXN";
	public static final String QUEUE_MK_PRO = "MK.PRO";
	public static final String QUEUE_ION_TXN = "ION.TXN";
	public static final String QUEUE_IONPRO = "ION.PRO";
	public static final String QUEUE_NOTIFICATION = "NOTIFICATION";
	public static final String QUEUE_EMAIL = "EMAIL";
	
	public static final String PWD_HISTORY = "|";

	public final static String FORBIDDEN_ERROR = "<?xml version=\"1.0\"?>\n" +
    "<response>\n" +
    "<message>\n" +
    "<code>404</code>\n" +
    "<description>FORBIDDEN</description>\n" +
    "</message>\n" +
    "</response>\n";

public final static String AUTHENTICATION_ERROR = "<?xml version=\"1.0\"?>\n" + 
		  "<response>\n" +
		  "<message>\n" +
		  "<code>401</code>\n" +
		  "<description>UNAUTHORIZED</description>\n" +
		  "</message>\n" +
		  "</response>\n";

public final static String LOGIN_SUCCESS = "<?xml version=\"1.0\"?>\n" + 
"<response>\n" +
"<message>\n" +
"<code>000</code>\n" +
"<description>Login Success</description>\n" +
"</message>\n" +
"</response>\n";

public final static String VALID_RESPONSE_HEADER = "<?xml version=\"1.0\"?>\n" + 
	       "<wsauth>\n";

public final static String VALID_RESPONSE_FOOTER = "</wsauth>\n";

public final static String VALID_RESPONSE_CICS = "<cics>\n" +
		 "<uid></uid>\n" + 
		 "<password></password>\n" + 
		 "</cics>\n";
}
