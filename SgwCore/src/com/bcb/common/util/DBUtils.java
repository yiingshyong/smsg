package com.bcb.common.util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

public class DBUtils {
	public static List DBHQLCommand(String dbCommand){
		return DBHQLCommand(dbCommand, new Object[]{});
	}
	
	public static List DBHQLCommand(String dbCommand, Session hibernate_session){
		return DBHQLCommand(dbCommand, null, hibernate_session);
	}
	
	public static List DBHQLCommand(String dbCommand, Object[] cmdParameters){
		List resultList=new ArrayList();
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		resultList=DBHQLCommand(dbCommand, cmdParameters, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return resultList;
		}else{
			return new ArrayList();
		}
	}
	
	public static List DBHQLCommand(String dbCommand, Object[] cmdParameters, Session hibernate_session){
		List resultList=new ArrayList();
		
		if(hibernate_session!=null){
			try{
				hibernate_session.beginTransaction();
				if ( ! hibernate_session.isConnected()){
					hibernate_session.reconnect(hibernate_session.connection());
				}
				
				Query dbQuery=hibernate_session.createQuery(dbCommand);
				for(int i=0;cmdParameters!=null&&i<cmdParameters.length;i++){// Set parameters
					dbQuery.setParameter(i, cmdParameters[i]);
				}

				resultList=dbQuery.list();
				//hibernate_session.getTransaction().commit();
			}catch(Exception ex){
				/*if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}*/
				ex.printStackTrace();
			}/*finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}*/
		}
		
		return resultList;
	}
	
	public static List DBSQLCommand(String dbCommand){
		return DBSQLCommand(dbCommand, new String[]{}, new NullableType[]{}, new Object[]{});
	}
	
	public static List DBSQLCommand(String dbCommand, Session hibernate_session){
		return DBSQLCommand(dbCommand, null, null, null, hibernate_session);
	}
	
	public static List DBSQLCommand(String dbCommand, Object[] cmdParameters){
		return DBSQLCommand(dbCommand, new String[]{}, new NullableType[]{}, cmdParameters);
	}
	
	public static List DBSQLCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters){
		List resultList=new ArrayList();
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		resultList=DBSQLCommand(dbCommand, cmdParamName, cmdParamType, cmdParameters, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return resultList;
		}else{
			return new ArrayList();
		}
	}
	
	public static List DBSQLCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters, Session hibernate_session){
		List resultList=new ArrayList();
		
		if(hibernate_session!=null){
			try{
				hibernate_session.beginTransaction();
				if ( ! hibernate_session.isConnected()){
					hibernate_session.reconnect(hibernate_session.connection());
				}
				
				SQLQuery dbQuery=hibernate_session.createSQLQuery(dbCommand);
				for(int i=0;cmdParameters!=null&&i<cmdParameters.length;i++){// Set parameters
					dbQuery.setParameter(i, cmdParameters[i]);
				}

				setMapping(dbQuery, cmdParamName, cmdParamType);
				resultList=dbQuery.list();
				//hibernate_session.getTransaction().commit();
			}catch(Exception ex){
				/*if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}*/
				ex.printStackTrace();
			}/*finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}*/
		}
		
		return resultList;
	}
	
	public static ScrollableResults DBSQLScrollCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters, Session hibernate_session){
		ScrollableResults resultList = null; 
		
		if(hibernate_session!=null){
			try{
				hibernate_session.beginTransaction();
				if ( ! hibernate_session.isConnected()){
					hibernate_session.reconnect(hibernate_session.connection());
				}
				
				SQLQuery dbQuery=hibernate_session.createSQLQuery(dbCommand);
				for(int i=0;cmdParameters!=null&&i<cmdParameters.length;i++){// Set parameters
					dbQuery.setParameter(i, cmdParameters[i]);
				}

				setMapping(dbQuery, cmdParamName, cmdParamType);
				
				resultList=dbQuery.setReadOnly(true).setCacheable(false).setFetchSize(Integer.MIN_VALUE).scroll(ScrollMode.FORWARD_ONLY);
				//hibernate_session.getTransaction().commit();
			}catch(Exception ex){
				/*if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}*/
				ex.printStackTrace();
			}/*finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}*/
		}
		
		return resultList;
	}
	
	public static SQLQuery setMapping(SQLQuery q, String[] mappingName, NullableType[] cmdParamType){
		for(int i=0;mappingName!=null&&i<mappingName.length; i++){
			q.addScalar(mappingName[i], cmdParamType[i]);
		}
		return q;
	}
	
	public static Object get(Class theModalClass, Object primaryKey){
		Object obj=null;
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		obj=get(theModalClass, primaryKey, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return obj;
		}else{
			return null;
		}
	}
	
	public static Object get(Class theModalClass, Object primaryKey, Session hibernate_session){
		Object resultObject=null;
		
		try{
			hibernate_session.beginTransaction();
			
			if ( ! hibernate_session.isConnected()){
				hibernate_session.reconnect(hibernate_session.connection());
			}
			
			if (primaryKey instanceof String) {
				resultObject= hibernate_session.get(theModalClass, primaryKey.toString());
			}else if(primaryKey instanceof Integer) {
				resultObject= hibernate_session.get(theModalClass, Integer.valueOf(primaryKey.toString()));
			}
			//hibernate_session.getTransaction().commit();
		}catch (Exception ex){
			ex.printStackTrace();
			/*if (hibernate_session.getTransaction()!=null) 
				hibernate_session.getTransaction().rollback();*/
		}/*finally{
			if(hibernate_session!=null&&hibernate_session.isOpen())
				hibernate_session.close();
		}*/
		
		return resultObject;
	}
	
	public static boolean saveOrUpdate(Object modalObject){
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		if(saveOrUpdate(modalObject, hibernate_session)){
			return closeTransaction(hibernate_session);
		}else{
			return false;
		}
	}
	
	public static boolean saveOrUpdate(Object modalObject, Session hibernate_session){
		if(hibernate_session!=null){
			try{
				hibernate_session.beginTransaction();
				if ( ! hibernate_session.isConnected()){
					hibernate_session.reconnect(hibernate_session.connection());
				}
				hibernate_session.saveOrUpdate(modalObject);
				//hibernate_session.getTransaction().commit();
				return true;
			}catch(Exception ex){
				/*if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}*/
				ex.printStackTrace();
				return false;
			}/*finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}*/
		}else{
			return false;
		}
	}
	
	public static boolean delete(Object modalObject){
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		if(delete(modalObject, hibernate_session)){
			return closeTransaction(hibernate_session);
		}else{
			return false;
		}
	}
	
	public static boolean delete(Object modalObject, Session hibernate_session){
		if(hibernate_session!=null){
			try{
				hibernate_session.beginTransaction();
				if ( ! hibernate_session.isConnected()){
					hibernate_session.reconnect(hibernate_session.connection());
				}
				hibernate_session.delete(modalObject);
				//hibernate_session.getTransaction().commit();
				return true;
			}catch(Exception ex){
				/*if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}*/
				ex.printStackTrace();
				return false;
			}/*finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}*/
		}else{
			return false;
		}
	}
	
	public static boolean closeTransaction(Session hibernate_session){
		if(hibernate_session!=null&&hibernate_session.isOpen()&&hibernate_session.getTransaction().isActive()){
			try{
				hibernate_session.getTransaction().commit();				
				return true;
			}catch(Exception ex){
				if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}
				ex.printStackTrace();
				return false;
			}finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}
		}else{
			return true;
		}
	}
	
	public static boolean rollbackTransaction(Session hibernate_session){
		if(hibernate_session!=null&&hibernate_session.isOpen()&&hibernate_session.getTransaction().isActive()){
			try{
				hibernate_session.getTransaction().rollback();
				return true;
			}catch(Exception ex){
				if(hibernate_session!=null){
					hibernate_session.getTransaction().rollback();
				}
				ex.printStackTrace();
				return false;
			}finally {
				if(hibernate_session!=null&&hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}
		}else{
			return true;
		}
	}
	
	public static String generateQuery(ArrayList<String> mainQuery, ArrayList conditionQuery, ArrayList<String> orderQuery){
		String query=Constants.STR_EMPTYSTRING;
		
		// Main Query
		for(int i=0;i<mainQuery.size();i++){
			query+=mainQuery.get(i);
		}
		
		// Condition Query
		for(int i=0;i<conditionQuery.size();i++){
			if(i==0){
				query+=" where ";
			}else{
				query+=" and ";
			}
			query+=conditionQuery.get(i);
		}
		
		// Order Query
		for(int i=0;i<orderQuery.size();i++){
			if(i==0){
				query+=" order by ";
			}else{
				query+=", ";
			}
			query+=orderQuery.get(i);
		}
		return query;
	}
}
