package com.bcb.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	private static DateUtil instance;
	private static SimpleDateFormat _dateFormatter;
	private static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	/**
     * Get instance of DateUtil.
     * 
     * @return instance of DateUtil
     */
	private DateUtil() {
			
		 _dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
	}
	
    public static synchronized DateUtil getInstance() {
        if (instance == null) {
            instance = new DateUtil();
        }

        return instance;
    }
    
    public String stringByDayMonthYear(Date date){
		
    	if (date == null){
    		return "";
    	}
		
		return _dateFormatter.format(date);
	}
}
