package com.bcb.common.util;

import javax.imageio.spi.ServiceRegistry;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate.encryptor.HibernatePBEEncryptorRegistry;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;

	static {
		try {
			PBEStringEncryptor stringEncryptor = HibernatePBEEncryptorRegistry.getInstance().getPBEStringEncryptor("hibernate.configuration.encryptor"); 
			Configuration configuration= new Configuration().configure();
				StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
				strongEncryptor.setAlgorithm("PBEWithMD5AndDES"); 
				strongEncryptor.setPassword("mysql");
				HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance(); 
				registry.registerPBEStringEncryptor("hibernate.configuration.encryptor", strongEncryptor); 

//				configuration.setProperty("hibernate.connection.password", strongEncryptor.decrypt(configuration.getProperty("hibernate.connection.password")));
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = configuration.buildSessionFactory();

		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			ex.printStackTrace();
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		if(!sessionFactory.getCurrentSession().isOpen()){
			sessionFactory.openSession();
		}
		
		if(!sessionFactory.getCurrentSession().getTransaction().isActive()){
			sessionFactory.getCurrentSession().beginTransaction();
		}
		
		if(!sessionFactory.getCurrentSession().isConnected()){
			sessionFactory.getCurrentSession().reconnect(sessionFactory.getCurrentSession().connection());
		}		
		return sessionFactory;
	}
	
	public static SessionFactory getOnlySessionFactory() {
		return sessionFactory;
	}
	public static SessionFactory getSessionFactoryServer(String server) {
		SessionFactory sessionFactoryServer;
		StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor(); 
		strongEncryptor.setAlgorithm("PBEWithMD5AndDES"); 
		strongEncryptor.setPassword("mysql"); 
		Configuration configurationServer= new Configuration().configure();
//		configurationServer.setProperty("hibernate.connection.password", strongEncryptor.decrypt(configurationServer.getProperty("hibernate.connection.password")));
		configurationServer.setProperty("hibernate.connection.url", server);
		sessionFactoryServer = configurationServer.buildSessionFactory();

		if(!sessionFactoryServer.getCurrentSession().isOpen()){
			sessionFactoryServer.openSession();
		}
		
		if(!sessionFactoryServer.getCurrentSession().getTransaction().isActive()){
			sessionFactoryServer.getCurrentSession().beginTransaction();
		}
		
		if(!sessionFactoryServer.getCurrentSession().isConnected()){
			sessionFactoryServer.getCurrentSession().reconnect(sessionFactoryServer.getCurrentSession().connection());
		}
		
		return sessionFactoryServer;
	}
}