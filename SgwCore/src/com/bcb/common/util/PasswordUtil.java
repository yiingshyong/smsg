package com.bcb.common.util;

import com.bcb.crsmsg.util.SecurityManager;


public class PasswordUtil {
	public static String checkStrength(String pass){
		boolean isNum = false;
		boolean isChar = false;
		boolean isSymbol = false;
		
		String result = null;
		
		
		//Check if the String contains number as well as char
		for(int i = 0; i < pass.length(); i++) {
			if(!isNum)
				if(pass.substring(i, i + 1).matches("[0-9]*"))
					isNum = true;
			
			if(!isChar)
				if(pass.substring(i, i + 1).matches("[a-zA-Z]*"))
					isChar = true;
			
			if(!isSymbol)
//				if(pass.substring(i, i + 1).matches("[@#$%]*"))
				if(pass.substring(i, i + 1).matches("[^\\w\\s]"))					
					isSymbol = true;
			
			if(isNum && isChar && isSymbol)
				break;
		}	
	

		if(isNum && isChar && isSymbol) { 
			result = Constants.MAPPINGVALUE_SUCCESS;
		} else if(!isNum) {
			result = "errors.chgpwd.passwordnonumber";
		} else if(!isChar) {
			result = "errors.chgpwd.passwordnoalphabet";
		} else if(!isSymbol) {
			result = "errors.chgpwd.passwordnosymbol";
		} 
		
		return result;
	}
	
	public static String checkHistory(String pass){
		boolean isNum = false;
		boolean isChar = false;
		boolean isSymbol = false;
		
		String result = null;
		
		
		//Check if the String contains number as well as char
		for(int i = 0; i < pass.length(); i++) {
			if(!isNum)
				if(pass.substring(i, i + 1).matches("[0-9]*"))
					isNum = true;
			
			if(!isChar)
				if(pass.substring(i, i + 1).matches("[a-zA-Z]*"))
					isChar = true;
			
			if(!isSymbol)
				if(pass.substring(i, i + 1).matches("[@#$%&!^*()]*"))
					isSymbol = true;
			
			if(isNum && isChar && isSymbol)
				break;
		}	
	

		if(isNum && isChar) { 
			result = Constants.MAPPINGVALUE_SUCCESS;
		} else if(!isNum) {
			result = "errors.chgpwd.passwordnonumber";
		} else if(!isChar) {
			result = "errors.chgpwd.passwordnoalphabet";
		} else if(!isSymbol) {
			result = "errors.chgpwd.passwordnosymbol";
		} 
		
		return result;
	}

}
