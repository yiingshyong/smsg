package com.bcb.common.util;

import java.util.Enumeration;
import java.util.Properties;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.util.ObjectUtils;

public class SecureDataSource extends PropertyPlaceholderConfigurer {
	@Override
	protected void convertProperties(Properties props) {
		super.convertProperties(props);
		// read the name of the properties file in key
		Enumeration<?> propertyNames = props.propertyNames();
		// Traverse key name
		while (propertyNames.hasMoreElements()) {
			String propertyName = (String) propertyNames.nextElement();
			// configuration value
			String propertyValue = props.getProperty(propertyName);
			// value after conversion
			String convertedValue = convertPropertyValue(propertyValue);
			String decodedValue = "";
			// value named password to decrypt

			if (ObjectUtils.nullSafeEquals(propertyValue, convertedValue)) {
				// Note: This is not to modify the configuration file, but to
				// change the configuration file is read into the key-value
				// pairs
				System.out.println("PropertyName and PropertyValue : "+propertyName+" :  "+propertyValue);
				if ("hibernate.connection.password".equals(propertyName)) {

					decodedValue = getDecrypted(convertedValue);
					props.setProperty(propertyName, decodedValue);
				}
			}
		}
	}
	private String getDecrypted(String decode) {
		String sPassword = "";

		String encryptionKey = "mysql";
		String algorithm = "PBEWithMD5AndDES";

		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();

		if (algorithm != "") {
			encryptor.setAlgorithm(algorithm);
		} else {
			System.out.println("----------------------------------------------------------------------------");
			System.out
					.println("WARNING: No encryption/decryption algorithm define, default to JASPYT preset algorithm!");
			// log4jUtil.info("WARNING: No encryption/decryption algorithm
			// define, default to JASPYT preset algorithm!");
		}

		if (encryptionKey != "") {
			encryptor.setPassword(encryptionKey);
		} else {
			System.out.println("--------------------------------------------------------");
			System.out.println("ERROR: No encryption/decryption key define, fail to process! Abort!");
			// log4jUtil.info("ERROR: No encryption/decryption key define, fail
			// to process! Abort!");
			return "";
		}
		return encryptor.decrypt(decode);

	}
}