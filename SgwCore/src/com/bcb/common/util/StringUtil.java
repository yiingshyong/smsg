package com.bcb.common.util;

public class StringUtil {
	private static StringUtil instance;
	private static final String STR_EMPTY = " ";
	private static final String STR_ZERO = "0";
	public static synchronized StringUtil getInstance() {
        if (instance == null) {
            instance = new StringUtil();
        }

        return instance;
    }
	
	public String getEmptyString(int noString){
		StringBuffer bf = new StringBuffer(STR_EMPTY);
		for (int i=0; i < noString; i++){
			bf.append(STR_EMPTY);
		}
		return bf.toString();
	}
	
	public String getZeroString(int noString){
		StringBuffer bf = new StringBuffer(STR_EMPTY);
		for (int i=0; i < noString; i++){
			bf.append(STR_ZERO);
		}
		return bf.toString();
	}
	
	public String getPadEmptyString(int noString, String value){
		
		StringBuffer bf = new StringBuffer("");
		bf.append(value);
		noString = noString - value.length();
		
		for (int i=0; i < noString; i++){
			bf.append(STR_EMPTY);
		
		}
		
		return bf.toString();
	}
	
	public String getPadFrontEmptyString(int noString, String value){
		
		StringBuffer bf = new StringBuffer("");
		
		noString = noString - value.length();
		
		for (int i=0; i < noString; i++){
			bf.append(STR_EMPTY);
		}
		bf.append(value);
		return bf.toString();
	}
}
