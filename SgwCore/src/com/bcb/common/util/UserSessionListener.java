package com.bcb.common.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.bcb.common.modal.LogonCredential;

public class UserSessionListener implements HttpSessionListener {

	private static Hashtable activeSessionsMap;
	private static Map<String, String> SESSION_ID_MAP = new HashMap<String, String>();

	public void sessionCreated(HttpSessionEvent se) {
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		LogonCredential aLogonCredential = null;
		
		if(activeSessionsMap != null) {
			if(se.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY) != null) {
				aLogonCredential = (LogonCredential) se.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);

				activeSessionsMap.remove(aLogonCredential.getUserId());
			}
		}
		
		SESSION_ID_MAP.remove(se.getSession().getId());
	}
	
	public static boolean isSessionValid(String sessionId){
		if(sessionId != null){
			return SESSION_ID_MAP.containsKey(sessionId);			
		}
		return false;
	}
	
	public static void storeSessionId(String sessionId){
		if(sessionId != null && !isSessionValid(sessionId)){
			SESSION_ID_MAP.put(sessionId, "");
		}
	}
	
	public static void storeSessionId(String userId, String sessionId){
		if(sessionId != null && !isSessionValid(sessionId)){
			SESSION_ID_MAP.put(sessionId, "");
		}
		if(activeSessionsMap == null){
			activeSessionsMap = new Hashtable();
		}		
		activeSessionsMap.put(userId, userId);
	}
	
	public static void removeSessionId(String sessionId){
		if(sessionId != null){
			SESSION_ID_MAP.remove(sessionId);			
		}
	}
	
	public static boolean checkLoginSession(String userId) {
		boolean exists = false;
		
		if(activeSessionsMap == null)
			activeSessionsMap = new Hashtable();
		
		if(activeSessionsMap.containsKey(userId)) {
			exists = true;
		}
		
		activeSessionsMap.put(userId, userId);
		
		return exists;
	}
	
	public static void logoffSession(String userId) {
		if(activeSessionsMap == null)
			activeSessionsMap = new Hashtable();
		
		if(activeSessionsMap.containsKey(userId)) {
			activeSessionsMap.remove(userId);
		}
		
	}
	
	//This portion will logoff previous login session
//	public static void newLoginSession(HttpSession currentUserSession) {
//		LogonCredential aLogonCredential = null;
//		HttpSession session = null;
//		
//		if(currentUserSession.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY) != null) {
//			aLogonCredential = (LogonCredential) currentUserSession.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
//			
//			if(activeSessionsMap == null)
//				activeSessionsMap = new Hashtable();
//			
//			//Invalidate previous session
//			if(activeSessionsMap.containsKey(aLogonCredential.getUserId())) {
//				session = (HttpSession) activeSessionsMap.get(aLogonCredential.getUserId());
//				
//				try {
//					session.invalidate();
//				} catch (IllegalStateException e) {
//					System.out.println("Session already invalidated");
//				}
//			}
//			
//			activeSessionsMap.put(aLogonCredential.getUserId(), currentUserSession);
//		}
//	}
}
