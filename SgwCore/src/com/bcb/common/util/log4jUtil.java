package com.bcb.common.util;

import org.apache.log4j.Logger;


public class log4jUtil {
	 private static Logger logger;
	 static {
	        try {
	            // Create the SessionFactory from hibernate.cfg.xml
	        	logger = Logger.getLogger("com.bcb.crms");

	        } catch (Throwable ex) {
	            // Make sure you log the exception, as it might be swallowed
	            System.err.println("Initial Logger creation failed." + ex);
	            ex.printStackTrace();
	            throw new ExceptionInInitializerError(ex);
	        }
	    }

	    public static void info(String msg) {
	    	logger.info(msg);
	        return;
	    }
	    public static void warn(String msg) {
	    	logger.warn(msg);
	        return;
	    }
	    public static void debug(String msg) {
	    	logger.debug(msg);
	        return;
	    }
	    public static void error(String msg, Throwable e) {
	    	logger.error(msg, e);
	        return;
	    }
	    public static void error(String msg) {
	    	logger.error(msg);
	        return;
	    }
	    public static void fatal(String msg) {
	    	logger.fatal(msg);
	        return;
	    }

}
