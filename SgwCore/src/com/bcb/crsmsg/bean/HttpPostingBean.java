package com.bcb.crsmsg.bean;

public class HttpPostingBean {

	private String mobileNo;
	private String message;
	private String requestMode;
	private String priority;
	private String userId;
	private String channelId;
	private String scheduledTimeStr;
	private String clientTimestampStr;
	private String clientIp;
	private static final String DELIM = ":"; 
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRequestMode() {
		return requestMode;
	}
	public void setRequestMode(String requestMode) {
		this.requestMode = requestMode;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getScheduledTimeStr() {
		return scheduledTimeStr;
	}
	public void setScheduledTimeStr(String scheduledTimeStr) {
		this.scheduledTimeStr = scheduledTimeStr;
	}
	public String getClientTimestampStr() {
		return clientTimestampStr;
	}
	public void setClientTimestampStr(String cliemtTimestampStr) {
		this.clientTimestampStr = cliemtTimestampStr;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}	
}
