package com.bcb.crsmsg.bean;

import java.util.List;

public class OutgoingBillingReportVO {

	private String dept;
	private String userId;
	private String channelId;
	private List<String> telcoSmsRateCosts;
	private List<String> telcoSmsRateCount;
	private String totalCost;
	private String totalCount;
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public List<String> getTelcoSmsRateCosts() {
		return telcoSmsRateCosts;
	}
	public void setTelcoSmsRateCosts(List<String> telcoSmsRateCosts) {
		this.telcoSmsRateCosts = telcoSmsRateCosts;
	}
	public List<String> getTelcoSmsRateCount() {
		return telcoSmsRateCount;
	}
	public void setTelcoSmsRateCount(List<String> telcoSmsRateCount) {
		this.telcoSmsRateCount = telcoSmsRateCount;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
