package com.bcb.crsmsg.bean;

import java.util.Date;

public class SIBSBatchOutputFileBean {

	private String mobileNo;
	private Date smsStatusDateTime;
	private Date smsCreatedDateTime;
	private String message;
	private String sentBy;
	private String telco;
	private String status;
	private String remark;
	private String refCode;
	
	
	public SIBSBatchOutputFileBean() {
		super();
	}

	public SIBSBatchOutputFileBean(String mobileNo, Date smsStatusDateTime,
			Date smsCreatedDateTime, String message, String sentBy,
			String telco, String status, String remark, String refCode) {
		super();
		this.mobileNo = mobileNo;
		this.smsStatusDateTime = smsStatusDateTime;
		this.smsCreatedDateTime = smsCreatedDateTime;
		this.message = message;
		this.sentBy = sentBy;
		this.telco = telco;
		this.status = status;
		this.remark = remark;
		this.refCode = refCode;
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSentBy() {
		return sentBy;
	}
	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}
	public String getTelco() {
		return telco;
	}
	public void setTelco(String telco) {
		this.telco = telco;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRefCode() {
		return refCode;
	}
	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public Date getSmsStatusDateTime() {
		return smsStatusDateTime;
	}

	public void setSmsStatusDateTime(Date smsStatusDateTime) {
		this.smsStatusDateTime = smsStatusDateTime;
	}

	public Date getSmsCreatedDateTime() {
		return smsCreatedDateTime;
	}

	public void setSmsCreatedDateTime(Date smsCreatedDateTime) {
		this.smsCreatedDateTime = smsCreatedDateTime;
	}
	
}
