package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.ADSSynch;

public class ADSSynchDao extends HibernateDaoSupport implements IADSSynchDao{
	public List<Object> findRecord(String dbCommand, Object[] cmdParameters) {
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public List<ADSSynch> findAll(){
		return getHibernateTemplate().find("from ADSSynch");
	}
	
	public boolean save(ADSSynch aSynch){
		try{
			getHibernateTemplate().save(aSynch);
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public boolean update(ADSSynch aSynch){
		try{
			getHibernateTemplate().update(aSynch);
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
}
