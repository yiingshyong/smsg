package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.util.AppPropertiesConfig;

public class AutoActionDao extends HibernateDaoSupport implements IAutoActionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<AutoAction> findNonDefaultActions() {
		List<AutoAction> results = getHibernateTemplate().find("from AutoAction where defaultAction != 'Y' and keyword = 0");
		if(results != null && !results.isEmpty()){
			return results;
		}
		return new ArrayList<AutoAction>();
	}
	
	@SuppressWarnings("unchecked")
	public AutoAction findNonDefaultActions(String className) {
		List<AutoAction> results = getHibernateTemplate().find("from AutoAction where defaultAction != 'Y' and keyword = 0 and actionClassName=? ", new Object[]{className});
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	public AutoAction getNonDefaultAutoActionByKeyword(Integer keywordId) {
		List<AutoAction> results = getHibernateTemplate().find("select b from AutoAction a, AutoAction b where a.keyword = ? and a.actionClassName=b.actionClassName and b.keyword='0' and b.defaultAction != 'Y'", new Object[]{keywordId});
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}
	
	public AutoAction getKeywordActionMethod(Integer keywordId) {
		List<AutoAction> results = getHibernateTemplate().find("select a from AutoAction a, AutoAction b where a.keyword = ? and a.actionClassName=b.actionClassName and b.keyword='0' and b.defaultAction != 'Y'", new Object[]{keywordId});
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isCreditCardActivationKeyword(Integer keywordId){
		List<AutoAction> results = getHibernateTemplate().find(" from AutoAction where keyword=? and actionClassName=?", new Object[]{keywordId, AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.CC_ACT_HTTP_ACTION)});
		if(results != null && !results.isEmpty()){
			return true;
		}
		return false;
	}
	
	public AutoAction findOCMActions(String paraKey) {
		List<AutoAction> results = getHibernateTemplate().find("from AutoAction where defaultAction != 'Y' and parameterKey = ? ", new Object[]{ paraKey});
		if(results != null && !results.isEmpty()){
			return results.get(0);
		}
		return null;
	}

}
