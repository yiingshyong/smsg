package com.bcb.crsmsg.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.BulkSmsFile;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class BulkSMSFileDao extends HibernateDaoSupport implements IBulkSMSFileDao {

	static Log log = LogManager.getLog();

	/*
	 * This method is to get bulk file save inside db to write out and allow qfp to run
	 *	 
	 **/

	@SuppressWarnings("unchecked")
	public List<BulkSmsFile> findMessages(String status) {
//		return getHibernateTemplate().find("from SmsQueue where (smsStatus=? or (smsStatus=? and smsScheduled=? and smsScheduledTime<=?)) order by priority,id", 
//				new Object[]{Constants.SMS_STATUS_4, Constants.SMS_STATUS_6, Constants.STATUS_YES, new Date()});		
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		StringBuffer queryBuffer = new StringBuffer();
//		queryBuffer.append("select new SmsQueue(q, t) from SmsQueue q, Department d, TblRef t ");
//		queryBuffer.append("where (q.smsStatus=? or (q.smsStatus=? and q.smsScheduled=? and q.smsScheduledTime<=?)) ");
//		queryBuffer.append("and q.deptId = d.id ");
//		queryBuffer.append("and t.tblRefId = d.smsType ");
//		queryBuffer.append("order by t.value ");
//		queryBuffer.append(prioritize? ",q.priority,q.id" : ",q.id");

		queryBuffer.append("select q from BulkSmsFile q ");
		queryBuffer.append("where q.bulkFileStatus=? ");

		try{
			Query query = session.createQuery(queryBuffer.toString());
			int count = 0;
			query.setParameter(count++, status);;
			List<BulkSmsFile> results = query.list();
			return results;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			if(session.isOpen()) session.close();
		}
	}

	public void updateStatus(List<BulkSmsFile> bulkSmsFile, String status) {
		
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		Transaction txn = session.getTransaction();
		txn.begin();
		try{
				
			String aaa="0";
			for(int f=0;f< bulkSmsFile.size();f++){
				aaa = aaa + "," + bulkSmsFile.get(f).getId();
			}
			Query query = session.createQuery("update BulkSmsFile set bulkFileStatus=? where id in ("+aaa+") ");
			String random = Double.toString(Math.random());
			query.setParameter(0, status);
			query.executeUpdate();
			txn.commit();
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			
			session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
			
		}catch(Exception e){
			e.printStackTrace();
			txn.rollback();
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());			
			if(session.isOpen()) session.close();
		}
	}
	public void updateSetupStatus(List<BulkSmsFile> bulkSmsFile, String status) {
		
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		Transaction txn = session.getTransaction();
		txn.begin();
		try{
				
			String aaa="0";
			for(int f=0;f< bulkSmsFile.size();f++){
				aaa = aaa + "," + bulkSmsFile.get(f).getBulkId();
			}
			Query query = session.createQuery("update BulkSmsSetup set setupStatus=? where id in ("+aaa+") ");
			String random = Double.toString(Math.random());
			query.setParameter(0, status);
			query.executeUpdate();
			txn.commit();
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			
			session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
			
		}catch(Exception e){
			e.printStackTrace();
			txn.rollback();
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());			
			if(session.isOpen()) session.close();
		}
	}
}
