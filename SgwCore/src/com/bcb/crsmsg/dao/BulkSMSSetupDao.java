package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.util.Constants;

public class BulkSMSSetupDao 
extends HibernateDaoSupport
implements IBatchSetupDao {
	
	public List<Object> findSetup(int fileConversionId) {
		final String queryString = "from BulkSmsSetup where fileConversion=? and setupStatus=?";
		return getHibernateTemplate().find(queryString, new Object[]{fileConversionId, Constants.STATUS_ACTIVE});
	}
	
	public List<Object> findAll() {
		return getHibernateTemplate().find("from BulkSMSSetup ");
	}
	
	public List<Object> findSetup(String dbCommand, Object[] cmdParameters){
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public List<Object> findAllMessageByPriority(int prioprity, int maxResult) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void deleteById(int id) {
		BulkSmsSetup obj = findById(id);
		if (obj != null) {
			getHibernateTemplate().delete(obj);
			getHibernateTemplate().flush();
		}
	}
	
	public void deleteByObject(Object bulkSMSSetupModal) {
		// TODO Auto-generated method stub
		
	}
	
	public BulkSmsSetup getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public BulkSmsSetup findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void insert(Object object) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public List<FtpSetup> getMessagesOrderByPriority(int maxResult) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void saveAll(List<Object> bulkSMSSetupList) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isExist(Object bulkSMSSetupModal) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void updateStatus(Object bulkSMSSetupModal, String status) {
		// TODO Auto-generated method stub
		BulkSmsSetup theSetup=(BulkSmsSetup)bulkSMSSetupModal;
		theSetup.setSetupStatus(status);
		getHibernateTemplate().saveOrUpdate(theSetup);
	}
	
	public void update(Object bulkSetupModal) {
		getHibernateTemplate().update(bulkSetupModal);
	}
	
}
