package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.util.SecurityManager;

public class CreditCardActivationDao extends HibernateDaoSupport implements ICreditCardActivationDao {

/*	@SuppressWarnings("unchecked")
	@Override
	public CreditCardActivation find(String mobileNo, String ccNo, String custId) {
		ccNo = SecurityManager.getInstance().encrypt(ccNo);
		if(mobileNo.startsWith("60")){
			mobileNo = mobileNo.substring(1);
		}
		List<CreditCardActivation> results = getHibernateTemplate().find(" from CreditCardActivation where lower(cardNo) = ? and mobileNo = ? and lower(customerId) = ? order by createdDatetime desc");
		if(results == null || results.isEmpty()){
			return null;
		}
		return results.get(0);
	}
*/
	@SuppressWarnings("unchecked")
	@Override
	@Deprecated
	public List<CreditCardActivation> findUniqueRecordByMobileNoSuffix(String mobileNo) {
		List<Integer> ids = getHibernateTemplate().find("select max(id) from CreditCardActivation where mobileNo like ? group by mobileNo, customerId, cardNo", new Object[]{"%" + mobileNo});
		if(ids == null || ids.isEmpty()) return null;
		List<CreditCardActivation> results = getHibernateTemplate().findByNamedParam("from CreditCardActivation where id in (:ids) ", "ids", ids);
		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CreditCardActivation find(String ccNo) {
		ccNo = SecurityManager.getInstance().encrypt(ccNo);
		List<CreditCardActivation> results = getHibernateTemplate().find(" from CreditCardActivation where cardNo = ? order by createdDatetime desc", new Object[]{ccNo});
		if(results == null || results.isEmpty()){
			return null;
		}
		return results.get(0);
	}

	public void deleteByRptId(int id) {
		getHibernateTemplate().bulkUpdate("delete from CreditCardActivation where ftpReportId = ?", new Object[]{id});
	}	

	@SuppressWarnings("unchecked")
	public List<Object> findByRptId(int reportId) {
		final String queryString = "from CreditCardActivation where ftpReportId=? ";
		return getHibernateTemplate().find(queryString, new Object[]{reportId});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CreditCardActivation> findByMobileNo(String mobileNo) {
		return getHibernateTemplate().find("from CreditCardActivation where mobileNo = ? order by id", new Object[]{mobileNo});
	}
	
	public List<CreditCardActivation> findByRefNo(String custId) {
		return getHibernateTemplate().find("from CreditCardActivation where customerId = ?", new Object[]{custId});
	}
	
	public void update(CreditCardActivation obj) {
		getHibernateTemplate().update(obj);
	}

}
