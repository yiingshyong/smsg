package com.bcb.crsmsg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.util.Constants;

public class CustomerDao extends HibernateDaoSupport implements ICustomerDao {

	Log log = LogManager.getLog();

	public List<Customer> findByCustRefNo(final String custRefNo) {
		return getHibernateTemplate().find("from Customer where custRefNo=?", new Object[]{custRefNo});
	}
	
	public List<Customer> findUnsubByTransferStatus(final String status) {
		return getHibernateTemplate().find("from Customer where custRefNo is not null and custRefNo!='' and smsStatus=? and (transferStatus is null or transferStatus=?)", new Object[]{Constants.STATUS_UNSUBSCRIPTION, status});
	}
	
	public void insert(Customer obj) throws Exception {
		getHibernateTemplate().save(obj);
	}
	
	public void update(Customer obj) {
		getHibernateTemplate().update(obj);
	}
	
	public void update(List<Customer> objList) {
		for(int i=0;i<objList.size();i++){
			getHibernateTemplate().update(objList.get(i));
		}
	}
}
