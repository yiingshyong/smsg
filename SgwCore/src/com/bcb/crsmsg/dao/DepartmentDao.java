package com.bcb.crsmsg.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.common.modal.Department;

public class DepartmentDao extends HibernateDaoSupport implements IDepartmentDao{

	@SuppressWarnings("unchecked")
	public List<Department> getDeptByUserId(String userId) {
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);
		try{
//			Query query = session.createQuery("select new Department(d,t) from Department d, User u, TblRef t where u.userId = ? and d.deptStatus='Active' and u.userGroup = d.id and t.tblRefId = d.smsType");
			Query query = session.createQuery("select d from Department d, User u where u.userId = ? and d.deptStatus='Active' and u.userGroup = d.id");
			query.setParameter(0, userId);
			List<Department> results = query.list();
			return results;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			if(session.isOpen()) session.close();
		}
	}

}
