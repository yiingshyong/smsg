package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.FileConversion;

public class FileConversionDao 
extends HibernateDaoSupport
implements IFileConversionDao {

	public List<FileConversion> findAll() {
		return getHibernateTemplate().find("from FileConversion");
	}

	public void saveAll(List<FileConversion> fcList) {
		getHibernateTemplate().saveOrUpdateAll(fcList);
		
	}

}
