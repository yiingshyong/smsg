package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.FtpReport;



public class FtpReportDao 
extends HibernateDaoSupport
implements IFtpReportDao {
	
	public List<Object> findReport(String dbCommand, Object[] cmdParameters){
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}

	public void insert(FtpReport object) throws Exception {
		// TODO Auto-generated method stub
		getHibernateTemplate().save(object);
	}

	public void update(FtpReport object) throws Exception {
		// TODO Auto-generated method stub
		getHibernateTemplate().update(object);
	}

}
