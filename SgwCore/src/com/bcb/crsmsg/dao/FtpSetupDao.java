package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.util.Constants;

public class FtpSetupDao 
extends HibernateDaoSupport
implements IBatchSetupDao {
	
	public List<Object> findSetup(int fileConversionId) {
		final String queryString = "from FtpSetup where fileConversion=? and setupStatus=?";
		return getHibernateTemplate().find(queryString, new Object[]{fileConversionId, Constants.STATUS_ACTIVE});
	}
	
	public List<Object> findAll() {
		return getHibernateTemplate().find("from FtpSetup ");
	}

	public List<Object> findSetup(String dbCommand, Object[] cmdParameters){
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public List<Object> findAllMessageByPriority(int prioprity, int maxResult) {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteById(int id) {
		FtpSetup obj = findById(id);
		if (obj != null) {
			getHibernateTemplate().delete(obj);
			getHibernateTemplate().flush();
		}
	}

	public void deleteByObject(Object ftpSetupModal) {
		// TODO Auto-generated method stub
		
	}

	public FtpSetup getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public FtpSetup findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void insert(Object object) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public List<FtpSetup> getMessagesOrderByPriority(int maxResult) {
		// TODO Auto-generated method stub
		return null;
	}

	public void saveAll(List<Object> ftpSetupList) {
		// TODO Auto-generated method stub
		
	}

	public boolean isExist(Object ftpSetupModal) {
		// TODO Auto-generated method stub
		return false;
	}

	public void updateStatus(Object ftpSetupModal, String status) {
		// TODO Auto-generated method stub
		
	}

	public void update(Object ftpSetupModal) {
		getHibernateTemplate().update(ftpSetupModal);
	}
}
