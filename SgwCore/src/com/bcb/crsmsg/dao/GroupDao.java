package com.bcb.crsmsg.dao;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.common.modal.Department;

public class GroupDao extends HibernateDaoSupport implements IGroupDao {

	
	Log log = LogManager.getLog();

	public Department findByGroupId(final int groupId) {
		
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(Department.class);
				crit.add(Expression.eq("id",groupId));
				crit.setMaxResults(1);
				return crit.uniqueResult();
			}
		};
		return (Department) getHibernateTemplate().execute(callback);

	}

	
}
