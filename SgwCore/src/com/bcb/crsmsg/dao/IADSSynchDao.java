package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.ADSSynch;

public interface IADSSynchDao {
	public List<Object> findRecord(String dbCommand, Object[] cmdParameters);
	
	public List<ADSSynch> findAll();

	public boolean save(ADSSynch aSynch);
	
	public boolean update(ADSSynch aSynch);
}
