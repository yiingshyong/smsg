package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.AutoAction;

public interface IAutoActionDao {

	public List<AutoAction> findNonDefaultActions();
	
	public AutoAction findNonDefaultActions(String className);
	
	public AutoAction getNonDefaultAutoActionByKeyword(Integer keywordId);	
	
	public AutoAction getKeywordActionMethod(Integer keywordId);
	
	public boolean isCreditCardActivationKeyword(Integer keywordId);	
	
	public AutoAction findOCMActions(String paraKey);
}
