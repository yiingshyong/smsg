package com.bcb.crsmsg.dao;

import java.util.List;

public interface IBatchSetupDao {
	public List<Object> findAllMessageByPriority(final int prioprity,final int maxResult);
	public List<Object> findSetup(int fileConversionId) ;
	
	public List<Object> findSetup(String dbCommand, Object[] cmdParameters);
	
	public void deleteById(int id);
	
	public void deleteByObject(Object objModal);
	
	public Object getById(int id);
	
	public Object findById(int id);
	
	public void insert(Object object) throws Exception;
	
	public void saveAll(List<Object> objList);
	
	public boolean isExist(Object objModal);
	
	public void updateStatus(Object objModal,String status);
	
	public void update(Object ftpSetupModal);
}
