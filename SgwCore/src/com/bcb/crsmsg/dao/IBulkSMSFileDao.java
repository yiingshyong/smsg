package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;


import com.bcb.crsmsg.modal.BulkSmsFile;
import com.bcb.crsmsg.modal.SmsQueue;

public interface IBulkSMSFileDao {
	
	static Log log = LogManager.getLog();
	
	public List<BulkSmsFile> findMessages(String status);
	public void updateStatus(List<BulkSmsFile> bulkSmsFile,String status);
}
