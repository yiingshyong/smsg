package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.BulkSmsSetup;


public interface IBulkSMSSetupDao {
	
	
	public List<BulkSmsSetup> findAllMessageByPriority(final int prioprity,final int maxResult);
	public List<BulkSmsSetup> findBulkSMSSetup(int fileConversionId) ;
	public void deleteById(int id);
	
	public void deleteByObject(BulkSmsSetup bulkSmsSetupModal);
	
	public BulkSmsSetup getById(int id);
	
	public BulkSmsSetup findById(int id);
	
	public void insert(BulkSmsSetup object) throws Exception;
	
	public void saveAll(List<BulkSmsSetup> bulkSmsSetupList);

	public boolean isExist(BulkSmsSetup bulkSmsSetupList);

	public void updateStatus(BulkSmsSetup bulkSmsSetupList,String status);
	
	
}
