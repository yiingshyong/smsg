package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.Customer;


public interface ICreditCardActivationDao {

//	public CreditCardActivation find(String mobileNo, String ccNo, String custId);
	public CreditCardActivation find(String ccNo);
	public void deleteByRptId(int id);	
	public List<Object> findByRptId(int id);
	public List<CreditCardActivation> findUniqueRecordByMobileNoSuffix(String mobileNo);
	public List<CreditCardActivation> findByMobileNo(String mobileNo);
	public List<CreditCardActivation> findByRefNo(String custId);
	public void update(CreditCardActivation obj) ;
}
