package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.Customer;

public interface ICustomerDao {
	public List<Customer> findByCustRefNo(final String custRefNo);
	public List<Customer> findUnsubByTransferStatus(final String status);
	public void insert(Customer obj) throws Exception;
	public void update(Customer obj) ;
	public void update(List<Customer> objList);
}
