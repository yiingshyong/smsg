package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.common.modal.Department;

public interface IDepartmentDao {

	public List<Department> getDeptByUserId(String userId);
	
}
