package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.FileConversion;

public interface IFileConversionDao {

	List<FileConversion> findAll();

	void saveAll(List<FileConversion> fcList);

}
