package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.FtpReport;


public interface IFtpReportDao {
	
	public List<Object> findReport(String dbCommand, Object[] cmdParameters);
	
	public void insert(FtpReport object) throws Exception;
	
	
	public void update(FtpReport object) throws Exception;
}
