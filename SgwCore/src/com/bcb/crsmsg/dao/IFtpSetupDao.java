package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.FtpSetup;


public interface IFtpSetupDao {
	
	
	public List<FtpSetup> findAllMessageByPriority(final int prioprity,final int maxResult);
	public List<FtpSetup> findFtpSetup(int fileConversionId) ;
	public void deleteById(int id);
	
	public void deleteByObject(FtpSetup ftpSetupModal);
	
	public FtpSetup getById(int id);
	
	public FtpSetup findById(int id);
	
	public void insert(FtpSetup object) throws Exception;
	
	public void saveAll(List<FtpSetup> ftpSetupList);

	public boolean isExist(FtpSetup ftpSetupModal);

	public void updateStatus(FtpSetup ftpSetupModal,String status);
	
	public List<FtpSetup> findSetup(String dbCommand, Object[] cmdParameters);
	
	public void update(Object ftpSetupModal);
}
