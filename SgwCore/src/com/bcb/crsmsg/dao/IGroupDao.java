package com.bcb.crsmsg.dao;

import com.bcb.common.modal.Department;


public interface IGroupDao {

	
	
	public Department findByGroupId(int groupId);
	

}
