package com.bcb.crsmsg.dao;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;

public interface ISenderThreadDao {

	void successStatusUpdating(SmsQueue smsQueue, Telco operator)throws Exception ;

	void failStatusUpdating(SmsQueue smsQueue, Telco operator)throws Exception;

}
