package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.ServiceProvider;

public interface IServiceProvider {

	public List<ServiceProvider> getActiveServiceProviders();
	public boolean inUsed(Integer id);
	public void delete(Integer id); 
}
