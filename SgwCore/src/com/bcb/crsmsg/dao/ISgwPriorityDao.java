package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.Priority;

public interface ISgwPriorityDao {
	
	
	List<Priority> findAll(); 
	
	List<Priority> findAllAvailable();
	
	List<Priority> findAllOrderByValue();

	List<Priority> findAllAvailableOrderByValue();
	
	void save(Priority pp);
	
	public Integer findLowestPriority();	

}
