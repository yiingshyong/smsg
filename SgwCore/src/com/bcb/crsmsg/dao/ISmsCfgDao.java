package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.HttpIpAddress;
import com.bcb.crsmsg.modal.SmsCfg;

public interface ISmsCfgDao {
	public SmsCfg findByCfgId(String cfgId);
	public SmsCfg findSmsCfg();	
	public void update(SmsCfg config);
	public List<HttpIpAddress> findIP(String ip);	
}
