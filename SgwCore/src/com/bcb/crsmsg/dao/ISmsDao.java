package com.bcb.crsmsg.dao;

import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsQueue;

public interface ISmsDao {

	boolean createObjectBySmsQueue(SmsQueue smsQueue);

	public List<Sms> findAllMessage(String dbCommand, Object[] cmdParameters);
	
	public Sms findRecordById(int id);
	
	public List<SIBSBatchOutputFileBean> retrieveSMSStatus(String ftpOwnerName, Date ftpDate);	
}
