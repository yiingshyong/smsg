package com.bcb.crsmsg.dao;

import com.bcb.crsmsg.modal.SmsInvalid;

public interface ISmsInvalidDao{

	public void insert(SmsInvalid obj) throws Exception;
}
