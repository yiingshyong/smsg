package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.SmsMQ;

public interface ISmsMQDao {
	
	public List<SmsMQ> findBySMSType(int smsTypeId);
	
	public List<SmsMQ> findAllNonClosedMQ();
	
	public SmsMQ getByMQName(String MQName);
	
	public List<SmsMQ> findAll();
	
	public SmsMQ findById(int mqId);	
		
}
