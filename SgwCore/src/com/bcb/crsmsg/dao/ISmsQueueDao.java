package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;

public interface ISmsQueueDao {
	
	static Log log = LogManager.getLog();
	
	public SmsQueue getSmsForTelco(int telcoId, SmsCfg smsCfg);
	
	public List<SmsQueue> findMessages(boolean prioritize, final int maxResult);
	
	public List<SmsQueue> findAllMessage(String dbCommand, Object[] cmdParameters);
	
	public Integer findCurrentQueueSize();
	
	public void deleteById(int id);
	
	public void deleteByObject(SmsQueue smsQueue);
	
	public SmsQueue getById(int id);
	
	public SmsQueue findById(int id);
	
	public void insert(SmsQueue object) throws Exception;

	public void saveAll(List<SmsQueue> smsList);

	public boolean isExist(SmsQueue smsQueue);

	public void update(SmsQueue smsQueue);
	
	public void updateStatus(SmsQueue smsQueue,String status);
	
	public void updateStatus(ArrayList<SmsQueue> smsQueue,String status);
	
	public void resetAllStatus(LinkedList<SmsQueue> smsQueueList,String status);
	
	public int initialiseAllMessageStatus(String smsStatusFrom, String smsStatusTo);
	
	public List<SmsQueue> updateStatus(List<SmsQueue> smsQueue,String status,boolean prioritize);
	
	public Integer findMessageSize(String status);
	
	public List<SIBSBatchOutputFileBean> retrieveSMSStatus(String ftpOwnerName, Date ftpDate);	
	
}
