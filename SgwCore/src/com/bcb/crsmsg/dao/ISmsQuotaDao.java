package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.SmsQuota;


public interface ISmsQuotaDao {
	public List<SmsQuota> findSmsQuota(String dbCommand, Object[] cmdParameters);
	public List<SmsQuota> findSmsQuota(int deptId, int size);	
	public void updateSmsQuota(SmsQuota theQuota);
	public List<SmsQuota> findSmsQuota(int deptId);
}
