package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.SmsRate;

public interface ISmsRate {

	public List<SmsRate> getActiveSmsRate();
	
	public boolean inUsed(Integer id);
	
	public void delete(Integer id);
	
	public List<SmsRate> getRates(Integer[] rateIds);
		
}
