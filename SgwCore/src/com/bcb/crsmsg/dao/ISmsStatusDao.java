package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.SmsStatus;

public interface ISmsStatusDao {
	public List<SmsStatus> findRecord(String dbCommand, Object[] cmdParameters);
	
	public List<SmsStatus> findAll();
	
	public SmsStatus findById(int id);
}
