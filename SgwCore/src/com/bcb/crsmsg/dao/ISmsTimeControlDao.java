package com.bcb.crsmsg.dao;

import java.util.List;
import java.util.Map;

public interface ISmsTimeControlDao {
	public List<Integer> findDeptBatchBlock();
	public List<Integer> findDeptWebBlock();
	public List<Integer> findDeptBlock(String controlMode);
	public Map<Integer, Boolean> findTimeCtrlBatchBlock();	
	public Map<Integer, Boolean> findTimeCtrlWebBlock();
	public Map<Integer, Boolean> findTimeCtrlBlock(String controlMode);
}
