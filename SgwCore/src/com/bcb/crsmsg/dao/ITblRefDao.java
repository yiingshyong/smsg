package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.TblRef;

public interface ITblRefDao {
	
	public List<TblRef> getSmsTypes();
	public TblRef selectSMSType(String userId, String channelId);	
	public List<TblRef> getAllSMSTypes();
	public Integer getSize(String label);
	public boolean isSmsTypeInUsed(int smsTypeId);	
	public void deleteSmsType(int smsTypeId);
	public void updateSmsType(TblRef smsType) throws Exception;
	
}
