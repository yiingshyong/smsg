package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.modal.Telco;

public interface ITelcoEsmsDao {
	public List<Option> findAllOption();	
	public List<Telco> findAll();
	public List<Telco> findTelcos(String... status);	
	public List<Telco> findAllTelcoEsmsAvailable();

	public List<Telco> findAllTelcoEsmsAvailableByPriority(
			Integer priorityValue);

	public List<Telco> findAllTelcoEsmsAvailableByTelcoRoutingMode(String routingMode, String routingParam);
	
	public void save(Telco obj);
	
	public void update(Telco obj);
	
	public void initialiseAllTelco(String updateQueryStr, Object[] updateParam);
	
	public Telco findById(int i);

	public Telco findAvailableTelcoById(int id);
	
	public List<Telco> findAvailableTelcoByIds(String... ids);
	
	public List<Telco> findAllTelcoEsmsAvailableOrderByPriority();
	
	public List<Telco> findByIds(String...ids);
	
}
