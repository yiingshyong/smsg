package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.MsgTemplate;

public interface ITemplateMessageDao {
	public MsgTemplate getById(int id);
	
	public List findAvailabelTemplateByMsgCode(String code);
}
