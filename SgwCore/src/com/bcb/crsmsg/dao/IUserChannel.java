package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.crsmsg.modal.UserChannel;

public interface IUserChannel {
	
	public List<UserChannel> getActiveUserChannels(String userId);
	
	public List<UserChannel> getAllUserChannels(String userId);	

	public List<UserChannel> getUserChannels(String userId, String status);
}
