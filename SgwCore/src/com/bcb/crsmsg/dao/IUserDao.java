package com.bcb.crsmsg.dao;

import java.util.List;

import com.bcb.common.modal.User;

public interface IUserDao {
	
	public User findByUserId(String userId);
	public boolean hasPermission(String userId, Integer permId);
	public List<User> findUserByIdLike(String userId);
	public boolean hasRights(String userId, int rightId);	
	
}
