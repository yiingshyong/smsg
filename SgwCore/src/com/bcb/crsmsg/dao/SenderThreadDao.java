package com.bcb.crsmsg.dao;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.sgwcore.senderprocessor.LogManager;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class SenderThreadDao 
extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
implements ISenderThreadDao {
	
	ISmsQueueDao smsQueueDao=null;
	
	Log log = LogManager.getLog();
	
	public SenderThreadDao(){
		
	}
	
	private void loadData()
	{
		try{
			smsQueueDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
	}
	
	public void failStatusUpdating(SmsQueue smsQueue, Telco operator) throws Exception {
		loadData();
		//unSentLogDao.createObjectBySmsQueue(smsQueue, operator);
	}
	
	public void successStatusUpdating(SmsQueue smsQueue, Telco operator) throws Exception {
		loadData();
		smsQueueDao.deleteById(smsQueue.getId());
		//sentLogDao.createObjectBySmsQueue(smsQueue, operator);
	}
	
}
