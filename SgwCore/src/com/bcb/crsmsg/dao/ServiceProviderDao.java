package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.modal.Telco;

public class ServiceProviderDao extends HibernateDaoSupport implements IServiceProvider {

	@SuppressWarnings("unchecked")
	public List<ServiceProvider> getActiveServiceProviders(){
		return getHibernateTemplate().find("from ServiceProvider");
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean inUsed(Integer id) {
		List<Telco> telcoAPIs = getHibernateTemplate().find("from Telco where svcPvdId = ?", new Object[]{id});
		return (telcoAPIs != null && !telcoAPIs.isEmpty());
	}

	@Override
	public void delete(Integer id) {
		getHibernateTemplate().bulkUpdate("delete from ServiceProvider where id = ?", new Object[]{id});
	}
	
}
