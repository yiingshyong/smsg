package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.Priority;
import com.bcb.crsmsg.util.Constants;

public class SgwPriorityDao 
extends HibernateDaoSupport
implements ISgwPriorityDao
{

	public List<Priority> findAll() {
		return getHibernateTemplate().find("from Priority");
	}
	
	public List<Priority> findAllAvailable() {
		return getHibernateTemplate().find("from Priority where status=?", Constants.STATUS_ACTIVE);
	}

	public List<Priority> findAllOrderByValue() {
      return getHibernateTemplate().find("from Priority order by id"); 
	}
	
	public List<Priority> findAllAvailableOrderByValue() {
	      return getHibernateTemplate().find("from Priority where status=? order by id", Constants.STATUS_ACTIVE); 
		}

	public Integer findLowestPriority(){
		return (Integer) getHibernateTemplate().find("select max(id) from Priority where status=?", Constants.STATUS_ACTIVE).get(0);
	}
	
	public void save(Priority pp) {
		getHibernateTemplate().save(pp);
	}

}
