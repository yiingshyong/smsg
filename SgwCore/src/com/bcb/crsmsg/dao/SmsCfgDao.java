package com.bcb.crsmsg.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.HttpIpAddress;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class SmsCfgDao extends HibernateDaoSupport implements ISmsCfgDao {

	Log log = LogManager.getLog();

	public SmsCfg findByCfgId(final String cfgId) {
		
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(SmsCfg.class);
				crit.add(Expression.eq("id",Integer.valueOf(cfgId)));
				crit.setMaxResults(1);
				return crit.uniqueResult();
			}
		};
		return (SmsCfg) getHibernateTemplate().execute(callback);

	}
	
	public SmsCfg findSmsCfg(){
		SmsCfg smsCfg=null;
		try {
			ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
			smsCfg = cfgDao.findByCfgId(Integer.valueOf(Constants.APPSVERSION).toString());
			if (smsCfg == null){
			}else{
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return smsCfg;
	}

	public void update(SmsCfg config){
		getHibernateTemplate().update(config);
	}
	
	@SuppressWarnings("unchecked")
	public List<HttpIpAddress> findIP(String ip){
		return getHibernateTemplate().find("from HttpIpAddress where ip = ?", new Object[]{ip});
	}
}
