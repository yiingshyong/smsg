package com.bcb.crsmsg.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsQueue;

public class SmsDao 
extends HibernateDaoSupport
implements ISmsDao
{
	public boolean createObjectBySmsQueue(SmsQueue smsQueue) {
		try{
			Sms aSms = new Sms();
			BeanUtils.copyProperties(smsQueue,aSms);
			getHibernateTemplate().save(aSms);
			return true;
		}catch (Exception ex){
			ex.printStackTrace();
			return false;
		}
	}
	
	public List<Sms> findAllMessage(String dbCommand, Object[] cmdParameters) {
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public Sms findRecordById(int id) {
		return (Sms)getHibernateTemplate().get(Sms.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<SIBSBatchOutputFileBean> retrieveSMSStatus(String ftpOwnerName, Date ftpDate){
		Calendar endStartDate = Calendar.getInstance();
		endStartDate.setTime(ftpDate);
		endStartDate.add(Calendar.DAY_OF_MONTH, 1);
		
		List<Integer> ftpReportIds = getHibernateTemplate().find("select a.id from FtpReport a, FtpSetup b where a.ftpSetup = b.id and b.ftpOwner = ? and a.startDate >= ? and a.startDate < ? and b.ftpMode='I'", 
						new Object[]{ftpOwnerName, ftpDate, endStartDate.getTime()});
		
		if(ftpReportIds == null || ftpReportIds.isEmpty()){
			return null;
		}
		return getHibernateTemplate().findByNamedParam("select new com.bcb.crsmsg.bean.SIBSBatchOutputFileBean(a.mobileNo,a.smsStatusDatetime, a.createdDatetime, a.message, a.sentBy, b.telcoName, c.statusName, a.remarks, a.msgRefNum) from Sms a, Telco b, SmsStatus c where a.telco=b.id and a.smsStatus=c.statusCode and a.ftp in (:ids)", "ids", ftpReportIds);
	}
}
