package com.bcb.crsmsg.dao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsInvalid;

public class SmsInvalidDao extends HibernateDaoSupport{

	public void insert(SmsInvalid obj) throws Exception {
		getHibernateTemplate().save(obj);
	}
}
