package com.bcb.crsmsg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.util.Constants;

public class SmsMQDao extends HibernateDaoSupport implements ISmsMQDao {

	static Log log = LogManager.getLog();
		
	@SuppressWarnings("unchecked")
	public List<SmsMQ> findBySMSType(int smsTypeId){
		return getHibernateTemplate().find("from SmsMQ where smsType = ?", smsTypeId);
	}
	
	@SuppressWarnings("unchecked")
	public List<SmsMQ> findAllNonClosedMQ() {
		Session session = getSession();		
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("select new SmsMQ(mq.mqId,t.telcoName,tbl.label,mq.noOfThread,t.telcoClassName) ");
		queryBuffer.append("from SmsMQ mq, Telco t, TblRef tbl "); 
//		queryBuffer.append("where t.status != ? ");
//		queryBuffer.append("and tbl.status = ? ");
		queryBuffer.append("where ");
		queryBuffer.append("mq.telcoId = t.id "); 
		queryBuffer.append("and mq.smsType = tbl.tblRefId ");
		queryBuffer.append("order by mq.mqId ");
		
		try{
			Query query = session.createQuery(queryBuffer.toString());
//			query.setParameter(0, Constants.FORM_FIELD_STATUS_CLOSED);
//			query.setParameter(1, Constants.FORM_FIELD_STATUS_ACTIVE);
			List<SmsMQ> results = query.list();
			return results;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()) releaseSession(session);
		}
	}
	
	@SuppressWarnings("unchecked")
	public SmsMQ findById(int mqId){
		List<SmsMQ> results = getHibernateTemplate().find("select new SmsMQ(mq.mqId,t.telcoName,tbl.label,mq.noOfThread,t.telcoClassName) " +
		"from SmsMQ mq, Telco t, TblRef tbl where mq.mqId = ? " +
		"and mq.telcoId = t.id and mq.smsType = tbl.tblRefId order by mq.smsType, mq.telcoId", mqId);
		if(results == null || results.isEmpty()){
			return null;
		}
		return results.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<SmsMQ> findAll(){
		return getHibernateTemplate().find("select new SmsMQ(mq.mqId,t.telcoName,tbl.label,mq.noOfThread,t.telcoClassName) " +
				"from SmsMQ mq, Telco t, TblRef tbl where mq.telcoId = t.id and mq.smsType = tbl.tblRefId order by mq.smsType, mq.telcoId");
	}
				
	@SuppressWarnings("unchecked")
	public SmsMQ getByMQName(String mqName) {
		Session session = getSession();		
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("select new SmsMQ(mq.mqId,t.telcoName,tbl.label,mq.noOfThread,t.telcoClassName) ");
		queryBuffer.append("from SmsMQ mq, Telco t, TblRef tbl "); 
		queryBuffer.append("where t.status != ? ");
		queryBuffer.append("and tbl.status = ? ");
		queryBuffer.append("and concat(telco_name,' (',label,')') = ? ");
		queryBuffer.append("and mq.telcoId = t.id "); 
		queryBuffer.append("and mq.smsType = tbl.tblRefId ");
		queryBuffer.append("order by mq.mqId ");
		SmsMQ smsMQ = null;
		try{
			Query query = session.createQuery(queryBuffer.toString());
			query.setParameter(0, Constants.FORM_FIELD_STATUS_CLOSED);
			query.setParameter(1, Constants.FORM_FIELD_STATUS_ACTIVE);
			query.setParameter(2, mqName);
			List<SmsMQ> results = query.list();
			if(results.size() == 1){
				smsMQ = results.get(0);
			}
			return smsMQ;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session.isOpen()) releaseSession(session);
		}
	}
}
