package com.bcb.crsmsg.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SmsQueueDao extends HibernateDaoSupport implements ISmsQueueDao {

	static Log log = LogManager.getLog();

	/*
	 * This method is to get sms that is allowed to be sent via the telco parameter
	 *	 
	 **/
	public SmsQueue getSmsForTelco(int telcoId, SmsCfg smsCfg){
		List<TblRef> smsTypes = DaoBeanFactory.getTblRefDao().getSmsTypes();
		if(smsTypes == null || smsTypes.isEmpty()) return null;
		//To get sms type which has this telco as first priority
		int smsTypeId = 0;
		int position = 0;
//		for(TblRef smsType : smsTypes){
//			String[] routableTelcos = StringUtils.split(smsType.getRoutableTelco(), smsCfg.getTableListDelimiter());
//			if(routableTelcos == null || routableTelcos.length == 0) continue;
//			for(int i=0; i<routableTelcos.length; i++){
//				if(Integer.parseInt(routableTelcos[i].trim()) == telcoId){
//					if(position == 0 || position > i+1){
//						position = i+1;
//						smsTypeId = smsType.getTblRefId();
//					}
//					break;
//				}
//			}
//			if(position == 1) break;
//		}
//		if(position == 0 || smsTypeId == 0) return null;
		List<Integer> tblRefs = new ArrayList<Integer>();
		for(TblRef smsType : smsTypes){
			String[] routableTelcos = StringUtils.split(smsType.getRoutableTelco(), smsCfg.getTableListDelimiter());
			if(routableTelcos == null || routableTelcos.length == 0) continue;
				for(String id : routableTelcos){
					if(Integer.parseInt(id.trim()) == telcoId){
						tblRefs.add(smsType.getTblRefId());
					}
				}
		}
		
		if(tblRefs.isEmpty()){
			return null;
		}
		
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		StringBuffer queryBuffer = new StringBuffer();
//		queryBuffer.append("select new SmsQueue(q, t) from SmsQueue q, Department d, TblRef t ");
//		queryBuffer.append("where (q.smsStatus=? or (q.smsStatus=? and q.smsScheduled=? and q.smsScheduledTime<=?)) ");
//		queryBuffer.append("and d.smsType = ? ");		
//		queryBuffer.append("and q.deptId = d.id ");
//		queryBuffer.append("and t.tblRefId = d.smsType ");
//		queryBuffer.append("order by q.priority,q.id");
		queryBuffer.append("select q from SmsQueue q ");
		queryBuffer.append("where (q.smsStatus=? or (q.smsStatus=? and q.smsScheduled=? and q.smsScheduledTime<=?)) ");
		queryBuffer.append("and q.type != ? ");
		queryBuffer.append("and q.smsType.tblRefId in (:list) ");
		queryBuffer.append("order by q.priority,q.id");
	
		try{
			Query query = session.createQuery(queryBuffer.toString());
			int count = 0;
			query.setParameter(count++, Constants.SMS_STATUS_4);
			query.setParameter(count++, Constants.SMS_STATUS_6);
			query.setParameter(count++, Constants.STATUS_YES);
			query.setParameter(count++, new Date());
			query.setParameter(count++, Constants.SMS_SOURCE_TYPE_5);
//			query.setParameter(count++, smsTypeId);
			query.setParameterList("list", tblRefs);
			query.setFetchSize(1).setMaxResults(1);
			List<SmsQueue> results = query.list();
			if(results == null || results.size() == 0) return null;
			return results.get(0);
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			if(session.isOpen()) session.close();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<SmsQueue> findMessages(boolean prioritize, final int maxResult) {
//		return getHibernateTemplate().find("from SmsQueue where (smsStatus=? or (smsStatus=? and smsScheduled=? and smsScheduledTime<=?)) order by priority,id", 
//				new Object[]{Constants.SMS_STATUS_4, Constants.SMS_STATUS_6, Constants.STATUS_YES, new Date()});		
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		StringBuffer queryBuffer = new StringBuffer();
//		queryBuffer.append("select new SmsQueue(q, t) from SmsQueue q, Department d, TblRef t ");
//		queryBuffer.append("where (q.smsStatus=? or (q.smsStatus=? and q.smsScheduled=? and q.smsScheduledTime<=?)) ");
//		queryBuffer.append("and q.deptId = d.id ");
//		queryBuffer.append("and t.tblRefId = d.smsType ");
//		queryBuffer.append("order by t.value ");
//		queryBuffer.append(prioritize? ",q.priority,q.id" : ",q.id");

		queryBuffer.append("select q from SmsQueue q ");
		queryBuffer.append("where (q.smsStatus=? or (q.smsStatus=? and q.smsScheduled=? and q.smsScheduledTime<=?)) ");
		queryBuffer.append("order by ");
		queryBuffer.append(prioritize? "q.priority," : "");
		queryBuffer.append(" q.smsType.value,q.id ");
		try{
			Query query = session.createQuery(queryBuffer.toString());
			int count = 0;
			query.setParameter(count++, Constants.SMS_STATUS_4);
			query.setParameter(count++, Constants.SMS_STATUS_6);
			query.setParameter(count++, Constants.STATUS_YES);
			query.setParameter(count++, new Date());
			query.setFetchSize(maxResult).setMaxResults(maxResult);
			List<SmsQueue> results = query.list();
			return results;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			if(session.isOpen()) session.close();
		}
	}
	
	public Integer findMessageSize(String status){
		return (Integer)getHibernateTemplate().find("select count(*) from SmsQueue where smsStatus=? ", new Object[]{Constants.SMS_STATUS_5}).get(0);		
	}
	
	public Integer findCurrentQueueSize() {
		return (Integer)getHibernateTemplate().find("select count(*) from SmsQueue where (smsStatus=? or (smsStatus=? and smsScheduled=? and smsScheduledTime<=?)) ", 
				new Object[]{Constants.SMS_STATUS_4, Constants.SMS_STATUS_6, Constants.STATUS_YES, new Date()}).get(0);
	}
	
	public List<SmsQueue> findAllMessage(String dbCommand, Object[] cmdParameters) {
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}

	public SmsQueue getById(int id) {
		return (SmsQueue) getHibernateTemplate().get(SmsQueue.class, id);
	}

	public SmsQueue findById(final int id) {
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(SmsQueue.class);
				crit.add(Expression.eq("id",id));
				crit.setMaxResults(1);
				return crit.uniqueResult();
			}
		};
		return (SmsQueue) getHibernateTemplate().execute(callback);
	}

	public void deleteById(int id) {
		SmsQueue obj = findById(id);
		if (obj != null) {
			getHibernateTemplate().delete(obj);
			getHibernateTemplate().flush();
		}
	}

	public void deleteByObject(SmsQueue smsQueue) {
		getHibernateTemplate().delete(smsQueue);
	}

	public void insert(SmsQueue obj) throws Exception {
		if(obj.getSmsType() == null){
			obj.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(obj.getSentBy(), obj.getChannelId()));
		}
		getHibernateTemplate().save(obj);
	}

	public Conjunction ApproveAndStatus(final String approval,
			final String status) {
		Conjunction con = Expression.conjunction();
		con.add(Expression.eq("approvalStatus", approval));
		con.add(Expression.eq("status", status));
		return con;
	}

	public void saveAll(List<SmsQueue> smsList) {
		for (SmsQueue obj : smsList) {
			try {
				insert(obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isExist(SmsQueue smsQueue) {
		try {
			if (smsQueue == null)
				return false;

			SmsQueue sms = findById(smsQueue.getId());
			if (sms != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public int initialiseAllMessageStatus(String smsStatusFrom, String smsStatusTo) {
		return getHibernateTemplate().bulkUpdate("update SmsQueue set smsStatus=? where smsStatus=?", new Object[]{smsStatusTo, smsStatusFrom});
	}
	
	public void update(SmsQueue smsQueue) {
		getHibernateTemplate().update(smsQueue);	
	}
	
	public void updateStatus(SmsQueue smsQueue, String status) {
		try{
		SmsQueue sms = findById(smsQueue.getId());
		if (sms != null) {
			sms.setSmsStatus(status);
			sms.setSmsStatusBy("automated");
			sms.setSmsStatusDatetime(new Date());
			getHibernateTemplate().update(sms);
			getHibernateTemplate().flush();
		}
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
	}
	
	public void updateStatus(ArrayList<SmsQueue> smsQueue, String status) {
		for(int i=0;i<smsQueue.size();i++){
			updateStatus(smsQueue.get(i), status);
		}
	}
	
	public List<SmsQueue> updateStatus(List<SmsQueue> smsQueueList, String status,boolean prioritize) {
		
		List<SmsQueue> results;
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
		Transaction txn = session.getTransaction();
		txn.begin();
		try{
				
			String aaa="0";
			for(int f=0;f< smsQueueList.size();f++){
				aaa = aaa + "," + smsQueueList.get(f).getId();
			}
			Query query = session.createQuery("update SmsQueue set smsStatus=?,smsStatusBy=?,smsStatusDatetime=? where id in ("+aaa+") and (smsStatus = ? or smsStatus = ?) ");
			String random = Double.toString(Math.random());
			if(random.length() > 15){
				random = random.substring(0,15);
			}
			query.setParameter(0, Constants.SMS_STATUS_M);
			query.setParameter(1, random);
			query.setParameter(2, new Date());
			query.setParameter(3, Constants.SMS_STATUS_4);
			query.setParameter(4, Constants.SMS_STATUS_6);
			query.executeUpdate();
			txn.commit();
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
			
			session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), true);	
			
			StringBuffer queryBuffer = new StringBuffer();
//			queryBuffer.append("select new SmsQueue(q, t) from SmsQueue q, Department d, TblRef t ");
//			queryBuffer.append("where q.smsStatusBy = ? ");
//			queryBuffer.append("and q.deptId = d.id ");
//			queryBuffer.append("and t.tblRefId = d.smsType "); 
//			queryBuffer.append("order by t.value ");
//			queryBuffer.append(prioritize? ",q.priority,q.id" : ",q.id");
			queryBuffer.append("select q from SmsQueue q ");
			queryBuffer.append("where q.smsStatusBy = ? ");			
			queryBuffer.append("order by ");
			queryBuffer.append(prioritize? "q.priority," : "");
			queryBuffer.append(" q.smsType.value,q.id ");			
			Query query2 = session.createQuery(queryBuffer.toString());
			query2.setParameter(0, random);
			results = query2.list();
			
		}catch(Exception e){
			e.printStackTrace();
			txn.rollback();
			return null;
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());			
			if(session.isOpen()) session.close();
		}
		return results;
	}
	
	public void resetAllStatus(LinkedList<SmsQueue> smsQueueList,String status){
		for(int i=0;i<smsQueueList.size();i++){
			SmsQueue sms=(SmsQueue)smsQueueList.get(i);
			updateStatus(sms, status);
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<SIBSBatchOutputFileBean> retrieveSMSStatus(String ftpOwnerName, Date ftpDate){
		Calendar endStartDate = Calendar.getInstance();
		endStartDate.setTime(ftpDate);
		endStartDate.add(Calendar.DAY_OF_MONTH, 1);
		
		List<Integer> ftpReportIds = getHibernateTemplate().find("select a.id from FtpReport a, FtpSetup b where a.ftpSetup = b.id and b.ftpOwner = ? and a.startDate >= ? and a.startDate < ? and b.ftpMode='I'", 
						new Object[]{ftpOwnerName, ftpDate, endStartDate.getTime()});
		
		if(ftpReportIds == null || ftpReportIds.isEmpty()){
			return null;
		}
		return getHibernateTemplate().findByNamedParam("select new com.bcb.crsmsg.bean.SIBSBatchOutputFileBean(a.mobileNo,a.smsStatusDatetime, a.createdDatetime, a.message, a.sentBy, b.telcoName, c.statusName, a.remarks, a.msgRefNum) from SmsQueue a, Telco b, SmsStatus c where a.telco=b.id and a.smsStatus=c.statusCode and a.ftp in (:ids)", "ids", ftpReportIds);
	}
	
}
