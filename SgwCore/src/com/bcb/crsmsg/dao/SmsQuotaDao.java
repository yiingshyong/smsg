package com.bcb.crsmsg.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsQuota;

public class SmsQuotaDao extends HibernateDaoSupport implements ISmsQuotaDao {

	Log log = LogManager.getLog();

	public List<SmsQuota> findSmsQuota(String dbCommand, Object[] cmdParameters) {
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public List<SmsQuota> findSmsQuota(int deptId) {
		return getHibernateTemplate().find("select o from SmsQuota o where ref = ?", new Object[]{deptId});
	}
	
	public List<SmsQuota> findSmsQuota(int deptId, int size) {
		return getHibernateTemplate().find("select o from SmsQuota o where ref = ? and quota >= ?", new Object[]{deptId, size});
	}
	
	public void updateSmsQuota(SmsQuota theQuota) {
		try{
				getHibernateTemplate().update(theQuota);
				getHibernateTemplate().flush();
			}catch(Exception e){
				log.error(e.getMessage(),e);
			}
	}
}
