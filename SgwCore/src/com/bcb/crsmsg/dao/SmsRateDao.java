package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SmsRateDao extends HibernateDaoSupport implements ISmsRate {

	@Override
	public List<SmsRate> getActiveSmsRate() {
		return getHibernateTemplate().find("from SmsRate");
	}
	
	public boolean inUsed(Integer id){
		List<ServiceProvider> providers = DaoBeanFactory.getServiceProviderGroupDao().getActiveServiceProviders();
		for(ServiceProvider svcProvider : providers){
			if(svcProvider.getDefaultSmsRateId() == id || Arrays.asList(StringUtils.splitByWholeSeparator(svcProvider.getSmsRateIds(),"|")).contains(String.valueOf(id))){
				return true;
			}
		}
		return false;
	}

	@Override
	public void delete(Integer id) {
		getHibernateTemplate().bulkUpdate("delete from SmsRate where id = ? ", new Object[]{id});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SmsRate> getRates(Integer[] rateIds) {
		Session session = getSession();
		try{
			session.beginTransaction();
			Query query = session.createQuery("from SmsRate where id in (:list)");
			query.setParameterList("list", rateIds);
			List<SmsRate> results = query.list();
			session.getTransaction().commit();
			return results != null ? results : new ArrayList<SmsRate>();
		}catch(Exception e){
			if(session.getTransaction().isActive()){
				session.getTransaction().rollback();
			}
		}finally{
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());						
			if(session != null && session.isOpen()){
				session.close();
			}
		}
		return null;
	}

}
