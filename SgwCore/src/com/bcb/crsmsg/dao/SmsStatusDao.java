package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsStatus;

public class SmsStatusDao extends HibernateDaoSupport implements ISmsStatusDao{
	public List<SmsStatus> findRecord(String dbCommand, Object[] cmdParameters) {
		return getHibernateTemplate().find(dbCommand, cmdParameters);
	}
	
	public List<SmsStatus> findAll(){
		return getHibernateTemplate().find("from SmsStatus");
	}
	
	public SmsStatus findById(int id) {
		return (SmsStatus)getHibernateTemplate().get(SmsStatus.class, id);
	}
}
