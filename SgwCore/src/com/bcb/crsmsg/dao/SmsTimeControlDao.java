package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsTimeControl;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;

public class SmsTimeControlDao extends HibernateDaoSupport implements ISmsTimeControlDao {
	Log log = LogManager.getLog();

	public List<Integer> findDeptBatchBlock() {
		return findDeptBlock("smsTimeControlBatch");
	}
	
	public List<Integer> findDeptWebBlock() {
		return findDeptBlock("smsTimeControlWeb");
	}

	public List<Integer> findDeptBlock(String controlMode) {
		Map<Integer, Boolean> onHoldMap = new HashMap<Integer, Boolean>();
		List<Integer> deptSmsOnHoldList=new ArrayList<Integer>();
		Calendar now=Calendar.getInstance();
		int dayOfWeek=now.get(Calendar.DAY_OF_WEEK);
		int hour=now.get(Calendar.HOUR_OF_DAY);
		int minute=now.get(Calendar.MINUTE);
		String minStr = Integer.toString(minute);
		minStr = minStr.length() == 1? "0" + minStr : minStr;
		String hourMin=Integer.toString(hour)+minStr;
		
		List controlList=getHibernateTemplate().find("from SmsTimeControl where "+controlMode+"=?", new Object[]{Constants.STATUS_YES});
		log.debug("controlList.size(): "+controlList.size());
		for(int i=0;i<controlList.size();i++){// For each SMS Time Control
			SmsTimeControl aControl=(SmsTimeControl)controlList.get(i);
			log.debug("aControl.getSmsTimeControlDay(): "+CommonUtils.NullChecker(aControl.getSmsTimeControlDay(), String.class).toString());
			log.debug("Integer.toString(dayOfWeek): "+Integer.toString(dayOfWeek));
			if(CommonUtils.NullChecker(aControl.getSmsTimeControlDay(), String.class).toString().contains(Integer.toString(dayOfWeek))){// today is in controlled
				String[] timeFrom=CommonUtils.NullChecker(aControl.getSmsTimeControlTimeFrom(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
				String[] timeTo=CommonUtils.NullChecker(aControl.getSmsTimeControlTimeTo(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
				if(Integer.valueOf(hourMin)<Integer.valueOf(timeFrom[dayOfWeek-1])||Integer.valueOf(hourMin)>Integer.valueOf(timeTo[dayOfWeek-1])){
					deptSmsOnHoldList.add(aControl.getDept());
					onHoldMap.put(aControl.getDept(), !aControl.getAutoPatchToUnsent());
				}
			}else{
				// Skip, no control needed
			}
		}

		return deptSmsOnHoldList;
	}
	public Map<Integer, Boolean> findTimeCtrlBatchBlock() {
		return findTimeCtrlBlock("smsTimeControlBatch");
	}
	
	public Map<Integer, Boolean> findTimeCtrlWebBlock() {
		return findTimeCtrlBlock("smsTimeControlWeb");
	}

	public Map<Integer, Boolean> findTimeCtrlBlock(String controlMode) {
		Map<Integer, Boolean> onHoldMap = new HashMap<Integer, Boolean>();
		Calendar now=Calendar.getInstance();
		int dayOfWeek=now.get(Calendar.DAY_OF_WEEK);
		int hour=now.get(Calendar.HOUR_OF_DAY);
		int minute=now.get(Calendar.MINUTE);
		String minStr = Integer.toString(minute);
		minStr = minStr.length() == 1? "0" + minStr : minStr;
		String hourMin=Integer.toString(hour)+minStr;
		
		List controlList=getHibernateTemplate().find("from SmsTimeControl where "+controlMode+"=?", new Object[]{Constants.STATUS_YES});
		log.debug("controlList.size(): "+controlList.size());
		for(int i=0;i<controlList.size();i++){// For each SMS Time Control
			SmsTimeControl aControl=(SmsTimeControl)controlList.get(i);
			log.debug("aControl.getSmsTimeControlDay(): "+CommonUtils.NullChecker(aControl.getSmsTimeControlDay(), String.class).toString());
			log.debug("Integer.toString(dayOfWeek): "+Integer.toString(dayOfWeek));
			if(CommonUtils.NullChecker(aControl.getSmsTimeControlDay(), String.class).toString().contains(Integer.toString(dayOfWeek))){// today is in controlled
				String[] timeFrom=CommonUtils.NullChecker(aControl.getSmsTimeControlTimeFrom(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
				String[] timeTo=CommonUtils.NullChecker(aControl.getSmsTimeControlTimeTo(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
				if(Integer.valueOf(hourMin)<Integer.valueOf(timeFrom[dayOfWeek-1])||Integer.valueOf(hourMin)>Integer.valueOf(timeTo[dayOfWeek-1])){
					onHoldMap.put(aControl.getDept(), !aControl.getAutoPatchToUnsent());
				}
			}else{
				// Skip, no control needed
			}
		}

		return onHoldMap;
	}
}
