package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.util.AppDBConfig;
import com.bcb.crsmsg.util.Constants;

public class TblRefDao extends HibernateDaoSupport implements ITblRefDao {

	private static final Log log = LogManager.getLog();

	public void deleteSmsType(int smsTypeId){
		getHibernateTemplate().bulkUpdate("delete from TblRef where tblRefId = ?", smsTypeId);
		getHibernateTemplate().bulkUpdate("delete from SmsMQ where smsType = ?", smsTypeId);
	}
	
	public boolean isSmsTypeInUsed(int smsTypeId){
		return ((Integer)getHibernateTemplate().
				find("select count(o.smsType) from UserChannel o where o.smsType.tblRefId = ?", smsTypeId).get(0)) > 0 ? true : false;
	}
	
	@SuppressWarnings("unchecked")
	public void updateSmsType(TblRef smsType) throws Exception{
		String[] svcProviders = StringUtils.split(smsType.getRoutableTelco(), "|");
		Integer[] svcProvidersInt = new Integer[svcProviders.length];
		List<Integer> ids = new ArrayList<Integer>();
		for(int i=0; i < svcProviders.length; i++){
			svcProvidersInt[i] = Integer.parseInt(svcProviders[i]);
			ids.add(Integer.parseInt(svcProviders[i]));
		}
		
		Session session = getSession();
		try{
			session.beginTransaction();
			session.saveOrUpdate(smsType);
			session.createQuery("delete from SmsMQ where telcoId not in (:list) and smsType = :type").setParameter("type", smsType.getTblRefId()).
			setParameterList("list", svcProvidersInt).executeUpdate();
			
			List<SmsMQ> mqCfgs = session.createQuery("from SmsMQ where smsType = ?").setParameter(0, smsType.getTblRefId()).list();
			Map<Integer, SmsMQ> mqCfgsStore = new HashMap<Integer, SmsMQ>();
			for(SmsMQ mqCfg : mqCfgs){
				mqCfgsStore.put(mqCfg.getTelcoId(), mqCfg);
			}
			
			for(String telcoId : svcProviders){
				if( !mqCfgsStore.containsKey(Integer.parseInt(telcoId)) ){
					log.info("Creating mq config for sms type id: " + smsType.getTblRefId() + " telco: " + telcoId);
					SmsMQ mq = new SmsMQ();
					mq.setCreatedBy(Constants.DB_CONSTANTS_CREATED_BY_SYSTEM);
					mq.setCreatedDatetime(new Date());
					mq.setNoOfThread(2);
					mq.setSmsType(smsType.getTblRefId());
					mq.setTelcoId(Integer.parseInt(telcoId));
					session.persist(mq);
				}
			}
			session.getTransaction().commit();			
		}catch(Exception e){
			session.getTransaction().rollback();
			log.info("Error while updating sms type. Reason:" + e.getMessage(), e);
			throw e;
		}finally{
			if(session != null && session.isOpen()){
				releaseSession(session);
			}
		}
	}
	
	public List<TblRef> getSmsTypes(){
		Session session = getSession();
		try{
			Query query = session.createQuery("from TblRef where tblName='sms_mq' and colName='sms_type' and status='Active' order by value");
			List<TblRef> refs = query.list();
			if(refs != null && !refs.isEmpty()){
				return refs;
			}	
			return null;
		}catch(Exception e){
			log.error("Error caught. Due to: " + e.getMessage(), e);
			return null;
		}finally{
			if(session != null && session.isOpen()) releaseSession(session);
		}		
	}
	
	public Integer getSize(String label){
		return (Integer)getHibernateTemplate().find("select count (o) from TblRef o where o.label = ?", label).get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<TblRef> getAllSMSTypes(){
		return getHibernateTemplate().find("from TblRef order by value");
	}
	
	@SuppressWarnings("unchecked")
	public TblRef selectSMSType(String userId, String channelId){
		TblRef smsType = null;
		Session session = getSession();
		List<UserChannel> userChannels;
		try{
			session.beginTransaction();
			if(StringUtils.isNotEmpty(channelId)){
				Query query = session.createQuery("select o from UserChannel o where o.userChannelIdObj.userId = ? and o.userChannelIdObj.channelId = ? and o.status='Active'");
				query.setParameter(0, userId).setParameter(1, channelId);
				userChannels = query.list();
				if(userChannels != null && !userChannels.isEmpty()){
					smsType = userChannels.get(0).getSmsType();
				}else{
					smsType = selectSMSType(userId, session);
				}
			}
			//no channel id
			if(smsType == null){
				smsType = selectSMSType(userId, session);
			}
			session.getTransaction().commit();
			return smsType;
		}catch(Exception e){
			session.getTransaction().rollback();
			throw new RuntimeException(e.getMessage(), e);
		}finally{
			if(session.isOpen()) releaseSession(session);
		}
	}			
	
	@SuppressWarnings("unchecked")
	private TblRef selectSMSType(String userId, Session session) throws Exception{
		Query query = session.createQuery("select o from UserChannel o where o.userChannelIdObj.userId = ? and o.status='Active'").setParameter(0, userId);
		List<UserChannel> userChannels = query.list();
		if(userChannels != null && userChannels.size() == 1){
			return userChannels.get(0).getSmsType();
		}
		return AppDBConfig.getInstance().getDefaultSmsType();
	}
	
}
