package com.bcb.crsmsg.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;



public class TelcoESmsDao extends HibernateDaoSupport implements ITelcoEsmsDao {

	private static final Log log = LogFactory.getLog(TelcoESmsDao.class);
	
	public List<Telco> findAll()
	{
		List<Telco> list= getHibernateTemplate().find("from Telco");
		return list;
	}
	
	public List<Option> findAllOption()
	{
		List<Option> list= getHibernateTemplate().find("select new Option(o.telcoId,o.telcoName) from telco o");
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Telco> findTelcos(String... status) {
		Session session = getSession();
		Query query = session.createQuery("from Telco where status in (:status)");
		query.setParameterList("status", status);
		Transaction txn = session.getTransaction();
		try{
			txn.begin();
			List<Telco> telcos =  query.list();
			txn.commit();
			return telcos;
		}catch(Exception e){
			txn.rollback();
			return new ArrayList<Telco>();
		}finally{
			if(session.isOpen())session.close();
		}
	}	

	public List<Telco> findAllTelcoEsmsAvailable() {
		final String queryString = "from Telco where status=?";
		return getHibernateTemplate().find(queryString, Constants.STATUS_ACTIVE);
	}

	public List<Telco> findAllTelcoEsmsAvailableByPriority(
			Integer priorityValue) {
		return getHibernateTemplate().find("from Telco where priority=? and status=?",new Object[]{priorityValue, Constants.STATUS_ACTIVE});	
	}
	
	public List<Telco> findAllTelcoEsmsAvailableOrderByCongestion() {
		return getHibernateTemplate().find("from Telco where status=? order by smsOnHand", Constants.STATUS_ACTIVE);	
	}
	
	public List<Telco> findAllTelcoEsmsAvailableOrderByCost() {
		return getHibernateTemplate().find("from Telco where status=? order by costPerSmsInter, costPerSmsWithin", Constants.STATUS_ACTIVE);	
	}
	
	public List<Telco> findAllTelcoEsmsAvailableByPrefix() {
		return getHibernateTemplate().find("from Telco where status=? order by priority, smsOnHand", Constants.STATUS_ACTIVE);
	}
	
	@SuppressWarnings("unchecked")
	public List<Telco> findAllTelcoEsmsAvailableOrderByPriority() {
		return getHibernateTemplate().find("from Telco where status=? order by priority", Constants.STATUS_ACTIVE);	
	}
	
	public List<Telco> findAllTelcoEsmsAvailableByTelcoRoutingMode(String routingMode, String routingParam){
		if(routingMode.equals(Constants.FORM_FIELD_TELCO_ROUTING_CONGESTION)){
			return findAllTelcoEsmsAvailableOrderByCongestion();
		}else if(routingMode.equals(Constants.FORM_FIELD_TELCO_ROUTING_COST)){
			return findAllTelcoEsmsAvailableOrderByCost();
		}else if(routingMode.equals(Constants.FORM_FIELD_TELCO_ROUTING_PREFIX)){
			return findAllTelcoEsmsAvailableByPrefix();
		}else if(routingMode.equals(Constants.FORM_FIELD_TELCO_ROUTING_PRIORITY)){
			return findAllTelcoEsmsAvailableOrderByPriority();
		}
		return new ArrayList<Telco>();
	}

	@SuppressWarnings("unchecked")
	public List<Telco> findByIds(String...ids){
		Integer[] idsInt = new Integer[ids.length];
		for(int i=0; i<ids.length; i++){
			idsInt[i] = Integer.parseInt(ids[i]);
		}
		return getHibernateTemplate().findByNamedParam("from Telco where id in (:list)", "list", idsInt);
	}
	
	public List<Telco> findAvailableTelcoByIds(String... ids) {
		if(ids == null || ids.length == 0){
			return null;
		}
		
		List<Integer> intIds = new ArrayList<Integer>();
		for(String id : ids){
			intIds.add(Integer.parseInt(id));
		}
		
		Session session = getSession();
		Query query = session.createQuery("from Telco where status = :status and id in ( :ids )");
		query.setParameter("status", Constants.STATUS_ACTIVE);
		query.setParameterList("ids", intIds);
		Transaction txn = session.getTransaction();
		try{
			txn.begin();
			List<Telco> telcos =  query.list();
			if(telcos == null || telcos.isEmpty()) return new ArrayList<Telco>();
			
			Map<Integer, Telco> map = new HashMap<Integer, Telco>();
			for(Telco t : telcos){
				map.put(t.getId(), t);
			}
			
			telcos.clear();
			for(Integer id : intIds){
				if(map.containsKey(id)){
					telcos.add(map.get(id));
				}
			}
			txn.commit();
			return telcos;
		}catch(Exception e){
			log.error("Error while getting telco by ids. Due to: " + e.getMessage(), e);
			txn.rollback();
			return new ArrayList<Telco>();
		}finally{
			if(session.isOpen())session.close();
		}
	}	
	
	public void save(Telco obj) {
		getHibernateTemplate().save(obj);	
	}
	
	public void update(Telco obj) {
		getHibernateTemplate().update(obj);
	}

	public void initialiseAllTelco(String updateQueryStr, Object[] updateParam) {
		getHibernateTemplate().bulkUpdate(updateQueryStr, updateParam);
	}
	
	public Telco findById(int id) {
		return (Telco) getHibernateTemplate().get(Telco.class,id);
	}
	
	public Telco findAvailableTelcoById(int id) {
		List telcoList=getHibernateTemplate().find("from Telco where id=? and status=?", new Object[]{id, Constants.STATUS_ACTIVE});
		if(telcoList.size()==1){
			return (Telco)telcoList.get(0);
		}else{
			return null;
		}
	}
}
