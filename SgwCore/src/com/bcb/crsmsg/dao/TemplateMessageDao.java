package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.util.Constants;

public class TemplateMessageDao extends HibernateDaoSupport implements ITemplateMessageDao {
	public MsgTemplate getById(int id) {
		return (MsgTemplate)getHibernateTemplate().get(MsgTemplate.class, id);
	}
	
	public List findAvailabelTemplateByMsgCode(String code) {
		return (List)getHibernateTemplate().find("from MsgTemplate where templateCode=? and status=?", new Object[]{code, Constants.STATUS_ACTIVE});
	}
}
