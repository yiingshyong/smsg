package com.bcb.crsmsg.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.util.Constants;

public class UserChannelDao extends HibernateDaoSupport implements IUserChannel {
		
	@Override
	public List<UserChannel> getActiveUserChannels(String userId) {		
		return getUserChannels(userId, Constants.FORM_FIELD_STATUS_ACTIVE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserChannel> getUserChannels(String userId, String status){

		return getHibernateTemplate().find("from UserChannel o where o.userChannelIdObj.userId = ? and o.status = ? ", new Object[]{userId, status});
	}

	@SuppressWarnings("unchecked")
	public List<UserChannel> getAllUserChannels(String userId){

		return getHibernateTemplate().find("from UserChannel o where o.userChannelIdObj.userId = ? ", new Object[]{userId});
	}
}
