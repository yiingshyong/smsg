package com.bcb.crsmsg.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.UserRights;
import com.bcb.crsmsg.util.Constants;

public class UserDao extends HibernateDaoSupport implements IUserDao {

	Log log = LogManager.getLog();

	public User findByUserId(final String userId) {
		
		HibernateCallback callback = new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria crit = session.createCriteria(User.class);
				crit.add(Expression.eq("userId",userId));
				crit.setMaxResults(1);
				return crit.uniqueResult();
			}
		};
		return (User) getHibernateTemplate().execute(callback);

	}
	
	@SuppressWarnings("unchecked")
	public List<User> findUserByIdLike(String userId){
		return getHibernateTemplate().find("from User where userId like ?", new Object[]{userId});
	}
	
	public boolean hasPermission(String userId, Integer permId){
		List result = getHibernateTemplate().find("select UR from UserRoles UR, PermRole PR, PermRef PRef where PRef.permValue = ? and UR.userId = ? and PR.permRef = PRef.permId and PR.roleRef.roleId = UR.roleId and UR.status='Active' ", new Object[]{permId, userId});
		if(result != null && !result.isEmpty()) return true;
		return false;
	}
	
	public boolean hasRights(String userId, int rightId){
		List<UserRights> rights = getHibernateTemplate().find("from UserRights where user.userId=? and rightsId=?", new Object[]{userId, rightId});
		return (rights != null && !rights.isEmpty());
	}

}
