package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;


/** @author Hibernate CodeGenerator */
public class ADSSynch implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5986718005611372171L;
	
	private int smsId;
	
    /** persistent field */
    private String accNo;

    /** nullable persistent field */
    private int transferFtp;

    /** nullable persistent field */
    private String transferStatus;
    
    /** nullable persistent field */
    private Date transferDatetime;

    /** full constructor */
    public ADSSynch(String mobileNo,Date smsCreatedDatetime,String accNo,int transferFtp,String transferStatus,Date transferDatetime) {
    	this.accNo=accNo;
    	this.transferFtp=transferFtp;
    	this.transferStatus=transferStatus;
    	this.transferDatetime=transferDatetime;
    }

    /** default constructor */
    public ADSSynch() {
    }

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public Date getTransferDatetime() {
		return transferDatetime;
	}

	public void setTransferDatetime(Date transferDatetime) {
		this.transferDatetime = transferDatetime;
	}

	public int getTransferFtp() {
		return transferFtp;
	}

	public void setTransferFtp(int transferFtp) {
		this.transferFtp = transferFtp;
	}

	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	public int getSmsId() {
		return smsId;
	}

	public void setSmsId(int synchId) {
		this.smsId = synchId;
	}
}
