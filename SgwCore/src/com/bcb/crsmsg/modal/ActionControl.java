package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class ActionControl implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1421083281429382680L;

	/** identifier field */
    private int controlId;

    /** nullable persistent field */
    private Integer department;

    /** nullable persistent field */
    private Integer fileConversion;

    /** persistent field */
    private String createdBy;

    /** persistent field */
    private Date createdDate;

    /** persistent field */
    private Date createdTime;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDate;

    /** nullable persistent field */
    private Date modifiedTime;

    /** persistent field */
    private String status;

    /** full constructor */
    public ActionControl(Integer department, Integer fileConversion, String createdBy, Date createdDate, Date createdTime, String modifiedBy, Date modifiedDate, Date modifiedTime, String status) {
        this.department = department;
        this.fileConversion = fileConversion;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
        this.modifiedTime = modifiedTime;
        this.status = status;
    }

    /** default constructor */
    public ActionControl() {
    }

    /** minimal constructor */
    public ActionControl(String createdBy, Date createdDate, Date createdTime, String status) {
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.status = status;
    }

    public int getControlId() {
        return this.controlId;
    }

    public void setControlId(int controlId) {
        this.controlId = controlId;
    }

    public Integer getDepartment() {
        return this.department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public Integer getFileConversion() {
        return this.fileConversion;
    }

    public void setFileConversion(Integer fileConversion) {
        this.fileConversion = fileConversion;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedTime() {
        return this.createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getModifiedTime() {
        return this.modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("controlId", getControlId())
            .toString();
    }

}
