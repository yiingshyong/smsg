package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class AutoAction implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5849079043263395787L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String actionClassName;

    /** nullable persistent field */
    private Integer actionSequence;

    /** nullable persistent field */
    private Integer keyword;
    
    /** nullable persistent field */
    private String defaultAction;
    
    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;
    
    private String description;
    private String serverIP;
    private int serverPort;
    private int serverResponseTimeout;
    private int maxFailureAttempt;
    private String svcWindow;
    private String successCode;
    private String failureCode;
    private String dataAge; 
    private String unavailabilitySms;
	private String parameterKey;
	
    /** full constructor */
    public AutoAction(String actionClassName, Integer actionSequence, Integer keyword) {
        this.actionClassName = actionClassName;
        this.actionSequence = actionSequence;
        this.keyword = keyword;
    }

    public String getUnavailabilitySms() {
		return unavailabilitySms;
	}

	public void setUnavailabilitySms(String unavailabilitySms) {
		this.unavailabilitySms = unavailabilitySms;
	}

	public String getParameterKey() {
		return parameterKey;
	}

	public void setParameterKey(String parameterKey) {
		this.parameterKey = parameterKey;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public int getServerResponseTimeout() {
		return serverResponseTimeout;
	}

	public void setServerResponseTimeout(int serverResponseTimeout) {
		this.serverResponseTimeout = serverResponseTimeout;
	}

	public int getMaxFailureAttempt() {
		return maxFailureAttempt;
	}

	public void setMaxFailureAttempt(int maxFailureAttempt) {
		this.maxFailureAttempt = maxFailureAttempt;
	}

	public String getSvcWindow() {
		return svcWindow;
	}

	public void setSvcWindow(String svcWindow) {
		this.svcWindow = svcWindow;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}

	public String getFailureCode() {
		return failureCode;
	}

	public void setFailureCode(String failureCode) {
		this.failureCode = failureCode;
	}

	public String getDataAge() {
		return dataAge;
	}

	public void setDataAge(String dataAge) {
		this.dataAge = dataAge;
	}

	/** default constructor */
    public AutoAction() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActionClassName() {
        return this.actionClassName;
    }

    public void setActionClassName(String actionClassName) {
        this.actionClassName = actionClassName;
    }

    public Integer getActionSequence() {
        return this.actionSequence;
    }

    public void setActionSequence(Integer actionSequence) {
        this.actionSequence = actionSequence;
    }

    public Integer getKeyword() {
        return this.keyword;
    }

    public void setKeyword(Integer keyword) {
        this.keyword = keyword;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public String getDefaultAction() {
		return defaultAction;
	}

	public void setDefaultAction(String defaultAction) {
		this.defaultAction = defaultAction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	
	

}
