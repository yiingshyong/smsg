package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

public class BatchSetup implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 7465705938864315017L;
	
	/** identifier field */
    private Integer id;
    
	/** nullable persistent field */
    private String fileName;
    
    /** nullable persistent field */
    private Integer messageTemplate;
    
    /** nullable persistent field */
    private String fileDelimiter;
    
    /** nullable persistent field */
    private String smsScheduled;
    
    /** nullable persistent field */
    private Date smsScheduledTime;
    
    /** nullable persistent field */
    private String smsAutoApproval;
    
    /** nullable persistent field */
    private String notificationSetupReceiver;

    /** nullable persistent field */
    private String notificationSetupSender;

    /** nullable persistent field */
    private String notificationSetupStatus;
    
	public String getNotificationSetupReceiver() {
		return notificationSetupReceiver;
	}

	public void setNotificationSetupReceiver(String notificationSetupReceiver) {
		this.notificationSetupReceiver = notificationSetupReceiver;
	}

	public String getNotificationSetupSender() {
		return notificationSetupSender;
	}

	public void setNotificationSetupSender(String notificationSetupSender) {
		this.notificationSetupSender = notificationSetupSender;
	}

	public String getNotificationSetupStatus() {
		return notificationSetupStatus;
	}

	public void setNotificationSetupStatus(String notificationSetupStatus) {
		this.notificationSetupStatus = notificationSetupStatus;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(Integer messageTemplate) {
		this.messageTemplate = messageTemplate;
	}

	public String getFileDelimiter() {
		return fileDelimiter;
	}

	public void setFileDelimiter(String fileDelimiter) {
		this.fileDelimiter = fileDelimiter;
	}

	public String getSmsScheduled() {
		return smsScheduled;
	}

	public void setSmsScheduled(String smsScheduled) {
		this.smsScheduled = smsScheduled;
	}

	public Date getSmsScheduledTime() {
		return smsScheduledTime;
	}

	public void setSmsScheduledTime(Date smsScheduledTime) {
		this.smsScheduledTime = smsScheduledTime;
	}

	public String getSmsAutoApproval() {
		return smsAutoApproval;
	}

	public void setSmsAutoApproval(String smsAutoApproval) {
		this.smsAutoApproval = smsAutoApproval;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
