package com.bcb.crsmsg.modal;

public class BillingReportDetailModal implements java.io.Serializable {

	private static final long serialVersionUID = -7850424907581517994L;
	private Integer id;
	private int version;
	private String date;
	private String mobileNo;
	private String message;
	private String sentBy;
	private String dept;
	private String scheduled;
	private String scheduledStatus;
	private String operator;
	private String mode;
	private String type;
	private String remarks;
	private String seq;
	private String deliveryReportDate;
	private String created;
	private String priority;
	private String approval;
	private String approvalDatetime;
	private String status;
	private String cost;

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSentBy() {
		return this.sentBy;
	}

	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	public String getDept() {
		return this.dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}



	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getMode() {
		return this.mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	/**
	 * @return Returns the deliveryReportDate.
	 */
	public String getDeliveryReportDate() {
		return deliveryReportDate;
	}

	/**
	 * @param deliveryReportDate The deliveryReportDate to set.
	 */
	public void setDeliveryReportDate(String deliveryReportDate) {
		this.deliveryReportDate = deliveryReportDate;
	}

	/**
	 * @return Returns the date.
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date The date to set.
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return Returns the scheduled.
	 */
	public String getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled The scheduled to set.
	 */
	public void setScheduled(String scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return Returns the seq.
	 */
	public String getSeq() {
		return seq;
	}

	/**
	 * @param seq The seq to set.
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getScheduledStatus() {
		return scheduledStatus;
	}

	public void setScheduledStatus(String scheduledStatus) {
		this.scheduledStatus = scheduledStatus;
	}

	public String getApprovalDatetime() {
		return approvalDatetime;
	}

	public void setApprovalDatetime(String approvalDatetime) {
		this.approvalDatetime = approvalDatetime;
	}

	/**
	 * @return Returns the cost.
	 */
	public String getCost() {
		return cost;
	}

	/**
	 * @param cost The cost to set.
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}

}
