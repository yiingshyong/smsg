package com.bcb.crsmsg.modal;

public class BillingReportHeaderModal implements java.io.Serializable {

	private static final long serialVersionUID = 2943796643922741249L;
	private String header;
	private String totalCharge_SMSSent;
	private String totalCharge_Cost;
	private String totalCharge_Gst;
	private String totalCharge_All;
	
	/**
	 * @return Returns the header.
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header The header to set.
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * @return Returns the totalCharge_Cost.
	 */
	public String getTotalCharge_Cost() {
		return totalCharge_Cost;
	}

	/**
	 * @param totalCharge_Cost The totalCharge_Cost to set.
	 */
	public void setTotalCharge_Cost(String totalCharge_Cost) {
		this.totalCharge_Cost = totalCharge_Cost;
	}

	/**
	 * @return Returns the totalCharge_SMSSent.
	 */
	public String getTotalCharge_SMSSent() {
		return totalCharge_SMSSent;
	}

	/**
	 * @param totalCharge_SMSSent The totalCharge_SMSSent to set.
	 */
	public void setTotalCharge_SMSSent(String totalCharge_SMSSent) {
		this.totalCharge_SMSSent = totalCharge_SMSSent;
	}

	public String getTotalCharge_Gst() {
		return totalCharge_Gst;
	}

	public void setTotalCharge_Gst(String totalCharge_Gst) {
		this.totalCharge_Gst = totalCharge_Gst;
	}

	public String getTotalCharge_All() {
		return totalCharge_All;
	}

	public void setTotalCharge_All(String totalCharge_All) {
		this.totalCharge_All = totalCharge_All;
	}
	
}
