package com.bcb.crsmsg.modal;



public class BillingReportSummaryByTelcoModal implements java.io.Serializable {

	private static final long serialVersionUID = 5575219208553457045L;
	private String totalCharge;

	/**
	 * @return Returns the totalCharge.
	 */
	public String getTotalCharge() {
		return totalCharge;
	}
	/**
	 * @param totalCharge The totalCharge to set.
	 */
	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}
	
	
	
}
