package com.bcb.crsmsg.modal;

import java.util.ArrayList;

public class BillingReportSummaryModal implements java.io.Serializable {

	private static final long serialVersionUID = 5575219208553457045L;
	private String totalCharge;
	private int seq;
	private String dept;
	private ArrayList telcoModal;
	private String userId;
	private String channelId;
	
	/**
	 * @return Returns the dept.
	 */
	public String getDept() {
		return dept;
	}

	/**
	 * @param dept The dept to set.
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}

	/**
	 * @return Returns the totalCharge.
	 */
	public String getTotalCharge() {
		return totalCharge;
	}

	/**
	 * @param totalCharge The totalCharge to set.
	 */
	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	/**
	 * @return Returns the seq.
	 */
	public int getSeq() {
		return seq;
	}

	/**
	 * @param seq The seq to set.
	 */
	public void setSeq(int seq) {
		this.seq = seq;
	}

	/**
	 * @return Returns the telcoModal.
	 */
	public ArrayList getTelcoModal() {
		return telcoModal;
	}

	/**
	 * @param telcoModal The telcoModal to set.
	 */
	public void setTelcoModal(ArrayList telcoModal) {
		this.telcoModal = telcoModal;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	

}
