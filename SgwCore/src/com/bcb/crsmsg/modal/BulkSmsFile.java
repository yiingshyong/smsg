package com.bcb.crsmsg.modal;

import java.io.FileInputStream;
import java.sql.Blob;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class BulkSmsFile extends BatchSetup {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2119942741009014063L;

	/** identifier field */
    private Integer id;
    
    /** nullable persistent field */
    private Integer bulkId;
    
    /** nullable persistent field */
    private byte[] content;
    	
    /** nullable persistent field */
    private String bulkFileStatus;
    
    /** nullable persistent field */
    private String fileName;
    
    /** nullable persistent field */
    private String uploadPath;
    
    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

	public BulkSmsFile(Integer id,Integer bulkId, byte[] content, String bulkFileStatus, String fileName,String uploadPath,
			String createdBy, Date createdDatetime) {
		super();
		this.id = id;
		this.bulkId = bulkId;
		this.content = content;
		this.bulkFileStatus = bulkFileStatus;
		this.fileName = fileName;
		this.uploadPath = uploadPath;
		this.createdBy = createdBy;
		this.createdDatetime = createdDatetime;
	}
    /** default constructor */
    public BulkSmsFile() {
    }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBulkId() {
		return bulkId;
	}
	public void setBulkId(Integer bulkId) {
		this.bulkId = bulkId;
	}
	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getBulkFileStatus() {
		return bulkFileStatus;
	}

	public void setBulkFileStatus(String bulkFileStatus) {
		this.bulkFileStatus = bulkFileStatus;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getUploadPath() {
		return uploadPath;
	}
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}




}
