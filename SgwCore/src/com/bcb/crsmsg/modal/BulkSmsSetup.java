package com.bcb.crsmsg.modal;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class BulkSmsSetup extends BatchSetup {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2119942741009014063L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String approvalBy;

    /** nullable persistent field */
    private Date approvalDatetime;

    /** nullable persistent field */
    private String bulkFile;

    /** nullable persistent field */
    private String fileName;
    
    /** nullable persistent field */
    private String bulkName;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private String dept;

    /** nullable persistent field */
    private Integer fileConversion;

    /** nullable persistent field */
    private String bulkFileMode;

    /** nullable persistent field */
    private String fileDelimiter;

    /** nullable persistent field */
    private String message;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String msgFormatType;

    /** nullable persistent field */
    private String msgVerification;

    /** nullable persistent field */
    private Integer priority;

    /** nullable persistent field */
    private String remarks;

    /** nullable persistent field */
    private String sentBy;

    /** nullable persistent field */
    private String setupStatus;

    /** nullable persistent field */
    private String smsScheduled;

    /** nullable persistent field */
    private Date smsScheduledTime;

    /** nullable persistent field */
    private String smsStatus;

    /** nullable persistent field */
    private String smsStatusBy;

    /** nullable persistent field */
    private Date smsStatusDatetime;

    /** nullable persistent field */
    private Integer telco;

    /** nullable persistent field */
    private String type;

    /** full constructor */
    public BulkSmsSetup(Integer id, String approvalBy, Date approvalDatetime, String bulkFile, String fileName, String bulkName, String createdBy, Date createdDatetime, String dept, Integer fileConversion, String fileDelimiter, String message, String modifiedBy, Date modifiedDatetime, String msgFormatType, String msgVerification, Integer priority, String remarks, String sentBy, String setupStatus, String smsScheduled, Date smsScheduledTime, String smsStatus, String smsStatusBy, Date smsStatusDatetime, Integer telco, String type) {
        this.id = id;
        this.approvalBy = approvalBy;
        this.approvalDatetime = approvalDatetime;
        this.bulkFile = bulkFile;
        this.fileName=fileName;
        this.bulkName = bulkName;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.dept = dept;
        this.fileConversion = fileConversion;
        this.fileDelimiter = fileDelimiter;
        this.message = message;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.msgFormatType = msgFormatType;
        this.msgVerification = msgVerification;
        this.priority = priority;
        this.remarks = remarks;
        this.sentBy = sentBy;
        this.setupStatus = setupStatus;
        this.smsScheduled = smsScheduled;
        this.smsScheduledTime = smsScheduledTime;
        this.smsStatus = smsStatus;
        this.smsStatusBy = smsStatusBy;
        this.smsStatusDatetime = smsStatusDatetime;
        this.telco = telco;
        this.type = type;
    }

    /** default constructor */
    public BulkSmsSetup() {
    }

    /** minimal constructor */
    public BulkSmsSetup(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApprovalBy() {
        return this.approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public Date getApprovalDatetime() {
        return this.approvalDatetime;
    }

    public void setApprovalDatetime(Date approvalDatetime) {
        this.approvalDatetime = approvalDatetime;
    }

    public String getBulkFile() {
        return this.bulkFile;
    }

    public void setBulkFile(String bulkFile) {
        this.bulkFile = bulkFile;
    }

    public String getBulkName() {
        return this.bulkName;
    }

    public void setBulkName(String bulkName) {
        this.bulkName = bulkName;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getDept() {
        return this.dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Integer getFileConversion() {
        return this.fileConversion;
    }

    public void setFileConversion(Integer fileConversion) {
        this.fileConversion = fileConversion;
    }

    public String getFileDelimiter() {
        return this.fileDelimiter;
    }

    public void setFileDelimiter(String fileDelimiter) {
        this.fileDelimiter = fileDelimiter;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getMsgFormatType() {
        return this.msgFormatType;
    }

    public void setMsgFormatType(String msgFormatType) {
        this.msgFormatType = msgFormatType;
    }

    public String getMsgVerification() {
        return this.msgVerification;
    }

    public void setMsgVerification(String msgVerification) {
        this.msgVerification = msgVerification;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentBy() {
        return this.sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getSetupStatus() {
        return this.setupStatus;
    }

    public void setSetupStatus(String setupStatus) {
        this.setupStatus = setupStatus;
    }

    public String getSmsScheduled() {
        return this.smsScheduled;
    }

    public void setSmsScheduled(String smsScheduled) {
        this.smsScheduled = smsScheduled;
    }

    public Date getSmsScheduledTime() {
        return this.smsScheduledTime;
    }

    public void setSmsScheduledTime(Date smsScheduledTime) {
        this.smsScheduledTime = smsScheduledTime;
    }

    public String getSmsStatus() {
        return this.smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String getSmsStatusBy() {
        return this.smsStatusBy;
    }

    public void setSmsStatusBy(String smsStatusBy) {
        this.smsStatusBy = smsStatusBy;
    }

    public Date getSmsStatusDatetime() {
        return this.smsStatusDatetime;
    }

    public void setSmsStatusDatetime(Date smsStatusDatetime) {
        this.smsStatusDatetime = smsStatusDatetime;
    }

    public Integer getTelco() {
        return this.telco;
    }

    public void setTelco(Integer telco) {
        this.telco = telco;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public String getBulkFileMode() {
		return bulkFileMode;
	}

	public void setBulkFileMode(String bulkFileMode) {
		this.bulkFileMode = bulkFileMode;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
