package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Contact implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3235440726209902682L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String address;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Integer department;

    /** nullable persistent field */
    private String designation;

    /** nullable persistent field */
    private String mobileNo;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String name;

    /** nullable persistent field */
    private String refNo;

    /** nullable persistent field */
    private String type;

    /** full constructor */
    public Contact(String address, String createdBy, Date createdDatetime, Integer department, String designation, String mobileNo, String modifiedBy, Date modifiedDatetime, String name, String refNo, String type) {
        this.address = address;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.department = department;
        this.designation = designation;
        this.mobileNo = mobileNo;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.name = name;
        this.refNo = refNo;
        this.type = type;
    }

    /** default constructor */
    public Contact() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Integer getDepartment() {
        return this.department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getDesignation() {
        return this.designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefNo() {
        return this.refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
