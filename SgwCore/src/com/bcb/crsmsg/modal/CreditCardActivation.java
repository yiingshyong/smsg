package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

/** @author Hibernate CodeGenerator */
public class CreditCardActivation implements Serializable {

	private static final long serialVersionUID = 7593862789188196705L;
	public static final String CC_ACTIVATION_STATUS_SUCCEED = "Y";
	public static final String CC_ACTIVATION_STATUS_FAILED = "F";
	public static final String CC_ACTIVATION_STATUS_NOT_ACTIVATED = "P";	
	private int id;
	private int ftpReportId;
	private int smsReceivedId;
	private String cardNo;
	private String mobileNo;
	private String customerId;
	private int failureAttemptCount;
	private String status;
	private Date createdDatetime;
	private Date lastModifiedDatetime;
	
	
	public CreditCardActivation(int id, int ftpReportId, int smsReceivedId,
			String cardNo, String mobileNo, String customerId,
			int failureAttemptCount, String status, Date createdDatetime,
			Date lastModifiedDatetime) {
		super();
		this.id = id;
		this.ftpReportId = ftpReportId;
		this.smsReceivedId = smsReceivedId;
		this.cardNo = cardNo;
		this.mobileNo = mobileNo;
		this.customerId = customerId;
		this.failureAttemptCount = failureAttemptCount;
		this.status = status;
		this.createdDatetime = createdDatetime;
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	
	public CreditCardActivation() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFtpReportId() {
		return ftpReportId;
	}
	public void setFtpReportId(int ftpReportId) {
		this.ftpReportId = ftpReportId;
	}
	public int getSmsReceivedId() {
		return smsReceivedId;
	}
	public void setSmsReceivedId(int smsReceivedId) {
		this.smsReceivedId = smsReceivedId;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public int getFailureAttemptCount() {
		return failureAttemptCount;
	}
	public void setFailureAttemptCount(int failureAttemptCount) {
		this.failureAttemptCount = failureAttemptCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Date getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Date lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
}
