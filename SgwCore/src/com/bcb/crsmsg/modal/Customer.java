package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Customer implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8058324976863159826L;

	/** identifier field */
    private int id;

    /** nullable persistent field */
    private String custRefNo;

    /** nullable persistent field */
    private String mobileNo;

    /** nullable persistent field */
    private String smsStatus;
    
    /** nullable persistent field */
    private String smsStatusBy;
    
    /** nullable persistent field */
    private Date smsStatusDatetime;
    
    /** nullable persistent field */
    private String transferStatus;
    
    /** nullable persistent field */
    private String createdBy;
    
    /** nullable persistent field */
    private Date createdDatetime;
    
    /** nullable persistent field */
    private String modifiedBy;
    
    /** nullable persistent field */
    private Date modifiedDatetime;

    /** full constructor */
    public Customer(String custRefNo, String mobileNo, String smsStatus) {
        this.custRefNo = custRefNo;
        this.mobileNo = mobileNo;
        this.smsStatus = smsStatus;
    }

    /** default constructor */
    public Customer() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustRefNo() {
        return this.custRefNo;
    }

    public void setCustRefNo(String custRefNo) {
        this.custRefNo = custRefNo;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getSmsStatus() {
        return this.smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public String getSmsStatusBy() {
		return smsStatusBy;
	}

	public void setSmsStatusBy(String smsStatusBy) {
		this.smsStatusBy = smsStatusBy;
	}

	public Date getSmsStatusDatetime() {
		return smsStatusDatetime;
	}

	public void setSmsStatusDatetime(Date smsStatusDatetime) {
		this.smsStatusDatetime = smsStatusDatetime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

}
