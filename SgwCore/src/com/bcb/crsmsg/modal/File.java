package com.bcb.crsmsg.modal;

import java.sql.Blob;
import javax.sql.rowset.serial.SerialBlob;

public class File implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 615926291097405052L;

	private int fileId;
	private int refNO;
	private String uploadBy;
	private String fileName;
	private String fileType;
	private String fileSize;
	private SerialBlob fileBin;
	
	public Blob getFileBin() {
		return fileBin;
	}
	public void setFileBin(SerialBlob fileBin) {
		this.fileBin = fileBin;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public int getRefNO() {
		return refNO;
	}
	public void setRefNO(int refNO) {
		this.refNO = refNO;
	}
	public String getUploadBy() {
		return uploadBy;
	}
	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int uploadId) {
		this.fileId = uploadId;
	}
	
	
}
