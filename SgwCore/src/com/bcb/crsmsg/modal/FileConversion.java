package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FileConversion implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3195494904622657663L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String conversionClassName;

    /** nullable persistent field */
    private String conversionName;

    /** persistent field */
    private String createdBy;

    /** persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDateTime;

    /** nullable persistent field */
    private Integer status;

    /** nullable persistent field */
    private Integer totalConversionThread;

    /** persistent field */
    private int version;
    
    /** persistent field */
    private String localFolder;

    /** persistent field */
    private String endWithFile;
    
    /** persistent field */
    private String defaultPriority;
    
    /** persistent field */
    private String batchSetupDao;
    
    /** full constructor */
    public FileConversion(String conversionClassName, String conversionName, String createdBy, Date createdDatetime, String modifiedBy, Date modifiedDateTime, Integer status, Integer totalConversionThread, int version, String localFolder, String endWithFile, String defaultPriority, String batchSetupDao) {
        this.conversionClassName = conversionClassName;
        this.conversionName = conversionName;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.modifiedBy = modifiedBy;
        this.modifiedDateTime = modifiedDateTime;
        this.status = status;
        this.totalConversionThread = totalConversionThread;
        this.version = version;
        this.localFolder=localFolder;
        this.endWithFile=endWithFile;
        this.defaultPriority=defaultPriority;
        this.batchSetupDao=batchSetupDao;
    }

    /** default constructor */
    public FileConversion() {
    }

    /** minimal constructor */
    public FileConversion(String createdBy, Date createdDatetime, int version) {
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConversionClassName() {
        return this.conversionClassName;
    }

    public void setConversionClassName(String conversionClassName) {
        this.conversionClassName = conversionClassName;
    }

    public String getConversionName() {
        return this.conversionName;
    }

    public void setConversionName(String conversionName) {
        this.conversionName = conversionName;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDateTime() {
        return this.modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTotalConversionThread() {
        return this.totalConversionThread;
    }

    public void setTotalConversionThread(Integer totalConversionThread) {
        this.totalConversionThread = totalConversionThread;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the localFolder.
	 */
	public String getLocalFolder() {
		return localFolder;
	}

	/**
	 * @param localFolder The localFolder to set.
	 */
	public void setLocalFolder(String localFolder) {
		this.localFolder = localFolder;
	}

	/**
	 * @return Returns the endWithFile.
	 */
	public String getEndWithFile() {
		return endWithFile;
	}

	/**
	 * @param endWithFile The endWithFile to set.
	 */
	public void setEndWithFile(String endWithFile) {
		this.endWithFile = endWithFile;
	}

	/**
	 * @return Returns the defaultPriority.
	 */
	public String getDefaultPriority() {
		return defaultPriority;
	}

	/**
	 * @param defaultPriority The defaultPriority to set.
	 */
	public void setDefaultPriority(String defaultPriority) {
		this.defaultPriority = defaultPriority;
	}

	public String getBatchSetupDao() {
		return batchSetupDao;
	}

	public void setBatchSetupDao(String batchSetupDao) {
		this.batchSetupDao = batchSetupDao;
	}

}
