package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FtpLog implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5868318357918790712L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private Integer fileConversion;

    /** nullable persistent field */
    private Date ftpEndTime;

    /** nullable persistent field */
    private String ftpFile;

    /** nullable persistent field */
    private Integer ftpSetup;

    /** nullable persistent field */
    private Date ftpStartTime;

    /** nullable persistent field */
    private String ftpStatus;

    /** nullable persistent field */
    private String remarks;

    /** full constructor */
    public FtpLog(Integer fileConversion, Date ftpEndTime, String ftpFile, Integer ftpSetup, Date ftpStartTime, String ftpStatus, String remarks) {
        this.fileConversion = fileConversion;
        this.ftpEndTime = ftpEndTime;
        this.ftpFile = ftpFile;
        this.ftpSetup = ftpSetup;
        this.ftpStartTime = ftpStartTime;
        this.ftpStatus = ftpStatus;
        this.remarks = remarks;
    }

    /** default constructor */
    public FtpLog() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFileConversion() {
        return this.fileConversion;
    }

    public void setFileConversion(Integer fileConversion) {
        this.fileConversion = fileConversion;
    }

    public Date getFtpEndTime() {
        return this.ftpEndTime;
    }

    public void setFtpEndTime(Date ftpEndTime) {
        this.ftpEndTime = ftpEndTime;
    }

    public String getFtpFile() {
        return this.ftpFile;
    }

    public void setFtpFile(String ftpFile) {
        this.ftpFile = ftpFile;
    }

    public Integer getFtpSetup() {
        return this.ftpSetup;
    }

    public void setFtpSetup(Integer ftpSetup) {
        this.ftpSetup = ftpSetup;
    }

    public Date getFtpStartTime() {
        return this.ftpStartTime;
    }

    public void setFtpStartTime(Date ftpStartTime) {
        this.ftpStartTime = ftpStartTime;
    }

    public String getFtpStatus() {
        return this.ftpStatus;
    }

    public void setFtpStatus(String ftpStatus) {
        this.ftpStatus = ftpStatus;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
