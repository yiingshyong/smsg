package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FtpReport implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8635214170174933130L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private int deptId;
    
    /** nullable persistent field */
    private String dept;

    /** nullable persistent field */
    private Date endDate;

    /** nullable persistent field */
    private Date endTime;

    /** nullable persistent field */
    private String fileName;
    
    /** nullable persistent field */
    private String filePath;

    /** nullable persistent field */
    private Integer noFailed;

    /** nullable persistent field */
    private Integer noSuccess;

    /** nullable persistent field */
    private String remark;

    /** nullable persistent field */
    private Date startDate;

    /** nullable persistent field */
    private Date startTime;

    /** nullable persistent field */
    private String type;
    
    /** nullable persistent field */
    private int ftpSetup;
    
    private Integer sent;
	private Integer unsent;
	private Integer pendingApp;
	private Integer pendingVer;
	private Integer queue;
	private Integer total;
	private String filteredFileName;
	
 

	/** full constructor */
    public FtpReport(String createdBy, String dept, Date endDate, Date endTime, String fileName, Integer noFailed, Integer noSuccess, String remark, Date startDate, Date startTime, String filePath, String type) {
        this.createdBy = createdBy;
        this.dept = dept;
        this.endDate = endDate;
        this.endTime = endTime;
        this.fileName = fileName;
        this.noFailed = noFailed;
        this.noSuccess = noSuccess;
        this.remark = remark;
        this.startDate = startDate;
        this.startTime = startTime;
        this.filePath=filePath;
        this.type=type;
        this.pendingApp= pendingApp;
        this.pendingVer= pendingVer;
        this.unsent= unsent;
        this.sent= sent;
        this.queue= queue;
    }

    /** default constructor */
    public FtpReport() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDept() {
        return this.dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getNoFailed() {
        return this.noFailed;
    }

    public void setNoFailed(Integer noFailed) {
        this.noFailed = noFailed;
    }

    public Integer getNoSuccess() {
        return this.noSuccess;
    }

    public void setNoSuccess(Integer noSuccess) {
        this.noSuccess = noSuccess;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPendingApp() {
		return pendingApp;
	}

	public void setPendingApp(Integer pendingApp) {
		this.pendingApp = pendingApp;
	}

	public Integer getPendingVer() {
		return pendingVer;
	}

	public void setPendingVer(Integer pendingVer) {
		this.pendingVer = pendingVer;
	}

	public Integer getQueue() {
		return queue;
	}

	public void setQueue(Integer queue) {
		this.queue = queue;
	}

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Integer getUnsent() {
		return unsent;
	}

	public void setUnsent(Integer unsent) {
		this.unsent = unsent;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public int getFtpSetup() {
		return ftpSetup;
	}

	public void setFtpSetup(int ftpSetup) {
		this.ftpSetup = ftpSetup;
	}

	public String getFilteredFileName() {
		if(this.fileName != null && this.fileName.length()>0)
		{
			String format = "yyMMddHHmmssSSS";
			String separator = ".";
			String[] parts = StringUtils.split(this.fileName, separator);
			if(parts.length >= 2 && parts[0].length() == format.length()){
				try{
					new SimpleDateFormat(format).parse(parts[0]);
					return StringUtils.substringAfter(fileName, separator);					
				}catch(ParseException pe){
					return fileName;
				}
			}
		}
		return this.fileName;
	}

	public void setFilteredFileName(String filteredFileName) {
		this.filteredFileName = filteredFileName;
	}
	

}
