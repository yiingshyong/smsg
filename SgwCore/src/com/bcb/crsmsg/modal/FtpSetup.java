package com.bcb.crsmsg.modal;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FtpSetup extends BatchSetup {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4020705003006214893L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Integer fileConversion;

    /** nullable persistent field */
    private String fileDelimiter;

    /** nullable persistent field */
    private String fileName;

    /** nullable persistent field */
    private String ftpDay;

    /** nullable persistent field */
    private String ftpDestDir;

    /** nullable persistent field */
    private String ftpMode;

    /** nullable persistent field */
    private String ftpMonth;
    
    /** nullable persistent field */
    private String ftpDayOfMonth;

    /** nullable persistent field */
    private String ftpHour;
    
    /** nullable persistent field */
    private String ftpHourFrom;
    
    /** nullable persistent field */
    private String ftpHourTo;
    
    /** nullable persistent field */
    private String ftpName;

    /** nullable persistent field */
    private String ftpPassword;

    /** nullable persistent field */
    private String ftpPort;

    /** nullable persistent field */
    private String ftpServer;

    /** nullable persistent field */
    private String ftpSourceDir;

    /** nullable persistent field */
    private String ftpTargetDir;

    /** nullable persistent field */
    private String ftpTime;

    /** nullable persistent field */
    private String ftpTimeMode;

    /** nullable persistent field */
    private String ftpUserName;

    /** nullable persistent field */
    private Integer messageTemplate;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String notificationSetupReceiver;

    /** nullable persistent field */
    private String notificationSetupSender;

    /** nullable persistent field */
    private String notificationSetupStatus;

    /** nullable persistent field */
    private String securityKey;

    /** nullable persistent field */
    private String securityKeyFolder;
    
    /** nullable persistent field */
    private String setupStatus;

    /** nullable persistent field */
    private String smsAutoApproval;

    /** nullable persistent field */
    private String smsScheduled;
    
    /** nullable persistent field */
    private Date smsScheduledTime;
    
    /** nullable persistent field */
    private String ftpOwner;
    
    /** nullable persistent field */
    private String ftpRetryNo;
    
    /** nullable persistent field */
    private String ftpRetryPeriod;
    
    /** nullable persistent field */
    private String ftpRetryStatus;

    /** nullable persistent field */
    private Date ftpLastDatetime;
    
    private String ftpDesc;
    
    private boolean removeFTPSrcFile;
    
    private boolean removeFTPLocalFile;
    
    private int reverseDay;

    /** full constructor */
    public FtpSetup(String createdBy, Date createdDatetime, Integer fileConversion, String fileDelimiter, String fileName, String ftpDay, String ftpDestDir, String ftpMode, String ftpDayOfMonth, String ftpMonth, String ftpName, String ftpPassword, String ftpPort, String ftpServer, String ftpSourceDir, String ftpTargetDir, String ftpTime, String ftpTimeMode, String ftpHour, String ftpHourFrom, String ftpHourTo, String ftpUserName, Integer messageTemplate, String modifiedBy, Date modifiedDatetime, String notificationSetupReceiver, String notificationSetupSender, String notificationSetupStatus, String securityKey, String securityKeyFolder, String setupStatus, String smsAutoApproval, String smsScheduled, Date smsScheduledTime, String owner, boolean removeFTPSrcFile, boolean removeFTPLocalFile, String ftpDesc, int reverseDay) {
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.fileConversion = fileConversion;
        this.fileDelimiter = fileDelimiter;
        this.fileName = fileName;
        this.ftpDay = ftpDay;
        this.ftpDestDir = ftpDestDir;
        this.ftpMode = ftpMode;
        this.ftpDayOfMonth=ftpDayOfMonth;
        this.ftpMonth = ftpMonth;
        this.ftpHour=ftpHour;
        this.ftpHourFrom=ftpHourFrom;
        this.ftpHourTo=ftpHourTo;
        this.ftpName = ftpName;
        this.ftpPassword = ftpPassword;
        this.ftpPort = ftpPort;
        this.ftpServer = ftpServer;
        this.ftpSourceDir = ftpSourceDir;
        this.ftpTargetDir = ftpTargetDir;
        this.ftpTime = ftpTime;
        this.ftpTimeMode = ftpTimeMode;
        this.ftpUserName = ftpUserName;
        this.messageTemplate = messageTemplate;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.notificationSetupReceiver = notificationSetupReceiver;
        this.notificationSetupSender = notificationSetupSender;
        this.notificationSetupStatus = notificationSetupStatus;
        this.securityKey = securityKey;
        this.securityKeyFolder = securityKeyFolder;
        this.setupStatus = setupStatus;
        this.smsAutoApproval = smsAutoApproval;
        this.smsScheduled=smsScheduled;
        this.smsScheduledTime = smsScheduledTime;
        this.ftpOwner=owner;
        this.removeFTPSrcFile = removeFTPSrcFile;
        this.removeFTPLocalFile = removeFTPLocalFile;
    	this.ftpDesc = ftpDesc;
    	this.reverseDay = reverseDay;
    }

    /** default constructor */
    public FtpSetup() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Integer getFileConversion() {
        return this.fileConversion;
    }

    public void setFileConversion(Integer fileConversion) {
        this.fileConversion = fileConversion;
    }

    public String getFileDelimiter() {
        return this.fileDelimiter;
    }

    public void setFileDelimiter(String fileDelimiter) {
        this.fileDelimiter = fileDelimiter;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFtpDay() {
        return this.ftpDay;
    }

    public void setFtpDay(String ftpDay) {
        this.ftpDay = ftpDay;
    }

    public String getFtpDestDir() {
        return this.ftpDestDir;
    }

    public void setFtpDestDir(String ftpDestDir) {
        this.ftpDestDir = ftpDestDir;
    }

    public String getFtpMode() {
        return this.ftpMode;
    }

    public void setFtpMode(String ftpMode) {
        this.ftpMode = ftpMode;
    }

    public String getFtpMonth() {
        return this.ftpMonth;
    }

    public void setFtpMonth(String ftpMonth) {
        this.ftpMonth = ftpMonth;
    }

    public String getFtpName() {
        return this.ftpName;
    }

    public void setFtpName(String ftpName) {
        this.ftpName = ftpName;
    }

    public String getFtpPassword() {
        return this.ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public String getFtpPort() {
        return this.ftpPort;
    }

    public void setFtpPort(String ftpPort) {
        this.ftpPort = ftpPort;
    }

    public String getFtpServer() {
        return this.ftpServer;
    }

    public void setFtpServer(String ftpServer) {
        this.ftpServer = ftpServer;
    }

    public String getFtpSourceDir() {
        return this.ftpSourceDir;
    }

    public void setFtpSourceDir(String ftpSourceDir) {
        this.ftpSourceDir = ftpSourceDir;
    }

    public String getFtpTargetDir() {
        return this.ftpTargetDir;
    }

    public void setFtpTargetDir(String ftpTargetDir) {
        this.ftpTargetDir = ftpTargetDir;
    }

    public String getFtpTime() {
        return this.ftpTime;
    }

    public void setFtpTime(String ftpTime) {
        this.ftpTime = ftpTime;
    }

    public String getFtpTimeMode() {
        return this.ftpTimeMode;
    }

    public void setFtpTimeMode(String ftpTimeMode) {
        this.ftpTimeMode = ftpTimeMode;
    }

    public String getFtpUserName() {
        return this.ftpUserName;
    }

    public void setFtpUserName(String ftpUserName) {
        this.ftpUserName = ftpUserName;
    }

    public Integer getMessageTemplate() {
        return this.messageTemplate;
    }

    public void setMessageTemplate(Integer messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getNotificationSetupReceiver() {
        return this.notificationSetupReceiver;
    }

    public void setNotificationSetupReceiver(String notificationSetupReceiver) {
        this.notificationSetupReceiver = notificationSetupReceiver;
    }

    public String getNotificationSetupSender() {
        return this.notificationSetupSender;
    }

    public void setNotificationSetupSender(String notificationSetupSender) {
        this.notificationSetupSender = notificationSetupSender;
    }

    public String getNotificationSetupStatus() {
        return this.notificationSetupStatus;
    }

    public void setNotificationSetupStatus(String notificationSetupStatus) {
        this.notificationSetupStatus = notificationSetupStatus;
    }

    public String getSecurityKey() {
        return this.securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public String getSetupStatus() {
        return this.setupStatus;
    }

    public void setSetupStatus(String setupStatus) {
        this.setupStatus = setupStatus;
    }

    public String getSmsAutoApproval() {
        return this.smsAutoApproval;
    }

    public void setSmsAutoApproval(String smsAutoApproval) {
        this.smsAutoApproval = smsAutoApproval;
    }

    public Date getSmsScheduledTime() {
        return this.smsScheduledTime;
    }

    public void setSmsScheduledTime(Date smsScheduledTime) {
        this.smsScheduledTime = smsScheduledTime;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the smsScheduled.
	 */
	public String getSmsScheduled() {
		return smsScheduled;
	}

	/**
	 * @param smsScheduled The smsScheduled to set.
	 */
	public void setSmsScheduled(String smsScheduled) {
		this.smsScheduled = smsScheduled;
	}

	/**
	 * @return Returns the ftpDayOfMonth.
	 */
	public String getFtpDayOfMonth() {
		return ftpDayOfMonth;
	}

	/**
	 * @param ftpDayOfMonth The ftpDayOfMonth to set.
	 */
	public void setFtpDayOfMonth(String ftpDayOfMonth) {
		this.ftpDayOfMonth = ftpDayOfMonth;
	}

	/**
	 * @return Returns the ftpHour.
	 */
	public String getFtpHour() {
		return ftpHour;
	}

	/**
	 * @param ftpHour The ftpHour to set.
	 */
	public void setFtpHour(String ftpHour) {
		this.ftpHour = ftpHour;
	}

	/**
	 * @return Returns the ftpHourFrom.
	 */
	public String getFtpHourFrom() {
		return ftpHourFrom;
	}

	/**
	 * @param ftpHourFrom The ftpHourFrom to set.
	 */
	public void setFtpHourFrom(String ftpHourFrom) {
		this.ftpHourFrom = ftpHourFrom;
	}

	/**
	 * @return Returns the ftpHourTo.
	 */
	public String getFtpHourTo() {
		return ftpHourTo;
	}

	/**
	 * @param ftpHourTo The ftpHourTo to set.
	 */
	public void setFtpHourTo(String ftpHourTo) {
		this.ftpHourTo = ftpHourTo;
	}

	public String getSecurityKeyFolder() {
		return securityKeyFolder;
	}

	public void setSecurityKeyFolder(String securityKeyFolder) {
		this.securityKeyFolder = securityKeyFolder;
	}

	public String getFtpOwner() {
		return ftpOwner;
	}

	public void setFtpOwner(String owner) {
		this.ftpOwner = owner;
	}

	public String getFtpRetryNo() {
		return ftpRetryNo;
	}

	public void setFtpRetryNo(String ftpRetryNo) {
		this.ftpRetryNo = ftpRetryNo;
	}

	public String getFtpRetryPeriod() {
		return ftpRetryPeriod;
	}

	public void setFtpRetryPeriod(String ftpRetryPeriod) {
		this.ftpRetryPeriod = ftpRetryPeriod;
	}

	public String getFtpRetryStatus() {
		return ftpRetryStatus;
	}

	public void setFtpRetryStatus(String ftpRetryStatus) {
		this.ftpRetryStatus = ftpRetryStatus;
	}

	public Date getFtpLastDatetime() {
		return ftpLastDatetime;
	}

	public void setFtpLastDatetime(Date ftpLastDatetime) {
		this.ftpLastDatetime = ftpLastDatetime;
	}

	public String getFtpDesc() {
		return ftpDesc;
	}

	public void setFtpDesc(String ftpDesc) {
		this.ftpDesc = ftpDesc;
	}

	public boolean isRemoveFTPSrcFile() {
		return removeFTPSrcFile;
	}

	public void setRemoveFTPSrcFile(boolean removeFTPSrcFile) {
		this.removeFTPSrcFile = removeFTPSrcFile;
	}

	public boolean isRemoveFTPLocalFile() {
		return removeFTPLocalFile;
	}

	public void setRemoveFTPLocalFile(boolean removeFTPLocalFile) {
		this.removeFTPLocalFile = removeFTPLocalFile;
	}

	public int getReverseDay() {
		return reverseDay;
	}

	public void setReverseDay(int reverseDay) {
		this.reverseDay = reverseDay;
	}

}
