package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;


/** @author Hibernate CodeGenerator */
public class HttpIpAddress implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5849079043263395787L;
	
	/** identifier field */
    private Integer id;

	private String ip;
	
	private String createdBy;
	
	private Date createdDatetime;
	
	private String modifiedBy;

    private Date modifiedDatetime;
    
    private String dept;
    
    private String system;	
	
	private String remarks;
	
	private String status;
	
    /** full constructor */
    public HttpIpAddress(Integer id, String ip, String createdBy, Date createdDatetime, String modifiedBy, Date modifiedDatetime, String dept, String system, String remarks, String status) {
    	this.id=id;
    	this.ip=ip;
    	this.createdBy=createdBy;
    	this.createdDatetime=createdDatetime;
    	this.modifiedBy=modifiedBy;
    	this.modifiedDatetime=modifiedDatetime;
    	this.dept=dept;
    	this.system=system;
    	this.remarks=remarks;
    	this.status=status;
    }

    /** default constructor */
    public HttpIpAddress() {
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    
}
