package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Keyword implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3783393027595623340L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String autoExpired;

    /** nullable persistent field */
    private Date expiredDate;

    /** nullable persistent field */
    private String autoReply;

    /** nullable persistent field */
    private String autoReplyMsg;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private String keywordName;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String ownership;

    /** nullable persistent field */
    private String redirectEmail;

    /** nullable persistent field */
    private String redirectMobileNo;

    /** nullable persistent field */
    private String redirectUrl;

    /** nullable persistent field */
    private String redirectUrlParam;
    
    /** nullable persistent field */
    private String status;
    
    /** nullable persistent field */
    private String charSet;
    
    private String maxRedirect_attempts;
    
	private String redirectInterval;
    
    private boolean encryption;
	
	private String encryptRedirectUrl;
    
    private transient SmsReceived smsReceive;
    /** full constructor */
    public Keyword(String autoExpired, Date autoExpiredDate, String autoReply, String autoReplyMsg, String createdBy, Date createdDatetime, String keywordName, String modifiedBy, Date modifiedDatetime, String ownership, String redirectEmail, String redirectMobileNo, String redirectUrl, String status, String charSet, String maxRedirect_attempts, String redirectInterval, boolean encryption, String encryptRedirectUrl) {
        this.autoExpired = autoExpired;
        this.expiredDate = autoExpiredDate;
        this.autoReply = autoReply;
        this.autoReplyMsg = autoReplyMsg;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.keywordName = keywordName;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.ownership = ownership;
        this.redirectEmail = redirectEmail;
        this.redirectMobileNo = redirectMobileNo;
        this.redirectUrl = redirectUrl;
        this.status = status;
        this.charSet = charSet;
        this.maxRedirect_attempts = maxRedirect_attempts;
        this.redirectInterval = redirectInterval;
        this.encryption = encryption;
		this.encryptRedirectUrl = encryptRedirectUrl;
    }

    /** default constructor */
    public Keyword() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAutoExpired() {
        return this.autoExpired;
    }

    public void setAutoExpired(String autoExpired) {
        this.autoExpired = autoExpired;
    }

    public Date getExpiredDate() {
        return this.expiredDate;
    }

    public void setExpiredDate(Date autoExpiredDate) {
        this.expiredDate = autoExpiredDate;
    }

    public String getAutoReply() {
        return this.autoReply;
    }

    public void setAutoReply(String autoReply) {
        this.autoReply = autoReply;
    }

    public String getAutoReplyMsg() {
        return this.autoReplyMsg;
    }

    public void setAutoReplyMsg(String autoReplyMsg) {
        this.autoReplyMsg = autoReplyMsg;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getKeywordName() {
        return this.keywordName;
    }

    public void setKeywordName(String keywordName) {
        this.keywordName = keywordName;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getOwnership() {
        return this.ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getRedirectEmail() {
        return this.redirectEmail;
    }

    public void setRedirectEmail(String redirectEmail) {
        this.redirectEmail = redirectEmail;
    }

    public String getRedirectMobileNo() {
        return this.redirectMobileNo;
    }

    public void setRedirectMobileNo(String redirectMobileNo) {
        this.redirectMobileNo = redirectMobileNo;
    }

    public String getRedirectUrl() {
        return this.redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the charSet.
	 */
	public String getCharSet() {
		return charSet;
	}

	/**
	 * @param charSet The charSet to set.
	 */
	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

	public String getRedirectUrlParam() {
		return redirectUrlParam;
	}

	public void setRedirectUrlParam(String redirectUrlParam) {
		this.redirectUrlParam = redirectUrlParam;
	}

	public boolean isEncryption() {
		return encryption;
	}

	public void setEncryption(boolean encryption) {
		this.encryption = encryption;
	}
	
	public String getEncryptRedirectUrl() {
        return this.encryptRedirectUrl;
    }

    public void setEncryptRedirectUrl(String encryptRedirectUrl) {
        this.encryptRedirectUrl = encryptRedirectUrl;
    }

	public SmsReceived getSmsReceive() {
		return smsReceive;
	}

	public void setSmsReceive(SmsReceived smsReceive) {
		this.smsReceive = smsReceive;
	}
	
	/**
	 * @return the maxRedirect_attempts
	 */
	public String getMaxRedirect_attempts() {
		return maxRedirect_attempts;
	}

	/**
	 * @param maxRedirect_attempts the maxRedirect_attempts to set
	 */
	public void setMaxRedirect_attempts(String maxRedirect_attempts) {
		this.maxRedirect_attempts = maxRedirect_attempts;
	}
	
	/**
	 * @return the redirectInterval
	 */
	public String getRedirectInterval() {
		return redirectInterval;
	}

	/**
	 * @param redirectInterval the redirectInterval to set
	 */
	public void setRedirectInterval(String redirectInterval) {
		this.redirectInterval = redirectInterval;
	}
	
	
}
