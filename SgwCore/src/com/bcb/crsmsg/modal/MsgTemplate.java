package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class MsgTemplate implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6974435076872312952L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Integer department;

    /** nullable persistent field */
    private String dynamicFieldName;

    /** nullable persistent field */
    private String dynamicFieldNo;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String status;

    /** nullable persistent field */
    private String templateCode;

    /** nullable persistent field */
    private String templateContent;

    /** nullable persistent field */
    private String type;
    
    /** nullable persistent field */
    private String msgFormatType;

    /** full constructor */
    public MsgTemplate(String createdBy, Date createdDatetime, Integer department, String dynamicFieldName, String dynamicFieldNo, String modifiedBy, Date modifiedDatetime, String status, String templateCode, String templateContent, String type, String msgFormatType) {
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.department = department;
        this.dynamicFieldName = dynamicFieldName;
        this.dynamicFieldNo = dynamicFieldNo;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.status = status;
        this.templateCode = templateCode;
        this.templateContent = templateContent;
        this.type = type;
        this.msgFormatType=msgFormatType;
    }

    /** default constructor */
    public MsgTemplate() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Integer getDepartment() {
        return this.department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getDynamicFieldName() {
        return this.dynamicFieldName;
    }

    public void setDynamicFieldName(String dynamicFieldName) {
        this.dynamicFieldName = dynamicFieldName;
    }

    public String getDynamicFieldNo() {
        return this.dynamicFieldNo;
    }

    public void setDynamicFieldNo(String dynamicFieldNo) {
        this.dynamicFieldNo = dynamicFieldNo;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTemplateCode() {
        return this.templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateContent() {
        return this.templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the msgFormatType.
	 */
	public String getMsgFormatType() {
		return msgFormatType;
	}

	/**
	 * @param msgFormatType The msgFormatType to set.
	 */
	public void setMsgFormatType(String msgFormatType) {
		this.msgFormatType = msgFormatType;
	}

}
