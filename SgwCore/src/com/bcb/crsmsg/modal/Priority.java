package com.bcb.crsmsg.modal;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Priority implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1065723049519135087L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String priorityName;

    /** nullable persistent field */
    private String status;

    /** persistent field */
    private int version;

    /** full constructor */
    public Priority(String priorityName, String status, int version) {
        this.priorityName = priorityName;
        this.status = status;
        this.version = version;
    }

    /** default constructor */
    public Priority() {
    }

    /** minimal constructor */
    public Priority(int version) {
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPriorityName() {
        return this.priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
