package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class Record implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4521705592553278494L;
	private ArrayList<String> column;

	/** default constructor */
	public Record() {
	}

	/**
	 * @return Returns the column.
	 */
	public ArrayList<String> getColumn() {
		return column;
	}

	/**
	 * @param column The column to set.
	 */
	public void setColumn(ArrayList<String> column) {
		this.column = column;
	}
}