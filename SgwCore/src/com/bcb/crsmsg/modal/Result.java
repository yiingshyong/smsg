package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class Result implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2315451975335754336L;
	private ArrayList<Record> record;

	/** default constructor */
	public Result() {
	}

	/**
	 * @return Returns the record.
	 */
	public ArrayList<Record> getRecord() {
		return record;
	}

	/**
	 * @param record The record to set.
	 */
	public void setRecord(ArrayList<Record> record) {
		this.record = record;
	}

}