package com.bcb.crsmsg.modal;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class RightsRef implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2453601396091230526L;

	/** identifier field */
    private Integer id;

    /** persistent field */
    private int appsId;

    /** persistent field */
    private String rightsName;

    /** persistent field */
    private int rightsValue;

    /** full constructor */
    public RightsRef(Integer id, int appsId, String rightsName, int rightsValue) {
        this.id = id;
        this.appsId = appsId;
        this.rightsName = rightsName;
        this.rightsValue = rightsValue;
    }

    /** default constructor */
    public RightsRef() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAppsId() {
        return this.appsId;
    }

    public void setAppsId(int appsId) {
        this.appsId = appsId;
    }

    public String getRightsName() {
        return this.rightsName;
    }

    public void setRightsName(String rightsName) {
        this.rightsName = rightsName;
    }

    public int getRightsValue() {
        return this.rightsValue;
    }

    public void setRightsValue(int rightsValue) {
        this.rightsValue = rightsValue;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
