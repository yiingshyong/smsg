package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.math.BigDecimal;

public class ServiceProvider implements Serializable{

	private Integer id;
	private String name;
	private String smsRateIds;
	private Integer defaultSmsRateId;
	private Integer colspan;
	private BigDecimal incomingGstCharge;
	private BigDecimal outgoingGstCharge;
	private Integer gstColspan;
	
	public ServiceProvider() {
		super();
	}

	public ServiceProvider(Integer id, String name, String smsRateIds, Integer defaultSmsRateId) {
		super();
		this.id = id;
		this.name = name;
		this.smsRateIds = smsRateIds;
		this.defaultSmsRateId = defaultSmsRateId;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSmsRateIds() {
		return smsRateIds;
	}
	public void setSmsRateIds(String smsRateIds) {
		this.smsRateIds = smsRateIds;
	}

	public Integer getDefaultSmsRateId() {
		return defaultSmsRateId;
	}

	public void setDefaultSmsRateId(Integer defaultSmsRateId) {
		this.defaultSmsRateId = defaultSmsRateId;
	}

	public Integer getColspan() {
		return colspan;
	}

	public void setColspan(Integer colspan) {
		this.colspan = colspan;
	}

	public BigDecimal getIncomingGstCharge() {
		return incomingGstCharge;
	}

	public void setIncomingGstCharge(BigDecimal incomingGstCharge) {
		this.incomingGstCharge = incomingGstCharge;
	}

	public BigDecimal getOutgoingGstCharge() {
		return outgoingGstCharge;
	}

	public void setOutgoingGstCharge(BigDecimal outgoingGstCharge) {
		this.outgoingGstCharge = outgoingGstCharge;
	}

	public Integer getGstColspan() {
		return gstColspan;
	}

	public void setGstColspan(Integer gstColspan) {
		this.gstColspan = gstColspan;
	}

}
