package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

/** @author Hibernate CodeGenerator */
public class SmsCfg implements Serializable {

	private static final long serialVersionUID = 7374509859277576273L;

    private Integer id;
    private String autoSmsFailure;
    private String autoSmsResend;
    private String autoSmsResendInterval;
    private String autoSmsResendLimit;
    private String bulkSmsThreshold;
    private String bulkSmsThresholdNotification;
    private String bulkSmsThresholdNotificationEmail;
    private String bulkSmsThresholdNotificationEmailSender;
    private String bulkSmsThresholdNotificationSms;
    private String bulkSmsThresholdNotificationSmsSender;
    private String queueSmsThreshold;
    private String queueSmsThresholdNotification;
    private String queueSmsThresholdNotificationEmail;
    private String queueSmsThresholdNotificationEmailSender;
    private String queueSmsThresholdNotificationSms;
    private String queueSmsThresholdNotificationSmsSender;
    private String queueSmsThresholdPeriod;
    private String queueSmsThresholdTime;
    private String concatenateSms;
    private String concatenateSmsNo;
    private String createdBy;
    private Date createdDatetime;
    private String ftpFailureNotification;
    private String ftpFailureNotificationEmail;
    private String ftpFailureNotificationEmailSender;
    private String ftpFailureNotificationSms;
    private String ftpFailureNotificationSmsSender;
    private String httpSmsService;
    private String ipControl;
    private String ipSkip;
    private String mobileCountryPrefix;
    private String modifiedBy;
    private Date modifiedDatetime;
    private String scheduledSmsControlDuration;
    private String smsFailureNotification;
    private String smsFailureNotificationEmail;
    private String smsFailureNotificationEmailSender;
    private String smsFailureNotificationSms;
    private String smsFailureNotificationSmsSender;
    private String smsFailureThreshold;
    private String smsFailureThresholdPeriod;
    private String smsFailureThresholdTime;
    private String smsPrioritize;
    private String smsQuota;
    private String smsQuotaDefault;
    private String smsTelcoRouting;
    private String smsTelcoRoutingParam;
    private String smsTimeControlBatch;
    private String smsTimeControlDay;
    private String smsTimeControlTimeFrom;
    private String smsTimeControlTimeTo;
    private String smsTimeControlWeb;
    private String tableListDelimiter;
    private String systemDebug;
    private String emailFormat;
    private int version;
    private String charPerSms;
    private String defaultFileDelimiter;
    private String usageLogDuration;
    private String systemLogDuration;
    private String passwordChangeDuration;
    private String accountDormancyCheck;
    private Integer totalQueuedSizeAlertThreshold;
    private Integer totalQueuedSizeAlertCheckPeriod;
    private Integer disabledSvcPvdCheckPeriod;
    private int bpMaxThread;
    private int bpMaxProcessPerThread;
    private int bpMaxRetrieve;
    private int bpRunInterval;
    private int bpWorkerKeepAliveTime;
    private int bpWorkerTimeWait;
    private String defaultRoutableTelco;
    private String serData;
    private int defaultSMSType;
    private String systemSMSSender;
    private String ccaServerIP;
    private int ccaServerPort;
    private int ccaServerResponseTimeout;
    private int ccaMaxFailureAttempt;
    private String ccaSvcWindow;
    private String ccaSuccessSms;
    private String ccaFailureSms;
    private String ccaHostUnavailabilitySms;
    private String ccaUnavailabilitySms;
    private String ccaDataAge;      
    private String clZPK;
    private String socketEncComp1;
    private String socketEncComp2;
    private String socketEncComp3;
    
	public SmsCfg(Integer id, String autoSmsFailure, String autoSmsResend,
			String autoSmsResendInterval, String autoSmsResendLimit,
			String bulkSmsThreshold, String bulkSmsThresholdNotification,
			String bulkSmsThresholdNotificationEmail,
			String bulkSmsThresholdNotificationEmailSender,
			String bulkSmsThresholdNotificationSms,
			String bulkSmsThresholdNotificationSmsSender,
			String queueSmsThreshold, String queueSmsThresholdNotification,
			String queueSmsThresholdNotificationEmail,
			String queueSmsThresholdNotificationEmailSender,
			String queueSmsThresholdNotificationSms,
			String queueSmsThresholdNotificationSmsSender,
			String queueSmsThresholdPeriod, String queueSmsThresholdTime,
			String concatenateSms, String concatenateSmsNo, String createdBy,
			Date createdDatetime, String ftpFailureNotification,
			String ftpFailureNotificationEmail,
			String ftpFailureNotificationEmailSender,
			String ftpFailureNotificationSms,
			String ftpFailureNotificationSmsSender, String httpSmsService,
			String ipControl, String ipSkip, String mobileCountryPrefix,
			String modifiedBy, Date modifiedDatetime,
			String scheduledSmsControlDuration, String smsFailureNotification,
			String smsFailureNotificationEmail,
			String smsFailureNotificationEmailSender,
			String smsFailureNotificationSms,
			String smsFailureNotificationSmsSender, String smsFailureThreshold,
			String smsFailureThresholdPeriod, String smsFailureThresholdTime,
			String smsPrioritize, String smsQuota, String smsQuotaDefault,
			String smsTelcoRouting, String smsTelcoRoutingParam,
			String smsTimeControlBatch, String smsTimeControlDay,
			String smsTimeControlTimeFrom, String smsTimeControlTimeTo,
			String smsTimeControlWeb, String tableListDelimiter,
			String systemDebug, String emailFormat, int version,
			String charPerSms, String defaultFileDelimiter,
			String usageLogDuration, String systemLogDuration,
			String passwordChangeDuration, String accountDormancyCheck,
			Integer totalQueuedSizeAlertThreshold,
			Integer totalQueuedSizeAlertCheckPeriod, int bpMaxThread,
			int bpMaxProcessPerThread, int bpMaxRetrieve, int defaultSMSType, String systemSMSSender,
			String ccaServerIP, int ccaServerPort, int ccaServerResponseTimeout, int ccaMaxFailureAttempt, 
			String ccaSvcWindow, String ccaSuccessSms, String ccaFailureSms, String ccaUnavailabilitySms, 
			String ccaHostUnavailabilitySms, String ccaDataAge,
			String clZPK, String socketEncComp1, String socketEncComp2, String socketEncComp3
			) {
		super();
		this.id = id;
		this.autoSmsFailure = autoSmsFailure;
		this.autoSmsResend = autoSmsResend;
		this.autoSmsResendInterval = autoSmsResendInterval;
		this.autoSmsResendLimit = autoSmsResendLimit;
		this.bulkSmsThreshold = bulkSmsThreshold;
		this.bulkSmsThresholdNotification = bulkSmsThresholdNotification;
		this.bulkSmsThresholdNotificationEmail = bulkSmsThresholdNotificationEmail;
		this.bulkSmsThresholdNotificationEmailSender = bulkSmsThresholdNotificationEmailSender;
		this.bulkSmsThresholdNotificationSms = bulkSmsThresholdNotificationSms;
		this.bulkSmsThresholdNotificationSmsSender = bulkSmsThresholdNotificationSmsSender;
		this.queueSmsThreshold = queueSmsThreshold;
		this.queueSmsThresholdNotification = queueSmsThresholdNotification;
		this.queueSmsThresholdNotificationEmail = queueSmsThresholdNotificationEmail;
		this.queueSmsThresholdNotificationEmailSender = queueSmsThresholdNotificationEmailSender;
		this.queueSmsThresholdNotificationSms = queueSmsThresholdNotificationSms;
		this.queueSmsThresholdNotificationSmsSender = queueSmsThresholdNotificationSmsSender;
		this.queueSmsThresholdPeriod = queueSmsThresholdPeriod;
		this.queueSmsThresholdTime = queueSmsThresholdTime;
		this.concatenateSms = concatenateSms;
		this.concatenateSmsNo = concatenateSmsNo;
		this.createdBy = createdBy;
		this.createdDatetime = createdDatetime;
		this.ftpFailureNotification = ftpFailureNotification;
		this.ftpFailureNotificationEmail = ftpFailureNotificationEmail;
		this.ftpFailureNotificationEmailSender = ftpFailureNotificationEmailSender;
		this.ftpFailureNotificationSms = ftpFailureNotificationSms;
		this.ftpFailureNotificationSmsSender = ftpFailureNotificationSmsSender;
		this.httpSmsService = httpSmsService;
		this.ipControl = ipControl;
		this.ipSkip = ipSkip;
		this.mobileCountryPrefix = mobileCountryPrefix;
		this.modifiedBy = modifiedBy;
		this.modifiedDatetime = modifiedDatetime;
		this.scheduledSmsControlDuration = scheduledSmsControlDuration;
		this.smsFailureNotification = smsFailureNotification;
		this.smsFailureNotificationEmail = smsFailureNotificationEmail;
		this.smsFailureNotificationEmailSender = smsFailureNotificationEmailSender;
		this.smsFailureNotificationSms = smsFailureNotificationSms;
		this.smsFailureNotificationSmsSender = smsFailureNotificationSmsSender;
		this.smsFailureThreshold = smsFailureThreshold;
		this.smsFailureThresholdPeriod = smsFailureThresholdPeriod;
		this.smsFailureThresholdTime = smsFailureThresholdTime;
		this.smsPrioritize = smsPrioritize;
		this.smsQuota = smsQuota;
		this.smsQuotaDefault = smsQuotaDefault;
		this.smsTelcoRouting = smsTelcoRouting;
		this.smsTelcoRoutingParam = smsTelcoRoutingParam;
		this.smsTimeControlBatch = smsTimeControlBatch;
		this.smsTimeControlDay = smsTimeControlDay;
		this.smsTimeControlTimeFrom = smsTimeControlTimeFrom;
		this.smsTimeControlTimeTo = smsTimeControlTimeTo;
		this.smsTimeControlWeb = smsTimeControlWeb;
		this.tableListDelimiter = tableListDelimiter;
		this.systemDebug = systemDebug;
		this.emailFormat = emailFormat;
		this.version = version;
		this.charPerSms = charPerSms;
		this.defaultFileDelimiter = defaultFileDelimiter;
		this.usageLogDuration = usageLogDuration;
		this.systemLogDuration = systemLogDuration;
		this.passwordChangeDuration = passwordChangeDuration;
		this.accountDormancyCheck = accountDormancyCheck;
		this.totalQueuedSizeAlertThreshold = totalQueuedSizeAlertThreshold;
		this.totalQueuedSizeAlertCheckPeriod = totalQueuedSizeAlertCheckPeriod;
		this.bpMaxThread = bpMaxThread;
		this.bpMaxProcessPerThread = bpMaxProcessPerThread;
		this.bpMaxRetrieve = bpMaxRetrieve;
		this.defaultSMSType = defaultSMSType;
		this.systemSMSSender = systemSMSSender;
		this.ccaServerIP = ccaServerIP;
		this.ccaServerPort = ccaServerPort;
		this.ccaServerResponseTimeout = ccaServerResponseTimeout;
		this.ccaMaxFailureAttempt = ccaMaxFailureAttempt;
		this.ccaSvcWindow = ccaSvcWindow;
		this.ccaSuccessSms = ccaSuccessSms;
		this.ccaFailureSms = ccaFailureSms;
		this.ccaUnavailabilitySms = ccaUnavailabilitySms;
		this.ccaHostUnavailabilitySms = ccaHostUnavailabilitySms;
		this.ccaDataAge = ccaDataAge;
		this.clZPK = clZPK;
	}

    /** default constructor */
    public SmsCfg() {
    }

    /** minimal constructor */
    public SmsCfg(int version) {
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAutoSmsFailure() {
        return this.autoSmsFailure;
    }

    public void setAutoSmsFailure(String autoSmsFailure) {
        this.autoSmsFailure = autoSmsFailure;
    }

    public String getAutoSmsResend() {
        return this.autoSmsResend;
    }

    public void setAutoSmsResend(String autoSmsResend) {
        this.autoSmsResend = autoSmsResend;
    }

    public String getAutoSmsResendInterval() {
        return this.autoSmsResendInterval;
    }

    public void setAutoSmsResendInterval(String autoSmsResendInterval) {
        this.autoSmsResendInterval = autoSmsResendInterval;
    }

    public String getAutoSmsResendLimit() {
        return this.autoSmsResendLimit;
    }

    public void setAutoSmsResendLimit(String autoSmsResendLimit) {
        this.autoSmsResendLimit = autoSmsResendLimit;
    }

    public String getBulkSmsThreshold() {
        return this.bulkSmsThreshold;
    }

    public void setBulkSmsThreshold(String bulkSmsThreshold) {
        this.bulkSmsThreshold = bulkSmsThreshold;
    }

    public String getBulkSmsThresholdNotificationEmail() {
        return this.bulkSmsThresholdNotificationEmail;
    }

    public void setBulkSmsThresholdNotificationEmail(String bulkSmsThresholdNotificationEmail) {
        this.bulkSmsThresholdNotificationEmail = bulkSmsThresholdNotificationEmail;
    }

    public String getConcatenateSms() {
        return this.concatenateSms;
    }

    public void setConcatenateSms(String concatenateSms) {
        this.concatenateSms = concatenateSms;
    }

    public String getConcatenateSmsNo() {
        return this.concatenateSmsNo;
    }

    public void setConcatenateSmsNo(String concatenateSmsNo) {
        this.concatenateSmsNo = concatenateSmsNo;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getFtpFailureNotificationEmail() {
        return this.ftpFailureNotificationEmail;
    }

    public void setFtpFailureNotificationEmail(String ftpFailureNotificationEmail) {
        this.ftpFailureNotificationEmail = ftpFailureNotificationEmail;
    }

    public String getHttpSmsService() {
        return this.httpSmsService;
    }

    public void setHttpSmsService(String httpSmsService) {
        this.httpSmsService = httpSmsService;
    }

    public String getIpControl() {
        return this.ipControl;
    }

    public void setIpControl(String ipControl) {
        this.ipControl = ipControl;
    }

    public String getIpSkip() {
        return this.ipSkip;
    }

    public void setIpSkip(String ipSkip) {
        this.ipSkip = ipSkip;
    }

    public String getMobileCountryPrefix() {
        return this.mobileCountryPrefix;
    }

    public void setMobileCountryPrefix(String mobileCountryPrefix) {
        this.mobileCountryPrefix = mobileCountryPrefix;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getScheduledSmsControlDuration() {
        return this.scheduledSmsControlDuration;
    }

    public void setScheduledSmsControlDuration(String scheduledSmsControlDuration) {
        this.scheduledSmsControlDuration = scheduledSmsControlDuration;
    }

    public String getSmsFailureNotificationEmail() {
        return this.smsFailureNotificationEmail;
    }

    public void setSmsFailureNotificationEmail(String smsFailureNotificationEmail) {
        this.smsFailureNotificationEmail = smsFailureNotificationEmail;
    }

    public String getSmsPrioritize() {
        return this.smsPrioritize;
    }

    public void setSmsPrioritize(String smsPrioritize) {
        this.smsPrioritize = smsPrioritize;
    }

    public String getSmsQuota() {
        return this.smsQuota;
    }

    public void setSmsQuota(String smsQuota) {
        this.smsQuota = smsQuota;
    }

    public String getSmsQuotaDefault() {
        return this.smsQuotaDefault;
    }

    public void setSmsQuotaDefault(String smsQuotaDefault) {
        this.smsQuotaDefault = smsQuotaDefault;
    }

    public String getSmsTelcoRouting() {
        return this.smsTelcoRouting;
    }

    public void setSmsTelcoRouting(String smsTelcoRouting) {
        this.smsTelcoRouting = smsTelcoRouting;
    }

    public String getSmsTelcoRoutingParam() {
		return smsTelcoRoutingParam;
	}

	public void setSmsTelcoRoutingParam(String smsTelcoRoutingParam) {
		this.smsTelcoRoutingParam = smsTelcoRoutingParam;
	}

	public String getSmsTimeControlBatch() {
        return this.smsTimeControlBatch;
    }

    public void setSmsTimeControlBatch(String smsTimeControlBatch) {
        this.smsTimeControlBatch = smsTimeControlBatch;
    }

    public String getSmsTimeControlDay() {
        return this.smsTimeControlDay;
    }

    public void setSmsTimeControlDay(String smsTimeControlDay) {
        this.smsTimeControlDay = smsTimeControlDay;
    }

    /**
	 * @return Returns the smsTimeControlTimeFrom.
	 */
	public String getSmsTimeControlTimeFrom() {
		return smsTimeControlTimeFrom;
	}

	/**
	 * @param smsTimeControlTimeFrom The smsTimeControlTimeFrom to set.
	 */
	public void setSmsTimeControlTimeFrom(String smsTimeControlTimeFrom) {
		this.smsTimeControlTimeFrom = smsTimeControlTimeFrom;
	}

	/**
	 * @return Returns the smsTimeControlTimeTo.
	 */
	public String getSmsTimeControlTimeTo() {
		return smsTimeControlTimeTo;
	}

	/**
	 * @param smsTimeControlTimeTo The smsTimeControlTimeTo to set.
	 */
	public void setSmsTimeControlTimeTo(String smsTimeControlTimeTo) {
		this.smsTimeControlTimeTo = smsTimeControlTimeTo;
	}

	public String getSmsTimeControlWeb() {
        return this.smsTimeControlWeb;
    }

    public void setSmsTimeControlWeb(String smsTimeControlWeb) {
        this.smsTimeControlWeb = smsTimeControlWeb;
    }

    public String getTableListDelimiter() {
        return this.tableListDelimiter;
    }

    public void setTableListDelimiter(String tableListDelimiter) {
        this.tableListDelimiter = tableListDelimiter;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the systemDebug.
	 */
	public String getSystemDebug() {
		return systemDebug;
	}

	/**
	 * @param systemDebug The systemDebug to set.
	 */
	public void setSystemDebug(String systemDebug) {
		this.systemDebug = systemDebug;
	}

	/**
	 * @return Returns the emailFormat.
	 */
	public String getEmailFormat() {
		return emailFormat;
	}

	/**
	 * @param emailFormat The emailFormat to set.
	 */
	public void setEmailFormat(String emailFormat) {
		this.emailFormat = emailFormat;
	}

	/**
	 * @return Returns the charPerSms.
	 */
	public String getCharPerSms() {
		return charPerSms;
	}

	/**
	 * @param charPerSms The charPerSms to set.
	 */
	public void setCharPerSms(String charPerSms) {
		this.charPerSms = charPerSms;
	}

	/**
	 * @return Returns the defaultFileDelimiter.
	 */
	public String getDefaultFileDelimiter() {
		return defaultFileDelimiter;
	}

	/**
	 * @param defaultFileDelimiter The defaultFileDelimiter to set.
	 */
	public void setDefaultFileDelimiter(String defaultFileDelimiter) {
		this.defaultFileDelimiter = defaultFileDelimiter;
	}

	public String getFtpFailureNotificationSms() {
		return ftpFailureNotificationSms;
	}

	public void setFtpFailureNotificationSms(String ftpFailureNotificationSms) {
		this.ftpFailureNotificationSms = ftpFailureNotificationSms;
	}

	public String getFtpFailureNotificationSmsSender() {
		return ftpFailureNotificationSmsSender;
	}

	public void setFtpFailureNotificationSmsSender(
			String ftpFailureNotificationSmsSender) {
		this.ftpFailureNotificationSmsSender = ftpFailureNotificationSmsSender;
	}

	public String getSmsFailureThreshold() {
		return smsFailureThreshold;
	}

	public void setSmsFailureThreshold(String smsFailureThreshold) {
		this.smsFailureThreshold = smsFailureThreshold;
	}

	public String getFtpFailureNotification() {
		return ftpFailureNotification;
	}

	public void setFtpFailureNotification(String ftpFailureNotification) {
		this.ftpFailureNotification = ftpFailureNotification;
	}

	public String getSmsFailureNotification() {
		return smsFailureNotification;
	}

	public void setSmsFailureNotification(String smsFailureNotification) {
		this.smsFailureNotification = smsFailureNotification;
	}

	public String getSmsFailureNotificationSms() {
		return smsFailureNotificationSms;
	}

	public void setSmsFailureNotificationSms(String smsFailureNotificationSms) {
		this.smsFailureNotificationSms = smsFailureNotificationSms;
	}

	public String getSmsFailureNotificationSmsSender() {
		return smsFailureNotificationSmsSender;
	}

	public void setSmsFailureNotificationSmsSender(
			String smsFailureNotificationSmsSender) {
		this.smsFailureNotificationSmsSender = smsFailureNotificationSmsSender;
	}

	public String getSmsFailureThresholdTime() {
		return smsFailureThresholdTime;
	}

	public void setSmsFailureThresholdTime(String smsFailureThresholdTime) {
		this.smsFailureThresholdTime = smsFailureThresholdTime;
	}

	public String getFtpFailureNotificationEmailSender() {
		return ftpFailureNotificationEmailSender;
	}

	public void setFtpFailureNotificationEmailSender(
			String ftpFailureNotificationEmailSender) {
		this.ftpFailureNotificationEmailSender = ftpFailureNotificationEmailSender;
	}

	public String getSmsFailureNotificationEmailSender() {
		return smsFailureNotificationEmailSender;
	}

	public void setSmsFailureNotificationEmailSender(
			String smsFailureNotificationEmailSender) {
		this.smsFailureNotificationEmailSender = smsFailureNotificationEmailSender;
	}

	public String getSmsFailureThresholdPeriod() {
		return smsFailureThresholdPeriod;
	}

	public void setSmsFailureThresholdPeriod(String smsFailureThresholdPeriod) {
		this.smsFailureThresholdPeriod = smsFailureThresholdPeriod;
	}

	public String getBulkSmsThresholdNotification() {
		return bulkSmsThresholdNotification;
	}

	public void setBulkSmsThresholdNotification(String bulkSmsThresholdNotification) {
		this.bulkSmsThresholdNotification = bulkSmsThresholdNotification;
	}

	public String getBulkSmsThresholdNotificationEmailSender() {
		return bulkSmsThresholdNotificationEmailSender;
	}

	public void setBulkSmsThresholdNotificationEmailSender(
			String bulkSmsThresholdNotificationEmailSender) {
		this.bulkSmsThresholdNotificationEmailSender = bulkSmsThresholdNotificationEmailSender;
	}

	public String getBulkSmsThresholdNotificationSms() {
		return bulkSmsThresholdNotificationSms;
	}

	public void setBulkSmsThresholdNotificationSms(
			String bulkSmsThresholdNotificationSms) {
		this.bulkSmsThresholdNotificationSms = bulkSmsThresholdNotificationSms;
	}

	public String getBulkSmsThresholdNotificationSmsSender() {
		return bulkSmsThresholdNotificationSmsSender;
	}

	public void setBulkSmsThresholdNotificationSmsSender(
			String bulkSmsThresholdNotificationSmsSender) {
		this.bulkSmsThresholdNotificationSmsSender = bulkSmsThresholdNotificationSmsSender;
	}

	public void setUsageLogDuration(String usageLogDuration) {
		this.usageLogDuration = usageLogDuration;
	}

	public String getUsageLogDuration() {
		return usageLogDuration;
	}

	public String getSystemLogDuration() {
		return systemLogDuration;
	}

	public void setSystemLogDuration(String systemLogDuration) {
		this.systemLogDuration = systemLogDuration;
	}

	public String getPasswordChangeDuration() {
		return passwordChangeDuration;
	}

	public void setPasswordChangeDuration(String passwordChangeDuration) {
		this.passwordChangeDuration = passwordChangeDuration;
	}

	public String getAccountDormancyCheck() {
		return accountDormancyCheck;
	}

	public void setAccountDormancyCheck(String accountDormancyCheck) {
		this.accountDormancyCheck = accountDormancyCheck;
	}

	public String getQueueSmsThreshold() {
		return queueSmsThreshold;
	}

	public void setQueueSmsThreshold(String queueSmsThreshold) {
		this.queueSmsThreshold = queueSmsThreshold;
	}

	public String getQueueSmsThresholdNotification() {
		return queueSmsThresholdNotification;
	}

	public void setQueueSmsThresholdNotification(
			String queueSmsThresholdNotification) {
		this.queueSmsThresholdNotification = queueSmsThresholdNotification;
	}

	public String getQueueSmsThresholdNotificationEmail() {
		return queueSmsThresholdNotificationEmail;
	}

	public void setQueueSmsThresholdNotificationEmail(
			String queueSmsThresholdNotificationEmail) {
		this.queueSmsThresholdNotificationEmail = queueSmsThresholdNotificationEmail;
	}

	public String getQueueSmsThresholdNotificationEmailSender() {
		return queueSmsThresholdNotificationEmailSender;
	}

	public void setQueueSmsThresholdNotificationEmailSender(
			String queueSmsThresholdNotificationEmailSender) {
		this.queueSmsThresholdNotificationEmailSender = queueSmsThresholdNotificationEmailSender;
	}

	public String getQueueSmsThresholdNotificationSms() {
		return queueSmsThresholdNotificationSms;
	}

	public void setQueueSmsThresholdNotificationSms(
			String queueSmsThresholdNotificationSms) {
		this.queueSmsThresholdNotificationSms = queueSmsThresholdNotificationSms;
	}

	public String getQueueSmsThresholdNotificationSmsSender() {
		return queueSmsThresholdNotificationSmsSender;
	}

	public void setQueueSmsThresholdNotificationSmsSender(
			String queueSmsThresholdNotificationSmsSender) {
		this.queueSmsThresholdNotificationSmsSender = queueSmsThresholdNotificationSmsSender;
	}

	public String getQueueSmsThresholdPeriod() {
		return queueSmsThresholdPeriod;
	}

	public void setQueueSmsThresholdPeriod(String queueSmsThresholdPeriod) {
		this.queueSmsThresholdPeriod = queueSmsThresholdPeriod;
	}

	public String getQueueSmsThresholdTime() {
		return queueSmsThresholdTime;
	}

	public void setQueueSmsThresholdTime(String queueSmsThresholdTime) {
		this.queueSmsThresholdTime = queueSmsThresholdTime;
	}

	public Integer getTotalQueuedSizeAlertThreshold() {
		return totalQueuedSizeAlertThreshold;
	}

	public void setTotalQueuedSizeAlertThreshold(
			Integer totalQueuedSizeAlertThreshold) {
		this.totalQueuedSizeAlertThreshold = totalQueuedSizeAlertThreshold;
	}

	public Integer getTotalQueuedSizeAlertCheckPeriod() {
		return totalQueuedSizeAlertCheckPeriod;
	}

	public void setTotalQueuedSizeAlertCheckPeriod(
			Integer totalQueuedSizeAlertCheckPeriod) {
		this.totalQueuedSizeAlertCheckPeriod = totalQueuedSizeAlertCheckPeriod;
	}

	public int getBpMaxThread() {
		return bpMaxThread;
	}

	public void setBpMaxThread(int bpMaxThread) {
		this.bpMaxThread = bpMaxThread;
	}

	public int getBpMaxProcessPerThread() {
		return bpMaxProcessPerThread;
	}

	public void setBpMaxProcessPerThread(int bpMaxProcessPerThread) {
		this.bpMaxProcessPerThread = bpMaxProcessPerThread;
	}

	public int getBpMaxRetrieve() {
		return bpMaxRetrieve;
	}

	public void setBpMaxRetrieve(int bpMaxRetrieve) {
		this.bpMaxRetrieve = bpMaxRetrieve;
	}

	public int getBpRunInterval() {
		return bpRunInterval;
	}

	public void setBpRunInterval(int bpRunInterval) {
		this.bpRunInterval = bpRunInterval;
	}

	public int getBpWorkerKeepAliveTime() {
		return bpWorkerKeepAliveTime;
	}

	public void setBpWorkerKeepAliveTime(int bpWorkerKeepAliveTime) {
		this.bpWorkerKeepAliveTime = bpWorkerKeepAliveTime;
	}

	public int getBpWorkerTimeWait() {
		return bpWorkerTimeWait;
	}

	public void setBpWorkerTimeWait(int bpWorkerTimeWait) {
		this.bpWorkerTimeWait = bpWorkerTimeWait;
	}

	public String getSerData() {
		return serData;
	}

	public void setSerData(String serData) {
		this.serData = serData;
	}

	public Integer getDisabledSvcPvdCheckPeriod() {
		return disabledSvcPvdCheckPeriod;
	}

	public void setDisabledSvcPvdCheckPeriod(Integer disabledSvcPvdCheckPeriod) {
		this.disabledSvcPvdCheckPeriod = disabledSvcPvdCheckPeriod;
	}

	public String getDefaultRoutableTelco() {
		return defaultRoutableTelco;
	}

	public void setDefaultRoutableTelco(String defaultRoutableTelco) {
		this.defaultRoutableTelco = defaultRoutableTelco;
	}

	public int getDefaultSMSType() {
		return defaultSMSType;
	}

	public void setDefaultSMSType(int defaultSMSType) {
		this.defaultSMSType = defaultSMSType;
	}

	public String getSystemSMSSender() {
		return systemSMSSender;
	}

	public void setSystemSMSSender(String systemSMSSender) {
		this.systemSMSSender = systemSMSSender;
	}

	public String getCcaServerIP() {
		return ccaServerIP;
	}

	public void setCcaServerIP(String ccaServerIP) {
		this.ccaServerIP = ccaServerIP;
	}

	public int getCcaServerPort() {
		return ccaServerPort;
	}

	public void setCcaServerPort(int ccaServerPort) {
		this.ccaServerPort = ccaServerPort;
	}

	public int getCcaMaxFailureAttempt() {
		return ccaMaxFailureAttempt;
	}

	public void setCcaMaxFailureAttempt(int ccaMaxFailureAttempt) {
		this.ccaMaxFailureAttempt = ccaMaxFailureAttempt;
	}

	public String getCcaSvcWindow() {
		return ccaSvcWindow;
	}

	public void setCcaSvcWindow(String ccaSvcWindow) {
		this.ccaSvcWindow = ccaSvcWindow;
	}

	public String getCcaSuccessSms() {
		return ccaSuccessSms;
	}

	public void setCcaSuccessSms(String ccaSuccessSms) {
		this.ccaSuccessSms = ccaSuccessSms;
	}

	public String getCcaFailureSms() {
		return ccaFailureSms;
	}

	public void setCcaFailureSms(String ccaFailureSms) {
		this.ccaFailureSms = ccaFailureSms;
	}

	public String getCcaUnavailabilitySms() {
		return ccaUnavailabilitySms;
	}

	public void setCcaUnavailabilitySms(String ccaUnavailabilitySms) {
		this.ccaUnavailabilitySms = ccaUnavailabilitySms;
	}

	public int getCcaServerResponseTimeout() {
		return ccaServerResponseTimeout;
	}

	public void setCcaServerResponseTimeout(int ccaServerResponseTimeout) {
		this.ccaServerResponseTimeout = ccaServerResponseTimeout;
	}

	public String getCcaHostUnavailabilitySms() {
		return ccaHostUnavailabilitySms;
	}

	public void setCcaHostUnavailabilitySms(String ccaHostUnavailabilitySms) {
		this.ccaHostUnavailabilitySms = ccaHostUnavailabilitySms;
	}

	public String getCcaDataAge() {
		return ccaDataAge;
	}

	public void setCcaDataAge(String ccaDataAge) {
		this.ccaDataAge = ccaDataAge;
	}

	public String getClZPK() {
		return clZPK;
	}

	public void setClZPK(String clZPK) {
		this.clZPK = clZPK;
	}
	public String getSocketEncComp1() {
		return socketEncComp1;
	}

	public void setSocketEncComp1(String socketEncComp1) {
		this.socketEncComp1 = socketEncComp1;
	}

	public String getSocketEncComp2() {
		return socketEncComp2;
	}

	public void setSocketEncComp2(String socketEncComp2) {
		this.socketEncComp2 = socketEncComp2;
	}

	public String getSocketEncComp3() {
		return socketEncComp3;
	}

	public void setSocketEncComp3(String socketEncComp3) {
		this.socketEncComp3 = socketEncComp3;
	}
}
