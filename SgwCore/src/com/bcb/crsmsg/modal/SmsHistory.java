package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class SmsHistory implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 9179090130471936582L;

	/** identifier field */
    private Integer historyId;

    /** nullable persistent field */
    private String approvalBy;

    /** nullable persistent field */
    private Date approvalDatetime;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Date deliveryReportDate;

    /** nullable persistent field */
    private Integer ftp;

    /** nullable persistent field */
    private String message;

    /** nullable persistent field */
    private String mobileNo;

    /** nullable persistent field */
    private String msgFormatType;

    /** nullable persistent field */
    private Integer priority;

    /** nullable persistent field */
    private String remarks;

    /** nullable persistent field */
    private String sentBy;

    /** persistent field */
    private int id;

    /** nullable persistent field */
    private String smsScheduled;
    
    /** nullable persistent field */
    private Date smsScheduledTime;

    /** nullable persistent field */
    private String smsStatus;

    /** nullable persistent field */
    private String smsStatusBy;

    /** nullable persistent field */
    private Date smsStatusDatetime;

    /** nullable persistent field */
    private Integer telco;

    /** nullable persistent field */
    private String type;

    /** persistent field */
    private int version;
    
    /** persistent field */
    private String dept;
    
    /** persistent field */
    private int deptId;

    private String modifiedBy;
    
    private Date modifiedDatetime;
    
    private String smsCharge;
    
    private String smsChargeMode;
    
    private String concatenateSms;
    
    public String getConcatenateSms() {
		return concatenateSms;
	}

	public void setConcatenateSms(String concatenateSms) {
		this.concatenateSms = concatenateSms;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	/** full constructor */
    public SmsHistory(String approvalBy, Date approvalDatetime, String createdBy, Date createdDatetime, Date deliveryReportDate, Integer ftp, String message, String mobileNo, String msgFormatType, Integer priority, String remarks, String sentBy, int smsId, String smsScheduled, Date smsScheduledTime, String smsStatus, String smsStatusBy, Date smsStatusDatetime, Integer telco, String type, int version, String dept) {
        this.approvalBy = approvalBy;
        this.approvalDatetime = approvalDatetime;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.deliveryReportDate = deliveryReportDate;
        this.ftp = ftp;
        this.message = message;
        this.mobileNo = mobileNo;
        this.msgFormatType = msgFormatType;
        this.priority = priority;
        this.remarks = remarks;
        this.sentBy = sentBy;
        this.id = smsId;
        this.smsScheduled = smsScheduled;
        this.smsScheduledTime = smsScheduledTime;
        this.smsStatus = smsStatus;
        this.smsStatusBy = smsStatusBy;
        this.smsStatusDatetime = smsStatusDatetime;
        this.telco = telco;
        this.type = type;
        this.version = version;
        this.dept=dept;
    }

    /** default constructor */
    public SmsHistory() {
    }

    /** minimal constructor */
    public SmsHistory(int smsId, int version) {
        this.id = smsId;
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApprovalBy() {
        return this.approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public Date getApprovalDatetime() {
        return this.approvalDatetime;
    }

    public void setApprovalDatetime(Date approvalDatetime) {
        this.approvalDatetime = approvalDatetime;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Date getDeliveryReportDate() {
        return this.deliveryReportDate;
    }

    public void setDeliveryReportDate(Date deliveryReportDate) {
        this.deliveryReportDate = deliveryReportDate;
    }

    public Integer getFtp() {
        return this.ftp;
    }

    public void setFtp(Integer ftp) {
        this.ftp = ftp;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMsgFormatType() {
        return this.msgFormatType;
    }

    public void setMsgFormatType(String msgFormatType) {
        this.msgFormatType = msgFormatType;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentBy() {
        return this.sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public Date getSmsScheduledTime() {
        return this.smsScheduledTime;
    }

    public void setSmsScheduledTime(Date smsScheduledTime) {
        this.smsScheduledTime = smsScheduledTime;
    }

    public String getSmsStatus() {
        return this.smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String getSmsStatusBy() {
        return this.smsStatusBy;
    }

    public void setSmsStatusBy(String smsStatusBy) {
        this.smsStatusBy = smsStatusBy;
    }

    public Date getSmsStatusDatetime() {
        return this.smsStatusDatetime;
    }

    public void setSmsStatusDatetime(Date smsStatusDatetime) {
        this.smsStatusDatetime = smsStatusDatetime;
    }

    public Integer getTelco() {
        return this.telco;
    }

    public void setTelco(Integer telco) {
        this.telco = telco;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the dept.
	 */
	public String getDept() {
		return dept;
	}

	/**
	 * @param dept The dept to set.
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getSmsScheduled() {
		return smsScheduled;
	}

	public void setSmsScheduled(String smsScheduled) {
		this.smsScheduled = smsScheduled;
	}

	public Integer getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Integer historyId) {
		this.historyId = historyId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	public String getSmsCharge() {
		return smsCharge;
	}

	public void setSmsCharge(String smsCharge) {
		this.smsCharge = smsCharge;
	}

	public String getSmsChargeMode() {
		return smsChargeMode;
	}

	public void setSmsChargeMode(String smsChargeMode) {
		this.smsChargeMode = smsChargeMode;
	}

}
