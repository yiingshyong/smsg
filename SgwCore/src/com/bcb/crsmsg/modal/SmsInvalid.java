package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class SmsInvalid implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -69409971410652197L;

	/** identifier field */
    private int id;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Integer ftp;

    /** nullable persistent field */
    private String invalidRecord;

    /** nullable persistent field */
    private String remarks;

    /** nullable persistent field */
    private String type;

    /** full constructor */
    public SmsInvalid(String createdBy, Date createdDatetime, Integer ftp, String invalidRecord, String remarks, String type) {
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.ftp = ftp;
        this.invalidRecord = invalidRecord;
        this.remarks = remarks;
        this.type = type;
    }

    /** default constructor */
    public SmsInvalid() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Integer getFtp() {
        return this.ftp;
    }

    public void setFtp(Integer ftp) {
        this.ftp = ftp;
    }

    public String getInvalidRecord() {
        return this.invalidRecord;
    }

    public void setInvalidRecord(String invalidRecord) {
        this.invalidRecord = invalidRecord;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

}
