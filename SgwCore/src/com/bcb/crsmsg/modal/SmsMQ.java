package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.bcb.sgwcore.senderprocessor.business.BusinessService;


public class SmsMQ implements Serializable {

	private static final long serialVersionUID = -999040017886428888L;
	private int mqId;
	private int telcoId;
	private int smsType;
	private int noOfThread;
	private String createdBy;
	private Date createdDatetime;
	private String modifiedBy;
	private Date modifiedDatetime;
	
	//for extra join info
	private String telcoName;
	private String value;
	private String telcoClassName;
	
	public SmsMQ(int mqId,String telcoName,String value,int noOfThread, String telcoClassName) {
		this.mqId = mqId;
		this.telcoName = telcoName;
		this.value = value;
		this.noOfThread = noOfThread;
		this.telcoClassName = telcoClassName;
	}
	
	public SmsMQ() {
	}
	public int getMqId() {
		return mqId;
	}
	public void setMqId(int mqId) {
		this.mqId = mqId;
	}
	public int getTelcoId() {
		return telcoId;
	}
	public void setTelcoId(int telcoId) {
		this.telcoId = telcoId;
	}
	public int getSmsType() {
		return smsType;
	}
	public void setSmsType(int smsType) {
		this.smsType = smsType;
	}
	public int getNoOfThread() {
		return noOfThread;
	}
	public void setNoOfThread(int noOfThread) {
		this.noOfThread = noOfThread;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}
	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	public String getTelcoName() {
		return telcoName;
	}
	public void setTelcoName(String telcoName) {
		this.telcoName = telcoName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getTelcoClassName() {
		return telcoClassName;
	}
	public void setTelcoClassName(String telcoClassName) {
		this.telcoClassName = telcoClassName;
	}
	public String getQueueName(){
		if(StringUtils.isNotEmpty(this.telcoName) && StringUtils.isNotEmpty(this.value)){
			return BusinessService.getQueueName(this.telcoName, this.value);
		}
		return null;
	}
}
