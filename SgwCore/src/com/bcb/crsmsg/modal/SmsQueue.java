package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class SmsQueue implements Serializable {

    /**
	 * 
	 */
//	public SmsQueue(SmsQueue queue, TblRef smsType){
//		try{
//			PropertyUtils.copyProperties(this, queue);
//			this.setSmsType(smsType);
//		}catch(Exception e){
//			throw new RuntimeException("Error while creating object.", e);
//		}
//	}
	private static final long serialVersionUID = -2445609013111391050L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String approvalBy;

    /** nullable persistent field */
    private Date approvalDatetime;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private Integer ftp;

    /** nullable persistent field */
    private String message;

    /** nullable persistent field */
    private String mobileNo;

    /** nullable persistent field */
    private String msgFormatType;

    /** nullable persistent field */
    private Integer priority;

    /** nullable persistent field */
    private String remarks;

    /** nullable persistent field */
    private String sentBy;

    /** nullable persistent field */
    private String smsScheduled;
    
    /** nullable persistent field */
    private Date smsScheduledTime;

    /** nullable persistent field */
    private String smsStatus;

    /** nullable persistent field */
    private String smsStatusBy;

    /** nullable persistent field */
    private Date smsStatusDatetime;

    /** nullable persistent field */
    private Integer telco;

    /** nullable persistent field */
    private String type;

    /** persistent field */
    private int version;
    
    /** persistent field */
    private String dept;
    
    /** persistent field */
    private int deptId;

	private String modifiedBy;
    
    private Date modifiedDatetime;
    
    private String smsCharge;
    
    private String smsChargeMode;
    
    private String concatenateSms;
    
    private TblRef smsType;
    
    private String telcoName;
    
    private String channelId;
    
    private int totalSms;
    
    private boolean decrypted;
    
    private Date clientTimestamp;
    
    private Integer smsRateId;
    
    private String retRefNum;    
    private String msgRefNum;
    
    
    /** full constructor */
    public SmsQueue(String approvalBy, Date approvalDatetime, String createdBy, Date createdDatetime, Integer ftp, String message, String mobileNo, String msgFormatType, Integer priority, String remarks, String sentBy, String smsScheduled, Date smsScheduledTime, String smsStatus, String smsStatusBy, Date smsStatusDatetime, Integer telco, String type, int version, String dept, int totalSms, String channelId, TblRef smsType, Integer smsRateId, String retRefNum, String msgRefNum) {
        this.approvalBy = approvalBy;
        this.approvalDatetime = approvalDatetime;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.ftp = ftp;
        this.message = message;
        this.mobileNo = mobileNo;
        this.msgFormatType = msgFormatType;
        this.priority = priority;
        this.remarks = remarks;
        this.sentBy = sentBy;
        this.smsScheduled = smsScheduled;
        this.smsScheduledTime = smsScheduledTime;
        this.smsStatus = smsStatus;
        this.smsStatusBy = smsStatusBy;
        this.smsStatusDatetime = smsStatusDatetime;
        this.telco = telco;
        this.type = type;
        this.version = version;
        this.dept=dept;
        this.totalSms = totalSms;
        this.channelId = channelId;
        this.smsType = smsType;
        this.smsRateId = smsRateId;
        this.retRefNum = retRefNum;
        this.msgRefNum = msgRefNum;
    }

    /** default constructor */
    public SmsQueue() {
    }

    /** minimal constructor */
    public SmsQueue(int version) {
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApprovalBy() {
        return this.approvalBy;
    }

    public void setApprovalBy(String approvalBy) {
        this.approvalBy = approvalBy;
    }

    public Date getApprovalDatetime() {
        return this.approvalDatetime;
    }

    public void setApprovalDatetime(Date approvalDatetime) {
        this.approvalDatetime = approvalDatetime;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public Integer getFtp() {
        return this.ftp;
    }

    public void setFtp(Integer ftp) {
        this.ftp = ftp;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMsgFormatType() {
        return this.msgFormatType;
    }

    public void setMsgFormatType(String msgFormatType) {
        this.msgFormatType = msgFormatType;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentBy() {
        return this.sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public Date getSmsScheduledTime() {
        return this.smsScheduledTime;
    }

    public void setSmsScheduledTime(Date smsScheduledTime) {
        this.smsScheduledTime = smsScheduledTime;
    }

    public String getSmsStatus() {
        return this.smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public String getSmsStatusBy() {
        return this.smsStatusBy;
    }

    public void setSmsStatusBy(String smsStatusBy) {
        this.smsStatusBy = smsStatusBy;
    }

    public Date getSmsStatusDatetime() {
        return this.smsStatusDatetime;
    }

    public void setSmsStatusDatetime(Date smsStatusDatetime) {
        this.smsStatusDatetime = smsStatusDatetime;
    }

    public Integer getTelco() {
        return this.telco;
    }

    public void setTelco(Integer telco) {
        this.telco = telco;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	/**
	 * @return Returns the smsScheduled.
	 */
	public String getSmsScheduled() {
		return smsScheduled;
	}

	/**
	 * @param smsScheduled The smsScheduled to set.
	 */
	public void setSmsScheduled(String smsScheduled) {
		this.smsScheduled = smsScheduled;
	}

	/**
	 * @return Returns the dept.
	 */
	public String getDept() {
		return dept;
	}

	/**
	 * @param dept The dept to set.
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	public String getSmsCharge() {
		return smsCharge;
	}

	public void setSmsCharge(String smsCharge) {
		this.smsCharge = smsCharge;
	}

	public String getSmsChargeMode() {
		return smsChargeMode;
	}

	public void setSmsChargeMode(String smsChargeMode) {
		this.smsChargeMode = smsChargeMode;
	}

	public String getConcatenateSms() {
		return concatenateSms;
	}

	public void setConcatenateSms(String concatenateSms) {
		this.concatenateSms = concatenateSms;
	}

	public int getTotalSms() {
		return totalSms;
	}

	public void setTotalSms(int totalSms) {
		this.totalSms = totalSms;
	}

	public String getTelcoName() {
		return telcoName;
	}

	public void setTelcoName(String telcoName) {
		this.telcoName = telcoName;
	}

	public TblRef getSmsType() {
		return smsType;
	}

	public void setSmsType(TblRef smsType) {		
		this.smsType = smsType;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public boolean isDecrypted() {
		return decrypted;
	}

	public void setDecrypted(boolean decrypted) {
		this.decrypted = decrypted;
	}

	public Date getClientTimestamp() {
		return clientTimestamp;
	}

	public void setClientTimestamp(Date clientTimestamp) {
		this.clientTimestamp = clientTimestamp;
	}

	public Integer getSmsRateId() {
		return smsRateId;
	}

	public void setSmsRateId(Integer smsRateId) {
		this.smsRateId = smsRateId;
	}

	public String getRetRefNum() {
		return retRefNum;
	}

	public void setRetRefNum(String retRefNum) {
		this.retRefNum = retRefNum;
	}

	public String getMsgRefNum() {
		return msgRefNum;
	}

	public void setMsgRefNum(String msgRefNum) {
		this.msgRefNum = msgRefNum;
	}
}
