package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class SmsQuota implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2365596969736667066L;

	/** identifier field */
    private Integer id;

    /** persistent field */
    private int quota;

    /** persistent field */
    private Integer ref;

    /** persistent field */
    private int totalQuota;
    
    private String createdBy;
    private Date createdDatetime;
    private String modifiedBy;
    private Date modifiedDatetime;

    /** full constructor */
    public SmsQuota(int quota, Integer ref) {
        this.quota = quota;
        this.ref = ref;
    }

    /** default constructor */
    public SmsQuota() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuota() {
        return this.quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public Integer getRef() {
        return this.ref;
    }

    public void setRef(Integer ref) {
        this.ref = ref;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public int getTotalQuota() {
		return totalQuota;
	}

	public void setTotalQuota(int totalQuota) {
		this.totalQuota = totalQuota;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

}
