package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.math.BigDecimal;

public class SmsRate implements Serializable {

	private Integer id;
	private String name;
	private BigDecimal rate;
	private String countryCodes;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getCountryCodes() {
		return countryCodes;
	}
	public void setCountryCodes(String countryCodes) {
		this.countryCodes = countryCodes;
	}
	
}
