package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;


/** @author Hibernate CodeGenerator */
public class SmsReceived implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8872855692300374542L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private Integer keyword;

    /** nullable persistent field */
    private String message;

    /** nullable persistent field */
    private String mobileNo;

    /** nullable persistent field */
    private Date receivedDatetime;

    /** nullable persistent field */
    private String remarks;

    /** persistent field */
    private int version;

    private String smsCharge;
    
    private String concatenateSms;
    
    private String dept;
    
    private String telco;
    
    private transient String msgNoKeyword;
    
    public String getConcatenateSms() {
		return concatenateSms;
	}

	public void setConcatenateSms(String concatenateSms) {
		this.concatenateSms = concatenateSms;
	}

	/** full constructor */
    public SmsReceived(Integer keyword, String message, String mobileNo, Date receivedDatetime, String remarks, int version) {
        this.keyword = keyword;
        this.message = message;
        this.mobileNo = mobileNo;
        this.receivedDatetime = receivedDatetime;
        this.remarks = remarks;
        this.version = version;
    }

    /** default constructor */
    public SmsReceived() {
    }

    /** minimal constructor */
    public SmsReceived(int version) {
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKeyword() {
        return this.keyword;
    }

    public void setKeyword(Integer keyword) {
        this.keyword = keyword;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Date getReceivedDatetime() {
        return this.receivedDatetime;
    }

    public void setReceivedDatetime(Date receivedDatetime) {
        this.receivedDatetime = receivedDatetime;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getSmsCharge() {
		return smsCharge;
	}

	public void setSmsCharge(String smsCharge) {
		this.smsCharge = smsCharge;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getTelco() {
		return telco;
	}

	public void setTelco(String telco) {
		this.telco = telco;
	}

	public String getMsgNoKeyword() {
		return msgNoKeyword;
	}

	public void setMsgNoKeyword(String msgNoKeyword) {
		this.msgNoKeyword = msgNoKeyword;
	}

}
