package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

/** @author Hibernate CodeGenerator */
public class SmsTimeControl implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7374509859277576273L;
	/** identifier field */
    private Integer id;

    /** not nullable persistent field */
    private Integer dept;
    
    /** nullable persistent field */
    private String smsTimeControlBatch;

    /** nullable persistent field */
    private String smsTimeControlWeb;
    
    /** nullable persistent field */
    private String smsTimeControlDay;

    /** nullable persistent field */
    private String smsTimeControlTimeFrom;
    
    /** nullable persistent field */
    private String smsTimeControlTimeTo;

    private String status;
	private String createdBy;
	private Date createdDatetime;
	private String modifiedBy;
	private Date modifiedDatetime;
	private Boolean autoPatchToUnsent;
	
	/** full constructor */
    public SmsTimeControl(String smsTimeControlBatch, String smsTimeControlDay, String smsTimeControlTimeFrom, String smsTimeControlTimeTo, String smsTimeControlWeb) {
    	this.smsTimeControlBatch = smsTimeControlBatch;
        this.smsTimeControlDay = smsTimeControlDay;
        this.smsTimeControlTimeFrom = smsTimeControlTimeFrom;
        this.smsTimeControlTimeTo = smsTimeControlTimeTo;
        this.smsTimeControlWeb = smsTimeControlWeb;
    }

    /** default constructor */
    public SmsTimeControl() {
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSmsTimeControlBatch() {
		return smsTimeControlBatch;
	}

	public void setSmsTimeControlBatch(String smsTimeControlBatch) {
		this.smsTimeControlBatch = smsTimeControlBatch;
	}

	public String getSmsTimeControlDay() {
		return smsTimeControlDay;
	}

	public void setSmsTimeControlDay(String smsTimeControlDay) {
		this.smsTimeControlDay = smsTimeControlDay;
	}

	public String getSmsTimeControlTimeFrom() {
		return smsTimeControlTimeFrom;
	}

	public void setSmsTimeControlTimeFrom(String smsTimeControlTimeFrom) {
		this.smsTimeControlTimeFrom = smsTimeControlTimeFrom;
	}

	public String getSmsTimeControlTimeTo() {
		return smsTimeControlTimeTo;
	}

	public void setSmsTimeControlTimeTo(String smsTimeControlTimeTo) {
		this.smsTimeControlTimeTo = smsTimeControlTimeTo;
	}

	public String getSmsTimeControlWeb() {
		return smsTimeControlWeb;
	}

	public void setSmsTimeControlWeb(String smsTimeControlWeb) {
		this.smsTimeControlWeb = smsTimeControlWeb;
	}

	public Integer getDept() {
		return dept;
	}

	public void setDept(Integer dept) {
		this.dept = dept;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getAutoPatchToUnsent() {
		return autoPatchToUnsent;
	}

	public void setAutoPatchToUnsent(Boolean autoPatchToUnsent) {
		this.autoPatchToUnsent = autoPatchToUnsent;
	}
}
