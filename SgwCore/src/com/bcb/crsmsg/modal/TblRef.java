package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.bcb.common.modal.Option;
import com.bcb.common.util.Constants;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class TblRef implements Serializable {

	private static final long serialVersionUID = -999040017886428888L;
	private int tblRefId;
	private String tblName;
	private String colName;
	private String value;
	private String label;
	private String httpAction;
	private String routingAction;
	private String routableTelco;
	private boolean encrypted;
	private boolean online;
	private String status;
	
	//transient
	private Map<String, Telco> telcoMap;
	
	
	public int getTblRefId() {
		return tblRefId;
	}
	public void setTblRefId(int tblRefId) {
		this.tblRefId = tblRefId;
	}
	public String getTblName() {
		return tblName;
	}
	public void setTblName(String tblName) {
		this.tblName = tblName;
	}
	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHttpAction() {
		return httpAction;
	}
	public void setHttpAction(String httpAction) {
		this.httpAction = httpAction;
	}
	public String getRoutingAction() {
		return routingAction;
	}
	public void setRoutingAction(String routingAction) {
		this.routingAction = routingAction;
	}
	public String getRoutableTelco() {
		return routableTelco;
	}
	public void setRoutableTelco(String routableTelco) {
		this.routableTelco = routableTelco;
	}
	public boolean isEncrypted() {
		return encrypted;
	}
	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
	
	//for front end display
	public String getDisplayRoutingAction(){
		if(StringUtils.isNotEmpty(this.routingAction)){
			for(Option o : CommonUtils.getRoutingRulesOption()){
				if(o.getValue().equals(this.routingAction)){
					return o.getName();
				}					
			}
		}
		return null;
	}
	
	public String getDisplayEncrypted(){
		return this.isEncrypted()? "YES" : "NO";
	}
	
	public String getDisplayOnline(){
		return this.isOnline()? "YES" : "NO";
	}
	
	public String getDisplayRoutableTelco(){
		if(StringUtils.isEmpty(this.routableTelco)) return null;
		StringBuffer sb = new StringBuffer();
		if(this.telcoMap == null){
			this.telcoMap = new HashMap<String, Telco>();
			List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAll();
			for(Telco t : telcos){
				telcoMap.put(String.valueOf(t.getId()), t);
			}
		}
		String[] ids = StringUtils.split(this.getRoutableTelco(),"|");
		for(String id : ids){
			sb.append(telcoMap.get(id).getTelcoName());
			sb.append(Constants.LINE_SEPARATOR);
		}
		return StringUtils.removeEnd(sb.toString(), Constants.LINE_SEPARATOR);
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}
	
}
