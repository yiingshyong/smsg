package com.bcb.crsmsg.modal;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class Telco implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4765212516081830140L;

	/** identifier field */
    private Integer id;

    /** nullable persistent field */
    private String costPerSmsInter;

    /** nullable persistent field */
    private String costPerSmsWithin;

    /** nullable persistent field */
    private String createdBy;

    /** nullable persistent field */
    private Date createdDatetime;

    /** nullable persistent field */
    private String modifiedBy;

    /** nullable persistent field */
    private Date modifiedDatetime;

    /** nullable persistent field */
    private String password;

    /** nullable persistent field */
    private Integer priority;

    /** nullable persistent field */
    private String remarks;

    /** nullable persistent field */
    private String smsOnHand;

    /** nullable persistent field */
    private String smsOnHandLimit;
    
    /** nullable persistent field */
    private String smsQueueThreshold;

    /** nullable persistent field */
    private Date smsQueueThresholdNotificationDatetime;
    
    /** nullable persistent field */
    private String smsQueueThresholdNo;

    /** nullable persistent field */
    private String status;
    
    /** nullable persistent field */
    private Date statusDatetime;

    /** nullable persistent field */
    private String telcoClassName;

    /** nullable persistent field */
    private String telcoCodeId;

    /** nullable persistent field */
    private String telcoName;

    /** nullable persistent field */
    private String telcoPrefix;

    /** nullable persistent field */
    private String telcoPrefixMatch;

    /** nullable persistent field */
    private String thresholdNotification;

    /** nullable persistent field */
    private String thresholdNotificationReceiver;

    /** nullable persistent field */
    private String thresholdNotificationSender;

    /** nullable persistent field */
    private String url;

    /** nullable persistent field */
    private String urlParam;
    
    /** nullable persistent field */
    private String userId;

    /** persistent field */
    private int version;

    /** nullable persistent field */
    private String shortCode;
    
    /** nullable persistent field */
    private String successCode;
    
    /** nullable persistent field */
    private String retryCode;

    private String prefixWithin;
    
    private String costIncoming;
    
    private String prefixIncoming;
    
    private Date lastStatusChecked;
    
    private String proxyAddr;
    
    private int proxyPort;
    
    private int svcPvdId;
    
    private String encryptionStatus;
    /** full constructor */
    public Telco(String costPerSmsInter, String costPerSmsWithin, String createdBy, Date createdDatetime, String modifiedBy, 
    		Date modifiedDatetime, String password, Integer priority, String remarks, String smsOnHand, String smsOnHandLimit, 
    		String smsQueueThreshold, String smsQueueThresholdNo, String status, String telcoClassName, String telcoCodeId, 
    		String telcoName, String telcoPrefix, String telcoPrefixMatch, String thresholdNotification, String thresholdNotificationReceiver, 
    		String thresholdNotificationSender, String url, String urlParam, String userId, int version, String shortCode, 
    		String successCode, String retryCode, String costIncoming, String prefixIncoming, String proxyAddr, int proxyPort) {
    	
        this.costPerSmsInter = costPerSmsInter;
        this.costPerSmsWithin = costPerSmsWithin;
        this.createdBy = createdBy;
        this.createdDatetime = createdDatetime;
        this.modifiedBy = modifiedBy;
        this.modifiedDatetime = modifiedDatetime;
        this.password = password;
        this.priority = priority;
        this.remarks = remarks;
        this.smsOnHand = smsOnHand;
        this.smsOnHandLimit=smsOnHandLimit;
        this.smsQueueThreshold = smsQueueThreshold;
        this.smsQueueThresholdNo = smsQueueThresholdNo;
        this.status = status;
        this.telcoClassName = telcoClassName;
        this.telcoCodeId = telcoCodeId;
        this.telcoName = telcoName;
        this.telcoPrefix = telcoPrefix;
        this.telcoPrefixMatch = telcoPrefixMatch;
        this.thresholdNotification = thresholdNotification;
        this.thresholdNotificationReceiver = thresholdNotificationReceiver;
        this.thresholdNotificationSender = thresholdNotificationSender;
        this.url = url;
        this.urlParam=urlParam;
        this.userId = userId;
        this.version = version;
        this.shortCode=shortCode;
        this.successCode=successCode;
        this.retryCode=retryCode;
        this.costIncoming = costIncoming;
        this.prefixIncoming = prefixIncoming;
        this.proxyAddr = proxyAddr;
        this.proxyPort = proxyPort;
        this.encryptionStatus =encryptionStatus;
    }

    /** default constructor */
    public Telco() {
    }

    /** minimal constructor */
    public Telco(int version) {
        this.version = version;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCostPerSmsInter() {
        return this.costPerSmsInter;
    }

    public void setCostPerSmsInter(String costPerSmsInter) {
        this.costPerSmsInter = costPerSmsInter;
    }

    public String getCostPerSmsWithin() {
        return this.costPerSmsWithin;
    }

    public void setCostPerSmsWithin(String costPerSmsWithin) {
        this.costPerSmsWithin = costPerSmsWithin;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDatetime() {
        return this.modifiedDatetime;
    }

    public void setModifiedDatetime(Date modifiedDatetime) {
        this.modifiedDatetime = modifiedDatetime;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSmsOnHand() {
        return this.smsOnHand;
    }

    public void setSmsOnHand(String smsOnHand) {
        this.smsOnHand = smsOnHand;
    }

    public String getSmsQueueThreshold() {
        return this.smsQueueThreshold;
    }

    public void setSmsQueueThreshold(String smsQueueThreshold) {
        this.smsQueueThreshold = smsQueueThreshold;
    }

    public String getSmsQueueThresholdNo() {
        return this.smsQueueThresholdNo;
    }

    public void setSmsQueueThresholdNo(String smsQueueThresholdNo) {
        this.smsQueueThresholdNo = smsQueueThresholdNo;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTelcoClassName() {
        return this.telcoClassName;
    }

    public void setTelcoClassName(String telcoClassName) {
        this.telcoClassName = telcoClassName;
    }

    public String getTelcoCodeId() {
        return this.telcoCodeId;
    }

    public void setTelcoCodeId(String telcoCodeId) {
        this.telcoCodeId = telcoCodeId;
    }

    public String getTelcoName() {
        return this.telcoName;
    }

    public void setTelcoName(String telcoName) {
        this.telcoName = telcoName;
    }

    public String getTelcoPrefix() {
        return this.telcoPrefix;
    }

    public void setTelcoPrefix(String telcoPrefix) {
        this.telcoPrefix = telcoPrefix;
    }

    public String getTelcoPrefixMatch() {
        return this.telcoPrefixMatch;
    }

    public void setTelcoPrefixMatch(String telcoPrefixMatch) {
        this.telcoPrefixMatch = telcoPrefixMatch;
    }

    public String getThresholdNotification() {
        return this.thresholdNotification;
    }

    public void setThresholdNotification(String thresholdNotification) {
        this.thresholdNotification = thresholdNotification;
    }

    public String getThresholdNotificationReceiver() {
        return this.thresholdNotificationReceiver;
    }

    public void setThresholdNotificationReceiver(String thresholdNotificationReceiver) {
        this.thresholdNotificationReceiver = thresholdNotificationReceiver;
    }

    public String getThresholdNotificationSender() {
        return this.thresholdNotificationSender;
    }

    public void setThresholdNotificationSender(String thresholdNotificationSender) {
        this.thresholdNotificationSender = thresholdNotificationSender;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("id", getId())
            .toString();
    }

	public String getUrlParam() {
		return urlParam;
	}

	public void setUrlParam(String urlParam) {
		this.urlParam = urlParam;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}

	public String getSmsOnHandLimit() {
		return smsOnHandLimit;
	}

	public void setSmsOnHandLimit(String smsOnHandLimit) {
		this.smsOnHandLimit = smsOnHandLimit;
	}

	public String getRetryCode() {
		return retryCode;
	}

	public void setRetryCode(String retryCode) {
		this.retryCode = retryCode;
	}

	public String getPrefixWithin() {
		return prefixWithin;
	}

	public void setPrefixWithin(String prefixInter) {
		this.prefixWithin = prefixInter;
	}

	public Date getStatusDatetime() {
		return statusDatetime;
	}

	public void setStatusDatetime(Date statusDatetime) {
		this.statusDatetime = statusDatetime;
	}

	public Date getSmsQueueThresholdNotificationDatetime() {
		return smsQueueThresholdNotificationDatetime;
	}

	public void setSmsQueueThresholdNotificationDatetime(
			Date smsQueueThresholdNotificationDatetime) {
		this.smsQueueThresholdNotificationDatetime = smsQueueThresholdNotificationDatetime;
	}

	public String getCostIncoming() {
		return costIncoming;
	}

	public void setCostIncoming(String costIncoming) {
		this.costIncoming = costIncoming;
	}

	public String getPrefixIncoming() {
		return prefixIncoming;
	}

	public void setPrefixIncoming(String prefixIncoming) {
		this.prefixIncoming = prefixIncoming;
	}

	public Date getLastStatusChecked() {
		return lastStatusChecked;
	}

	public void setLastStatusChecked(Date lastStatusChecked) {
		this.lastStatusChecked = lastStatusChecked;
	}

	public String getProxyAddr() {
		return proxyAddr;
	}

	public void setProxyAddr(String proxyAddr) {
		this.proxyAddr = proxyAddr;
	}

	public int getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	public int getSvcPvdId() {
		return svcPvdId;
	}

	public void setSvcPvdId(int svcPvdId) {
		this.svcPvdId = svcPvdId;
	}

	public String getEncryptionStatus() {
		return encryptionStatus;
	}

	public void setEncryptionStatus(String encryptionStatus) {
		this.encryptionStatus = encryptionStatus;
	}
	
}
