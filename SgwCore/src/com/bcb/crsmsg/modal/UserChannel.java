package com.bcb.crsmsg.modal;

import java.io.Serializable;

public class UserChannel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private UserChannelID userChannelIdObj;
	private TblRef smsType;
	private String status;
	private int timeValidity;
		
	public UserChannel() {
		this.smsType = new TblRef();
		this.userChannelIdObj = new UserChannelID();
	}

	public TblRef getSmsType() {
		return smsType;
	}

	public void setSmsType(TblRef smsType) {
		this.smsType = smsType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserChannelID getUserChannelIdObj() {
		return userChannelIdObj;
	}

	public void setUserChannelIdObj(UserChannelID userChannelIdObj) {
		this.userChannelIdObj = userChannelIdObj;
	}

	public int getTimeValidity() {
		return timeValidity;
	}

	public void setTimeValidity(int timeValidity) {
		this.timeValidity = timeValidity;
	}
	
}
