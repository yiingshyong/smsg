package com.bcb.crsmsg.modal;

import java.io.Serializable;

public class UserChannelID implements Serializable {

	private static final long serialVersionUID = 1L;
	private String channelId;
	private String userId;
	
	public UserChannelID() {
		super();
	}

	public UserChannelID(String channelId, String userId) {
		super();
		this.channelId = channelId;
		this.userId = userId;
	}
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0 == null) return false;
		if(!(arg0 instanceof UserChannelID)) return false;
		UserChannelID arg1 = (UserChannelID) arg0;
		return (this.channelId.equals(arg1.getChannelId()) && this.userId.equals(arg1.getUserId()));
	}
	
	@Override
	public int hashCode() {
		int hsCode;
		hsCode = channelId.hashCode();
		hsCode = 19 * hsCode+ userId.hashCode();
		return hsCode;
	}	
}
