package com.bcb.crsmsg.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class AppDBConfig {
	
	private static final Log log = LogFactory.getLog(AppDBConfig.class);
	private static AppDBConfig instance;
	private SmsCfg smsCfg;
	private TblRef defaultSmsType;
	private TblRef systemSmsType;
	
	static{
		try {
			AppDBConfig.init();
		} catch (Exception e) {
			log.error("Error initializing ...Reason: " + e.getMessage(), e);
		}
	}
	
	private AppDBConfig() {
		super();
	}
	
	private static void init() throws Exception{
		log.info("Initializing application db cache");
		instance = new AppDBConfig();
		instance.smsCfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		if(instance.getSmsCfg().getDefaultSMSType() == 0) throw new SgwException("Default SMS Type not set!");
		if(StringUtils.isEmpty(instance.getSmsCfg().getSystemSMSSender())) throw new SgwException("System SMS Sender not set!");
		instance.defaultSmsType = (TblRef) DaoBeanFactory.getHibernateTemplate().get(TblRef.class, instance.getSmsCfg().getDefaultSMSType());
		if(instance.defaultSmsType == null){
			throw new SgwException("Default SMS Type not found. Id: " + instance.getSmsCfg().getDefaultSMSType());
		}
		instance.systemSmsType = DaoBeanFactory.getTblRefDao().selectSMSType(instance.getSmsCfg().getSystemSMSSender(), null);  
	}
	
	public static AppDBConfig getInstance(){
		return instance;
	}

	public SmsCfg getSmsCfg() {
		return DaoBeanFactory.getSmsCfgDao().findSmsCfg();
//		return smsCfg;
	}

	public TblRef getDefaultSmsType() {
		return (TblRef) DaoBeanFactory.getHibernateTemplate().get(TblRef.class, getSmsCfg().getDefaultSMSType());
//		return defaultSmsType;
	}

	public TblRef getSystemSmsType() {
		return systemSmsType;
	}
	
}
