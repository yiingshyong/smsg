package com.bcb.crsmsg.util;


import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AppPropertiesConfig {

	private static final Log log = LogFactory.getLog(AppPropertiesConfig.class);
	private static AppPropertiesConfig instance;
	private Configuration settings;
	
	//variables in properties
	public static final String MQ_HOST = AppPropertiesConfig.getInstance().getSettings().getString("activemq.host");
	public static final String MQ_PORT = AppPropertiesConfig.getInstance().getSettings().getString("activemq.port");
	public static final String MQ_JMX_PORT = AppPropertiesConfig.getInstance().getSettings().getString("activemq.jmx.port");
	public static final String MK_SIM_RETRY_CODE = "MK.sim.code.retry";
	public static final String MK_SIM_SUCCESS_CODE = "MK.sim.code.success";
	public static final String MK_SIM_STATUS = "MK.sim.status";
	public static final String IONNEX_SIM_RETRY_CODE = "ionnex.sim.code.retry";
	public static final String IONNEX_SIM_SUCCESS_CODE = "ionnex.sim.code.success";
	public static final String IONNEX_SIM_STATUS = "ionnex.sim.status";	
	public static final String GENERIC_SVCPVD_SIM_RETRY_CODE = "genericsvcpvd.sim.code.retry";
	public static final String GENERIC_SVCPVD_SIM_SUCCESS_CODE = "genericsvcpvd.sim.code.success";
	public static final String GENERIC_SVCPVD_SIM_STATUS = "genericsvcpvd.sim.status";
	public static final String NOTIFICATION_SMTP_IP = "notification.stmp.ip";
	public static final String HTTP_CLIENT_CONNECT_TIMEOUT = "httpclient.timeout.connect";
	public static final String HTTP_CLIENT_CONNECT_READ = "httpclient.timeout.read";
	public static final String SP_SINGLE_RETRY = "sp.singe.retry";
	public static final String SMS_FAILED_ALERT_IMMEDIATE = "sp.smsfailedAlert.immediate";
	public static final String SP_FAILEDOVER_ON_RETRY = "sp.retry.failover";
	public static final String MQ_ASYNC_SEND = "mq.asynSend";
	public static final String MQ_PERSISTENCE_MODE = "mq.persistent";
	public static final String BP_RESET_MQ_MSGS = "bp.reset.mq.msgs";
	public static final String HTTP_POSTING_PARAM_CHANNEL = "httpposting.param.channel";
	public static final String HTTP_POSTING_PARAM_CLIENT_TIMESTAMP = "httpposting.param.timestamp";
	public static final String HTTP_POSTING_PARAM_CLIENT_TIMESTAMP_FORMAT = "httpposting.param.timestamp.format";
	public static final String TIME_EXPIRED_SMS_REMARK = "sms.time.expired.remark";
	public static final String SMS_RECEIVED_MK_SHORTCODE = "sms.received.mk.shortcode";
	public static final String SMS_COUNTRY_CODE_MAX_LENGTH = "sms.countrycode.maxlength";
	public static final String SMSG_AUTO_CLOSE_SESSION_END_REQUEST = "smsg.sessionAutoCloseAfterRequest";
	public static final String SIBS_CTRL_FILE_NAME = "sibs.batch.ctrlFileName";
	public static final String SIBS_CTRL_FILE_DATA = "sibs.batch.ctrlFileData";
	public static final String CC_MASKING_POS = "cc.masking.pos";
	public static final String CC_MASKING_CHAR = "cc.masking.char";
	public static final String CC_ACT_HOST_MSG_DEFAULT_4 = "cc.act.hostMsgDefault4";
	public static final String CC_ACT_HTTP_ACTION = "cc.act.http.action";
	public static final String SMSG_PUBLIC_URL="smsg.publicURL";
	public static final String MULTIPLE_SMS_ALERT="alertMsg.multipleSms";
	
	
	private AppPropertiesConfig() {
	}
	
	public static AppPropertiesConfig getInstance(){
		try{
			if(instance == null){
				log.info("Initialising resource config file ...");
				instance = new AppPropertiesConfig();
				instance.setSettings(new PropertiesConfiguration("settings.properties"));
				((PropertiesConfiguration)instance.getSettings()).setAutoSave(true);
				((PropertiesConfiguration)instance.getSettings()).setReloadingStrategy(new FileChangedReloadingStrategy());
			}
			return instance;
		}catch(Exception e){
			log.error("Error while initialising config file. Reason: " + e);
			throw new RuntimeException(e);
		}		
	}

	public Configuration getSettings() {
		return settings;
	}

	public void setSettings(Configuration settings) {
		this.settings = settings;
	}
	
}
