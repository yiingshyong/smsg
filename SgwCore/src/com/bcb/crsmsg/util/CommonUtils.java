package com.bcb.crsmsg.util;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.Option;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.OutboxModal;
import com.bcb.crsmsg.modal.Record;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CommonUtils {
	public static String getPropValue(String inType, String argument){
		String outString = Constants.STR_EMPTY; 
		try
		{
			if (!argument.trim().equals(Constants.STR_EMPTY))
			{
				ResourceBundle bundle = ResourceBundle.getBundle ("com.bcb.crsmsg.util." + inType);
				outString = bundle.getString(argument.trim());
			}
		}
		catch (Exception e)
		{
			System.out.println(inType + " >> " + e);
		}
		
		return outString;
		
	}
	
	public static Result populateResult(List resultList){
		Result theResult=new Result();
		Record aRecord=null;
		ArrayList<Record> recordList=new ArrayList<Record>();
		ArrayList<String> columnList=new ArrayList<String>();//category, classification, details
		Object[] aRecordObj=null;

		for(int i=0;i<resultList.size();i++){
			aRecordObj=(Object[])resultList.get(i);// a record
			aRecord=new Record();
			columnList=new ArrayList<String>();
			for(int j=0;j<aRecordObj.length;j++){
				columnList.add(NullChecker(aRecordObj[j], aRecordObj.getClass()).toString());
			}
			aRecord.setColumn(columnList);
			recordList.add(aRecord);
		}
		
		theResult.setRecord(recordList);
		
		return theResult;
	}
	
	public static String getTelcoName(int telcoId)
	{
		String outString = Constants.STR_EMPTY;
		
		try{
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select telco_name from telco where telco_id = ?").setInteger(0,telcoId).list();
			
			if (resultList.size() == 1){
				outString = resultList.get(0).toString();
			}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		
		
		return outString;
	}
	
	public static String getDeptName(String userId)
	{
		String outString = Constants.STR_EMPTY;
		
		try{
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select dept_name from department d where dept_id = ? ")
			.setInteger(0,Integer.parseInt(userId)).list();
			
			if (resultList.size() == 1){
				outString = resultList.get(0).toString();
			}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		
		
		return outString;
	}
	
	public static String getStatus(String status)
	{
		String outString = Constants.STR_EMPTY;
		
		try{
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			//hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select status_name from sms_status where status_code = ?").setString(0,status).list();
			
			if (resultList.size() == 1)
			{
				outString = resultList.get(0).toString();
			}
			
			
			
			
			////System.out.println("getUserName >> " + outString);
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		
		
		return outString;
	}
	
	public static int getPendingCount(String user)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=null;
				if(user.length()>0){
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status in ('Q', 'R', 'T', 'V') and sent_by in(" + user + ")").list();
				}else{
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status in ('Q', 'R', 'T', 'V')").list();
				}
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getPendingApprovalCount(String user)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				List resultList=null;
				
				if(user.length()>0){
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status='A'  and sent_by in(" + user + ")").list();
				}else{
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status='A'").list();
				}
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getFailedCount(String user)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=null;
				if(user.length()>0){
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='U' and sent_by in(" + user + ")").list();
				}else{
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='U'").list();
				}
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			System.out.println("ERROR : " + ex);
			ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getSentCount(String user)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=null;
				if(user.length()>0){
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='S' and sent_by in(" + user + ")").list();
				}else{
					resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='S'").list();
				}
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getFtpSentCount(int id)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='S' and ftp=?").setInteger(0,id).list();
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	
	public static int getFtpFailedCount(int id)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms where sms_status='U' and ftp=?").setInteger(0,id).list();
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			System.out.println("ERROR : " + ex);
			ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getFtpPendingApprovalCount(int id)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status='A' and ftp=?").setInteger(0,id).list();
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getFtpPendingVerificationCount(int id)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status='V' and ftp=?").setInteger(0,id).list();
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	
	public static int getFtpPendingCount(int id)
	{
		int outString = 0;
		
		try{
	
				Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				List resultList=hibernate_session.createSQLQuery("select count(sms_id) from sms_queue where sms_status in ('Q', 'R', 'T') and ftp=?").setInteger(0,id).list();
				
				if (resultList.size() == 1)
				{
					outString = Integer.parseInt(String.valueOf((BigInteger)resultList.get(0)));
				}
			
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		////System.out.println("getContractType >> " + outString);
		
		return outString;
	}
	/**
	 * 
	 * @param obj		Object to be checked for null
	 * @param classType	Class type of the object to be checked
	 * @return			Original object if is no null, new instance of object if is null
	 */
	public static Object NullChecker(Object obj, Class classType){
		if(obj==null){// Object is null
			if(classType.equals(String.class)){
				return Constants.STR_EMPTY;
			}else if(classType.equals(Integer.class)){
				return Integer.valueOf(0);
			}else if(classType.equals(Double.class)){
				return Double.valueOf(0);
			}else if(classType.equals(Date.class)){
				return Constants.STR_EMPTY;
			}else{
				try{
					obj=Constants.STR_EMPTY;
				}catch (Exception ex){
					ex.printStackTrace();
				}
				return obj;
			}
		}else{// Object is not null
			return obj;
		}
	}
	
	public static String escapeDelimiter(String delimiter){
		if(delimiter.equals(Constants.SPECIAL_CHAR_PIPE)){
			return "["+delimiter+"]";
		}else{
			return delimiter;
		}
	}
	
	public static int calculateCharPerSms(String msgFormatType, String defaultNoChar, String smsConcatenate, String maxSMSNo){
		int  header=40;// header 40 byes (assume)
		int noOfChar=Integer.valueOf(defaultNoChar);
		
		if (msgFormatType.equals(Constants.FORM_FIELD_CHARSET_ASCIITEXT)){// ACII/Text
			if(!maxSMSNo.equals(Constants.VALUE_1)&&smsConcatenate.equals(Constants.STATUS_YES)){
				noOfChar -=7;
			}
		}else if (msgFormatType.equals(Constants.FORM_FIELD_CHARSET_UTF8)){// UTF-8
			noOfChar=noOfChar*2-header;
			noOfChar /= 4;//4 hexadecimal char (2 bytes) into one chinese char
			if(!maxSMSNo.equals(Constants.VALUE_1)&&smsConcatenate.equals(Constants.STATUS_YES)){
				noOfChar -=3;
			}
		}
		
		return noOfChar;
	}
	
	public static int rndNextInt(int number, int divider){
		int nextInd=number/divider;
		if(number%divider>0){
			nextInd+=1;
		}
		return nextInd;
	}
	
	public static ArrayList<String>strToArray(String aStr, int arraySize){
		ArrayList<String> msgArray=new ArrayList<String>();
		while(NullChecker(aStr, String.class).toString().length()>0){
			if(aStr.length()>arraySize){
				msgArray.add(aStr.substring(0, arraySize));
				aStr=aStr.substring(arraySize);
			}else{
				msgArray.add(aStr);
				aStr=Constants.STR_EMPTY;
			}
		}
		if(msgArray.size()>1 && msgArray.get(msgArray.size()-1).length()>146){
			msgArray.add("");// when no of sms > 1, total reverse character=7+no of sms *7
		}
		return msgArray;
	}
	
	public static List<String> strToArray(String aStr, List<String> msgArrayList, String seperator){
		String[] msgArray=NullChecker(aStr, String.class).toString().split("["+seperator+"]");
		
		for(int i=0;i<msgArray.length;i++){
			msgArrayList.add(msgArray[i]);
		}
		return msgArrayList;
	}
	
	public static List<String> strToArray(String aStr, String seperator){
		List<String> msgArrayList=new ArrayList<String>();
		return strToArray(aStr, msgArrayList, seperator);
	}
	
	public static String arrayToString(ArrayList<String> arrayStr, String seperator){
		String returnStr=Constants.STR_EMPTY;
		for(int i=0;arrayStr!=null&&i<arrayStr.size();i++){
			if(i!=0)returnStr+=seperator;
			returnStr+=arrayStr.get(i);
		}
		return returnStr;
	}
	
	public static String arrayToString(String[] arrayStr, String seperator){
		String returnStr=Constants.STR_EMPTY;
		for(int i=0;arrayStr!=null&&i<arrayStr.length;i++){
			if(i!=0)returnStr+=seperator;
			returnStr+=arrayStr[i];
		}
		return returnStr;
	}
	
	public static ArrayList<OutboxModal> populateOutboxModal(List prList_result){
		Iterator resultIter = prList_result.iterator();
		ArrayList<OutboxModal> dataResult = new ArrayList<OutboxModal>();
		int runningCounter = 0;

		while (resultIter.hasNext()){
			runningCounter += 1;
			Object[] rowResult = (Object[]) resultIter.next();	
			OutboxModal outBox= new OutboxModal();
			outBox.setId(Integer.valueOf(CommonUtils.NullChecker(rowResult[0], Integer.class).toString()));
			outBox.setSeq(String.valueOf(runningCounter));
			if(rowResult[1]!=null){
				outBox.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
			}else{
				outBox.setDate(Constants.STR_EMPTY);
			}
			outBox.setMobileNo(CommonUtils.NullChecker(rowResult[2], String.class).toString());
			outBox.setMessage(CommonUtils.NullChecker(rowResult[3], String.class).toString());
			outBox.setSentBy(CommonUtils.NullChecker(rowResult[4], String.class).toString());
			outBox.setDept(CommonUtils.NullChecker(rowResult[5], String.class).toString() );
			if(rowResult[6]!=null&&CommonUtils.NullChecker(rowResult[14], String.class).toString().equals(Constants.STATUS_YES)){
				outBox.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
			}else{
				outBox.setScheduled(Constants.STR_EMPTY);
			}
			outBox.setMode(Constants.STR_EMPTY);
			outBox.setType(CommonUtils.NullChecker(rowResult[7], String.class).toString());
			outBox.setRemarks(CommonUtils.NullChecker(rowResult[8], String.class).toString());
			outBox.setApproval(CommonUtils.NullChecker(rowResult[9], String.class).toString());
			outBox.setPriority(CommonUtils.NullChecker(rowResult[10], Integer.class).toString());
			//outBox.setStatus(CommonUtils.NullChecker(rowResult[11], String.class).toString());
			outBox.setStatus(CommonUtils.getStatus(CommonUtils.NullChecker(rowResult[11], String.class).toString()));
			outBox.setOperator(CommonUtils.getTelcoName(Integer.valueOf(CommonUtils.NullChecker(rowResult[12], Integer.class).toString())));
			//outBox.setOperator(((Integer)rowResult[12]).toString());
			if(rowResult[13]!=null){
				outBox.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[13]));
			}else{
				outBox.setCreated(Constants.STR_EMPTY);
			}
			
			outBox.setScheduledStatus(CommonUtils.NullChecker(rowResult[14], String.class).toString());
			if(rowResult[15]!=null){
				outBox.setApprovalDatetime( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[15]));
			}else{
				outBox.setApprovalDatetime(Constants.STR_EMPTY);
			}
			outBox.setDeliveryReportDate("");
		    dataResult.add(outBox);
		}
		
		return dataResult;
	}
	
	public static String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
//	public static String normalizeMobile(String mobileNo) {
//		mobileNo = mobileNo.trim().replaceAll(" ", "").replaceAll("-", "").replaceAll("[+]", "").replaceAll("[(]", "").replaceAll("[)]", "");
//		if (NullChecker(mobileNo, String.class).toString().length()>0&&mobileNo.startsWith("0"))//number start with 0 will be padded a 6 to make it m'sia number
//		{
//			mobileNo="6"+mobileNo;
//		}
//		return mobileNo;
//	}

	public static String getUserList(Session hibernate_session, String u){
		StringBuffer  userId = new StringBuffer("");
		
		Integer group_id =(Integer) hibernate_session.createSQLQuery("select u.user_group from user u where u.user_id = ? ")
		.setString(0,u).list().get(0);
		
		
		List prList_result = hibernate_session.createSQLQuery("select u.user_id  from user u where  u.user_group = ? ")
		.setInteger(0,group_id).list();
		
		Iterator iter = prList_result.iterator();
		while (iter.hasNext()){
			String user =  (String) iter.next();
			userId.append("'");
			userId.append(user);
			userId.append("',");
		}
		
		return userId.substring(0, userId.length() -1);
		
	}
	
	public static List<SmsQueue> sortSmsQueue(List<SmsQueue> queueList, SmsCfg theCfg){
		List<SmsQueue> newQueueList=new ArrayList<SmsQueue>();
		List<SmsQueue> concatenateSmsList=new ArrayList<SmsQueue>();
		List<String> idList=new ArrayList<String>();
		
		for(int i=0;i<queueList.size();i++){// get single sms list and get the concatenate sms sequence list
			if(CommonUtils.NullChecker(queueList.get(i).getConcatenateSms(), String.class).toString().length()==0){
				newQueueList.add(queueList.get(i));
			}else{
				concatenateSmsList.add(queueList.get(i));
				if(CommonUtils.NullChecker(queueList.get(i).getConcatenateSms(), String.class).toString().contains(theCfg.getTableListDelimiter())){// get the concatenate sms id in sequence
					idList.addAll(CommonUtils.strToArray(queueList.get(i).getConcatenateSms(), theCfg.getTableListDelimiter()));
				}else{
				}
			}
		}

		for(int i=0;i<idList.size();i++){// get the concatenate sms list in sequence
			for(int j=0;j<concatenateSmsList.size();j++){
				if(concatenateSmsList.get(j).getId().toString().equals(idList.get(i))){
					newQueueList.add(concatenateSmsList.get(j));
					j=concatenateSmsList.size();// terminate the concatenate sms list loop once found the desired record
				}
			}
		}
		return newQueueList;
	}
	
	public static List<Sms> sortSms(List<Sms> smsList, SmsCfg theCfg){
		List<Sms> newSmsList=new ArrayList<Sms>();
		List<Sms> concatenateSmsList=new ArrayList<Sms>();
		List<String> idList=new ArrayList<String>();
		
		for(int i=0;i<smsList.size();i++){// get single sms list and get the concatenate sms sequence list
			if(CommonUtils.NullChecker(smsList.get(i).getConcatenateSms(), String.class).toString().length()==0){
				newSmsList.add(smsList.get(i));
			}else{
				concatenateSmsList.add(smsList.get(i));
				if(CommonUtils.NullChecker(smsList.get(i).getConcatenateSms(), String.class).toString().contains(theCfg.getTableListDelimiter())){// get the concatenate sms id in sequence
					idList.addAll(CommonUtils.strToArray(smsList.get(i).getConcatenateSms(), theCfg.getTableListDelimiter()));
				}else{
				}
			}
		}

		for(int i=0;i<idList.size();i++){// get the concatenate sms list in sequence
			for(int j=0;j<concatenateSmsList.size();j++){
				if(concatenateSmsList.get(j).getId().toString().equals(idList.get(i))){
					newSmsList.add(concatenateSmsList.get(j));
					j=concatenateSmsList.size();// terminate the concatenate sms list loop once found the desired record
				}
			}
		}
		return newSmsList;
	}
	
	public static boolean checkQuota(String refId, int smsNo, Session hibernate_session){
		boolean status=false;
		List quotaList=hibernate_session.createQuery("select id from SmsQuota where ref=? and quota>=?").setInteger(0, Integer.valueOf(refId)).setInteger(1, smsNo).list();
		if(quotaList.size()>0){
			status=true;
		}
		return status;
	}
	
	public static List<Option> getAllSmsRates(){
		List<Option> options = new ArrayList<Option>();
		List<SmsRate> smsRates = DaoBeanFactory.getSmsRateDao().getActiveSmsRate();
		if(smsRates == null || smsRates.isEmpty()){
			return options;
		}
		for(SmsRate smsRate : smsRates){
			options.add(new Option(String.valueOf(smsRate.getId()), smsRate.getName()));			
		}
		return options;
	}
	
	public static List<Option> getSvcProviderOptions(){
		List<Option> options = new ArrayList<Option>();
		List<ServiceProvider> svcPvds = DaoBeanFactory.getServiceProviderGroupDao().getActiveServiceProviders();
		if(svcPvds == null || svcPvds.isEmpty()){
			return options;
		}
		for(ServiceProvider svcPvd : svcPvds){
			options.add(new Option(String.valueOf(svcPvd.getId()), svcPvd.getName()));			
		}
		return options;
	}
	
	public static List<Option> getStatusOption(){
		List<Option> options = new ArrayList<Option>();
		options.add(new Option(Constants.FORM_FIELD_STATUS_ACTIVE, Constants.FORM_FIELD_STATUS_ACTIVE));
		options.add(new Option(Constants.FORM_FIELD_STATUS_DISABLED, Constants.FORM_FIELD_STATUS_DISABLED));
		return options;
	}
	
//	public static List<Option> getSvcProviderOption(){
//		List<Option> options = new ArrayList<Option>();
//		options.add(new Option(Constants.FORM_FIELD_STATUS_ACTIVE, Constants.FORM_FIELD_STATUS_ACTIVE));
//		options.add(new Option(Constants.FORM_FIELD_STATUS_DISABLED, Constants.FORM_FIELD_STATUS_DISABLED));
//		return options;
//	}
//	
	public static List<Option> getRoutingRulesOption(){
		List<Option> options = new ArrayList<Option>();
		options.add(new Option(Constants.DB_CONSTANTS_TBLREF_ROUTING_ACTION_PRIORITY, "Priority Routing"));
		return options;
	}
	
	public static LogonCredential getLogonCredential(HttpServletRequest request){
		return (LogonCredential) request.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);		
	}
	
	public static List<Option> getPriorityOption(){
		List<Option> options = new ArrayList<Option>();
		for(int i=9; i >= 1; i--){
			options.add(new Option(String.valueOf(i), String.valueOf(i)));
		}
		return options;
	}
	
	public static List<Option> getNonDefaultAutoActions(){
		List<AutoAction> autoActions = DaoBeanFactory.getAutoActionDao().findNonDefaultActions();
		List<Option> results = new ArrayList<Option>();
		results.add(new Option("0", "Not Required"));
		for(AutoAction a : autoActions){
			results.add(new Option(String.valueOf(a.getId()), a.getDescription()));
		}
		return results;
	}
}
