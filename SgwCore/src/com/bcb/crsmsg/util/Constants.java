package com.bcb.crsmsg.util;

public class Constants {
	/* Action Error Arguement */
	public static final String ERROR_LOGONACTION =  "logon";
	public static final String ERROR_NONLOGONACTION =  "notlogon";
	public static final String ERROR_USER_SEARCH =  "usersearch";
	public static final String ERROR_USER_SAVE_ADD =  "saveadduser";
	public static final String ERROR_USER_ADD =  "adduser";
	public static final String ERROR_USER_EDIT =  "edituser";
	public static final String ERROR_MSG_EDIT =  "editmessage";
	public static final String ERROR_USER_SAVE_EDIT =  "saveedituser";
	public static final String ERROR_USER_DELETE =  "deleteuser";
	public static final String ERROR_QUOTA_UPDATE =  "updatequota";
	public static final String ERROR_ADDRESSBOOK_SEARCH =  "addressbooksearch";
	public static final String ERROR_CONTACT_DELETE =  "deletecontact";
	public static final String ERROR_CONTACT_SAVE_ADD =  "saveaddcontact";
	public static final String ERROR_CONTACT_ADD =  "addcontact";
	public static final String ERROR_CONTACT_EDIT =  "editcontact";
	public static final String ERROR_CONTACT_SAVE_EDIT =  "saveeditcontact";
	public static final String ERROR_MSG_SAVE_EDIT =  "saveeditmsg";
	public static final String ERROR_NUMBER_LISTING =  "numberlisting";
	public static final String ERROR_TEMPLATEMESSAGE_SEARCH =  "templatemessagesearch";
	public static final String ERROR_TEMPLATEMESSAGE_DELETE =  "deletetemplatemessage";
	public static final String ERROR_TEMPLATEMESSAGE_SAVE_ADD =  "saveaddtemplatemessage";
	public static final String ERROR_TEMPLATEMESSAGE_ADD =  "addtemplatemessage";
	public static final String ERROR_TEMPLATEMESSAGE_EDIT =  "edittemplatemessage";
	public static final String ERROR_TEMPLATEMESSAGE_SAVE_EDIT =  "saveedittemplatemessage";
	public static final String ERROR_CANNEDMESSAGE_SEARCH =  "cannedmessagesearch";
	public static final String ERROR_KEYWORD_SEARCH =  "keywordsearch";
	public static final String ERROR_KEYWORD_DELETE =  "deletekeyword";
	public static final String ERROR_IPADDRESS_DELETE =  "deleteIPAddress";
	public static final String ERROR_KEYWORD_SAVE_ADD =  "saveaddkeyword";
	public static final String ERROR_IP_SAVE_ADD =  "saveaddip";
	public static final String ERROR_IP_SAVE_EDIT =  "saveeditip";
	public static final String ERROR_KEYWORD_ADD =  "addkeyword";
	public static final String ERROR_KEYWORD_EDIT =  "editkeyword";
	public static final String ERROR_KEYWORD_SAVE_EDIT =  "saveeditkeyword";
	public static final String ERROR_SYSTEMCFG_EDIT =  "editsystemcfg";
	public static final String ERROR_SYSTEMCFG_SAVE_EDIT =  "saveeditsystemcfg";
	public static final String ERROR_SMSCONTROL_EDIT =  "editsmscontrol";
	public static final String ERROR_SMSCONTROL_SAVE_EDIT =  "saveeditsmscontrol";
	public static final String ERROR_SMSTIMECONTROL_EDIT =  "editsmstimecontrol";
	public static final String ERROR_SMSTIMECONTROL_SAVE_EDIT =  "saveeditsmstimecontrol";
	public static final String ERROR_SMS_SEND =  "sendsms";
	public static final String ERROR_SMS_SEND_SAVE =  "savesendsms";
	public static final String ERROR_SMS_BULK =  "bulksms";
	public static final String ERROR_SMS_BULK_SAVE =  "savebulksms";
	public static final String ERROR_FTPSETUP_SEARCH =  "ftpsetupsearch";
	public static final String ERROR_FTPSETUP_DELETE =  "deleteftpsetup";
	public static final String ERROR_FTPSETUP_SAVE_ADD =  "saveaddftpsetup";
	public static final String ERROR_FTPSETUP_ADD =  "addftpsetup";
	public static final String ERROR_FTPSETUP_EDIT =  "editftpsetup";
	public static final String ERROR_FTPSETUP_SAVE_EDIT =  "saveeditftpsetup";
	public static final String ERROR_SECURITY_CONTROL_EDIT =  "editsecuritycontrol";
	public static final String ERROR_SECURITY_CONTROL_SAVE_EDIT =  "saveeditsecuritycontrol";
	public static final String ERROR_OUTBOX =  "outbox";
	public static final String ERROR_INBOX =  "inbox";
	public static final String ERROR_COMMON_INBOX =  "commoninbox";
	public static final String ERROR_RECEIVEDLOG_SEARCH =  "receivedlogsearch";
	public static final String ERROR_QLOG =  "qlog";
	public static final String ERROR_SENTLOG =  "sentlog";
	public static final String ERROR_UNSENTLOG =  "unsentlog";
	public static final String ERROR_PENDINGAPPROVAL =  "pendingapproval";
	public static final String ERROR_TELCO_SAVE_EDIT =  "saveedittelco";
	public static final String ERROR_CUSTOMER_SEARCH =  "customersearch";
	public static final String ERROR_CUSTOMER_DELETE =  "deletecustomer";
	public static final String ERROR_CUSTOMER_SAVE_ADD =  "saveaddcustomer";
	public static final String ERROR_CUSTOMER_ADD =  "addcustomer";
	public static final String ERROR_CUSTOMER_EDIT =  "editcustomer";
	public static final String ERROR_CUSTOMER_SAVE_EDIT =  "saveeditcustomer";
	public static final String ERROR_CCCFG_SAVE_EDIT = "saveeditcccfg";
	public static final String ERROR_CC_CONFIG = "ccconfig";
	public static final String ERROR_CARD_LOG = "cardactivationlog";
	public static final String ERROR_AUTOACTION_SAVE_EDIT =  "saveeditautoaction";
	/* Action Error Arguement*/
		
	/* Formattting */
	// Date Formatting
	public static final String DATE_YYYY_MM_DD = "YYYY-MM-DD HH:NN:SS.ZZZ";	
	public static final String DATE_YYYY_MM_DD_HHmmss = "yyyy-MM-dd HH:mm:ss";	
	public static final String DATE_YYYYMMDDHHSSZZZ = "yyyyMMddhhmmss";	
	public static final String DATE_YYMMDDHHSS = "yyMMddhhmmss";	
	public static final String DATE_MMddyyyy = "MM/dd/yyyy";
	public static final String TIME_hhmmss = "hh:mm:ss";
	public static final String DATE_DDMMYYYY = "DD-MM-YYYY";
	public static final String DATEFORMAT_HOURBYMINUTESECOND = "hh:mm:ss";
	public static final String DATEFORMAT_HOURBYMINUTE = "HHmm";
	public static final String DECIMALFORMAT = "0.00";
	public static final String DATE_yyyy_MM_dd_hh_mm_ss = "yyyy-MM-dd HH:mm:ss";	
	public static final String STR_DATEFROM ="Date From";
	public static final String STR_DATETO ="Date To";
	public static final String DATEFORMAT_DAYBYMONTHYEAR = "dd/MM/yyyy";
	public static final String DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE = "dd/MM/yyyy HH:mm";
	public static final String DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE_SECOND = "dd/MM/yyyy HH:mm:ss";
	public static final String DATEFORMAT_JAVACSRIPT = "MMM dd, yyyy HH:mm:ss";
	public static final String DATEFORMAT_DDMMYY="ddMMyy";
	public static final String DATEFORMAT_DDMMYYYY="ddMMyyyy";
	public static final String DATEFORMAT_YYYY_MM_DD="yyyy-MM-dd";
	/* Formattting */
	
	/* Forms */
	// Form Name
	public static final String FORM_USER_SEARCH =  "UserSearchForm";
	public static final String FORM_USER =  "UserForm";
	public static final String FORM_QUOTA =  "QuotaForm";
	public static final String FORM_CONTACT =  "ContactForm";
	public static final String FORM_TEMPLATEMESSAGE =  "TemplateMessageForm";
	public static final String FORM_TEMPLATEMESSAGE_SEARCH =  "TemplateMessageSearchForm";
	public static final String FORM_KEYWORD =  "KeywordForm";
	public static final String FORM_IP = "IPForm";
	public static final String FORM_KEYWORD_SEARCH =  "KeywordSearchForm";
	public static final String FORM_SYSTEM_CFG =  "SystemCfgForm";
	public static final String FORM_FTP_SETUP_SEARCH =  "FTPSetupSearchForm";
	public static final String FORM_FTP_SETUP =  "FTPSetupForm";
	public static final String FORM_SEND_SMS =  "SendSMSForm";
	public static final String FORM_BULK_SMS =  "BulkSMSForm";
	public static final String FORM_SECURITY_CONTROL =  "SecurityControlForm";
	public static final String FORM_EDIT_SMS =  "EditSMSForm";
	public static final String FORM_TELCO =  "TelcoForm";
	public static final String FORM_SENT_LOG =  "SentLogForm";
	public static final String FORM_UNSENT_LOG =  "UnSentLogForm";
	public static final String FORM_FTP_BLUK_LOG =  "FtpBulkLogForm";
	public static final String FORM_MANUAL_BLUK_LOG =  "ManualBulkLogForm";
	public static final String FORM_OUTBOX =  "OutboxForm";
	public static final String FORM_FTP_REPORT=  "FtpReportForm";
	public static final String FORM_CHANNEL_FORM=  "ChannelForm";
	public static final String FORM_SMS_TYPE=  "SMSTypeForm";
	public static final String FORM_QUEUE_LISTENER_CONFIG = "QueueListenerCfgForm";
	public static final String FORM_CARD_CFG =  "CardActivationCfgForm";
	public static final String FORM_CARD_LOG = "CardActivationLogForm";
	public static final String FORM_AUTOACTION = "AutoActionForm";
	// Form Field
	public static final String FORM_FIELD_USER_ID ="User";
	public static final String FORM_FIELD_USER_NAME ="Name";
	public static final String FORM_FIELD_USER_ROLE ="Role";
	public static final String FORM_FIELD_USER_DEPARTMENT ="Department";
	public static final String FORM_FIELD_USER_DEPARTMENT_NAME ="Department Name";
	public static final String FORM_FIELD_PASSWORD ="Password";
	public static final String FORM_FIELD_RIGHTS ="Additional Rights";
	public static final String FORM_FIELD_QUOTA ="Quota";
	public static final String FORM_FIELD_QUOTA_TOPUP_MODE ="Top-Up Mode";
	public static final String FORM_FIELD_QUOTA_TOPUP_MODE_TOPUP ="topup";
	public static final String FORM_FIELD_QUOTA_TOPUP_MODE_RESET ="reset";
	public static final String FORM_FIELD_MOBILE_NO ="Mobile No.";
	public static final String FORM_FIELD_DESIGNATION ="Designation";
	public static final String FORM_FIELD_CUST_REF_NO ="Customer Ref No.";
	public static final String FORM_FIELD_CONTACT_TYPE ="Contact Type";
	public static final String FORM_FIELD_CONTACT_TYPE_PERSONAL ="Personal";
	public static final String FORM_FIELD_CONTACT_TYPE_GROUP ="Group";
	public static final String FORM_FIELD_MESSAGE_CODE="Message Code";
	public static final String FORM_FIELD_MESSAGE_TEXT="Message Text";
	public static final String FORM_FIELD_DYNAMIC_FIELD_NO="Dynamic Field No.";
	public static final String FORM_FIELD_DYNAMIC_FIELD_NAME="Dynamic Field Name";
	public static final String FORM_FIELD_STATUS="Status";
	public static final String FORM_FIELD_STATUS_ACTIVE="Active";
	public static final String FORM_FIELD_STATUS_DISABLED="Disabled";
	public static final String FORM_FIELD_STATUS_CLOSED="Closed";
	public static final String FORM_FIELD_STATUS_YES="Yes";
	public static final String FORM_FIELD_STATUS_NO="No";
	public static final String FORM_FIELD_TYPE ="Type";
	//security control
	public static final String FORM_FIELD_IPADDRESS ="IP Address";
	public static final String FORM_FIELD_SYSTEM ="System";
	
	public static final String FORM_FIELD_KEYWORD ="Keyword";
	public static final String FORM_FIELD_OWNERSHIP ="Ownership";
	public static final String FORM_FIELD_CHARSET="Character Set";
	public static final String FORM_FIELD_CHARSET_ASCIITEXT="ASCII/Text";
	public static final String FORM_FIELD_CHARSET_UTF8="UTF-8";
	public static final String FORM_FIELD_REDIRECT_URL ="Redirect URL";
	public static final String FORM_FIELD_REDIRECT_URL_PARAM ="Redirect URL Parameters";
	public static final String FORM_FIELD_REDIRECT_EMAIL ="Redirect Email";
	public static final String FORM_FIELD_REDIRECT_MOBILENO ="Redirect Mobile No.";
	
	public static final String FORM_FIELD_AUTO_REPLY_MSG="Auto Reply Message";
	public static final String FORM_FIELD_FAILURE_FTP_EMAIL="Failure FTP Notice Email Account";
	public static final String FORM_FIELD_FAILURE_FTP_EMAIL_SENDER="Failure FTP Notice Email Sender";
	public static final String FORM_FIELD_FAILURE_FTP_SMS="Failure FTP Notice SMS No.";
	public static final String FORM_FIELD_FAILURE_FTP_SMS_SENDER="Failure FTP Notice SMS Sender";
	public static final String FORM_FIELD_FAILURE_SMS_EMAIL="SMS Failure Notice Email Account";
	public static final String FORM_FIELD_FAILURE_SMS_EMAIL_SENDER="SMS Failure Notice Email Sender";
	public static final String FORM_FIELD_FAILURE_SMS_SMS="SMS Failure Notice SMS No.";
	public static final String FORM_FIELD_FAILURE_SMS_SMS_SENDER="SMS Failure Notice SMS Sender";
	public static final String FORM_FIELD_FAILURE_SMS_THRESHOLD="SMS Failure Threshold";
	public static final String FORM_FIELD_FAILURE_SMS_THRESHOLD_PERIOD="SMS Failure Threshold Recheck Period";
	public static final String FORM_FIELD_FAILURE_SMS_THRESHOLD_TIME="SMS Failure Threshold Time";
	public static final String FORM_FIELD_MOBILE_PREFIX="Mobile Prefix Number";
	public static final String FORM_FIELD_BULK_SMS_THRESHOLD="Bulk SMS Threshold";
	public static final String FORM_FIELD_BULK_SMS_THRESHOLD_EMAIL="Bulk SMS Threshold Notice Email Account";
	public static final String FORM_FIELD_BULK_SMS_THRESHOLD_EMAIL_SENDER="Bulk SMS Threshold Notice Email Sender";
	public static final String FORM_FIELD_BULK_SMS_THRESHOLD_SMS="Bulk SMS Threshold Notice SMS No.";
	public static final String FORM_FIELD_BULK_SMS_THRESHOLD_SMS_SENDER="Bulk SMS Threshold Notice SMS Sender";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD="Queue SMS Threshold";
	public static final String FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD="Queue Total SMS Threshold";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_EMAIL="Queue SMS Threshold Notice Email Account";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_EMAIL_SENDER="Queue SMS Threshold Notice Email Sender";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_SMS="Queue SMS Threshold Notice SMS No.";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_SMS_SENDER="Queue SMS Threshold Notice SMS Sender";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_PERIOD="Queue SMS Threshold Recheck Period";
	public static final String FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD_PERIOD="Queue Total SMS Threshold Recheck Period";
	public static final String FORM_FIELD_QUEUE_SMS_THRESHOLD_TIME="Queue SMS Threshold Time";
	public static final String FORM_FIELD_PENDING_SMS_AUTO_FAILURE="Pending SMS Auto Failure";
	public static final String FORM_FIELD_AUTO_RESEND_SMS_LIMIT="SMS Resend Limit";
	public static final String FORM_FIELD_AUTO_RESEND_SMS_INTERVAL="SMS Resend Interval";
	
	public static final String FORM_FIELD_AUTO_REDIRECT_SMS_LIMIT="Maximum Redirect Attempts";
	public static final String FORM_FIELD_AUTO_REDIRECT_SMS_INTERVAL="Redirect Interval";	
	public static final String KEYWORD_REDIRECT="RedirectTest";
	public static final String REDIRECT_SUCCESS_CODE= "-1";
	public static final String REDIRECT_SUCCESS_REMARK= "SMS: Redirect SUCCESS";
	public static final String REDIRECT_FAILURE_REMARK= "SMS: Redirect FAILURE";
	
	public static final String FORM_FIELD_SMS_QUOTA_DEFAULT="SMS Default Quota";
	public static final String FORM_FIELD_PASSWORD_RESET_POLICY="Password Reset Policy";
	public static final String FORM_FIELD_SCHEDULED_SMS_CONTROL="Scheduled SMS Control";
	public static final String FORM_FIELD_IP_SKIP_CONTROL="IP to skip control";
	public static final String FORM_FIELD_SMS_PRIORITY="SMS Priority";
	public static final String FORM_FIELD_SMS_CONTENT="SMS Content";
	public static final String FORM_FIELD_SMS_BULKFILE="Bulk SMS File";
	public static final String FORM_FIELD_TEMPLATE_MESSAGE="Template Message";
	public static final String FORM_FIELD_SFTP_ENTRY_NAME="SFTP Entry Name";
	public static final String FORM_FIELD_SFTP_SERVER_HOST="SFTP Server/Host";
	public static final String FORM_FIELD_SFTP_PORT="SFTP Port";
	public static final String FORM_FIELD_SFTP_USER_NAME="SFTP User Name";
	public static final String FORM_FIELD_SFTP_PASSWORD="SFTP Password";
	public static final String FORM_FIELD_SFTP_PASSWORD_CONFIRMATION="SFTP Confirmation Password";
	public static final String FORM_FIELD_SFTP_TARGET_FILE="Target File";
	public static final String FORM_FIELD_SFTP_NOTIFICATION_SETUP_SENDER="Notification Setup Sender";
	public static final String FORM_FIELD_SFTP_NOTIFICATION_SETUP_RECEIVER="Notification Setup Receiver";
	public static final String FORM_FIELD_SFTP_MODE="FTP Mode";
	public static final String FORM_FIELD_SFTP_FILE_CONVERSION="Action Method";
	public static final String FORM_FIELD_SFTP_RETRY_NO="SFTP Auto Retry No.";
	public static final String FORM_FIELD_SFTP_RETRY_PERIOD="SFTP Auto Retry Period";
	public static final String FORM_FIELD_TIME_MODE="SFTP Time";
	public static final String FORM_FIELD_TIME_MODE_MONTHLY="1";
	public static final String FORM_FIELD_TIME_MODE_WEEKLY="2";
	public static final String FORM_FIELD_TIME_MODE_HOURLY="3";
	public static final String FORM_FIELD_TELCO_NAME="Telco Name";
	public static final String FORM_FIELD_TELCO_URL="Telco URL";
	public static final String FORM_FIELD_TELCO_USERNAME="Telco User Name";
	public static final String FORM_FIELD_TELCO_PASSWORD="Telco Password";
	public static final String FORM_FIELD_TELCO_PREFIX_ROUTING="Mobile Prefix Routing";
	public static final String FORM_FIELD_TELCO_PREFIX_HOME="Home Prefix";
	public static final String FORM_FIELD_TELCO_PREFIX_INCOMING="Incoming Prefix";
	public static final String FORM_FIELD_TELCO_WITHIN_CHARGE="Within Operator Charge";
	public static final String FORM_FIELD_TELCO_INTER_CHARGE="Inter Operator Charge";
	public static final String FORM_FIELD_TELCO_INCOMING_CHARGE="Incoming Charge";
	public static final String FORM_FIELD_TELCO_NOTIFICATION_EMAILFROM="Email From";
	public static final String FORM_FIELD_TELCO_NOTIFICATION_EMAILTO="Email To";
	public static final String FORM_FIELD_TELCO_ROUTING_PRIORITY="By Telco Priority";
	public static final String FORM_FIELD_TELCO_ROUTING_PRIORITY_SEQUENCE="Telco Priority";
	public static final String FORM_FIELD_TELCO_ROUTING_COST="By Cost Effective";
	public static final String FORM_FIELD_TELCO_ROUTING_CONGESTION="By Telco Congestion Status";
	public static final String FORM_FIELD_TELCO_ROUTING_PREFIX="By Telco Prefix";
	public static final String FORM_FIELD_QUEUE_THRESHOLD_NO="Queue Threshold No.";
	public static final String FORM_FIELD_BULK_FILE_MODE_PHONE="Phone";
	public static final String FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE="Phone & Template";
	public static final String FORM_FIELD_FTP_MODE_INBOUND="I";
	public static final String FORM_FIELD_FTP_MODE_OUTBOUND="O";
	public static final String FORM_FIELD_SUBSCRIBE="Subscribe";
	public static final String FORM_FIELD_UNSUBSCRIBE="Unsubcribe";
	public static final String FORM_FIELD_USAGE_LOG_MAINTENANCE="SMS Usage Log Maintenance";
	public static final String FORM_FIELD_CREDIT_CARD_ACTIVATION_LOG_MAINTENANCE="Credit Card Activation Log Maintenance";
	public static final String FORM_FIELD_SYSTEM_LOG_MAINTENANCE="SMS System Log Maintenance";
	public static final String FORM_FIELD_ACCOUNT_DORMANCY_CHECK="Account Dormancy Check";
	public static final String FORM_LABEL_SUBSCRIBE="S";
	public static final String FORM_LABEL_UNSUBSCRIBE="U";
	public static final String FORM_LABEL_SMS_STATUS="SMS Status";
	public static final String FORM_LABEL_DATE_RANGE="Date Range";
	public static final String FORM_FIELD_CHANNEL_ID="Channel ID";
	public static final String FORM_FIELD_SMS_TYPE="SMS Type";
	public static final String FORM_FIELD_SMS_TYPE_PRIORITY="Priority";
	public static final String FORM_FIELD_SMS_TYPE_ROUTING_RULES="Routing Rules";
	public static final String FORM_FIELD_SMS_TYPE_SERVICE_PROVIDER="Service Providers";
	public static final String REQUEST_PARAMETER_LOAD="load";
	public static final String REQUEST_PARAMETER_SAVE="save";
	public static final String REQUEST_PARAMETER_DELETE="delete";
	public static final String FORM_FIELD_SYS_SMS_DEFAULT_SENDER="System SMS Default User";
	public static final String FORM_FIELD_DEFAULT_SMS_TYPE="Default SMS Type";
	public static final String DB_CONSTANTS_TBLREF_TBLNAME_SMSMQ="sms_mq";
	public static final String DB_CONSTANTS_TBLREF_COLNAME="sms_type";
	public static final String DB_CONSTANTS_TBLREF_ROUTING_ACTION_PRIORITY="routeByPriority";
	public static final String DB_CONSTANTS_CREATED_BY_SYSTEM="SYSTEM";
	public static final String FORM_FIELD_CCA_SERVER_PORT="Card Link Server TCP Port No";
	public static final String FORM_FIELD_CCA_SERVER_TIMEOUT="Card Link Server Connection Timeout";
	public static final String FORM_FIELD_FAILURE_ATTEMPT="Maximum Number of Activation Attempt";
	public static final String FORM_FIELD_CCA_SVC_WINDOWS="Time";
		
	// Field Length
	public static final String LENGTH_15 ="15";
	public static final String LENGTH_16 ="16";
	public static final String LENGTH_50 ="50";
	public static final String LENGTH_2 ="2";
	public static final String LENGTH_0 ="0";
	public static final String LENGTH_100 ="100";
	public static final String LENGTH_20 ="20";
	public static final String LENGTH_25 ="25";
	public static final String LENGTH_500 ="500";
	public static final String LENGTH_6 ="6";
	public static final String LENGTH_200 ="200";
	public static final String LENGTH_5 ="5";
	public static final String LENGTH_10 ="10";
	public static final String LENGTH_9="9";
	public static final String LENGTH_1 ="1";
	public static final String LENGTH_255 ="255";
	public static final String LENGTH_1000 ="1000";
	public static final String USER_HEADER_LENTH="7";
	
	// Field Value
	public static final String VALUE_0 ="0";
	public static final String VALUE_1 ="1";
	
	// Field label
	public static final String FORM_FIELD_LABEL_SUNDAY="Sunday";
	public static final String FORM_FIELD_LABEL_MONDAY="Monday";
	public static final String FORM_FIELD_LABEL_TUESDAY="Tuesday";
	public static final String FORM_FIELD_LABEL_WEDNESDAY="Wednesday";
	public static final String FORM_FIELD_LABEL_THURSDAY="Thursday";
	public static final String FORM_FIELD_LABEL_FRIDAY="Friday";
	public static final String FORM_FIELD_LABEL_SATURDAY="Saturday";
	
	public static final String FORM_FIELD_LABEL_JANUARY="January";
	public static final String FORM_FIELD_LABEL_FEBRUARY="February";
	public static final String FORM_FIELD_LABEL_MARCH="March";
	public static final String FORM_FIELD_LABEL_APRIL="April";
	public static final String FORM_FIELD_LABEL_MAY="May";
	public static final String FORM_FIELD_LABEL_JUNE="June";
	public static final String FORM_FIELD_LABEL_JULY="July";
	public static final String FORM_FIELD_LABEL_AUGUST="August";
	public static final String FORM_FIELD_LABEL_SEPTEMBER="September";
	public static final String FORM_FIELD_LABEL_OCTOBER="October";
	public static final String FORM_FIELD_LABEL_NOVEMBER="November";
	public static final String FORM_FIELD_LABEL_DECEMBER="December";
	public static final String FORM_FIELD_LABEL_FTP_MODE_INBOUND="Inbound FTP";
	public static final String FORM_FIELD_LABEL_FTP_MODE_OUTBOUND="Outbound FTP";
	/* Forms */
	
	/* Actions */
	public static final String ACTION_USER_SEARCH ="UserSearch";
	public static final String ACTION_USER_SAVE_ADD ="SaveAddUser";
	public static final String ACTION_USER_SAVE_EDIT ="SaveEditUser";
	public static final String ACTION_USER_EDIT ="EditUser";
	public static final String ACTION_USER_ADD ="AddUser";
	public static final String ACTION_USER_DELETE ="SaveDeleteUser";
	public static final String ACTION_QUOTA_UPDATE ="UpdateQuota";
	public static final String ACTION_ADDRESSBOOK_SEARCH ="AddressBookSearch";
	public static final String ACTION_CONTACT_DELETE ="SaveDeleteContact";
	public static final String ACTION_CONTACT_EDIT ="EditContact";
	public static final String ACTION_CONTACT_ADD ="AddContact";
	public static final String ACTION_CONTACT_SAVE_ADD ="SaveAddContact";
	public static final String ACTION_CONTACT_SAVE_EDIT ="SaveEditContact";
	public static final String ACTION_MSG_SAVE_EDIT ="SaveEditMsg";
	public static final String ACTION_NUMBER_LISTING ="NumberListing";
	public static final String ACTION_TEMPLATEMESSAGE_SEARCH ="TemplateMessageSearch";
	public static final String ACTION_TEMPLATEMESSAGE_DELETE ="SaveDeleteTemplateMessage";
	public static final String ACTION_TEMPLATEMESSAGE_EDIT ="EditTemplateMessage";
	public static final String ACTION_TEMPLATEMESSAGE_ADD ="AddTemplateMessage";
	public static final String ACTION_TEMPLATEMESSAGE_SAVE_ADD ="SaveAddTemplateMessage";
	public static final String ACTION_TEMPLATEMESSAGE_SAVE_EDIT ="SaveEditTemplateMessage";
	public static final String ACTION_CANNEDMESSAGE_SEARCH ="CannedMessageSearch";
	public static final String ACTION_KEYWORD_SEARCH ="KeywordSearch";
	public static final String ACTION_IPADDRESS_SEARCH ="IPAddressSearch";
	public static final String ACTION_KEYWORD_DELETE ="SaveDeleteKeyword";
	public static final String ACTION_KEYWORD_EDIT ="EditKeyword";
	public static final String ACTION_IPADDRESS_EDIT ="EditIPAddress";
	public static final String ACTION_KEYWORD_ADD ="AddKeyword";
	public static final String ACTION_KEYWORD_SAVE_ADD ="SaveAddKeyword";
	public static final String ACTION_KEYWORD_SAVE_EDIT ="SaveEditKeyword";
	public static final String ACTION_SYSTEMCFG_EDIT ="EditSystemCfg";
	public static final String ACTION_SYSTEMCFG_SAVE_EDIT ="SaveEditSystemCfg";
	public static final String ACTION_SMSCONTROL_EDIT ="EditSMSControl";
	public static final String ACTION_SMSCONTROL_SAVE_EDIT ="SaveEditSMSControl";
	public static final String ACTION_SMSTIMECONTROL_EDIT ="EditSMSTimeControl";
	public static final String ACTION_SMSTIMECONTROL_SAVE_EDIT ="SaveEditSMSTimeControl";
	public static final String ACTION_SMS_SEND ="SendSMS";
	public static final String ACTION_SMS_SEND_SAVE ="SaveSendSMS";
	public static final String ACTION_SMS_BULK ="BulkSMS";
	public static final String ACTION_SMS_BULK_SAVE ="SaveBulkSMS";
	public static final String ACTION_FTPSETUP_SEARCH ="FTPSetupSearch";
	public static final String ACTION_FTPSETUP_DELETE ="SaveDeleteFTPSetup";
	public static final String ACTION_FTPSETUP_EDIT ="EditFTPSetup";
	public static final String ACTION_FTPSETUP_ADD ="AddFTPSetup";
	public static final String ACTION_FTPSETUP_SAVE_ADD ="SaveAddFTPSetup";
	public static final String ACTION_FTPSETUP_SAVE_EDIT ="SaveEditFTPSetup";
	public static final String ACTION_SECURITY_CONTROL_SAVE_EDIT ="SaveEditSecurityControl";
	public static final String ACTION_SECURITY_CONTROL_SAVE_ADD ="SaveAddSecurityControl";
	public static final String ACTION_SECURITY_CONTROL_EDIT ="EditSecurityControl";
	public static final String ACTION_MSG_EDIT ="EditMSG";
	public static final String ACTION_SMS_ACTION_SEND ="SendSMSAction";
	public static final String ACTION_SMS_ACTION_RECEIVE ="ReceiveSMSAction";
	public static final String ACTION_OUTBOX ="Outbox";
	public static final String ACTION_INBOX ="Inbox";
	public static final String ACTION_COMMON_INBOX ="CommonInbox";
	public static final String ACTION_RECEIVEDLOG_SEARCH ="ReceivedLogSearch";
	public static final String ACTION_QLOG ="QLog";
	public static final String ACTION_SENTLOG ="SentLog";
	public static final String ACTION_UNSENTLOG ="UnSentLog";
	public static final String ACTION_PENDINGAPPROVAL ="PendingApproval";
	public static final String ACTION_TELCO_SAVE_EDIT ="SaveEditTelco";
	public static final String ACTION_CUSTOMER_SEARCH ="CustomerSearch";
	public static final String ACTION_CUSTOMER_DELETE ="SaveDeleteCustomer";
	public static final String ACTION_CUSTOMER_EDIT ="EditCustomer";
	public static final String ACTION_CUSTOMER_ADD ="AddCustomer";
	public static final String ACTION_CUSTOMER_SAVE_ADD ="SaveAddCustomer";
	public static final String ACTION_CUSTOMER_SAVE_EDIT ="SaveEditCustomer";
	public static final String ACTION_CCCFG_SAVE_EDIT="SaveEditCardCfg";
	public static final String ACTION_CC_CONFIG = "CCConfig";
	public static final String ACTION_AUTOACTION_SEARCH ="AutoActionSearch";
	public static final String ACTION_AUTOACTION_ADD ="AutoActionAdd";
	public static final String ACTION_AUTOACTION_EDIT ="AutoActionEdit";
	public static final String ACTION_AUTOACTION_SAVE_EDIT ="AutoActionSaveEdit";
	/* Actions */
	
	/* Mapping Arguement */
	public static final String MAPPINGVALUE_SUCCESS = "success";
	public static final String MAPPINGVALUE_FAILED = "failed";
	public static final String MAPPINGVALUE_NOTLOGON = "notlogon";
	public static final String MAPPINGVALUE_ACCDENY =  "accdeny";
	/* Mapping Arguement */
	
	/* Request/Session Key and Parameters */
	public static final String LOGONCREDENTIAL_KEY = "logoncredential";
	public static final String USER_KEY = "user";
	public static final String PENDING_COUNT_KEY = "pendingCount";
	public static final String APPROVAL_COUNT_KEY = "approvalCount";
	public static final String FAILED_COUNT_KEY = "failedCount";
	public static final String SENT_COUNT_KEY = "sentCount";
	public static final String INBOX_RESULT_KEY="inBox";
	public static final String OUTBOX_RESULT_KEY="outBox";
	public static final String QLOG_RESULT_KEY="qLog";
	public static final String SENTLOG_RESULT_KEY="sentLog";
	public static final String UNSENTLOG_RESULT_KEY="unsentLog";
	public static final String PENDINGLOG_RESULT_KEY="pendingLog";
	public static final String USER_QUOTA_KEY="userquota";
	public static final String SESSION_KEY_MULTIPLE_SMS_MSG="multiSmsAlertMsg";
	/* Request/Session Key */
	
	/* Application Defined Value */
	// Apps Info
	public static final String APPSID = CommonUtils.getPropValue("crsmsg" , "appsid");
	public static final String APPSNAME= CommonUtils.getPropValue("crsmsg" , "appsname");
	public static final String APPSVERSION= CommonUtils.getPropValue("crsmsg" , "appsversion");
	
	// Perm Value
	public static final String PERM_ADMIN_ID = CommonUtils.getPropValue("crsmsg" , "permission.adminuser");
	public static final String USER_SEARCH = CommonUtils.getPropValue("crsmsg" , "permission.usersearch");
	public static final String FTP_SETUP = CommonUtils.getPropValue("crsmsg" , "permission.ftpsetup");
	public static final String FTP_LOG = CommonUtils.getPropValue("crsmsg" , "permission.ftplog");
	public static final String ADDRESS_BOOK = CommonUtils.getPropValue("crsmsg" , "permission.addressbook");
	public static final String TEMPLATE_MSG = CommonUtils.getPropValue("crsmsg" , "permission.templatemessage");
	public static final String KEYWORD_MGMT = CommonUtils.getPropValue("crsmsg" , "permission.keywordmgmt");
	public static final String SYSTEM_SETTING = CommonUtils.getPropValue("crsmsg" , "permission.systemsetting");
	public static final String SECURITY_CONTROL = CommonUtils.getPropValue("crsmsg" , "permission.securitycontrol");
	public static final String CUSTOMER_MAINTENANCE = CommonUtils.getPropValue("crsmsg" , "permission.customermaintenance");
	public static final String INBOX = CommonUtils.getPropValue("crsmsg" , "permission.inbox");
	public static final String COMMON_INBOX = CommonUtils.getPropValue("crsmsg" , "permission.commoninbox");
	public static final String OUTBOX = CommonUtils.getPropValue("crsmsg" , "permission.outbox");
	public static final String SEND_SMS = CommonUtils.getPropValue("crsmsg" , "permission.sendsms");
	public static final String BULK_SMS = CommonUtils.getPropValue("crsmsg" , "permission.bulksms");
	public static final String RECEIVED_LOG = CommonUtils.getPropValue("crsmsg" , "permission.receivedlog");
	public static final String QUEUE_LOG = CommonUtils.getPropValue("crsmsg" , "permission.queuelog");
	public static final String SENT_LOG = CommonUtils.getPropValue("crsmsg" , "permission.sentlog");
	public static final String UNSENT_LOG = CommonUtils.getPropValue("crsmsg" , "permission.unsentlog");
	public static final String BULK_SEND_LOG = CommonUtils.getPropValue("crsmsg" , "permission.bulksendlog");
	public static final String PENDING_APPROVAL_LOG = CommonUtils.getPropValue("crsmsg" , "permission.pendingapprovallog");
	public static final String FTP_REPORT = CommonUtils.getPropValue("crsmsg" , "permission.ftpreport");
	public static final String BILLING_REPORT = CommonUtils.getPropValue("crsmsg" , "permission.billingreport");
	public static final String BILLING_REPORT_INCOMING = CommonUtils.getPropValue("crsmsg" , "permission.billingreportincoming");
	public static final String DELIVERY_REPORT = CommonUtils.getPropValue("crsmsg" , "permission.deliveryreport");
	public static final String CUSTOMER_RESPONSE_REPORT = CommonUtils.getPropValue("crsmsg" , "permission.customeresponsereport");
	public static final String PERM_ACTIVEMQ_CONSOLE = CommonUtils.getPropValue("crsmsg" , "permission.activemqconsole");
	public static final String PERM_JMX = CommonUtils.getPropValue("crsmsg" , "permission.jmx");
	public static final String CARD_ACTIVATION_CONF = CommonUtils.getPropValue("crsmsg" , "permission.cardactivationconf");
	public static final String CARD_ACTIVATION_LOG = CommonUtils.getPropValue("crsmsg" , "permission.cardactivationlog");
	public static final String CARD_ACTIVATION_LOG_DELETE = CommonUtils.getPropValue("crsmsg" , "permission.cardactivationlogdelete");
	public static final String CARD_ACTIVATION_RECEIVED_SMS = CommonUtils.getPropValue("crsmsg","permission.cardactivationreceivedsms");
	public static final String BILLING_REPORT_DEPT = CommonUtils.getPropValue("crsmsg" , "permission.billingreportdept");
	public static final String SOCKET_ENC_KEY = CommonUtils.getPropValue("crsmsg" , "permission.socketenckey");
	
	// User Rights
	public static final String ACTION_ALL = CommonUtils.getPropValue("crsmsg" , "rights.action.all");
	public static final String ACTION_DEPARTMENT = CommonUtils.getPropValue("crsmsg" , "rights.action.department");
	public static final String ACTION_PERSONAL = CommonUtils.getPropValue("crsmsg" , "rights.action.personal");
	public static final String SMS_APPROVAL = CommonUtils.getPropValue("crsmsg" , "rights.sms.approval");
	public static final String SMS_PRIORITY_CHANGE = CommonUtils.getPropValue("crsmsg" , "rights.sms.prioritychange");
//	public static final String SMS_TIMING_CONTROL = CommonUtils.getPropValue("crsmsg" , "rights.sms.timingcontrol");
	public static final String ADDRESSBOOK_EXPORT = CommonUtils.getPropValue("crsmsg" , "rights.addressbook.export");
	
	// Aditional Cfg
	public static final String DEFAULT_FTP_PORT = CommonUtils.getPropValue("crsmsg" , "ftp.default.port");
	public static final String FTP_FILE_HEADER = CommonUtils.getPropValue("crsmsg" , "ftp.file.header");
	public static final String FTP_FILE_DATA = CommonUtils.getPropValue("crsmsg" , "ftp.file.data");
	public static final String FTP_FILE_EOF = CommonUtils.getPropValue("crsmsg" , "ftp.file.eof");
	public static final String FEATURE_DISABLE_SIGNAL = CommonUtils.getPropValue("crsmsg" , "feature.disable.signal");// FTP send SMS
	public static final String DEFAULT_SFTP_KEY_FOLDER=CommonUtils.getPropValue("crsmsg" , "sftp.default.key.folder");
	public static final String TEMPLATE_DYNAMIC_FIELD_LEFT_INDICATOR = CommonUtils.getPropValue("crsmsg" , "template.dynamicfield.left.indicator");
	public static final String TEMPLATE_DYNAMIC_FIELD_RIGHT_INDICATOR = CommonUtils.getPropValue("crsmsg" , "template.dynamicfield.right.indicator");
	public static final String TEMPLATE_FIELD_MOBILE_NO = CommonUtils.getPropValue("crsmsg" , "template.field.mobileno.index");
	public static final String TEMPLATE_FIELD_TEMPLATE_CODE = CommonUtils.getPropValue("crsmsg" , "template.field.templatecode.index");
	public static final String TEMPLATE_FIELD_CUST_REF_NO = CommonUtils.getPropValue("crsmsg" , "template.field.custrefno.index");
	public static final String TEMPLATE_FIELD_ACC_NO = CommonUtils.getPropValue("crsmsg" , "template.field.accountno.index");
	public static final String SYNC_FIELD_MOBILE_NO = CommonUtils.getPropValue("crsmsg" , "sync.field.mobileno.index");
	public static final String SYNC_FIELD_CUST_REF_NO = CommonUtils.getPropValue("crsmsg" , "sync.field.custrefno.index");
	public static final String FTP_REPORT_TYPE_1 = CommonUtils.getPropValue("crsmsg" , "ftpreport.type.1");
	public static final String FTP_REPORT_TYPE_2 = CommonUtils.getPropValue("crsmsg" , "ftpreport.type.2");
	public static final String SMS_SOURCE_TYPE_1 = CommonUtils.getPropValue("crsmsg" , "sms.source.type.1");// Online Ad Hoc send SMS
	public static final String SMS_SOURCE_TYPE_2 = CommonUtils.getPropValue("crsmsg" , "sms.source.type.2");// HTTP Post send SMS
	public static final String SMS_SOURCE_TYPE_3 = CommonUtils.getPropValue("crsmsg" , "sms.source.type.3");// Bulk send SMS
	public static final String SMS_SOURCE_TYPE_4 = CommonUtils.getPropValue("crsmsg" , "sms.source.type.4");// FTP send SMS
	public static final String SMS_SOURCE_TYPE_5 = CommonUtils.getPropValue("crsmsg" , "sms.source.type.5");// System send SMS
	public static final String SMS_INCOME_PARAM_MOBILE_NO = CommonUtils.getPropValue("crsmsg" , "sms.income.param.mobileno");
	public static final String SMS_INCOME_PARAM_MESSAGE = CommonUtils.getPropValue("crsmsg" , "sms.income.param.message");
	public static final String SMS_INCOME_PARAM_RECEIVED_DATETIME = CommonUtils.getPropValue("crsmsg" , "sms.income.param.receiveddatetime");
	public static final String SMS_INCOME_SHORTCODE = CommonUtils.getPropValue("crsmsg" , "sms.income.param.shortcode");
	public static final String SMS_INCOME_MSG_ID = CommonUtils.getPropValue("crsmsg" , "sms.income.param.msgid");
	public static final String SMS_INCOME_RKEY = CommonUtils.getPropValue("crsmsg" , "sms.income.param.rkey");
	public static final String SMS_INCOME_SUBID = CommonUtils.getPropValue("crsmsg" , "sms.income.param.subid");
	public static final String SMS_INCOME_TELCOID = CommonUtils.getPropValue("crsmsg" , "sms.income.param.telcoid");
	public static final String FILE_CONVERSION_NORMAL_FTP = CommonUtils.getPropValue("crsmsg" , "fileconversion.ftp");
	public static final String FILE_CONVERSION_BULK_SMS = CommonUtils.getPropValue("crsmsg" , "fileconversion.bulk");
	public static final String FILE_CONVERSION_CC_FTP = CommonUtils.getPropValue("crsmsg" , "fileconversion.ftp.cc");
	public static final String FILE_CONVERSION_PHONE_SYNCHRONISATION = CommonUtils.getPropValue("crsmsg" , "fileconversion.sync.phone");
	public static final String TELCO_MAXIS_URL_USERHEADER=CommonUtils.getPropValue("crsmsg" , "telco.maxis.url.userheader");
	public static final String TEMPLATE_FIELD_SIBS_REF = CommonUtils.getPropValue("crsmsg" , "template.field.sibs.ref");
	public static final String TEMPLATE_FIELD_SIBS_MSG = CommonUtils.getPropValue("crsmsg" , "template.field.sibs.msg");
	public static final String CC_ACTIVATION_SVC_WINDOW_SEPARATOR = CommonUtils.getPropValue("crsmsg" , "cc.activation.svcWindow.separator");	
	public static final String TEMPLATE_FIELD_CC_REF = CommonUtils.getPropValue("crsmsg" , "template.field.cc.ref");
	public static final String AUTOACTION_SVC_WINDOW_SEPARATOR = CommonUtils.getPropValue("crsmsg" , "autoAction.svcWindow.separator");	
	public static final String FILE_CONVERSION_CCA_PHONE_SYNCHRONISATION = CommonUtils.getPropValue("crsmsg" , "fileconversion.cca.sync.phone");

	/* Application Defined Value */
	
	/* SSO Member URL*/
	public static final String ACTIVEMQ_CONSOLE_URL=CommonUtils.getPropValue("crsmsg" , "activemq.web.url");	
	
	/* Error Messages */
	public static final String ERRMSG_SYSTEM = "errors.system";
	public static final String ERRMSG_RECORDAPPROVED = "errors.crsmsg.record.approved";
	public static final String ERRMSG_RECORDRESEND = "errors.crsmsg.record.resend";
	public static final String ERRMSG_RECORDDELETE= "errors.crsmsg.record.delete";
	public static final String ERRMSG_NOTLOGON = "errors.logon.notlogon";
	public static final String ERRMSG_RECORD_NOTFOUND = "errors.crsmsg.record.notfound";
	public static final String ERRMSG_RECORD_NOTFOUND_FOR = "errors.crsmsg.record.no.found.for";
	public static final String ERRMSG_RECORD_NOTFOUND_WITHREASON ="errors.crsmsg.record.notfound.withreason";
	public static final String ERRMSG_RECORD_EXIST = "errors.crsmsg.record.exist";
	public static final String ERRMSG_RECORD_NOT_EXIST ="errors.crsmsg.record.notexist";
	public static final String ERRMSG_RECORD_IN_USED ="errors.crsmsg.record.inused";	
	public static final String ERRMSG_ACTION_SUCCESS="errors.crsmsg.action.success";
	public static final String ERRMSG_ACTION_SUCCESS_WITHWARNING="errors.crsmsg.action.success.withwarning";
	public static final String ERRMSG_ACTION_FAIL="errors.crsmsg.action.fail";
	public static final String ERRMSG_NO_RECORD_SELECTED="errors.crsmsg.noRecordSelected";
	public static final String ERRMSG_ACTION_FAIL_WITHREASON="errors.crsmsg.action.fail.withreason";
	public static final String ERRMSG_ACTION_STATUS="errors.crsmsg.action.status";
	public static final String ERRMSG_ACTION_EMAIL_SUCCESS="errors.crsmsg.action.email.success";
	public static final String ERRMSG_ACTION_EMAIL_FAIL="errors.crsmsg.action.email.fail";
	public static final String ERRMSG_ACTION_FAIL_NOCHANGE="errors.crsmsg.action.fail.nochange";
	public static final String ERRMSG_ACTION_FAIL_RESTRICTION="errors.crsmsg.action.fail.restriction";
	public static final String ERRMSG_ACTION_FAIL_DUPLICATE="errors.crsmsg.action.fail.duplicate";
	public static final String ERRMSG_FIELD_MANDATORY = "errors.crsmsg.field.mandatory";
	public static final String ERRMSG_SPECIAL_CHARACTERS = "errors.crsmsg.field.special";
	public static final String ERRMSG_FIELD_SEARCH_DATE_RANGE_DAYS = "errors.crsmsg.field.date.range.days";
	public static final String ERRMSG_FILE_LENGTH_EXCEED= "errors.crsmsg.file.name.exceed";
	public static final String ERRMSG_ACTION_DENIED ="errors.crsmsg.actiondenied";
	public static final String ERRMSG_ACTION_DENIED_EMAIL_DOMAIN ="errors.email.domain.notallowed";
	public static final String ERRMSG_FIELD_LIMIT_EXCEED = "errors.crsmsg.field.limit.exceed";
	public static final String ERRMSG_FIELD_COMPARE_NOTMATCH = "errors.crsmsg.field.compare.notmatch";
	public static final String ERRMSG_FIELD_FORMAT_INVALID = "errors.crsmsg.format.invalid";
	public static final String ERRMSG_FIELD_MUST_GREATER_THAN = "errors.crsmsg.field.mustGreaterThan";	
	public static final String ERRMSG_DATECOMPARE = "errors.crsmsg.datecompare";
	public static final String ERRMSG_SELECT ="error.crsmsg.select";
	public static final String ERRMSG_HOLIDAY_SAVE_SUCCESS="errors.crsmsg.holiday.save.success";
	public static final String ERRMSG_HOLIDAY_DESC_BLANK="errors.crsmsg.holiday.holdesc.blank";
	public static final String ERRMSG_HOLIDAY_WORK_START_DATE_BLANK="errors.crsmsg.holiday.workstart.blank";
	public static final String ERRMSG_HOLIDAY_HOLIDAY_START_DATE_BLANK="errors.crsmsg.holiday.holstart.blank";
	public static final String ERRMSG_HOLIDAY_HOLIDAY_START_LATERTHEN_END="errors.crsmsg.holiday.startend";
	public static final String PARAMETER_SEARCH = "search";
	public static final String PARAMETER_SEARCH_INCOMING = "search_incoming";
	public static final String PARAMETER_SEARCH_OUTGOING = "search_outgoing";
	
	public static final String PARAMETER_SEARCHDETAILS = "searchDetails";
	public static final String PARAMETER_LOAD = "load";
	public static final String PARAMETER_LOADINCOMING = "loadIncoming";
	public static final String ERRMSG_PARAMETERS_RECORD ="Record";
	public static final String ERRMSG_PARAMETERS_ATTACHMENT ="Attachment";
	public static final String ERRMSG_PARAMETERS_DOCUMENT_EDIT_HISTORY ="Document edit history";
	public static final String ERRMSG_PARAMETERS_DOCUMENT_APPROVAL_HISTORY ="Document approval history";
	public static final String ERRMSG_PARAMETERS_CASE_SUMMARY ="Case Summary";
	public static final String ERRMSG_PARAMETERS_EMAIL ="Email";
	public static final String ERRMSG_PARAMETERS_CATEGORY ="category";
	public static final String ERRMSG_PARAMETERS_EXPERT_ACTION_11_STR ="Require Management Approval";//Require Management Approval
	public static final String ERRMSG_PARAMETERS_EXPERT_LVL2NABOVE ="Lvl 2 Experts and above";
	public static final String ERRMSG_ACTION_SAVE_SUCCESS="errors.crsmsg.save.success";
	public static final String ERRMSG_INCORRECT_KEY_LENGTH="errors.crsmsg.incorrect.keylength";	
	
	/* Error Messages */
	
	/* Status */
	public static final String STATUS_OPEN= "OPEN";
	public static final String STATUS_CLOSED= "CLOSED";
	public static final String STATUS_YES= "Y";
	public static final String STATUS_NO= "N";
	public static final String STATUS_SUCCESS= "S";
	public static final String STATUS_FAIL= "F";
	public static final String STATUS_TRUE= "TRUE";
	public static final String STATUS_TRUE_1= "1";
	public static final String STATUS_FALSE= "FALSE";
	public static final String STATUS_FALSE_0= "0";
	public static final String STATUS_ACTIVE= "Active";
	public static final String STATUS_DISABLED= "Disabled";
	public static final String STATUS_SUBSCRIPTION= "S";
	public static final String STATUS_UNSUBSCRIPTION= "U";
	public static final String STATUS_OK= "OK";
	/* Status */
	
	/* Actions Parameters */
	public static final String ACTION_PARA_RECORD= "Record";
	public static final String ACTION_PARA_AUDIT_TRAIL= "Audit Trail";
	public static final String ACTION_PARA_SAVED= "saved";
	public static final String ACTION_PARA_APPROVED= "approved";
	public static final String ACTION_PARA_DELETED= "deleted";
	public static final String ACTION_PARA_UPDATED= "updated";
	public static final String ACTION_PARA_RESEND= "resend";
	public static final String ACTION_PARA_PROCESS= "process";
	public static final String ACTION_PARA_REASON_QUOTA_EXCEED= "SMS Quota has been exceeded! Please top up for SMS to send!";
	/* Actions Parameters */
	
	/* Common Strings */
	public static final String REPORT_MAX_DATE_RANGE = CommonUtils.getPropValue("crsmsg" , "report.search.max.range");;
	public static final String STR_SAVE="Save";
	public static final String STR_SUBMIT="Submit";
	public static final String STR_ROUTE_LEVEL_0= "0";
	public static final String STR_PERCENT = "%";
	public static final String STR_EMPTY = "";
	public static final String STR_STATUS_MK = "-1";//for macrokiosk succes MO
	public static final String STR_SPACE= " ";
	public static final String STR_PAGER_OFFSET="pager.offset";
	public static final String STR_SEARCH="search";
	public static final String STR_VAR="var";
	public static final String STR_COUNT="count";
	public static final String STR_ZERO = "0";
	public static final String STR_EMPTYSTRING = " ";
	public static final String STR_DASH = "-";
	public static final String STR_COMMA = ",";
	public static final String STR_BY_MOBILE="By Mobile #";
	public static final String STR_BY_SENDER="By Sender";
	public static final String STR_BY_DEPARTMENT="By Department ";
	public static final String STR_BY_MOBILE_OPERATOR="By Telco";
	public static final String STR_BY_TELCO_API="By Svc Provider API";
	public static final String STR_BY_SEND_MODE="By Send Mode";
	public static final String STR_BY_SEND_TYPE="By Send Type";
	public static final String STR_BY_CUST_REF_NO="By Cust Ref No.";
	public static final String STR_BY_KEYWORD="By Keyword";
	public static final String STR_BY_PREFIX="By Prefix";
	public static final String STR_FILE_NAME="File Name";
	public static final String STR_MOBILE_NO="Mobile No";
	public static final String STR_CARD_NO="Card No";
	public static final String STR_IC_NO="IC No";
	public static final String STR_STATUS="Status";
	public static final String STR_BY_AUTO_ACTION="By Action Method";
	/* Common Strings */
	
	/* Special Characters */
	public static final String SPECIAL_CHAR_NEWLINE="\r\n";
	public static final String SPECIAL_CHAR_COMMA=",";
	public static final String SPECIAL_CHAR_PIPE="|";
	public static final String SPECIAL_CHAR_DOT=".";
	public static final String SPECIAL_DASH="-";
	public static final String SPECIAL_SLASH="/";
	public static final String SPECIAL_BACKSLASH="\\";
	public static final String SPECIAL_QUOTE_SINGLE="'";
	/* Special Characters */
	
	/* Priority Level */
	public static final String PRIORITY_LEVEL_1 ="1";
	public static final String PRIORITY_LEVEL_2 ="2";
	public static final String PRIORITY_LEVEL_3 ="3";
	public static final String PRIORITY_LEVEL_4 ="4";
	public static final String PRIORITY_LEVEL_5 ="5";
	public static final String PRIORITY_LEVEL_6 ="6";
	public static final String PRIORITY_LEVEL_7 ="7";
	public static final String PRIORITY_LEVEL_8 ="8";
	public static final String PRIORITY_LEVEL_9 ="9";
	/* Report Label */
	
	/* SMS Status */
	public static final String SMS_STATUS_1="S"; // Sent
	public static final String SMS_STATUS_2="U"; // Unsent (Failed)
	public static final String SMS_STATUS_3="A"; // Pending Approval
	public static final String SMS_STATUS_4="Q"; // Queue
	public static final String SMS_STATUS_5="R"; // Resend
	public static final String SMS_STATUS_6="T"; // Time/Scheduled
	public static final String SMS_STATUS_7="V"; // Pending Verification
	public static final String SMS_STATUS_M="M"; // Messages in processor memory
	
	/*SMS Sender */
	public static final String SMS_REMARK_TELCO_CHECK="Telco Status Check";
	
	/* SMS Charge Mode */
	public static final String SMS_CHARGE_MODE_1="I"; // Inter charge
	public static final String SMS_CHARGE_MODE_2="W"; // Within Charge

	/* Message Resource Properties Defined Value */
	public static final String MAIL_STATUS_SUCCESS="SUCCESS";
	/* Message Resource Properties Defined Value */
	
	/*Export to CSV*/
	
	public static final String PARAMETER_GENERATECVS = "generateCSV";
	public static final String PARAMETER_GENERATE_BILLINGDETAILSCVS = "generateBillingReportDetailsCSV";
	public static final String PARAMETER_APPROVE = "approveMsg";
	public static final String PARAMETER_DELETE = "deleteMsg";
	public static final String PARAMETER_RESEND = "resendMsg";
	public static final String PARAMETER_APPROVEFTP = "approveFTP";
	public static final String OUTBOX_KEY = "outboxData";
	public static final String INBOX_KEY = "inboxData";
	public static final String RECEIVEDLOG_KEY = "receivedLogData";
	public static final String QLOG_KEY = "qLogData";
	public static final String SENTLOG_KEY = "sentLogData";
	public static final String BILLINGBYDEPT_KEY = "BillingReportByDeptData";
	public static final String BILLINGBYDEPT_KEY_RATES = "OutgoingReportRates";
	public static final String BILLINGBYDEPT_KEY_SVC_PROVIDER = "OutgoingReportSvcPvds";
	public static final String TELCOLIST_KEY = "telcoListData";
	public static final String PENDINGAPPROVAL_KEY = "pendingAppData";
	public static final String FTP_REPORT_KEY = "ftpReport";
	public static final String FTPBULKLOG_KEY = "ftpBulkLogData";
	public static final String MANUALBULKLOG_KEY = "manualBulkLogData";
	public static final String UNSENTLOG_KEY = "unsentLogData";
	public static final String DBOBJ = "dbobj";
	public static final String TOTALSMS_KEY = "totalSmsData";
	public static final String CONTENTTYPE_CSV = "text/csv";
	public static final String HEADER = "Content-disposition";
	public static final String CSV_STRSTART = "\"";
	public static final String CSV_STREND = "\",";
	public static final String CSV_STREMPTY = "\"\",";
	public static final String STR_FILENAME = "filename=";
	public static final String CSVFILENAME_USERREPORT ="User.csv";
	public static final String CSVFILENAME_OUTBOXREPORT ="Outbox.csv";
	public static final String CSVFILENAME_INBOXREPORT ="Inbox.csv";
	public static final String CSVFILENAME_COMMON_INBOXREPORT ="CommonInbox.csv";
	public static final String CSVFILENAME_RECEIVEDLOGREPORT ="Received Log.csv";
	public static final String CSVFILENAME_QLOGREPORT ="Qlog.csv";
	public static final String CSVFILENAME_QUEUEREPORT ="QReport.csv";
	public static final String CSVFILENAME_PAPROVALREPORT ="PAReport.csv";
	public static final String CSVFILENAME_PVERIFICATIONREPORT ="PVReport.csv";
	public static final String CSVFILENAME_SENTREPORT ="SentReport.csv";
	public static final String CSVFILENAME_UNSENTREPORT ="UnsentReport.csv";
	public static final String CSVFILENAME_TOTALREPORT ="TotalReport.csv";
	public static final String CSVFILENAME_INVALIDREPORT ="InvalidReport.csv";
	public static final String CSVFILENAME_SENTLOGREPORT ="Sentlog.csv";
	public static final String CSVFILENAME_BILLINGREPORT ="BillingReport.csv";
	public static final String CSVFILENAME_UNSENTLOGREPORT ="Unsentlog.csv";
	public static final String CSVFILENAME_REPORT ="Report.csv";
	public static final String CSVFILENAME_PENDINGAPPLOGREPORT ="PendingApprovallog.csv";
	public static final String CSVFILENAME_FTPBULKLOGREPORT ="FTPReport.csv";
	public static final String CSVFILENAME_MANUALBULKLOGREPORT ="ManualBulkLog.csv";
	public static final String CSVFILENAME_FTPREPORT ="FTPLog.csv";
	public static final String CSVFILENAME_ADDRESSBOOK ="AddressBook.csv";
	public static final String CSVFILENAME_TEMPLATEMESSAGE ="TemplateMessage.csv";
	public static final String CSVFILENAME_IPADDRESSLIST ="IPAddressList.csv";
	public static final String CARD_LOG_KEY = "cardActivationLog";
	public static final String CSVFILENAME_CARDACTIVATIONLOG = "CardActivationLog.csv";
	public static final String CSVFILENAME_CARDACTIVATIONLOGDTLS = "CardActivationLogDetails.csv";
	public static final String CSVFILENAME_CARDACTIVATIONSMSLOG = "CardActivationReceivedSMSLog.csv";
	public static final String FTP_REPORT_ID="ftp_report_id";
	public static final String FTP_FILE_NAME="fileName";
	public static final String FTP_REMARK="remark";
	public static final String FTP_START_DATE="startDate";
	public static final String FTP_TYPE = "type";
	public static final String MESSAGE = "message";
	public static final String Y="Y";
	public static final String F="F";
	public static final String P="P";        
	public static final String GST="GST";
	public static final String HIBERNATE_CONFIG_FILE="hibernate.crsmsg.cfg.xml";
}
