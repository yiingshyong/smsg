package com.bcb.crsmsg.util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

import com.bcb.common.util.DBUtils;

public class DBUtilsCrsmsg extends DBUtils{
	private String command;
	private String[] cmdParamName;
	private NullableType[] cmdType;
	private Object[] cmdParamObj;	
	
	
	public DBUtilsCrsmsg(String command, String[] cmdParamName,
			NullableType[] cmdType, Object[] cmdParamObj) {
		super();
		this.command = command;
		this.cmdParamName = cmdParamName;
		this.cmdType = cmdType;
		this.cmdParamObj = cmdParamObj;
	}

	public static List DBHQLCommand(String dbCommand){
		return DBHQLCommand(dbCommand, new Object[]{});
	}
	
	public static List DBHQLCommand(String dbCommand, Object[] cmdParameters){
		List resultList=new ArrayList();
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		resultList=DBHQLCommand(dbCommand, cmdParameters, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return resultList;
		}else{
			return new ArrayList();
		}	
	}
	
	public static List DBSQLCommand(String dbCommand){
		return DBSQLCommand(dbCommand, new String[]{}, new NullableType[]{}, new Object[]{});
	}
	
	public static List DBSQLCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters){
		List resultList=new ArrayList();
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		
		resultList=DBSQLCommand(dbCommand, cmdParamName, cmdParamType, cmdParameters, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return resultList;
		}else{
			return new ArrayList();
		}	
	}
	public static List DBSQLCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters, String hibernateValue){
		List resultList=new ArrayList();
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactoryServer(hibernateValue).getCurrentSession();
		
		resultList=DBSQLCommand(dbCommand, cmdParamName, cmdParamType, cmdParameters, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return resultList;
		}else{
			return new ArrayList();
		}	
	}
	public static ScrollableResults DBSQLScrollCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters){
		ScrollableResults resultList=null;
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		
		resultList=DBSQLScrollCommand(dbCommand, cmdParamName, cmdParamType, cmdParameters, hibernate_session);
		return resultList;
	}
	public static ScrollableResults DBSQLScrollCommand(String dbCommand, String[] cmdParamName, NullableType[] cmdParamType, Object[] cmdParameters, String hibernateValue){
		ScrollableResults resultList=null;
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactoryServer(hibernateValue).getCurrentSession();
		
		resultList=DBSQLScrollCommand(dbCommand, cmdParamName, cmdParamType, cmdParameters, hibernate_session);
		return resultList;
	}
	public static Object get(Class theModalClass, Object primaryKey){
		Object obj=null;
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		
		obj=get(theModalClass, primaryKey, hibernate_session);
		if(closeTransaction(hibernate_session)){
			return obj;
		}else{
			return null;
		}
	}
	
	public static boolean saveOrUpdate(Object modalObject){
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		if(saveOrUpdate(modalObject, hibernate_session)){
			return closeTransaction(hibernate_session);
		}else{
			return false;
		}
	}
	
	public static boolean delete(Object modalObject){
		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactory().getCurrentSession();
		if(delete(modalObject, hibernate_session)){
			return closeTransaction(hibernate_session);
		}else{
			return false;
		}
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String[] getCmdParamName() {
		return cmdParamName;
	}

	public void setCmdParamName(String[] cmdParamName) {
		this.cmdParamName = cmdParamName;
	}

	public NullableType[] getCmdType() {
		return cmdType;
	}

	public void setCmdType(NullableType[] cmdType) {
		this.cmdType = cmdType;
	}

	public Object[] getCmdParamObj() {
		return cmdParamObj;
	}

	public void setCmdParamObj(Object[] cmdParamObj) {
		this.cmdParamObj = cmdParamObj;
	}
}
