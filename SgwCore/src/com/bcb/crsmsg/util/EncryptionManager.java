package com.bcb.crsmsg.util;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class EncryptionManager {

	private static final Log log = LogFactory.getLog(EncryptionManager.class);
	private static EncryptionManager instance;
	private SecretKeySpec secretKey ;
	private static byte[] key ;
	private StandardPBEStringEncryptor jasyptEnc;

	private EncryptionManager() {
		super();
	}

	public static EncryptionManager getInstance(){
		if(instance == null){
    	MessageDigest sha = null;
		try {
			log.info("Creating EncryptionManager");
			instance = new EncryptionManager();					
			String encryptionPass = CommonUtils.getPropValue("encryptManager" , "encryption.key" ); 
			
			key = encryptionPass.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-256");
			key = sha.digest(key);
	        key = Arrays.copyOf(key, 32); // for 256 bit - NEED the unlimited security policy jar FILE in jdk
			instance.secretKey = new SecretKeySpec(key, "AES");
			
		} catch(Exception e){
			System.out.println(e.getStackTrace());
			throw new RuntimeException("Error creating encryption manager. Reason: " + e.getMessage(), e);
		}
		}
		return instance;
    }
	
	
	public String encrypt(String clearText){
		try{
			 Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		     cipher.init(Cipher.ENCRYPT_MODE, instance.secretKey);
			 return Base64.encodeBase64String(cipher.doFinal(clearText.getBytes("UTF-8")));
		}catch(Exception e){
			throw new SecurityException("Encryption Manager:Error while encrypting.", e );
		}
	}

	public String decrypt(String encryptedText){
		try{
			
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, instance.secretKey);		
			return new String(cipher.doFinal(Base64.decodeBase64(encryptedText)));
		}catch(Exception e){
			throw new SecurityException("Encryption Manager:Error while decrypting.", e);
		}
	}


	public StandardPBEStringEncryptor getJasyptEnc() {
		return jasyptEnc;
	}

	public void setJasyptEnc(StandardPBEStringEncryptor jasyptEnc) {
		this.jasyptEnc = jasyptEnc;
	}
	
	
}
