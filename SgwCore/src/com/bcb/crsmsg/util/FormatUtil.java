package com.bcb.crsmsg.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatUtil {
	private static FormatUtil instance;
	private static SimpleDateFormat _dateFormatter;
	private static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
	private static final String sPositiveNumberPrefix = "###,###,###,###,###,###,##0.";
	private static final String sNegativeNumberPrefix = "-###,###,###,###,###,###,##0.";
	
	public String formatNumber(double dNumber, int iDecimalPlaces) {
	    DecimalFormat df = new DecimalFormat(getNumberFormat(iDecimalPlaces));
	    return df.format(dNumber);
	}
	
	public double formatDoubleNumber(double dNumber, int iDecimalPlaces) {
	    DecimalFormat df = new DecimalFormat(getNumberFormat(iDecimalPlaces));
	    return Double.parseDouble(df.format(dNumber));
	}
	
	public String formatCurrency(double dNumber) {
		DecimalFormat formatter = new DecimalFormat("#,###,###,###");
		return formatter.format(dNumber);
	}
	
	public String getNumberFormat(int decimalPlaces) {
        String sDecimalSuffix = padBack(0, decimalPlaces, '0');
        StringBuffer pattern = new StringBuffer(sPositiveNumberPrefix).append(sDecimalSuffix).append(';').append(sNegativeNumberPrefix).append(sDecimalSuffix);
        return pattern.toString();
	}
	
	public static String padBack(int iInString, int iStringLen, char cPadChar) {
	        return padBack(String.valueOf(iInString), iStringLen, cPadChar);
	}
	 
	private static String padBack(String sInString, int iStringLen, char cPadChar) {
	        StringBuffer sbBuff = new StringBuffer(sInString);
	
	        while (sbBuff.length() < iStringLen) {
	            sbBuff.append(cPadChar);
	        }
	
	        return sbBuff.toString();
    }
	
	
	/**
     * Get instance of DateUtil.
     * 
     * @return instance of DateUtil
     */
	private FormatUtil() {
			
		 _dateFormatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
	}
	
    public static synchronized FormatUtil getInstance() {
        if (instance == null) {
            instance = new FormatUtil();
        }

        return instance;
    }
    
    public String stringByDayMonthYear(Date date){
		
    	if (date == null){
    		return "";
    	}
		
		return _dateFormatter.format(date);
	}
}
