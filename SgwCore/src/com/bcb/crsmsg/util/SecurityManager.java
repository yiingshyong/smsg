package com.bcb.crsmsg.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.MessageDigest;
//import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.sgwcore.util.DaoBeanFactory;
//sun.misc.BASE64Encoder; -java8 edited 20191124

public class SecurityManager {

	private static final Log log = LogFactory.getLog(SecurityManager.class);
	private static SecurityManager instance;
	private Key key;
	private StandardPBEStringEncryptor jasyptEnc;

	private SecurityManager() {
		super();
	}

	public static SecurityManager getInstance(){
		if(instance == null){
			try{
				log.info("Creating security manager");
				instance = new SecurityManager();					
				SmsCfg config = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
				instance.key = null;
				if(StringUtils.isEmpty(config.getSerData())){
					log.warn("Generating new encryption key. Will have problem decrypting old data.");
					KeyGenerator gen = KeyGenerator.getInstance("AES");
					gen.init(128);
					instance.key = gen.generateKey();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ObjectOutputStream oos = new ObjectOutputStream(baos);
					oos.writeObject(instance.key);
					oos.flush();
					oos.close();
					String objectEncodedText = Base64.encodeBase64String(baos.toByteArray());
					config.setSerData(objectEncodedText);
					DaoBeanFactory.getSmsCfgDao().update(config);
				}
				if(instance.key == null){
					String objectEncodedText = config.getSerData();
					instance.key = (Key)new ObjectInputStream(new ByteArrayInputStream(Base64.decodeBase64(objectEncodedText))).readObject();
				}
				instance.jasyptEnc = new StandardPBEStringEncryptor(); 
				instance.jasyptEnc.setAlgorithm("PBEWithMD5AndDES"); 
				instance.jasyptEnc.setPassword("p@33w0rd"); 								
			}catch(Exception e){
				throw new RuntimeException("Error creating security manager. Reason: " + e.getMessage(), e);
			}
		}
		return instance;
	}
	
	
	public String decrypt(String encryptedText){
		try{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, instance.key);			
			return new String(cipher.doFinal(Base64.decodeBase64(encryptedText)));
		}catch(Exception e){
			throw new SecurityException("Error while decrypting.", e);
		}
	}
	
	public String encrypt(String clearText){
		try{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, instance.key);
			return Base64.encodeBase64String(cipher.doFinal(clearText.getBytes()));
		}catch(Exception e){
			throw new SecurityException("Error while encrypting.", e );
		}
	}
	
	public String decryptKeyword(String encryptedText){
		try{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, instance.key);			
			return new String(cipher.doFinal(Base64.decodeBase64(encryptedText)));
		}catch(Exception e){
			throw new SecurityException("Error while decrypting.", e);
		}
	}
	
	public String encryptKeyword(String clearText){
		try{
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, instance.key);
			return Base64.encodeBase64String(cipher.doFinal(clearText.getBytes()));
		}catch(Exception e){
			throw new SecurityException("Error while encrypting.", e );
		}
	}
	
	public String encryptSHA2(String clearText){ 
		MessageDigest md = null;
		String hash=null; 
		try
		{
			md = MessageDigest.getInstance("SHA-256"); //step 2
			md.update(clearText.getBytes("UTF-8")); //step 3
			 
			byte raw[] = md.digest(); //step 4
			//hash = (new BASE64Encoder()).encode(raw); //step 5
			//edited 20191124
			//Base64. mimeEncoder = java.util.Base64.getMimeEncoder();	
			//hash = Base64.encodeBase64String(raw); //step 5
			hash = new String(java.util.Base64.getMimeEncoder().encode(raw));
		}
		catch(Exception e)
		{
			throw new SecurityException("Error while encrypting.", e );
		}
		return hash; //step 6
	}

	public StandardPBEStringEncryptor getJasyptEnc() {
		return jasyptEnc;
	}

	public void setJasyptEnc(StandardPBEStringEncryptor jasyptEnc) {
		this.jasyptEnc = jasyptEnc;
	}
	
	
}
