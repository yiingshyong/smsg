package com.bcb.crsmsg.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMailBean {
	private String p_from="";
	private String p_to="";
	private String p_cc="";
	private String p_bcc="";
	private String p_subject="";
	private String p_message="";
	private String p_smtpServer=CommonUtils.getPropValue("crsmsg" , "server.email");
	public final static String MAIL_STATUS_SUCCESS="S";
	
	public String send(){
		return send(p_from, p_to, p_cc, p_bcc, p_subject, p_message, p_smtpServer);
	}
	
	public String send(String p_from, String p_to, String p_cc, String p_bcc,
			String p_subject, String p_message, String p_smtpServer) 
	{
		this.p_from=p_from;
		this.p_to=p_to;
		this.p_cc=p_cc;
		this.p_bcc=p_bcc;
		this.p_subject=p_subject;
		this.p_message=p_message;
		this.p_smtpServer=p_smtpServer;  
		
		
		//String l_result = "<BR><BR><BR><BR><BR><BR><BR>";
		// Name of the Host machine where the SMTP server is running
		String l_result = "";
		String l_host = p_smtpServer;
		
		// Gets the System properties
		Properties l_props = System.getProperties();
		
		// Puts the SMTP server name to properties object
		l_props.put("mail.smtp.host", l_host);
		
		// Get the default Session using Properties Object
		Session l_session = Session.getDefaultInstance(l_props, null);
		
		l_session.setDebug(true); // Enable the debug mode
		
		try {
			
			MimeMessage l_msg = new MimeMessage(l_session); // Create a New message
			
			l_msg.setFrom(new InternetAddress(p_from)); // Set the From address
			
			// Setting the "To recipients" addresses
			l_msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(p_to, false));
						
			// Setting the "Cc recipients" addresses 
			l_msg.setRecipients(Message.RecipientType.CC,
					InternetAddress.parse(p_cc, false));
			
			// Setting the "BCc recipients" addresses
			
			l_msg.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse(p_bcc, false));
			
			l_msg.setSubject(p_subject); // Sets the Subject
			
			// Create and fill the first message part
			MimeBodyPart l_mbp = new MimeBodyPart();
			l_mbp.setText(p_message);
			
			// Create the Multipart and its parts to it
			Multipart l_mp = new MimeMultipart();
			l_mp.addBodyPart(l_mbp);
			
			
			// Add the Multipart to the message
			l_msg.setContent(l_mp);
			
			// Set the Date: header
			l_msg.setSentDate(new Date());
			
			// Send the message
			Transport.send(l_msg);
			
			l_result = MAIL_STATUS_SUCCESS;
			// If here, then message is successfully sent.
			// Display Success message
			//l_result = l_result + "<FONT SIZE=4 COLOR=\"blue\"><B>Success!</B>"+
			//           "<FONT SIZE=4 COLOR=\"black\"> "+
			
			//           "<HR><FONT color=green><B>Mail was successfully sent to </B></FONT>: "+p_to+"<BR>";
			//if CCed then, add html for displaying info
			//if (!p_cc.equals(""))
			//  l_result = l_result +"<FONT color=green><B>CCed To </B></FONT>: "+p_cc+"<BR>";
			//if BCCed then, add html for displaying info
			//if (!p_bcc.equals(""))
			//  l_result = l_result +"<FONT color=green><B>BCCed To </B></FONT>: "+p_bcc ;
			
			//l_result = l_result+"<BR><HR>";
		} catch (MessagingException mex) { // Trap the MessagingException Error
			// If here, then error in sending Mail. Display Error message.
			//l_result = l_result + "<FONT SIZE=4 COLOR=\"blue\"> <B>Error : </B><BR><HR> "+
			//          "<FONT SIZE=3 COLOR=\"black\">"+mex.toString()+"<BR><HR>";
			
			mex.printStackTrace();
			l_result = mex.toString();
			
		} catch (Exception e) {
			
			// If here, then error in sending Mail. Display Error message.
			//l_result = l_result + "<FONT SIZE=4 COLOR=\"blue\"> <B>Error : </B><BR><HR> "+
			//           "<FONT SIZE=3 COLOR=\"black\">"+e.toString()+"<BR><HR>";
			l_result = e.toString();
			e.printStackTrace();
			
		}//end catch block
		
		return l_result;
		
	} // end of method send
	
	String ArrayToString(ArrayList<String> arrayStr){
		String combineStr="";
		for(int i=0;i<arrayStr.size();i++){
			if(i!=0){
				combineStr+=",";
			}
			combineStr+=arrayStr.get(i);
		}
		return combineStr;
	}
	
	/**
	 * @return Returns the p_message.
	 */
	public String getP_message() {
		return p_message;
	}
	
	/**
	 * @param p_message The p_message to set.
	 */
	public void setP_message(String p_message) {
		this.p_message = p_message;
	}
	
	/**
	 * @return Returns the p_smtpServer.
	 */
	public String getP_smtpServer() {
		return p_smtpServer;
	}
	
	/**
	 * @param server The p_smtpServer to set.
	 */
	public void setP_smtpServer(String server) {
		p_smtpServer = server;
	}
	
	/**
	 * @return Returns the p_subject.
	 */
	public String getP_subject() {
		return p_subject;
	}
	
	/**
	 * @param p_subject The p_subject to set.
	 */
	public void setP_subject(String p_subject) {
		this.p_subject = p_subject;
	}

	public String getP_bcc() {
		return p_bcc;
	}

	public void setP_bcc(String p_bcc) {
		this.p_bcc = p_bcc;
	}

	public String getP_cc() {
		return p_cc;
	}

	public void setP_cc(String p_cc) {
		this.p_cc = p_cc;
	}

	public String getP_from() {
		return p_from;
	}

	public void setP_from(String p_from) {
		this.p_from = p_from;
	}

	public String getP_to() {
		return p_to;
	}

	public void setP_to(String p_to) {
		this.p_to = p_to;
	}
	
} //end of bean
