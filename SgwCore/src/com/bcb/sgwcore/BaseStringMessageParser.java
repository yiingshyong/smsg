package com.bcb.sgwcore;

public class BaseStringMessageParser {

	protected int offset;
	protected String msgStr;
	
	protected String getField(int length){
		String field = this.msgStr.substring(offset, offset+length);
		offset+=length;
		return field;
	}
	
}
