package com.bcb.sgwcore;

public class BaseStringMessageParserOCM
{

	protected int offset;
	protected String msgStr;
	
	protected String getField(int length){
		String field = this.msgStr.trim().substring(offset, offset+length);
		offset+=length;
		return field;
	}
	
}
