package com.bcb.sgwcore;

public class SgwException extends Exception {
	
	public static final String SMS_CFG_NOT_FOUND = "SMS Config Not Found";
	public static final String USER_NOT_FOUND = "User Not Found";

	private static final long serialVersionUID = 1L;

	public SgwException() {
		super();
	}

	public SgwException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SgwException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SgwException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
