package com.bcb.sgwcore.creditcard;

import org.apache.commons.lang.StringUtils;

import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.sgwcore.BaseStringMessageParser;

public class CCAHostRequestBean extends BaseStringMessageParser{
	
	public static final String DEFAULT_1 = "TCPT";
	public static final String DEFAULT_2 = "";
	public static final String DEFAULT_3 = "00000076";
	public static final String DEFAULT_4 = "CSMS";
	public static final String DEFAULT_5 = "C301";
	public static final int DEFAULT_1_LENGTH = 4;
	public static final int DEFAULT_2_LENGTH = 46;	
	public static final int DEFAULT_3_LENGTH = 8;	
	public static final int DEFAULT_4_LENGTH = 4;	
	public static final int DEFAULT_5_LENGTH = 4;	
	public static final int CC_NO_LENGTH = 16;	
	public static final int CUST_ID_LENGTH = 26;	
	public static final int MOBILE_NO_LENGTH = 18;	

	private String default1;
	private String default2;
	private String default3;
	private String default4;
	private String default5;
	private String ccNo;
	private String custId;
	private String mobileNo;	
	
	public CCAHostRequestBean(String ccNo, String custId, String mobileNo) {
		super();
		this.ccNo = ccNo;
		this.custId = custId;
		this.mobileNo = mobileNo;
	}
	
	public CCAHostRequestBean(String msgStr) {
		this.msgStr = msgStr;
	}

	public String formatMsg(boolean maskCC){
		StringBuffer sb = new StringBuffer();
		sb.append(StringUtils.rightPad(StringUtils.isEmpty(default1)? DEFAULT_1 : default1, DEFAULT_1_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(default2)? DEFAULT_2 : default2, DEFAULT_2_LENGTH, " "))
		.append(StringUtils.leftPad(StringUtils.isEmpty(default3)? DEFAULT_3 : default3, DEFAULT_3_LENGTH, "0"))
		.append(StringUtils.rightPad(StringUtils.isEmpty(default4)? AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.CC_ACT_HOST_MSG_DEFAULT_4) : default4, DEFAULT_4_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(default5)? DEFAULT_5 : default5, DEFAULT_5_LENGTH, " "));
		
		if(maskCC){
			sb.append(StringUtils.leftPad(maskCCNo(), CC_NO_LENGTH, "0"));			
		}else{			
			sb.append(StringUtils.leftPad(ccNo, CC_NO_LENGTH, "0"));
		}
		
		sb.append(StringUtils.rightPad(custId, CUST_ID_LENGTH, " "))
		.append(StringUtils.rightPad(mobileNo, MOBILE_NO_LENGTH, " "));		
		this.msgStr = sb.toString();
		return this.msgStr;
	}
	
//	public String formatLogMsg(){		
//		StringBuffer sb = new StringBuffer();
//		sb.append(StringUtils.rightPad(StringUtils.isEmpty(default1)? DEFAULT_1 : default1, DEFAULT_1_LENGTH, " "))
//		.append(StringUtils.rightPad(StringUtils.isEmpty(default2)? DEFAULT_2 : default2, DEFAULT_2_LENGTH, " "))
//		.append(StringUtils.leftPad(StringUtils.isEmpty(default3)? DEFAULT_3 : default3, DEFAULT_3_LENGTH, "0"))
//		.append(StringUtils.rightPad(StringUtils.isEmpty(default4)? DEFAULT_4 : default4, DEFAULT_4_LENGTH, " "))
//		.append(StringUtils.rightPad(StringUtils.isEmpty(default5)? DEFAULT_5 : default5, DEFAULT_5_LENGTH, " "))		
//		.append(StringUtils.leftPad(maskCCNo(), CC_NO_LENGTH, "0"))
//		.append(StringUtils.rightPad(custId, CUST_ID_LENGTH, " "))
//		.append(StringUtils.rightPad(mobileNo, MOBILE_NO_LENGTH, " "));		
//		this.msgStr = sb.toString();
//		return this.msgStr;
//	}
	
	private String maskCCNo(){
		return CCActivationUtil.maskCCNumber(this.ccNo);
	}
	
	//For host simulator to parse request str into bean
	public CCAHostRequestBean parseMsg(String msgStr){
		this.msgStr = msgStr;
		this.default1 = getField(DEFAULT_1_LENGTH);
		this.default2 = getField(DEFAULT_2_LENGTH);
		this.default3 = getField(DEFAULT_3_LENGTH);
		this.default4 = getField(DEFAULT_4_LENGTH);
		this.default5 = getField(DEFAULT_5_LENGTH);
		this.ccNo = getField(CC_NO_LENGTH);
		this.custId = getField(CUST_ID_LENGTH);
		this.mobileNo = getField(MOBILE_NO_LENGTH);
		return this;
	}
		
	public String getDefault1() {
		return default1;
	}

	public void setDefault1(String default1) {
		this.default1 = default1;
	}

	public String getDefault2() {
		return default2;
	}

	public void setDefault2(String default2) {
		this.default2 = default2;
	}

	public String getDefault3() {
		return default3;
	}

	public void setDefault3(String default3) {
		this.default3 = default3;
	}

	public String getDefault4() {
		return default4;
	}

	public void setDefault4(String default4) {
		this.default4 = default4;
	}

	public String getDefault5() {
		return default5;
	}

	public void setDefault5(String default5) {
		this.default5 = default5;
	}

	public String getCcNo() {
		return ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMsgStr() {
		return msgStr;
	}

	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}
	
	
}
