package com.bcb.sgwcore.creditcard;

import org.apache.commons.lang.StringUtils;

import com.bcb.sgwcore.BaseStringMessageParser;

public class CCAHostResponseBean extends BaseStringMessageParser {
	public static final String RESULT_CODE_SUCCESS = "00"; //other than this considered failed
	public static final String RETURN_CODE_YES = "Y";
	public static final String RETURN_CODE_NO = "N";	
	public static final String DEFAULT_ONE = "N";
	public static final String DEFAULT_TWO = "000";
	
	public static final int RESULT_CODE_LENGTH = 2;
	public static final int DEFAULT_ONE_LENGTH = 1;
	public static final int DEFAULT_TWO_LENGTH = 3;
	public static final int CARD_NO_LENGTH = 16;
	public static final int CUST_ID_LENGTH = 26;
	public static final int MOBILE_NO_LENGTH = 18;
	public static final int ANSWER_LENGTH = 1;
	
	
	private String resultCode;
	private String defaultOne; // default to 'N'
	private String defaultTwo; // default to '000'
	private String cardNo;
	private String customerId;
	private String mobileNo;
	//overall result Y-All answer correct; N-One or more answer is incorrect
	private String allMatch;
	private String cardExists;
	private String validBlockCode;
	private String craExist;
	private String idMatch;
	private String crExist;
	private String mobileNoMatch;
		 
	
	public CCAHostResponseBean parseStr(String msgStr){
		this.msgStr = msgStr;
		this.resultCode = getField(RESULT_CODE_LENGTH);
		this.defaultOne = getField(DEFAULT_ONE_LENGTH);
		this.defaultTwo = getField(DEFAULT_TWO_LENGTH);
		this.cardNo = getField(CARD_NO_LENGTH);
		this.customerId = getField(CUST_ID_LENGTH);
		this.mobileNo = getField(MOBILE_NO_LENGTH);		
		this.allMatch =getField(ANSWER_LENGTH);
		this.cardExists = getField(ANSWER_LENGTH);
		this.validBlockCode = getField(ANSWER_LENGTH);
		this.craExist = getField(ANSWER_LENGTH);
		this.idMatch = getField(ANSWER_LENGTH);
		this.crExist = getField(ANSWER_LENGTH);
		this.mobileNoMatch = getField(ANSWER_LENGTH);	
		return this;
	}
	
	//to return host response but card number masked
	public String formatLogMsgFromHost(){
		StringBuffer msgLog = new StringBuffer();
		msgLog.append(resultCode).append(defaultOne).append(defaultTwo);
		if(StringUtils.isEmpty(this.cardNo)){
			//abnormal response as per msg spec
			return msgLog.toString();
		}else{
			msgLog.append(CCActivationUtil.maskCCNumber(cardNo)).append(customerId).append(mobileNo).append(allMatch).append(cardExists).append(validBlockCode)
			.append(craExist).append(idMatch).append(crExist).append(mobileNoMatch);
			return msgLog.toString();					
		}
	}
	
	//used by host simulator to construct response msg to SMSG
	public String formatMsg(){
		StringBuffer sb = new StringBuffer();
		sb.append(StringUtils.rightPad(StringUtils.isEmpty(resultCode)? RESULT_CODE_SUCCESS : resultCode, RESULT_CODE_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(defaultOne)? DEFAULT_ONE : defaultOne, DEFAULT_ONE_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(defaultTwo)? DEFAULT_TWO : defaultTwo, DEFAULT_TWO_LENGTH, " "));
		
		if(resultCode == null){
			sb.append("*EOM*");
			this.msgStr = sb.toString();
			return this.msgStr;			
		}
		
		sb.append(StringUtils.leftPad(StringUtils.isEmpty(cardNo)? "" : cardNo, CARD_NO_LENGTH, "0"))
		.append(StringUtils.rightPad(StringUtils.isEmpty(customerId)? "" : customerId, CUST_ID_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(mobileNo)? "" : mobileNo, MOBILE_NO_LENGTH, " "))		
		.append(StringUtils.rightPad(StringUtils.isEmpty(allMatch)? "" : allMatch, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(cardExists)? "" : cardExists, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(validBlockCode)? "" : validBlockCode, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(craExist)? "" : craExist, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(idMatch)? "" : idMatch, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(crExist)? "" : crExist, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(mobileNoMatch)? "" : mobileNoMatch, ANSWER_LENGTH, " "));
		this.msgStr = sb.toString();
		return this.msgStr;
	}
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getDefaultOne() {
		return defaultOne;
	}
	public void setDefaultOne(String defaultOne) {
		this.defaultOne = defaultOne;
	}
	public String getDefaultTwo() {
		return defaultTwo;
	}
	public void setDefaultTwo(String defaultTwo) {
		this.defaultTwo = defaultTwo;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getAllMatch() {
		return allMatch;
	}
	public void setAllMatch(String allMatch) {
		this.allMatch = allMatch;
	}
	public String getCardExists() {
		return cardExists;
	}
	public void setCardExists(String cardExists) {
		this.cardExists = cardExists;
	}
	public String getValidBlockCode() {
		return validBlockCode;
	}
	public void setValidBlockCode(String validBlockCode) {
		this.validBlockCode = validBlockCode;
	}
	public String getCraExist() {
		return craExist;
	}
	public void setCraExist(String craExist) {
		this.craExist = craExist;
	}
	public String getIdMatch() {
		return idMatch;
	}
	public void setIdMatch(String idMatch) {
		this.idMatch = idMatch;
	}
	public String getCrExist() {
		return crExist;
	}
	public void setCrExist(String crExist) {
		this.crExist = crExist;
	}
	public String getMobileNoMatch() {
		return mobileNoMatch;
	}
	public void setMobileNoMatch(String mobileNoMatch) {
		this.mobileNoMatch = mobileNoMatch;
	}
	
	
}
