package com.bcb.sgwcore.creditcard;

import java.io.ByteArrayOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CCAHostSimulator implements Runnable{

	private static final Log log = LogFactory.getLog(CCAHostSimulator.class);
	private Socket clientSocket;
	private String responseMsg;
	
	public static void main(String[] args) {
		try {
			CCAHostSimulator server = new CCAHostSimulator();
			if(args.length>0){
				server.responseMsg = args[0];					
			}
			server.startup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void startup() throws Exception{
		SmsCfg smsCfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		ServerSocket serverSocket = new ServerSocket(smsCfg.getCcaServerPort());
		while(true){
			new Thread(new CCAHostSimulator(serverSocket.accept(), this.responseMsg)).start();			
		}
	}

	private CCAHostSimulator(){};
	
	private CCAHostSimulator(Socket clientSocket, String responseMsg){
		this.responseMsg = responseMsg;
		this.clientSocket = clientSocket;
		log.info("New Client, IP:" + this.clientSocket.getInetAddress().getHostAddress() + " Port:" + clientSocket.getPort());
	}
	
	@Override
	public void run() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		CCAHostResponseBean responseBean = null;
		try{
			byte[] readBuf = new byte[1024];			
			int readCount = 0;
			while( baos.size() < 126 && (readCount = clientSocket.getInputStream().read(readBuf, 0, readBuf.length)) != -1){
				baos.write(readBuf,0, readCount);
			}	
			baos.flush();
			baos.close();			
			log.info("Request: '" + baos.toString() + "'");
			//parse request
			CCAHostRequestBean reqBean = new CCAHostRequestBean(baos.toString());
			reqBean.parseMsg(baos.toString());
			if(StringUtils.isEmpty(this.responseMsg)){
				responseBean = new CCAHostResponseBean();
				responseBean.setCardNo(reqBean.getCcNo());
				responseBean.setCustomerId(reqBean.getCustId());
				responseBean.setMobileNo(reqBean.getMobileNo());
				responseBean.setCardExists(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setIdMatch(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setMobileNoMatch(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setResultCode(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setAllMatch(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setCraExist(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setValidBlockCode(CCAHostResponseBean.RETURN_CODE_NO);
				responseBean.setCrExist(CCAHostResponseBean.RETURN_CODE_NO);
				
				CreditCardActivation result = DaoBeanFactory.getCCActivationDao().find(reqBean.getCcNo());
				if(result != null){
					responseBean.setCardExists(CCAHostResponseBean.RETURN_CODE_YES);
					if(!StringUtils.isEmpty(reqBean.getCustId()) && reqBean.getCustId().trim().equalsIgnoreCase(result.getCustomerId())){
						responseBean.setIdMatch(CCAHostResponseBean.RETURN_CODE_YES);
					}
					if(!StringUtils.isEmpty(reqBean.getMobileNo()) && reqBean.getMobileNo().trim().equalsIgnoreCase(result.getMobileNo())){
						responseBean.setMobileNoMatch(CCAHostResponseBean.RETURN_CODE_YES);
					}				
				}
				
				if(responseBean.getCardExists().equals(CCAHostResponseBean.RETURN_CODE_YES) 
						&& responseBean.getIdMatch().equals(CCAHostResponseBean.RETURN_CODE_YES)
						&& responseBean.getMobileNoMatch().equals(CCAHostResponseBean.RETURN_CODE_YES)
				){
					log.info("Successful activated");
					responseBean.setResultCode(CCAHostResponseBean.RESULT_CODE_SUCCESS);
	//				responseBean.setResultCode(null);
					responseBean.setAllMatch(CCAHostResponseBean.RETURN_CODE_YES);
					responseBean.setCraExist(CCAHostResponseBean.RETURN_CODE_YES);
					responseBean.setValidBlockCode(CCAHostResponseBean.RETURN_CODE_YES);
					responseBean.setCrExist(CCAHostResponseBean.RETURN_CODE_YES);				
				}
			}
		}catch(Exception e){
			log.error("Error processing client activation request. Reason: " + e.getMessage(), e);
			responseBean = new CCAHostResponseBean();
			responseBean.setResultCode(null);
		}finally{
			try{
				String responseStr = null;
				if(responseBean != null){
					 responseStr = responseBean.formatMsg();					
				}else{
					responseStr = this.responseMsg;
				}
				log.info("Response Str: '" + responseStr + "'");
				clientSocket.getOutputStream().write(responseStr.getBytes());
			}catch(Exception e){
				log.error("Error sending back response to client");
			}
			try{
				clientSocket.close();
			}catch(Exception e){
				log.error("Error closing socket");
			}
		}
	}
}
