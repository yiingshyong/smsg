package com.bcb.sgwcore.creditcard;

import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.sgwcore.util.DaoBeanFactory;


public class CCASMSGResponse {
	
	private String activationMessage;
	private String responseSMS;
	private String result;
	private boolean incrementAttemptCount;
//	public static String ACTIVATION_RESULT_SUCCESS = "S";
//	public static String ACTIVATION_RESULT_FAILED = "F";
//	public static String ACTIVATION_RESULT_NA = "N";
	
//	public static final String ACTIVATION_MSG_SUCCESS = "Card Successfully Activated";
//	public static final String ACTIVATION_MSG_CARD_NOT_EXIST = "CL: Card Not Exists";
//	public static final String ACTIVATION_MSG_CRA_NOT_EXIST = "CL: CRA Not Exists";
//	public static final String ACTIVATION_MSG_MOBILE_NO_UNMATCHED = "CL: Mobile No Unmatch";
//	public static final String ACTIVATION_MSG_ID_UNMATCHED = "CL: IC Unmatch";
//	public static final String ACTIVATION_MSG_INVALID_BLOCK_CODE = "CL: Invalid Block Code";
//	public static final String ACTIVATION_MSG_CR_NOT_EXIST = "CL: CR Not Exists";
//	public static final String FIRST_LVL_CHECK_MSG_CARD_NO_NOT_FOUND = "SMS: Card No Not Found";
//	public static final String FIRST_LVL_CHECK_MSG_CUSTOMER_NO_UNMATCH = "SMS: Customer No Unmatch";
//	public static final String FIRST_LVL_CHECK_MSG_MOBILE_NO_UNMATCH = "SMS: Mobile No Unmatch";
//	public static final String FIRST_LVL_CHECK_MSG_EXCEEDED_FAILURE_ATTEMPT = "SMS: Exceeded Invalid Attempt Threshold";
//	public static final String FIRST_LVL_CHECK_MSG_SERVER_RESPONSE_TIMEOUT = "SMS: CardLink Response Timeout";
//	public static final String FIRST_LVL_CHECK_MSG_SERVER_CONNECTION_FAILED = "SMS: Connection Failed";
//	public static final String FIRST_LVL_CHECK_MSG_EXCEEDED_SVC_WINDOW = "SMS: Exceeded Card Activation Time Frame";
	
//	public static CCASMSGResponse SUCCESS_RESPONSE = getSuccessResponse("Card Successfully Activated", false);
//	public static CCASMSGResponse NA_RESPONSE = getUnavailableResponse("SMS: Exceeded Card Activation Time Frame", false);
//	public static CCASMSGResponse FAILURE_HOST_CARD_NOT_EXIST = getFailureResponse("CL: Card Not Exists", false);
//	public static CCASMSGResponse FAILURE_HOST_CRA_NOT_EXIST = getFailureResponse("CL: CRA Not Exists", true);
//	public static CCASMSGResponse FAILURE_HOST_MOBILE_UNMATCH = getFailureResponse("CL: Mobile No Unmatch", true);
//	public static CCASMSGResponse FAILURE_HOST_CUST_ID_UNMATCH = getFailureResponse("CL: Customer No Unmatch", true);
//	public static CCASMSGResponse FAILURE_HOST_INVALID_BLOCK_CODE = getFailureResponse("CL: Invalid Block Code", true);
//	public static CCASMSGResponse FAILURE_HOST_CR_NOT_EXIST = getFailureResponse("CL: CR Not Exists", true);
//	public static CCASMSGResponse FAILURE_HOST_UNDEFINED_ERROR = getFailureResponse("CL: Undefined Error", false);
//	public static CCASMSGResponse FAILURE_HOST_NON_ALL_MATCH = getFailureResponse("CL: Non All Matched", false);
//	public static CCASMSGResponse FAILURE_HOST_RESULT_CODE_ERROR = getFailureResponse("CL: Error Result Code ", false);
//	public static CCASMSGResponse FAILURE_HOST_RESULT_CODE_EMPTY = getFailureResponse("CL: Result Code Empty", false);
//	public static CCASMSGResponse FAILURE_FIRST_LVL_CARD_NOT_EXIST = getFailureResponse("SMS: Card No Not Exists", false);
//	public static CCASMSGResponse FAILURE_FIRST_LVL_EXCEEDED_FAILURE_ATTEMPT = getFailureResponse("SMS: Exceeded Invalid Attempt Threshold", true);
//	public static CCASMSGResponse FAILURE_FIRST_LVL_EXCEEDED_SVC_WINDOW = getFailureResponse("SMS: Exceeded Card Activation Time Frame", false);
//	public static CCASMSGResponse FAILURE_FIRST_LVL_INVALID_INPUT_FORMAT = getFailureResponse("SMS: Missing input or invalid input format", false);
//	public static CCASMSGResponse FAILURE_FIRST_LVL_ALREADY_ACTIVATED = getFailureResponse("SMS: Already Been Activated", false, CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED); // TODO: Clarify
//	public static CCASMSGResponse FAILURE_SMSG_UNEXPECTED_ERROR = getFailureResponse("SMS: Unexpected Error", false);
//	public static CCASMSGResponse FAILURE_SMSG_HOST_COMM_ERROR = getFailureResponse("SMS: SMSG - Host Communication Error", false);
//	public static CCASMSGResponse FAILURE_SMSG_HOST_CONNECTION_FAILED = getFailureResponse("SMS: Connection Failed ", false);
//	public static CCASMSGResponse FAILURE_SMSG_HOST_RESPONSE_TIMEOUT = getFailureResponse("SMS: CardLink Response Timeout", false);
		
	public static CCASMSGResponse getFailureFirstLvlMobileUnmatch(String mobileNoCust, String mobileNoSys){
		CCASMSGResponse response = new CCASMSGResponse();
		response.setResult(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED);
		response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaFailureSms());
		response.setActivationMessage("SMS: Mobile No Unmatch " + mobileNoSys + " (Sys) " + mobileNoCust + " (sms) ");
		response.setIncrementAttemptCount(true);
		return response;		
	}
	
	public static CCASMSGResponse getFailureFirstLvCustIdUnmatch(String custIdCust, String custIdSys){
		CCASMSGResponse response = new CCASMSGResponse();
		response.setResult(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED);
		response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaFailureSms());
		response.setActivationMessage("SMS: Customer No Unmatch " + custIdSys + " (Sys) " + custIdCust + " (sms) ");
		response.setIncrementAttemptCount(true);
		return response;		
	}
	
	public static CCASMSGResponse getFailureResponse(String activationMsg, boolean incrementAttemptCount){
		return getFailureResponse(activationMsg, incrementAttemptCount, null, null);
	}
		
	public static CCASMSGResponse getHostFailureResponse(String activationMsg, boolean incrementAttemptCount){
		return getFailureResponse(activationMsg, incrementAttemptCount, null, DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaHostUnavailabilitySms());
	}
	
	public static CCASMSGResponse getFailureResponse(String activationMsg, boolean incrementAttemptCount, String result, String smsReply){
		CCASMSGResponse response = new CCASMSGResponse();
		if(result == null){
			response.setResult(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED);
		}else{
			response.setResult(result);
		}
		if(smsReply == null ){
			response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaFailureSms());			
		}else{
			response.setResponseSMS(smsReply);
		}
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	public static CCASMSGResponse getSuccessResponse(String activationMsg, boolean incrementAttemptCount){
		CCASMSGResponse response = new CCASMSGResponse();
		response.setResult(CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED);
		response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaSuccessSms());
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	public static CCASMSGResponse getUnavailableResponse(String activationMsg, boolean incrementAttemptCount){
		CCASMSGResponse response = new CCASMSGResponse();
		response.setResult(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED);
		response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaUnavailabilitySms());
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	private CCASMSGResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getActivationMessage() {
		return activationMessage;
	}
	public void setActivationMessage(String activationMessage) {
		this.activationMessage = activationMessage;
	}
	public String getResponseSMS() {
		return responseSMS;
	}
	public void setResponseSMS(String responseSMS) {
		this.responseSMS = responseSMS;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	public boolean isIncrementAttemptCount() {
		return incrementAttemptCount;
	}

	public void setIncrementAttemptCount(boolean incrementAttemptCount) {
		this.incrementAttemptCount = incrementAttemptCount;
	}
	
	
}
