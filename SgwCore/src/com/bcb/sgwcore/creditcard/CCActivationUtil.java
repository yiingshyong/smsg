package com.bcb.sgwcore.creditcard;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.util.Constants;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CCActivationUtil {
		
	private static final Log log = LogFactory.getLog(CCActivationUtil.class);
	
	public static CCASMSGResponse activateCardViaSocket(String mobileNo, String ccNo, String customerId)throws Exception {
		long startTime = System.currentTimeMillis();
		CCAHostRequestBean reqBean = new CCAHostRequestBean(ccNo, customerId, mobileNo);	
		SmsCfg smsCfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		Socket socket = null;
		ByteArrayOutputStream responseBaos = new ByteArrayOutputStream();
		try{
			socket = new Socket(smsCfg.getCcaServerIP(), smsCfg.getCcaServerPort());
			socket.setKeepAlive(true);
			socket.setSoTimeout(smsCfg.getCcaServerResponseTimeout() * 1000);
			
			String reqMsg = reqBean.formatMsg(false); log.error(">>reqMsg:"+reqMsg+"<<");
			socket.getOutputStream().write(reqMsg.getBytes());
			socket.getOutputStream().flush();
			
			byte[] readBuf = new byte[1024];
			int readLength = 0;
			while( (readLength = socket.getInputStream().read(readBuf, 0, readBuf.length )) != -1){
				responseBaos.write(readBuf,0, readLength);
			}		
			responseBaos.flush();
		//	log.error("Response: '" + responseBaos.toString() + "'");			
		}catch(SocketTimeoutException ste){
			return CCASMSGResponse.getHostFailureResponse("SMS: CardLink Response Timeout", false);
		}catch(SocketException e){
			return CCASMSGResponse.getHostFailureResponse("SMS: Connection Failed ", false);
		}catch(IOException ioe){
			return CCASMSGResponse.getHostFailureResponse("SMS: SMSG - Host Communication Error", false);
		}finally{
			if(socket != null){
				try{
					socket.close();
				}catch(Exception e){
					log.error("Error closing socket. Reason: " + e.getMessage(), e);
				}
			}
		}
		long timeTaken = System.currentTimeMillis() - startTime;
		StringBuilder logMsg = new StringBuilder();
		
		CCASMSGResponse smsgResponse = null;
		//parse response
		try{
			logMsg.append("'").append(reqBean.formatMsg(true)).append("' '");
			CCAHostResponseBean responseBean = null;
			boolean parseError = false;
			try{
				responseBean = new CCAHostResponseBean().parseStr(responseBaos.toString()); log.error(">>responseBean:"+responseBean+"<<");
				log.error(">>responseBean.resultcode():"+responseBean.getResultCode()+"<<");
			}catch(Exception e){
				parseError = true;
				log.error("Error while parsing host response -> '" + responseBaos.toString() + "'");
				logMsg.append(responseBaos.toString() + "' '");				
				smsgResponse = CCASMSGResponse.getHostFailureResponse("CL: Undefined Error", false);
				smsgResponse.setActivationMessage(smsgResponse.getActivationMessage() + Constants.LINE_SEPARATOR + ": '" + responseBaos.toString() + "'");
			}
			if(!parseError){
				logMsg.append(responseBean.formatLogMsgFromHost()).append("' '");				
				if(!StringUtils.isBlank(responseBean.getResultCode())){						
						if(!responseBean.getResultCode().equals(CCAHostResponseBean.RESULT_CODE_SUCCESS)){
							smsgResponse = CCASMSGResponse.getHostFailureResponse("CL: Error Result Code - '" + responseBean.getResultCode() + "'", false);						
						}else if(responseBean.getCardExists().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: Card Not Exists", true);
						}else if(responseBean.getValidBlockCode().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: Invalid Block Code", true);		
						}else if(responseBean.getCraExist().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: CRA Not Exists", true);					
						}else if(responseBean.getCrExist().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: CR Not Exists", true);					
						}else if(responseBean.getIdMatch().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: Customer No Unmatch", true);					
						}else if(responseBean.getMobileNoMatch().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: Mobile No Unmatch", true);					
						}else if(responseBean.getAllMatch().equalsIgnoreCase(CCAHostResponseBean.RETURN_CODE_NO)){
							smsgResponse = CCASMSGResponse.getFailureResponse("CL: Overall Result Failed", true);
						}else{
							smsgResponse = CCASMSGResponse.getSuccessResponse("Card Successfully Activated", false);						
						}
				}else{
					smsgResponse = CCASMSGResponse.getHostFailureResponse("CL: Result Code Empty", false);
				}
			}
		}catch(Exception e){
			log.error("Unexpected error caught. Reason: " + e.getMessage(), e);
			throw e;
		}finally{
			logMsg.append(smsgResponse == null ? "-" : smsgResponse.getActivationMessage()).append("' ");
			logMsg.append(timeTaken);
			log.info(logMsg.toString());			
		}
		return smsgResponse;
	}
	
	@SuppressWarnings("unchecked")
	public static String maskCCNumber(String ccNumber){
		if(StringUtils.isEmpty(ccNumber)){
			return ccNumber;
		}
		List<String> maskingPos = new ArrayList<String>(); 
		char maskingChar = '*';
		try{
			maskingPos = AppPropertiesConfig.getInstance().getSettings().getList(AppPropertiesConfig.CC_MASKING_POS);
			maskingChar = AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.CC_MASKING_CHAR).charAt(0);
		}catch(Exception e){
			log.error("Error while getting masking setting, masking might be failed. Reason: " + e.getMessage(), e);
		}
		char[] nos = ccNumber.toCharArray();
		for(int i=0; i<nos.length; i++){
			if(maskingPos.contains(String.valueOf(i+1))){
				nos[i] = maskingChar;
			}
		}
		return String.valueOf(nos);
	}
}
