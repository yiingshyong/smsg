package com.bcb.sgwcore.creditcard;

import com.bcb.sgwcore.util.DaoBeanFactory;

public class OCMDebitSMSGResponse {
	
	private String activationMessage;
	private String responseSMS;
	private String result;
	private boolean incrementAttemptCount;

	public static final String OPTIN_STATUS_SUCCEED = "Y";
	public static final String OPTIN_STATUS_FAILED = "F";
	public static final String OPTIN_UNEXPECTED_ERROR = "P";
	
	public static OCMDebitSMSGResponse getFailureResponse(String activationMsg, boolean incrementAttemptCount){
		return getFailureResponse(activationMsg, incrementAttemptCount, OPTIN_UNEXPECTED_ERROR, null,null);
	}
		
	public static OCMDebitSMSGResponse getHostFailureResponse(String activationMsg, boolean incrementAttemptCount, String key){
		return getFailureResponse(activationMsg, incrementAttemptCount, null,DaoBeanFactory.getAutoActionDao().findOCMActions(key).getUnavailabilitySms(), key);
	}
	
	public static OCMDebitSMSGResponse getFailureResponse(String activationMsg, boolean incrementAttemptCount, String result, String smsReply, String key){
		OCMDebitSMSGResponse response = new OCMDebitSMSGResponse();
		if(result == null){
			response.setResult(OPTIN_STATUS_FAILED);
		}else{
			response.setResult(result);
		}
		if(smsReply == null ){
			response.setResponseSMS(DaoBeanFactory.getAutoActionDao().findOCMActions("OCM").getUnavailabilitySms());			
		}else{
			response.setResponseSMS(smsReply);
		}
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	public static OCMDebitSMSGResponse getSuccessResponse(String activationMsg, boolean incrementAttemptCount){
		OCMDebitSMSGResponse response = new OCMDebitSMSGResponse();
		response.setResult(OPTIN_STATUS_SUCCEED);
		response.setResponseSMS(DaoBeanFactory.getSmsCfgDao().findSmsCfg().getCcaSuccessSms());
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	public static OCMDebitSMSGResponse getUnavailableResponse(String activationMsg, boolean incrementAttemptCount, String key){
		OCMDebitSMSGResponse response = new OCMDebitSMSGResponse();
		response.setResult(OPTIN_STATUS_FAILED);
		response.setResponseSMS(DaoBeanFactory.getAutoActionDao().findOCMActions(key).getUnavailabilitySms());
		response.setActivationMessage(activationMsg);
		response.setIncrementAttemptCount(incrementAttemptCount);
		return response;
	}
	
	private OCMDebitSMSGResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getActivationMessage() {
		return activationMessage;
	}
	public void setActivationMessage(String activationMessage) {
		this.activationMessage = activationMessage;
	}
	public String getResponseSMS() {
		return responseSMS;
	}
	public void setResponseSMS(String responseSMS) {
		this.responseSMS = responseSMS;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	public boolean isIncrementAttemptCount() {
		return incrementAttemptCount;
	}

	public void setIncrementAttemptCount(boolean incrementAttemptCount) {
		this.incrementAttemptCount = incrementAttemptCount;
	}
	
	
}
