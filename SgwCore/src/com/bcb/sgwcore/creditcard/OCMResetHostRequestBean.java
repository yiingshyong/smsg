package com.bcb.sgwcore.creditcard;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import com.bcb.sgwcore.BaseStringMessageParser;

public class OCMResetHostRequestBean extends BaseStringMessageParser{
	
	public static final String DEFAULT_1 = "TX5B";
	//public static final String DEFAULT_1 = "0200623B048108A10000";
	public static final String DEFAULT_2 = "00B4";
	public static final String DEFAULT_3 = "0200";
	public static final String DEFAULT_4 = "623B048108A10000";
	public static final String DEFAULT_5 = "190000000000000000000";
	public static final String DEFAULT_6 = "630002"; // Process Code to configure here
	
	
	public static final String DEFAULT_7 = "0603101327";//MMDDhhmmss
	public static final String DEFAULT_8 = "533509";
	public static final String DEFAULT_9 = "101327";//transaction time Hhmmss
	public static final String DEFAULT_10 = "0603";//transaction date MMDD 
	public static final String DEFAULT_11 = "0000";
	public static final String DEFAULT_12 = "0000";
	public static final String DEFAULT_13 = "000";
	public static final String DEFAULT_14 = "00";
	public static final String DEFAULT_15 = "06501855";
	public static final String DEFAULT_16 = "000000533509";
	
	public static final String DEFAULT_17 = StringUtils.rightPad("SMSGT00", 8," ");
	public static final String DEFAULT_18 = StringUtils.rightPad("SMSG", 18," ")+StringUtils.rightPad("KUALALUMPUR", 15," ")+StringUtils.rightPad(" ", 5," ")+"MY";
	public static final String DEFAULT_19 = "023SMSG";
	
	public static final int DEFAULT_1_LENGTH = 4;
	public static final int DEFAULT_2_LENGTH = 4;	
	public static final int DEFAULT_3_LENGTH = 4;	
	public static final int DEFAULT_4_LENGTH = 16;	
	public static final int DEFAULT_5_LENGTH = 6;	
	public static final int DEFAULT_6_LENGTH = 10;	
	public static final int DEFAULT_7_LENGTH = 6;	
	public static final int DEFAULT_8_LENGTH = 6;	
	public static final int DEFAULT_9_LENGTH = 4;	
	public static final int DEFAULT_10_LENGTH = 8;	
	public static final int DEFAULT_11_LENGTH = 8;	
	public static final int DEFAULT_12_LENGTH = 40;	
	public static final int DEFAULT_13_LENGTH = 26;	
	
	public static final int CC_NO_LENGTH = 4;		
	public static final int MOBILE_NO_LENGTH = 15;	

	private String default1;
	private String default2;
	private String default3;
	private String default4;
	private String default5;
	private String ccNo;
	private String custId;
	private String mobileNo;	
	
	public OCMResetHostRequestBean(String ccNo, String mobileNo) {
		super();
		this.ccNo = ccNo;
		this.mobileNo = mobileNo;
	}
	
	public OCMResetHostRequestBean(String msgStr) {
		this.msgStr = msgStr;
	}

	public String formatMsg(){
		
		SimpleDateFormat trans_formatter = new SimpleDateFormat("MMddHHmmss");
		SimpleDateFormat transTime_formatter = new SimpleDateFormat("HHmmss");
		SimpleDateFormat transDate_formatter = new SimpleDateFormat("MMdd");
		
		StringBuffer sb = new StringBuffer();
		//sb.append(DEFAULT_1)
		//sb.append(DEFAULT_2)
		sb.append(DEFAULT_3)
		.append(DEFAULT_4)
		
		
		.append(DEFAULT_5)
		.append(DEFAULT_6)
		.append(trans_formatter.format(new Date()))//datetime
		.append(DEFAULT_8)
		.append(transTime_formatter.format(new Date()))//time
		.append(transDate_formatter.format(new Date()))//date
		.append(DEFAULT_11)
		.append(DEFAULT_12)
		.append(DEFAULT_13)
		.append(DEFAULT_14)
		.append(DEFAULT_15)
		.append(DEFAULT_16)
		.append(DEFAULT_17)
		.append(DEFAULT_18)
		.append(DEFAULT_19);

		sb.append(StringUtils.leftPad(ccNo, CC_NO_LENGTH, "0"));
		sb.append(StringUtils.rightPad(mobileNo, MOBILE_NO_LENGTH, " "));	
		this.msgStr = sb.toString();
		return this.msgStr;
	}

	
	//For host simulator to parse request str into bean
	public OCMResetHostRequestBean parseMsg(String msgStr){
		this.msgStr = msgStr;
		this.default1 = getField(DEFAULT_1_LENGTH);
		this.default2 = getField(DEFAULT_2_LENGTH);
		this.default3 = getField(DEFAULT_3_LENGTH);
		this.default4 = getField(DEFAULT_4_LENGTH);
		this.default5 = getField(DEFAULT_5_LENGTH);
		this.ccNo = getField(CC_NO_LENGTH);
		this.mobileNo = getField(MOBILE_NO_LENGTH);
		return this;
	}
		
	public String getDefault1() {
		return default1;
	}

	public void setDefault1(String default1) {
		this.default1 = default1;
	}

	public String getDefault2() {
		return default2;
	}

	public void setDefault2(String default2) {
		this.default2 = default2;
	}

	public String getDefault3() {
		return default3;
	}

	public void setDefault3(String default3) {
		this.default3 = default3;
	}

	public String getDefault4() {
		return default4;
	}

	public void setDefault4(String default4) {
		this.default4 = default4;
	}

	public String getDefault5() {
		return default5;
	}

	public void setDefault5(String default5) {
		this.default5 = default5;
	}

	public String getCcNo() {
		return ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMsgStr() {
		return msgStr;
	}

	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}
	
	
}
