package com.bcb.sgwcore.creditcard;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.bcb.sgwcore.BaseStringMessageParserOCM;

public class OCMResetHostResponseBean extends BaseStringMessageParserOCM {
	private static final Log log = LogFactory.getLog(OCMResetHostResponseBean.class);
	public static final String RESULT_CODE_SUCCESS = "00"; //other than this considered failed

	public static final String DEFAULT_ONE = "N";
	public static final String DEFAULT_TWO = "000";

	public static final int CARD_NO_LENGTH = 16;
	public static final int MOBILE_NO_LENGTH = 18;
	
	public static final int MSG_LENGTH = 3;
	public static final int MSG_TYPE_LENGTH = 4;
	public static final int PRI_BITMAP_LENGTH = 16;
	public static final int SEC_BITMAP_LENGTH = 16;
	public static final int PRI_ACC_NO_LENGTH = 21;
	public static final int PROCESS_CODE_LENGTH = 6;
	public static final int TRANS_DATETIME_LENGTH = 10;
	public static final int AUDIT_NO_LENGTH = 6;
	
	public static final int SETTLEMENT_DATE_LENGTH = 4;
	public static final int DATE_CONVERSION_LENGTH = 4;
	public static final int INST_ID_LENGTH = 8;
	public static final int REF_NUM_LENGTH = 12;
	public static final int ID_RESPONSE_LENGTH = 6;
	
	public static final int RESULT_CODE_LENGTH = 2;
	public static final int TERMINAL_ID_LENGTH = 8;
	public static final int HOST_REPLY_LENGTH = 150;

	
	
	private String resultCode;
	private String cardNo;
	private String mobileNo;

	
	private String msgLength;
	private String msgType;
	private String primaryBitmap;
	private String secBitmap;
	private String primaryAccNo;
	private String processCode;
	private String transDateTime;
	private String auditNo;
	private String settlementDate;
	private String dateConversion;
	private String instId;
	private String refNum;
	private String idResponse;
	private String terminalId;
	private String hostReply;
		 
	
	public OCMResetHostResponseBean parseStr(String msgStr){
		
		log.info("ResponseString trimmed:"+msgStr.trim());
		this.msgStr = msgStr;
		//this.msgLength = getField(MSG_LENGTH);
		this.msgType = getField(MSG_TYPE_LENGTH);
		log.info("msgType:"+msgType);
		
		this.primaryBitmap = getField(PRI_BITMAP_LENGTH);
		log.info("primaryBitmap:"+primaryBitmap);
		
		this.secBitmap = getField(SEC_BITMAP_LENGTH);
		log.info("secBitmap:"+secBitmap);
		
		this.primaryAccNo = getField(PRI_ACC_NO_LENGTH);
		log.info("primaryAccNo:"+primaryAccNo);
		
		this.processCode = getField(PROCESS_CODE_LENGTH);
		log.info("processCode:"+processCode);
		
		this.transDateTime = getField(TRANS_DATETIME_LENGTH);
		log.info("transDateTime:"+transDateTime);
		
		this.auditNo = getField(AUDIT_NO_LENGTH);
		log.info("auditNo:"+auditNo);
		
		this.settlementDate = getField(SETTLEMENT_DATE_LENGTH);
		log.info("settlementDate:"+settlementDate);
		
		this.dateConversion = getField(DATE_CONVERSION_LENGTH);
		log.info("dateConversion:"+dateConversion);
		
		this.instId = getField(INST_ID_LENGTH);
		log.info("instId:"+instId);
		
		this.refNum = getField(REF_NUM_LENGTH);
		log.info("refNum:"+refNum);
		
		this.idResponse = getField(ID_RESPONSE_LENGTH);
		log.info("idResponse:"+idResponse);
		
		this.resultCode = getField(RESULT_CODE_LENGTH);
		log.info("resultCode:"+resultCode);
		
		this.terminalId = getField(TERMINAL_ID_LENGTH);
		log.info("terminalId:"+terminalId);
		
		//this.hostReply = getField(HOST_REPLY_LENGTH);
		//System.out.println("hostReply:"+hostReply);
		return this;
	}
	
	//to return host response but card number masked - for log
	public String formatLogMsgFromHost(){
		StringBuffer msgLog = new StringBuffer();
		msgLog.append(resultCode).append(hostReply);
		
		return msgLog.toString();					

	}
	
	//used by host simulator to construct response msg to SMSG
	public void formatMsg(){/*
		StringBuffer sb = new StringBuffer();
		sb.append(StringUtils.rightPad(StringUtils.isEmpty(resultCode)? RESULT_CODE_SUCCESS : resultCode, RESULT_CODE_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(defaultOne)? DEFAULT_ONE : defaultOne, DEFAULT_ONE_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(defaultTwo)? DEFAULT_TWO : defaultTwo, DEFAULT_TWO_LENGTH, " "));
		
		if(resultCode == null){
			sb.append("*EOM*");
			this.msgStr = sb.toString();
			return this.msgStr;			
		}
		
		sb.append(StringUtils.leftPad(StringUtils.isEmpty(cardNo)? "" : cardNo, CARD_NO_LENGTH, "0"))
		.append(StringUtils.rightPad(StringUtils.isEmpty(customerId)? "" : customerId, CUST_ID_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(mobileNo)? "" : mobileNo, MOBILE_NO_LENGTH, " "))		
		.append(StringUtils.rightPad(StringUtils.isEmpty(allMatch)? "" : allMatch, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(cardExists)? "" : cardExists, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(validBlockCode)? "" : validBlockCode, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(craExist)? "" : craExist, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(idMatch)? "" : idMatch, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(crExist)? "" : crExist, ANSWER_LENGTH, " "))
		.append(StringUtils.rightPad(StringUtils.isEmpty(mobileNoMatch)? "" : mobileNoMatch, ANSWER_LENGTH, " "));
		this.msgStr = sb.toString();
		return this.msgStr;
	*/}
	
	
	public String getMsgLength() {
		return msgLength;
	}

	public void setMsgLength(String msgLength) {
		this.msgLength = msgLength;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getPrimaryBitmap() {
		return primaryBitmap;
	}

	public void setPrimaryBitmap(String primaryBitmap) {
		this.primaryBitmap = primaryBitmap;
	}

	public String getSecBitmap() {
		return secBitmap;
	}

	public void setSecBitmap(String secBitmap) {
		this.secBitmap = secBitmap;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getTransDateTime() {
		return transDateTime;
	}

	public void setTransDateTime(String transDateTime) {
		this.transDateTime = transDateTime;
	}

	public String getAuditNo() {
		return auditNo;
	}

	public void setAuditNo(String auditNo) {
		this.auditNo = auditNo;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getHostReply() {
		return hostReply;
	}

	public void setHostReply(String hostReply) {
		this.hostReply = hostReply;
	}

	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPrimaryAccNo() {
		return primaryAccNo;
	}

	public void setPrimaryAccNo(String primaryAccNo) {
		this.primaryAccNo = primaryAccNo;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getDateConversion() {
		return dateConversion;
	}

	public void setDateConversion(String dateConversion) {
		this.dateConversion = dateConversion;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getIdResponse() {
		return idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}
	
	
	
}
