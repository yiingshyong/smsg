package com.bcb.sgwcore.creditcard;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.util.Constants;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class OCMResetTempUtil {
		
	private static final Log log = LogFactory.getLog(OCMResetTempUtil.class);

	public static OCMResetSMSGResponse optInCardViaSocket(String mobileNo, String ccNo, Integer attempt)throws Exception {
		long startTime = System.currentTimeMillis();
		OCMResetHostRequestBean reqBean = new OCMResetHostRequestBean(ccNo, mobileNo);	
		String parakey = "OCMReset";//PUT THIS IS PROPERTIES FILE
		AutoAction autoAction = DaoBeanFactory.getAutoActionDao().findOCMActions(parakey);
		Socket socket = null;
		String tempMobile=mobileNo;
		String tempCCno= ccNo;
		Integer maxAttempt = autoAction.getMaxFailureAttempt();
		Integer countAttempt = attempt;
		ByteArrayOutputStream responseBaos = new ByteArrayOutputStream();
		try{
			log.info("current count attempt: " + countAttempt);
			socket = new Socket(autoAction.getServerIP(), autoAction.getServerPort());
			//log.info("Server IP: "+autoAction.getServerIP());
			//log.info("Server Port: "+autoAction.getServerPort());
			//log.info("Socket: "+socket.toString());
			socket.setKeepAlive(true);
			
			socket.setSoTimeout(autoAction.getServerResponseTimeout() * 1000);
	
			String reqMsg = reqBean.formatMsg();
			String reqMsg1 = "TX5B";

			//log.info("Outgoing msg: "+reqMsg);
			//log.info("Outgoing msg Bytes: "+reqMsg.getBytes().toString());

			//convert the header to byte
			byte format = (byte)0x00;
		    byte format2 = (byte)0xb4;

		        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		        outputStream.write(format);
		        outputStream.write(format2);
		        outputStream.write(reqMsg.getBytes());
		        
		        byte c[] = outputStream.toByteArray();
		        //log.info("OCM Bytes: " + byteToString(c));
		    
		    //flush the header 1st
			socket.getOutputStream().write(reqMsg1.getBytes());
			socket.getOutputStream().flush();
			//flush the rest of the byte array
			socket.getOutputStream().write(c);			
			socket.getOutputStream().flush();
		
			byte[] readBuf = new byte[1024];
			int readLength = 0;

			while( (readLength = socket.getInputStream().read(readBuf, 0, readBuf.length )) != -1){
				responseBaos.write(readBuf,0, readLength);
			}
			responseBaos.flush();
		}catch(SocketTimeoutException ste){
			countAttempt+=1;
			if(countAttempt<=maxAttempt){
				log.info("1.Attempting a retry " + countAttempt);
				optInCardViaSocket(tempMobile,tempCCno,countAttempt);
				
			}
			
			return OCMResetSMSGResponse.getHostFailureResponse("SMS: OCM Response Timeout", false,parakey);
		}catch(SocketException e){
			log.error("error socket:"+e.getMessage(), e);
			countAttempt+=1;
			if(countAttempt<=maxAttempt){
				log.info("1.1 Attempting a retry " + countAttempt);
				optInCardViaSocket(tempMobile,tempCCno,countAttempt);
				
			}
			
			return OCMResetSMSGResponse.getHostFailureResponse("SMS: Connection Failed ", false,parakey);
		}catch(IOException ioe){
			countAttempt+=1;
			if(countAttempt<=maxAttempt){
				log.info("1.2 Attempting a retry " + countAttempt);
				optInCardViaSocket(tempMobile,tempCCno,countAttempt);
				
			}
			return OCMResetSMSGResponse.getHostFailureResponse("SMS: SMSG - Host Communication Error", false,parakey);
		}finally{
			if(socket != null){
				try{
					socket.close();
				}catch(Exception e){
					log.error("Error closing socket. Reason: " + e.getMessage(), e);
				}
			}
		}
		long timeTaken = System.currentTimeMillis() - startTime;
		StringBuilder logMsg = new StringBuilder();
	
		OCMResetSMSGResponse smsgResponse = null;
		//parse response
		try{
	
			logMsg.append("'").append(reqBean.formatMsg()).append("' '");
			OCMResetHostResponseBean responseBean = null;
			boolean parseError = false;
			try{
				//log.info("Response>>>>>"+responseBaos.toString());	
			
				responseBean = new OCMResetHostResponseBean().parseStr(responseBaos.toString());
			}catch(Exception e){
				parseError = true;
				log.error("Error while parsing host response -> '" + responseBaos.toString() + "'");
				log.error("Error while parsing host response length -> '" + responseBaos.toString().length() + "'");
				logMsg.append(responseBaos.toString() + "' '");				
				smsgResponse = OCMResetSMSGResponse.getHostFailureResponse("OCM: Undefined Error", false,parakey);
				smsgResponse.setActivationMessage(smsgResponse.getActivationMessage() + Constants.LINE_SEPARATOR + ": '" + responseBaos.toString() + "'");
			}
			if(!parseError){
				logMsg.append(responseBean.formatLogMsgFromHost()).append("' '");	//this for log info only			
				if(!StringUtils.isBlank(responseBean.getResultCode())){						
						if(!responseBean.getResultCode().equals(OCMHostResponseBean.RESULT_CODE_SUCCESS)){
							smsgResponse = OCMResetSMSGResponse.getFailureResponse("OCM: Unsuccessful Reset Temp Pin - Error Result Code - '" + responseBean.getResultCode() + "'", false);						
						}else{
							smsgResponse = OCMResetSMSGResponse.getSuccessResponse("OCM : Reset Temp Pin Successful.", false);						
						}
				}else{
					smsgResponse = OCMResetSMSGResponse.getFailureResponse("OCM: Result Code Empty", false);
				}
			}
		}catch(Exception e){
			log.error("Unexpected error caught. Reason: " + e.getMessage(), e);
			throw e;
		}finally{
			logMsg.append(smsgResponse == null ? "-" : smsgResponse.getActivationMessage()).append("' ");
			logMsg.append(timeTaken);
			
			log.info(logMsg.toString());			
		}
		return smsgResponse;
	}
	
	    public static String byteToString(byte[] b) {
	        StringBuilder sb = new StringBuilder();
	        for (int i = 0; i < b.length; i++) {
	            int curByte = b[i] & 0xFF;
	            sb.append("0x");
	            if (curByte <= 0x0F) {
	                sb.append("0");
	            }
	            sb.append(Integer.toString(curByte, 16));
	            sb.append(" ");
	        }
	        return sb.toString().trim();
	    }

}
