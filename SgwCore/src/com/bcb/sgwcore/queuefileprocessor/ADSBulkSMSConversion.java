package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.IADSSynchDao;
import com.bcb.crsmsg.modal.ADSSynch;
import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class ADSBulkSMSConversion extends DefaultBulkSMSConversion{
	public String process(String line, FtpReport reportModal, String user, BulkSmsSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process conversion of Bulk SMS");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]")); 
			log.debug("data length: "+data.length);
			
			if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
				log.debug("theTemplate: "+theTemplate);
				theTemplate=findTemplateMessage(theTemplate, data);
				log.debug("theTemplate: "+theTemplate);
			}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
				theTemplate=new MsgTemplate();
			}
			
			String returnMsg = validation(data, theTemplate);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
				ArrayList<Integer> smsIdList=new ArrayList<Integer>();
				if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg, smsIdList)){
					if(saveAccountNo(data, smsIdList)){
					}else{
						returnMsg="Error in saving acc no. record";
					}
				}else{
					returnMsg="Error in creating SMS! ";
				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean createSMS(String[] data, String user, FtpReport reportModal, BulkSmsSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, ArrayList<Integer> smsIdList){
		try{
			int charPerSms=CommonUtils.calculateCharPerSms(aSetup.getMsgFormatType(), CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			
			ArrayList<String> msgArray=new ArrayList<String>();
			log.debug("CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString(): "+CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString());
			String message = "";
			if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
				message = generateSMSMessage(data, theTemplate);
			}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
				message = aSetup.getMessage();
			}
			msgArray = CommonUtils.strToArray(message, charPerSms);
			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
				SmsQueue sms = new SmsQueue();
				sms.setCreatedBy(user);
				sms.setCreatedDatetime(new Date());
				sms.setDeptId(reportModal.getDeptId());
				sms.setDept(reportModal.getDept());
				sms.setFtp(reportModal.getId());
				sms.setMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
				sms.setMessage(message);
				sms.setTotalSms(msgArray.size());
				sms.setMsgFormatType(aSetup.getMsgFormatType());
				sms.setPriority(Integer.valueOf(fileConversion.getDefaultPriority()));
				sms.setRemarks(Constants.STR_EMPTY);
				sms.setSentBy(user);
				sms.setSmsScheduled(aSetup.getSmsScheduled());
				sms.setSmsScheduledTime(aSetup.getSmsScheduledTime());
				//sms.setTelco(selectTelco(theCfg.getSmsTelcoRouting(), sms.getMobileNo(), theTelco).getId());
				sms.setTelco(aSetup.getTelco());
				sms.setVersion(Integer.valueOf(Constants.APPSVERSION));
				sms.setSmsStatus(aSetup.getSmsStatus());
				sms.setSmsStatusBy("automated");
				sms.setSmsStatusDatetime(new Date());
				sms.setApprovalBy("automated");
				sms.setApprovalDatetime(new Date());
				sms.setType(aSetup.getType());
				smsQueueDao.insert(sms);
				log.debug("SMS created");
				smsIdList.add(sms.getId());// Add id into list to be saved in saveAccNo method
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected boolean saveAccountNo(String[] data, ArrayList<Integer> smsIdList){
		try{
			IADSSynchDao adsDao = (IADSSynchDao) SgwCoreHibernateUtil.getBean("ADSSynchDao");
			for(int i=0;i<smsIdList.size();i++){
				ADSSynch aSynch=new ADSSynch();
				aSynch.setSmsId(smsIdList.get(i));
				aSynch.setAccNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_ACC_NO)]);
				aSynch.setTransferStatus(Constants.VALUE_0);
				if(!adsDao.save(aSynch)){
					return false;
				}
			}
		}catch (Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected String validateHeader(String[] data){
		return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_ACC_NO)+1);
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate){
		return validateDataLength(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate){
		return validateDataContent(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate){
		return generateSMSMessage(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
}
