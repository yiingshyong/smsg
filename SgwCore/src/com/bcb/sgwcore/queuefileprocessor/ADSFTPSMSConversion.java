package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.IADSSynchDao;
import com.bcb.crsmsg.modal.ADSSynch;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class ADSFTPSMSConversion extends DefaultFTPSMSConversion{
	public ADSFTPSMSConversion(){super();};
	
	public String process(String line, FtpReport reportModal, String user, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process conversion of ADS FTP SMS");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]")); 
			log.debug("data length: "+data.length);
			
			log.debug("theTemplate: "+theTemplate);
			theTemplate=findTemplateMessage(theTemplate, data);
			log.debug("theTemplate: "+theTemplate);
			
			String returnMsg = validation(data, theTemplate);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
					ArrayList<Integer> smsIdList=new ArrayList<Integer>();
					if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg, smsIdList)){
						if(saveAccountNo(data, smsIdList)){
						}else{
							returnMsg="Error in saving acc no. record";
						}
					}else{
						returnMsg="Error in creating SMS! ";
					}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean createSMS(String[] data, String user, FtpReport reportModal, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, ArrayList<Integer> smsIdList){
		try{
			int charPerSms=CommonUtils.calculateCharPerSms(theTemplate.getMsgFormatType(), CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			String message = generateSMSMessage(data, theTemplate);
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);// 2nd item is template message code, used to create sms message
	
			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
			SmsQueue sms = new SmsQueue();
			sms.setCreatedBy(user);
			sms.setCreatedDatetime(new Date());
			sms.setDeptId(reportModal.getDeptId());
			sms.setDept(reportModal.getDept());
			sms.setFtp(reportModal.getId());
			sms.setMessage(message);// 2nd item is template message code, used to create sms message
			sms.setTotalSms(msgArray.size());
			sms.setMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			sms.setMsgFormatType(theTemplate.getMsgFormatType());
			sms.setPriority(Integer.valueOf(fileConversion.getDefaultPriority()));
			sms.setRemarks(Constants.STR_EMPTY);
			sms.setSentBy(user);
			sms.setSmsScheduled(aSetup.getSmsScheduled());
			sms.setSmsScheduledTime(aSetup.getSmsScheduledTime());
			//sms.setTelco(selectTelco(theCfg.getSmsTelcoRouting(), sms.getMobileNo(), theTelco).getId());
			sms.setTelco(0);
			sms.setVersion(Integer.valueOf(Constants.APPSVERSION));
			
			if(CommonUtils.NullChecker(aSetup.getSmsAutoApproval(), String.class).toString().equals(Constants.STATUS_YES)){
				sms.setApprovalBy("automated");
				sms.setApprovalDatetime(new Date());
				if (CommonUtils.NullChecker(aSetup.getSmsScheduled(), String.class).toString().equals(Constants.STATUS_YES)&&CommonUtils.NullChecker(aSetup.getSmsScheduledTime(), String.class).toString().length()>0){
					sms.setSmsStatus(Constants.SMS_STATUS_6);
				}else{
					sms.setSmsStatus(Constants.SMS_STATUS_4);
				}
			}else{
				sms.setSmsStatus(Constants.SMS_STATUS_3);
			}
			
			sms.setSmsStatusBy("automated");
			sms.setSmsStatusDatetime(new Date());
			sms.setType(Constants.SMS_SOURCE_TYPE_4);
			smsQueueDao.insert(sms);
			smsIdList.add(sms.getId());// Add id into list to be saved in saveAccNo method
			log.debug("SMS created");
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected boolean saveAccountNo(String[] data, ArrayList<Integer> smsIdList){
		try{
			IADSSynchDao adsDao = (IADSSynchDao) SgwCoreHibernateUtil.getBean("ADSSynchDao");
			for(int i=0;i<smsIdList.size();i++){
				ADSSynch aSynch=new ADSSynch();
				aSynch.setSmsId(smsIdList.get(i));
				aSynch.setAccNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_ACC_NO)]);
				aSynch.setTransferStatus(Constants.VALUE_0);
				if(!adsDao.save(aSynch)){
					return false;
				}
			}
		}catch (Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected String validateHeader(String[] data){
		return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_ACC_NO)+1);
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate){
		return validateDataLength(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate){
		return validateDataContent(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate){
		return generateSMSMessage(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
}
