package com.bcb.sgwcore.queuefileprocessor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.bcb.common.util.Constants;
import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CCARecordFTPConversion extends FTPInboundProcess{
	
	public CCARecordFTPConversion(){super();};
	
	@SuppressWarnings("finally")
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception{
		FtpSetup theSetup = (FtpSetup) aSetup;
		log.debug("Process conversion of New Credit Card");
		String errorMessage = "";
		try{
			log.debug("line: " + line);			
			String data[] = trimData(StringUtils.splitByWholeSeparatorPreserveAllTokens(line, theSetup.getFileDelimiter())); 
			log.debug("data length: "+data.length);
						
			String mobileNo = data[0];
			String ccNo = data[2];
			String custId = data[3];
			if(StringUtils.isEmpty(ccNo)){
				errorMessage = "Credit Card Number Is Empty";
			}else if(ccNo.trim().length() < 16){
				errorMessage = "Invalid Length Credit Card Number";
			}else if(!StringUtils.isNumeric(ccNo.trim())){
				errorMessage = "Non Numeric Credit Card Number";
			}
			
			if(StringUtils.isNotEmpty(errorMessage)){
				errorMessage += Constants.LINE_SEPARATOR;
			}
			
			if(StringUtils.isEmpty(mobileNo)){
				errorMessage += "Mobile Number Is Empty";
//			}else if(ccNo.trim().length() < 16){
//				errorMessage = "Invalid Length Mobile Number";
			}else if(!StringUtils.isNumeric(mobileNo.trim())){
				errorMessage += "Non Numeric Mobile Number";
			}

			if(StringUtils.isNotEmpty(errorMessage)){
				errorMessage += Constants.LINE_SEPARATOR;
			}
			
			if(StringUtils.isEmpty(custId)){
				errorMessage += "Customer ID Is Empty";
			}
						
			if(StringUtils.isEmpty(errorMessage)){
					if(!saveRecord(ccNo, mobileNo, custId, reportModal)){
						errorMessage = "Failed to save record";
					}
			}
		}catch(Exception e){
			log.error(e.getMessage(),e);
			if(StringUtils.isNotEmpty(errorMessage)){
				errorMessage += Constants.LINE_SEPARATOR;
			}
			errorMessage += "Unexpected exception while processing record"; 
		}finally{
			if(StringUtils.isNotEmpty(errorMessage)){
				if(!getRemarks().contains(errorMessage)){
					getRemarks().add(errorMessage);
				}
			}
			if(StringUtils.isEmpty(errorMessage)){
				return null;
			}
			return errorMessage;
		}
	}
	
	protected boolean saveRecord(String ccNo, String mobileNo, String custId, FtpReport report){
		try{
			Date todayDate = new Date();
			CreditCardActivation cca = new CreditCardActivation();
			cca.setCreatedDatetime(todayDate);
			cca.setLastModifiedDatetime(todayDate);
			cca.setFailureAttemptCount(0);
			cca.setStatus(CreditCardActivation.CC_ACTIVATION_STATUS_NOT_ACTIVATED);
			cca.setFtpReportId(report.getId());
			
			cca.setCardNo(SecurityManager.getInstance().encrypt(ccNo));
			cca.setMobileNo(mobileNo);
			cca.setCustomerId(custId);
			
			DaoBeanFactory.getHibernateTemplate().save(cca);	
			return true;
		}catch (Exception ex){
			log.error("Unexpected Error Caught. Reason: " + ex.getMessage(), ex);
			return false;
		}
	}

	@Override
	public String postTemplateProcess() {
		super.postTemplateProcess();
		return (String) DaoBeanFactory.getHibernateTemplate().executeWithNativeSession(new HibernateCallback() {			
			@Override
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {	
				Connection connection = session.connection();
				int uniqueCount = 0;
				int dupsArchivedCount = 0;
				int dupsDeletedCount = 0;
				int createTmpTable=1;
				int dropTmpTable=1;
				int totalRecordMain=0;
				try{
					log.info("Check for duplicates records");
					connection.setAutoCommit(false);
					Statement stmt = connection.createStatement();
					
					ResultSet rs = stmt.executeQuery("select count(id) from cc_activation");
					if(rs.next()){
						totalRecordMain = rs.getInt(1);						
					}
					createTmpTable = stmt.executeUpdate("create temporary table cc_unique (id int(10) not null, primary key(id))");
					uniqueCount = stmt.executeUpdate("insert into cc_unique select max(id) from cc_activation group by card_no");
					if(uniqueCount < totalRecordMain){
						dupsArchivedCount = stmt.executeUpdate("insert into cc_activation_duplicate (select a.* from cc_activation a left outer join cc_unique b on a.id=b.id where b.id is null)");
						dupsDeletedCount = stmt.executeUpdate("delete a from cc_activation a left outer join cc_unique b on a.id=b.id where b.id is null");						
					}
					dropTmpTable = stmt.executeUpdate("drop temporary table cc_unique");
					connection.commit();
					return null;
				}catch(Exception e){
					connection.rollback();
					log.error("Unexpected Error. Reason: " + e.getMessage(), e);
					return "Error while performing duplicate credit card records cleanup. Message: " + e.getMessage();
				}finally{
					log.info("Tmp Tbl created: " + (createTmpTable == 0 ? "YES":"NO") + " Total record: " + totalRecordMain + " unique record: " + uniqueCount + " duplicates archived: " + dupsArchivedCount + " duplicates deleted: " + dupsDeletedCount + " Tmp Tbl Drop: " + (dropTmpTable==0? "YES" : "NO"));
				}
			}
		});
	}
	
	public static void main(String[] args) {
		String postActionStatus = new CCARecordFTPConversion().postTemplateProcess();
		log.info(postActionStatus == null ? "Post Action Run Successfully." : postActionStatus);
	}
	
}
