package com.bcb.sgwcore.queuefileprocessor;

import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.ICustomerDao;
import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class CCBulkSMSConversion extends DefaultBulkSMSConversion{
	public String process(String line, FtpReport reportModal, String user, BulkSmsSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process conversion of Bulk SMS");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]")); 
			log.debug("data length: "+data.length);
			
			if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
				log.debug("theTemplate: "+theTemplate);
				theTemplate=findTemplateMessage(theTemplate, data);
				log.debug("theTemplate: "+theTemplate);
			}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
				theTemplate=new MsgTemplate();
			}
			
			String returnMsg = validation(data, theTemplate);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
				if(createCustomer(data)){
					if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg)){
					}else{
						returnMsg="Error in creating SMS! ";
					}
				}else{
					returnMsg="Error in creating customer record";
				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean createCustomer(String[] data){
		try{
			ICustomerDao custDao = (ICustomerDao) SgwCoreHibernateUtil.getBean("customerDao");
			List<Customer> custList = custDao.findByCustRefNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)]);
			
			if(custList.size()==0){
				Customer aCustomer=new Customer();
				aCustomer.setMobileNo(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]));
				aCustomer.setCustRefNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)]);
				aCustomer.setSmsStatus(Constants.STATUS_SUBSCRIPTION);
				aCustomer.setSmsStatusBy("CCFTP");
				aCustomer.setSmsStatusDatetime(new Date());
				aCustomer.setCreatedBy("CCFTP");
				aCustomer.setCreatedDatetime(new Date());
				aCustomer.setTransferStatus(Constants.VALUE_0);
				custDao.insert(aCustomer);
			}else if(custList.size()==1){// Customer already exist, check if need to update hp no.
				Customer aCustomer=(Customer)custList.get(0);
				log.debug("aCustomer.getMobileNo(): "+aCustomer.getMobileNo());
				if(aCustomer.getMobileNo().equals(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]))){// Same mobile no., no need to do anything
				}else{// Not same hp no., update hp no.
					aCustomer.setMobileNo(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]));
					aCustomer.setSmsStatus(Constants.STATUS_SUBSCRIPTION);
					aCustomer.setSmsStatusBy("CCFTP");
					aCustomer.setSmsStatusDatetime(new Date());
					aCustomer.setModifiedBy("CCFTP");
					aCustomer.setModifiedDatetime(new Date());
					aCustomer.setTransferStatus(Constants.VALUE_0);
					custDao.update(aCustomer);
				}
			}else{// Duplicate custRefNo exist
				return false;
			}
		}catch (Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected String validateHeader(String[] data){
		return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)+1);
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate){
		return validateDataLength(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)+1);
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate){
		return validateDataContent(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)+1);
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate){
		return generateSMSMessage(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)+1);
	}
	
	protected String validateDuplicateRecord(String[] data, MsgTemplate theTemplate){// Need to check on duplicate cust ref no. with different mobile no.
		String errorCode=null;
		String custRefNo=data[Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)];
		String mobileNo=data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)];
		
		if(theTemplate!=null){
			for(int i=0;i<duplicateRecordControl[0].size();i++){
				if(custRefNo.equals(duplicateRecordControl[0].get(i))&&!mobileNo.equals(duplicateRecordControl[1].get(i))){
					errorCode="Duplicate Cust Ref No. with different mobile no.!";
				}
			}
			
			// Adding custRefNo & mobile no. to control list
			duplicateRecordControl[0].add(custRefNo);
			duplicateRecordControl[1].add(mobileNo);
		}
		return errorCode;
	}
}
