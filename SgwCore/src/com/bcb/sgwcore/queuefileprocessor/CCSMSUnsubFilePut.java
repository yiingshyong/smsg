package com.bcb.sgwcore.queuefileprocessor;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.ICustomerDao;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class CCSMSUnsubFilePut extends FTPOutboundProcess{
	public CCSMSUnsubFilePut(){super();}

	public boolean createOutBoundFiles(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg) throws Exception {
		log.debug("CreateOutBoundFiles");
		boolean status=true;
		
		// Read unsub customer
		ICustomerDao custDao = (ICustomerDao) SgwCoreHibernateUtil.getBean("customerDao");
		List<Customer> custList=custDao.findUnsubByTransferStatus(Constants.STATUS_FALSE_0);

		SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DDMMYYYY);
		FileOutputStream fos = new FileOutputStream(outboundFile.getTheFile());
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos));

		// write header
		out.write(Constants.FTP_FILE_HEADER);
		out.write(theCfg.getDefaultFileDelimiter());
		out.write(theFormatter.format(new Date()));
		out.write(Constants.SPECIAL_CHAR_NEWLINE);
		
		// write content
		for(int i=0;i<custList.size();i++){
			Customer aCustomer=(Customer)custList.get(i);
			aCustomer.setTransferStatus(Constants.STATUS_TRUE_1);
			
			out.write(Constants.FTP_FILE_DATA);
			out.write(theCfg.getDefaultFileDelimiter());
			
			String smsStatusDatetime=Constants.STR_EMPTY;
			if(aCustomer.getSmsStatusDatetime()==null){
				for(int j=0;j<8;j++)smsStatusDatetime+=Constants.VALUE_0;
			}else{
				smsStatusDatetime=theFormatter.format(aCustomer.getSmsStatusDatetime());
			}
			out.write(smsStatusDatetime);
			
			out.write(theCfg.getDefaultFileDelimiter());
			out.write(CommonUtils.NullChecker(aCustomer.getCustRefNo(), String.class).toString());
			out.write(theCfg.getDefaultFileDelimiter());
			String mobileNo=CommonUtils.NullChecker(aCustomer.getMobileNo(), String.class).toString();
			if(mobileNo.length()==0){
				for(int j=mobileNo.length();j<18;j++)mobileNo+=Constants.VALUE_0;
			}else{
				for(int j=mobileNo.length();j<18;j++)mobileNo+=Constants.STR_SPACE;
			}
			
			out.write(mobileNo);
			out.write(Constants.SPECIAL_CHAR_NEWLINE);
			custList.set(i, aCustomer);
		}
		
		// write trailer
		out.write(Constants.FTP_FILE_EOF);
		
		// Close the writter
		out.flush();
		out.close();
		fos.flush();
		out.close();
		
		custDao.update(custList);
		log.debug("status: "+status);
		log.debug("end CCSMSUnsubFilePut");
		return status;
	}

}
