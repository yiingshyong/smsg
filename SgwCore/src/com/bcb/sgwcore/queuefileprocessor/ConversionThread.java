package com.bcb.sgwcore.queuefileprocessor;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.dao.IBatchSetupDao;
import com.bcb.crsmsg.dao.ISmsCfgDao;
import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.FileUtil;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class ConversionThread implements Runnable {

	FileReader fileReader;
	FileConversion fileConversion;
	IDataProcess dataProcess;
	boolean isRunning;
	Log log = LogManager.getLog();
	List<Object> fcList;
	
	public ConversionThread(FileReader fileReader) {
		log.debug("init Conversion Thread with fileReader");
		this.fileReader = fileReader;
		this.fileConversion = this.fileReader.getFileConversion();
		try {
			this.dataProcess = new DefaultDataConversion(fileConversion);
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	public void run() {
		log.info(" ConversionThread for " + Thread.currentThread().getName()
				+ " is up");
		isRunning = true;

		int count = 0;
		int TiringValue = 5000;
		IBatchSetupDao setupDao;
		ISmsCfgDao cfgDao;
		SmsCfg theCfg;
		//List<Telco> theTelco;
		//ITelcoEsmsDao telcoDao;
		BatchSetup theSetup;
		
		while (isRunning) {
			String filename = fileReader.removeFirst();
			if (filename != null && FileUtil.isExist(filename)) {
				log.debug("filename: "+filename);
				try{
					setupDao = (IBatchSetupDao) SgwCoreHibernateUtil.getBean(fileConversion.getBatchSetupDao());// Keep checking for new FtpSetup
					fcList = setupDao.findSetup(fileConversion.getId());// Keep checking for new FtpSetup
					
					cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");// Keep checking for new smsCfg
					theCfg = cfgDao.findByCfgId(Constants.APPSVERSION);// Keep checking for smsCfg
					
					/*telcoDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");// Keep checking for new telco
					theTelco = telcoDao.findAllTelcoEsmsAvailableByTelcoRoutingMode(theCfg.getSmsTelcoRouting(), theCfg.getSmsTelcoRoutingParam());// Keep checking for telco
					*/
					log.debug("fcList.size(): "+fcList.size());
					String exactFileName = StringUtils.substringAfterLast(filename, System.getProperty("file.separator"));
					for(Object aSetup:fcList){
						theSetup=(BatchSetup)aSetup;
						log.debug("aSetup.getFileName()   : " + theSetup.getFileName());
						log.debug("Exact file name on disk: " + exactFileName);
						if(exactFileName.equals(theSetup.getFileName())){
							//dataProcess.processAll(filename, aSetup, theCfg, theTelco);
							dataProcess.processAll(filename, aSetup, theCfg, null);
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			} /*else {
				sleepAWhile();
			}*/

			if (count > TiringValue) {
				sleepAWhile();
				count = 0;
			}

			count++;
		}

		log.info(" ConversionThread for " + Thread.currentThread().getName()
				+ " is shutdown");

	}
	
	private void sleepAWhile() {
		try {
			Thread.yield();
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void shutdown() {
		isRunning = false;
	}

}
