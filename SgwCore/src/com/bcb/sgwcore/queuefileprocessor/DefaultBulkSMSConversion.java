package com.bcb.sgwcore.queuefileprocessor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SendMailBean;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class DefaultBulkSMSConversion extends DefaultSMSConversion{
	public DefaultBulkSMSConversion(){super();}
	
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception {
		return process(line, reportModal, user, (BulkSmsSetup)aSetup, theTemplate, theCfg, theTelco);
	}
	
	public String process(String line, FtpReport reportModal, String user, BulkSmsSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process conversion of Bulk SMS");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]", -1)); 
			log.debug("data length: "+data.length);
			
			if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
				log.debug("theTemplate: "+theTemplate);
				theTemplate=findTemplateMessage(theTemplate, data);
				log.debug("theTemplate: "+theTemplate);
			}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
				theTemplate=new MsgTemplate();
			}
			
			String returnMsg = validation(data, theTemplate, aSetup);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
				if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg)){
				}else{
					returnMsg="Error in creating SMS! ";
				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean createSMS(String[] data, String user, FtpReport reportModal, BulkSmsSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg){
		try{
			int charPerSms=CommonUtils.calculateCharPerSms(aSetup.getMsgFormatType(), CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			
			ArrayList<String> msgArray=new ArrayList<String>();
			log.debug("CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString(): "+CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString());
			String message = "";
			if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
				message = generateSMSMessage(data, theTemplate);
			}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
				message = aSetup.getMessage();
			}
			msgArray=CommonUtils.strToArray(message, charPerSms);

			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
			SmsQueue sms = new SmsQueue();
			sms.setCreatedBy(user);
			sms.setCreatedDatetime(new Date());
			sms.setDeptId(reportModal.getDeptId());
			sms.setDept(reportModal.getDept());
			sms.setFtp(reportModal.getId());
			sms.setMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			sms.setMessage(message);
			sms.setTotalSms(msgArray.size());
			sms.setMsgFormatType(aSetup.getMsgFormatType());
			sms.setPriority(aSetup.getPriority());
			sms.setRemarks(Constants.STR_EMPTY);
			sms.setSentBy(user);
			sms.setSmsScheduled(aSetup.getSmsScheduled());
			sms.setSmsScheduledTime(aSetup.getSmsScheduledTime());
			//sms.setTelco(selectTelco(theCfg.getSmsTelcoRouting(), sms.getMobileNo(), theTelco).getId());
			sms.setTelco(aSetup.getTelco());
			sms.setVersion(Integer.valueOf(Constants.APPSVERSION));
			sms.setSmsStatus(aSetup.getSmsStatus());
			sms.setSmsStatusBy("automated");
			sms.setSmsStatusDatetime(new Date());
			sms.setApprovalBy("automated");
			sms.setApprovalDatetime(new Date());
			sms.setType(aSetup.getType());
			smsQueueDao.insert(sms);
			log.debug("SMS created");
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected String validation(String[] data, MsgTemplate theTemplate, BulkSmsSetup aSetup){
		log.debug("Check Validation of SMS");
		String errorsCode = null;
		ArrayList<String> errorsCodeList=new ArrayList<String>();
		
		errorsCode=validateHeader(data, aSetup);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		else{// only proceed to other validation if header is ok
			errorsCode=validateMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
			
			errorsCode=validateTemplateMessage(theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
			
			errorsCode=validateDataLength(data, theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null){
				errorsCodeList.add(errorsCode);
			}else{// only check for data content if data length is valid
				errorsCode=validateDataContent(data, theTemplate);
				log.debug("errorsCode: "+errorsCode);
				if(errorsCode!=null)errorsCodeList.add(errorsCode);
			}
			
			errorsCode=validateDuplicateRecord(data, theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
		}
		errorsCode=CommonUtils.arrayToString(errorsCodeList, Constants.SPECIAL_CHAR_COMMA);

		log.debug("errorsCode: "+errorsCode);
		return (errorsCode.length()==0)? null:errorsCode;
	}
	
	protected String validateHeader(String[] data, BulkSmsSetup aSetup){
		if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE)){
			return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
		}else if(CommonUtils.NullChecker(aSetup.getBulkFileMode(), String.class).toString().equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)){
			return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)+1);
		}else{
			return null;
		}
	}
	
	public String sendLimitationNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;

		List<String> toList=CommonUtils.strToArray(theCfg.getBulkSmsThresholdNotificationEmail(), CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
		for(int i=0;i<toList.size();i++){
			if(i!=0){returnCodes+=Constants.SPECIAL_CHAR_NEWLINE;}
			returnCodes+=sendLimitationEmailNotification(theSetup, theReportModal, theCfg.getBulkSmsThresholdNotificationEmailSender(), toList.get(i), totalRecords, noOfFailed);
		}
		
		toList=CommonUtils.strToArray(theCfg.getBulkSmsThresholdNotificationSms(), CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
		for(int i=0;i<toList.size();i++){
			if(i!=0){returnCodes+=Constants.SPECIAL_CHAR_NEWLINE;}
			returnCodes+=sendLimitationSmsNotification(theSetup, theReportModal, theCfg, theCfg.getBulkSmsThresholdNotificationSmsSender(), toList.get(i), totalRecords, noOfFailed);
		}
		
		return returnCodes;
	}
	
	public String sendLimitationEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendLimitationEmailNotification((BulkSmsSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendLimitationEmailNotification(BulkSmsSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;
		SendMailBean theMail=new SendMailBean();
		String subject="Bulk file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Hit Bulk Limit";
		String message="Bulk file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Hit Bulk Limit "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Sender: "+theReportModal.getCreatedBy()+Constants.SPECIAL_CHAR_NEWLINE+
			"Dept: "+theReportModal.getDept()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total created SMS: "+(totalRecords-noOfFailed)+Constants.SPECIAL_CHAR_NEWLINE;
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
		}else{
			returnCodes="Bulk file process limitation email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
		return returnCodes;
	}
	
	public String sendLimitationSmsNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendLimitationSmsNotification((BulkSmsSetup)theSetup, theReportModal, theCfg, from, to, totalRecords, noOfFailed);
	}
	
	public String sendLimitationSmsNotification(BulkSmsSetup theSetup, FtpReport theReportModal, SmsCfg theCfg, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;
		try{
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
			String message="Bulk file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: Hit Bulk Limit "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"Status Time: "+theFormatter.format(new Date())+Constants.SPECIAL_CHAR_NEWLINE+
			"Sender: "+theReportModal.getCreatedBy()+Constants.SPECIAL_CHAR_NEWLINE+
			"Dept: "+theReportModal.getDept()+Constants.SPECIAL_CHAR_NEWLINE+
			"SMS created: "+(totalRecords-noOfFailed)+Constants.SPECIAL_CHAR_NEWLINE;
			
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
			
//			for(int j=0;j<msgArray.size();j++){
				SmsQueue aSms=new SmsQueue();
				aSms.setCreatedBy(theCfg.getFtpFailureNotificationSmsSender());
				aSms.setCreatedDatetime(new Date());
				aSms.setDeptId(theReportModal.getDeptId());
				aSms.setDept(theReportModal.getDept());
				aSms.setMessage(message);
				aSms.setTotalSms(msgArray.size());
				aSms.setMobileNo(to);
				aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
				aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
				aSms.setSmsStatus(Constants.SMS_STATUS_4);
				aSms.setSmsStatusBy("DBulkSMSCvn");
				aSms.setSmsStatusDatetime(new Date());
				aSms.setSentBy(theCfg.getFtpFailureNotificationSmsSender());
				aSms.setType(Constants.SMS_SOURCE_TYPE_5);
				
				ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
				smsDao.insert(aSms);
				
//				lastSmsIdList.add(CommonUtils.NullChecker(aSms.getId(), String.class).toString());
//				
//				aSms.setConcatenateSms(msgArray.size()==1?Constants.STR_EMPTY:j==0?CommonUtils.NullChecker(aSms.getId(), String.class).toString():j!=msgArray.size()-1?lastSmsId:CommonUtils.arrayToString(lastSmsIdList, theCfg.getTableListDelimiter()));
//				smsDao.update(aSms);
//				lastSmsId=CommonUtils.NullChecker(aSms.getId(), String.class).toString();
//			}
		}catch(Exception ex){
			ex.printStackTrace();
			returnCodes="Bulk file process limitation SMS notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
			"Details: "+ex.getCause();
		}
		return returnCodes;
	}
}
