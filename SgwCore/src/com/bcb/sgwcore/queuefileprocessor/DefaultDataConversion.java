package com.bcb.sgwcore.queuefileprocessor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.dao.IFtpReportDao;
import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.sgwcore.util.FileUtil;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class DefaultDataConversion implements IDataProcess{
	
	FileConversion fileConversion;
	ILineTemplate templateProcess;
	Log log=LogManager.getLog();
	
	
	static	SimpleDateFormat sdfFile = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
	static	SimpleDateFormat sdfFolder = new SimpleDateFormat("yyyyMMdd");
	

	public DefaultDataConversion(FileConversion fileConversion) {
		log.debug("initialize default data conversion");
		this.fileConversion=fileConversion;
		try{
			templateProcess =  (ILineTemplate) Class.forName(fileConversion.getConversionClassName()).newInstance();
			log.debug("create conversion class instance: "+fileConversion.getConversionClassName());
			templateProcess.setFileConversion(fileConversion);
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
	}

	public void processAll(String filename, Object aSetup, SmsCfg theCfg, List<Telco> theTelco) {
		log.debug("processAll: "+filename);
		if (FileUtil.isExist(filename)){
			log.debug(filename+ " exist");
			String fileInProcess = filename+FileUtil.IN_PROCESS;
			if(FileUtil.rename(filename, fileInProcess)){
				FtpReport reportModal = new FtpReport();
				log.debug("FileUtil process");
				
				FileUtil.process(fileInProcess, templateProcess, aSetup, reportModal, theCfg, theTelco);
					
				log.debug("backup Files");
				backupFiles(fileInProcess, reportModal, (BatchSetup)aSetup);
			}else{
				log.error("process file renamed failed for "+filename);
			}
		}
	}
	
	public String processAll(FileObj outboundFile, Object aSetup, FtpReport reportModal , SmsCfg theCfg) {
		log.debug("processAll: "+outboundFile);
		try{
			if(!outboundFile.getTheFile().exists()){
				if(!outboundFile.getTheFile().getParentFile().exists()){
					if(!outboundFile.getTheFile().getParentFile().mkdirs()){
						return "Error in creating directories!";
					}
				}
				
				if(!outboundFile.getTheFile().createNewFile()){
					return "Error in creating new file!";
				}
			}
			
			if(!FileUtil.process(outboundFile, templateProcess, aSetup, reportModal, theCfg)){
				return "Error in process";
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return "Error in processAll";
		}
			
		return null;
	}

	public boolean backupFiles(String fileInProcess, FtpReport reportModal, BatchSetup setup) {
		String fileLog = getFileDoneLog(fileInProcess);
		if(FileUtil.copy(fileInProcess, fileLog)){
			try{
				reportModal.setFilePath(fileLog);
				IFtpReportDao ftpReportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
				ftpReportDao.update(reportModal);
				
				if(setup instanceof FtpSetup && ((FtpSetup)setup).isRemoveFTPLocalFile()){
					if(!new File(fileLog).delete()){
						log.error("Unable to delete '" + fileLog + "'");
					}					
				}
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return FileUtil.delete(fileInProcess);
		}else{
			return false;
		}
	}
	
	private static String getFileDoneLog(String fileDone) {
		int dot=fileDone.lastIndexOf(FileUtil.getFileSeparator());
		
		StringBuffer fileLog = new StringBuffer(); 
		fileLog.append(fileDone.substring(0, dot));
		fileLog.append(FileUtil.getFileSeparator());
		fileLog.append(FileUtil.LOG);
		fileLog.append(FileUtil.getFileSeparator());
		fileLog.append(sdfFolder.format(new Date()));
		fileLog.append(FileUtil.getFileSeparator());
		fileLog.append(fileDone.substring(dot+1, fileDone.length()));
		fileLog.append("_");
		fileLog.append(sdfFile.format(new Date()));
		return fileLog.toString();
	}

	public ILineTemplate getTemplateProcess() {
		return templateProcess;
	}

	public void setTemplateProcess(ILineTemplate templateProcess) {
		this.templateProcess = templateProcess;
	}
}
