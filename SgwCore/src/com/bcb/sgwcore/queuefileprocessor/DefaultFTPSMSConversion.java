package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SendMailBean;

public class DefaultFTPSMSConversion extends DefaultSMSConversion{
	public DefaultFTPSMSConversion(){super();}
	
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception {
		return process(line, reportModal, user, (FtpSetup)aSetup, theTemplate, theCfg, theTelco);
	}
	
	public String process(String line, FtpReport reportModal, String user, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process conversion of FTP SMS");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]", -1)); 
			log.debug("data length: "+data.length);
			
			log.debug("theTemplate: "+theTemplate);
			theTemplate=findTemplateMessage(theTemplate, data);
			log.debug("theTemplate: "+theTemplate);
			
			String returnMsg = validation(data, theTemplate);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
				if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg)){
				}else{
					returnMsg="Error in creating SMS! ";
				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean createSMS(String[] data, String user, FtpReport reportModal, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg){
		try{
			int charPerSms=CommonUtils.calculateCharPerSms(theTemplate.getMsgFormatType(), CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			String message = generateSMSMessage(data, theTemplate);			
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);// 2nd item is template message code, used to create sms message
			SmsQueue sms = new SmsQueue();
			sms.setCreatedBy(user);
			sms.setCreatedDatetime(new Date());
			sms.setDeptId(reportModal.getDeptId());
			sms.setDept(reportModal.getDept());
			sms.setFtp(reportModal.getId());
			sms.setMessage(message);// 2nd item is template message code, used to create sms message
			sms.setTotalSms(msgArray.size());
			sms.setMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			sms.setMsgFormatType(theTemplate.getMsgFormatType());
			sms.setPriority(Integer.valueOf(fileConversion.getDefaultPriority()));
			sms.setRemarks(Constants.STR_EMPTY);
			sms.setSentBy(user);
			sms.setSmsScheduled(aSetup.getSmsScheduled());
			sms.setSmsScheduledTime(aSetup.getSmsScheduledTime());
			//sms.setTelco(selectTelco(theCfg.getSmsTelcoRouting(), sms.getMobileNo(), theTelco).getId());
			sms.setTelco(0);
			sms.setVersion(Integer.valueOf(Constants.APPSVERSION));
			
			if(CommonUtils.NullChecker(aSetup.getSmsAutoApproval(), String.class).toString().equals(Constants.STATUS_YES)){
				sms.setApprovalBy("automated");
				sms.setApprovalDatetime(new Date());
				if (CommonUtils.NullChecker(aSetup.getSmsScheduled(), String.class).toString().equals(Constants.STATUS_YES)&&CommonUtils.NullChecker(aSetup.getSmsScheduledTime(), String.class).toString().length()>0){
					sms.setSmsStatus(Constants.SMS_STATUS_6);
				}else{
					sms.setSmsStatus(Constants.SMS_STATUS_4);
				}
			}else{
				sms.setSmsStatus(Constants.SMS_STATUS_3);
			}
			
			sms.setSmsStatusBy("automated");
			sms.setSmsStatusDatetime(new Date());
			sms.setType(Constants.SMS_SOURCE_TYPE_4);
			smsQueueDao.insert(sms);
			log.debug("SMS created");
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
/*	public String sendSuccessEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendSuccessEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendSuccessEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Success";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Success"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;

		message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();

		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process success email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
	public String sendFailureEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendFailureEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendFailureEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Failed";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Failed"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;
		
		if(totalRecords==0&&noOfFailed==0){
			message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();
		}
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process failure email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
*/	
	protected String validateDuplicateRecord(String[] data, MsgTemplate theTemplate){// FTP will contain duplicate entry with different message
		String errorCode=null;
		return errorCode;
	}
}
