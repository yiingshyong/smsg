package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.List;

import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ITemplateMessageDao;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class DefaultSMSConversion extends FTPInboundProcess{
	ISmsQueueDao smsQueueDao;
	
	public DefaultSMSConversion(){
		try {
			smsQueueDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}

	protected String validation(String[] data, MsgTemplate theTemplate){
		log.debug("Check Validation of SMS");
		String errorsCode = null;
		ArrayList<String> errorsCodeList=new ArrayList<String>();
		
		errorsCode=validateHeader(data);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		else{// only proceed to other validation if header is ok
			errorsCode=validateMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
			
			errorsCode=validateTemplateMessage(theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
			
			errorsCode=validateDataLength(data, theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null){
				errorsCodeList.add(errorsCode);
			}else{// only check for data content if data length is valid
				errorsCode=validateDataContent(data, theTemplate);
				log.debug("errorsCode: "+errorsCode);
				if(errorsCode!=null)errorsCodeList.add(errorsCode);
			}
			
			errorsCode=validateDuplicateRecord(data, theTemplate);
			log.debug("errorsCode: "+errorsCode);
			if(errorsCode!=null)errorsCodeList.add(errorsCode);
		}
		errorsCode=CommonUtils.arrayToString(errorsCodeList, Constants.SPECIAL_CHAR_COMMA);

		log.debug("errorsCode: "+errorsCode);
		return (errorsCode.length()==0)? null:errorsCode;
	}
	
	protected String validateHeader(String[] data){
		return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String validateHeader(String[] data, Integer headerNo){
		String returnCode=null;
		// validate Header length
		if(data.length>=headerNo){// valid length
		}else{// header no. not correct
			returnCode="Invalid record header. Record should consist of "+headerNo+" header(s)";
		}

		return returnCode;
	}
	
	protected String validateTemplateMessage(MsgTemplate theTemplate){
		String returnCode=null;
		log.debug("theTemplate: "+theTemplate);
		if(theTemplate==null){
			returnCode="Msg Template Code does not exist. ";
		}else{
		}
		
		return returnCode;
	}
	
	protected MsgTemplate findTemplateMessage(MsgTemplate theTemplate, String[] data){
		if(theTemplate==null){
			try{// Look at file msg template code, will be slower
				if(data.length>Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)){// Contain template code in file line read
					ITemplateMessageDao templateMsgDao = (ITemplateMessageDao) SgwCoreHibernateUtil.getBean("templateMessageDao");
					List theTemplateList=templateMsgDao.findAvailabelTemplateByMsgCode(data[Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)]);
					if(theTemplateList.size()==1){
						theTemplate=(MsgTemplate)theTemplateList.get(0);
					}else{// No template code found
						
					}
				}else{// No template code in file line read
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
				theTemplate=null;
			}
		}else{
			
		}
		return theTemplate;
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate){
		return validateDataLength(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate, Integer headerNo){
		String errorCode=null;
		
		if(theTemplate!=null&&CommonUtils.NullChecker(theTemplate.getDynamicFieldNo(), String.class).toString().length()>0&&data.length-headerNo!=Integer.valueOf(CommonUtils.NullChecker(theTemplate.getDynamicFieldNo(), Integer.class).toString())){
			//errorCode="Template message dynamic field length not correct: "+(data.length-2)+" compare to template dynamic field no.: "+theTemplate.getDynamicFieldNo()+". ";
			errorCode="File record dynamic field input not match with template message dynamic field no.! ";
		}
		return errorCode;
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate){
		return validateDataContent(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate, Integer headerNo){
		String returnCodes=null;
		
		if(theTemplate!=null&&CommonUtils.NullChecker(theTemplate.getDynamicFieldName(), String.class).toString().length()>0){
			String[] dynamicFieldName=theTemplate.getDynamicFieldName().split("["+FileReader.tableListDelimiter+"]");
			log.debug("dynamicFieldName.length: "+dynamicFieldName.length);
			for(int i=0;i<dynamicFieldName.length;i++){
				if(theTemplate.getTemplateContent().contains(Constants.TEMPLATE_DYNAMIC_FIELD_LEFT_INDICATOR+dynamicFieldName[i].trim()+Constants.TEMPLATE_DYNAMIC_FIELD_RIGHT_INDICATOR)){
					if(CommonUtils.NullChecker(data[i+headerNo], String.class).toString().length()==0){
						return "File record dynamic field input contains empty input!";
					}
				}else{// dynamic field not used, skip validation
					
				}
			}
		}
		
		return returnCodes;
	}
	
	protected String validateDuplicateRecord(String[] data, MsgTemplate theTemplate){
		String errorCode=null;
		String mobileNo=data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)];
		if(theTemplate!=null){
			for(int i=0;i<duplicateRecordControl[0].size();i++){
				if(mobileNo.equals(duplicateRecordControl[0].get(i))&&(theTemplate.getTemplateCode()==null||theTemplate.getTemplateCode().equals(duplicateRecordControl[1].get(i)))){
					errorCode="Duplicate mobile no. and template code!";
				}
			}
			
			// Adding mobile no & template code to control list
			duplicateRecordControl[0].add(mobileNo);
			duplicateRecordControl[1].add(theTemplate.getTemplateCode());
		}
		return errorCode;
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate){
		return generateSMSMessage(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_TEMPLATE_CODE)+1);
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate, Integer headerNo){
		String msg=(theTemplate.getTemplateContent()==null)?Constants.STR_EMPTY:theTemplate.getTemplateContent();
		if(CommonUtils.NullChecker(theTemplate.getDynamicFieldName(), String.class).toString().length()>0){
			String[] dynamicFieldName=theTemplate.getDynamicFieldName().split("["+FileReader.tableListDelimiter+"]");
			log.debug("dynamicFieldName.length: "+dynamicFieldName.length);
			for(int i=0;i<dynamicFieldName.length;i++){
				if(msg.contains(Constants.TEMPLATE_DYNAMIC_FIELD_LEFT_INDICATOR+dynamicFieldName[i].trim()+Constants.TEMPLATE_DYNAMIC_FIELD_RIGHT_INDICATOR))
					msg=msg.replace(Constants.TEMPLATE_DYNAMIC_FIELD_LEFT_INDICATOR+dynamicFieldName[i].trim()+Constants.TEMPLATE_DYNAMIC_FIELD_RIGHT_INDICATOR, data[i+headerNo].trim());
			}
		}
		return msg;
	}
}
