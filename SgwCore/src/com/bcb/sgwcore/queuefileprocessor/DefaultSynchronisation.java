package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;

import com.bcb.crsmsg.dao.ICustomerDao;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class DefaultSynchronisation extends FTPInboundProcess{
	ICustomerDao customerDao;
	
	public DefaultSynchronisation(){
		try {
			customerDao = (ICustomerDao) SgwCoreHibernateUtil.getBean("customerDao");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}

	protected String validation(String[] data){
		log.debug("Check Validation of Phone Synchronisation");
		String errorsCode = null;
		ArrayList<String> errorsCodeList=new ArrayList<String>();
		
		errorsCode=validateMobileNo(data[Integer.valueOf(Constants.SYNC_FIELD_MOBILE_NO)]);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		
		errorsCode=validateDataLength(data);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		
		errorsCode=validateDuplicateRecord(data);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		
		errorsCode=validateDataContent(data);
		log.debug("errorsCode: "+errorsCode);
		if(errorsCode!=null)errorsCodeList.add(errorsCode);
		
		errorsCode=CommonUtils.arrayToString(errorsCodeList, Constants.SPECIAL_CHAR_COMMA);
		
		log.debug("errorsCode: "+errorsCode);
		return (errorsCode.length()==0)? null:errorsCode;
	}
	
	protected String validateDataLength(String[] data){
		return validateDataLength(data, Integer.valueOf(Constants.SYNC_FIELD_CUST_REF_NO)+1);
	}
	
	protected String validateDataLength(String[] data, Integer headerNo){
		String errorCode=null;
		
		if(data.length!=headerNo){
			errorCode="File record data length not correct! ";
		}
		
		return errorCode;
	}
	
	protected String validateDataContent(String[] data){
		String returnCodes=null;
		
		for(int i=0;i<data.length;i++){
			if(CommonUtils.NullChecker(data[i], String.class).toString().length()==0){
				return "File record dynamic field input contains empty input!";
			}
		}
		
		return returnCodes;
	}
	
	protected String validateDuplicateRecord(String[] data){
		String errorCode=null;
		return errorCode;
	}
}
