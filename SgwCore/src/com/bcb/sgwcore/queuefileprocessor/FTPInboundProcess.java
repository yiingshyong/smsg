package com.bcb.sgwcore.queuefileprocessor;

import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SendMailBean;

public class FTPInboundProcess extends FTPProcess{
	public static String[] trimData(String[] data){
		for(int i=0;i<data.length;i++){
			data[i]=data[i].trim();
		}
		return data;
	}

	protected String validateMobileNo(String mobileNo){
		String returnCodes=null;
		
		if (mobileNo ==null||mobileNo.trim().length()==0 ){
			log.debug("return mobileNo 4: " + mobileNo);
			returnCodes="Empty mobile number(s)! ";
		}else{
			mobileNo = BusinessService.normalizeMobile(mobileNo);
			 
			if(!BusinessService.validateMobileNo(mobileNo)){
				log.debug( "Invalid number " + mobileNo);
				returnCodes= "Invalid mobile number! ";
			}else{
			}
		}		
		return returnCodes;
	}
	
	public String sendSuccessEmailNotification(Object setup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		BatchSetup theSetup = (BatchSetup) setup;
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Success";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Success"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;

		message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();

		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process success email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
	public String sendFailureEmailNotification(Object setup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		BatchSetup theSetup = (BatchSetup) setup;
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Failed";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Failed"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;
		
		if(totalRecords==0&&noOfFailed==0){
			message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();
		}
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process failure email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
}
