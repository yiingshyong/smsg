package com.bcb.sgwcore.queuefileprocessor;

import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SendMailBean;

public class FTPOutboundProcess extends FTPProcess{
	public String process(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg){
		log.debug("Process FTPOutboundProcess");
		try{
			String returnMsg=null;
			
			if(createOutBoundFiles(outboundFile, aSetup, reportModal, theCfg)){
			}else{
				returnMsg="Error in FTP outbound files";
			}
			log.debug("returnMsg: "+returnMsg);
			
			if(returnMsg!=null){
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			
			if(CommonUtils.NullChecker(reportModal.getRemark(), String.class).toString().length()>0){
				if(!getRemarks().contains(reportModal.getRemark())){
					getRemarks().add(reportModal.getRemark());
				}
			}
			
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	public boolean createOutBoundFiles(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg) throws Exception{
		boolean status=true;
		return status;
	}
	
	public String sendSuccessEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendSuccessEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendSuccessEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Success";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Success"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE;
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process success email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
	public String sendFailureEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendFailureEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendFailureEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Failed";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Failed"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE;
		
		if(totalRecords==0&&noOfFailed==0){
			message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();
		}
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process failure email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
}
