package com.bcb.sgwcore.queuefileprocessor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.NotificationService;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.jcraft.jsch.ChannelSftp;

public class FTPProcess implements ILineTemplate{
	static Log log = LogManager.getLog();
	FileConversion fileConversion;
	ArrayList<String> remarks=new ArrayList<String>();
	ArrayList<String>[] duplicateRecordControl=new ArrayList[]{new ArrayList<String>(), new ArrayList<String>()};// mobile no., template code
	ArrayList<String> duplicateRecord=new ArrayList<String>();

	public String process(File outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String process(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception {
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendSuccessEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendFailureEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendLimitationEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendSuccessSmsNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=null;
		return returnCodes;
	}

	public String sendFailureSmsNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		
		BatchSetup setup = (BatchSetup) theSetup;
		SmsCfg theCfg = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
		
		SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
		String message="Processing FTP-ed file: "+CommonUtils.NullChecker(setup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
		"Status: Failed/Error"+Constants.SPECIAL_CHAR_NEWLINE+
		"Status Time: "+theFormatter.format(new Date())+Constants.SPECIAL_CHAR_NEWLINE;

		//		String message = "FTP file Processing: "+CommonUtils.NullChecker(setup.getFileName(), String.class).toString()+" - Failed";
//		String message="FTP file: "+CommonUtils.NullChecker(setup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
//			"File Process Status: Failed"+Constants.SPECIAL_CHAR_NEWLINE+
//			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
//			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
//			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
//			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;
		
//		if(totalRecords==0&&noOfFailed==0){
//			message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
//				theReportModal.getRemark();
//		}
		
//		if(theReportModal != null && theReportModal.getRemark() != null && theReportModal.getRemark().trim().length() > 0){
//			message += Constants.SPECIAL_CHAR_NEWLINE + "Remarks: " + theReportModal.getRemark().trim();
//		}
		
		String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
		for(String receiver : toList){
			NotificationService.sendNotificationSms(message, theCfg.getFtpFailureNotificationSmsSender(), "FtpProcessor", receiver, theCfg);
		}
		
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendLimitationSmsNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=null;
		return returnCodes;
	}
	
	public String sendSuccessNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;
		String tempReturnCodes=Constants.STR_EMPTY;
		
		returnCodes+=sendSuccessEmailNotification(theSetup, theReportModal, from, to, totalRecords, noOfFailed);
		tempReturnCodes+=sendSuccessSmsNotification(theSetup, theReportModal, from, to, totalRecords, noOfFailed);
		returnCodes+=(tempReturnCodes.length()==0?Constants.STR_EMPTY:returnCodes.length()==0?tempReturnCodes:Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE+tempReturnCodes);
		
		return returnCodes;
	}
	
	public String sendFailureNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;
		String tempReturnCodes=Constants.STR_EMPTY;
		
		returnCodes+=sendFailureEmailNotification(theSetup, theReportModal, from, to, totalRecords, noOfFailed);
		tempReturnCodes+=sendFailureSmsNotification(theSetup, theReportModal, from, to, totalRecords, noOfFailed);
		returnCodes+=(tempReturnCodes.length()==0?Constants.STR_EMPTY:returnCodes.length()==0?tempReturnCodes:Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE+tempReturnCodes);
		
		return returnCodes;
	}
	
	public String sendLimitationNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, Integer totalRecords, Integer noOfFailed){
		String returnCodes=Constants.STR_EMPTY;
		return returnCodes;
	}
	
	public FileConversion getFileConversion() {
		return fileConversion;
	}

	public void setFileConversion(FileConversion fileConversion) {
		this.fileConversion = fileConversion;
	}
	
	public ArrayList<String> getRemarks() {
		return remarks;
	}

	public void setRemarks(ArrayList<String> remarks) {
		this.remarks = remarks;
	}
	
	public ArrayList<String> getDuplicateRecord() {
		return duplicateRecord;
	}

	public void setDuplicateRecord(ArrayList<String> duplicateRecord) {
		this.duplicateRecord = duplicateRecord;
	}

	public ArrayList[] getDuplicateRecordControl() {
		return duplicateRecordControl;
	}

	public void setDuplicateRecordControl(ArrayList[] duplicateRecordControl) {
		this.duplicateRecordControl = duplicateRecordControl;
	}

	@Override
	public void interceptFTPProcess(ChannelSftp ftpChannel,BatchSetup setup) {
	}

	@Override
	public String postTemplateProcess() {
		return null;
	}
		
}
