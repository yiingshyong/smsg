package com.bcb.sgwcore.queuefileprocessor;

import java.io.File;

public class FileObj{
	File theFile;
	
	public FileObj(String filename){
		theFile=new File(filename);
	}

	public File getTheFile() {
		return theFile;
	}

	public void setTheFile(File theFile) {
		this.theFile = theFile;
	}
}
