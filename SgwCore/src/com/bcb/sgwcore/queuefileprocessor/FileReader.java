package com.bcb.sgwcore.queuefileprocessor;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.dao.GroupDao;
import com.bcb.crsmsg.dao.IBatchSetupDao;
import com.bcb.crsmsg.dao.ISmsCfgDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.IUserDao;
import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.bcb.sgwcore.util.FileUtil;
import com.bcb.sgwcore.util.SendMailBean;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

public class FileReader implements Runnable {
	
	Log log = LogManager.getLog();
	List<Object> fcList;
	FileConversion fileConversion = null;
	FtpSetup modal;
	boolean isRunning;
	private String dir;
	private String endWith[];
	private List<ConversionThread> conversionThreadList;
	private int totalConversionThread;
	public static String tableListDelimiter;
	
	private LinkedList files = new LinkedList();
	FileReader(){}
	public FileReader(FileConversion fileConversion) {
		log.debug("Initialize File Reader");
		log.debug("File Conversion used for initialize File Reader: " + fileConversion.getConversionName());
		this.fileConversion = fileConversion;
		
		configureParam();
	}
	
	private void configureParam() {
		log.debug(" File Reader.configureParam()");
		
		log.debug("configureParam() - get Local Folder DIR");
		dir = fileConversion.getLocalFolder();
		log.debug("dir: "+dir);
		if (dir.indexOf(":") > 0) {
			// windows
			//dir = dir.replaceAll("/", FileUtil.getFileSeparator());
			dir.replace("/", FileUtil.getFileSeparator());
		}
		log.debug("dir: "+dir);
		
		log.debug("configureParam() - get End With List");
		
		List endWithList = new ArrayList();
		SmsCfg smsCfg=DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		tableListDelimiter=smsCfg.getTableListDelimiter();
		
		StringTokenizer st = new StringTokenizer(fileConversion
				.getEndWithFile(), tableListDelimiter); // Get all the FTP file support file type from a list field
		
		while (st.hasMoreTokens()) {
			endWithList.add(st.nextToken().trim());
		}
		endWith = new String[endWithList.size()];
		endWithList.toArray(endWith);
		
		totalConversionThread = fileConversion.getTotalConversionThread();
		log.debug("totalConversionThread: "+totalConversionThread);
		
		/* don't want minimum thread, controlled in DB - this is to control so that outbound FTP won't start as thread
		 if(totalConversionThread<1)
		 {
		 totalConversionThread =2; // Minimum conversion thread set to 2
		 }*/
		log.debug("totalConversionThread: "+totalConversionThread);
	}
	
	public void run() {
		log.info(fileConversion.getConversionName() + " is running up");
		isRunning = true;
		testFolder();
		setupConversionThread(); // Setup & run file conversion for FTP files
		while (isRunning) {
			try {
				//Declare Dao
				ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");// Keep checking for cfg
				SmsCfg theCfg = cfgDao.findByCfgId(Constants.APPSVERSION);// Keep checking for new FtpSetup
				
				IBatchSetupDao ftpDao = (IBatchSetupDao) SgwCoreHibernateUtil.getBean("ftpSetupDao");// Keep checking for new FtpSetup
				fcList = ftpDao.findSetup(fileConversion.getId());// Keep checking for new FtpSetup
				log.debug("fcList size of FTP Setup: "+fcList.size());
				
				for(int i=0;i<fcList.size();i++){// loop throught ftpsetup list
					modal =(FtpSetup) fcList.get(i);
					if (modal.getSetupStatus().equals(Constants.STATUS_ACTIVE)){// ftp enabled
						performFtpProcess(modal, theCfg);
						ftpDao.update(modal);
					}else{// ftp disabled, do nothing
					}
				}
				populatedLocalFiles();
			} catch (Exception e) {
				log.info(e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sleepAWhile();
		}
		log.info(fileConversion.getConversionName() + " is shutdown");
	}
	
	public void performFtpProcess(FtpSetup modal, SmsCfg theCfg) throws Exception{
		log.debug("PerformFtpProcess");
		ArrayList<String> remarks=new ArrayList<String>();// keep track any ftp errors
		
		boolean reachFtpTime=checkValidateFTPTime(modal);
		boolean retry=isFtpAutoRetry(remarks, theCfg, modal);
		log.debug("reachFtpTime: "+reachFtpTime);
		log.debug("retry: "+retry);
		
		if(reachFtpTime||retry){// reach ftp time or should auto retry
			DefaultDataConversion dataProcess = new DefaultDataConversion(fileConversion);
			FtpReport reportModal = new FtpReport();
			
			if(runProcess(theCfg, dataProcess, reportModal)){// process success
				log.debug("process success");
				if(runFtp(remarks, theCfg, dataProcess, reportModal)){	// ftp success
					log.debug("ftp success");
					modal.setFtpRetryStatus(Constants.STATUS_NO);

					if(runBackup(remarks, theCfg, dataProcess, reportModal, modal)){// backup file success
						log.debug("backup success");
					}else{// backup file failed
						log.debug("backup failed");
						sendFailureNotification(remarks, theCfg);
					}
				}else{// ftp failed
					log.error("ftp failed");
//					for(String remark : remarks){
//						log.error("Error during ftp: " + remark);
//					}
					checkFtpAutoRetry(remarks, theCfg, dataProcess, reportModal, modal);
					log.debug("status: "+modal.getFtpRetryStatus());
				}
			}else{// process failed				
				log.error("ftp process failed");
				checkFtpAutoRetry(remarks, theCfg, dataProcess, reportModal, modal);
			}
			
			if(reachFtpTime){
				log.debug("update ftp last time: "+new Date());
				modal.setFtpLastDatetime(new Date());
			}
		}else{// not running ftp
		}
		log.debug("End PerformFtpProcess");
	}
	
	public void checkFtpAutoRetry(ArrayList<String> remarks, SmsCfg theCfg, DefaultDataConversion dataProcess, FtpReport reportModal, FtpSetup theSetup) throws Exception{
		if(CommonUtils.NullChecker(modal.getFtpRetryNo(), String.class).toString().length()>0){// activate auto retry
			modal.setFtpRetryStatus(Constants.STATUS_YES);
		}else{//auto retry not available
			modal.setFtpRetryStatus(Constants.STATUS_NO);
			
			if(runBackup(remarks, theCfg, dataProcess, reportModal, theSetup)){// backup file success
			}else{// backup file failed
			}
			
			sendFailureNotification(remarks, theCfg);
		}
		log.debug("status: "+modal.getFtpRetryStatus());
	}
	public static void main(String[] args) throws Exception{
		System.out.println("home:" + System.getProperty("sgw.home"));
		FileReader r = new FileReader();
		SmsCfg theCfg = new SmsCfg();
		FtpSetup theSetup = new FtpSetup();		
		theSetup.setFtpRetryStatus(Constants.STATUS_YES);
		theSetup.setFtpRetryPeriod("2");
		theSetup.setFtpRetryNo("5");
		theSetup.setFtpLastDatetime(new SimpleDateFormat("ddMMyyyy HHmm").parse("27122010 1832"));
		System.out.println("last ftp: " + theSetup.getFtpLastDatetime());
		boolean result = r.isFtpAutoRetry(new ArrayList<String>(), theCfg, theSetup);
		System.out.println("result:" + result);
	}
	public boolean isFtpAutoRetry(ArrayList<String> remarks, SmsCfg theCfg, FtpSetup theSetup){
		boolean status=false;
		log.debug("retry status:"+theSetup.getFtpRetryStatus());
		if(CommonUtils.NullChecker(theSetup.getFtpRetryStatus(), String.class).toString().equals(Constants.STATUS_YES)){// retry status is yes
			String retryPeriod=Constants.VALUE_1;// minimum retry period is 1 minute
			log.debug("theSetup.getFtpRetryPeriod(): "+theSetup.getFtpRetryPeriod());
			if(CommonUtils.NullChecker(theSetup.getFtpRetryPeriod(), String.class).toString().length()>0){// got period
				retryPeriod=theSetup.getFtpRetryPeriod();
			}else{// no period, max retry 1 time
				retryPeriod=Constants.VALUE_1;
			}
			log.debug("retryPeriod: "+retryPeriod);
			Date maxAllowedAutoRetryTime=new Date((theSetup.getFtpLastDatetime().getTime()+Long.valueOf(theSetup.getFtpRetryNo())*Long.valueOf(retryPeriod)*1000*60)/60000);
			Date now=new Date(new Date().getTime()/60000);
			log.debug("max time: "+maxAllowedAutoRetryTime.getTime());
			log.debug("now: "+now.getTime());
			log.debug("compare: "+maxAllowedAutoRetryTime.compareTo(now));
			if(maxAllowedAutoRetryTime.compareTo(now)>=0){// the max allowed retry time is valid
				Long diffTime=(now.getTime()-theSetup.getFtpLastDatetime().getTime()/60000);
				log.debug("last ftp time: "+theSetup.getFtpLastDatetime().getTime());
				log.debug("diffTime: "+diffTime);
				if(diffTime>0){
					log.debug("minutes: "+diffTime%Long.valueOf(retryPeriod));
					if(diffTime%Long.valueOf(retryPeriod)==0){
						status=true;
					}else{
						status=false;
					}
				}else{// not time to retry
					status=false;
				}
			}else{// exceeded retry time allowed
				status=false;
				modal.setFtpRetryStatus(Constants.STATUS_NO);
				sendFailureNotification(remarks, theCfg);
			}
			log.debug("status: "+status);
		}else{// should not retry
			status=false;
		}
		
		return status;
	}
	
	public boolean runProcess(SmsCfg theCfg, DefaultDataConversion dataProcess, FtpReport reportModal){
		boolean status=true;
		
		FileObj outputFile=new FileObj(fileConversion.getLocalFolder()+File.separator + modal.getFileName());
		
		if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_INBOUND)){
		}else if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_OUTBOUND)){
			log.debug("Process outbound FTP");
			if(dataProcess.processAll(outputFile, modal, reportModal, theCfg)==null){
			}else{// error in process
				status=false;
			}
		}else{// Not valid FTP Mode
			status=false;
		}
		
		return status;
	}
	
	public boolean runFtp(ArrayList<String> remarks, SmsCfg theCfg, DefaultDataConversion dataProcess, FtpReport reportModal) throws Exception{
		if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_INBOUND)){
			remarks.addAll(executeInboundSftp(fileConversion, modal, dataProcess.getTemplateProcess()));
		}else if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_OUTBOUND)){
			log.debug("Process outbound FTP");
			FileObj outputFile=new FileObj(fileConversion.getLocalFolder()+File.separator + reportModal.getFileName());
			
			remarks.addAll(executeOutboundSftp(outputFile.getTheFile(), fileConversion, modal, dataProcess.getTemplateProcess()));
		}else{// Not valid FTP Mode
		}
		return remarks.size()==0?true:false;
	}
	
	public boolean runBackup(ArrayList<String> remarks, SmsCfg theCfg, DefaultDataConversion dataProcess, FtpReport reportModal, BatchSetup setup) throws Exception{
		boolean status=true;
		if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_INBOUND)){
		}else if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_OUTBOUND)){
			log.debug("Process outbound FTP");
			FileObj outputFile=new FileObj(fileConversion.getLocalFolder()+File.separator + reportModal.getFileName());
			
			status=dataProcess.backupFiles(outputFile.getTheFile().getCanonicalPath(), reportModal, setup);
		}else{// Not valid FTP Mode
		}
		
		return status;
	}
	
	public void sendFailureNotification(ArrayList<String> remarks, SmsCfg theCfg){
		if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("E")){
			String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
			for(int i=0;i<toList.length;i++){
				sendFailureEmailNotification(modal, toList[i], remarks);
			}
		}
		if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("S")){
			String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
			for(int i=0;i<toList.length;i++){
				sendFailureSmsNotification(modal, toList[i], theCfg, remarks);
			}
		}
	}
	
	/*public void run() {
		log.info(fileConversion.getConversionName() + " is running up");
		isRunning = true;
		testFolder();
		setupConversionThread(); // Setup & run file conversion for FTP files
		while (isRunning) {
			IBatchSetupDao ftpDao;
			ISmsCfgDao cfgDao;
			try {
				cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");// Keep checking for cfg
				SmsCfg theCfg = cfgDao.findByCfgId(Constants.APPSVERSION);// Keep checking for new FtpSetup
				
				ftpDao = (IBatchSetupDao) SgwCoreHibernateUtil.getBean("ftpSetupDao");// Keep checking for new FtpSetup
				fcList = ftpDao.findSetup(fileConversion.getId());// Keep checking for new FtpSetup
				log.debug("fcList size of FTP Setup: "+fcList.size());
				if (fcList.size() > 0){
					Iterator iter = fcList.iterator();
					while (iter.hasNext()){// loop throught ftpsetup list
						modal =(FtpSetup) iter.next();
						if (modal.getSetupStatus().equals(Constants.STATUS_ACTIVE)){// ftp enabled
							ArrayList<String> remarks=new ArrayList<String>();// keep track any ftp errors
							
							if(checkValidateFTPTime(modal)){// reach ftp time
								if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_INBOUND)){
									remarks=executeInboundSftp(fileConversion, modal);
								}else if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_OUTBOUND)){
									DefaultDataConversion dataProcess = new DefaultDataConversion(fileConversion);
									FtpReport reportModal = new FtpReport();
									log.debug("Process outbound FTP");
									FileObj outputFile=new FileObj(fileConversion.getLocalFolder()+File.separator + modal.getFileName());
									
									if(dataProcess.processAll(outputFile, modal, reportModal, theCfg)==null){
										remarks=executeOutboundSftp(outputFile.getTheFile(), fileConversion, modal);
									}else{// error in process
									}
									
									if(remarks.size()==0||CommonUtils.NullChecker(modal.getFtpRetryNo(), String.class).toString().length()==0){
										dataProcess.backupFiles(outputFile.getTheFile().getCanonicalPath(), reportModal);
									}
								}else{// Not valid FTP Mode
								}
								
								if(remarks.size()>0&&CommonUtils.NullChecker(modal.getFtpRetryNo(), String.class).toString().length()==0){// contains error in FTP process
									if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("E")){
										String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
										for(int i=0;i<toList.length;i++){
											sendFailureEmailNotification(modal, toList[i], remarks);
										}
									}
									if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("S")){
										String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
										for(int i=0;i<toList.length;i++){
											sendFailureSmsNotification(modal, toList[i], theCfg, remarks);
										}
									}
								}
							}else if(checkRetry(modal)){// auto retry ftp process
								if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_INBOUND)){
									remarks=executeInboundSftp(fileConversion, modal);
								}else if(CommonUtils.NullChecker(modal.getFtpMode(), String.class).toString().equals(Constants.FORM_FIELD_FTP_MODE_OUTBOUND)){
									DefaultDataConversion dataProcess = new DefaultDataConversion(fileConversion);
									FtpReport reportModal = new FtpReport();
									log.debug("Process outbound FTP");
									FileObj outputFile=new FileObj(fileConversion.getLocalFolder()+File.separator + modal.getFileName());
									
									remarks=executeOutboundSftp(outputFile.getTheFile(), fileConversion, modal);
									if(remarks.size()==0||CommonUtils.NullChecker(modal.getFtpRetryStatus(), String.class).toString().equals(Constants.STATUS_NO)){
										dataProcess.backupFiles(outputFile.getTheFile().getCanonicalPath(), reportModal);
									}
								}else{// Not valid FTP Mode
								}
								
								if(remarks.size()>0&&CommonUtils.NullChecker(modal.getFtpRetryStatus(), String.class).equals(Constants.STATUS_NO)){// contains error in FTP process
									if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("E")){
										String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
										for(int i=0;i<toList.length;i++){
											sendFailureEmailNotification(modal, toList[i], remarks);
										}
									}
									if(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().contains("S")){
										String[] toList=CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
										for(int i=0;i<toList.length;i++){
											sendFailureSmsNotification(modal, toList[i], theCfg, remarks);
										}
									}
								}
							}
						}else{// ftp disabled, do nothing
						}
					}
				}
				populatedLocalFiles();
			} catch (Exception e) {
				log.info(e);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sleepAWhile();
		}
		log.info(fileConversion.getConversionName() + " is shutdown");
	}*/
	
	public boolean checkFtpRetry(FtpSetup aSetup){
		return false;
	}
	
	public void sendFailureEmailNotification(FtpSetup theSetup, String to, ArrayList<String> remarks){
		SendMailBean theMail=new SendMailBean();
		
		String subject="FTP file Staging Retrieve Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Failed/Error";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
		"FTP Process Status: Failed/Error"+Constants.SPECIAL_CHAR_NEWLINE+
		"Process Failed/Error Time: "+new Date()+Constants.SPECIAL_CHAR_NEWLINE;
		
		for(int i=0;i<remarks.size();i++){
			if(i==0){
				message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE;
			}
			message+=remarks.get(i)+Constants.SPECIAL_CHAR_NEWLINE;
		}
		
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(theSetup.getNotificationSetupSender(),String.class).toString().length()>0?theSetup.getNotificationSetupSender():Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		theMail.send();
	}
	
	public void sendFailureSmsNotification(FtpSetup theSetup, String to, SmsCfg theCfg, ArrayList<String> remarks){
		try{
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
			String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: Failed/Error"+Constants.SPECIAL_CHAR_NEWLINE+
			"Status Time: "+theFormatter.format(new Date())+Constants.SPECIAL_CHAR_NEWLINE;

			for(int i=0;i<remarks.size();i++){
				if(i==0){
					message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE;
				}
				message+=remarks.get(i)+Constants.SPECIAL_CHAR_NEWLINE;
			}
			
			int deptId=0;
			String dept=Constants.STR_EMPTY;
			IUserDao userDao;
			userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
			User usermodal = userDao.findByUserId(theCfg.getFtpFailureNotificationSmsSender());
			if (usermodal == null){
			}else{
				GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
				Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
				if (groupmodal != null){
					deptId=groupmodal.getId();
					dept=groupmodal.getDeptName();
				}
			}
			
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			SmsQueue aSms=new SmsQueue();
			aSms.setCreatedBy(theCfg.getFtpFailureNotificationSmsSender());
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			aSms.setTotalSms(msgArray.size());
			aSms.setMobileNo(to);
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_4);
			aSms.setSmsStatusBy("FileReader");
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(theCfg.getFtpFailureNotificationSmsSender());
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
			smsDao.insert(aSms);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public FileConversion getFileConversion()
	{
		return this.fileConversion;
	}
	
	private void setupConversionThread() {
		log.debug("Setup Conversion Thread");
		conversionThreadList = new ArrayList<ConversionThread>();
		log.debug("totalConversionThread: "+totalConversionThread);
		for(int i=0;i<totalConversionThread;i++){// Created numbers of conversion thread according to defined in file conversion record, min 2
			ConversionThread conversionThread = new ConversionThread(this);
			Thread tt = new Thread(conversionThread);
			tt.setName("ct|"+fileConversion.getConversionName()+"|"+i);
			tt.start();
			conversionThreadList.add(conversionThread);
		}
	}
	
	private void testFolder() {
		log.debug("Enter testfolder()");
		String testFile = dir + FileUtil.getFileSeparator() + "test.txt";
		log.debug("testFile: "+testFile);
		FileUtil.createFolder(testFile);
	}
	
	private synchronized void addFile(String filename) {
		files.addLast(filename);
	}
	
	public synchronized String removeFirst() {
		try {
			if (files != null && files.size() > 0) {
				return (String) files.removeFirst();
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}
	
	private void populatedLocalFiles() {
		log.debug("populatedLocalFiles");
		try {
			List<String> files = new ArrayList<String>();
			FileUtil.getAllFiles(files, new File(dir), endWith);
			log.debug("dir: "+dir);
			log.debug("endWith: "+endWith);
			log.debug("file size: "+files.size());
			for (String filename: files) {
				addFile(filename);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	private void sleepAWhile() {
		
		try {
			Thread.yield();
			Thread.sleep(60000); // Sleep 60 seconds
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void shutdown() {
		isRunning = false;
		
		shutdownConversionThread();
		
	}
	
	private void shutdownConversionThread() {
		for (ConversionThread thread : conversionThreadList) {
			thread.shutdown();
		}
		
	}
	
	private boolean checkValidateFTPTime(FtpSetup theSetup){
		if(theSetup.getFtpTimeMode().equals(Constants.FORM_FIELD_TIME_MODE_MONTHLY)){
			return checkValidDate(modal.getFtpMonth(), modal.getFtpDayOfMonth(), modal.getFtpTime());
		}else if(theSetup.getFtpTimeMode().equals(Constants.FORM_FIELD_TIME_MODE_WEEKLY)){
			return checkValidDay(modal.getFtpDay(), modal.getFtpTime());
		}else if(theSetup.getFtpTimeMode().equals(Constants.FORM_FIELD_TIME_MODE_HOURLY)){
			return checkValidHour(modal.getFtpHour(), modal.getFtpHourFrom(), modal.getFtpHourTo());
		}else{
			return false;
		}
	}
	
	private boolean checkValidDate(String ftpMonth, String ftpDayOfMonth, String ftpTime){
		log.debug("checkValidDate - check if reach FTP month, date and time");
		
		String[] theFtpMonths = ftpMonth.split("["+tableListDelimiter+"]");
		Calendar c = Calendar.getInstance();
		
		String currMonth=Integer.toString(c.get(Calendar.MONTH)); // Get this month
		String currDayOfMonth=Integer.toString(c.get(Calendar.DAY_OF_MONTH)); // Get this day of month
		
		String minutes = Constants.STR_EMPTY;
		String hourDay = Constants.STR_EMPTY;
		
		if (c.get(Calendar.MINUTE) < 10){
			minutes = "0" +  String.valueOf(c.get(Calendar.MINUTE));
		}else{
			minutes = String.valueOf(c.get(Calendar.MINUTE));
		}
		
		if (c.get(Calendar.HOUR_OF_DAY) < 10){
			hourDay = "0" +  String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}else{
			hourDay = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}
		String currTime = hourDay+minutes; // Get time now
		
		log.debug("currMonth: "+currMonth);
		log.debug("currDayOfMonth: "+currDayOfMonth);
		log.debug( "currTime: " + currTime);
		
		log.debug("ftpMonth: "+ftpMonth);
		log.debug("ftpDayOfMonth: "+ftpDayOfMonth);
		log.debug( "ftpTime: " + ftpTime);
		
		for (int i =0; i < theFtpMonths.length; i++){// For each ftp month
			log.info("theFtpMonths: "+theFtpMonths[i]);
			if (currMonth.equals(theFtpMonths[i])){
				log.info( " fall in month " + currMonth);
				if(currDayOfMonth.equals(ftpDayOfMonth)){
					log.info( " fall in day of month " + currDayOfMonth);
					if (currTime.equals(ftpTime)){
						log.info( " fall in time " + currTime);
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private boolean checkValidHour(String ftpHour, String ftpHourFrom, String ftpHourTo){
		log.debug("checkValidHour - check if reach hour and time");
		Calendar c = Calendar.getInstance();
		
		String minutes = Constants.STR_EMPTY;
		String hourDay = Constants.STR_EMPTY;
		
		if (c.get(Calendar.MINUTE) < 10){
			minutes = "0" +  String.valueOf(c.get(Calendar.MINUTE));
		}else{
			minutes = String.valueOf(c.get(Calendar.MINUTE));
		}
		
		if (c.get(Calendar.HOUR_OF_DAY) < 10){
			hourDay = "0" +  String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}else{
			hourDay = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}
		String currTime = hourDay+minutes; // Get time now
		
		log.info( "currTime: " + currTime);
		
		ArrayList<String> ftpTime=new ArrayList<String>();
		
		int ftpHourInt=Integer.valueOf(ftpHour)*100; // Make it as hourly
		int ftpHourFromInt=Integer.valueOf(ftpHourFrom);
		int ftpHourToInt=Integer.valueOf(ftpHourTo);
		
		if(ftpHourFromInt>ftpHourToInt){// If hour to is until next day
			ftpHourToInt+=2400;// extend one day
		}
		
		int ftpHourly=ftpHourFromInt;
		String ftpHourlyStr;
		while(ftpHourly<=ftpHourToInt){
			ftpHourlyStr=Integer.toString(ftpHourly);
			if(ftpHourlyStr.length()<4){
				ftpHourlyStr="0"+ftpHourlyStr;
			}
			ftpTime.add(ftpHourlyStr);
			log.debug("Hourly FTP time: "+ftpHourlyStr);
			ftpHourly+=ftpHourInt;
		}
		
		for (int i =0; i < ftpTime.size(); i++){// For each ftp hour
			log.debug("theFtpHourly: "+ftpTime.get(i));
			if (currTime.equals(ftpTime.get(i))){
				log.info( " fall in time " + currTime);
				return true;
			}
		}
		
		return false;
	}
	
	private boolean checkValidDay(String days, String ftpTime){
		log.debug("checkValid Day - check if reach FTP day and time");
		
		String startDay[] = days.split("["+tableListDelimiter+"]");
		//String startDay[] = days.split(((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION))).getTableListDelimiter());
		Calendar c = Calendar.getInstance();
		String minutes = Constants.STR_EMPTY;
		String hourDay = Constants.STR_EMPTY;
		String day_in_week = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
		log.debug("day_in_week; "+day_in_week);
		if (c.get(Calendar.MINUTE) < 10){
			minutes = "0" +  String.valueOf(c.get(Calendar.MINUTE));
		}else{
			minutes = String.valueOf(c.get(Calendar.MINUTE));
		}
		
		if (c.get(Calendar.HOUR_OF_DAY) < 10){
			hourDay = "0" +  String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}else{
			hourDay = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
		}
		
		String currTime = hourDay+minutes;
		
		log.info( "currTime: " + currTime);
		log.info( "ftpTime: " + ftpTime);
		for (int i =0; i < startDay.length; i++){
			if (day_in_week.equals(startDay[i])){
				log.info( " fall in day " + day_in_week);
				if (currTime.equals(ftpTime)){
					log.info( " fall in time " + currTime);
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns a Sftp session conncted using the Jsch library.
	 */
	public static Session connectSFTP(final String host, final String user,
			final String pass) throws JSchException {
		JSch jsch = new JSch();
		Session session = jsch.getSession(user, host, 22);
		session.setUserInfo(new UserInfo() {
			public String getPassphrase() {
				return null;
			}
			public String getPassword() {
				return null;
			}
			public boolean promptPassphrase(String string) {
				return false;
			}
			public boolean promptPassword(String string) {
				return false;
			}
			public boolean promptYesNo(String string) {
				return true;
			}
			public void showMessage(String string) {
			}
		});
		
		session.setPassword(pass);
		session.connect();
		return session;
	}
	
	private ArrayList<String> executeInboundSftp(FileConversion fileConversion, FtpSetup modal, ILineTemplate templateProcess) throws JSchException, SftpException { 
		log.debug("Running FTP Process to retrieve files");
		JSch jSch = new JSch();        
		final byte[] prvkey;
		ArrayList<String> remarks=new ArrayList<String>();
		
		try {
			if(modal.getSecurityKey()!=null){
				File securityKey=new File(modal.getSecurityKey());
				if(securityKey.exists()){
					prvkey = getBytesFromFile(securityKey);
				}else{
					prvkey = new byte[0];
				}
			}else
				prvkey = new byte[0];
//			Private key must be byte array       
			//final byte[] emptyPassPhrase = new byte[0]; // Empty passphrase for now, get real passphrase from MyUserInfo  
			log.debug("modal.getFtpUserName(): "+modal.getFtpUserName());
			log.debug("prvkey: "+prvkey);
			jSch.addIdentity(modal.getFtpUserName(),    // String userName            
					prvkey,          // byte[] privateKey             
					null,            // byte[] publicKey            
					new byte[0]  // byte[] passPhrase       
			);    
			log.debug("[executeSftp] use file:" + modal.getSecurityKey());	
			log.debug("[executeSftp] username:" + modal.getFtpUserName());
			log.debug("[executeSftp] getFtpServer:" + modal.getFtpServer());
			
			String sftpPort=(modal.getFtpPort()==null)?Constants.DEFAULT_FTP_PORT:modal.getFtpPort();
			
			Session session = jSch.getSession(modal.getFtpUserName(), modal.getFtpServer(), Integer.valueOf(sftpPort));
			
			UserInfo ui=new MyUserInfo(); // MyUserInfo implements UserInfo        
			session.setUserInfo(ui);        
			session.connect();        
			Channel channel = session.openChannel("sftp");        
			ChannelSftp sftp = (ChannelSftp) channel;        
			sftp.connect();        
			log.debug("[executeSftp] Successfully connected");
			File fo = null;
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			String remoteFileName = modal.getFtpSourceDir() + File.separator + modal.getFileName(); 
			sftp.get( remoteFileName, outputStream);
			log.debug("[executeSftp] Start get filename():" + modal.getFileName());
			BufferedWriter out = null;
			try {
				fo = new File(fileConversion.getLocalFolder()+File.separator + modal.getFileName());
				FileOutputStream fos = new FileOutputStream(fo);
				out = new BufferedWriter(new OutputStreamWriter(fos));						
				out.write(outputStream.toString());
				out.flush();
				log.debug("end copying file " + modal.getFileName());				
				templateProcess.interceptFTPProcess(sftp, modal);
				if(modal.isRemoveFTPSrcFile()){
					sftp.rm(remoteFileName);
				}
			} catch (FileNotFoundException fe) {
				log.error("Error", fe);
				remarks.add("File not found from staging: "+modal.getFileName());
				throw fe;
			} catch (IOException e) {
				remarks.add("File from staging IO problem: "+modal.getFileName());
				log.error("Error", e);
				throw e;
			} finally {
				try {
					if (out != null) out.close();
				} catch (IOException ex) {
					remarks.add("File from staging IO problem: "+modal.getFileName());
				}
			}
			if(fo == null){
				remarks.add("'" + modal.getFileName() + "' was not found in server.");
			}
			sftp.disconnect();        
			session.disconnect();   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			remarks.add("Connection failed established to staging server "+modal.getFtpServer());
			remarks.add("Details: "+e.getMessage());
			log.debug("identity name: "+jSch.getIdentityNames());
			
			e.printStackTrace();
		} 
		
		return remarks;
	}
	
	private ArrayList<String> executeOutboundSftp(File outboundFile, FileConversion fileConversion, FtpSetup modal, ILineTemplate templateProcess) throws JSchException, SftpException { 
		log.debug("Running FTP Process to output files");
		JSch jSch = new JSch();        
		final byte[] prvkey;
		ArrayList<String> remarks=new ArrayList<String>();
		
		try {
			if(modal.getSecurityKey()!=null){
				File securityKey=new File(modal.getSecurityKey());
				if(securityKey.exists()){
					prvkey = getBytesFromFile(securityKey);
				}else{
					prvkey = new byte[0];
				}
			}else
				prvkey = new byte[0];
//			Private key must be byte array       
			//final byte[] emptyPassPhrase = new byte[0]; // Empty passphrase for now, get real passphrase from MyUserInfo  
			log.debug("modal.getFtpUserName(): "+modal.getFtpUserName());
			log.debug("prvkey: "+prvkey);
			jSch.addIdentity(modal.getFtpUserName(),    // String userName            
					prvkey,          // byte[] privateKey             
					null,            // byte[] publicKey            
					new byte[0]  // byte[] passPhrase       
			);    
			log.debug("[executeSftp] use file:" + modal.getSecurityKey());	
			log.debug("[executeSftp] username:" + modal.getFtpUserName());
			log.debug("[executeSftp] getFtpServer:" + modal.getFtpServer());
			
			String sftpPort=(modal.getFtpPort()==null)?Constants.DEFAULT_FTP_PORT:modal.getFtpPort();
			
			Session session = jSch.getSession(modal.getFtpUserName(), modal.getFtpServer(), Integer.valueOf(sftpPort));
			
			UserInfo ui=new MyUserInfo(); // MyUserInfo implements UserInfo        
			session.setUserInfo(ui);        
			session.connect();        
			Channel channel = session.openChannel("sftp");        
			ChannelSftp sftp = (ChannelSftp) channel;        
			sftp.connect();        
			log.debug("[executeSftp] Successfully connected");
			
			String dst = "";				
			if(modal.getFtpSourceDir() != null && modal.getFtpSourceDir().trim().length() > 0){
//				sftp.cd(modal.getFtpSourceDir().trim());
				dst = modal.getFtpSourceDir().trim() + File.separator;
				System.out.println("outbound dst: " + dst);
			}
			final Vector files = sftp.ls("."); 
			for (Iterator iter = files.iterator(); iter.hasNext();) {
				final ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) iter.next();
				
				if(modal.getFileName().split("[.]", -1).length>=2){
					String filename=modal.getFileName().split("[.]", -1)[0];
					String extension=modal.getFileName().split("[.]", -1)[1];
					if (entry.getFilename().contains(filename)&&
							entry.getFilename().contains(extension)){
						sftp.rm(entry.getFilename());
					}
				}else{
				}
			}
			
			FileInputStream fis=null;
			try{
				log.debug("FTP file: "+outboundFile.getAbsolutePath());
				if(outboundFile.exists()){					
					fis = new FileInputStream(outboundFile);
					sftp.put(fis, dst + outboundFile.getName());
				}else{
					remarks.add("File not found from local ftp folder: "+modal.getFileName());
				}
				templateProcess.interceptFTPProcess(sftp, modal);
			}catch (FileNotFoundException fe) {
				log.error("Error", fe);
				remarks.add("File not found from local ftp folder: "+modal.getFileName());
				throw fe;
			} finally {
				try {
					if (fis != null) fis.close();
				} catch (IOException ex) {
					remarks.add("File to staging IO problem: "+modal.getFileName());
				}
			}
			
			sftp.disconnect();        
			session.disconnect();   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			remarks.add("Connection failed established to staging server "+modal.getFtpServer());
			remarks.add("Details: "+e.getMessage());
			log.debug("identity name: "+jSch.getIdentityNames());
			
			e.printStackTrace();
		} 
		
		return remarks;
	}
	
	private static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		System.out.println("\nDEBUG: FileInputStream is " + file);
		
		// Get the size of the file
		long length = file.length();
		System.out.println("DEBUG: Length of " + file + " is " + length + "\n");
		
		/*
		 * You cannot create an array using a long type. It needs to be an int
		 * type. Before converting to an int type, check to ensure that file is
		 * not loarger than Integer.MAX_VALUE;
		 */
		if (length > Integer.MAX_VALUE) {
			System.out.println("File is too large to process");
			return null;
		}
		
		// Create the byte array to hold the data
		byte[] bytes = new byte[(int)length];
		
		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while ( (offset < bytes.length)
				&&
				( (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) ) {
			
			offset += numRead;
			
		}
		
		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}
		
		is.close();
		return bytes;
		
	}
	
	
	public static class MyUserInfo implements UserInfo, UIKeyboardInteractive{
		String passwd;
		
		public String getPassword(){ return passwd; }
		public boolean promptYesNo(String str){
			return true;
		}
		public String getPassphrase() {
			// TODO Auto-generated method stub
			return null;
		}
		public boolean promptPassword(String arg0) {
			// TODO Auto-generated method stub
			return false;
		}
		public boolean promptPassphrase(String arg0) {
			// TODO Auto-generated method stub
			return false;
		}
		public void showMessage(String arg0) {
			// TODO Auto-generated method stub
			
		}
		public String[] promptKeyboardInteractive(String arg0, String arg1, String arg2, String[] arg3, boolean[] arg4) {
			// TODO Auto-generated method stub
			return null;
		}
	}
}
