package com.bcb.sgwcore.queuefileprocessor;

import java.util.List;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;


public interface IDataProcess {

	void processAll(String filename, Object aSetup, SmsCfg theCfg, List<Telco> theTelco);

}
