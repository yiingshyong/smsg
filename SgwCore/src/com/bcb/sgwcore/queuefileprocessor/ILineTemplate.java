package com.bcb.sgwcore.queuefileprocessor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.jcraft.jsch.ChannelSftp;

public interface ILineTemplate{
	public void setFileConversion(FileConversion fileConversion);
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate aTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception;
	public String process(File outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg);
	public String process(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg);
	public ArrayList<String> getRemarks();
	public void setRemarks(ArrayList<String> remarks);
	public String sendSuccessEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendFailureEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendLimitationEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendSuccessSmsNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendFailureSmsNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendLimitationSmsNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendSuccessNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendFailureNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed);
	public String sendLimitationNotification(Object theSetup, FtpReport theReportModal, SmsCfg theCfg, Integer totalRecords, Integer noOfFailed);
	public ArrayList<String> getDuplicateRecord() ;
	public void setDuplicateRecord(ArrayList<String> duplicateRecord);
	public ArrayList[] getDuplicateRecordControl();
	public void setDuplicateRecordControl(ArrayList[] duplicateRecordControl);
	public void interceptFTPProcess(ChannelSftp ftpChannel, BatchSetup setup);
	public String postTemplateProcess();
}
