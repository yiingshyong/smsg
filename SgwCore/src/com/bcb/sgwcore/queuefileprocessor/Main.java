package com.bcb.sgwcore.queuefileprocessor;

import org.apache.commons.logging.Log;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;
public class Main implements WrapperListener{
	
	MainConversionProcessor mcp = new MainConversionProcessor();  
	Log log = LogManager.getLog(); 
	

	public static void main(String args[])
	{	System.out.println("Startup Wrapper");
		WrapperManager.start(new Main(), args);
	}

	public void controlEvent(int event)
	{
		
		 if (WrapperManager.isControlledByNativeWrapper()) { 
		 } else { 
			 if ((event ==
		  WrapperManager.WRAPPER_CTRL_C_EVENT) || (event ==
		  WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event ==
		 WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){ WrapperManager.stop(0); } }
		
	}

	public Integer start(String[] param){
		log.debug("main conversion processor start");
		mcp.start();
		return null;
	}

	public int stop(int exitcode){
		log.debug("main conversion processor shut down");
		mcp.shutdown();
		return 0;
	}

}
