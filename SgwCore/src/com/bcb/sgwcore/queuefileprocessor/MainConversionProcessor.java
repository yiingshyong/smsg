package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.dao.IFileConversionDao;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;


public class MainConversionProcessor extends Thread {
	
	IFileConversionDao fileConversionDao=null;
	List<FileConversion> fcList;
	List<FileReader> fileReaderList;
	Log log = LogManager.getLog(); 
	
	public MainConversionProcessor()
	{
		log.debug("main conversion processor initialization");
		try {
			fileConversionDao = (IFileConversionDao) SgwCoreHibernateUtil.getBean("fileConversionDao");
			fcList = fileConversionDao.findAll(); // Get File Conversion records
			fileReaderList=new ArrayList<FileReader>();
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}
	
	
	public void run(){
		log.debug("main conversion processor run");
		
		if (fcList!=null&&fcList.size()>0)
		{
			log.debug("fcList.size(): "+fcList.size());
			for (FileConversion fc: fcList) { // One File Reader Thread created for each file conversion record
				FileReader fileReader = new FileReader(fc);
				Thread tt = new Thread(fileReader);
				tt.start();
				fileReaderList.add(fileReader);
			}
		}
	}

	public void shutdown() {
		log.debug("main conversion processor shut down");
		if(fileReaderList!=null){
			for (FileReader fileReader : fileReaderList) {
				fileReader.shutdown();
			}
		}
	}

}
