package com.bcb.sgwcore.queuefileprocessor;

import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SendMailBean;

public class PhoneSynchronisation extends DefaultSynchronisation{
	public PhoneSynchronisation(){super();}
	
	public String process(String line, FtpReport reportModal, String user, Object aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco) throws Exception {
		return process(line, reportModal, user, (FtpSetup)aSetup,theCfg, theTelco);
	}
	
	public String process(String line, FtpReport reportModal, String user, FtpSetup aSetup, SmsCfg theCfg, List<Telco> theTelco){	
		log.debug("Process Phone Synchronisation");
		try{
			log.debug("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]")); 
			log.debug("data length: "+data.length);

			String returnMsg = validation(data);// validate data
			log.debug("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
				if(synchronise(data, user, reportModal, aSetup, theCfg)){
				}else{
					returnMsg="Error in synchronising phone number! ";
				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	protected boolean synchronise(String[] data, String user, FtpReport reportModal, FtpSetup aSetup, SmsCfg theCfg){
		try{
			List custList=customerDao.findByCustRefNo(data[Integer.valueOf(Constants.SYNC_FIELD_CUST_REF_NO)]);
			if(custList.size()==0){
				Customer aCustomer=new Customer();
				aCustomer.setMobileNo(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.SYNC_FIELD_MOBILE_NO)]));
				aCustomer.setCustRefNo(data[Integer.valueOf(Constants.SYNC_FIELD_CUST_REF_NO)]);
				aCustomer.setSmsStatus(Constants.STATUS_SUBSCRIPTION);
				aCustomer.setSmsStatusBy("PhoneSync");
				aCustomer.setSmsStatusDatetime(new Date());
				aCustomer.setCreatedBy("PhoneSync");
				aCustomer.setCreatedDatetime(new Date());
				aCustomer.setTransferStatus(Constants.VALUE_0);				
				customerDao.insert(aCustomer);
			}else if(custList.size()==1){// Customer already exist, check if need to update hp no.
				Customer aCustomer=(Customer)custList.get(0);
				if(aCustomer.getMobileNo().equals(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.SYNC_FIELD_MOBILE_NO)]))){// Same mobile no., no need to do anything
				}else{// Not same hp no., update hp no.
					aCustomer.setMobileNo(BusinessService.normalizeMobile(data[Integer.valueOf(Constants.SYNC_FIELD_MOBILE_NO)]));
					aCustomer.setModifiedBy("PhoneSync");
					aCustomer.setModifiedDatetime(new Date());
					customerDao.update(aCustomer);
				}
			}else{// Duplicate custRefNo exist
				return false;
			}
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	public String sendSuccessEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendSuccessEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendSuccessEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Success";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Success"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;

		theMail.setP_to(to);
		theMail.setP_from(from);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process success email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
	public String sendFailureEmailNotification(Object theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		return sendFailureEmailNotification((FtpSetup)theSetup, theReportModal, from, to, totalRecords, noOfFailed);
	}
	
	public String sendFailureEmailNotification(FtpSetup theSetup, FtpReport theReportModal, String from, String to, Integer totalRecords, Integer noOfFailed){
		SendMailBean theMail=new SendMailBean();
		String subject="FTP file Process: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+" - Failed";
		String message="FTP file: "+CommonUtils.NullChecker(theSetup.getFileName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"File Process Status: Failed"+Constants.SPECIAL_CHAR_NEWLINE+
			"Process Start Time: "+theReportModal.getStartDate()+Constants.STR_SPACE+theReportModal.getStartTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Process End Time: "+theReportModal.getEndDate()+Constants.STR_SPACE+theReportModal.getEndTime()+Constants.SPECIAL_CHAR_NEWLINE+
			"Total No. of record(s) processed: "+totalRecords+Constants.SPECIAL_CHAR_NEWLINE+
			"No. of failed record(s): "+noOfFailed+Constants.SPECIAL_CHAR_NEWLINE;
		
		if(totalRecords==0&&noOfFailed==0){
			message+="Remarks: "+Constants.SPECIAL_CHAR_NEWLINE+
				theReportModal.getRemark();
		}
		
		theMail.setP_to(to);
		theMail.setP_from(from);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			return null;
		}else{
			return "FTP file process failure email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
	}
	
	protected String validateDuplicateRecord(String[] data){
		String errorCode=null;
		String custRefNo=data[Integer.valueOf(Constants.SYNC_FIELD_CUST_REF_NO)];
		String mobileNo=data[Integer.valueOf(Constants.SYNC_FIELD_MOBILE_NO)];
		
		for(int i=0;i<duplicateRecordControl[0].size();i++){
			if(custRefNo.equals(duplicateRecordControl[0].get(i))&&!mobileNo.equals(duplicateRecordControl[1].get(i))){
				errorCode="Duplicate Cust Ref No. with different mobile no.!";
			}
		}
			
		// Adding custRefNo & mobile no. to control list
		duplicateRecordControl[0].add(custRefNo);
		duplicateRecordControl[1].add(mobileNo);
		return errorCode;
	}
}
