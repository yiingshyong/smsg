package com.bcb.sgwcore.queuefileprocessor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.ICustomerDao;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class SIBSFTPSMSConversion extends DefaultFTPSMSConversion{
	public SIBSFTPSMSConversion(){super();};
	
	public String process(String line, FtpReport reportModal, String user, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg, List<Telco> theTelco){	
		log.info("Process conversion of SIBS FTP SMS");
		try{
//			log.info("line: "+line);
			String data[] = trimData(line.split("["+aSetup.getFileDelimiter()+"]")); 
//			log.info("data length: "+data.length);
//			log.info("data[Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)]:"+data[Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)]);
//			log.info("data[Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_MSGO)]:"+data[Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_MSG)]);
//			log.info("data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]:"+data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
//			log.info("theTemplate: "+theTemplate);
			theTemplate=findTemplateMessage(theTemplate, data);
			log.info("theTemplate: "+theTemplate);
			
			String returnMsg = validation(data, theTemplate);// validate data
			log.info("returnMsg: "+returnMsg);
			
			if (returnMsg == null){
//				if(createCustomer(data)){
					if(createSMS(data, user, reportModal, aSetup, theTemplate, theCfg)){
					}else{
						returnMsg="Error in creating SMS! ";
					}
//				}else{
//					returnMsg="Error in creating customer record";
//				}
			}else{
				if(!getRemarks().contains(returnMsg)){
					getRemarks().add(returnMsg);
				}
			}
			return returnMsg;
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "Exception in code of record process.";
		}
	}
	
	
	protected boolean createSMS(String[] data, String user, FtpReport reportModal, FtpSetup aSetup, MsgTemplate theTemplate, SmsCfg theCfg){
		try{
			int charPerSms=CommonUtils.calculateCharPerSms(theTemplate.getMsgFormatType(), CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			String message = generateSMSMessage(data, theTemplate);	 log.info("message:"+message);
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);// 2nd item is template message code, used to create sms message
			SmsQueue sms = new SmsQueue();
			sms.setCreatedBy(user);
			sms.setCreatedDatetime(new Date());
			sms.setDeptId(reportModal.getDeptId());
			sms.setDept(reportModal.getDept());
			sms.setFtp(reportModal.getId());
			sms.setMessage(message);// 2nd item is template message code, used to create sms message
			sms.setTotalSms(msgArray.size());
			sms.setMobileNo(data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)]);
			sms.setMsgFormatType(theTemplate.getMsgFormatType());
			sms.setPriority(Integer.valueOf(fileConversion.getDefaultPriority()));
			sms.setRemarks(Constants.STR_EMPTY);
			sms.setSentBy(user);
			sms.setSmsScheduled(aSetup.getSmsScheduled());
			sms.setSmsScheduledTime(aSetup.getSmsScheduledTime());
			//sms.setTelco(selectTelco(theCfg.getSmsTelcoRouting(), sms.getMobileNo(), theTelco).getId());
			sms.setTelco(0);
			sms.setVersion(Integer.valueOf(Constants.APPSVERSION));
			
			if(CommonUtils.NullChecker(aSetup.getSmsAutoApproval(), String.class).toString().equals(Constants.STATUS_YES)){
				sms.setApprovalBy("automated");
				sms.setApprovalDatetime(new Date());
				if (CommonUtils.NullChecker(aSetup.getSmsScheduled(), String.class).toString().equals(Constants.STATUS_YES)&&CommonUtils.NullChecker(aSetup.getSmsScheduledTime(), String.class).toString().length()>0){
					sms.setSmsStatus(Constants.SMS_STATUS_6);
				}else{
					sms.setSmsStatus(Constants.SMS_STATUS_4);
				}
			}else{
				sms.setSmsStatus(Constants.SMS_STATUS_3);
			}
			
			sms.setSmsStatusBy("automated");
			sms.setSmsStatusDatetime(new Date());
			sms.setType(Constants.SMS_SOURCE_TYPE_4); 
			sms.setMsgRefNum(data[Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)]);
			smsQueueDao.insert(sms);
			log.info("SMS created");
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
			return false;
		}
		return true;
	}
	
	protected String validateHeader(String[] data){
		return validateHeader(data, Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)+1);
	}
	
	protected String validateDataLength(String[] data, MsgTemplate theTemplate){		
		return validateDataLength(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)+1);
	}
	
	protected String validateDataContent(String[] data, MsgTemplate theTemplate){
		return validateDataContent(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)+1);
	}
	
	protected String generateSMSMessage(String[] data, MsgTemplate theTemplate){
		return generateSMSMessage(data, theTemplate, Integer.valueOf(Constants.TEMPLATE_FIELD_SIBS_REF)+1);
	}
	
//	protected String validateDuplicateRecord(String[] data, MsgTemplate theTemplate){// Need to check on duplicate cust ref no. with different mobile no.
//		String errorCode=null;
//		String custRefNo=data[Integer.valueOf(Constants.TEMPLATE_FIELD_CUST_REF_NO)];
//		String mobileNo=data[Integer.valueOf(Constants.TEMPLATE_FIELD_MOBILE_NO)];
//		
//		if(theTemplate!=null){
//			for(int i=0;i<duplicateRecordControl[0].size();i++){
//				if(custRefNo.equals(duplicateRecordControl[0].get(i))&&!mobileNo.equals(duplicateRecordControl[1].get(i))){
//					errorCode="Duplicate Cust Ref No. with different mobile no.!";
//				}
//			}
//			
//			// Adding custRefNo & mobile no. to control list
//			duplicateRecordControl[0].add(custRefNo);
//			duplicateRecordControl[1].add(mobileNo);
//		}
//		return errorCode;
//	}
}
