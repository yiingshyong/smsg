package com.bcb.sgwcore.queuefileprocessor;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.bcb.crsmsg.bean.SIBSBatchOutputFileBean;
import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

public class SIBSSMSStatusFilePut extends FTPOutboundProcess{
	public SIBSSMSStatusFilePut(){super();}

	public boolean createOutBoundFiles(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg) throws Exception{

		boolean status=true;
		File newFile=null;
		
		//Rename file
		if(outboundFile.getTheFile().exists()){
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DDMMYY);
			File oldFile=outboundFile.getTheFile();
			String newFileName=oldFile.getName().split("["+Constants.SPECIAL_CHAR_DOT+"]", -1)[0]+theFormatter.format(new Date())+(oldFile.getName().contains(Constants.SPECIAL_CHAR_DOT)?Constants.SPECIAL_CHAR_DOT+oldFile.getName().split("["+Constants.SPECIAL_CHAR_DOT+"]", -1)[1]:Constants.STR_EMPTY);
			
			newFile=new File(oldFile.getParent()+File.separator+newFileName);
			if(oldFile.renameTo(newFile)){
				outboundFile.setTheFile(newFile);// update to point to the new file
				log.debug("file path: "+outboundFile.getTheFile().getAbsolutePath());
			}else{
				return false;
			}
		}else{
			return false;
		}
		
		Calendar calToday = Calendar.getInstance();
//		calToday.add(Calendar.DAY_OF_MONTH, -1);
		calToday.set(Calendar.HOUR_OF_DAY, 0);
		calToday.set(Calendar.MINUTE, 0);
		calToday.set(Calendar.SECOND, 0);
		calToday.set(Calendar.MILLISECOND, 0);

		List<SIBSBatchOutputFileBean> smsList = DaoBeanFactory.getSmsDao().retrieveSMSStatus(aSetup.getFtpOwner(), calToday.getTime());
		List<SIBSBatchOutputFileBean> smsQueueList = DaoBeanFactory.getSmsQueueDao().retrieveSMSStatus(aSetup.getFtpOwner(), calToday.getTime());
		List<SIBSBatchOutputFileBean> allSms = new ArrayList<SIBSBatchOutputFileBean>();
		if(smsList != null && !smsList.isEmpty()){
			allSms.addAll(smsList);
		}
		if(smsQueueList != null && !smsQueueList.isEmpty()){
			allSms.addAll(smsQueueList);
		}
		
		FileOutputStream fos = new FileOutputStream(outboundFile.getTheFile());
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos));

		// write header
		// write header
		
		// write content
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(SIBSBatchOutputFileBean bean : allSms){
			out.write(CommonUtils.println(bean.getMobileNo()));
			out.write(CommonUtils.println(sdf.format(bean.getSmsStatusDateTime())));
			out.write(CommonUtils.println(sdf.format(bean.getSmsCreatedDateTime())));
			out.write(CommonUtils.println(bean.getMessage()));
			out.write(CommonUtils.println(bean.getSentBy()));
			out.write(CommonUtils.println(bean.getTelco()));
			out.write(CommonUtils.println(bean.getStatus()));
			out.write(CommonUtils.println(bean.getRemark()));
			out.write(StringUtils.removeEnd(CommonUtils.println(bean.getRefCode()),","));			
			out.write(Constants.SPECIAL_CHAR_NEWLINE);
		}
		
		out.flush();
		out.close();
		fos.flush();
		out.close();
		return status;
	}

	@Override
	public void interceptFTPProcess(ChannelSftp ftpChannel, BatchSetup setup) {
		try{
			
			Calendar calYst = Calendar.getInstance();
			calYst.add(Calendar.DAY_OF_MONTH, -1);
			calYst.set(Calendar.HOUR_OF_DAY, 0);
			calYst.set(Calendar.MINUTE, 0);
			calYst.set(Calendar.SECOND, 0);
			calYst.set(Calendar.MILLISECOND, 0);
			FtpSetup ftpSetup = (FtpSetup) setup;
			String searchPath = "";
			Vector list = null;
			
			String fileName = ftpSetup.getFileName().split("[.]", -1)[0];
			String extension = ftpSetup.getFileName().split("[.]", -1)[1];
			fileName = fileName + new SimpleDateFormat(Constants.DATEFORMAT_DDMMYY).format(calYst.getTime());
			fileName = StringUtils.isEmpty(extension)? fileName : (fileName + Constants.SPECIAL_CHAR_DOT + extension);
			searchPath = StringUtils.isEmpty(ftpSetup.getFtpSourceDir())? fileName : (ftpSetup.getFtpSourceDir().trim() + File.separator + fileName);
			list = null;
			try{
				list = ftpChannel.ls(searchPath);
				if(list != null && !list.isEmpty()){
					ftpChannel.rm(searchPath);
				}
			}catch(SftpException se){
				log.error("Error remove file -> " + searchPath + ".Reason: " + se.getMessage(),se);
			}
			
			//ctrl file
			String ctrlFileName = AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SIBS_CTRL_FILE_NAME);
			String oldCtrlFileName = ctrlFileName; 
			String datePattern = StringUtils.substringBetween(ctrlFileName, "{", "}");
			if(StringUtils.isNotEmpty(datePattern)){
				ctrlFileName = StringUtils.replace(ctrlFileName, datePattern, new SimpleDateFormat(datePattern).format(new Date()));
				ctrlFileName = StringUtils.remove(ctrlFileName, "{");
				ctrlFileName = StringUtils.remove(ctrlFileName, "}");						
							
				oldCtrlFileName = StringUtils.replace(oldCtrlFileName, datePattern, new SimpleDateFormat(datePattern).format(calYst.getTime()));
				oldCtrlFileName = StringUtils.remove(oldCtrlFileName, "{");
				oldCtrlFileName = StringUtils.remove(oldCtrlFileName, "}");						
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SIBS_CTRL_FILE_DATA).getBytes());
			baos.flush();			
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());			
			ftpChannel.put(bais, ftpSetup.getFtpSourceDir() + File.separator + ctrlFileName);
			
			if(StringUtils.isNotEmpty(datePattern)){
				searchPath = StringUtils.isEmpty(ftpSetup.getFtpSourceDir())? oldCtrlFileName : (ftpSetup.getFtpSourceDir().trim() + File.separator + oldCtrlFileName);
				try{
					list = ftpChannel.ls(searchPath);
					if(list != null && !list.isEmpty()){
						ftpChannel.rm(searchPath);
					}
				}catch(SftpException se){
					log.error("Error remove file -> " + searchPath + ".Reason: " + se.getMessage(),se);
				}
			}

			bais.close();
			baos.close();
		}catch(Exception e){
			log.error("Error intercepting ftp process. Reason: " + e.getMessage(), e);
		}		
	}
}
