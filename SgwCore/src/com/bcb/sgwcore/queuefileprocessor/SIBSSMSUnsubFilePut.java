package com.bcb.sgwcore.queuefileprocessor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.dao.IADSSynchDao;
import com.bcb.crsmsg.dao.IBatchSetupDao;
import com.bcb.crsmsg.dao.IFtpReportDao;
import com.bcb.crsmsg.dao.ISmsDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ISmsStatusDao;
import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsStatus;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class SIBSSMSUnsubFilePut extends FTPOutboundProcess{
	public SIBSSMSUnsubFilePut(){super();}

	public boolean createOutBoundFiles(FileObj outboundFile, FtpSetup aSetup, FtpReport reportModal, SmsCfg theCfg) throws Exception{
		log.info("CreateOutBoundFiles");
		boolean status=true;
		File newFile=null;
		
		//Rename file
		if(outboundFile.getTheFile().exists()){
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DDMMYY);
			File oldFile=outboundFile.getTheFile();
			log.info("file name: "+oldFile.getName());
			log.info("file name first: "+oldFile.getName().split("[.]", -1)[0]);
			log.info("file name last: "+oldFile.getName().split("[.]", -1)[1]);
			String newFileName=oldFile.getName().split("["+Constants.SPECIAL_CHAR_DOT+"]", -1)[0]+Constants.STR_SPACE+theFormatter.format(new Date())+(oldFile.getName().contains(Constants.SPECIAL_CHAR_DOT)?Constants.SPECIAL_CHAR_DOT+oldFile.getName().split("["+Constants.SPECIAL_CHAR_DOT+"]", -1)[1]:Constants.STR_EMPTY);
			log.info("newFileName: "+newFileName);
			log.info("File.separator: "+File.separator);
			log.info("new File path: "+oldFile.getParent()+File.separator+newFileName);
			
			newFile=new File(oldFile.getParent()+File.separator+newFileName);
			if(oldFile.renameTo(newFile)){
				outboundFile.setTheFile(newFile);// update to point to the new file
				log.info("file path: "+outboundFile.getTheFile().getAbsolutePath());
			}else{
				return false;
			}
		}else{
			return false;
		}
		
		IADSSynchDao synchDao=(IADSSynchDao)SgwCoreHibernateUtil.getBean("ADSSynchDao");
		ISmsDao smsDao=(ISmsDao)SgwCoreHibernateUtil.getBean("smsDao");
		ISmsStatusDao smsStatusDao = (ISmsStatusDao) SgwCoreHibernateUtil.getBean("smsStatusDao");
		ITelcoEsmsDao telcoDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
		IBatchSetupDao setupDao = (IBatchSetupDao) SgwCoreHibernateUtil.getBean("ftpSetupDao");
		IFtpReportDao reportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
		ISmsQueueDao queueDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
		
		
		List<Object> smsQueueList=new ArrayList<Object>();
		List<Object> smsList=new ArrayList<Object>();
		List<SmsStatus> statusList=new ArrayList<SmsStatus>();
		List<Telco> telcoList=new ArrayList<Telco>();
		List<Object> setupList=new ArrayList<Object>();
		List<Object> reportList=new ArrayList<Object>();
		
		SimpleDateFormat dateFormatter=new SimpleDateFormat(Constants.DATEFORMAT_YYYY_MM_DD);
		
		// Get Ftp setup list of the defined owner
		setupList=setupDao.findSetup("select id from FtpSetup where ftpOwner=?", new Object[]{aSetup.getFtpOwner()});

		// Get Ftp Report list for the Ftp Setup id list
		String parameters=Constants.STR_EMPTY;
		for(int i=0;i<setupList.size();i++){
			if(i!=0){
				parameters+=Constants.SPECIAL_CHAR_COMMA;
			}
			parameters+="?";
		}
//		log.info("ftpsetup?: "+parameters);
//		log.info("ftpsetupId: "+setupList);

		reportList=reportDao.findReport("select id from FtpReport where ftpSetup in ("+parameters+") and startDate like '"+dateFormatter.format(new Date())+"%'", setupList.toArray());
		// Get Ftp Report list for the Ftp Setup
		
		// Get Sms list from Ftp Report id list
		parameters=Constants.STR_EMPTY;
		String id ="";
		for(int i=0;i<reportList.size();i++){
			if(i!=0){
				parameters+=Constants.SPECIAL_CHAR_COMMA;				
			}
			parameters+="?";
		}
//		log.info("ftpreport?: "+parameters);
//		log.info("ftpreportId: "+reportList);
		
		id=reportList.toString().replace("[", "").replace("]", "");

//		reportList.add(Constants.STATUS_FALSE_0);
		
		smsQueueList=synchDao.findRecord("select s from SmsQueue s where s.ftp in ("+parameters+") ", reportList.toArray());
		smsList=synchDao.findRecord("select s from Sms s where s.ftp in ("+parameters+") ", reportList.toArray());
	
		// Get Sms list from Ftp Report id list

//		log.info("sms list: "+smsList);
//		log.info("smsQueueList list: "+smsQueueList.toString());
		statusList=smsStatusDao.findAll();
		telcoList=telcoDao.findAll();
		
		FileOutputStream fos = new FileOutputStream(outboundFile.getTheFile());
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fos));

		// write header
		// write header
		
		// write content
		for(int i=0;i<smsList.size();i++){
			Sms aSms=(Sms)smsList.get(i);
			
			out.write(aSms.getMobileNo());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getSmsStatusDatetime(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getCreatedDatetime(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getMessage(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getSentBy(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			
			for(int j=0;j<telcoList.size();j++){
				Telco theTelco=(Telco)telcoList.get(j);
				if(theTelco.getId().equals(Integer.valueOf(CommonUtils.NullChecker(aSms.getTelco(), Integer.class).toString()))){
					out.write(CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString());
					out.write(Constants.SPECIAL_CHAR_COMMA);
					break;// break the for loop
				}
			}
			
			for(int j=0;j<statusList.size();j++){
				SmsStatus theStatus=(SmsStatus)statusList.get(j);
				if(theStatus.getStatusCode().equals(CommonUtils.NullChecker(aSms.getSmsStatus(), Integer.class).toString())){
					out.write(CommonUtils.NullChecker(theStatus.getStatusName(), String.class).toString());
					out.write(Constants.SPECIAL_CHAR_COMMA);
					break;// break the for loop
				}
			}
		
			out.write(CommonUtils.NullChecker(aSms.getRemarks(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getMsgRefNum(), String.class).toString());
//			out.write(Constants.SPECIAL_CHAR_COMMA);			

			out.write(Constants.SPECIAL_CHAR_NEWLINE);
		}
		// write content
		
//		 write content
		for(int i=0;i<smsQueueList.size();i++){
			SmsQueue aSms=(SmsQueue)smsQueueList.get(i);
			
			out.write(aSms.getMobileNo());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getSmsStatusDatetime(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getCreatedDatetime(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getMessage(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getSentBy(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			
			for(int j=0;j<telcoList.size();j++){
				Telco theTelco=(Telco)telcoList.get(j);
				if(theTelco.getId().equals(Integer.valueOf(CommonUtils.NullChecker(aSms.getTelco(), Integer.class).toString()))){
					out.write(CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString());
					out.write(Constants.SPECIAL_CHAR_COMMA);
					break;// break the for loop
				}
			}
			
			for(int j=0;j<statusList.size();j++){
				SmsStatus theStatus=(SmsStatus)statusList.get(j);
				if(theStatus.getStatusCode().equals(CommonUtils.NullChecker(aSms.getSmsStatus(), Integer.class).toString())){
					out.write(CommonUtils.NullChecker(theStatus.getStatusName(), String.class).toString());
					out.write(Constants.SPECIAL_CHAR_COMMA);
					break;// break the for loop
				}
			}
		
			out.write(CommonUtils.NullChecker(aSms.getRemarks(), String.class).toString());
			out.write(Constants.SPECIAL_CHAR_COMMA);
			out.write(CommonUtils.NullChecker(aSms.getMsgRefNum(), String.class).toString());
//			out.write(Constants.SPECIAL_CHAR_COMMA);
			
			out.write(Constants.SPECIAL_CHAR_NEWLINE);
		}
		// write content
		
		// write trailer
		// write trailer
		
		// Close the writter
		out.flush();
		out.close();
		fos.flush();
		out.close();
		// Close the writter		

		
		log.info("status: "+status);
		log.info("end SIBSSMSUnsubFilePut");
		return status;
	}

}
