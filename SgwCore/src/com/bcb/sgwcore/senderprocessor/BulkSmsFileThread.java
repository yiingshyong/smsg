package com.bcb.sgwcore.senderprocessor;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.crsmsg.dao.BulkSMSFileDao;
import com.bcb.crsmsg.modal.BulkSmsFile;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;


public class BulkSmsFileThread extends Thread{
	private static final Log log = LogFactory.getLog(BulkSmsFileThread.class);
	public static void main(String[] args) {

		BulkSmsFileThread bulkSmsFileThread = new BulkSmsFileThread();
		bulkSmsFileThread.start();
	}
	
	@Override
	public void run() {
		boolean loop = true;

		while(loop){

			
			try{

				log.info("BulkSmsFileThread : Start");
				BulkSMSFileDao setupDao = (BulkSMSFileDao) SgwCoreHibernateUtil.getBean("bulkSMSFileDao");
				List<BulkSmsFile> bulkSmsFiles = setupDao.findMessages(Constants.SMS_STATUS_4);
				setupDao.updateStatus(bulkSmsFiles,Constants.SMS_STATUS_M);
				for(BulkSmsFile file:bulkSmsFiles){
					File uploadFileObject = new File(file.getUploadPath(), file.getFileName());
					if(!uploadFileObject.getParentFile().exists()){
						uploadFileObject.mkdirs();
					}
					FileOutputStream fileOutStream = new FileOutputStream(uploadFileObject);
					fileOutStream.write(file.getContent());
					fileOutStream.flush();
					fileOutStream.close();
				}
				setupDao.updateStatus(bulkSmsFiles,Constants.SMS_STATUS_1);
				setupDao.updateSetupStatus(bulkSmsFiles,Constants.STATUS_ACTIVE);
				log.info("BulkSmsFileThread : Finish");
								
				Thread.sleep(60000);
			}catch (Exception runex){
				runex.printStackTrace();
				log.info("BulkSmsFileThread : Error");
			}finally{

			}
		}
		 
		Thread.interrupted();
		System.exit(0); 
	}	
	

}
