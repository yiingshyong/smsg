package com.bcb.sgwcore.senderprocessor;

import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.EncryptionManager;
import com.bcb.sgwcore.util.SgwParam;

public class GenericSvcProviderClientImpl extends TelcoSmsSender {
	
	private String URL_PARAM_SEP = "&";
	private String URL_PARAM_ASSIGNMENT = "=";
	private String SMSG_URL_PARAM_SEP = "|&|";
	private String SMSG_URL_PARAM_ASSIGNMENT = "|=|";
	
	static{
		Velocity.init();
	}
	
	public String generateQueryString(SmsQueue smsQueue) throws Exception{
		//Boolean encryption = operator.getEncryption();true/false
		super.generateQueryString(smsQueue);	
		String encryption = operator.getEncryptionStatus();
		log.info("encryption status:"+encryption);
		StringBuffer sb = new StringBuffer(operator.getUrl());
		StringBuffer encryptedurl = new StringBuffer(operator.getUrl());	
		StringBuffer encryptedsb = new StringBuffer();	
		VelocityContext context = new VelocityContext();
		context.put("sms", smsQueue);
		context.put("telco", super.operator);
		context.put("mobileNo", super.mobileNo);
		StringWriter outputWriter = new StringWriter();
		Velocity.evaluate(context, outputWriter, "", operator.getUrlParam());
		String urlStr = outputWriter.toString();
		String[] params = StringUtils.splitByWholeSeparator(urlStr, SMSG_URL_PARAM_SEP);
		for(String param : params){
			String[] paramPair = StringUtils.splitByWholeSeparator(param, SMSG_URL_PARAM_ASSIGNMENT);
			String paramValue = URLEncoder.encode(paramPair[1], "UTF-8");
			if(encryption.equals("TRUE")){
				encryptedsb.append(paramPair[0]).append(URL_PARAM_ASSIGNMENT).append(paramValue).append(URL_PARAM_SEP);
			}else{
				sb.append(paramPair[0]).append(URL_PARAM_ASSIGNMENT).append(paramValue).append(URL_PARAM_SEP);
			}
			
		}		
	
		if(encryption.equals("TRUE")){
			//log.info("sb.toString():"+sb.toString());
			String msgData = StringUtils.removeEnd(encryptedsb.toString(), URL_PARAM_SEP);
			String encryptedData = URLEncoder.encode((EncryptionManager.getInstance().encrypt(msgData)),"UTF-8");
			//log.info("encryptedData:"+encryptedData);
			log.info("encryptedurl+encryptedData:"+encryptedurl+encryptedData);
			return encryptedurl+encryptedData;
		}else{
			return StringUtils.removeEnd(sb.toString(), URL_PARAM_SEP);
		}
		
	}
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		log.debug("GenericSvcProviderClientImpl-send concatenate sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String concatenateSms=Constants.STR_EMPTY;
		String tempMsg=Constants.STR_EMPTY;
		String returnStatus=Constants.STR_EMPTY;
		
		for(int i=0;i<smsQueue.size();i++){
			concatenateSms+=smsQueue.get(i).getMessage();
			if(i==smsQueue.size()-1){// final concatenate sms, send the sms
				try {
					tempMsg=smsQueue.get(i).getMessage();
					smsQueue.get(i).setMessage(concatenateSms);
					returnStatus=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
					smsQueue.get(i).setMessage(tempMsg);
				} catch (SecurityException se){
					throw new SecurityException(se);
				} catch (Exception e) {
					log.error("Service Provider: " + this.operator.getTelcoName() + 
								" Mobile No: " + this.mobileNo + 
								" SMS Id: " + smsQueue.get(i).getId() + 
								" i o error " + e.getMessage(), e);
					returnStatus=SgwParam.RETURN_CODE_SGW_PROBLEM;
				}
			}
		}
		
		for(int i=0;i<smsQueue.size();i++){
			returnCode.add(returnStatus);
		}
		
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		ArrayList<String> returnCode=sendMessage(smsQueue);
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		return returnCodes;
	}	
}
