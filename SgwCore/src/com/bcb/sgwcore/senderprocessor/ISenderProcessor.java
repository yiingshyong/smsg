package com.bcb.sgwcore.senderprocessor;

import java.util.Collection;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;

public interface ISenderProcessor {
	
	public void setupConfiguration(Telco operator);
	public boolean sendMessage(SmsQueue smsQueue);
	public boolean sendAllMessages(Collection<SmsQueue> col);
}
