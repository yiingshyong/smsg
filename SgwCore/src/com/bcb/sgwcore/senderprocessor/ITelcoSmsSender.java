package com.bcb.sgwcore.senderprocessor;

import java.util.ArrayList;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;

public interface ITelcoSmsSender {
	public void setupCfg(Telco operator);
	
	public String sendMessage(String queryString, SmsQueue smsQueue) throws Exception;
	
	public void initSSL();
	
	public String sendMessage(SmsQueue smsQueue);
	
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue);
	
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes);
	
	public String generateQueryString(SmsQueue smsQueue) throws Exception;

	public void calculateCharge(SmsQueue theSms);
	
	public void calculateCharge(ArrayList<SmsQueue> theSms, ArrayList<String> returnCodes);
}
