package com.bcb.sgwcore.senderprocessor;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;
import com.bcb.sgwcore.util.SgwCoreUtil;

public class OperatorController extends Thread{
	
	boolean isRunning=false;
	ITelcoEsmsDao telcoEsmsDao = null;
	Log log = LogManager.getLog();
	List<Telco> telcoEsmsList=null;
	int totalOperatorSize=0;
	Executor executor=null;
	
	public OperatorController(){
		log.debug("OperatorController ");
		try {
			telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
			telcoEsmsList = telcoEsmsDao.findAllTelcoEsmsAvailable();
			totalOperatorSize=telcoEsmsList.size();
			log.debug("telcoEsmsList " + totalOperatorSize);
			executor = Executors.newFixedThreadPool (totalOperatorSize);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}	
	}
	
	public void shutdown()
	{
		isRunning=false;
	}
	
	
	public void run(){
		isRunning=true;
		
		while(isRunning)
		{
			log.debug("operator controller is up");
			for (Telco operator : telcoEsmsList) {
				ITelcoSmsSender telcoSmsSender;
				try {
					telcoSmsSender = (ITelcoSmsSender) Class.forName(operator.getTelcoClassName()).newInstance();
					executor.execute(new SenderThread(operator,telcoSmsSender));
				} catch (Exception e) {
					log.error(e.getMessage(),e);
				}
					
			}
			try{
				log.debug("sleep a while");
				sleep(SgwCoreUtil.getThreadInterval());
			}catch(Exception e){
					log.error(e.getMessage(),e);
			}
			
		}
		
		
		
		
		
		
	}

}
