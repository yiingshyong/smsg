package com.bcb.sgwcore.senderprocessor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.dao.GroupDao;
import com.bcb.crsmsg.dao.ISmsCfgDao;
import com.bcb.crsmsg.dao.ISmsDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ISmsQuotaDao;
import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.dao.IUserDao;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsQuota;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.CommonUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.bcb.sgwcore.util.SendMailBean;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;
import com.bcb.sgwcore.util.SgwParam;

public class SenderThread implements Runnable {
	Log log = LogManager.getLog();
	Telco telcoEsms;
	ITelcoSmsSender telcoSmsSender;
	Integer operatorId;
	boolean isRunning;
	static int defaultRetry = 3;
	// Dao
	// ISenderThreadDao senderThreadDao;
	ISmsDao smsDao = null;
	ISmsQueueDao smsQueueDao = null;
	//MainSenderProcessor msp;
	LinkedList<SmsQueue> smsList = null;
	
	public SenderThread(Telco telcoEsms, ITelcoSmsSender telcoSmsSender) {
		log.debug("Instantiate Sender Thread");
		try {
			smsDao = (ISmsDao) SgwCoreHibernateUtil
			.getBean("smsDao");
			smsQueueDao = (ISmsQueueDao) SgwCoreHibernateUtil
			.getBean("smsQueueDao");
			smsList = new LinkedList<SmsQueue>();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		this.telcoEsms = telcoEsms;
		this.operatorId = telcoEsms.getId();
		this.telcoSmsSender = telcoSmsSender;
		log.debug("Exit Sender Thread");
		
	}
	
	public synchronized void addSms(SmsQueue sms) {
		log.debug("before addSms smsList: "+smsList);
		smsList.addLast(sms);
		log.debug("after addSms smsList: "+smsList);
	}
	
	public synchronized void addFirstSms(SmsQueue sms) {
		log.debug("before addFirstSms smsList: "+smsList);
		smsList.addFirst(sms);
		log.debug("after addFirstSms smsList: "+smsList);
	}
	
	public static void main(String[] args) {
		Telco tc = new Telco();
		tc.setId(1);
		ITelcoSmsSender sender = new TelcoSmsSender2();
		SenderThread t = new SenderThread(tc, sender);		
//		t.run();		
//		Thread thread = new Thread(t);
//		thread.start();
		SmsQueue q1 = new SmsQueue();
		SmsQueue q2 = new SmsQueue();
		SmsQueue q3 = new SmsQueue();
		q1.setId(1);
		q2.setId(2);
		q3.setId(3);
		q1.setConcatenateSms("1");
		q2.setConcatenateSms("1");
		q3.setConcatenateSms("1|2|3");
		t.addSms(q1);
		t.addSms(q2);
		t.addSms(q3);
		
		List<SmsQueue> msgs = t.removeFirstSmsList();
		for(SmsQueue q : msgs){
			System.out.println("id: " + q.getId());
		}
	}
	/**
	 * Retrieve top sms in the list
	 */
	private synchronized ArrayList<SmsQueue> removeFirstSmsList() {
		log.debug("remove first sms");
		SmsQueue sms = null;
		ArrayList<SmsQueue> concatenateSmsQueue=new ArrayList<SmsQueue>();
		boolean isCompleteSms=false;
		
		try {
			
			while(smsList != null && smsList.size() > 0&&!isCompleteSms) {
				log.debug("smsList size: "+smsList.size());
				log.debug("isCompleteSms: "+isCompleteSms);
				
				sms = (SmsQueue) smsList.removeFirst();
				
				if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().length()==0){// is a single sms
					log.debug("is a single sms");
					isCompleteSms=true;
					concatenateSmsQueue=new ArrayList<SmsQueue>();
				}else{// is concatenate sms
					log.debug("is concatenate sms");
					if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().equals(sms.getId().toString())){// first concatenate sms
						log.debug("is first concatenate msg");
						concatenateSmsQueue=new ArrayList<SmsQueue>();
					}else if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().contains(Constants.SPECIAL_CHAR_PIPE)){// signal end of concatenate sms
						log.debug("is last concatente msg");
						for(int i=0;i<concatenateSmsQueue.size();i++){// check if concatenateSmsQueue collected is complete
							if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().contains(concatenateSmsQueue.get(i).getId().toString())){
							}else{
								isCompleteSms=false;
							}
						}
						isCompleteSms=true;
					}else{// part of concatenate sms
						log.debug("is mid of concatenate msg");
						isCompleteSms=false;
					}
				}
				
				if (smsQueueDao.isExist(sms)) {
					log.debug("sms exist, adding in queue concatenate list");
					concatenateSmsQueue.add(sms);
				}else{// sms not exist, fail the whole sms
					log.debug(" sms " + sms.getId() + " already delete");
				}
			}
			
			if(isCompleteSms){
				log.debug("is complete sms");
			}else{
				log.debug("is not complete sms");
				for(int i=concatenateSmsQueue.size()-1;i>=0;i--){// add back the removed sms
					addFirstSms(concatenateSmsQueue.get(i));
					log.debug("smsList size: "+smsList.size());
				}
				return null;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
		
		return concatenateSmsQueue;
	}
	
	public synchronized int getTotalSmsList() {
		int total = 0;
		
		try {
			total = smsList.size();
		} catch (Exception e) {
			
		}
		return total;
	}
	
	public void run() {
		isRunning = true;
		int failCounter=0;
		log.info("telco " + this.telcoEsms.getTelcoName() + " is up");
		ArrayList<String> returnCodes = new ArrayList<String>();
		
		while (isRunning) {
			while (getTotalSmsList() > 0) {
				try{
					ITelcoEsmsDao telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
					this.telcoEsms=telcoEsmsDao.findById(this.telcoEsms.getId());
					
					if(this.telcoEsms.getStatus().equals(Constants.STATUS_CLOSED)){
						// need to release the sms queue list of the closed telco and route to other telco to resend
						smsQueueDao.resetAllStatus(smsList, Constants.SMS_STATUS_4);
						smsList= new LinkedList<SmsQueue>();
						failCounter=0;
					}else{
						ArrayList<SmsQueue> smsQueue = removeFirstSmsList();
						log.debug("smsQueue: "+smsQueue);
						
						if(smsQueue!=null&&smsQueue.size()>0){
							// check quota
							boolean send=true;
							ISmsCfgDao smsCfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
							SmsCfg theCfg=smsCfgDao.findByCfgId(Constants.APPSVERSION);
							ISmsQuotaDao smsQuotaDao = (ISmsQuotaDao) SgwCoreHibernateUtil.getBean("smsQuotaDao");
							SmsQuota theQuota=null;
							
							if(CommonUtils.NullChecker(theCfg.getSmsQuota(), String.class).toString().equals(Constants.STATUS_YES)){
								List quotaList=smsQuotaDao.findSmsQuota("from SmsQuota where ref=? and quota>=?", new Object[]{smsQueue.get(0).getDeptId(), smsQueue.size()});
								if(quotaList.size()==1){
									theQuota=(SmsQuota)quotaList.get(0);
								}else{// exceed quota
									log.error("Department exceed quota: "+smsQueue.get(0).getDeptId());
									smsQueueDao.updateStatus(smsQueue, Constants.SMS_STATUS_4);
									send=false;
								}
							}else{// no need check quota
								/*if(quotaList.size()==1){
									theQuota=(SmsQuota)quotaList.get(0);
								}else{
									send=false;
								}*/
							}

							if(send && isBlockedByTimeControl(smsQueue)){
								send = false;
							}

							if(send){
								telcoSmsSender.setupCfg(this.telcoEsms);								
								returnCodes = CommonUtil.normalizeReturnCode(telcoSmsSender.sendMessage(smsQueue));
								log.debug("returnCodes: "+returnCodes);
								
								boolean retry=false;
								if (returnCodes.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// fail to send
									retry=true;
								}else{// check for retry code
									String[] retryCode=CommonUtils.NullChecker(this.telcoEsms.getRetryCode(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
									for(int i=0;i<retryCode.length;i++){
										if(returnCodes.contains(retryCode[i])){
											retry=true;
										}
									}
								}
								
								log.debug("retry: "+retry);
								if(retry){// got error or retry code
									log.debug("going to retry send sms queue");
									log.debug("returnCodes: "+returnCodes);
									if(retry(smsQueue, returnCodes)){// retry completed
										// continue the process
										log.debug("retry done, either successful or fail completely, update status");
										
										log.debug("telco status: "+this.telcoEsms.getStatus());
										if(this.telcoEsms.getStatus().equals(Constants.STATUS_DISABLED)){
											this.telcoEsms.setStatus(Constants.STATUS_ACTIVE);
											this.telcoEsms.setStatusDatetime(new Date());
											this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
											telcoEsmsDao.update(this.telcoEsms);
										}
										telcoSmsSender.calculateCharge(smsQueue, returnCodes);
										statusUpdating(smsQueue, returnCodes);
										updateQuota(theQuota, returnCodes);
										failCounter=0;
									}else{//failed on retry sending sms
										log.debug("retry failed");
										// Temporary disable the telco
										if(this.telcoEsms!=null){
											long disabledTime=(new Date().getTime()-this.telcoEsms.getStatusDatetime().getTime());
											failCounter+=1;
											boolean update=false;
											
											if(this.telcoEsms.getStatus().equals(Constants.STATUS_ACTIVE)){
												this.telcoEsms.setStatus(Constants.STATUS_DISABLED);// temporary disabled
												this.telcoEsms.setStatusDatetime(new Date());
												this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
												telcoEsmsDao.update(this.telcoEsms);
											}else{
												log.debug("disabledTime: "+disabledTime);
												log.debug("threshold period: "+Long.valueOf(theCfg.getSmsFailureThresholdPeriod())*1000*60);
												if(CommonUtils.NullChecker(theCfg.getSmsFailureThresholdPeriod(), String.class).toString().length()>0&&
														disabledTime<Long.valueOf(theCfg.getSmsFailureThresholdPeriod())*1000*60){// to hold fail counter before reach enough recheck time
													failCounter=0;
													log.debug("Not yet reach recheck time, reset counter");
												}else{
													log.debug("Reach recheck time, start counter");
												}
											}
											
											if(CommonUtils.NullChecker(theCfg.getSmsFailureNotification(), String.class).toString().contains("E")){// send email
												if((CommonUtils.NullChecker(theCfg.getSmsFailureThreshold(), String.class).toString().length()>0&&
														failCounter>Integer.valueOf(theCfg.getSmsFailureThreshold()))){
													String[] toList=CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
													log.debug("Sending email notification");
													for(int i=0;i<toList.length;i++){
														sendFailureEmailNotification(this.telcoEsms, toList[i], theCfg, disabledTime, failCounter);
													}
													update=true;
												}
											}
											if(CommonUtils.NullChecker(theCfg.getSmsFailureNotification(), String.class).toString().contains("S")){// send sms
												if((CommonUtils.NullChecker(theCfg.getSmsFailureThreshold(), String.class).toString().length()>0&&
														failCounter>Integer.valueOf(theCfg.getSmsFailureThreshold()))){
													String[] toList=CommonUtils.NullChecker(theCfg.getSmsFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
													log.debug("Sending SMS notification");
													for(int i=0;i<toList.length;i++){
														sendFailureSmsNotification(this.telcoEsms, toList[i], theCfg, disabledTime, failCounter);
													}
													update=true;
												}
											}
											if(update){
												failCounter=0;
												this.telcoEsms.setStatusDatetime(new Date());
												telcoEsmsDao.update(this.telcoEsms);
											}
										}
										
										// need to release the sms queue list of the disabled telco and route to other telco to resend
										smsQueueDao.resetAllStatus(smsList, Constants.SMS_STATUS_4);
										smsList= new LinkedList<SmsQueue>();
										log.debug("retry failed");
									}
								}else{// no error or retry
									log.debug("telco status: "+this.telcoEsms.getStatus());
									if(this.telcoEsms.getStatus().equals(Constants.STATUS_DISABLED)){
										this.telcoEsms.setStatus(Constants.STATUS_ACTIVE);
										this.telcoEsms.setStatusDatetime(new Date());
										this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
										telcoEsmsDao.update(this.telcoEsms);
									}
									telcoSmsSender.calculateCharge(smsQueue, returnCodes);
									statusUpdating(smsQueue, returnCodes);
									updateQuota(theQuota, returnCodes);
									failCounter=0;
								}
							}
							
							log.debug("Telco ID: "+this.telcoEsms.getId());
							log.debug("sms list: "+getTotalSmsList());
							telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
							this.telcoEsms=telcoEsmsDao.findById(this.telcoEsms.getId());
							this.telcoEsms.setSmsOnHand(Integer.toString(getTotalSmsList()));
							log.debug("telco smsonhand: "+this.telcoEsms.getSmsOnHand());
							telcoEsmsDao.update(this.telcoEsms);
						}
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		
		log.info("telco " + this.telcoEsms.getTelcoName() + " is shutdown");
	}
	
	private boolean isBlockedByTimeControl(ArrayList<SmsQueue> smsList) throws Exception{
		List onHoldList=new ArrayList();
		if(smsList == null || smsList.isEmpty()){
			return false;
		}
		SmsQueue sms = smsList.get(0);
		if(sms.getType().equals(Constants.SMS_SOURCE_TYPE_1)||sms.getType().equals(Constants.SMS_SOURCE_TYPE_2)){
			onHoldList= DaoBeanFactory.getTimeControlDao().findDeptWebBlock();
		}else if(sms.getType().equals(Constants.SMS_SOURCE_TYPE_3)||sms.getType().equals(Constants.SMS_SOURCE_TYPE_4)){
			onHoldList= DaoBeanFactory.getTimeControlDao().findDeptBatchBlock();
		}else{
			// Error, no available sms type match
		}
		if(onHoldList.contains(sms.getDeptId())){
			for(SmsQueue smsqueue : smsList){
				smsqueue.setRemarks("SMS Time Controlled - Hold");
				smsqueue.setModifiedBy("SmsTimeControl");
				smsqueue.setModifiedDatetime(new Date());
				smsQueueDao.update(smsqueue);				
			}
			smsQueueDao.updateStatus(smsList, Constants.SMS_STATUS_4);								
			return true;
		}
		return false;
		
	}
	
	private boolean retry(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes){
		log.debug("retry to send queue sms");
		int retry = defaultRetry;
		
		SmsCfg theCfg=null;
		try {
			ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
			theCfg = cfgDao.findByCfgId(Integer.valueOf(Constants.APPSVERSION).toString());
			if (theCfg == null){
				return false;
			}else{
				if(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString().length()>0){
					retry=Integer.valueOf(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString());
				}
				int runningRetry = 0;
				boolean retryStatus=true;
				telcoSmsSender.setupCfg(this.telcoEsms);
				
				while (retryStatus&&runningRetry< retry){
					if(CommonUtils.NullChecker(theCfg.getAutoSmsResendInterval(), String.class).toString().length()>0&&
							Integer.valueOf(theCfg.getAutoSmsResendInterval())>0){
						sleepforRetryInterval(Integer.valueOf(theCfg.getAutoSmsResendInterval())*60000);
					}else{// minimum retry interval is 1 minute
						sleepforRetryInterval(60000);
					}
					
					retryStatus=false;
					//log.debug("runningRetry: " + runningRetry + ", SMS Id: " + smsQueue.getId());					
					CommonUtil.normalizeReturnCode(telcoSmsSender.sendMessage(smsQueue, returnCodes));
					//returnCodes=SgwParam.RETURN_CODE_SGW_PROBLEM;
					log.debug("retry return Codes: "+returnCodes);
					if (returnCodes.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// fail to send
						retryStatus=true;
					}else{// check for retry code
						String[] retryCode=CommonUtils.NullChecker(this.telcoEsms.getRetryCode(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
						
						for(int i=0;i<retryCode.length;i++){
							if(returnCodes.contains(retryCode[i])){
								retryStatus=true;
							}
						}
					}
					runningRetry++;
				}
				
				// retry limit reached
				if(retryStatus){
					return !requeueSms(smsQueue, returnCodes);
				}else{// no error or retry, retry completed
					return true;
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/*private boolean retry(SmsQueue smsQueue){
	 log.debug("retry to send queue sms");
	 int retry = defaultRetry;
	 
	 SmsCfg theCfg=null;
	 try {
	 ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
	 theCfg = cfgDao.findByCfgId(Integer.valueOf(Constants.APPSVERSION).toString());
	 if (theCfg == null){
	 return false;
	 }else{
	 if(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString().length()>0){
	 retry=Integer.valueOf(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString());
	 }
	 int runningRetry = 0;
	 telcoSmsSender.setupCfg(this.telcoEsms);
	 String returnCodes = "-"; 
	 
	 while (runningRetry < retry){
	 if(CommonUtils.NullChecker(theCfg.getAutoSmsResendInterval(), String.class).toString().length()>0&&
	 Integer.valueOf(theCfg.getAutoSmsResendInterval())>0){
	 sleepforRetryInterval(Integer.valueOf(theCfg.getAutoSmsResendInterval())*60000);
	 }else{
	 sleepforRetryInterval(60000);
	 }
	 
	 log.debug("runningRetry: " + runningRetry + ", SMS Id: " + smsQueue.getId());
	 returnCodes = telcoSmsSender.sendMessage(smsQueue);
	 //returnCodes=SgwParam.RETURN_CODE_SGW_PROBLEM;
	  returnCodes = CommonUtil.normalizeReturnCode(returnCodes);
	  if (returnCodes.toUpperCase().contains(this.telcoEsms.getSuccessCode().toUpperCase())) {
	  log.debug(smsQueue.getId() + " in " + this.telcoEsms.getTelcoName() + " success ");
	  telcoSmsSender.calculateCharge(smsQueue);
	  successStatusUpdating(smsQueue, returnCodes);
	  break;
	  } else {
	  //log.error(smsQueue.getId() + "in " + telcoEsms.getTelcoName() + " failed error [" + returnCodes+"]");
	   //failStatusUpdating(smsQueue, telcoEsms, returnCodes);
	    }
	    runningRetry++;
	    }
	    
	    if(runningRetry==retry){// reach retry limit and not successful
	    log.error(smsQueue.getId() + "in " + this.telcoEsms.getTelcoName() + " failed error [" + returnCodes+"]");
	    requeueSms(smsQueue, returnCodes);
	    
	    // Temporary disable the telco
	     ITelcoEsmsDao telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
	     this.telcoEsms=telcoEsmsDao.findById(this.telcoEsms.getId());
	     if(this.telcoEsms!=null&&this.telcoEsms.getStatus().equals(Constants.STATUS_ACTIVE)){
	     this.telcoEsms.setStatus(Constants.STATUS_DISABLED);// temporary disabled
	     this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
	     telcoEsmsDao.update(this.telcoEsms);
	     }
	     return false;
	     }else{// sms successful sent
	     return true;
	     }
	     }
	     
	     } catch (Exception e) {
	     // TODO Auto-generated catch block
	      e.printStackTrace();
	      return false;
	      }
	      }*/
	
	/*private void requeueSms(SmsQueue smsQueue, String returnCodes) {
	 try {
	 smsQueue.setRemarks("Error: " + returnCodes);
	 log.debug(" trying to requeue sms");
	 smsQueueDao.updateStatus(smsQueue, Constants.SMS_STATUS_4);
	 } catch (Exception e) {
	 log.error(e.getMessage(), e);
	 }
	 }*/
	
	private boolean requeueSms(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		boolean queue=true;
		try {
			for(int i=0;i<returnCodes.size();i++){
				if(returnCodes.get(i).contains(this.telcoEsms.getSuccessCode())){
					queue=false;
				}
			}
			
			if(queue){
				log.debug("not contain success case, requeue sms");
				for(int i=0;i<smsQueue.size();i++){
					smsQueue.get(i).setRemarks("Error: " + returnCodes);
					log.debug(" trying to requeue sms");
					smsQueueDao.updateStatus(smsQueue.get(i), Constants.SMS_STATUS_4);
				}
			}else{
				log.debug("contain success msg, cannot requeue");
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return queue;
	}
	
	private void statusUpdating(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		log.debug("status updating");
		try {
			log.debug("return Codes: "+returnCodes);
			for(int i=0;i<returnCodes.size();i++){
				if(returnCodes.get(i).contains(this.telcoEsms.getSuccessCode())){
					log.debug("sms success sent");
					smsQueue.get(i).setSmsStatus(Constants.SMS_STATUS_1);
				}else{
					log.debug("sms failed to send");
					smsQueue.get(i).setRemarks("Error: " + returnCodes.get(i));
					
					smsQueue.get(i).setSmsStatus(Constants.SMS_STATUS_2);
				}
				
				smsQueue.get(i).setSmsStatusBy("automated");
				smsQueue.get(i).setSmsStatusDatetime(new Date());
				log.debug(" finished ");
			}
			
			log.debug(" trying to save sms and delete record in smsQueue");
			log.debug("smsQueue: "+smsQueue);
			for(int i=0;i<smsQueue.size();i++){
				log.debug("smsQueue size: "+smsQueue.size());
				log.debug("smsQueue: "+smsQueue.get(i));
				if(smsDao.createObjectBySmsQueue(smsQueue.get(i))){
					log.debug("successfully create sms: "+smsQueue.get(i).getId());
					smsQueueDao.deleteById(smsQueue.get(i).getId());
				}else{// failed to save sms, skip delete
					log.debug("failed to save sms, update queue sms so that won't be process again");
					smsQueueDao.update(smsQueue.get(i));
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	private boolean updateQuota(SmsQuota theQuota, ArrayList<String> returnCodes){
		boolean status=true;
		int quotaAdjustment=0;
		try{
			if(theQuota!=null){
				for(int i=0;i<returnCodes.size();i++){
					if(returnCodes.get(i).contains(this.telcoEsms.getSuccessCode())){
						quotaAdjustment-=1;// collect no. of quota to be deduct
					}else{// failed SMS, no need deduct quota
					}
				}
				
				if(quotaAdjustment<0){
					ISmsQuotaDao smsQuotaDao = (ISmsQuotaDao) SgwCoreHibernateUtil.getBean("smsQuotaDao");
					List quotaList=smsQuotaDao.findSmsQuota("from SmsQuota where id=?", new Object[]{theQuota.getId()});// Get latest quota
					if(quotaList.size()==1){
						theQuota=(SmsQuota)quotaList.get(0);
						theQuota.setQuota(theQuota.getQuota()+quotaAdjustment);
						theQuota.setModifiedBy("SenderThread");
						theQuota.setModifiedDatetime(new Date());
						smsQuotaDao.updateSmsQuota(theQuota);
					}else{// No quota found
					}
				}
			}else{// No quota found
			}
		}catch(Exception ex){
			status=false;
		}
		return status;
	}
	
	public void sendFailureEmailNotification(Telco theTelco, String to, SmsCfg theCfg, long disabledTime, int failCounter){
		SendMailBean theMail=new SendMailBean();
		
		String subject="Telco "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+" - "+CommonUtils.NullChecker(theTelco.getStatus(), String.class).toString();
		String message="Telco Name: "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
		"Telco Status: "+theTelco.getStatus()+" with failing "+failCounter+" time(s)"+Constants.SPECIAL_CHAR_NEWLINE+
		"Telco Status Duration: "+disabledTime/60/1000+" minute(s)"+Constants.SPECIAL_CHAR_NEWLINE;
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmailSender(),String.class).toString().length()>0?theCfg.getSmsFailureNotificationEmailSender():Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		theMail.send();
	}
	
	public void sendFailureSmsNotification(Telco theTelco, String to, SmsCfg theCfg, long disabledTime, int failCounter){
		try{
			String message="Telco "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: "+theTelco.getStatus()+" with failing "+failCounter+" time(s)"+Constants.SPECIAL_CHAR_NEWLINE+
			"Duration: "+disabledTime/60/1000+" minute(s)"+Constants.SPECIAL_CHAR_NEWLINE;
			
			int deptId=0;
			String dept=Constants.STR_EMPTY;
			IUserDao userDao;
			userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
			User usermodal = userDao.findByUserId(theCfg.getSmsFailureNotificationSmsSender());
			if (usermodal == null){
			}else{
				GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
				Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
				if (groupmodal != null){
					deptId=groupmodal.getId();
					dept=groupmodal.getDeptName();
				}
			}
			
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
			
			SmsQueue aSms=new SmsQueue();
			aSms.setCreatedBy(theCfg.getFtpFailureNotificationSmsSender());
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			aSms.setTotalSms(msgArray.size());
			aSms.setMobileNo(to);
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_4);
			aSms.setSmsStatusBy("SenderThread");
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(theCfg.getFtpFailureNotificationSmsSender());
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
			smsDao.insert(aSms);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	/*private void failStatusUpdating(SmsQueue smsQueue, String returnCodes) {
	 try {
	 smsQueue.setRemarks("Error: " + returnCodes);
	 log.debug(" trying to save record in sms");
	 smsQueue.setSmsStatus(Constants.SMS_STATUS_2);
	 smsQueue.setSmsStatusBy("automated");
	 smsQueue.setSmsStatusDatetime(new Date());
	 smsDao.createObjectBySmsQueue(smsQueue);
	 log.debug(" trying to delete record in smsQueue");
	 smsQueueDao.deleteById(smsQueue.getId());
	 } catch (Exception e) {
	 log.error(e.getMessage(), e);
	 }
	 }*/
	
	/*private void successStatusUpdating(SmsQueue smsQueue, String returnCodes) {
	 try {
	 log.debug(" trying to save record in sms");
	 smsQueue.setSmsStatus(Constants.SMS_STATUS_1);
	 smsQueue.setSmsStatusBy("automated");
	 smsQueue.setSmsStatusDatetime(new Date());
	 smsDao.createObjectBySmsQueue(smsQueue);
	 log.debug(" trying to delete record in smsQueue");
	 smsQueueDao.deleteById(smsQueue.getId());
	 log.debug(" finished ");
	 } catch (Exception e) {
	 log.error(e.getMessage(), e);
	 }
	 }*/
	
	/*private void sleepAWhile() {
	 try {
	 // log.debug("sleep awhile");
	  Thread.yield();
	  Thread.sleep(5000);
	  } catch (InterruptedException e) {
	  log.error(e.getMessage(), e);
	  e.printStackTrace();
	  }
	  
	  }*/
	
	/*private void sleepforConnectionFailed() {
	 try {
	 log.debug("sleep for Connection Failed");
	 Thread.yield();
	 //Thread.sleep(1000);
	  Thread.sleep(60000);
	  } catch (InterruptedException e) {
	  log.error(e.getMessage(), e);
	  e.printStackTrace();
	  }
	  }*/
	
	private void sleepforRetryInterval(int retryInterval) {
		try {
			log.debug("sleep for retry interval");
			Thread.yield();
			Thread.sleep(retryInterval);
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
	
	public void shutDown() {
		isRunning = false;
	}
	
	public String sendQueueThresholdNotification(SmsCfg theCfg){
		String returnCodes=Constants.STR_EMPTY;

		List<String> toList=CommonUtils.strToArray(theCfg.getQueueSmsThresholdNotificationEmail(), CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
		for(int i=0;i<toList.size();i++){
			if(i!=0){returnCodes+=Constants.SPECIAL_CHAR_NEWLINE;}
			returnCodes+=sendQueueThresholdEmailNotification(theCfg, theCfg.getQueueSmsThresholdNotificationEmailSender(), toList.get(i));
		}
		
		toList=CommonUtils.strToArray(theCfg.getQueueSmsThresholdNotificationSms(), CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
		for(int i=0;i<toList.size();i++){
			if(i!=0){returnCodes+=Constants.SPECIAL_CHAR_NEWLINE;}
			returnCodes+=sendQueueThresholdSmsNotification(theCfg, theCfg.getQueueSmsThresholdNotificationSmsSender(), toList.get(i));
		}
		
		return returnCodes;
	}
	
	public String sendQueueThresholdEmailNotification(SmsCfg theCfg, String from, String to){
		String returnCodes=Constants.STR_EMPTY;
		SendMailBean theMail=new SendMailBean();
		String subject="Telco "+CommonUtils.NullChecker(telcoEsms.getTelcoName(), String.class).toString()+" - Hit SMS Queue Limit";
		String message="Telco: "+CommonUtils.NullChecker(telcoEsms.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: Hit SMS Queue Limit "+CommonUtils.NullChecker(theCfg.getQueueSmsThreshold(), String.class).toString()+" with "+telcoEsms.getSmsOnHand()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status Time: "+new Date()+Constants.SPECIAL_CHAR_NEWLINE;

		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(from,String.class).toString().length()>0?from:Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		String mailStatus=theMail.send();
		if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
		}else{
			returnCodes="SMS Queue Threshold email notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
				"Details: "+mailStatus;
		}
		return returnCodes;
	}
	
	public String sendQueueThresholdSmsNotification(SmsCfg theCfg, String from, String to){
		String returnCodes=Constants.STR_EMPTY;
		try{
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
			String message="Telco: "+CommonUtils.NullChecker(telcoEsms.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: Hit Queue Limit "+CommonUtils.NullChecker(theCfg.getQueueSmsThreshold(), String.class).toString()+" with "+telcoEsms.getSmsOnHand()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status Time: "+theFormatter.format(new Date())+Constants.SPECIAL_CHAR_NEWLINE;

			int deptId=0;
			String dept=Constants.STR_EMPTY;
			IUserDao userDao;
			userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
			User usermodal = userDao.findByUserId(theCfg.getQueueSmsThresholdNotificationSmsSender());
			if (usermodal == null){
			}else{
				GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
				Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
				if (groupmodal != null){
					deptId=groupmodal.getId();
					dept=groupmodal.getDeptName();
				}
			}
			
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
			ArrayList<String> lastSmsIdList=new ArrayList<String>();
			String lastSmsId=Constants.STR_EMPTY;
			
			SmsQueue aSms=new SmsQueue();
			aSms.setCreatedBy(theCfg.getQueueSmsThresholdNotificationSmsSender());
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			aSms.setTotalSms(msgArray.size());
			aSms.setMobileNo(to);
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_4);
			aSms.setSmsStatusBy("SenderThread");
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(theCfg.getQueueSmsThresholdNotificationSmsSender());
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
			smsDao.insert(aSms);
		}catch(Exception ex){
			ex.printStackTrace();
			returnCodes="Queue SmsThreshold SMS notification failed to be sent out to: "+to+Constants.SPECIAL_CHAR_NEWLINE+
			"Details: "+ex.getCause();
		}
		return returnCodes;
	}
	
	public void checkQueueThreshold(SmsCfg theCfg){
		log.debug("checkQueueThreshold");
		try{
			// Check queue threshold
			ITelcoEsmsDao telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
			this.telcoEsms=telcoEsmsDao.findById(this.telcoEsms.getId());
			
			log.debug("notification enabled");
			if(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotification(), String.class).toString().length()>0){// Notification option enabled
				log.debug("threshold: "+theCfg.getQueueSmsThreshold());
				log.debug("sms on hand: "+this.telcoEsms.getSmsOnHand());
				if(CommonUtils.NullChecker(theCfg.getQueueSmsThreshold(), String.class).toString().length()>0&&
					Integer.valueOf(CommonUtils.NullChecker(this.telcoEsms.getSmsOnHand(), Integer.class).toString())>Integer.valueOf(theCfg.getQueueSmsThreshold())){// Threshold reach
					log.debug("threshold reach");
					log.debug("theCfg.getQueueSmsThresholdPeriod(): "+theCfg.getQueueSmsThresholdPeriod());
					log.debug("this.telcoEsms.getSmsQueueThresholdNotificationDatetime(): "+this.telcoEsms.getSmsQueueThresholdNotificationDatetime());
					log.debug("new Date().getTime(): "+new Date().getTime());
					log.debug("Long.valueOf(theCfg.getQueueSmsThresholdPeriod())*60*1000: "+(Long.valueOf(theCfg.getQueueSmsThresholdPeriod())*60*1000));
					if((CommonUtils.NullChecker(theCfg.getQueueSmsThresholdPeriod(), String.class).toString().length()>0&&
							(this.telcoEsms.getSmsQueueThresholdNotificationDatetime()==null||(CommonUtils.NullChecker(this.telcoEsms.getSmsQueueThresholdNotificationDatetime(), String.class).toString().length()>0&&
							new Date().getTime()>=(this.telcoEsms.getSmsQueueThresholdNotificationDatetime().getTime()+Long.valueOf(theCfg.getQueueSmsThresholdPeriod())*60*1000))))){//notification has reach the recheck time or notification timeframe
						log.debug("reach time to send notification");
						boolean notified=false;
						if(theCfg.getQueueSmsThresholdNotification().contains("E")){
							String[] toList=CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
							for(int i=0;i<toList.length;i++){
								sendQueueThresholdEmailNotification(theCfg, theCfg.getQueueSmsThresholdNotificationEmailSender(), toList[i]);
								notified=true;
							}
						}
						
						if(theCfg.getQueueSmsThresholdNotification().contains("S")){
							String[] toList=CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
							for(int i=0;i<toList.length;i++){
								sendQueueThresholdSmsNotification(theCfg, theCfg.getQueueSmsThresholdNotificationSmsSender(), toList[i]);
								notified=true;
							}
						}
						
						if(notified){// Have perform notification action, record the notification time
							this.telcoEsms.setSmsQueueThresholdNotificationDatetime(new Date());
							this.telcoEsms.setModifiedBy("SenderThread");
							this.telcoEsms.setModifiedDatetime(new Date());
							telcoEsmsDao.update(telcoEsms);
						}
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			log.debug("Error in checking sms queue threshold: "+ex.getLocalizedMessage());
		}
		log.debug("End checkQueueThreshold");
	}

}
