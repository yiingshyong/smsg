package com.bcb.sgwcore.senderprocessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.dao.ISgwPriorityDao;
import com.bcb.crsmsg.dao.ISmsDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.modal.Priority;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class TelcoEsmsController extends Thread{
	
	boolean isRunning=false;
	ITelcoEsmsDao telcoEsmsDao = null;
	Log log = LogManager.getLog();
	List<Telco> telcoEsmsList=null; // All Telco list
	int totalOperatorSize=0; // telcoEsmsList size
	List<SenderThread> threadList = null;
	//MainSenderProcessor msp=null;
	
//	object
	List<Priority> priorityList=null;
	
	ISgwPriorityDao sgwPriorityDao =null;
	
	//Map telcoEsmsMap =null; 
	
	Map<Integer,Map> senderThreadMap =null;
	
	ISmsQueueDao smsQueueDao=null;
	
	public TelcoEsmsController(){
		log.debug("TelcomEsmsController initiation");
		try {
			threadList = new ArrayList<SenderThread>();
			senderThreadMap = Collections.synchronizedMap(new HashMap<Integer,Map>());
			settingUp();
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}	
	}
	
	public void shutdown(){
		for (SenderThread senderThread : threadList) {
			senderThread.shutDown();
		}
	}
	
	public void settingUp(){
		log.debug("TelcoEsmsController - settingUp()");
		loadAllDao();
		for (Telco operator : telcoEsmsList) {// Loop through telco list, create sender thread for each telco, regardless of status
			log.debug("Telco: "+operator.getTelcoName());
			ITelcoSmsSender telcoSmsSender;
			try {
				log.debug("Instantiate Telco Sender Class");
				telcoSmsSender = (ITelcoSmsSender) Class.forName(operator.getTelcoClassName()).newInstance();// one telcoSmsSender for each telco
				SenderThread senderThread = new SenderThread(operator,telcoSmsSender);// one sender thread for each telco
				log.debug("operator.getPriority(): "+operator.getPriority());
				log.debug("senderThread: "+senderThread);
				storeSenderThreadMapById(operator.getId(),senderThread);// bind sender thread with telco id
				threadList.add(senderThread);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}	
		}
		
	}
	
	public void run(){
		for (SenderThread senderThread : threadList) {// Thread for Telco
			Thread tt = new Thread(senderThread);
			tt.start();
		}
	}
	
	private void loadAllDao() {
		log.debug("TelcoEsmsController - loadAllDao()");
		try{
			
			smsQueueDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
			
			telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
			/*
			 ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");// Keep checking for new smsCfg
			 SmsCfg theCfg = cfgDao.findByCfgId(Constants.APPSVERSION);// Keep checking for smsCfg
			 */
			
			telcoEsmsDao.initialiseAllTelco("update Telco set smsOnHand=?", new Object[]{0});// reset all telco sms on hand to 0 as processor restarted
			
			telcoEsmsList = telcoEsmsDao.findAll();
			
			log.debug("telco list: "+telcoEsmsList);
			totalOperatorSize=telcoEsmsList.size();
			log.debug("telco size: "+telcoEsmsList.size());
			log.debug("total " + totalOperatorSize);
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		log.debug("Exit TelcoEsmsController - loadAllDao");
	}
	
	/**
	 * 
	 * @param smsList the queue sms list passed from MainSenderProcessor
	 * @param theCfg the sms cfg passed from MainSenderProcessor
	 */
	public void add(List<SmsQueue> smsList, SmsCfg theCfg){
		log.debug("add in TelcoEsmsController");
		try{
			SenderThread st=null;
			List<SenderThread> telcoEsms = getSenderThreadList();
			int total=telcoEsms.size();
			log.debug("telcoEsms size: "+total);
			if (total>0){// got telco, either active or disabled
				// Get case to test on the inactive telco, activate available disabled telco
				telcoEsmsList = telcoEsmsDao.findAll();
				for(Telco aTelco:telcoEsmsList){
					st=telcoEsms.get(aTelco.getId()-1);
					st.checkQueueThreshold(theCfg);
					
					if(smsList.size()>1){// Only test on inactive telco when have more then one queue sms, so that won't trapped in the testing
						log.debug("sms list size: "+smsList.size());
						//SmsQueue sms=smsList.get(smsList.size()-1);//get the last sms in list
						SmsQueue sms=smsList.get(0);//get the first sms in list, due to last portion of sms queue list contains concatenate sms
						log.debug("aTelco.getStatus(): "+aTelco.getStatus());
						if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().length()==0){// Only perform test if its not a concatenate sms
							if(aTelco.getStatus().equals(Constants.STATUS_DISABLED)){
								sms.setTelco(aTelco.getId());
								smsQueueDao.updateStatus(sms, Constants.SMS_STATUS_5);
								st.addSms(sms);// adding queue sms into sender thread to process, to test if the telco is up
								smsList.remove(sms);
							}
						}else{// is a concatenate sms, not going for testing
							
						}
					}
				}
				
				Integer telco=0;
				
				for (SmsQueue sms : smsList) {// loop through queue sms list
					if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().length()==0){// is single msg
						log.debug("is single sms");
						sms=populateTelco(sms, theCfg, true);
						telco=0;
					}else{// is concatenate sms, should populate into same telco as initial concatenate msg
						if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().equals(sms.getId().toString())){// first concatenate msg
							log.debug("is first concatenate msg");
							sms=populateTelco(sms, theCfg, true);
							telco=sms.getTelco();
						}else{// mid or end of concatenate msg
							log.debug("is mid or end of concatenate msg");
							sms.setTelco(telco);
							sms=populateTelco(sms, theCfg, true);
						}
					}
					
					log.debug("sender thread list size: "+telcoEsms.size());
					log.debug("sms.getTelco(): "+sms.getTelco());
					if(sms.getTelco()>0){
						log.debug("sender thread: "+(sms.getTelco()-1));
						st=telcoEsms.get(sms.getTelco()-1);// retrieve sender thread
						st.addSms(sms);// adding queue sms into sender thread to process
					}else{// No telco found, should failover the sms to other telco, neclect any routing rules
						sms.setRemarks("Routing rules based telco not available. Failed over telco!");
						
						if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().length()==0){// is single msg
							log.debug("is single sms");
							sms=populateTelco(sms, theCfg, false);
							telco=0;
						}else{// is concatenate sms, should populate into same telco as initial concatenate msg
							if(CommonUtils.NullChecker(sms.getConcatenateSms(), String.class).toString().equals(sms.getId().toString())){// first concatenate msg
								log.debug("is first concatenate msg");
								sms=populateTelco(sms, theCfg, false);
								telco=sms.getTelco();
							}else{// mid or end of concatenate msg
								log.debug("is mid or end of concatenate msg");
								sms.setTelco(telco);
								sms=populateTelco(sms, theCfg, false);
							}
						}
						
						log.debug("sender thread list size: "+telcoEsms.size());
						if(sms.getTelco()>0){
							log.debug("sender thread: "+(sms.getTelco()-1));
							st=telcoEsms.get(sms.getTelco()-1);// retrieve sender thread
							st.addSms(sms);// adding queue sms into sender thread to process
						}else{// All telcos are unavailable
							//failStatusUpdating(sms, "No telco available");
							smsList=new ArrayList<SmsQueue>();
						}
					}
				}
			}else{
				log.error("no available telco sms");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		log.debug("Exist add in TelcoEsmsController");
	}
	
	private void failStatusUpdating(SmsQueue smsQueue, String returnCodes) {
		try {
			smsQueue.setRemarks("Error: " + returnCodes);
			log.debug(" trying to save record in sms");
			smsQueue.setSmsStatus(Constants.SMS_STATUS_2);
			smsQueue.setSmsStatusBy("automated");
			smsQueue.setSmsStatusDatetime(new Date());
			ISmsDao smsDao = (ISmsDao) SgwCoreHibernateUtil.getBean("smsDao");
			smsDao.createObjectBySmsQueue(smsQueue);
			log.debug(" trying to delete record in smsQueue");
			smsQueueDao.deleteById(smsQueue.getId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Select approriate telco
	 * @param smsQueue
	 * @param theCfg
	 * @return
	 */
	public SmsQueue populateTelco(SmsQueue smsQueue, SmsCfg theCfg, boolean routingRules){
		try{
			log.debug("populateTelco");
			List<Telco> telcoEsmsList=new ArrayList<Telco>();
			
			Telco aTelco=null;
			if(theCfg.getSmsTelcoRouting().equals(Constants.FORM_FIELD_TELCO_ROUTING_PREFIX)&&routingRules){
				if(CommonUtils.NullChecker(smsQueue.getTelco(), String.class).toString().length()>0&&smsQueue.getTelco()>0){
					aTelco=telcoEsmsDao.findAvailableTelcoById(smsQueue.getTelco());
				}else{
					telcoEsmsList = telcoEsmsDao.findAllTelcoEsmsAvailableByTelcoRoutingMode(theCfg.getSmsTelcoRouting(), theCfg.getSmsTelcoRoutingParam());// Keep checking for telco
					aTelco=selectTelco(theCfg.getSmsTelcoRouting(), smsQueue.getMobileNo(), telcoEsmsList);
				}
				if(aTelco!=null){// telco routing by prefix
					log.debug("Route by prefix");
					smsQueue.setTelco(aTelco.getId());
					
					log.debug("Telco ID: "+aTelco.getId());
					log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
					aTelco.setSmsOnHand(Integer.toString(Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHand(), Integer.class).toString())+1));
					log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
					aTelco.setModifiedBy("populateTelco");
					aTelco.setModifiedDatetime(new Date());
					telcoEsmsDao.update(aTelco);
					smsQueueDao.updateStatus(smsQueue, Constants.SMS_STATUS_5);
				}else{
					smsQueue.setTelco(0);
					smsQueueDao.update(smsQueue);
				}
			}else{// telco routing not by prefix
				log.debug("Not route by prefix");
				if(CommonUtils.NullChecker(smsQueue.getTelco(), String.class).toString().length()>0&&smsQueue.getTelco()>0){
					aTelco=telcoEsmsDao.findAvailableTelcoById(smsQueue.getTelco());
				}else{
					telcoEsmsList = telcoEsmsDao.findAllTelcoEsmsAvailable();// Get telco regardless of routing rules - for failover situation
				}
				
				if(aTelco!=null&&(aTelco.getSmsOnHandLimit()!=null&&aTelco.getSmsOnHand()!=null
						&&Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHand(), Integer.class).toString())<Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHandLimit(), Integer.class).toString()))){
					smsQueue.setTelco(aTelco.getId());
					smsQueueDao.updateStatus(smsQueue, Constants.SMS_STATUS_5);
					
					log.debug("Telco ID: "+aTelco.getId());
					log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
					aTelco.setSmsOnHand(Integer.toString(Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHand(), Integer.class).toString())+1));
					log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
					aTelco.setModifiedBy("populateTelco");
					aTelco.setModifiedDatetime(new Date());
					telcoEsmsDao.update(aTelco);
				}else{
					for(int i=0;i<telcoEsmsList.size();i++){// telco list order by routing mode or without routing mode implemented for fail over
						aTelco=telcoEsmsList.get(i);
						
						if(aTelco.getSmsOnHandLimit()!=null&&aTelco.getSmsOnHand()!=null
								&&Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHand(), Integer.class).toString())<Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHandLimit(), Integer.class).toString())){
							smsQueue.setTelco(aTelco.getId());
							smsQueueDao.updateStatus(smsQueue, Constants.SMS_STATUS_5);
							
							log.debug("Telco ID: "+aTelco.getId());
							log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
							aTelco.setSmsOnHand(Integer.toString(Integer.valueOf(CommonUtils.NullChecker(aTelco.getSmsOnHand(), Integer.class).toString())+1));
							log.debug("telco smsonhand: "+aTelco.getSmsOnHand());
							aTelco.setModifiedBy("populateTelco");
							aTelco.setModifiedDatetime(new Date());
							telcoEsmsDao.update(aTelco);
							i=telcoEsmsList.size();
						}
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		log.debug("exit populateTelco");
		return smsQueue;
	}
	
	/**
	 * Select appropriate telco according to telco prefix routing mode
	 * @param telcoRoutingMode
	 * @param mobileNo
	 * @param telcoEsmsList
	 * @return
	 */
	private Telco selectTelco(String telcoRoutingMode, String mobileNo, List<Telco> telcoEsmsList){
		log.debug("selectTelco");
		log.debug("telcoRoutingMode: "+telcoRoutingMode);
		if (CommonUtils.NullChecker(telcoRoutingMode, String.class).toString()!=Constants.VALUE_0&&telcoRoutingMode.equals(Constants.FORM_FIELD_TELCO_ROUTING_PREFIX)){
			Telco aTelco=null;
			log.debug("telcoEsmsList: "+telcoEsmsList);
			for(int i=0;i<telcoEsmsList.size();i++){// loop through available telco list
				aTelco=telcoEsmsList.get(i);
				log.debug("aTelco.getTelcoPrefix(): "+aTelco.getTelcoPrefix());
				String[] prefix=CommonUtils.NullChecker(aTelco.getTelcoPrefix(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
				for(int j=0;j<prefix.length;j++){
					log.debug("prefix: "+prefix[j]);
					if(prefix[j].length()>0&&BusinessService.normalizeMobile(mobileNo).startsWith(prefix[j])&&aTelco.getStatus().equals(Constants.STATUS_ACTIVE)){
						return aTelco;
					}
				}
			}
			return null;
		}else{
			return null;
		}
	}
	
	public synchronized List getSenderThreadListByPriority(int priorityValue)
	{
		List<SenderThread> list=null;
		try{
			log.debug("priorityValue: "+priorityValue);
			Map map=(Map) senderThreadMap.get(priorityValue);// retrieve senderThread list according to priority
			log.debug("map: "+map);
			list = new ArrayList(map.values());
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		return list;
	}
	
	public synchronized List<SenderThread> getSenderThreadList()
	{
		log.debug("getSenderThreadList()");
		List<SenderThread> list=new ArrayList<SenderThread>();
		try{
			for(int i=0;i<telcoEsmsList.size();i++){
				Map map=(Map) senderThreadMap.get(((Telco)telcoEsmsList.get(i)).getId());// retrieve senderThread list
				log.debug("map values: "+map.values());
				list.addAll(map.values());
			}
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		log.debug("exit getSenderThreadList()");
		return list;
	}
	/*
	 private synchronized void storeSenderThreadMap(int priorityValue,SenderThread st)
	 {
	 log.debug("storeSenderThreadMap");
	 try{
	 log.debug("priorityValue: "+priorityValue);
	 Map stMap =(Map) senderThreadMap.get(priorityValue);
	 
	 if (stMap==null){
	 stMap = new HashMap();
	 }
	 stMap.put(st,st);
	 senderThreadMap.put(priorityValue,stMap);// store sender thread according to priority
	 }catch(Exception e){
	 log.error(e.getMessage(),e);
	 }
	 log.debug("Exit storeSenderThreadMap");
	 }
	 */
	private synchronized void storeSenderThreadMapById(int telcoId,SenderThread st)
	{
		log.debug("storeSenderThreadMapById");
		try{
			log.debug("telcoId: "+telcoId);
			Map stMap =(Map) senderThreadMap.get(telcoId);
			log.debug("stMap: "+stMap);
			
			if (stMap==null){
				stMap = new HashMap();
			}
			stMap.put(st,st);
			senderThreadMap.put(telcoId,stMap);// store sender thread according to telcoId
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		log.debug("Exit storeSenderThreadMapById");
	}
	
}
