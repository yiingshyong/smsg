package com.bcb.sgwcore.senderprocessor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.bcb.sgwcore.util.SgwParam;
import com.sun.net.ssl.internal.ssl.Provider;
// -edited 20191124

public class TelcoSmsSender implements ITelcoSmsSender {

	static Log log = LogManager.getLog();

	Telco operator = null;
	String mobileNo = "";
	/**
	 * Send single msg
	 */
	public String sendMessage(SmsQueue smsQueue) {
		initSSL();
		String returnCode = Constants.SPECIAL_DASH;
		try {
			returnCode=sendMessage(generateQueryString(smsQueue), smsQueue);
		} catch (Exception e) {
			log.error("i o error " + e.getMessage(), e);
			returnCode = SgwParam.RETURN_CODE_SGW_PROBLEM;
		}
		return returnCode;
	}	
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		log.debug("TelcoSmsSender-send concatenate sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String status=Constants.STR_EMPTY;
		
		for(int i=0;i<smsQueue.size();i++){
			if(returnCode.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// if one failed, subsequence concatenate sms should fail and retry
				returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
			}else if(status.length()>0){// contain not success case, status apply to remaining concatenate msg
				returnCode.add(status);
			}else{
				try {
					status=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
					returnCode.add(status);
					if(status.contains(operator.getSuccessCode())){
						status=Constants.STR_EMPTY;
					}else{
						
					}
				}catch(SecurityException se){
					throw new SecurityException(se.getMessage(),se);
				} catch (Exception e) {
					log.error("Caught exception. Due to: " + e.getMessage(), e);
					returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
				}
			}
		}
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		log.debug("retry send sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String status=Constants.STR_EMPTY;
		
		log.debug("smsQueue size: "+smsQueue.size());
		for(int i=0;i<smsQueue.size();i++){
			if(returnCodes.get(i).contains(operator.getSuccessCode())){// success, no need retry sending
				returnCode.add(returnCodes.get(i));
			}else{// retry send sms
				if(returnCode.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// if one failed, subsequence concatenate sms should fail and retry
					log.debug("contains error, apply to remaining msg");
					returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
				}else if(status.length()>0){// contain not success case, status apply to remaining concatenate msg
					log.debug("contains not success case, status apply to remaining msg");
					returnCode.add(status);
				}else{
					log.debug("retry sending sms");
					try {
						status=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
						log.debug("status: "+status);
						returnCode.add(status);
						if(status.contains(operator.getSuccessCode())){
							status=Constants.STR_EMPTY;
						}else{
							log.debug("retry status not success");
						}
					} catch (Exception e) {
						log.error("Exception while calling svc provider" + e.getMessage(), e);
						returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
					}
				}
			}
		}
		
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		return returnCode;
	}	

	public String generateQueryString(SmsQueue smsQueue) throws Exception{
    	if(smsQueue.getSmsType().isEncrypted() && !smsQueue.isDecrypted() && !StringUtils.isEmpty(smsQueue.getType()) && (smsQueue.getType().equals(Constants.SMS_SOURCE_TYPE_2) || smsQueue.getType().equals("T"))){
    		smsQueue.setMessage(SecurityManager.getInstance().decrypt(smsQueue.getMessage()));
    		smsQueue.setDecrypted(true);
    	}
		
		this.mobileNo = BusinessService.normalizeMobile(smsQueue.getMobileNo());
		return new StringBuffer().toString();
	}
	
	public String generateQueryString(SmsQueue[] smsQueue) throws Exception{
		return new StringBuffer().toString();
	}
	
	public String sendMessage(String queryString, SmsQueue smsQueue) throws Exception{
		//check time expired and return expired msg if expired
		if(BusinessService.hasSMSExpired(smsQueue)){
			return (AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.TIME_EXPIRED_SMS_REMARK));
		}
		
		URLConnection conn = null;
	
		URL objURL = new URL(queryString.toString());
		
		Date stopTime = null;
		
		if(StringUtils.isNotEmpty(operator.getProxyAddr()) && operator.getProxyPort() > 0){
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(operator.getProxyAddr(), operator.getProxyPort()));
			conn = objURL.openConnection(proxy);			
		}else{
			conn = objURL.openConnection();									
		}
		
		log.debug("conn: "+conn);

		String strLine = Constants.STR_EMPTY;
		String returnCode = Constants.STR_EMPTY;
		if(conn!=null){
			log.debug("TelcoSMSSender conn is not null");
			conn.setConnectTimeout(Integer.parseInt(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.HTTP_CLIENT_CONNECT_TIMEOUT))*1000);
			conn.setReadTimeout(Integer.parseInt(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.HTTP_CLIENT_CONNECT_READ))*1000);
			conn.setDoOutput(true);
			log.debug("TelcoSMSSender Create bufferreader");
			log.debug(queryString.toString());
			StringBuilder sb = new StringBuilder();
			sb.append(this.operator.getTelcoName()).append(" ").append(this.operator.getUrl()).append(" ").append(smsQueue.getId());
			log.info(sb.toString());
			Date startTime = new Date();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			int i = 0;
			while ((strLine = br.readLine()) != null) {
				returnCode += strLine;
				if (i > 2000) {
					break;
				}
				i++;
			}
			br.close();	
			log.debug("Message sent");
			stopTime = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS");
			sb.setLength(0);
			sb.append(" Response code: ").append(((HttpURLConnection)conn).getResponseCode())
			  .append(" Returned code: ").append(returnCode)
			  .append(" Id: ").append(smsQueue.getId())
			  .append(" Service Provider: ").append(this.operator.getTelcoName())
			  .append(" Mobile No: ").append(this.mobileNo)
			  .append(" Time utilized: ").append(stopTime.getTime()-startTime.getTime());
			log.info(sb.toString());
//			log.info( " Response code: " + ((HttpURLConnection)conn).getResponseCode() 
//					+ " Returned code: " + returnCode  
//					+ " Service Provider: " + this.operator.getTelcoName() 
//					+ " Mobile No: " + this.mobileNo 
//					+ " Start Time: " + sdf.format(startTime) 
//					+ " End Time: " + sdf.format(stopTime) 
//					+ " Time utilized: " + (stopTime.getTime()-startTime.getTime())
//					);
			sdf = null; 
			startTime = null;
			stopTime = null;			
		}else{
			returnCode="Cannot establish connection with telco.";
		}
		
		return returnCode.toString();
	}
	
	public void initSSL(){
		System.setProperty("java.protocol.handler.pkgs",
				"com.sun.net.ssl.internal.www.protocol");
		//edited 20191124
		Security.addProvider(new Provider());
		//java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		//java.security.Security.addProvider(Security.getProvider("SUNJSSE"));
	}
	
	public void setupCfg(Telco operator) {
		this.operator = operator;
	}
	
	public static void main(String args[])
	{
		
		
		TelcoSmsSender sender=new TelcoSmsSender();
 
		try{
			
			//sender.sendMessage(a);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public String extractReturnCodes(String returnCodes){
		return returnCodes;
	}
	
	public void calculateCharge(SmsQueue theSms){
//		theSms.setSmsCharge(operator.getCostPerSmsInter());
//		theSms.setSmsChargeMode(Constants.SMS_CHARGE_MODE_1);
//		
//		String[] prefixList=CommonUtils.NullChecker(operator.getPrefixWithin(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
//		for(int i=0;i<prefixList.length;i++){
//			if(BusinessService.normalizeMobile(theSms.getMobileNo()).toString().startsWith(prefixList[i])){// mobile no. is in the telco within charge list
//				theSms.setSmsCharge(operator.getCostPerSmsWithin());
//				theSms.setSmsChargeMode(Constants.SMS_CHARGE_MODE_2);
//			}
//		}
		
	}
	
	//calculate charge need to change, just 5cent X 3 = 15 cent then store into smsCharge in sms_queue
	public void calculateCharge(ArrayList<SmsQueue> theSms, ArrayList<String> returnCodes){
		log.debug("calculate charge");
		
		log.debug("return Codes: "+returnCodes);
		for(int j=0;j<returnCodes.size();j++){
			if(returnCodes.get(j).contains(operator.getSuccessCode())){
				Integer smsRateId = BusinessService.selectSmsRate(this.operator.getSvcPvdId(), BusinessService.normalizeMobile(theSms.get(j).getMobileNo()));
				SmsRate smsRate = (SmsRate) DaoBeanFactory.getHibernateTemplate().get(SmsRate.class, smsRateId);
				theSms.get(j).setSmsCharge(smsRate.getRate().multiply(new BigDecimal(String.valueOf(theSms.get(j).getTotalSms()))).toString());
				theSms.get(j).setSmsChargeMode(Constants.SMS_CHARGE_MODE_1);
				theSms.get(j).setSmsRateId(smsRateId);
			}
		}
	}
}
