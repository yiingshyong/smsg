package com.bcb.sgwcore.senderprocessor;

import java.net.URLEncoder;
import java.util.ArrayList;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwParam;

public class TelcoSmsSender1 extends TelcoSmsSender {
	public String generateQueryString(SmsQueue smsQueue, String userHeader) throws Exception{
		StringBuffer queryString = new StringBuffer();
		String[] queryParam=operator.getUrlParam()!=null?operator.getUrlParam().split(CommonUtils.escapeDelimiter(Constants.SPECIAL_CHAR_PIPE)):null;
		int index=0;
		
		queryString.append(operator.getUrl());
		
		queryString.append(queryParam[index++]+"=").append(operator.getUserId().trim());
		queryString.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());
		queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));
		if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_ASCIITEXT)){
			queryString.append("&"+queryParam[index++]+"=").append("A");// English
		}else if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_UTF8)){
			queryString.append("&"+queryParam[index++]+"=").append("U");
		}
		queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));
		queryString.append("&"+queryParam[index++]+"=").append(userHeader);
		queryString.append("&"+queryParam[index++]+"=").append("TRUE");
		return queryString.toString();
	}
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String status=Constants.STR_EMPTY;
		Integer userHeaderMsgNo=1;
		String userHeader=smsQueue.size()>1?generateUserHeader(smsQueue.size()):Constants.STR_EMPTY;
		log.debug("userHeader: "+userHeader);
		
		for(int i=0;i<smsQueue.size();i++, userHeaderMsgNo++){
			if(returnCode.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// if one failed, subsequence concatenate sms should fail and retry
				returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
			}else if(status.length()>0){// contain not success case, status apply to remaining concatenate msg
				returnCode.add(status);
			}else{
				try {
					status=sendMessage(generateQueryString(smsQueue.get(i), userHeader.length()==0?userHeader:userHeader+(userHeaderMsgNo.toString().length()==1?Constants.VALUE_0+userHeaderMsgNo.toString():userHeaderMsgNo.toString())), smsQueue.get(i));
					returnCode.add(status);
					if(status.contains(operator.getSuccessCode())){
						status=Constants.STR_EMPTY;
					}else{
						
					}
				} catch (Exception e) {
					log.error("i o error " + e.getMessage(), e);
					returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
				}
			}
		}
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		log.debug("retry send sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String status=Constants.STR_EMPTY;
		Integer userHeaderMsgNo=1;
		String userHeader=smsQueue.size()>1?generateUserHeader(smsQueue.size()):Constants.STR_EMPTY;
		log.debug("userHeader: "+userHeader);
		
		log.debug("smsQueue size: "+smsQueue.size());
		for(int i=0;i<smsQueue.size();i++, userHeaderMsgNo++){
			if(returnCodes.get(i).contains(operator.getSuccessCode())){// success, no need retry sending
				returnCode.add(returnCodes.get(i));
			}else{// retry send sms
				if(returnCode.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// if one failed, subsequence concatenate sms should fail and retry
					log.debug("contains error, apply to remaining msg");
					returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
				}else if(status.length()>0){// contain not success case, status apply to remaining concatenate msg
					log.debug("contains not success case, status apply to remaining msg");
					returnCode.add(status);
				}else{
					log.debug("retry sending sms");
					try {
						status=sendMessage(generateQueryString(smsQueue.get(i), userHeader.length()==0?userHeader:userHeader+(userHeaderMsgNo.toString().length()==1?Constants.VALUE_0+userHeaderMsgNo.toString():userHeaderMsgNo.toString())), smsQueue.get(i));
						log.debug("status: "+status);
						returnCode.add(status);
						if(status.contains(operator.getSuccessCode())){
							status=Constants.STR_EMPTY;
						}else{
							log.debug("retry status not success");
						}
					} catch (Exception e) {
						log.error("i o error " + e.getMessage(), e);
						returnCode.add(SgwParam.RETURN_CODE_SGW_PROBLEM);
					}
				}
			}
		}
		
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		
		return returnCode;
	}	
	
	/**
	 * Generate concatenate msg user header, must not be more then 14 hexadecimal characters, 7 ASCII characters
	 * @return
	 */
	private String generateUserHeader(Integer smsSize){
		String userHeader=Constants.STR_EMPTY;
		userHeader=Constants.TELCO_MAXIS_URL_USERHEADER+(smsSize.toString().length()==1?Constants.VALUE_0+smsSize:smsSize);
		return userHeader;
	}
}
