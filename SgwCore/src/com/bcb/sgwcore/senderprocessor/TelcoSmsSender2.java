package com.bcb.sgwcore.senderprocessor;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.EncryptionManager;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwParam;

public class TelcoSmsSender2 extends TelcoSmsSender {
	@SuppressWarnings("deprecation")
	public String generateQueryString(SmsQueue smsQueue) throws Exception{
		super.generateQueryString(smsQueue);
		StringBuffer queryString = new StringBuffer();
		StringBuffer queryStringEncryption = new StringBuffer();
		String[] queryParam=operator.getUrlParam()!=null?operator.getUrlParam().split(CommonUtils.escapeDelimiter(Constants.SPECIAL_CHAR_PIPE)):null;
		int index=0;
		queryString.append(operator.getUrl());
		
		String encryption = operator.getEncryptionStatus();
		
		if(encryption.equals("TRUE")){
			
			queryStringEncryption.append(queryParam[index++]+"=").append(operator.getUserId().trim());//user
			queryStringEncryption.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());//pass
			queryStringEncryption.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));//to

			queryStringEncryption.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));//msg			

			if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_ASCIITEXT)){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("1");//type
			}else if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_UTF8)){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("6");//type
			}
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//charge
			
			if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("65")){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("146073300");//sp for sg
			}else if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("60")){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("32988");//sp for m'sia
			}
			
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//recid
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//moid
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//groupID
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//subsid
			
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("17");//server 17
			if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("65")){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("146073300");//brand for sg
			}else if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("60")){
				queryStringEncryption.append("&"+queryParam[index++]+"=").append("32988");//brand for m'sia
			}
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//text
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//url
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("");//telcoid
			
		}else{
			queryString.append(queryParam[index++]+"=").append(operator.getUserId().trim());//user
			queryString.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());//pass
			queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));//to
//			if(smsQueue.getSmsType().isEncrypted() && !StringUtils.isEmpty(smsQueue.getType()) && smsQueue.getType().equals(Constants.SMS_SOURCE_TYPE_2)){
//				String decrypted = SecurityManager.getInstance().decrypt(smsQueue.getMessage());
//				queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(decrypted, "UTF-8"));//msg						
//			}else{
				queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));//msg			
//			}
			if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_ASCIITEXT)){
				queryString.append("&"+queryParam[index++]+"=").append("1");//type
			}else if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_UTF8)){
				queryString.append("&"+queryParam[index++]+"=").append("6");//type
			}
			queryString.append("&"+queryParam[index++]+"=").append("");//charge
			
			if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("65")){
				queryString.append("&"+queryParam[index++]+"=").append("146073300");//sp for sg
			}else if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("60")){
				queryString.append("&"+queryParam[index++]+"=").append("32988");//sp for m'sia
			}
			
			queryString.append("&"+queryParam[index++]+"=").append("");//recid
			queryString.append("&"+queryParam[index++]+"=").append("");//moid
			queryString.append("&"+queryParam[index++]+"=").append("");//groupID
			queryString.append("&"+queryParam[index++]+"=").append("");//subsid
			/* Change server to 17 for centralising all the sms destination
			if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("6012")||BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("6017")){
				queryString.append("&"+queryParam[index++]+"=").append("15");//server 15 - for maxis, for not barred shortcode on maxis no.
			}else{
				queryString.append("&"+queryParam[index++]+"=").append("10");//server 10 - for maxis & others
			}*/
			queryString.append("&"+queryParam[index++]+"=").append("17");//server 17
			if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("65")){
				queryString.append("&"+queryParam[index++]+"=").append("146073300");//brand for sg
			}else if(BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith("60")){
				queryString.append("&"+queryParam[index++]+"=").append("32988");//brand for m'sia
			}
			queryString.append("&"+queryParam[index++]+"=").append("");//text
			queryString.append("&"+queryParam[index++]+"=").append("");//url
			queryString.append("&"+queryParam[index++]+"=").append("");//telcoid
		}
		
		
		log.info("encryption status:"+encryption);
		if(encryption.equals("TRUE")){
		
			String msgData = queryStringEncryption.toString();
			String encryptedData = URLEncoder.encode((EncryptionManager.getInstance().encrypt(msgData)),"UTF-8");
			
			String finalQuery = queryString.toString()+encryptedData;
			log.info("finalQuery:"+finalQuery);
			return finalQuery;
		}else{
			log.info("queryString.toString():"+queryString.toString());
			return queryString.toString();
		}
	}
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		log.debug("TelcoSmsSender2-send concatenate sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String concatenateSms=Constants.STR_EMPTY;
		String tempMsg=Constants.STR_EMPTY;
		String returnStatus=Constants.STR_EMPTY;
		
		for(int i=0;i<smsQueue.size();i++){
			concatenateSms+=smsQueue.get(i).getMessage();
			if(i==smsQueue.size()-1){// final concatenate sms, send the sms
				try {
					tempMsg=smsQueue.get(i).getMessage();
					smsQueue.get(i).setMessage(concatenateSms);
					returnStatus=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
					smsQueue.get(i).setMessage(tempMsg);
				} catch (SecurityException se){
					throw new SecurityException(se);
				} catch (Exception e) {
					log.error("i o error " + e.getMessage(), e);
					returnStatus=SgwParam.RETURN_CODE_SGW_PROBLEM;
				}
			}
		}
		
		for(int i=0;i<smsQueue.size();i++){
			returnCode.add(returnStatus);
		}
		
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		ArrayList<String> returnCode=sendMessage(smsQueue);
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		return returnCodes;
	}	
}
