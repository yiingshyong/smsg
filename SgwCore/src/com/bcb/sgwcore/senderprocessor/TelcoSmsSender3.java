package com.bcb.sgwcore.senderprocessor;

import java.net.URLEncoder;
import java.util.ArrayList;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwParam;

public class TelcoSmsSender3 extends TelcoSmsSender {
	public String generateQueryString(SmsQueue smsQueue) throws Exception{
		super.generateQueryString(smsQueue);		
		StringBuffer queryString = new StringBuffer();
		String[] queryParam=operator.getUrlParam()!=null?operator.getUrlParam().split(CommonUtils.escapeDelimiter(Constants.SPECIAL_CHAR_PIPE)):null;
		int index=0;
		
		queryString.append(operator.getUrl());
		
		queryString.append(queryParam[index++]+"=").append(operator.getUserId().trim());//user
		queryString.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());//pass
		if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_ASCIITEXT)){
			queryString.append("&"+queryParam[index++]+"=").append("0");//type
		}else if(smsQueue.getMsgFormatType().equals(Constants.FORM_FIELD_CHARSET_UTF8)){
			queryString.append("&"+queryParam[index++]+"=").append("5");//type
		}
		
		queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));//to
//		if(smsQueue.getSmsType().isEncrypted() && !StringUtils.isEmpty(smsQueue.getType()) && smsQueue.getType().equals(Constants.SMS_SOURCE_TYPE_2)){
//			String decrypted = SecurityManager.getInstance().decrypt(smsQueue.getMessage());
//			queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(decrypted, "UTF-8"));//msg
//		}else{
			queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));//msg			
//		}
		queryString.append("&"+queryParam[index++]+"=").append("cimbbank");//from
		queryString.append("&"+queryParam[index++]+"=").append("MES01");//telcoid
		
		return queryString.toString();
	}
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		log.debug("TelcoSmsSender3-send concatenate sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String concatenateSms=Constants.STR_EMPTY;
		String tempMsg=Constants.STR_EMPTY;
		String returnStatus=Constants.STR_EMPTY;
		
		for(int i=0;i<smsQueue.size();i++){
			concatenateSms+=smsQueue.get(i).getMessage();
			if(i==smsQueue.size()-1){// final concatenate sms, send the sms
				try {
					tempMsg=smsQueue.get(i).getMessage();
					smsQueue.get(i).setMessage(concatenateSms);
					returnStatus=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
					smsQueue.get(i).setMessage(tempMsg);
				} catch (SecurityException se){
					throw new SecurityException(se);
				} catch (Exception e) {
					log.error("i o error " + e.getMessage(), e);
					returnStatus=SgwParam.RETURN_CODE_SGW_PROBLEM;
				}
			}
		}
		
		for(int i=0;i<smsQueue.size();i++){
			returnCode.add(returnStatus);
		}
		
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		ArrayList<String> returnCode=sendMessage(smsQueue);
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		return returnCodes;
	}	
}
