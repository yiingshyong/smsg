package com.bcb.sgwcore.senderprocessor;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.EncryptionManager;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.SgwParam;

public class TelcoSmsSender5 extends TelcoSmsSender {
	@SuppressWarnings("deprecation")
	public String generateQueryString(SmsQueue smsQueue) throws Exception{
		log.info("TelcoSmsSender5");
		
		super.generateQueryString(smsQueue);
		StringBuffer queryString = new StringBuffer();
		StringBuffer queryStringEncryption = new StringBuffer();
		String[] queryParam=operator.getUrlParam()!=null?operator.getUrlParam().split(CommonUtils.escapeDelimiter(Constants.SPECIAL_CHAR_PIPE)):null;
		int index=0;
		queryString.append(operator.getUrl());
		
		String encryption = operator.getEncryptionStatus();
		
		if(encryption.equals("TRUE")){
			
			queryStringEncryption.append(queryParam[index++]+"=").append(operator.getUserId().trim());//user--kh gw-username
			queryStringEncryption.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());//pass -- kh gw-password
			
			queryStringEncryption.append("&"+queryParam[index++]+"=").append("CIMB");// kh gw-to	
			queryStringEncryption.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));//to
			queryStringEncryption.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));//msg	 -- kh gw-text		

			
		}else{
			queryString.append(queryParam[index++]+"=").append(operator.getUserId().trim());//user  kh:gw-username
			queryString.append("&"+queryParam[index++]+"=").append(operator.getPassword().trim());//pass  kh:gw-password 
			
			queryString.append("&"+queryParam[index++]+"=").append("CIMB");//to kh:gw-from 
			queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(BusinessService.normalizeMobile(smsQueue.getMobileNo()), "UTF-8"));// tokh:gw-to
			queryString.append("&"+queryParam[index++]+"=").append(URLEncoder.encode(smsQueue.getMessage(), "UTF-8"));//msg	kh:gw-text		

		}
		
		
		log.info("encryption status:"+encryption);
		if(encryption.equals("TRUE")){
		
			String msgData = queryStringEncryption.toString();
			String encryptedData = URLEncoder.encode((EncryptionManager.getInstance().encrypt(msgData)),"UTF-8");
			
			String finalQuery = queryString.toString()+encryptedData;
			log.info("finalQuery:"+finalQuery);
			return finalQuery;
		}else{
			log.info("queryString.toString():"+queryString.toString());
			return queryString.toString();
		}
	}
	
	/**
	 * Send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue) {
		log.debug("TelcoSmsSender5-send concatenate sms");
		initSSL();
		ArrayList<String> returnCode = new ArrayList<String>();
		String concatenateSms=Constants.STR_EMPTY;
		String tempMsg=Constants.STR_EMPTY;
		String returnStatus=Constants.STR_EMPTY;
		
		for(int i=0;i<smsQueue.size();i++){
			concatenateSms+=smsQueue.get(i).getMessage();
			if(i==smsQueue.size()-1){// final concatenate sms, send the sms
				try {
					tempMsg=smsQueue.get(i).getMessage();
					smsQueue.get(i).setMessage(concatenateSms);
					returnStatus=sendMessage(generateQueryString(smsQueue.get(i)), smsQueue.get(i));
					smsQueue.get(i).setMessage(tempMsg);
				} catch (SecurityException se){
					throw new SecurityException(se);
				} catch (Exception e) {
					log.error("i o error " + e.getMessage(), e);
					returnStatus=SgwParam.RETURN_CODE_SGW_PROBLEM;
				}
			}
		}
		
		for(int i=0;i<smsQueue.size();i++){
			returnCode.add(returnStatus);
		}
		
		return returnCode;
	}	
	
	/**
	 * Retry send Concatenate msg
	 */
	public ArrayList<String> sendMessage(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		ArrayList<String> returnCode=sendMessage(smsQueue);
		for(int i=0;i<returnCodes.size();i++){// update the returnCodes list by object, not by return value
			returnCodes.set(i, returnCode.get(i));
		}
		return returnCodes;
	}	
}
