package com.bcb.sgwcore.senderprocessor.business;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class BatchInsertClient {

	public static void main(String[] args) {				
		try{
			new BatchInsertClient().insertSmsToDB(Integer.valueOf(args[0]), args[1], args[2], args[3]);
		}catch(Exception e){
			System.out.println("Error while running batch insert. System will exit.");
			e.printStackTrace();
		}
	}
	
	//param 1 - number of sms
	//param 2 - prefix
	
	public void insertSmsToDB(int smsNumber, String prefix, String message, String userId){
		User user = DaoBeanFactory.getUserDao().findByUserId(userId);
		SmsCfg theCfg = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
		String charSet = Constants.FORM_FIELD_CHARSET_ASCIITEXT;
		int charPerSms=CommonUtils.calculateCharPerSms(charSet, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
		ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);			
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();		
		
		for(int i=0; i < smsNumber; i++){
			System.out.println("Creating sms number " + i);
			SmsQueue aSMS=new SmsQueue();
			aSMS.setMobileNo(prefix + "1234567");
//			aSMS.setSmsScheduled(theForm.getScheduledSMS());
			aSMS.setMsgFormatType(charSet);
			aSMS.setMessage(message + i);
			aSMS.setTotalSms(msgArray.size());
			aSMS.setPriority(9);
			aSMS.setCreatedBy(userId);
			aSMS.setCreatedDatetime(new Date());
			aSMS.setSentBy(userId);
			
			aSMS.setSmsStatus(Constants.SMS_STATUS_4);
			aSMS.setApprovalBy("automated");
			aSMS.setApprovalDatetime(new Date());
			
			aSMS.setSmsStatusBy(userId);
			aSMS.setSmsStatusDatetime(new Date());
			aSMS.setTelco(0);
			aSMS.setType(Constants.SMS_SOURCE_TYPE_1);
			aSMS.setVersion(Integer.valueOf(Constants.APPSVERSION));
			aSMS.setDept(((Department)DBUtilsCrsmsg.get(Department.class, Integer.valueOf(user.getUserGroup()), hibernate_session)).getDeptName());			
			aSMS.setDeptId(Integer.valueOf(user.getUserGroup()));			
			try {
				DaoBeanFactory.getSmsQueueDao().insert(aSMS);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}		
	}
		
	public <A> A sendSmsToWeb(A type, A type2){
		return type;
	}
	
	public void sendBulkSms(String... params){
		
	}
	
	public void sendSmsBySocket(String... params){
		
	}
	
}
