package com.bcb.sgwcore.senderprocessor.business;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;


public class BlockingThreadPoolExecutor extends ThreadPoolExecutor {

	private static final Log log = LogManager.getLog();
	private BlockThenRunPolicy policy;
	
    public BlockingThreadPoolExecutor(
        int poolSize,
        int queueSize,
        long keepAliveTime,
        long maxBlockingTime,
        Callable<Boolean> blockingTimeCallback) {

        super(
                poolSize, // Core size
                poolSize, // Max size
                keepAliveTime,
                TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(
                    // to avoid redundant threads
                    queueSize
                ), 
                // our own RejectExecutionHandler � see below
                new BlockThenRunPolicy(
                    maxBlockingTime,
                    TimeUnit.MILLISECONDS,
                    blockingTimeCallback
                )
        );

    }

    @Override
    public void setRejectedExecutionHandler (RejectedExecutionHandler h) {
        throw new UnsupportedOperationException("setRejectedExecutionHandler is not allowed on this class.");
    }
    
	// -------------------------------------------------- 
    // Inner private class of BlockingThreadPoolExecutor
    // A reject policy that waits on the queue
    // -------------------------------------------------- 
    public static class BlockThenRunPolicy
        implements RejectedExecutionHandler {

        private long blockTimeout;
        private TimeUnit blocTimeoutUnit;
        private Callable<Boolean> blockTimeoutCallback;

		public BlockThenRunPolicy(long blockTimeout, TimeUnit blocTimeoutUnit, Callable<Boolean> blockTimeoutCallback) {
			super();
			this.blockTimeout = blockTimeout;
			this.blocTimeoutUnit = blocTimeoutUnit;
			this.blockTimeoutCallback = blockTimeoutCallback;
		}

        public void rejectedExecution(
            Runnable task,
            ThreadPoolExecutor executor) {           
        	
            BlockingQueue<Runnable> queue = executor.getQueue();
            boolean taskSent = false;

            while (!taskSent) {

                if (executor.isShutdown()) {                	
                    throw new RejectedExecutionException("ThreadPoolExecutor has shutdown while attempting to offer a new task.");
                }

                try {
                	log.info("Pool full, waiting for free slot.");
                    // offer the task to the queue, for a blocking-timeout
                    if (queue.offer(task, blockTimeout, blocTimeoutUnit)) {
                        taskSent = true;
                    }
                    else {
                        // task not accepted
                        Boolean result = null;
                        try {
                            result = blockTimeoutCallback.call();
                        }
                        catch(Exception e) {
                            throw new RejectedExecutionException("Error while invoking client's callback.Due to: " + e.getMessage(), e);
                        }
                        // check the Callback result
                        if(result == false) {
                            throw new RejectedExecutionException("User decided to stop waiting for task insertion");                        
                        }
                        else {
                            // user decided to keep waiting (may log it)
                            continue;
                        }
                    }
                }
                catch (InterruptedException e) {
                	log.warn("Interrupted. Exit waiting on queue. Check stack trace if it's not a manual shutdown");
                	e.printStackTrace();
                }

            } // end of while for InterruptedException 
        }

		public long getBlockTimeout() {
			return blockTimeout;
		}

		public void setBlockTimeout(long blockTimeout) {
			this.blockTimeout = blockTimeout;
		}

        // end of method rejectExecution

        // --------------------------------------------------

    } // end of inner private class BlockThenRunPolicy
    

}