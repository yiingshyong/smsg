package com.bcb.sgwcore.senderprocessor.business;

import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class BusinessProcessorAgent implements Runnable {
			
	private static final Log log = LogManager.getLog();
	private List<SmsQueue> inMemorySms;
	private SmsCfg smsConfig;
	private int toMqCount;
	private int toDbCount;
	
	public BusinessProcessorAgent(List<SmsQueue> sms, SmsCfg smsConfig) {
		this.inMemorySms = sms;
		this.smsConfig = smsConfig;
		toMqCount = 0;
		toDbCount = 0;
	}
	
	public void run() {	
		try{
			if(inMemorySms != null && !inMemorySms.isEmpty()){
				log.info("Start processing of " + inMemorySms.size() + " sms.");
				long startTime = System.currentTimeMillis();
				int size = inMemorySms.size();
				inMemorySms = BusinessService.discardTimeCtrlSms(inMemorySms);
				log.debug("Number of blocked sms: " + (size-inMemorySms.size()));
				for(SmsQueue smsQueue : inMemorySms){
					if(Thread.interrupted()){
						log.warn("Interrupted. Will exit processing. Check if shutdown called.");
						break;
					}
//					smsQueue = BusinessService.serviceProviderRouting(smsQueue, smsConfig);
					smsQueue = ServiceProviderRouter.route(smsQueue, smsConfig);
					if(smsQueue.getTelco() == 0) {
						log.warn("Will discard from sending as no service provider found for this sms.");
						smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
						DaoBeanFactory.getSmsQueueDao().update(smsQueue);					
						toDbCount++;
						continue;
					}
//					if(BusinessService.updateQuota(smsQueue, smsConfig, "BPA-", false)){ //- decrement
//						log.debug("Quota feature enabled. Minus from quota " + smsQueue.getTotalSms());
//						log.warn("Quota full for dept: " + smsQueue.getDept() + ". Will discard from sending."); 
//						smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
//						smsQueue.setSmsStatusDatetime(new Date());
//						smsQueue.setSmsStatusBy("BPA-QUOTA");
//						smsQueue.setRemarks("Quota full");
//						smsQueue.setPriority(99);//set to the lowest priority so that they won't be stuck on top of the queue
//						DaoBeanFactory.getSmsQueueDao().update(smsQueue);					
//						toDbCount++;
//						continue;
//					}					
					if(BusinessService.sendToMQ(smsQueue, smsConfig)){
						toMqCount++;
					}else{
						toDbCount++;
					}
				}
				log.info("End of Task. MQ: " + toMqCount + ", DB: "  + toDbCount + ". Summary: " + (toDbCount + toMqCount) + "/" + inMemorySms.size() + " sms processed in " + (System.currentTimeMillis()-startTime) + "ms");
			}
		}catch(Exception e){
			log.error("Exception caught. Due to: " + e.getMessage(), e);
		}
	}
	
}
