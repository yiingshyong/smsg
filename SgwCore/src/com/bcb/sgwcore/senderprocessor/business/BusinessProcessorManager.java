package com.bcb.sgwcore.senderprocessor.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.logging.Log;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.google.common.collect.Lists;

@ManagedResource
public class BusinessProcessorManager extends Thread implements Callable<Boolean>{

	private static final Log log = LogManager.getLog();
	private boolean isRunning = true;
	private SmsCfg smsConfig; 
	private ExecutorService workersMgr;
	private int noOfWorkerThread = 3; 
	private int runInterval = 60; // interval in second between subsequent run of manager
	private int workerKeepAliveTime = 15 * 60; //worker thread keep alive time in second, after this, idle thread will be teared down
	private int workerTimeWait = 15 * 60; // time wait in second, if worker thread is full, call() will be invoked after this period
	private int noOfSmsInMemory = 5000; // no of sms loaded from db into memory 
	private int msgPerWorkerThread = 1000; // no of sms for each worker
	private int workerThreadAwaitTerminationTimeout = 300; //TODO: get from db
	private Boolean notified = false;
	private Date lastStatusCheckTime = new Date();
 	private JMXConnector jmxConnector;	
	
	public BusinessProcessorManager() {
		try{
			log.info("Cleaning biz processor state in database ... ");
//			DaoBeanFactory.getSmsQueueDao().initialiseAllMessageStatus(Constants.SMS_STATUS_M, Constants.SMS_STATUS_4);
			resetMessageStatus();
			loadConfigFromDB();
			workersMgr = new BlockingThreadPoolExecutor(noOfWorkerThread, 1, workerKeepAliveTime * 1000, workerTimeWait * 1000, this);
		}catch(Exception e){
			throw new RuntimeException("Error while creating biz processor. Due to: " + e.getMessage(), e);
		}
	}
	
	private void resetMessageStatus() throws Exception{
		DaoBeanFactory.getSmsQueueDao().initialiseAllMessageStatus(Constants.SMS_STATUS_M, Constants.SMS_STATUS_4);
		if(AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.BP_RESET_MQ_MSGS) 
				&& !AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.MQ_PERSISTENCE_MODE)){
			if(DaoBeanFactory.getSmsQueueDao().findMessageSize(Constants.SMS_SOURCE_TYPE_5) > 0){				
				this.jmxConnector = JMXConnectorFactory.newJMXConnector(new JMXServiceURL("rmi", "", 0, "/jndi/rmi://" + AppPropertiesConfig.MQ_HOST + ":" + Integer.parseInt(AppPropertiesConfig.MQ_JMX_PORT) + "/jmxrmi"), null);
				this.jmxConnector.connect();
				List<SmsMQ> queues = DaoBeanFactory.getSmsMQDao().findAllNonClosedMQ();
				List<TblRef> smsTypes = DaoBeanFactory.getTblRefDao().getSmsTypes();
				Map<String, TblRef> tblRefDb = new HashMap<String, TblRef>();
				for(TblRef r : smsTypes){
					tblRefDb.put(r.getLabel(), r);
				}				
				boolean reset = true;
				for(SmsMQ queue : queues){
					String queueName = BusinessService.getQueueName(queue.getTelcoName(), tblRefDb.get(queue.getValue()));
					Integer smsOnhand = ((Long)jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("org.apache.activemq:BrokerName=localhost,Type=Queue,Destination=" + queueName), "QueueSize")).intValue();
					if(smsOnhand > 0){
						log.warn("There are message(s) q-ing in MQ. No reset of status " + Constants.SMS_STATUS_5);
						reset = false;
						break;
					}
				}
				if(reset){
					log.info("Message status " + Constants.SMS_STATUS_5 + " will be reset to " + Constants.SMS_STATUS_4);
					DaoBeanFactory.getSmsQueueDao().initialiseAllMessageStatus(Constants.SMS_STATUS_5, Constants.SMS_STATUS_4);					
				}
			}
		}
	}
	
	@Override
	public void run() {
			log.info("Starting up " + this.getClass().getSimpleName());			
				while (isRunning){	
					try{
							log.info("Running ...");
							checkDisabledSvcProvider(null);
							List<SmsQueue> loadedSms = loadSmsIntoMemory();
							log.debug("Loaded into memory");
							List<List<SmsQueue>> partitionLists = Lists.partition(loadedSms, msgPerWorkerThread);					
							for(List<SmsQueue> partition : partitionLists){
								log.debug("Distribute to agent size: " + partition.size());
								workersMgr.submit(new BusinessProcessorAgent(partition, smsConfig));
							}	
							yield();
							log.debug("Going into sleep mode ... ");
							sleep(runInterval * 1000);
							smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);		
							log.debug("Woke up from sleep mode ... ");					
						}catch(RejectedExecutionException ree){
							if(!isRunning){
								log.warn("Task won't be submitted to worker as system is shutting down. Message: " + ree.getMessage() + ". No worries, this's not an error :-)");
							}else{
								log.error(ree.getMessage(), ree);
							}
						}catch(InterruptedException e){
							if(!isRunning){
								log.warn("Got interrupted. It's a shutdown call.");
							}else{
								log.error("Unexpected InterruptedException caught and no shutdown call received. Check stacktrace.", e);
							}
						}catch(Exception e){
							log.error("Unexpected error caught. Due to: " + e.getMessage(), e);
						}
			}			
	}
	
	private void checkDisabledSvcProvider(List<SmsQueue> sms){
//		if(System.currentTimeMillis() - this.lastStatusCheckTime.getTime() < smsConfig.getDisabledSvcPvdCheckPeriod() * 1000){
//			return;
//		}
//		this.lastStatusCheckTime = new Date();
		List<Telco> disabledTelcos = DaoBeanFactory.getTelcoDao().findTelcos(Constants.STATUS_DISABLED);
		List<SmsQueue> testSms = new ArrayList<SmsQueue>();
		if(disabledTelcos != null && !disabledTelcos.isEmpty()){
//			TblRef expressType = DaoBeanFactory.getTblRefDao().getExpressQSmsTypeRef(); //Try get highest priority sms type from db
			for(int i=0; i<disabledTelcos.size(); i++){
				Telco telco = disabledTelcos.get(i);
				
//				if(sms.isEmpty()) return; // can't test if sms is empty
				if(smsConfig.getDisabledSvcPvdCheckPeriod() == null 
					|| smsConfig.getDisabledSvcPvdCheckPeriod() <= 0
					|| telco.getLastStatusChecked() == null
					|| System.currentTimeMillis() - telco.getLastStatusChecked().getTime() < smsConfig.getDisabledSvcPvdCheckPeriod() * 1000){
					log.debug("Disabled telco checking is either disabled or time for checking not reached yet.");
					continue; // disabled time not long enough for checking
				}
				
				log.warn("System has disabled service provider -> " + telco.getTelcoName() + ". Try to check the latest status of it.");
				
				
				SmsQueue smsQueue = DaoBeanFactory.getSmsQueueDao().getSmsForTelco(telco.getId(), smsConfig);
				if(smsQueue == null) {
					log.warn("No available sms found for checking status of this telco -> " + telco.getTelcoName());
					continue;
				}
					
				telco.setLastStatusChecked(new Date());
				DaoBeanFactory.getTelcoDao().update(telco);//update last check time
				
//				smsQueue = sms.get(0);
				smsQueue.setTelco(telco.getId());
				smsQueue.setTelcoName(telco.getTelcoName());
//				Boolean encrypted = smsQueue.getSmsType().getEncrypted();
//				smsQueue.setSmsType(expressType);
//				smsQueue.getSmsType().setEncrypted(encrypted);
				smsQueue.setRemarks(Constants.SMS_REMARK_TELCO_CHECK);
				smsQueue.setSmsStatusDatetime(new Date());
				smsQueue.setSmsStatusBy("BPTelcoCheck");
				smsQueue.setSmsStatus(Constants.SMS_STATUS_5);
				DaoBeanFactory.getSmsQueueDao().update(smsQueue);
				boolean sent = JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(smsQueue.getTelcoName(), smsQueue.getSmsType()), smsQueue);
				if(!sent){
					log.error("Sending to MQ failed.");
					smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
					DaoBeanFactory.getSmsQueueDao().update(smsQueue);										
				}else{
//					sms.remove(0);
				}
			}
			
		}
//		workersMgr.submit(new BusinessProcessorAgent(testSms, smsConfig));
	}
	
	//called when hits timeout waiting for free worker thread
	public Boolean call() throws Exception {
		log.warn("Waiting for free worker exceeds timeout " + workerTimeWait + " seconds. This might due to a short timeout/slow processing in worker thread. Please check bp configuration.");
		return true;
	}
	
	public List<SmsQueue> loadSmsIntoMemory(){
		log.debug("Load sms queue into memory ...");		
		List<SmsQueue> smsListInMemory = new ArrayList<SmsQueue>();
		
		boolean isByPriority = CommonUtils.NullChecker(smsConfig.getSmsPrioritize(), String.class).toString().equals(Constants.STATUS_YES)? true : false;
//		smsListInMemory = DaoBeanFactory.getSmsQueueDao().findMessages(isByPriority, noOfSmsInMemory);
		
		// "M" status as a flag so that next retrieve will not reselect selected records in case business process for the current execution has not done yet
		int fetchedSize = noOfSmsInMemory;
		int updatedSize = 0;
//		smsListInMemory = DaoBeanFactory.getSmsQueueDao().updateStatus(smsListInMemory, Constants.SMS_STATUS_M, isByPriority);
		
		
		//part of fetched sms updated by other bp
		while(updatedSize < fetchedSize){
			List<SmsQueue> smsListInMemory2 = DaoBeanFactory.getSmsQueueDao().findMessages(isByPriority, fetchedSize-updatedSize);
			fetchedSize = smsListInMemory2.size();
			if(fetchedSize == 0){
				break;
			}
			smsListInMemory2 = DaoBeanFactory.getSmsQueueDao().updateStatus(smsListInMemory2, Constants.SMS_STATUS_M, isByPriority);
			updatedSize = smsListInMemory2.size();
			smsListInMemory.addAll(smsListInMemory2);
		}
		return smsListInMemory;
	}
	
	public void shutdown(){		
		isRunning = false;	
		log.info("Shutting down " + this.getClass().getSimpleName());
		interrupt();
		workersMgr.shutdown();
		try{
			int timeout = workerThreadAwaitTerminationTimeout;
			log.warn("Awaiting worker thread processing. Timeout -> " + timeout + " seconds");
			workersMgr.awaitTermination(timeout, TimeUnit.SECONDS);
			log.warn("All Worker threads shutdown completely");
		}catch(InterruptedException ie){
			log.error("Interrupted while waiting for worker threads to completely shutdown", ie);
		}		
		JmsProducerManager.getInstance().shutdown();
		updateConfig();
		log.info("Alrite, shutdown successful ;-)");
	}

	private void updateConfig(){
		log.info("Saving latest configurations.");
		smsConfig.setBpMaxProcessPerThread(this.msgPerWorkerThread);
		smsConfig.setBpMaxRetrieve(this.noOfSmsInMemory);
		smsConfig.setBpMaxThread(this.noOfWorkerThread);
		smsConfig.setBpRunInterval(this.runInterval);
		smsConfig.setBpWorkerKeepAliveTime(this.workerKeepAliveTime);
		smsConfig.setBpWorkerTimeWait(this.workerTimeWait);
		DaoBeanFactory.getSmsCfgDao().update(smsConfig);		
	}
	
	@ManagedAttribute
	public int getNoOfWorkerThread() {
		return noOfWorkerThread;
	}

	@ManagedAttribute
	public void setNoOfWorkerThread(int noOfWorkerThread) {
		if(((ThreadPoolExecutor)workersMgr).getCorePoolSize() != noOfWorkerThread){
			((ThreadPoolExecutor)workersMgr).setCorePoolSize(noOfWorkerThread);
			((ThreadPoolExecutor)workersMgr).setMaximumPoolSize(noOfWorkerThread);
		}		
		this.noOfWorkerThread = noOfWorkerThread;
	}
	
	@ManagedOperation
	public void loadConfigFromDB(){
		smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
		if(smsConfig == null){
			String message = "System error: No sms config define for app version " + Constants.APPSVERSION + ". Processor will halt."; 
			this.isRunning = false;
			throw new RuntimeException(message);
		}	
		noOfWorkerThread = smsConfig.getBpMaxThread();
		runInterval = smsConfig.getBpRunInterval();
		workerKeepAliveTime = smsConfig.getBpWorkerKeepAliveTime();
		msgPerWorkerThread = smsConfig.getBpMaxProcessPerThread();
		workerTimeWait = smsConfig.getBpWorkerTimeWait();
		noOfSmsInMemory = smsConfig.getBpMaxRetrieve();		
	}

	@ManagedAttribute
	public int getRunInterval() {
		return runInterval;
	}

	@ManagedAttribute
	public void setRunInterval(int runInterval) {
		this.runInterval = runInterval;
	}

	@ManagedAttribute
	public int getWorkerKeepAliveTime() {
		return workerKeepAliveTime;
	}

	@ManagedAttribute
	public void setWorkerKeepAliveTime(int workerKeepAliveTime) {
		((ThreadPoolExecutor)workersMgr).setKeepAliveTime(workerKeepAliveTime, TimeUnit.SECONDS);
		this.workerKeepAliveTime = workerKeepAliveTime;
	}

	@ManagedAttribute
	public int getWorkerTimeWait() {
		return workerTimeWait;
	}

	@ManagedAttribute
	public void setWorkerTimeWait(int workerTimeWait) {
		((BlockingThreadPoolExecutor.BlockThenRunPolicy)((ThreadPoolExecutor)workersMgr).getRejectedExecutionHandler()).setBlockTimeout(workerTimeWait);
		this.workerTimeWait = workerTimeWait;
	}

	@ManagedAttribute
	public int getNoOfSmsInMemory() {
		return noOfSmsInMemory;
	}

	@ManagedAttribute
	public void setNoOfSmsInMemory(int noOfSmsInMemory) {
		this.noOfSmsInMemory = noOfSmsInMemory;
	}

	@ManagedAttribute
	public int getMsgPerWorkerThread() {
		return msgPerWorkerThread;
	}

	@ManagedAttribute
	public void setMsgPerWorkerThread(int msgPerWorkerThread) {
		this.msgPerWorkerThread = msgPerWorkerThread;
	}
		
}
