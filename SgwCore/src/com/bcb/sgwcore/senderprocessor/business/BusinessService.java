package com.bcb.sgwcore.senderprocessor.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsQuota;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.modal.UserChannelID;
import com.bcb.crsmsg.util.AppDBConfig;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class BusinessService extends DaoBeanFactory{
	
	private static final Log log = LogManager.getLog();
		
	/**
	 * Call this to send SMS immediately without waiting for BP to poll it from db.
	 * This method is for high priority SMS, eg. TAC request. 
	 * This method do some processing, save to DB and put into message queue.
	 * @param smsQueue - SmsQueue object that has not yet been saved to DB.
	 */
	public static Integer processSMS(SmsQueue smsQueue) throws Exception{
		long startTime = System.currentTimeMillis();
		try{
			SmsCfg smsCfg = AppDBConfig.getInstance().getSmsCfg();
			smsQueue = ServiceProviderRouter.route(smsQueue, smsCfg);
			if(smsQueue.getTelco() == 0) {
				log.warn("SMS will route to queue. Reason: No available service provider found.");
				smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
				DaoBeanFactory.getSmsQueueDao().insert(smsQueue);
				return smsQueue.getId();
			}
//			if(BusinessService.updateQuota(smsQueue, smsCfg, "BPA-", false)){ //- decrement
//				log.debug("Quota feature enabled. Minus from quota " + smsQueue.getTotalSms());
//				log.warn("Quota full for dept: " + smsQueue.getDept() + ". Will discard from sending."); 
//				smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
//				smsQueue.setSmsStatusDatetime(new Date());
//				smsQueue.setSmsStatusBy("BPA-QUOTA");
//				smsQueue.setRemarks("Quota full");
//				smsQueue.setPriority(99);//set to the lowest priority so that they won't be stuck on top of the queue
//				DaoBeanFactory.getSmsQueueDao().update(smsQueue);					
//			}	
			BusinessService.sendToMQ(smsQueue, smsCfg);
			log.debug("Time taken: " + (System.currentTimeMillis()-startTime) + "ms");
			return smsQueue.getId();
		}catch(Exception e){
			log.error("Error processing online sms. Reason: " + e.getMessage());			
			throw e;
		}
	}
	
	public static boolean sendToMQ(SmsQueue smsQueue, SmsCfg smsConfig){
		smsQueue.setSmsStatus(Constants.SMS_STATUS_5);
		smsQueue.setSmsStatusDatetime(new Date());
		smsQueue.setSmsStatusBy("BPA");
		DaoBeanFactory.getHibernateTemplate().saveOrUpdate(smsQueue);
		boolean sent = JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(smsQueue.getTelcoName(), smsQueue.getSmsType()), smsQueue);
		if(!sent){
			log.error("Sending to MQ failed. SMS routed to queue. Id: " + smsQueue.getId());
			smsQueue.setSmsStatus(Constants.SMS_STATUS_4);//update status from 'M' back to 'Q'
			smsQueue.setSmsStatusDatetime(new Date());
			smsQueue.setSmsStatusBy("BPA-SEND2MQ");
			smsQueue.setRemarks("Send to mq failed");
			DaoBeanFactory.getSmsQueueDao().update(smsQueue);					
			BusinessService.updateQuota(smsQueue, smsConfig, "BPA+", true);//increment quota as it's not used when sending failed.
			return false;
		}
		return true;
	}
	
	public static List<SmsQueue> discardTimeCtrlSms(List<SmsQueue> unfilteredSms){
		log.debug("Filter out time controlled sms");
		List<SmsQueue> filteredSms = new ArrayList<SmsQueue>();
		try{
			Map<Integer, Boolean> timeCtrlDeptIds = new HashMap<Integer, Boolean>();
			
			Map<Integer, Boolean> batchDeptOnHoldList = getTimeControlDao().findTimeCtrlBatchBlock();
			Map<Integer, Boolean> webDeptOnHoldList = getTimeControlDao().findTimeCtrlWebBlock();
			
			for(SmsQueue sms : unfilteredSms){
				if(sms.getType().equals(Constants.SMS_SOURCE_TYPE_1)||sms.getType().equals(Constants.SMS_SOURCE_TYPE_2)){
					timeCtrlDeptIds = webDeptOnHoldList;
				}else if(sms.getType().equals(Constants.SMS_SOURCE_TYPE_3)||sms.getType().equals(Constants.SMS_SOURCE_TYPE_4)){
					timeCtrlDeptIds = batchDeptOnHoldList;
				}else{
					// no time control for this sms type, sms will not be discarded
				}
				if(timeCtrlDeptIds.containsKey(sms.getDeptId())){
					// discard the sms and update db
					sms.setRemarks("SMS Time Controlled - Hold");
					sms.setModifiedBy("SmsTimeControl");
					sms.setModifiedDatetime(new Date());
					if(!timeCtrlDeptIds.get(sms.getDeptId())){  // set to unsent if retry is false
						sms.setSmsStatus(Constants.SMS_STATUS_2);
						if(DaoBeanFactory.getSmsDao().createObjectBySmsQueue(sms)){
							DaoBeanFactory.getSmsQueueDao().deleteByObject(sms);
						}else{
							sms.setSmsStatus(Constants.SMS_STATUS_4);
							sms.setPriority(99);//set to the lowest priority so that they won't be stuck on top of the queue
							getSmsQueueDao().update(sms);
						}
					}						
					else{
						sms.setSmsStatus(Constants.SMS_STATUS_4);
						sms.setPriority(99);//set to the lowest priority so that they won't be stuck on top of the queue
						getSmsQueueDao().update(sms);
					}
				}else{
					filteredSms.add(sms);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			log.error("Error during time control checking. Due to : " + ex.getMessage(), ex);
		}
		unfilteredSms = null;
		return filteredSms;
	}
		
	/**
	 * 
	 * @param sms
	 * @param smsConfig
	 * @param increment - true if incrementing quota value and vice versa
	 * @return true if quota is full, is only usable when increment is false. Increment true will always return false result.
	 */
	public static synchronized boolean updateQuota(SmsQueue sms, SmsCfg smsConfig, String modifiedBy, boolean increment){
		//quota checking feature disabled
		log.debug("Checking quota ...");
		if(!CommonUtils.NullChecker(smsConfig.getSmsQuota(), String.class).toString().equals(Constants.STATUS_YES)){
			return false; 
		}
		List<SmsQuota> quotas = null;
		if(increment){
			quotas = getSmsQuataDao().findSmsQuota(sms.getDeptId());
		}else{
			quotas = getSmsQuataDao().findSmsQuota(sms.getDeptId(), sms.getTotalSms());
		}
		
		if(quotas != null && !quotas.isEmpty()) {
			SmsQuota q = quotas.get(0);
			q.setQuota(increment ? q.getQuota() + sms.getTotalSms() : q.getQuota()-sms.getTotalSms());
			q.setModifiedBy(modifiedBy);
			q.setModifiedDatetime(new Date());
			getSmsQuataDao().updateSmsQuota(q);
			return false;
		}else{
			return true;
		}		
	}
		
	public static String getQueueName(String telcoName, TblRef tblRef){
		return getQueueName(telcoName, tblRef.getLabel());
	}

	public static String getQueueName(String telcoName, String smsType){
		return telcoName + " (" + smsType + ")";
	}
	
	public static boolean hasSMSExpired(SmsQueue smsQueue){
		if(StringUtils.isEmpty(smsQueue.getChannelId()) || smsQueue.getClientTimestamp() == null){
			return false;//valid all time
		}
		UserChannel channel = (UserChannel) DaoBeanFactory.getHibernateTemplate().get(UserChannel.class, new UserChannelID(smsQueue.getChannelId(), smsQueue.getCreatedBy()));
		if(channel != null && channel.getTimeValidity() > 0){
			return (new Date().getTime() - smsQueue.getClientTimestamp().getTime() <= channel.getTimeValidity()*1000 )? false : true;
		}
		return false;
	}

	public static Integer selectSmsRate(Integer svcProviderId, String mobileNumber){
		ServiceProvider provider = (ServiceProvider) DaoBeanFactory.getHibernateTemplate().get(ServiceProvider.class, svcProviderId);
		String[] rateIdsStr = StringUtils.splitByWholeSeparator(provider.getSmsRateIds(), Constants.SPECIAL_CHAR_PIPE);
		Integer[] rateIds =  new Integer[rateIdsStr.length];		
		for(int i=0; i<rateIdsStr.length; i++){
			rateIds[i] = Integer.parseInt(rateIdsStr[i]);
		}
		List<SmsRate> rates = DaoBeanFactory.getSmsRateDao().getRates(rateIds);
		if(rates == null || rates.isEmpty()) return null;
		
		int maxLength = AppPropertiesConfig.getInstance().getSettings().getInt(AppPropertiesConfig.SMS_COUNTRY_CODE_MAX_LENGTH);
		String mobileCountryCode = StringUtils.substring(mobileNumber, 0, 5);
		for(int i=maxLength ; i>0; i--){
			String searchStr = StringUtils.substring(mobileCountryCode, 0, i);
			for(SmsRate rate : rates){
				if(StringUtils.isEmpty(rate.getCountryCodes())){
					return rate.getId();
				}
				List<String> countryCodes = Arrays.asList(StringUtils.splitByWholeSeparator(rate.getCountryCodes(), com.bcb.common.util.Constants.LINE_SEPARATOR));
				if(countryCodes.contains(searchStr)){
					return rate.getId();
				}
			}
		}
		log.warn("No match for mobile no ->'" + mobileNumber + "' .Using default Rate");
		return provider.getDefaultSmsRateId();
	}

	
	public static String normalizeMobile(String mobileNo) {
		mobileNo = mobileNo.trim().replaceAll(" ", "").replaceAll("-", "").replaceAll("[+]", "").replaceAll("[(]", "").replaceAll("[)]", "");
		if (StringUtils.isNotEmpty(mobileNo) && mobileNo.startsWith("0"))//number start with 0 will be padded a 6 to make it m'sia number
		{
			mobileNo="6"+mobileNo;
		}
		return mobileNo;
	}
	
	public static boolean validateMobileNo(String mobileNo){
		if (mobileNo ==null||mobileNo.trim().length()==0 ){
			return false;
		}else{
			mobileNo = BusinessService.normalizeMobile(mobileNo);
			if(StringUtils.isBlank(mobileNo) || !StringUtils.isNumeric(mobileNo)) {
				return false;
			}
		}
		
		return true;
	}
	
}

