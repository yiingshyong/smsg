package com.bcb.sgwcore.senderprocessor.business;

public class CacheManager {

	private static CacheManager instance;
		
	private CacheManager() {
		super();
	}

	public static CacheManager getInstance(){
		if(instance == null){
			instance = new CacheManager();
		}
		return instance;
	}
}
