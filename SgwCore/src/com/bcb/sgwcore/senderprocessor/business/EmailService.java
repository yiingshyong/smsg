package com.bcb.sgwcore.senderprocessor.business;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import org.apache.commons.logging.Log;

import com.bcb.sgwcore.util.SendMailBean;

public class EmailService implements Callable<Boolean>{
	
	private static EmailService emailSvc;
	private ExecutorService emailQueue;
	private static final Log log = LogManager.getLog();
	
	private EmailService(){
		emailQueue = new BlockingThreadPoolExecutor(2, 100, 0, 10*60*1000, this);
	}
	
	public static EmailService getInstance(){
		if(emailSvc == null){
			emailSvc = new EmailService();
		}
		return emailSvc;
	}
	
	public void sendMail(final SendMailBean mailer){
		emailQueue.execute(new Runnable() {
			public void run() {
				log.info("Start sending email ...");
				try{
					mailer.send();
					log.info("Done.");
				}catch(Exception e){
					log.error("Error while sending email. Due to: " + e.getMessage(), e);
				}
			}
		});
	}

	public Boolean call() throws Exception {
		return true;
	}

}
