package com.bcb.sgwcore.senderprocessor.business;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.bcb.crsmsg.modal.SmsQueue;


public class JmsExample {

	public static void main(String[] args) {
		try{
			consumeMessage();
//			for(int i=0; i<10; i++){
//				createMessage();
//			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void createMessage() throws Exception{
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://BC786468PC:61616");

        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination destination = session.createQueue("TEST.FOO");

        MessageProducer producer = session.createProducer(destination);
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);

        SmsQueue sms = new SmsQueue();
        sms.setId(123);
        ObjectMessage message = session.createObjectMessage(sms);

        System.out.println("Sent message: "+ message.hashCode() + " : " + Thread.currentThread().getName());
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
		
	}
	
	public static void consumeMessage() throws Exception{
		boolean run = true;
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://BC786468PC:61616");		
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        Destination destination = session.createQueue("TEST.FOO");
        
        MessageConsumer consumer = session.createConsumer(destination);  
        consumer.setMessageListener(new MessageListener() {
			public void onMessage(Message arg0) {
				//send 
			}
		});
        
        Message message = null;  
        while(run){
	    	if((message = consumer.receive(1000)) != null){
	            message = (TextMessage) message;
	            System.out.println("Received: " + message);
	    	}
		}
        
        consumer.close();
        session.close();
        connection.close();
	}
	
	public void browser() throws Exception{
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://BC786468PC:61616");		
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
//		ActiveMQQueueBrowser browser = new ActiveMQQueueBrowser();
	}
}
