package com.bcb.sgwcore.senderprocessor.business;

import java.util.LinkedList;
import java.util.List;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsQueue;

public class JmsProducer extends Thread{
	
	private static final Log log = LogManager.getLog();
	private List<SmsQueue> smsQueues;
	private Connection conn;
	private String queueName;
	private Session session;
	private MessageProducer producer;
	private boolean isRunning = true;
	private int count = 0;
	private LinkedList<JmsProducer> producerPool;
	private int count2 = 0;

	public JmsProducer(Connection conn,  LinkedList<JmsProducer> producerPool){
		this.conn = conn;
		this.producerPool = producerPool;
	}
	
	public JmsProducer(Connection conn, String queueName) {		
		this.conn = conn;
		this.queueName = queueName;
		try{
			session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        Destination destination = session.createQueue(queueName);
	        producer = session.createProducer(destination);
	        producer.setDeliveryMode(DeliveryMode.PERSISTENT);	        
		}catch(Exception e){
			log.error("Error creating jms producer thread. Due to: " + e.getMessage(), e);
		} 		
	}

	@Override
	public void run() {
		while(isRunning){
			log.info(getName() + " processing msg#:" + ++count2);
			synchronized (producerPool) {
				producerPool.remove(this);
			}
			if(isInterrupted()) isRunning = false; // stop in next loop
			if(smsQueues != null && !smsQueues.isEmpty()){
				for(SmsQueue smsQueue : smsQueues){			
					try{
						if(session == null){
							session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);													
						}
				        Destination destination = session.createQueue(queueName);
				        producer = session.createProducer(destination);
				        producer.setDeliveryMode(DeliveryMode.PERSISTENT);	        
						
						ObjectMessage msg = session.createObjectMessage(smsQueue);
						log.info("Sending msg to mq " + queueName);
						log.info(getId() + ": " + ++count);
						producer.send(msg);
						log.info("Sent.");
					}catch(Exception e){
						log.error("Error while sending msg to queue: " + queueName + " .Sms id: " + smsQueue.getId() + ".Due to: " + e.getMessage(), e);
						throw new RuntimeException();
						//update status to Q ?
					}
				}	
			}
			try{
				if(isRunning) 
					synchronized (producerPool) {
						producerPool.add(this);
						producerPool.notify();
//						sleep(1000);
					}
					synchronized (this) {
						log.info("Going to standby mode ...");
						wait();							
						log.info("Woke up from hibernate ...");
					}
			}catch(InterruptedException exception){
				log.warn("interrupted! What to do?");
			}			
		}
		try {
			log.info("Shutting down " + getName());
			session.close();
		} catch (JMSException e) {
			log.error("Error while closing session. Due to: " + e.getMessage(), e);
		}			
	}
		
	public void shutDown(){
		try{
			isRunning = false;
			log.info("Shut down called ...");
		}catch(Exception e){
			log.error("Error while closing session. Due to: " + e.getMessage(), e);
		}
	}

	public List<SmsQueue> getSmsQueues() {
		return smsQueues;
	}

	public void setSmsQueues(List<SmsQueue> smsQueues) {
		this.smsQueues = smsQueues;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

}
