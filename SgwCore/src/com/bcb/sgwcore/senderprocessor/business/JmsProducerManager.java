package com.bcb.sgwcore.senderprocessor.business;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.AppPropertiesConfig;
public class JmsProducerManager {

	private static final Log log = LogManager.getLog();
	private static JmsProducerManager instance;
	private PooledConnectionFactory connPool;
		
	private JmsProducerManager(){
		setup();
	}
	
	public void setup(){
		if(instance == null){
			log.info("Setting connection pooling for jms producer");
			ActiveMQConnectionFactory connFactory = new ActiveMQConnectionFactory("tcp://" + AppPropertiesConfig.MQ_HOST + ":" + AppPropertiesConfig.MQ_PORT);//TODO: configurable parameter into sms_cfg table
			connFactory.setUseAsyncSend(AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.MQ_ASYNC_SEND));
			connPool = new PooledConnectionFactory(connFactory);
		}
	}
	
	public boolean sendMessage(String queueName, SmsQueue... smsQueues){
		Connection conn = null;
		Session session = null;
		try{
			conn = connPool.createConnection();
			session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
		    Destination destination = session.createQueue(queueName);		    
		    MessageProducer producer = session.createProducer(destination);
		    producer.setDeliveryMode(AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.MQ_PERSISTENCE_MODE)?DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT);	        
			
		    if(smsQueues != null && smsQueues.length > 0){
				try {
				    for(SmsQueue smsQueue : smsQueues){
						log.debug("Request to send message: queue: " + queueName + " SmsId: " + smsQueue.getId() + " message: " + smsQueue.getMessage());
						ObjectMessage msg = session.createObjectMessage(smsQueue);
//						msg.setJMSMessageID(String.valueOf(smsQueue.getId()));
//						msg.setJMSType("abc");
						producer.send(msg);
						session.commit();
				    }	
				} catch (Exception e) {
					log.error("Error while sending to mq. Due to: " + e.getMessage(), e);
					session.rollback();
					return false;
				}							
		    }
		    return true;
		}catch(Exception e){
			log.error("Error in sending to mq. Due to: " + e.getMessage(), e);
			return false;
		}finally{
			try {
				if(session != null){
					session.close();
				}
			} catch (Exception e) {
				log.error("Error while closing jms session. Due to: " + e.getMessage(), e);
			}
			try {
				if(conn != null){
					conn.close();					
				}
			} catch (Exception e) {
				log.error("Error while closing jms connection. Due to: " + e.getMessage(), e);
			}
		}
	}
		
	public static JmsProducerManager getInstance(){
		if(instance == null){
			instance = new JmsProducerManager();
		}
		return instance;
	}
	
	public void shutdown(){
		log.info("Stopping jms producer connection pooling...");
		connPool.stop();		
	}

	public PooledConnectionFactory getConnPool() {
		return connPool;
	}

	public void setConnPool(PooledConnectionFactory connPool) {
		this.connPool = connPool;
	}

}
