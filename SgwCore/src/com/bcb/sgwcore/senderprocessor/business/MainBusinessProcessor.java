package com.bcb.sgwcore.senderprocessor.business;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

public class MainBusinessProcessor implements WrapperListener{
   
	private BusinessProcessorManager bpMgr;
	private static ApplicationContext applicationContext;

	public static void main(String args[])
	{
		WrapperManager.start(new MainBusinessProcessor(), args);
	}

	public void controlEvent(int event)
	{
		
		 if (WrapperManager.isControlledByNativeWrapper()) { 
		 } else { 
			 if ((event ==
		  WrapperManager.WRAPPER_CTRL_C_EVENT) || (event ==
		  WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event ==
		 WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){ WrapperManager.stop(0); } }
		
	}

	public Integer start(String[] param){
		JmsProducerManager.getInstance();
		applicationContext = new ClassPathXmlApplicationContext("bp-jmx.xml");
		bpMgr = (BusinessProcessorManager) applicationContext.getBean("BusinessManager");
		bpMgr.start();
		return null;
	}

	public int stop(int exitcode){
		bpMgr.shutdown();
		return 0;
	}
	
	public static ApplicationContext getBizProContext(){
		return applicationContext;
	}

}
