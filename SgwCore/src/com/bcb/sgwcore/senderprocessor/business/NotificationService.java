package com.bcb.sgwcore.senderprocessor.business;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.bcb.sgwcore.util.SendMailBean;

public class NotificationService extends DaoBeanFactory{
	private static final Log log = LogManager.getLog();
	
	public static void sendQueueThresholdEmailNotification(String telcoName, int currQueueSize, SmsCfg smsConfig){
		NotificationTemplate template = NotificationTemplate.getSvcProviderQThresholdTemplate(
				telcoName, Integer.parseInt(smsConfig.getQueueSmsThreshold()), currQueueSize);
		String[] recipients = CommonUtils.NullChecker(smsConfig.getQueueSmsThresholdNotificationEmail(), String.class).toString()
		.split(CommonUtils.escapeDelimiter(smsConfig.getTableListDelimiter()));
		String sender = smsConfig.getQueueSmsThresholdNotificationEmailSender();
		for(String recipient : recipients){
			SendMailBean mailer = new SendMailBean();
			mailer.setP_to(recipient);
			mailer.setP_from(CommonUtils.NullChecker(sender,String.class).toString().length() > 0 ? sender : Constants.APPSNAME);
			mailer.setP_subject(template.getSubject());
			mailer.setP_message(template.getBody());
			EmailService.getInstance().sendMail(mailer);
		}
	}
	
	public static void sendQueueThresholdSmsNotification(String telcoName, int currQueueSize, SmsCfg smsConfig){
		NotificationTemplate template = NotificationTemplate.getSvcProviderQThresholdTemplate(
				telcoName, Integer.parseInt(smsConfig.getQueueSmsThreshold()), currQueueSize);
		String[] recipients = CommonUtils.NullChecker(smsConfig.getQueueSmsThresholdNotificationSms(), String.class).toString()
								.split(CommonUtils.escapeDelimiter(smsConfig.getTableListDelimiter()));
		String sender = smsConfig.getQueueSmsThresholdNotificationSmsSender();
		for(String recipient : recipients){
			createSmsQueue(template.getBody(), sender, "ThresholdMtr", recipient, smsConfig);
		}
	}
	
	public static void sendTotalQEmailAlert(Integer currQueuedSize, SmsCfg config){
		NotificationTemplate template = NotificationTemplate.getTotalQueueNotificationTemplate(config.getTotalQueuedSizeAlertThreshold(), currQueuedSize);
		String from = config.getQueueSmsThresholdNotificationEmailSender();
		
		SendMailBean mailer = new SendMailBean();
		mailer.setP_from(CommonUtils.NullChecker(from, String.class).toString().length() > 0 ? from : Constants.APPSNAME);
		mailer.setP_subject(template.getSubject());
		mailer.setP_message(template.getBody());
		
		if(config.getQueueSmsThresholdNotification().contains("E")){
			String[] recipients=CommonUtils.NullChecker(config.getQueueSmsThresholdNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(config.getTableListDelimiter()));
			for(String recipient : recipients){
				mailer.setP_to(recipient);
				EmailService.getInstance().sendMail(mailer);
			}

		}
	}
	
	public static void sendTotalQSmsAlert(Integer currQueuedSize, SmsCfg config){
		NotificationTemplate template = NotificationTemplate.getTotalQueueNotificationTemplate(config.getTotalQueuedSizeAlertThreshold(), currQueuedSize);
		String[] recipients=CommonUtils.NullChecker(config.getQueueSmsThresholdNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(config.getTableListDelimiter()));
		String sender = config.getQueueSmsThresholdNotificationSmsSender();
		for(String recipient : recipients){
			createSmsQueue(template.getBody(), sender, "TotalQMtr", recipient, config);
		}
	}
	
	public static void sendNotificationSms(String message, String senderUserId, String statusBy, String mobileNo, SmsCfg smsConfig){
		createSmsQueue(message, senderUserId, statusBy, mobileNo, smsConfig);
	}
	
	private static SmsQueue createSmsQueue(String message, String senderUserId, String statusBy, String mobileNo, SmsCfg smsConfig){
		
		if(smsConfig == null){
			smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
		}
		int deptId=0;
		String dept=Constants.STR_EMPTY;		
		User usermodal = getUserDao().findByUserId(senderUserId);
		if (usermodal == null){
		}else{
			Department groupmodal = getGroupDao().findByGroupId(usermodal.getUserGroup());
			if (groupmodal != null){
				deptId = groupmodal.getId();
				dept = groupmodal.getDeptName();
			}
		}
		
			SmsQueue aSms = new SmsQueue();
			aSms.setCreatedBy(senderUserId);
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, smsConfig.getCharPerSms(), smsConfig.getConcatenateSms(), smsConfig.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			aSms.setTotalSms(msgArray.size());			
			aSms.setMobileNo(BusinessService.normalizeMobile(mobileNo));
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_5);
			aSms.setSmsStatusBy(StringUtils.substring(statusBy, 0, 15)); //db length = 15
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(senderUserId);
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			try{
				aSms.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(aSms.getSentBy(), null));
				aSms = ServiceProviderRouter.route(aSms, smsConfig);
				if(aSms.getTelco() == 0 ){
					log.error("None of the telco is active. Sms will not directly be sent to MQ. Store it in database for the moment.");
					aSms.setSmsStatus(Constants.SMS_STATUS_4);
					DaoBeanFactory.getSmsQueueDao().insert(aSms);
				}else{
					DaoBeanFactory.getSmsQueueDao().insert(aSms);
//					aSms.setSmsType(getTblRefDao().getExpressQSmsTypeRef());//get from db, order by type for highest priority
					if(JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(aSms.getTelcoName(), aSms.getSmsType()), aSms)){
					}else{
						log.error("Sms notification failed to be sent to mq. Sms id: " + aSms.getId());
						aSms.setSmsStatus(Constants.SMS_STATUS_4);
						DaoBeanFactory.getSmsQueueDao().update(aSms);
					}					
				}
				
			}catch(Exception e){
				log.error("Error while creating sms queue. Due to: " + e.getMessage(), e);
			}
			return aSms;
	}
	
}
