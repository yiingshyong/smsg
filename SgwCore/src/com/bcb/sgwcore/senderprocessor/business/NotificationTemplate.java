package com.bcb.sgwcore.senderprocessor.business;

import java.util.Date;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.util.Constants;
import com.ibm.icu.text.SimpleDateFormat;

public class NotificationTemplate {
	
	private static final SimpleDateFormat DT_FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final Log log = LogManager.getLog(); 
	private String subject;
	private String body;
	
	public static NotificationTemplate getSvcProviderQThresholdTemplate(String telcoName, int configLimit, int currSize){
		NotificationTemplate template = new NotificationTemplate();
		template.subject = "Telco " +  telcoName + " - Hit SMS Queue Limit";
		template.body = "Telco: " + telcoName + Constants.SPECIAL_CHAR_NEWLINE +
		"Status: Hit SMS Queue Limit " + configLimit + " with " + currSize + Constants.SPECIAL_CHAR_NEWLINE +
		"Status Time: " + DT_FORMATTER.format(new Date()) + Constants.SPECIAL_CHAR_NEWLINE;			
		return template;
	}

	public static NotificationTemplate getTotalQueueNotificationTemplate(int configLimit, int currSize){
		NotificationTemplate template = new NotificationTemplate();
		template.subject = "Total SMS Queued Exceeded Alert";
		StringBuffer message = new StringBuffer(template.subject).append(Constants.SPECIAL_CHAR_NEWLINE);
		message.append("Threshold Size: " + String.valueOf(configLimit)).append(Constants.SPECIAL_CHAR_NEWLINE);
		message.append("Current Queued Size: " + currSize).append(Constants.SPECIAL_CHAR_NEWLINE);
		message.append("Status Date: " + DT_FORMATTER.format(new Date())).append(Constants.SPECIAL_CHAR_NEWLINE);		
		template.body = message.toString(); 					
		return template;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
