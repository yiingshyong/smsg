package com.bcb.sgwcore.senderprocessor.business;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.util.CommonUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;

/*
 * This class responsible for assigning which service provider used to send sms. 
 * Any new routing mode should be added into it as a new method.
 */

public class ServiceProviderRouter {
	
	private static final Log log = LogFactory.getLog(ServiceProviderRouter.class);
	
	public static SmsQueue route(SmsQueue smsQueue, SmsCfg smsCfg) throws Exception{
		return ServiceProviderRouter.route(smsQueue, smsCfg, null);
	}
	
	public static SmsQueue route(SmsQueue smsQueue, SmsCfg smsCfg, String[] ignoreTelcos) throws Exception{
		smsQueue.setTelco(0);
		TblRef theSmsType = smsQueue.getSmsType();
		
		if( theSmsType == null || theSmsType.getTblRefId() == 0){
			//re-check database to get sms type
			theSmsType = DaoBeanFactory.getTblRefDao().selectSMSType(smsQueue.getSentBy(), smsQueue.getChannelId());
			smsQueue.setSmsType(theSmsType);
			if( theSmsType == null || theSmsType.getTblRefId() == 0 ){
				throw new SgwException("Can't determine svc provider. Reason: No sms type tagged to this sms -> " + smsQueue.getId());				
			}
		}
	
		try{		
			Telco telco = null;
			//if routing mode & routable telco is not defined
			if(StringUtils.isEmpty(theSmsType.getRoutingAction()) || StringUtils.isEmpty(theSmsType.getRoutableTelco())){
				log.warn("Routing mode not defined for sms type -> " + theSmsType.getLabel() + ". Switch to default routing mode.");
				telco =  defaultRouting(smsCfg, ignoreTelcos);
			}else{//if they are defined
				telco = (Telco)ServiceProviderRouter.class.getDeclaredMethod(theSmsType.getRoutingAction(), SmsQueue.class, SmsCfg.class, TblRef.class, String[].class).invoke(null, smsQueue, smsCfg, theSmsType, ignoreTelcos);				
			}
			
			if(telco == null){
				log.warn("No available Service Provider found for " +
						"sms type -> " + theSmsType.getLabel() + " using routing mode -> " + theSmsType.getRoutingAction() + 
						". Trying to switch to default routing mode.");
				if((telco = defaultRouting(smsCfg, ignoreTelcos)) == null){
					log.error("Can't find any available service provider. Please check.");
					return smsQueue;
				}
			}
			smsQueue.setTelco(telco.getId());
			smsQueue.setTelcoName(telco.getTelcoName());
			return smsQueue;			
		}catch(Exception e){
			log.error("Error while assigning service provider to SMS. Due to: " + e.getMessage(), e);
			return smsQueue;
		}
	}

	@SuppressWarnings("unused")
	private static Telco routeByPriority(SmsQueue smsQueue, SmsCfg smsCfg, TblRef theSmsType, String[] ignoreTelcos){
		
		List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAvailableTelcoByIds(
								CommonUtil.remove(StringUtils.split(theSmsType.getRoutableTelco(), smsCfg.getTableListDelimiter()), ignoreTelcos)
							);		
		if(telcos == null || telcos.isEmpty()){
			return null;
		}
		
		return telcos.get(0);
	}
	
	@SuppressWarnings("unused")
	private static Telco routeByPrefix(SmsQueue smsQueue, SmsCfg smsCfg, TblRef theSmsType, String[] ignoreList){
		List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAvailableTelcoByIds(
								CommonUtil.remove(StringUtils.split(theSmsType.getRoutableTelco(), smsCfg.getTableListDelimiter()), ignoreList)
								);
		
		if(telcos == null || telcos.isEmpty()){
			return null;
		}
		
		for(Telco telco : telcos){
				String[] prefix=StringUtils.split(telco.getTelcoPrefix(), smsCfg.getTableListDelimiter());
				if(prefix == null){
					break;
				}
				for(int i=0; i<prefix.length; i++){
					if(prefix[i].length()>0 && BusinessService.normalizeMobile(smsQueue.getMobileNo()).startsWith(prefix[i])){
						return telco;
					}
				}
		}
		
		//if no matching telco found by prefix, take the first available telco
		log.debug("No matching telco found by prefix.");
		if(telcos != null && !telcos.isEmpty()){
			log.debug("Assign the first available telco -> " + telcos.get(0).getTelcoName());
			return telcos.get(0);			
		}
		
		return null;
	}
	
	private static Telco defaultRouting(SmsCfg smsCfg, String[] ignoreTelcos){
//		List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAllTelcoEsmsAvailableOrderByPriority();
		if(StringUtils.isEmpty(smsCfg.getDefaultRoutableTelco())) {
			log.warn("Default routing service provider is not set.");
			return null;
		}
		
		List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAvailableTelcoByIds(
				CommonUtil.remove(StringUtils.split(smsCfg.getDefaultRoutableTelco(), smsCfg.getTableListDelimiter()),ignoreTelcos)
				);	
		
		if(telcos == null || telcos.size() ==0) return null;
		return telcos.get(0);
	}
}
