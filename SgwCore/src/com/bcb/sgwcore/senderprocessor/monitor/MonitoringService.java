package com.bcb.sgwcore.senderprocessor.monitor;

import java.net.MalformedURLException;
import java.util.Date;

import javax.management.InstanceNotFoundException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.sgwcore.senderprocessor.business.NotificationService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class MonitoringService extends DaoBeanFactory{

	private static final Log log = LogManager.getLog();
 	private JMXConnector jmxConnector;
 	private static MonitoringService instance;
 	
 	private MonitoringService(){
 		setupJmxClient();
 	}
 	
 	public static MonitoringService getInstance(){
 		if(instance == null) instance = new MonitoringService();
 		return instance;
 	}
 	 	 	
	public void svcProviderQueueThresholdChecking(SmsCfg smsConfig, Telco svcProvider, String queueName){
		log.info("Checking q threshold for svc provider: " + svcProvider.getTelcoName() + " for queue " + queueName);
		if( !(CommonUtils.NullChecker(smsConfig.getQueueSmsThresholdNotification(), String.class).toString().length() > 0)){
			//threshold notification disable;
			log.debug("Queue sms threshold notification disabled.");
		}
//		Integer smsOnhand = Integer.valueOf(CommonUtils.NullChecker(svcProvider.getSmsOnHand(), Integer.class).toString());
		try{
			jmxConnector.connect();
			Integer smsOnhand = ((Long)jmxConnector.getMBeanServerConnection().getAttribute(new ObjectName("org.apache.activemq:BrokerName=localhost,Type=Queue,Destination=" + queueName), "QueueSize")).intValue();
		
			if( smsOnhand > Integer.valueOf(smsConfig.getQueueSmsThreshold()) ){// Threshold reach			
				log.info("Threshold exceeded! Current size: " + smsOnhand + " Threshold size: " + smsConfig.getQueueSmsThreshold() );
				boolean hasNotification = false;
						if(smsConfig.getQueueSmsThresholdNotification().contains("E")){
							log.info("Sending email notification");
							NotificationService.sendQueueThresholdEmailNotification(queueName, smsOnhand, smsConfig);
							hasNotification = true;
						}
						
						if(smsConfig.getQueueSmsThresholdNotification().contains("S")){
							log.info("Sending sms notification");
							NotificationService.sendQueueThresholdSmsNotification(queueName, smsOnhand, smsConfig);
							hasNotification = true;
						}
						
						if(hasNotification){// Have perform notification action, record the notification time
							svcProvider.setSmsQueueThresholdNotificationDatetime(new Date());
							svcProvider.setModifiedBy("ThresholdCheck");
							svcProvider.setModifiedDatetime(new Date());
							getTelcoDao().update(svcProvider);
						}
				}
		}catch(InstanceNotFoundException infe){
			log.error("Queue name " + queueName + " not found. Please check.", infe);
		}catch(Exception e){
			log.error("Error while getting queue size", e);
		}
	}
	
	public void destroy(){
		if(this.jmxConnector != null){
			try {
				log.warn("Closing jmx connection.");
				this.jmxConnector.close();
			} catch (Exception e) {
				log.error("Error while closing jmx connection. Due to: " + e.getMessage(), e);
			}
		}
	}
	
	private void setupJmxClient(){
		try {
			log.info("Creating jmx connection to activemq");
			jmxConnector = JMXConnectorFactory.newJMXConnector(createConnectionURL(AppPropertiesConfig.MQ_HOST, Integer.parseInt(AppPropertiesConfig.MQ_JMX_PORT)), null);			
		}catch(Exception e){
			log.error("Error while connecting to activemq jmx");
		}
		
	}
	
	private JMXServiceURL createConnectionURL(String host, int port) throws MalformedURLException
	{
	    return new JMXServiceURL("rmi", "", 0, "/jndi/rmi://" + host + ":" + port + "/jmxrmi");
	}	
	
}
