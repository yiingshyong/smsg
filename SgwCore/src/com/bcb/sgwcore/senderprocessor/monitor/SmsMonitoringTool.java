package com.bcb.sgwcore.senderprocessor.monitor;

import org.apache.commons.logging.Log;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

public class SmsMonitoringTool implements WrapperListener{

	private static Log log = LogManager.getLog();
	
	private TotalQueuedMonitor totalQueueMonitor = new TotalQueuedMonitor();
	private TelcoThresholdMonitor thresholdMntr = new TelcoThresholdMonitor();

	public static void main(String[] args) {
		WrapperManager.start(new SmsMonitoringTool(), args);		
	}
	public Integer start(String[] args) {
		totalQueueMonitor.start();
		thresholdMntr.start();
		return null;
	}

	public int stop(int arg0) {
		totalQueueMonitor.shutdown();
		thresholdMntr.shutdown();
		MonitoringService.getInstance().destroy();
		return 0;
	}
	
	public void shutdown(){
		log.info("Shutting down SMS Monitoring Tool");
		totalQueueMonitor.shutdown();
		thresholdMntr.shutdown();
	}

	public void controlEvent(int event) {
		 if (WrapperManager.isControlledByNativeWrapper()) { 
		 } else { 
			 if ((event ==
		  WrapperManager.WRAPPER_CTRL_C_EVENT) || (event ==
		  WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event ==
		 WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){ WrapperManager.stop(0); } }
	}

	
}
