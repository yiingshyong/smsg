package com.bcb.sgwcore.senderprocessor.monitor;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class TelcoThresholdMonitor extends Thread{

	private static Log log = LogManager.getLog();
	private boolean run = true;
	
	@Override
	public void run() {	
		log.info("Starting " + this.getClass().getSimpleName());
		try{
			while(run){
				SmsCfg config = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
				Integer checkPeriod = (config.getQueueSmsThresholdPeriod() == null || config.getQueueSmsThresholdPeriod().length()==0)? 0 : Integer.parseInt(config.getQueueSmsThresholdPeriod());
								
				if( checkPeriod < 0){
					log.warn(getClass().getSimpleName() + " was turned off.");				
				}else{
					log.info("Monitoring ...");
					List<Telco> telcos = DaoBeanFactory.getTelcoDao().findAll();
					Map<String, Telco> telcoDB = new HashMap<String, Telco>();
					for(Telco t : telcos){
						telcoDB.put(t.getTelcoName(), t);
					}
					List<TblRef> smsTypes = DaoBeanFactory.getTblRefDao().getSmsTypes();
					Map<String, TblRef> tblRefDb = new HashMap<String, TblRef>();
					for(TblRef r : smsTypes){
						tblRefDb.put(r.getLabel(), r);
					}
					
					List<SmsMQ> queues = DaoBeanFactory.getSmsMQDao().findAllNonClosedMQ();
					for(SmsMQ queue : queues){
						if(telcoDB.get(queue.getTelcoName()) == null){
							log.warn("No Telco With Name: '" + queue.getTelcoName() + "'. Please check.");
							continue;
						}
						MonitoringService.getInstance().svcProviderQueueThresholdChecking(config, telcoDB.get(queue.getTelcoName()), BusinessService.getQueueName(queue.getTelcoName(), tblRefDb.get(queue.getValue())));																			
					}
				}
				yield();
				sleep(checkPeriod*60*1000);
			}	
		}catch(Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		log.info(this.getClass().getSimpleName() + " Stopped.");
	}
	
	
	public void shutdown(){
		log.info("Shutting down " + this.getClass().getSimpleName());
		run = false;
	}
	
}