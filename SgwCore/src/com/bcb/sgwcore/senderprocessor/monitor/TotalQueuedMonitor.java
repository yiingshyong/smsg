package com.bcb.sgwcore.senderprocessor.monitor;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.NotificationService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class TotalQueuedMonitor extends Thread{

	private static Log log = LogManager.getLog();
	private boolean run = true;
	
	@Override
	public void run() {	
		log.info("Starting " + this.getClass().getSimpleName());
		try{
			while(run){
				SmsCfg config = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
				int checkPeriod = config.getTotalQueuedSizeAlertCheckPeriod();
				
				if( checkPeriod < 0){
					log.warn("Total Queued Size Monitoring was turned off.");				
				}else{
					log.info("Monitoring ...");
					Integer currQueuedsize = DaoBeanFactory.getSmsQueueDao().findCurrentQueueSize();
					if( currQueuedsize > config.getTotalQueuedSizeAlertThreshold()){
						log.info("Hit SMS Total Q Threshold ");
						NotificationService.sendTotalQEmailAlert(currQueuedsize, config);
						NotificationService.sendTotalQSmsAlert(currQueuedsize, config);
				}
				yield();
				sleep(checkPeriod*60*1000);
				}	
			}
		}catch(Exception e){
			log.error(e);
		}
		log.info(this.getClass().getSimpleName() + " Stopped.");		
	}
	
	public void shutdown(){
		log.info("Shutting down " + this.getClass().getSimpleName());
		run = false;
	}
	
}
