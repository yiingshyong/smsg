package com.bcb.sgwcore.senderprocessor.sender;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LogManager {

	static Log log = null;
	
	
	public static Log getLog()
	{
		if (log==null)
		{
			log = LogFactory.getLog(LogManager.class);
		}
		return log;
	}
	
}
