package com.bcb.sgwcore.senderprocessor.sender;

import org.apache.commons.logging.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

import com.bcb.sgwcore.senderprocessor.LogManager;

public class MainSenderProcessor implements WrapperListener{
	
	private static final Log log = LogManager.getLog();
	private SenderProcessorManager senderProcessorManager;
	private ApplicationContext applicationContext;
	
	public static void main(String args[])  {
		
		WrapperManager.start(new MainSenderProcessor(), args);
		
	}
	public void controlEvent(int event)
	{
		
		 if (WrapperManager.isControlledByNativeWrapper()) { 
		 } else { 
			 if ((event ==
		  WrapperManager.WRAPPER_CTRL_C_EVENT) || (event ==
		  WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event ==
		 WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){ WrapperManager.stop(0); } }
		
	}

	public Integer start(String[] param){
		log.debug("MainSenderProcessor start");
		applicationContext = new ClassPathXmlApplicationContext("sp-jmx.xml");
		senderProcessorManager = (SenderProcessorManager) applicationContext.getBean("SenderManager");
		senderProcessorManager.start();
		return null;
	}

	public int stop(int exitcode){
		log.debug("MainSenderProcessor stop");
		senderProcessorManager.shutdown();
		log.info("main conversion processor shut down End");
		return 0;
	}

}
