package com.bcb.sgwcore.senderprocessor.sender;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.dao.GroupDao;
import com.bcb.crsmsg.dao.ISmsCfgDao;
import com.bcb.crsmsg.dao.ISmsDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.dao.IUserDao;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.ITelcoSmsSender;
import com.bcb.sgwcore.senderprocessor.LogManager;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.senderprocessor.business.JmsProducerManager;
import com.bcb.sgwcore.senderprocessor.business.ServiceProviderRouter;
import com.bcb.sgwcore.util.CommonUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.bcb.sgwcore.util.SendMailBean;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;
import com.bcb.sgwcore.util.SgwParam;

public class MyConsumer implements MessageListener, ExceptionListener {

	
	private static final Log log = LogManager.getLog();
	private Telco telcoEsms;
	private ITelcoSmsSender telcoSmsSender;
//	private static int defaultRetry = 3;
	private ISmsDao smsDao = null;
	private ISmsQueueDao smsQueueDao = null;
	private	SmsQueue SMSQueue,smsQ;
	private int counter;
	private String mqName;
	private SmsCfg smsConfig;
	private int failCounter=0;
	boolean finishTask=true;
	private static Boolean RETRYING = false;
	private	String orgTelco;
	private boolean end = false;

	
	public MyConsumer(ITelcoSmsSender telcoSmsSender,int counter,String mqName){///counter and mqName is for testing only

		log.debug("Instantiate Sender Thread");
		try {
			smsDao = (ISmsDao) SgwCoreHibernateUtil.getBean("smsDao");
			smsQueueDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
			//smsList = new LinkedList<SmsQueue>();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		this.telcoSmsSender = telcoSmsSender;
		this.counter = counter;
		this.mqName = mqName;
		
		
	}
	
	synchronized public void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
        System.exit(1);
	}

	public void onMessage(Message message) {
		finishTask=false;
		end = false;
		boolean expiredSMS = false;
		if (message instanceof ObjectMessage) {
            try {
            	SMSQueue = (SmsQueue)((ObjectMessage) message).getObject();
  			    log.debug("Thread "+mqName+", no. "+counter+", Received sms.getId() = " + SMSQueue.getId());
  	
  				ArrayList<String> returnCodes = new ArrayList<String>();
  				ArrayList<SmsQueue> smsList = new ArrayList<SmsQueue>() ;	//new design will always one sms in the list
  				smsList.add(SMSQueue);
				
				ITelcoEsmsDao telcoEsmsDao = (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
				this.telcoEsms=telcoEsmsDao.findById(SMSQueue.getTelco()); 
				
				log.debug("Telco status = " + this.telcoEsms.getStatus());
				if(this.telcoEsms.getStatus().equals(Constants.FORM_FIELD_STATUS_CLOSED) 
						|| ( this.telcoEsms.getStatus().equals(Constants.FORM_FIELD_STATUS_DISABLED) 
						&& (SMSQueue.getRemarks()==null || !SMSQueue.getRemarks().equals(Constants.SMS_REMARK_TELCO_CHECK))) ){
//					// need to release the sms queue list of the closed telco and route to other telco to resend
					smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
//					smsQ = BusinessService.serviceProviderRouting(SMSQueue, smsConfig);
					smsQ = ServiceProviderRouter.route(SMSQueue, smsConfig);
					log.debug("Next available telco = " + smsQ.getTelco());
					if(smsQ.getTelco() != 0){
						JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(smsQ.getTelcoName(), smsQ.getSmsType()), smsQ);
						//BusinessService.updateSmsOnHand(this.telcoEsms.getId(), -1);
						//BusinessService.updateSmsOnHand(smsQ.getTelco(), 1);
					}else{
						smsQueueDao.updateStatus(smsList, Constants.SMS_STATUS_4);
						//BusinessService.updateSmsOnHand(this.telcoEsms.getId(), -1);
						BusinessService.updateQuota(SMSQueue, smsConfig,"SenderThread", true);
					}
					
					failCounter=0;
				}else{
					log.debug("smsQueue: "+smsList);
					ISmsCfgDao smsCfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
					SmsCfg theCfg=smsCfgDao.findByCfgId(Constants.APPSVERSION);
					
					List<SmsQueue> inMemorySms = new ArrayList<SmsQueue>(smsList);
					if(BusinessService.discardTimeCtrlSms(inMemorySms).size() !=0 ){
						telcoSmsSender.setupCfg(this.telcoEsms);
						//check time validity
						if(BusinessService.hasSMSExpired(smsList.get(0))){
			            	SMSQueue.setRemarks(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.TIME_EXPIRED_SMS_REMARK));
			            	SMSQueue.setSmsStatusDatetime(new Date());
			            	SMSQueue.setSmsStatusBy("SP");
			            	SMSQueue.setSmsStatus(Constants.SMS_STATUS_2);
							if(smsDao.createObjectBySmsQueue(SMSQueue)){
								smsQueueDao.deleteById(SMSQueue.getId());
							}            	
							return;
						}
						returnCodes = CommonUtil.normalizeReturnCode(telcoSmsSender.sendMessage(smsList));
						log.debug("returnCodes: "+returnCodes);

						boolean retry=false;
						if (returnCodes.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// fail to send
							retry=true;
						}else{// check for retry code
							String[] retryCode=CommonUtils.NullChecker(this.telcoEsms.getRetryCode(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
							for(int i=0;i<retryCode.length;i++){
								if(returnCodes.contains(retryCode[i])){
									retry=true;
								}
							}
						}
						log.debug("retry: "+retry);
						boolean otherThreadTrying = false;
						if(Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SP_SINGLE_RETRY))){
							if(retry){
								//to make sure no multiple thread retry sending at the same time
								synchronized (SenderProcessorManager.SVC_PROVIDERS_RETRY) {
									if(!SenderProcessorManager.SVC_PROVIDERS_RETRY.get(mqName)){
										SenderProcessorManager.SVC_PROVIDERS_RETRY.put(mqName, true);
									}
									else{
										otherThreadTrying = true;
									}
								}
							}
							if(otherThreadTrying){
								log.warn("Other thread doing retry");
								smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
								if(!Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SP_FAILEDOVER_ON_RETRY))){
									synchronized (SenderProcessorManager.SVC_PROVIDERS_RETRY) {
										SenderProcessorManager.SVC_PROVIDERS_RETRY.wait();
										smsQ = ServiceProviderRouter.route(SMSQueue, smsConfig);									
									}
								}else{
									smsQ = ServiceProviderRouter.route(SMSQueue, smsConfig, new String[]{String.valueOf(SMSQueue.getTelco())});									
								}
								log.debug("Next available telco = " + smsQ.getTelco());
								if(smsQ.getTelco() != 0){
									JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(smsQ.getTelcoName(), smsQ.getSmsType()), smsQ);
								}else{
									smsQueueDao.updateStatus(smsList, Constants.SMS_STATUS_4);
									BusinessService.updateQuota(SMSQueue, smsConfig,"SenderThread", true);
								}
								retry=false;
								end = true;
							}															
						}
						if(retry){// got error or retry code
//						if(retry){// got error or retry code
							log.debug("going to retry send sms queue");
							if(retry(smsList, returnCodes)){// retry completed
								// continue the process
								log.debug("retry done, either successful or fail completely, update status");
								log.debug("telco status: "+this.telcoEsms.getStatus());
								if(this.telcoEsms.getStatus().equals(Constants.STATUS_DISABLED)){
									this.telcoEsms.setStatus(Constants.STATUS_ACTIVE);
									this.telcoEsms.setStatusDatetime(new Date());
									//this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
									telcoEsmsDao.update(this.telcoEsms);
								}
								telcoSmsSender.calculateCharge(smsList, returnCodes);
								statusUpdating(smsList, returnCodes);
								failCounter=0;
							}else{//failed on retry sending sms
								log.debug("retry failed");
								// Temporary disable the telco
								if(this.telcoEsms!=null){
									failCounter+=1;
									boolean update=false;
									boolean firstDisabled = false;
									if(this.telcoEsms.getStatus().equals(Constants.STATUS_ACTIVE)){
										this.telcoEsms.setStatus(Constants.STATUS_DISABLED);// temporary disabled
										this.telcoEsms.setStatusDatetime(new Date());
										//this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
										telcoEsmsDao.update(this.telcoEsms);
										firstDisabled = true;
									}
//									else{
									long disabledTime=(new Date().getTime()-this.telcoEsms.getStatusDatetime().getTime());
									log.debug("Svc Provider : " + this.telcoEsms.getTelcoName() + " was disabled for " + (disabledTime/1000) + " seconds");
									if(CommonUtils.NullChecker(theCfg.getSmsFailureThresholdPeriod(), String.class).toString().length()>0&&
											disabledTime<Long.valueOf(theCfg.getSmsFailureThresholdPeriod())*1000*60){// to hold fail counter before reach enough recheck time										
										failCounter=0;
										log.debug("Not yet reach recheck time, reset counter");
									}
//									}
								
									if(CommonUtils.NullChecker(theCfg.getSmsFailureNotification(), String.class).toString().contains("E")){// send email
										if((Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SMS_FAILED_ALERT_IMMEDIATE)) && firstDisabled) || (CommonUtils.NullChecker(theCfg.getSmsFailureThreshold(), String.class).toString().length()>0&&
												failCounter>=Integer.valueOf(theCfg.getSmsFailureThreshold()))){
											String[] toList=CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
											log.debug("Sending email notification");
											for(int i=0;i<toList.length;i++){
												sendFailureEmailNotification(this.telcoEsms, toList[i], theCfg, disabledTime, failCounter);
											}
											update=true;
										}
									}
									if(CommonUtils.NullChecker(theCfg.getSmsFailureNotification(), String.class).toString().contains("S")){// send sms
										if((Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SMS_FAILED_ALERT_IMMEDIATE)) && firstDisabled) || (CommonUtils.NullChecker(theCfg.getSmsFailureThreshold(), String.class).toString().length()>0&&
												failCounter>=Integer.valueOf(theCfg.getSmsFailureThreshold()))){
											String[] toList=CommonUtils.NullChecker(theCfg.getSmsFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
											log.debug("Sending SMS notification");
											for(int i=0;i<toList.length;i++){
												sendFailureSmsNotification(this.telcoEsms, toList[i], theCfg, disabledTime, failCounter);
											}
											update=true;
										}
										 
									}
									if(update){
										failCounter=0;
										this.telcoEsms.setStatusDatetime(new Date());
										telcoEsmsDao.update(this.telcoEsms);
									}
								}
								
								// need to release the sms queue list of the disabled telco and route to other telco to resend
								log.debug("retry failed");
								smsConfig = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
//								smsQ = BusinessService.serviceProviderRouting(SMSQueue, smsConfig);
								orgTelco = SMSQueue.getTelcoName();
								smsQ = ServiceProviderRouter.route(SMSQueue, smsConfig);
								if(smsQ.getTelco() != 0){
									smsQ.setRemarks( (smsQ.getRemarks()==null? "" : smsQ.getRemarks() +  com.bcb.common.util.Constants.LINE_SEPARATOR) + orgTelco + "->" + smsQ.getTelcoName());
									JmsProducerManager.getInstance().sendMessage(BusinessService.getQueueName(smsQ.getTelcoName(), smsQ.getSmsType()), smsQ);
									//BusinessService.updateSmsOnHand(this.telcoEsms.getId(), -1);
									//BusinessService.updateSmsOnHand(smsQ.getTelco(), 1);
								}else{
									smsQueueDao.updateStatus(smsList, Constants.SMS_STATUS_4);
									BusinessService.updateQuota(SMSQueue,smsConfig,"SenderThread",true);
									//smsList= new LinkedList<SmsQueue>();
									//BusinessService.updateSmsOnHand(this.telcoEsms.getId(), -1);
								}
							}
							if(Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SP_SINGLE_RETRY)) && !otherThreadTrying){
								synchronized (SenderProcessorManager.SVC_PROVIDERS_RETRY) {
									SenderProcessorManager.SVC_PROVIDERS_RETRY.put(mqName, false);
									if(!Boolean.parseBoolean(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.SP_FAILEDOVER_ON_RETRY))){
										SenderProcessorManager.SVC_PROVIDERS_RETRY.notifyAll();
									}
								}							
							}
						}else if(end){
							//do nothing
						}
						else{// no error or retry on first sending
							log.debug("telco status: "+this.telcoEsms.getStatus());
							if(expiredSMS){
								
							}
							else if(this.telcoEsms.getStatus().equals(Constants.STATUS_DISABLED)){
								this.telcoEsms.setStatus(Constants.STATUS_ACTIVE);
								this.telcoEsms.setStatusDatetime(new Date());
								//this.telcoEsms.setSmsOnHand(Constants.VALUE_0);
								telcoEsmsDao.update(this.telcoEsms);
								//tested
							}
							log.debug("The message is: " + smsList.get(0).getMessage() + ", id: " + smsList.get(0).getId());
							telcoSmsSender.calculateCharge(smsList, returnCodes);
							statusUpdating(smsList, returnCodes);
							failCounter=0;
						}
					}else{
						log.debug("Blocked by time control, sms.getId() = " + SMSQueue.getId());
					}
					
				}
				
				log.debug("Thread "+mqName+", no = "+counter+" end, failCounter = " + failCounter);
				
  	
            }catch (JMSException ex) {
            	log.error("SMS NOT SEND - Caught JMS Exception. Due to: " + ex.getMessage(), ex);
            }catch (SecurityException se){
            	log.error("SMS NOT SEND - Unable to decrypt message content.Message will be routed to unsent folder. Details: ", se);
            	SMSQueue.setRemarks("Unable to decrypt message content.");
            	SMSQueue.setSmsStatusDatetime(new Date());
            	SMSQueue.setSmsStatusBy("SP");
            	SMSQueue.setSmsStatus(Constants.SMS_STATUS_2);
				if(smsDao.createObjectBySmsQueue(SMSQueue)){
					smsQueueDao.deleteById(SMSQueue.getId());
				}            	
            }catch(Exception ex){
            	log.error("SMS NOT SEND - Unexpected exception while sending SMS. ID: " + SMSQueue.getId() + ". Due to: " + ex.getMessage(), ex);
            	SMSQueue.setRemarks("Error. " + ex.getMessage());
            	SMSQueue.setSmsStatus(Constants.SMS_STATUS_2);
				if(smsDao.createObjectBySmsQueue(SMSQueue)){
					log.info("SMS ID: " + SMSQueue.getId() + " routed to UNSENT");
					smsQueueDao.deleteById(SMSQueue.getId());
				}else{
					log.error("SMS ID: " + SMSQueue.getId() + " failed to be routed to UNSENT. Please check.");
				}
			}
            
		}else{
			log.error("This should not happen. Received Non JMS ObjectMessage: " + message);
		}
		finishTask=true;
	
	}
	
	private boolean retry(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes){
		log.debug("retry to send queue sms");
		int retry = 0;;
		SmsCfg theCfg=null;
		try {
			ISmsCfgDao cfgDao = (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
			theCfg = cfgDao.findByCfgId(Integer.valueOf(Constants.APPSVERSION).toString());
			if (theCfg == null){
				return false;
			}else{
//				if(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString().length()>0){
					retry=Integer.valueOf(CommonUtils.NullChecker(theCfg.getAutoSmsResendLimit(), Integer.class).toString());
//				}
				int runningRetry = 0;
				boolean retryStatus=true;
				telcoSmsSender.setupCfg(this.telcoEsms);
				
				while (retryStatus&&runningRetry< retry){
					log.info("Retry #" + runningRetry + " id:" + smsQueue.get(0).getId());
					if(CommonUtils.NullChecker(theCfg.getAutoSmsResendInterval(), String.class).toString().length()>0&&
							Integer.valueOf(theCfg.getAutoSmsResendInterval())>0){
						sleepforRetryInterval(Integer.valueOf(theCfg.getAutoSmsResendInterval())*1000);
					}else{// minimum retry interval is 1 minute
						sleepforRetryInterval(60000);
					}
					
					retryStatus=false;
					//log.debug("runningRetry: " + runningRetry + ", SMS Id: " + smsQueue.getId());					
					CommonUtil.normalizeReturnCode(telcoSmsSender.sendMessage(smsQueue, returnCodes));
					log.debug("retry return Codes: "+returnCodes);
					if (returnCodes.contains(SgwParam.RETURN_CODE_SGW_PROBLEM)){// fail to send
						retryStatus=true;
					}else{// check for retry code
						String[] retryCode=CommonUtils.NullChecker(this.telcoEsms.getRetryCode(), String.class).toString().split("["+Constants.SPECIAL_CHAR_PIPE+"]");
						
						for(int i=0;i<retryCode.length;i++){
							if(returnCodes.contains(retryCode[i])){
								retryStatus=true;
							}
						}
					}
					runningRetry++;
				}
				
				// retry limit reached
				if(retryStatus){
					return !requeueSms(smsQueue, returnCodes);
				}else{// no error or retry, retry completed
					return true;
				}
			}
		} catch (SecurityException se){
			throw new SecurityException(se);			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return false;
		}
	}
	
	private boolean requeueSms(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		boolean queue=true;
		try {
			for(int i=0;i<returnCodes.size();i++){
				if(returnCodes.get(i).contains(this.telcoEsms.getSuccessCode())){
					queue=false;
				}
			}
			
			if(queue){
//				log.debug("not contain success case, requeue sms");
//				for(int i=0;i<smsQueue.size();i++){
//					smsQueue.get(i).setRemarks("Error: " + returnCodes);
//					smsQueue.get(i).setSmsStatusBy("System Retry");
////					log.debug(" trying to requeue sms");
////					smsQueueDao.updateStatus(smsQueue.get(i), Constants.SMS_STATUS_4);don't have to update, as sms will be failed over
//					DaoBeanFactory.getSmsQueueDao().update(smsQueue.get(i));
//				}
			}else{
				log.debug("contain success msg, cannot requeue");
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return queue;
	}
	
	private void statusUpdating(ArrayList<SmsQueue> smsQueue, ArrayList<String> returnCodes) {
		log.debug("status updating");
		try {
			log.debug("return Codes: "+returnCodes);
			for(int i=0;i<returnCodes.size();i++){
				if(returnCodes.get(i).contains(this.telcoEsms.getSuccessCode())){
					log.debug("sms success sent");
					smsQueue.get(i).setSmsStatus(Constants.SMS_STATUS_1);
				}else{
					log.debug("sms failed to send");
					smsQueue.get(i).setRemarks("Error: " + returnCodes.get(i));
					
					smsQueue.get(i).setSmsStatus(Constants.SMS_STATUS_2);
				}
				
				smsQueue.get(i).setSmsStatusBy("automated");
				smsQueue.get(i).setSmsStatusDatetime(new Date());
				log.debug(" finished ");
			}
			
			log.debug(" trying to save sms and delete record in smsQueue");
			log.debug("smsQueue: "+smsQueue);
			for(int i=0;i<smsQueue.size();i++){
				log.debug("smsQueue size: "+smsQueue.size());
				log.debug("smsQueue: "+smsQueue.get(i));
				if(smsQueue.get(i).getSmsType().isEncrypted()){
					smsQueue.get(i).setMessage("");
				}
				if(smsDao.createObjectBySmsQueue(smsQueue.get(i))){
					log.debug("successfully create sms: "+smsQueue.get(i).getId());
					smsQueueDao.deleteById(smsQueue.get(i).getId());
				}else{// failed to save sms, skip delete
					log.error("Failed to save sms, update queue sms so that won't be process again");
					smsQueueDao.update(smsQueue.get(i));
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public void sendFailureEmailNotification(Telco theTelco, String to, SmsCfg theCfg, long disabledTime, int failCounter){
		SendMailBean theMail=new SendMailBean();
		
		String subject="Telco "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+" - "+CommonUtils.NullChecker(theTelco.getStatus(), String.class).toString();
		String message="Telco Name: "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
		"Telco Status: "+theTelco.getStatus()+" with failing "+failCounter+" time(s)"+Constants.SPECIAL_CHAR_NEWLINE+
		"Telco Status Duration: "+disabledTime/60/1000+" minute(s)"+Constants.SPECIAL_CHAR_NEWLINE;
		theMail.setP_to(to);
		theMail.setP_from(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmailSender(),String.class).toString().length()>0?theCfg.getSmsFailureNotificationEmailSender():Constants.APPSNAME);
		theMail.setP_subject(subject);
		theMail.setP_message(message);
		
		theMail.send();
	}
	
	public void sendFailureSmsNotification(Telco theTelco, String to, SmsCfg theCfg, long disabledTime, int failCounter){
		try{
			String message="Telco "+CommonUtils.NullChecker(theTelco.getTelcoName(), String.class).toString()+Constants.SPECIAL_CHAR_NEWLINE+
			"Status: "+theTelco.getStatus()+" with failing "+failCounter+" time(s)"+Constants.SPECIAL_CHAR_NEWLINE+
			"Duration: "+disabledTime/60/1000+" minute(s)"+Constants.SPECIAL_CHAR_NEWLINE;
			
			int deptId=0;
			String dept=Constants.STR_EMPTY;
			IUserDao userDao;
			userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
			User usermodal = userDao.findByUserId(theCfg.getSmsFailureNotificationSmsSender());
			if (usermodal == null){
			}else{
				GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
				Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
				if (groupmodal != null){
					deptId=groupmodal.getId();
					dept=groupmodal.getDeptName();
				}
			}
			
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
//			ArrayList<String> lastSmsIdList=new ArrayList<String>();
//			String lastSmsId=Constants.STR_EMPTY;
			
//			for(int j=0;j<msgArray.size();j++){
				SmsQueue aSms=new SmsQueue();
				aSms.setCreatedBy(theCfg.getSmsFailureNotificationSmsSender());
				aSms.setCreatedDatetime(new Date());
				aSms.setDeptId(deptId);
				aSms.setDept(dept);
				aSms.setMessage(message);
				aSms.setTotalSms(msgArray.size());
				aSms.setMobileNo(to);
				aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
				aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
				aSms.setSmsStatus(Constants.SMS_STATUS_4);
				aSms.setSmsStatusBy("SenderThread");
				aSms.setSmsStatusDatetime(new Date());
				aSms.setSentBy(theCfg.getSmsFailureNotificationSmsSender());
				aSms.setType(Constants.SMS_SOURCE_TYPE_5);
				
				ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
				smsDao.insert(aSms);
				
//				lastSmsIdList.add(CommonUtils.NullChecker(aSms.getId(), String.class).toString());
//				
//				aSms.setConcatenateSms(theCfg.getConcatenateSms().equals(Constants.STATUS_YES)?(msgArray.size()==1?Constants.STR_EMPTY:j==0?CommonUtils.NullChecker(aSms.getId(), String.class).toString():j!=msgArray.size()-1?lastSmsId:CommonUtils.arrayToString(lastSmsIdList, theCfg.getTableListDelimiter())):Constants.STR_EMPTY);
//				smsDao.update(aSms);
//				lastSmsId=CommonUtils.NullChecker(aSms.getId(), String.class).toString();
//			}
		}catch(Exception ex){
			log.error(ex.getMessage(), ex);
		}
	}
	
	private void sleepforRetryInterval(int retryInterval) {
		try {
			log.debug("sleep for retry interval");
			Thread.yield();
			Thread.sleep(retryInterval);
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
		}
	}
}
