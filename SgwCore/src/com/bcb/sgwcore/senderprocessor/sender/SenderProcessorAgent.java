package com.bcb.sgwcore.senderprocessor.sender;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.sgwcore.senderprocessor.ITelcoSmsSender;
import com.bcb.sgwcore.senderprocessor.LogManager;
public class SenderProcessorAgent extends Thread{
	
	private static final Log log = LogManager.getLog();
	boolean isRunning2;
	int counter;
    ActiveMQConnectionFactory connFactory; 
	Connection connection;
	Session session;
	Destination destination;
	MessageConsumer consumer;
	MyConsumer myConsumer;
    
    public SenderProcessorAgent(ITelcoSmsSender telcoSmsSender,int counter, String mqName) {
    		
    	this.counter = counter;		///
    	
    	connFactory = new ActiveMQConnectionFactory("tcp://" + AppPropertiesConfig.MQ_HOST + ":" + AppPropertiesConfig.MQ_PORT);
		try{
			connection = connFactory.createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        destination = session.createQueue(mqName);
	        consumer = session.createConsumer(destination);
	
	        myConsumer = new MyConsumer(telcoSmsSender,counter,mqName);
	       	connection.setExceptionListener(myConsumer);
	       	consumer.setMessageListener(myConsumer);
	       	connection.start();
		}catch(Exception e){
			log.error("Error creating connection to broker. Due to: " + e.getMessage(), e);
			throw new RuntimeException(e);
		}
		
    	System.out.println("in spthread "+mqName+", counter = " + counter);
    }

	
	public SenderProcessorAgent(SenderProcessorAgent senderProcessorAgent) {
		// TODO Auto-generated constructor stub
	}


	public void run(){
		try {
			synchronized (this) {
				wait();
			} 
			log.debug("bye bye counter = " + counter);
		}catch (Exception ex) {
			ex.printStackTrace();
		}finally{
			try{
				if(consumer !=null){consumer.close();}
				if(session !=null){session.close();}
				if(connection !=null){connection.close();}
			}catch (Exception ex) {ex.printStackTrace();}
		}
	}
	
	public void shutdown(){
		try{
			connection.stop();
			connection.close();
			log.debug(counter + " close.");
			int z=0;
			while(z==0){
				if(myConsumer.finishTask==true){
					z++;
					log.debug("shutdown ok = " + counter);
				}
			}
			synchronized (this) {
				notify();
			} 
		
		}catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
	}
}



