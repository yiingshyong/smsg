package com.bcb.sgwcore.senderprocessor.sender;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.sgwcore.senderprocessor.ITelcoSmsSender;
import com.bcb.sgwcore.senderprocessor.LogManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

@ManagedResource
public class SenderProcessorManager extends Thread{
	
	private static final Log log = LogManager.getLog();
	List <SmsMQ> smsMQList;
	SmsMQ smsMQ;
	SenderProcessorAgent senderProcessorAgent;
	Map<String,Map<Integer,SenderProcessorAgent>> mqMap =Collections.synchronizedMap(new HashMap<String,Map<Integer,SenderProcessorAgent>>());
	public static Map<String,Boolean> SVC_PROVIDERS_RETRY= Collections.synchronizedMap(new HashMap<String,Boolean>());
	Map<Integer,SenderProcessorAgent> tempMap;
	ITelcoSmsSender telcoSmsSender;
	int counter =501;	//<--testing purpose, need to remove
	
	
	public SenderProcessorManager(){
		try{
			smsMQList = DaoBeanFactory.getSmsMQDao().findAllNonClosedMQ();
		}catch(Exception e){log.error(e.getMessage(), e);}
		
	}
	@Override
	public void run(){
		
		try{
			String mqName;
			
			for(int r=0; r<smsMQList.size(); r++){
				smsMQ = (SmsMQ) smsMQList.get(r);
				SenderProcessorManager.SVC_PROVIDERS_RETRY.put(smsMQ.getQueueName(), false);
			}
			for(int r=0; r<smsMQList.size(); r++){
				tempMap =Collections.synchronizedMap(new HashMap<Integer,SenderProcessorAgent>());
				smsMQ = (SmsMQ) smsMQList.get(r);
				mqName =  smsMQ.getTelcoName()+" ("+smsMQ.getValue()+ ")";
				if((Map<Integer,SenderProcessorAgent>) mqMap.get(mqName) !=null){
					tempMap = (Map<Integer,SenderProcessorAgent>) mqMap.get(mqName);
				}
				for(int x=tempMap.size(); x<smsMQ.getNoOfThread(); x++){
					telcoSmsSender = (ITelcoSmsSender) Class.forName(smsMQ.getTelcoClassName()).newInstance();
					senderProcessorAgent = new SenderProcessorAgent(telcoSmsSender,counter,mqName);
					senderProcessorAgent.start();
					tempMap.put(counter,senderProcessorAgent);
					counter++;
				}
				mqMap.put(mqName,tempMap);
				
			}
			
			synchronized (this) {
				wait();
			} 
			
		}catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}finally{
		}
	}
	
	public void shutdown(){
		try{
			int tempMapSize=0;
			for(String s : mqMap.keySet()) {
				tempMap = (Map<Integer,SenderProcessorAgent>) mqMap.get(s);
				int[] counters = new int[tempMap.size()];
				int a = 0;
				for(Integer counterInMap : tempMap.keySet()) {
					counters[a] = counterInMap;
					a++;
				} 
				tempMapSize = tempMap.size();
				for(int x=0; x<tempMapSize; x++){
					senderProcessorAgent = (SenderProcessorAgent) tempMap.get(counters[x]);
					senderProcessorAgent.shutdown();
					tempMap.remove(counters[x]);
				}
				mqMap.put(s,tempMap);
			}

			synchronized (this) {
				notify();
			} 
		}catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	@ManagedAttribute
	public String getQueueAndNumberOfThread() {
		StringBuffer sb = new StringBuffer();
		for(String s : mqMap.keySet()) {
			sb.append(s+" : Running thread = "+mqMap.get(s).size()+"\n");
			
		}
		System.out.println(sb.toString());//dunno why will print twice at wrapper log
		return sb.toString();
	}
	
	@ManagedOperation
	public void adjustThreads(String mqName, int threads) {
		try{
			for(String s : mqMap.keySet()) {
				if(mqName.equals(s)){
					tempMap = (Map<Integer,SenderProcessorAgent>) mqMap.get(mqName);
					if(tempMap.size() != threads){
						if(tempMap.size() < threads){//add thread
							for(int x=tempMap.size(); x<threads; x++){
								smsMQ = DaoBeanFactory.getSmsMQDao().getByMQName(mqName);
								if(smsMQ != null){
									telcoSmsSender = (ITelcoSmsSender) Class.forName(smsMQ.getTelcoClassName()).newInstance();
									senderProcessorAgent = new SenderProcessorAgent(telcoSmsSender,counter,mqName);
									senderProcessorAgent.start();
									tempMap.put(counter,senderProcessorAgent);
									counter++;
								}
							}
						}else{ //kill thread
							int[] counters = new int[tempMap.size()];
							int a = 0;
							for(Integer counterInMap : tempMap.keySet()) {
								counters[a] = counterInMap;
								a++;
							} 
							
							int diff = tempMap.size() - threads; 
							for(int x=0; x<diff; x++){
								senderProcessorAgent = (SenderProcessorAgent) tempMap.get(counters[x]);
								senderProcessorAgent.shutdown();
								tempMap.remove(counters[x]);
							}
						}
						mqMap.put(mqName,tempMap);
					}
				}
			}
		}catch(Exception ex) {log.error(ex.getMessage(), ex);}
	}
}
