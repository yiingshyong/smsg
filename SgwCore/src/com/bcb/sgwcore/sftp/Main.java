package com.bcb.sgwcore.sftp;

import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

public class Main implements WrapperListener{
	
	sFTPProcessor sftp = new sFTPProcessor();  
   
	

	public static void main(String args[])
	{
		WrapperManager.start(new Main(), args);
	}

	public void controlEvent(int event)
	{
		
		 if (WrapperManager.isControlledByNativeWrapper()) { 
		 } else { 
			 if ((event ==
		  WrapperManager.WRAPPER_CTRL_C_EVENT) || (event ==
		  WrapperManager.WRAPPER_CTRL_CLOSE_EVENT) || (event ==
		 WrapperManager.WRAPPER_CTRL_SHUTDOWN_EVENT)){ WrapperManager.stop(0); } }
		
	}

	public Integer start(String[] param){	
		sftp.start();
		return null;
	}

	public int stop(int exitcode){
	
		return 0;
	}

}
