package com.bcb.sgwcore.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;

import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.LogManager;


public class CommonUtil {
	
	static Log log = LogManager.getLog();
	
	public static String[] remove(String[] orgArray, String[] ignoreArray){
		if(ignoreArray == null || ignoreArray.length==0){
			return orgArray;
		}
		if(orgArray == null || orgArray.length == 0){
			return orgArray;
		}
		List<String> orgList = Arrays.asList(orgArray);
		List<String> ignoreList = Arrays.asList(ignoreArray);		
		List<String> result = new ArrayList<String>(); 
		for(String item : orgList){
			if(!ignoreList.contains(item)){
				result.add(item);
			}
		}
		return result.toArray(new String[result.size()]);
	}
	
//	public static String normalizeMobile(String mobileNo) {
//		log.debug("mobile: " + mobileNo);
//		mobileNo = mobileNo.trim().replaceAll(" ", "").replaceAll("-", "").replaceAll("[+]", "");
//		if (!mobileNo.startsWith("6"))
//		{
//			mobileNo="6"+mobileNo;
//		}
////		if (!mobileNo.startsWith("+"))
////		{
////			mobileNo="+"+mobileNo;
////		}
//		return mobileNo;
//	}
	
	public static String retrievePrefix(String mobileNo){
		String prefix=Constants.STR_EMPTY;
		
		if(mobileNo.startsWith("6")){
			prefix=mobileNo.substring(0, 4);
		}else if(mobileNo.startsWith("+")){
			prefix=mobileNo.substring(0, 5);
		}else{// no 6 or +
			prefix=mobileNo.substring(0, 3);
		}
		
		return prefix;
	}
	
	public static String httpMessage(String message) {
		try{
		message = message.trim();	
		return message.replaceAll(" ","+");
		}catch(Exception e){
			log.error(e.getMessage(),e);
			return "no";
		}
	}
	
	public static String normalizeReturnCode(String returnCode) {
		try{
			returnCode = returnCode.trim();
			if (returnCode.startsWith(Constants.SPECIAL_DASH))
				return returnCode.substring(1,returnCode.length());
			else 
				return returnCode;
		}catch(Exception e)	{
			log.error(e.getMessage(),e);
			return SgwParam.RETURN_CODE_SGW_PROBLEM;	
		}
	}

	public static ArrayList<String> normalizeReturnCode(ArrayList<String> returnCode) {
		for(int i=0;i<returnCode.size();i++){
			try{
				returnCode.set(i, returnCode.get(i).trim());
			}catch(Exception e)	{
				log.error(e.getMessage(),e);
				returnCode.set(i, SgwParam.RETURN_CODE_SGW_PROBLEM);	
			}
		}
		
		return returnCode;
	}
}
