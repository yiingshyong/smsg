package com.bcb.sgwcore.util;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.bcb.crsmsg.dao.GroupDao;
import com.bcb.crsmsg.dao.IAutoActionDao;
import com.bcb.crsmsg.dao.ICreditCardActivationDao;
import com.bcb.crsmsg.dao.IDepartmentDao;
import com.bcb.crsmsg.dao.IServiceProvider;
import com.bcb.crsmsg.dao.ISgwPriorityDao;
import com.bcb.crsmsg.dao.ISmsCfgDao;
import com.bcb.crsmsg.dao.ISmsDao;
import com.bcb.crsmsg.dao.ISmsMQDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ISmsQuotaDao;
import com.bcb.crsmsg.dao.ISmsRate;
import com.bcb.crsmsg.dao.ISmsTimeControlDao;
import com.bcb.crsmsg.dao.ITblRefDao;
import com.bcb.crsmsg.dao.ITelcoEsmsDao;
import com.bcb.crsmsg.dao.IUserChannel;
import com.bcb.crsmsg.dao.IUserDao;

public class DaoBeanFactory {

	public static HibernateTemplate getHibernateTemplate() {
		try{
			return (HibernateTemplate) SgwCoreHibernateUtil.getBean("hibernateTemplate");			
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}

	public static ISmsQueueDao getSmsQueueDao() {
		try{
			return (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");			
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ISmsDao getSmsDao() {
		try{
			return (ISmsDao) SgwCoreHibernateUtil.getBean("smsDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ISmsCfgDao getSmsCfgDao() {
		try{
			return (ISmsCfgDao) SgwCoreHibernateUtil.getBean("smsCfgDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ITelcoEsmsDao getTelcoDao() {
		try{
			return (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ISmsTimeControlDao getTimeControlDao() {
		try{
			return (ISmsTimeControlDao) SgwCoreHibernateUtil.getBean("smsTimeControlDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ISgwPriorityDao getPriorityDao() {
		try{
			return (ISgwPriorityDao) SgwCoreHibernateUtil.getBean("sgwPriorityDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ITelcoEsmsDao getServiceProviderDao() {
		try{
			return (ITelcoEsmsDao) SgwCoreHibernateUtil.getBean("telcoESmsDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ISmsQuotaDao getSmsQuataDao() {
		try{
			return (ISmsQuotaDao) SgwCoreHibernateUtil.getBean("smsQuotaDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static IUserDao getUserDao() {
		try{
			return (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static GroupDao getGroupDao() {
		try{
			return (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	public static ISmsMQDao getSmsMQDao() {
		try{
			return (ISmsMQDao) SgwCoreHibernateUtil.getBean("smsMQDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	public static ITblRefDao getTblRefDao() {
		try{
			return (ITblRefDao) SgwCoreHibernateUtil.getBean("tblRefDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static IDepartmentDao getDeptDao() {
		try{
			return (IDepartmentDao) SgwCoreHibernateUtil.getBean("deptDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static IUserChannel getUserChannelDao() {
		try{
			return (IUserChannel) SgwCoreHibernateUtil.getBean("userChannelDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}

	public static ISmsRate getSmsRateDao() {
		try{
			return (ISmsRate) SgwCoreHibernateUtil.getBean("smsRateDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}

	public static IServiceProvider getServiceProviderGroupDao() {
		try{
			return (IServiceProvider) SgwCoreHibernateUtil.getBean("serviceProviderDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static ICreditCardActivationDao getCCActivationDao() {
		try{
			return (ICreditCardActivationDao) SgwCoreHibernateUtil.getBean("ccActivationDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
	
	public static IAutoActionDao getAutoActionDao() {
		try{
			return (IAutoActionDao) SgwCoreHibernateUtil.getBean("autoActionDao");
		}catch(Exception e){
			throw new RuntimeException("Error in DaoBeanFactory. Reason: " + e.getMessage(), e);
		}
	}
}
