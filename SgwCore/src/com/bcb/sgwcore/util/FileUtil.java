package com.bcb.sgwcore.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.dao.BulkSMSSetupDao;
import com.bcb.crsmsg.dao.GroupDao;
import com.bcb.crsmsg.dao.IFtpReportDao;
import com.bcb.crsmsg.dao.ISmsQueueDao;
import com.bcb.crsmsg.dao.ISmsQuotaDao;
import com.bcb.crsmsg.dao.ITemplateMessageDao;
import com.bcb.crsmsg.dao.IUserDao;
import com.bcb.crsmsg.dao.SmsInvalidDao;
import com.bcb.crsmsg.modal.BatchSetup;
import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsInvalid;
import com.bcb.crsmsg.modal.SmsQuota;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.queuefileprocessor.FileObj;
import com.bcb.sgwcore.queuefileprocessor.ILineTemplate;
import com.bcb.sgwcore.queuefileprocessor.LogManager;
;

public class FileUtil {
	
	public static final String IN_PROCESS = ".ip";
	public static final String DONE_PROCESS = ".log";
	public static final String LOG = "log";
	static Log log = LogManager.getLog();
	static IFtpReportDao ftpReportDao;
	public static void createFolder(String target) {
		File tdir = new File(target).getParentFile();
		if (!tdir.exists()) {
			log.info(tdir.getAbsolutePath()+ " created");
			tdir.mkdirs();
		}
	}
	
	public static String getFileSeparator() {
		return System.getProperty("file.separator");
	}
	
	public static void getAllFiles(List list, File dir, String[] endsWith)
	throws Exception {
		log.debug("getAllFiles");
		log.debug("dir: "+dir.getAbsolutePath());
		int counter = 0;
		
		if (dir.isDirectory()) {
			File[] files = dir.listFiles();
			log.debug("files=" + files.length);
			for (int i = 0; i < files.length; i++) {
				// log.debug("\t" + i + ".file=" + files[i].getAbsolutePath());
				if (files[i].isDirectory()) {
					// getAllFiles(list, files[i], endsWith);
					// dont do any thing
				} else if(files[i].isFile()) {// is file\
					log.debug("endsWith: "+endsWith);
					if(endsWith==null||endsWith.length==0){
						if(files[i].getName().endsWith(IN_PROCESS)){
							log.debug(files[i].getName()+" is in process");
						}else{
							if (counter > 3000) {
								break;
							}
							
							try {
								list.add(files[i].getPath());
								counter++;
							} catch (Exception e) {
								LogManager.getLog().error(e.getMessage(), e);
							}
						}
					}else{
						for (int j = 0; j < endsWith.length; j++) {
							if(files[i].getName().endsWith(IN_PROCESS)){
								log.debug(files[i].getName()+" is in process");
							}else{
								if (files[i].getName().toUpperCase().endsWith(endsWith[j].toUpperCase())) {
									// list.add(files[i].getName());
									/*
									 * if (counter > FileUtil.LIMIT) { try {
									 * Thread.yield(); Thread.sleep(1000); counter = 0;
									 * } catch (Exception e) { log.error(e.getMessage(),
									 * e); } }
									 */
									
									if (counter > 3000) {
										break;
									}
									
									try {
										list.add(files[i].getPath());
										counter++;
									} catch (Exception e) {
										LogManager.getLog().error(e.getMessage(), e);
									}
									break;
								}
							}
						}
					}
				}else{
					// Can't identity the file type
				}
			}
		} else {
			log.debug("endsWith.length: "+endsWith.length);
			for (int j = 0; j < endsWith.length; j++) {
				if (dir.getName().endsWith(endsWith[j])) {
					// list.add(files[i].getName());
					list.add(dir.getPath());
					break;
				}
			}
		}
	}
	
	public static boolean process(String filename, ILineTemplate lineTemplate, Object aSetup, FtpReport reportModal, SmsCfg theCfg, List<Telco> theTelco) {
		return process(new File(filename), lineTemplate, aSetup, reportModal, theCfg, theTelco);
	}
	
	public static boolean process(File file, ILineTemplate lineTemplate, Object aSetup, FtpReport reportModal, SmsCfg theCfg, List<Telco> theTelco) {
		BufferedReader br = null;
		int successCt = 0;
		int failedCt = 0;
		int emptyLineNo = 0;
		int fileLineNo=2;
		String user =null;
		log.debug("[process] Start process: " + file.getName());
		boolean status=true;
		
		try {
			reportModal.setStartDate(new Date());
			reportModal.setStartTime(new Date());
			
			ftpReportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
			reportModal.setFtpSetup(((BatchSetup)aSetup).getId());
			ftpReportDao.insert(reportModal);
			
			br = new BufferedReader(new FileReader(file));
			String line = br.readLine();// File date
			log.debug("file date: "+line);
			String fileDate = CommonUtils.NullChecker(line, String.class).toString().trim();
			
			if(aSetup.getClass().equals(FtpSetup.class)&&((FtpSetup)aSetup).getFileConversion().equals(Integer.valueOf(Constants.FILE_CONVERSION_PHONE_SYNCHRONISATION))){
				user=((FtpSetup)aSetup).getCreatedBy();
			}else if(aSetup.getClass().equals(FtpSetup.class)&&((FtpSetup)aSetup).getFileConversion().equals(Integer.valueOf(Constants.FILE_CONVERSION_CCA_PHONE_SYNCHRONISATION))){
				user=((FtpSetup)aSetup).getCreatedBy();
			}else if (aSetup.getClass().equals(BulkSmsSetup.class)){
				user=((BulkSmsSetup)aSetup).getCreatedBy();
			}else{
				line = (String) br.readLine();// User Name
				log.debug("user name: "+line);
				user = CommonUtils.NullChecker(line, String.class).toString().trim();
			}
			
			lineTemplate.setRemarks(new ArrayList<String>());
			
			String validateResultHeader = validateFileHeader(fileDate,user, reportModal, aSetup);
			String validateResultTrailer = validateFileTrailer(file.getCanonicalPath());
			
//			if(validateResultHeader==null)
			reportModal.setCreatedBy(user);
			
			ArrayList<String> remarks=new ArrayList<String>();
			String remarksStr=Constants.STR_EMPTY;
			
			if (validateResultHeader == null&&validateResultTrailer==null){
				log.debug("dept : " + reportModal.getDept());
				
				ITemplateMessageDao templateMsgDao = (ITemplateMessageDao) SgwCoreHibernateUtil.getBean("templateMessageDao");
				log.debug("((BatchSetup)aSetup).getMessageTemplate(): "+((BatchSetup)aSetup).getMessageTemplate());
				MsgTemplate theTemplate=templateMsgDao.getById(Integer.valueOf(CommonUtils.NullChecker(((BatchSetup)aSetup).getMessageTemplate(), Integer.class).toString()));
				log.debug("theTemplate: "+theTemplate);
				
				ArrayList<Integer> errorLineNoList=new ArrayList<Integer>();
				ArrayList<Integer> emptyLineNoList=new ArrayList<Integer>();
				lineTemplate.setDuplicateRecord(new ArrayList<String>());
				lineTemplate.setDuplicateRecordControl(new ArrayList[]{new ArrayList<String>(), new ArrayList<String>()});
				
				line = (String) br.readLine();
				while (line != null&&!line.trim().equals(Constants.FTP_FILE_EOF)) {
					fileLineNo++;
					if(line.trim().equals(Constants.STR_EMPTY)){
						emptyLineNo++;
						emptyLineNoList.add(fileLineNo);
						line = (String) br.readLine();
					}else{
						if(aSetup.getClass().equals(BulkSmsSetup.class)&&
								CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotification(), String.class).toString().length()>0&&
								CommonUtils.NullChecker(theCfg.getBulkSmsThreshold(), Integer.class).toString().length()>0&&
								(successCt+failedCt)>=Integer.valueOf(theCfg.getBulkSmsThreshold())){// check for bulk sms threshold limitation
								line=Constants.FTP_FILE_EOF;
								fileLineNo-=1;
						}else{
							try {
								try {
									line = line.trim();
									log.debug("line other : " + line);
									
									String returnCode = lineTemplate.process(line, reportModal, user, aSetup, theTemplate, theCfg, theTelco);
									if (returnCode == null){
										successCt++;
									}else{
										failedCt++;
										errorLineNoList.add(fileLineNo);
										
										// Insert as invalid record
										SmsInvalid invalidRecord=new SmsInvalid();
										invalidRecord.setCreatedBy(reportModal.getCreatedBy());
										invalidRecord.setCreatedDatetime(new Date());
										invalidRecord.setFtp(reportModal.getId());
										invalidRecord.setInvalidRecord(line);
										invalidRecord.setRemarks(returnCode);
										
										if(aSetup.getClass().equals(BulkSmsSetup.class)){
											invalidRecord.setType(Constants.FTP_REPORT_TYPE_2);
										}else if(aSetup.getClass().equals(FtpSetup.class)){
											invalidRecord.setType(Constants.FTP_REPORT_TYPE_1);
										}
										
										SmsInvalidDao invalidDao = (SmsInvalidDao) SgwCoreHibernateUtil.getBean("smsInvalidDao");
										invalidDao.insert(invalidRecord);
									}
								} catch (Exception e) {
									LogManager.getLog().error(e.getMessage(), e);
									continue;
								}
								line = (String) br.readLine();
							} catch (Exception e) {
								LogManager.getLog().error(e.getMessage(), e);
								continue;
							} catch (Throwable e) {
								LogManager.getLog().error(e.getMessage(), e);
								continue;
							}
						}
					}
				}
				
				if(line==null){
					remarks.add("No proper file EOF found! "+Constants.SPECIAL_CHAR_NEWLINE);
					status=false;
				}else if(line.trim().equals(Constants.FTP_FILE_EOF)){
					remarks.add("File line(s) processed: "+fileLineNo+". "+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
				}else{
					remarks.add("Invalid file EOF found! "+Constants.SPECIAL_CHAR_NEWLINE);
					status=false;
				}
				
				if(errorLineNoList.size()>0){
					String errorRemarks="Error(s) in file(s) line: ";
					for(int i=0;i<errorLineNoList.size();i++){
						int errorLine=(errorLineNoList.get(i));
						if(i!=0){
							errorRemarks+=Constants.SPECIAL_CHAR_COMMA+Constants.STR_SPACE;
						}
						errorRemarks+=errorLine;
					}
					errorRemarks+=". "+Constants.SPECIAL_CHAR_NEWLINE;
					
					errorRemarks+="Error(s) Summary: "+Constants.SPECIAL_CHAR_NEWLINE;
					for(int i=0;i<lineTemplate.getRemarks().size();i++){
						errorRemarks+=(i+1)+") ";
						errorRemarks+=lineTemplate.getRemarks().get(i)+Constants.SPECIAL_CHAR_NEWLINE;
					}
					remarks.add(errorRemarks+Constants.SPECIAL_CHAR_NEWLINE);
				}
				
				if(emptyLineNo>0&&errorLineNoList.size()>0){
					String emptyRemarks=emptyLineNo+" file line(s) empty at line: ";
					for(int i=0;i<emptyLineNoList.size();i++){
						if(i!=0){
							emptyRemarks+=Constants.SPECIAL_CHAR_COMMA+Constants.STR_SPACE;
						}
						emptyRemarks+=emptyLineNoList.get(i);
					}
					remarks.add(emptyRemarks+". "+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
				}
				
				String postActionStatus = lineTemplate.postTemplateProcess();
				if( postActionStatus != null ){
					remarks.add(postActionStatus + ". " + Constants.SPECIAL_CHAR_NEWLINE + Constants.SPECIAL_CHAR_NEWLINE);
					status = false;
				}
				
			}else{
				status=false;
				
				if(validateResultHeader!=null){
					remarks.add(validateResultHeader+"."+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
				}
				if(validateResultTrailer!=null){
					remarks.add(validateResultTrailer+"."+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
				}	
			}
						
			// Update bulksetup type- FtpSetup or BulkSMSSetup
			log.debug("aSetup.getClass().equals(BulkSmsSetup.class): "+aSetup.getClass().equals(BulkSmsSetup.class));
			if(aSetup.getClass().equals(BulkSmsSetup.class)){
				BulkSmsSetup theSetup=(BulkSmsSetup)aSetup;
				BulkSMSSetupDao setupDao = (BulkSMSSetupDao) SgwCoreHibernateUtil.getBean("bulkSMSSetupDao");
				setupDao.updateStatus(theSetup, Constants.STATUS_CLOSED);
				reportModal.setType(Constants.FTP_REPORT_TYPE_2);
			}else if(aSetup.getClass().equals(FtpSetup.class)){
				reportModal.setType(Constants.FTP_REPORT_TYPE_1);
			}
			
			// Check quota
			if(theCfg.getSmsQuota().equals(Constants.STATUS_YES) && reportModal.getDeptId() != 0){// check quota
				ISmsQuotaDao smsQuotaDao = (ISmsQuotaDao) SgwCoreHibernateUtil.getBean("smsQuotaDao");
				List quotaList=smsQuotaDao.findSmsQuota("from SmsQuota where ref=?", new Object[]{reportModal.getDeptId()});
				log.debug("quotaList:"+quotaList);
				if(quotaList.size()==1){
						ISmsQueueDao smsDao = (ISmsQueueDao) SgwCoreHibernateUtil.getBean("smsQueueDao");
						List smsList=smsDao.findAllMessage("select id from SmsQueue where ftp=?", new Object[]{reportModal.getId()});
						log.debug("smsList:"+smsList);
						SmsQuota theQuota=(SmsQuota)quotaList.get(0);
						log.debug("theQuota.getQuota():"+theQuota.getQuota());
						if(theQuota.getQuota()>=smsList.size()){
						}else{
							remarks.add(Constants.ACTION_PARA_REASON_QUOTA_EXCEED+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
						}
				}else{// error sms quota
					remarks.add("SMS Quota Info Incorrect!"+Constants.SPECIAL_CHAR_NEWLINE+Constants.SPECIAL_CHAR_NEWLINE);
				}
			}			
			// Populate remarks
			for(int i=0;i<remarks.size();i++){
				remarksStr+=remarks.get(i);
			}
			log.error(remarksStr);
			reportModal.setRemark(remarksStr);
			reportModal.setNoSuccess(successCt);
			reportModal.setNoFailed(failedCt);
			String fileName = file.getName().replace(FileUtil.IN_PROCESS, Constants.STR_EMPTY);
			//reportModal.setFileName(fileName.substring(0, fileName.length()-4));
			reportModal.setFileName(fileName);
			reportModal.setFilePath(file.getCanonicalPath());
			reportModal.setEndDate(new Date());
			reportModal.setEndTime(new Date());
		} catch (FileNotFoundException e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} catch (IOException e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} catch (Throwable e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					LogManager.getLog().error(e.getMessage(), e);
				} catch (Throwable e) {
					LogManager.getLog().error(e.getMessage(), e);
				}
		}
		
		// Send Email
		String remarks=Constants.STR_EMPTY;
		log.debug("((BatchSetup)aSetup).getNotificationSetupStatus(): "+((BatchSetup)aSetup).getNotificationSetupStatus());
		if(CommonUtils.NullChecker(((BatchSetup)aSetup).getNotificationSetupStatus(), String.class).toString().equals(Constants.STATUS_YES)){
			log.debug("status: "+status);
			if(status){
				remarks=lineTemplate.sendSuccessEmailNotification(aSetup, reportModal, ((BatchSetup)aSetup).getNotificationSetupSender(), ((BatchSetup)aSetup).getNotificationSetupReceiver(), fileLineNo-2-emptyLineNo, failedCt);
			}else{
				remarks=lineTemplate.sendFailureEmailNotification(aSetup, reportModal, ((BatchSetup)aSetup).getNotificationSetupSender(), ((BatchSetup)aSetup).getNotificationSetupReceiver(), fileLineNo-2-emptyLineNo, failedCt);
				lineTemplate.sendFailureSmsNotification(aSetup, reportModal, null, null, fileLineNo-2-emptyLineNo, failedCt);
			}
			log.debug("remarks: "+remarks);
		}
		log.debug("count: "+successCt+failedCt);
		if(aSetup.getClass().equals(BulkSmsSetup.class)&&CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotification(), String.class).toString().length()>0){
			if(CommonUtils.NullChecker(theCfg.getBulkSmsThreshold(), Integer.class).toString().length()>0&&
				(successCt+failedCt)>=Integer.valueOf(theCfg.getBulkSmsThreshold())){// check for bulk sms threshold limitation
				String tempRemarks=Constants.STR_EMPTY;
				if(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotification(), String.class).toString().contains("E")){
					String[] toList=CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
					for(int i=0;i<toList.length;i++){
						tempRemarks+=lineTemplate.sendLimitationEmailNotification(aSetup, reportModal, theCfg.getBulkSmsThresholdNotificationEmailSender(), toList[i], successCt+failedCt, failedCt);
						remarks+=(tempRemarks.length()==0?Constants.STR_EMPTY:remarks.length()==0?tempRemarks:Constants.SPECIAL_CHAR_NEWLINE+tempRemarks);
					}
				}
				if(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotification(), String.class).toString().contains("S")){
					String[] toList=CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
					for(int i=0;i<toList.length;i++){
						tempRemarks+=lineTemplate.sendLimitationSmsNotification(aSetup, reportModal, theCfg, theCfg.getBulkSmsThresholdNotificationSmsSender(), toList[i], successCt+failedCt, failedCt);
						remarks+=(tempRemarks.length()==0?Constants.STR_EMPTY:remarks.length()==0?tempRemarks:Constants.SPECIAL_CHAR_NEWLINE+tempRemarks);
					}
				}
			}
		}else{// is ftp setup, no need limit notification
		}
		
		if(StringUtils.isNotEmpty(remarks)){
			reportModal.setRemark(reportModal.getRemark()+remarks+".");
			
		}

		try{
			log.debug("Update ftp report");
			ftpReportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
			ftpReportDao.update(reportModal);
		}catch(Exception ex){
			ex.printStackTrace();
			LogManager.getLog().error("Failure in update FtpReport"+ex.getMessage(), ex);
		}
		return status;
	}
	
	public static boolean process(FileObj file, ILineTemplate lineTemplate, Object aSetup, FtpReport reportModal, SmsCfg theCfg) {
		BufferedReader br = null;
		String user =null;
		
		log.debug("[process] Start process: " + file.getTheFile().getName());
		boolean status=true;
		
		try {
			ArrayList<String> remarks=new ArrayList<String>();
			String remarksStr=Constants.STR_EMPTY;
			
			reportModal.setStartDate(new Date());
			reportModal.setStartTime(new Date());
			
			ftpReportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
			reportModal.setFtpSetup(((BatchSetup)aSetup).getId());
			ftpReportDao.insert(reportModal);
			
			if(CommonUtils.NullChecker(((FtpSetup)aSetup).getFtpOwner(), String.class).toString().equals(Constants.VALUE_0)){
				user=((FtpSetup)aSetup).getCreatedBy();
			}else{
				user=((FtpSetup)aSetup).getFtpOwner();
			}
			reportModal.setCreatedBy(user);
			
			IUserDao userDao;
			
			try {
				userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
				User usermodal = userDao.findByUserId(user);
				if (usermodal == null){
					reportModal.setRemark("Invalid User: "+user);
				}else{
					GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
					Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
					if (groupmodal != null){
						reportModal.setDeptId(groupmodal.getId());
						reportModal.setDept(groupmodal.getDeptName());
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				reportModal.setRemark("No user exist: "+user);
			}
			
			String returnCode=lineTemplate.process(file, (FtpSetup)aSetup, reportModal, theCfg);
			if(returnCode==null){
				reportModal.setNoSuccess(1);
				reportModal.setNoFailed(0);
			}else{
				reportModal.setNoSuccess(0);
				reportModal.setNoFailed(1);
				status=false;
			}
			
			// Update bulksetup type- FtpSetup
			if(aSetup.getClass().equals(FtpSetup.class)){
				reportModal.setType(Constants.FTP_REPORT_TYPE_1);
			}
			
			remarks=lineTemplate.getRemarks();
			for(int i=0;i<remarks.size();i++){
				remarksStr+=remarks.get(i)+Constants.SPECIAL_CHAR_NEWLINE;
			}
			
			reportModal.setRemark(remarksStr);
			String fileName = file.getTheFile().getName().replace(FileUtil.IN_PROCESS, Constants.STR_EMPTY);
			reportModal.setFileName(fileName);
			reportModal.setFilePath(file.getTheFile().getCanonicalPath());
			reportModal.setEndDate(new Date());
			reportModal.setEndTime(new Date());
		} catch (FileNotFoundException e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} catch (IOException e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} catch (Throwable e) {
			LogManager.getLog().error(e.getMessage(), e);
			status=false;
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					LogManager.getLog().error(e.getMessage(), e);
				} catch (Throwable e) {
					LogManager.getLog().error(e.getMessage(), e);
				}
		}
		
		String emailRemarks=null;
		log.debug("((BatchSetup)aSetup).getNotificationSetupStatus(): "+((BatchSetup)aSetup).getNotificationSetupStatus());
		if(CommonUtils.NullChecker(((BatchSetup)aSetup).getNotificationSetupStatus(), String.class).toString().equals(Constants.STATUS_YES)){
			log.debug("status: "+status);
			if(status){
				emailRemarks=lineTemplate.sendSuccessEmailNotification(aSetup, reportModal, ((BatchSetup)aSetup).getNotificationSetupSender(), ((BatchSetup)aSetup).getNotificationSetupReceiver(), 0, 0);
			}else{
				emailRemarks=lineTemplate.sendFailureEmailNotification(aSetup, reportModal, ((BatchSetup)aSetup).getNotificationSetupSender(), ((BatchSetup)aSetup).getNotificationSetupReceiver(), 0, 0);
			}
			log.debug("emailRemarks: "+emailRemarks);
		}
		if(emailRemarks!=null){
			reportModal.setRemark((reportModal.getRemark()==null?Constants.STR_EMPTY:reportModal.getRemark())+emailRemarks+".");
		}
		
		try{
			log.debug("Update ftp report");
			ftpReportDao = (IFtpReportDao) SgwCoreHibernateUtil.getBean("ftpReportDao");
			ftpReportDao.update(reportModal);
		}catch(Exception ex){
			ex.printStackTrace();
			LogManager.getLog().error("Failure in update FtpReport"+ex.getMessage(), ex);
		}
		return status;
	}
	
	public static boolean delete(String filename) {
		LogManager.getLog().debug("delete file :" + filename);
		File file = new File(filename);
		if (!file.exists())
			return false;
		return file.delete();
	}
	
	public static boolean copy(String source, String target) {
		log.debug("Copying file from " + source + " to " + target);
		File tdir = new File(target).getParentFile();
		if (!tdir.exists()) {
			tdir.mkdirs();
		}
		try {
			// Create channel on the source
			FileChannel srcChannel = new FileInputStream(source).getChannel();
			
			// Create channel on the destination
			FileChannel dstChannel = new FileOutputStream(target).getChannel();
			
			// Copy file contents from source to destination
			dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
			
			// Close the channels
			srcChannel.close();
			dstChannel.close();
			log.debug("DONE Copying file from " + source + " to " + target);
			return true;
		} catch (IOException e) {
			// FileUtil.logStackTrace(log, e);
			return false;
		}
	}
	
	public static boolean isExist(String filename) {
		File file = new File(filename);
		return file.exists();
	}
	
	public static boolean rename(String originalFile, String targetFile) {
		File file = new File(originalFile);
		return file.renameTo(new File(targetFile));
	}
	
	private static String validateFileHeader(String fileDate, String user, FtpReport reportModal, Object aSetup) {
		int reverseDay = 0;
		if(aSetup.getClass().equals(FtpSetup.class)){
			reverseDay = ((FtpSetup)aSetup).getReverseDay();
		}
		
		ArrayList<String> remarks=new ArrayList<String>();
		
		
		log.debug("ValidateFileHeader");
		if (fileDate == null||fileDate.trim().length()==0){
			remarks.add("No Date");
		}else{
			Date ftpDate=null;
			// Get today's date
			
			fileDate=fileDate.trim();
			SimpleDateFormat formatter = null;
			if(fileDate.length()!=Constants.DATEFORMAT_DDMMYY.length()&&fileDate.length()!=Constants.DATEFORMAT_DDMMYYYY.length()){
				remarks.add("File Date: "+fileDate+" format not correct. Correct format is "+Constants.DATEFORMAT_DDMMYY+" or "+Constants.DATEFORMAT_DDMMYYYY);
			}else{
				if(fileDate.length()==Constants.DATEFORMAT_DDMMYY.length()){
					formatter = new SimpleDateFormat(Constants.DATEFORMAT_DDMMYY);
				}else if(fileDate.length()==Constants.DATEFORMAT_DDMMYYYY.length()){
					formatter = new SimpleDateFormat(Constants.DATEFORMAT_DDMMYYYY);
				}
				
				try {
					Calendar expectedFileDate = Calendar.getInstance();
					expectedFileDate.add(Calendar.DAY_OF_MONTH, -reverseDay);
					ftpDate= formatter.parse(fileDate);
					
					if (ftpDate.compareTo(expectedFileDate.getTime()) > 0){
						remarks.add("Future Date: "+fileDate);
					}else{
						long diff = ftpDate.getTime() - expectedFileDate.getTime().getTime();
						
						long diffDay = diff / (1000L*60L*60L*24L);
						
						if (diffDay < 0){
							remarks.add("Back Date: "+fileDate);
						}
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					remarks.add("Error read file date: "+fileDate);
				}
			}
		}
		
		IUserDao userDao;
		
		try {
			userDao = (IUserDao) SgwCoreHibernateUtil.getBean("userDao");
			User usermodal = userDao.findByUserId(user);
			if (usermodal == null){
				remarks.add("Invalid User: "+user);
			}else{
				GroupDao groupDao= (GroupDao) SgwCoreHibernateUtil.getBean("groupDao");
				Department groupmodal = groupDao.findByGroupId(usermodal.getUserGroup());
				if (groupmodal != null){
					reportModal.setDeptId(groupmodal.getId());
					reportModal.setDept(groupmodal.getDeptName());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			remarks.add("No user exist: "+user);
		}
		
		String remarksStr=Constants.STR_EMPTY;
		if(remarks.size()>0){
			for(int i=0;i<remarks.size();i++){
				if(i!=0){
					remarksStr+=Constants.SPECIAL_CHAR_COMMA+Constants.STR_SPACE;
				}
				remarksStr+=remarks.get(i);
			}
			return remarksStr;
		}else{
			return null;
		}
	}
	
	private static String validateFileTrailer(String filename){
		log.debug("Validate File Trailer");
		try{
			log.debug("filename: "+filename);
			ReverseFileReader reader=new ReverseFileReader(filename);
			
			String line=reader.readLine();
			log.debug("line: "+line);
			while(line!=null&&line.trim().equals(Constants.STR_EMPTY)){
				line=reader.readLine();
			}
			log.debug("line: "+line);
			reader.closeReader();
			
			if(line.trim().equals(Constants.FTP_FILE_EOF)){
				log.debug("line: "+line);
				return null;
			}else{
				log.debug("no proper EOF found!");
				return "No proper file EOF found!";
			}
			
		}catch (Exception ex){
			ex.printStackTrace();
			return "Exception occured in validating file trailer!";
		}
	}
}
