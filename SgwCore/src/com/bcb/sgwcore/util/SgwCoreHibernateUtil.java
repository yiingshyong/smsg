package com.bcb.sgwcore.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SgwCoreHibernateUtil {
	
private static ApplicationContext applicationContext=null;
	
	static{
		try{
			getApplicationContext();
		}catch(Exception e){
			throw new RuntimeException("Error while initialising spring bean factory.", e);
		}
	}
	
	private static ApplicationContext getApplicationContext() throws Exception
	{
		if (applicationContext==null){
			applicationContext = new ClassPathXmlApplicationContext("sgw-hibernate.xml");
		}
		
		return applicationContext;
	}
	
	public static Object getBean(String beanName) throws Exception
	{	
		return getApplicationContext().getBean(beanName);
	}

}
