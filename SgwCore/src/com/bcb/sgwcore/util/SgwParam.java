package com.bcb.sgwcore.util;

public class SgwParam {

	public static final Integer AVAILABLE = 1;
	public static final Integer NOT_AVAILABLE = 2;
	public static final String SUCCESS = "S";
	public static final String FAIL = "F";
	public static final String ASCII = "A";
	public static final String UTF = "U";
	
	public static final String APPROVE = "Y";
	public static final String DISAPPROVE = "N";
	public static final String PENDING = "P";
	
	public static final String RETURN_CODE_SUCESSS="01010";
	//public static final String RETURN_CODE_SUCESSS="01012";
	public static final String RETURN_CODE_SGW_PROBLEM = "99999";
	
	public static final String PENDING_TO_SEND = "P";
	public static final String READY_TO_SEND = "R";
	

}
