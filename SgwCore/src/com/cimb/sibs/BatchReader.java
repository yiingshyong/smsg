package com.cimb.sibs;

import java.text.SimpleDateFormat;



public class BatchReader implements Runnable {
	
	boolean isRunning;
	static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	
	BatchReportTool monitorTool;
	public BatchReader() {
		System.out.println("Construct Batch Reader");
	}
	
	public void run() {
		isRunning = true;
		//Thread tt = new Thread();
		System.out.println("Running........ Batch Reader");
		
		while (isRunning) {
			monitorTool = new BatchReportTool();
			monitorTool.startMonitor();
			sleepAWhile();
		}
	}

	private void sleepAWhile() {

		try {
			Thread.yield();
			Thread.sleep(60000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void shutdown() {
		isRunning = false;
		
	//	shutdownConversionThread();
		
	}
	
	
	
}
