package com.cimb.sibs;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.Format;

public class BatchReportTool {
	//static Logger logger = Logger.getLogger(BatchReportTool.class.getName());
	//static final String REOSURCE_FILE= "netfire";
	Format formatter_full;
	Format formatter_day;
	

	public void startMonitor () 
	{
		System.out.println("System Start.......");
		//program start here.
		
	}
	
	private Connection getConnection() throws SQLException, ClassNotFoundException{
			
		Class.forName(PropertyUtil.getKey("JdbcClassName"));
		String url =
			PropertyUtil.getKey("JdbcURL");
		
		Connection con;
		
		con = DriverManager.getConnection(
                url,PropertyUtil.getKey("DBUSERNAME"), PropertyUtil.getKey("DBPASSWORD"));
		
		return con;
	}

}
