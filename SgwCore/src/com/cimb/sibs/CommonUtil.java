package com.cimb.sibs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CommonUtil{
	
	private static Log log = LogManager.getLog();
	private static String STR_EMPTY = "";
	private static String userName = PropertyUtil.getKey("DBUSERNAME");
	private static String password = PropertyUtil.getKey("DBPASSWORD");
	private static String url = PropertyUtil.getKey("JdbcURL");
	private static String classForName = PropertyUtil.getKey("JdbcClassName");
	public static final String SMS_STATUS_1 = "S";
	public static final String SMS_STATUS_2 = "U";
	public static final String SMS_STATUS_3 = "A";
	public static final String SMS_STATUS_4 = "Q";
	public static final String SMS_STATUS_5 = "R";
	public static final String SMS_STATUS_6 = "T";
	public static final String SMS_STATUS_7 = "V";
	private Connection conn;
	private TblRef defaultSmsType;
	private int charPerSms;
	public enum TCPStatus{
		
		INVALID_IP("Invalid IP", 3),
		INVALID_MOBILE_NO("Invalid Mobile Number", 5),
		INVALID_USER_ID("Invalid User Id", 11),
		INVALID_REQUEST_MODE("Invalid Request Mode", 10),
		SUCCESS("Success", -1),
		APP_ERR("Application Error", 0);
		
		private String desc;
		private int code;
		private TCPStatus(String desc, int code) {
			this.desc = desc;
			this.code = code;
		}
		public String getDesc() {
			return desc;
		}
		public int getCode() {
			return code;
		}			
	}

	public static int findTcpStatusCode(String desc){
		for(TCPStatus s : TCPStatus.values()){
			if(s.getDesc().equals(desc)){
				return s.getCode();
			}
		}
		return 0;
	}
	
	public CommonUtil() {
		this.conn = createDBConnection();
	}
  
	private Connection createDBConnection(){
		try{
			log.info("Creating db connection url: " + url);
			Class.forName(classForName).newInstance();
			return (Connection)DriverManager.getConnection(url, userName, password);	      
		}catch(Exception e){
			throw new RuntimeException("Error creating database connection. Reason: " + e.getMessage(), e);
		}	  
	}

	public long processRequest(String mobileNo, String mode, String priority, String userId, String tarMsg, String ipAddress, String retRefNum, String msgRefNum)throws SgwException{
		try{
			SmsQueue smsQueue = new SmsQueue();
			String errorDesc = checkSMSValidation(ipAddress, mobileNo, tarMsg, mode, "", userId, smsQueue);
			if(StringUtils.isNotEmpty(errorDesc)){
				throw new SgwException(errorDesc);
			}		    
      
			smsQueue.setCreatedBy(userId);
			smsQueue.setCreatedDatetime(new Date());
			smsQueue.setSmsStatusDatetime(smsQueue.getCreatedDatetime());
			smsQueue.setMessage(tarMsg);
			smsQueue.setMobileNo(mobileNo);

			if (mode.toLowerCase().equals("text"))
				smsQueue.setMsgFormatType("ASCII/Text");
			else if ((mode.toLowerCase().equals("utf")) || (mode.toLowerCase().equals("unicode"))) {
				smsQueue.setMsgFormatType("UTF-8");
			}

			smsQueue.setPriority(Integer.valueOf(1));
			smsQueue.setRemarks(STR_EMPTY);
			smsQueue.setSentBy(userId);
			smsQueue.setTelco(Integer.valueOf(0));
			smsQueue.setVersion(1);
			smsQueue.setType("T");
			smsQueue.setSmsStatus("Q");
			smsQueue.setApprovalBy("automated");
			smsQueue.setApprovalDatetime(new Date());
			smsQueue.setRetRefNum(retRefNum);
			smsQueue.setMsgRefNum(msgRefNum);
			return insertSMSQueue(smsQueue);			
		}catch(SgwException ve){
			throw ve;
		}catch(Exception e){
			log.error("Exception caught: " + e.getMessage(), e);
			throw new SgwException(TCPStatus.APP_ERR.getDesc());
		}
  } 
  
	private String checkSMSValidation(String ipAddress, String mobileNo, String msg, String requestMode, String param_scheduled_time, String userId, SmsQueue smsQueue) throws Exception{
	  
		if (validateMobileNo(mobileNo)!= null) {
			return TCPStatus.INVALID_MOBILE_NO.getDesc();
		}
		if (validateIp(ipAddress) != null) { 
			return TCPStatus.INVALID_IP.getDesc();
		}
		if (validateRequestMode(requestMode.toLowerCase()) != null) {
			return TCPStatus.INVALID_REQUEST_MODE.getDesc();
		}
		if(validateUser(smsQueue, userId) != null){
			return TCPStatus.INVALID_USER_ID.getDesc();
		}
		
		return null;
  }

  	private String validateMobileNo(String mobileNo) throws Exception{
  		if ((mobileNo == null) || (mobileNo.trim().length() == 0)) {
  			return TCPStatus.INVALID_MOBILE_NO.getDesc();
  		}
  		mobileNo = BusinessService.normalizeMobile(mobileNo);
  		return StringUtils.isNumeric(mobileNo)? null : TCPStatus.INVALID_MOBILE_NO.getDesc();    
  	}


  	private static String validateRequestMode(String requestMode){
  		if(StringUtils.isEmpty(requestMode)){
  			return TCPStatus.INVALID_REQUEST_MODE.getDesc(); 
  		}
  		if ((requestMode.equals("utf")) || (requestMode.equals("text")) || (requestMode.equals("unicode"))) {
  			return null;
  		}
  		return TCPStatus.INVALID_REQUEST_MODE.getDesc();
  	}

  	private String validateIp(String ipAddress)throws Exception {
  		String value = null;
  		if (conn != null) {
  			StringBuffer sql = new StringBuffer("Select * from http_ip_address where ip = ?");
  			PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString());
  			prest.setString(1, ipAddress);
  			ResultSet rs = (ResultSet)prest.executeQuery();
  			
  			int count = 0;
  			if (rs.next())
  			{
  				count++;
  			}
  			rs.close();
  			prest.close();

  			if (count == 0) {
  				return TCPStatus.INVALID_IP.getDesc();
  			}
  		}
  		return value;
  	}

  	private String validateUser(SmsQueue sms, String user) throws Exception{
  		if ( StringUtils.isEmpty(user)){
  			return TCPStatus.INVALID_USER_ID.getDesc();
  		}
  		StringBuffer sql = new StringBuffer("Select d.DEPT_ID, d.dept_name from user u, department d where u.user_id = ? and u.user_group = d.DEPT_ID and d.DEPT_STATUS=?");
  		PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString());
  		prest.setString(1, user);
  		prest.setString(2, "Active");
  		ResultSet rs = (ResultSet)prest.executeQuery();
	
  		int count = 0;
  		if (rs.next())
  		{
  			sms.setDeptId(rs.getInt(1));
  			sms.setDept(rs.getString(2));
  			count++;
  		}
	
  		rs.close();
  		prest.close();
	
  		if (count == 0) {
  			return TCPStatus.INVALID_USER_ID.getDesc();
  		}

  		return null;
  }

  	private long insertSMSQueue(SmsQueue smsqueue) throws Exception{
  		long id = 0;	  
  		smsqueue.setSmsType(selectSMSType(smsqueue.getCreatedBy(), smsqueue.getChannelId()));
  		smsqueue.setTotalSms(CommonUtils.strToArray(smsqueue.getMessage(), getCharPerSms()).size());
	  
  		if(smsqueue.getSmsType().isOnline()){
  			smsqueue.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(smsqueue.getCreatedBy(), smsqueue.getChannelId()));
  			return BusinessService.processSMS(smsqueue);		
  		}
	  
  		StringBuffer sql = new StringBuffer("INSERT INTO sms_queue ");

  		sql.append("(created_by, created_datetime, sms_status_datetime, message, mobile_no, msg_format_type, priority, sent_by, telco, version, type, sms_status, approval_by, approval_datetime, dept, dept_id, total_sms, sms_type)");
  		sql.append(" values(?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP(), ?, ?, ?, ?)");
  		try {
  			PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
  			prest.setString(1, smsqueue.getCreatedBy());
  			prest.setString(2, smsqueue.getMessage());
  			prest.setString(3, smsqueue.getMobileNo());
  			prest.setString(4, smsqueue.getMsgFormatType());
  			prest.setInt(5, smsqueue.getPriority().intValue());
  			prest.setString(6, smsqueue.getCreatedBy());
  			prest.setInt(7, smsqueue.getTelco().intValue());
  			prest.setInt(8, smsqueue.getVersion());
  			prest.setString(9, smsqueue.getType());
  			prest.setString(10, smsqueue.getSmsStatus());
  			prest.setString(11, smsqueue.getApprovalBy());
  			prest.setString(12, smsqueue.getDept());
  			prest.setInt(13, smsqueue.getDeptId());
  			prest.setInt(14, smsqueue.getTotalSms());
  			prest.setInt(15, smsqueue.getSmsType().getTblRefId());//TODO: channel id will be null for now until change of socket message format to include channel id parameter from client
  			log.debug("Inserting into db");
  			int update = prest.executeUpdate();
  			ResultSet rs = prest.getGeneratedKeys();
  			if(rs.next()){
  				id = rs.getLong(1);
  			}
  			prest.close();
  			return id;
  		}catch (SQLException e) {
  			log.error("Failed to insert SMS. Reason: " + e.getMessage(), e);
  			throw e;
  		}
  	}
  
	private int getCharPerSms() throws Exception{
		if(this.charPerSms == 0){
			  PreparedStatement stmt = conn.prepareStatement("select char_per_sms, concatenate_sms,concatenate_sms_no from sms_cfg where version = ?");
			  stmt.setInt(1, Integer.parseInt(Constants.APPSVERSION));
			  ResultSet rs = stmt.executeQuery();
			  if(rs.next()){
				  this.charPerSms = CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, rs.getString(1), rs.getString(2), rs.getString(3));
			  }			
		}
		return this.charPerSms;
	}
	
	private TblRef getDefaultSmsType() throws Exception{
		if(defaultSmsType == null){
			defaultSmsType = new TblRef();
			PreparedStatement stmt = conn.prepareStatement("select a.tbl_ref_id, a.online from tbl_ref a, sms_cfg b where a.tbl_ref_id = b.default_sms_type and b.version = ?");
			stmt.setInt(1, Integer.parseInt(Constants.APPSVERSION));
			ResultSet rs = stmt.executeQuery();
			if(rs.next()){
				defaultSmsType.setTblRefId(rs.getInt(1));
				defaultSmsType.setOnline(rs.getBoolean(2));
			}
			rs.close();
			stmt.close();		  
		}
		return this.defaultSmsType;
	}
  
  private TblRef selectSMSType(String userId, String channelId) throws Exception{
	  PreparedStatement stmt;
	  //if has channel id
	  if(StringUtils.isNotEmpty(channelId)){
		  stmt = conn.prepareStatement("select a.sms_type, b.online from user_channel a, tbl_ref b where a.user_id = ? and a.channel_id = ? and a.sms_type=b.tbl_ref_id and a.status='Active' ");
		  stmt.setString(1, userId);
		  stmt.setString(2, channelId);
	  }else{//check if it's only one channel for user
		  stmt = conn.prepareStatement("select a.sms_type, b.online from user_channel a, tbl_ref b where user_id = ? and a.sms_type=b.tbl_ref_id and a.status='Active' ");
		  stmt.setString(1, userId);
	  }
	  ResultSet rs = (ResultSet)stmt.executeQuery();
	  TblRef smsType = new TblRef();
	  if(rs.next()){
		  smsType.setTblRefId(rs.getInt(1));
		  smsType.setOnline(rs.getBoolean(2));
	  }
	  //if record size not 1, lookup default sms type
	  if(smsType.getTblRefId() == 0 || rs.next()){
		  stmt.close();
		  rs.close();
		  smsType = getDefaultSmsType();
	  }
	  if(smsType.getTblRefId() == 0){
		  throw new RuntimeException("No Default Sms Type was set. Please configure in system setting.");
	  }
	  return smsType;
  }
  
  public void closeConnection() throws Exception{
	  this.conn.close();
  }
}