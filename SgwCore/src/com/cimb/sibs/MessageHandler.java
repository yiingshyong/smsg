package com.cimb.sibs;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import com.bcb.sgwcore.SgwException;
import com.cimb.sibs.CommonUtil.TCPStatus;

public class MessageHandler extends Thread{
	private Socket connectionSocket;
	private CommonUtil commonUtil;
	private static Log log = LogManager.getLog();
	private int portnum;
	private int msgCount;
	private DataOutputStream output;

	public MessageHandler(Socket c) throws IOException{
		this.connectionSocket = c;
		this.portnum = this.connectionSocket.getPort();
		int timeout = Integer.parseInt(PropertyUtil.getKey("TIMEOUT_IN_MINUTE"));
		c.setSoTimeout(60000 * timeout);
		c.setKeepAlive(true);
	}

	public void run() {
		long smsId = 0;
		int errorCode = 0;
		String msgId;
		try{
			log.info("New client connection from " + this.connectionSocket.getInetAddress().getHostAddress() + " on port " + this.connectionSocket.getPort());
			StringBuffer request = new StringBuffer();
			BufferedInputStream bis = new BufferedInputStream(this.connectionSocket.getInputStream());
			InputStreamReader isr = new InputStreamReader(bis, "US-ASCII");

			if (PropertyUtil.getKey("REPLY").equals("Y")) {
				output = new DataOutputStream(this.connectionSocket.getOutputStream());			
			}
			
			msgCount = 1;
			msgId = String.valueOf(this.portnum) + ":" + msgCount;
			long startTime = 0;
			int j = 0;
			int messageLenght = Integer.parseInt(PropertyUtil.getKey("sibs.message.length"));			
			log.debug("Waiting for incoming msg " + msgId);
			while (true) { 
				int i = isr.read();
				if (i == -1) {
					log.info("EOF detected, connection will be closed.");
					break;
				}
 
				if (j < 0) { 
				}else {
					request.append((char)i);
				}
				j++;
				if (j == messageLenght) { 
					String retRefNum = "";
					String msgRefNum = "";					
					String tarNum = "";
					String tarMode = "";
					String tarmPriority = "";
					String userId = "";
					String tarMsg = "";
					String socket = "";
					try{
						startTime = System.currentTimeMillis();        		
						socket = request.substring(0,9);  
						tarNum = request.substring(9, 29);
						tarMode = request.substring(29, 36);
						tarmPriority = request.substring(36, 37);
						userId = request.substring(37, 47);
						retRefNum = request.substring(47,69);
						msgRefNum = request.substring(69,89);
						tarMsg = request.substring(89);
				  
						if(commonUtil == null){
							commonUtil = new CommonUtil();
						}  
						try{
							smsId = commonUtil.processRequest(removeSpaces(tarNum.trim()), removeSpaces(tarMode), removeSpaces(tarmPriority), removeSpaces(userId), tarMsg.trim(), this.connectionSocket.getInetAddress().getHostAddress(), removeSpaces(retRefNum), removeSpaces(msgRefNum));
							errorCode = TCPStatus.SUCCESS.getCode();
						}catch(SgwException se){
							errorCode = CommonUtil.findTcpStatusCode(se.getMessage());
							log.error("Msg: " + msgId + " : Caught Defined Error: " + errorCode + "-" + se.getMessage());
						}				
							      
					}catch(Exception e){
						log.error("Unexpected Exception Caught when processing request " + msgId + " Reason: "+ e.getMessage(), e);
						errorCode = TCPStatus.APP_ERR.getCode();
					}finally{
						String responseMsg = "";
						if (PropertyUtil.getKey("REPLY").equals("Y")) { 
							responseMsg = retRefNum + msgRefNum + StringUtils.rightPad(String.valueOf(errorCode), 2, " ");
							responseMsg = StringUtils.leftPad(String.valueOf(responseMsg.length()), 9, "0") + responseMsg;
							output.writeBytes(new String(responseMsg.getBytes(), "US-ASCII"));
							output.flush();
						}
						StringBuffer logStatus = new StringBuffer();
						logStatus.append(msgId).append(" ")
						.append("'").append(request.toString()).append("'").append(" ")
						.append(errorCode).append(" ")
						.append(smsId).append(" ")
						.append(StringUtils.isEmpty(responseMsg)? "-" : "'" + responseMsg).append("'").append(" ")
						.append((System.currentTimeMillis()-startTime));						
						log.info(logStatus.toString());
					}
				
					//reset for next message
					j = 0;
					request = new StringBuffer("");
					msgCount++;
					msgId = String.valueOf(this.portnum) + ":" + msgCount;
					log.debug("Waiting for incoming msg " + msgId);
				} 
			}
		}catch (Exception e) {
			log.error("Unexpected error caught when handling client at port: " + this.portnum + " Reason: "+ e.getMessage(), e);
		}finally{
			if(!connectionSocket.isClosed()){
				try{
					if(commonUtil != null){
						commonUtil.closeConnection();
					}
					connectionSocket.getInputStream().close();
					connectionSocket.close();
					log.info("Port " + portnum + " connection closed");
				}catch(Exception e){
					log.error("Error closing socket connection. Reason: " + e.getMessage(), e);
				}
    	}
    }
  }

	private static byte[] encodeInt(int i){
		byte[] b = new byte[4];
	    b[0] = (byte)((i & 0xFF000000) >> 24);
	    b[1] = (byte)((i & 0xFF0000) >> 16);
	    b[2] = (byte)((i & 0xFF00) >> 8);
	    b[3] = (byte)(i & 0xFF);
	    return b;
	}

	private static String removeSpaces(String s) {
		StringTokenizer st = new StringTokenizer(s, " ", false);
		String t = "";
		while (st.hasMoreElements()) t = t + st.nextElement();
		return t;
	}
}
