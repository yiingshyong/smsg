package com.cimb.socket;


import org.apache.commons.logging.Log;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

public class Main
  implements WrapperListener
{
  TcpProcessor mcp = new TcpProcessor();

  Log log = LogManager.getLog();

  public static void main(String[] args)
  {
	  try{
		  WrapperManager.start(new Main(), args);
	  }catch(Exception e){
		  System.err.println("Tcp Listener Error: " + e.getMessage());
		  e.printStackTrace();
	  }
  }

  public void controlEvent(int event)
  {
    if (!WrapperManager.isControlledByNativeWrapper())
    {
      if ((event == 200) || (event == 201) || (event == 203))
      {
        WrapperManager.stop(0);
      }
    }
  }

  public Integer start(String[] param) {
	  try{
	    this.log.debug("main TCP processor start");
	    Class.forName("com.bcb.sgwcore.util.SgwCoreHibernateUtil");// load this class
	    this.mcp.start();
	  }catch(Exception e){
		  log.error("Error tcp engine: " + e);
	  }

    return null;
  }

  public int stop(int exitcode) {
    this.log.debug("main TCP processor start");

    this.mcp.shutdown();
    return 0;
  }
}