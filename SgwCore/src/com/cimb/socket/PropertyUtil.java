package com.cimb.socket;

import java.util.ResourceBundle;

public class PropertyUtil
{

	private static final ResourceBundle rb = ResourceBundle.getBundle("socket");	
	public static String getKey(String KEY)
  {
	  
    try
    {
      return rb.getString(KEY);
    }
    catch (Exception e) {
    	throw new RuntimeException("Error while getting resource using key: '" + KEY + "'", e);
    }
  }
}