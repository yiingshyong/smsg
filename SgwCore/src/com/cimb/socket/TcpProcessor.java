package com.cimb.socket;

import java.io.IOException;
import java.net.ServerSocket;

import org.apache.commons.logging.Log;

public class TcpProcessor extends Thread
{
  Log log = LogManager.getLog();
  ServerSocket welcomeSocket;
  private int connNum =0;
  
  public TcpProcessor()
  {
  }

  public void run()
  {
    this.log.debug("Start TcpProcessor");
    try {
        int port = Integer.parseInt(PropertyUtil.getKey("PORT"));
      this.log.info("Creating socket server instance listening on " + port);
      this.welcomeSocket = new ServerSocket(port);
      while (true)
      {
    	  try{
    		  new MessageHandler(this.welcomeSocket.accept()).start();
    	  }catch(Exception e){
    		  log.error("Error accepting new client. Due to: " + e.getMessage(), e);
    	  }
      }
    }
    catch (IOException e)
    {
    	log.error("Error creating listener. Reason: " + e.getMessage(), e);
    }
  }

  public void shutdown()
  {
    System.out.println("Shutdown");
  }

public int getConnNum() {
	return connNum;
}

public void setConnNum(int connNum) {
	this.connNum = connNum;
}
}