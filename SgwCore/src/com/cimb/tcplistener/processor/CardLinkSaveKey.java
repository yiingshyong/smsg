package com.cimb.tcplistener.processor;

import java.math.BigInteger;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CardLinkSaveKey {

	public static void main(String[] args) {
		System.out.println("Component 1: '" + args[0] + "'");
		System.out.println("Component 2: '" + args[1] + "'");
		System.out.println("Component 3: '" + args[2] + "'");
		BigInteger result = new BigInteger(args[0],16).xor(new BigInteger(args[1], 16)).xor(new BigInteger(args[2], 16));
		String hexString = result.toString(16);
		String finalKey = hexString + hexString.substring(0,16);
		String encryptedKey = SecurityManager.getInstance().getJasyptEnc().encrypt(finalKey);
		SmsCfg cfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		cfg.setClZPK(encryptedKey);
		DaoBeanFactory.getHibernateTemplate().update(cfg);
		System.out.println("ZPK: '" + hexString + "'");
		System.out.println("Final Key: '" + finalKey + "'");
		System.out.println("Key deposited");
	}
}
