package com.cimb.tcplistener.processor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CommonUtil
{
  static Log log = LogManager.getLog();

  private static String STR_EMPTY = "";
  private static String userName = PropertyUtil.getKey("DBUSERNAME");
  private static String password = PropertyUtil.getKey("DBPASSWORD");
  private static String url = PropertyUtil.getKey("JdbcURL");
  private static String classForName = PropertyUtil.getKey("JdbcClassName");
  public static final String SMS_STATUS_1 = "S";
  public static final String SMS_STATUS_2 = "U";
  public static final String SMS_STATUS_3 = "A";
  public static final String SMS_STATUS_4 = "Q";
  public static final String SMS_STATUS_5 = "R";
  public static final String SMS_STATUS_6 = "T";
  public static final String SMS_STATUS_7 = "V";
  private Connection conn;
  private TblRef defaultSmsType;
  private int charPerSms;
  private static final String PIN_ENC_ENCLOSURE = PropertyUtil.getKey("pin.enc.enclosure");
  private static final String PIN_ENC_PADDING = PropertyUtil.getKey("pin.enc.padding");
  private static final Pattern PIN_ENC_PATTERN = Pattern.compile(PIN_ENC_ENCLOSURE + ".*" + PIN_ENC_ENCLOSURE);
  private String ZPK;
  private static final String ENC_MODE_3DES_ECB_NOPADDING = "DESede/ECB/NoPadding";
  private static final String ENC_ALGO = "DESede";
  

  
  public CommonUtil() {
	  this.conn = createDBConnection();
	  setupPin();
  }
  
  private void setupPin(){
	  	SmsCfg cfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();	  	
	  	this.ZPK = SecurityManager.getInstance().getJasyptEnc().decrypt(cfg.getClZPK());
  }
  
//  private Connection getConnection(){
//	  if(this.conn == null || this.conn.isValid(10)){
//		  this.conn = createDBConnection();
//	  }
//	  return this.conn;
//  }
  
  private Connection createDBConnection(){
	  try{
		  log.info("Creating db connection url: " + url + " user: " + userName + " password: " + password);
	      Class.forName(classForName).newInstance();
	      return (Connection)DriverManager.getConnection(url, userName, password);	      
	  }catch(Exception e){
		  throw new RuntimeException("Error creating database connection. Reason: " + e.getMessage(), e);
	  }	  
  }

public long processMsg(String mobileNo, String mode, String priority, String userId, String tarMsg, String ipAddress)throws SgwException
  {
	long startTime = System.currentTimeMillis();
	  try{
    log.debug("in processMsg");
    String errorCode = checkSMSValidation(ipAddress, mobileNo, tarMsg, mode, "");
    if(StringUtils.isNotEmpty(errorCode )){
    	throw new SgwException("SMS Validation error. Error Code: " + errorCode);
    }
    
      SmsQueue sms = new SmsQueue();
      errorCode = validateUser(sms, userId);
      if( StringUtils.isNotEmpty(errorCode)){
      	throw new SgwException("User Validation error. Error Code: " + errorCode);    	  
      }
      
      	
        sms.setCreatedBy(userId);
        sms.setCreatedDatetime(new Date());
        sms.setSmsStatusDatetime(sms.getCreatedDatetime());
        sms.setMessage(decrypt(tarMsg));
        sms.setMobileNo(mobileNo);

        if (mode.toLowerCase().equals("text"))
          sms.setMsgFormatType("ASCII/Text");
        else if ((mode.toLowerCase().equals("utf")) || (mode.toLowerCase().equals("unicode"))) {
          sms.setMsgFormatType("UTF-8");
        }

        sms.setPriority(Integer.valueOf(1));

        sms.setRemarks(STR_EMPTY);
        sms.setSentBy(userId);
        sms.setTelco(Integer.valueOf(0));

        sms.setVersion(1);
        sms.setType("T");

        log.debug("TCP Approval>"+PropertyUtil.getKey("APPROVAL")+"--");
        if (PropertyUtil.getKey("APPROVAL").equals("Y")) {
	        if(getApprovalRight(userId)){
	        	sms.setSmsStatus(Constants.SMS_STATUS_3);
	        	log.debug("TCP SMS STATUS - Need Approval>"+Constants.SMS_STATUS_3+"--");
			}else {
				log.debug("TCP SMS STATUS - No Approval>"+Constants.SMS_STATUS_4+"--");
				sms.setSmsStatus(Constants.SMS_STATUS_4);
				sms.setApprovalBy("automated");
				sms.setApprovalDatetime(new Date());
			}
        }else{
        	log.debug("TCP SMS STATUS>"+Constants.SMS_STATUS_4+"--");
        	sms.setSmsStatus(Constants.SMS_STATUS_4);
			sms.setApprovalBy("automated");
			sms.setApprovalDatetime(new Date());
        }
        
    
        log.debug("Inserting into database");
        long result = insertSMSQueue(sms);
        log.debug("Time taken:" + (System.currentTimeMillis()-startTime));
        return result;
	  }catch(SgwException ve){
		  throw ve;
	  }catch(Exception e){
		  log.error("Exception caught: " + e.getMessage(), e);
		  return 0;
	  }

  }

  private String checkSMSValidation(String ipAddress, String mobileNo, String msg, String requestMode, String param_scheduled_time) throws Exception
  {
	  log.debug("Validating SMS");
    String errorsCode = null;

    errorsCode = validateMobileNo(mobileNo);

    if (errorsCode != null) {
      return errorsCode;
    }
    if (validateIp(ipAddress) != null) {
      return "3";
    }
    if (requestMode == null)
      return "10";
    if (validateRequestMode(requestMode.toLowerCase()) != null) {
      return "10";
    }

    if (msg == null)
      return "4";
    if (msg.equals("")) {
      return "4";
    }

    return errorsCode;
  }

  private String checkMobile(String mobileNo)throws Exception{
    String mobile_num = null;

    String prefix = STR_EMPTY;
    if (mobileNo.startsWith("6")) {
      if (mobileNo.startsWith("65"))
        prefix = "65";
      else
        prefix = mobileNo.substring(0, 4);
    }
    else prefix = mobileNo.substring(0, 3);

    if (conn != null) {
      log.debug("Check prefix: " + prefix);
      try
      {
        StringBuffer sql = new StringBuffer("Select * from telco where telco_prefix like ?");
        java.sql.PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString());
        prest.setString(1, "%" + prefix + "%");

        ResultSet rs = (ResultSet)prest.executeQuery();

        int count = 0;
        if (rs.next())
        {
          count++;
        }

        rs.close();
        prest.close();

        if (count == 0) {
          return "4";
        }

      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }

    }

    return mobile_num;
  }

  private String validateMobileNo(String mobileNo) throws Exception{
    if ((mobileNo == null) || (mobileNo.trim().length() == 0)) {
      return "4";
    }
    mobileNo = BusinessService.normalizeMobile(mobileNo);
//    Pattern pattern = Pattern.compile("^[6][05][189]\\d{7,9}$");
//
//    Matcher l_matcher = pattern.matcher(mobileNo);
//
//    if (!l_matcher.matches()) {
//      return "5-Invalid mobile number -> " + mobileNo;
//    }
//    if (checkMobile(mobileNo) != null) {
//        return "5-Invalid mobile number -> " + mobileNo;
//    }

    return StringUtils.isNumeric(mobileNo)? null : "5-Invalid mobile number -> " + mobileNo;
    
  }

//  private String normalizeMobile(String mobileNo)
//  {
//    mobileNo = mobileNo.trim().replaceAll(" ", "").replaceAll("-", "").replaceAll("[+]", "").replaceAll("[(]", "").replaceAll("[)]", "");
//    if ((NullChecker(mobileNo, String.class).toString().length() > 0) && (mobileNo.startsWith("0")))
//    {
//      mobileNo = "6" + mobileNo;
//    }
//    return mobileNo;
//  }

  private Object NullChecker(Object obj, Class classType)
  {
    if (obj == null) {
      if (classType.equals(String.class))
        return STR_EMPTY;
      if (classType.equals(Integer.class))
        return Integer.valueOf(0);
      if (classType.equals(Double.class))
        return Double.valueOf(0.0D);
      if (classType.equals(Date.class))
        return STR_EMPTY;
      try
      {
        obj = STR_EMPTY;
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      return obj;
    }

    return obj;
  }

  private static String validateRequestMode(String requestMode)
  {
    if ((requestMode.equals("utf")) || (requestMode.equals("text")) || (requestMode.equals("unicode"))) {
      return null;
    }

    return "10-Invalid Request Mode -> " + requestMode;
  }

  private String validateIp(String ipAddress)throws Exception {
    String value = null;

    if (conn != null) {
      log.debug("Validating ip address: " + ipAddress);
        StringBuffer sql = new StringBuffer("Select * from http_ip_address where ip = ?");
        PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString());
        prest.setString(1, ipAddress);

        ResultSet rs = (ResultSet)prest.executeQuery();

        int count = 0;
        if (rs.next())
        {
          count++;
        }

        rs.close();
        prest.close();

        if (count == 0) {
          return "3-Invalid IP Address ->" + ipAddress;
        }

    }

    return value;
  }

  private String validateUser(SmsQueue sms, String user) throws Exception
  {
    log.debug("Validating User: " + user);
    if ( StringUtils.isEmpty(user)){
    	return "11 - Invalid user '" + user + "'";
    }

    StringBuffer sql = new StringBuffer("Select d.DEPT_ID, d.dept_name from user u, department d where u.user_id = ? and u.user_group = d.DEPT_ID and d.DEPT_STATUS=?");
	  PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString());
	  prest.setString(1, user);
	  prest.setString(2, "Active");
	  ResultSet rs = (ResultSet)prest.executeQuery();
	
	  int count = 0;
	  if (rs.next())
	  {
	    sms.setDeptId(rs.getInt(1));
	    sms.setDept(rs.getString(2));
	    count++;
	  }
	
	  rs.close();
	  prest.close();
	
	  if (count == 0) {
	    return "11 - User not found '" + user + "'";
	  }

    return null;
  }

  private long insertSMSQueue(SmsQueue smsqueue) throws Exception
  {
	  long id = 0;
	  
	  smsqueue.setSmsType(selectSMSType(smsqueue.getCreatedBy(), smsqueue.getChannelId()));
	  smsqueue.setTotalSms(CommonUtils.strToArray(smsqueue.getMessage(), getCharPerSms()).size());
	  
	  if(smsqueue.getSmsType().isOnline()){
		  log.info("Online sms ..");
		  smsqueue.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(smsqueue.getCreatedBy(), smsqueue.getChannelId()));
		return BusinessService.processSMS(smsqueue);		
	  }
	  
	  if(smsqueue.getSmsType().isEncrypted()){
		  smsqueue.setMessage(SecurityManager.getInstance().encrypt(smsqueue.getMessage()));
	  }
	  
    StringBuffer sql = new StringBuffer("INSERT INTO sms_queue ");

    sql.append("(created_by, created_datetime, sms_status_datetime, message, mobile_no, msg_format_type, priority, sent_by, telco, version, type, sms_status, approval_by, approval_datetime, dept, dept_id, total_sms, sms_type)");
    sql.append(" values(?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP(), ?, ?, ?, ?)");
    try {
      PreparedStatement prest = (PreparedStatement)conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
      prest.setString(1, smsqueue.getCreatedBy());
      prest.setString(2, smsqueue.getMessage());
      prest.setString(3, smsqueue.getMobileNo());
      prest.setString(4, smsqueue.getMsgFormatType());
      prest.setInt(5, smsqueue.getPriority().intValue());
      prest.setString(6, smsqueue.getCreatedBy());
      prest.setInt(7, smsqueue.getTelco().intValue());
      prest.setInt(8, smsqueue.getVersion());
      prest.setString(9, smsqueue.getType());
      prest.setString(10, smsqueue.getSmsStatus());
      prest.setString(11, smsqueue.getApprovalBy());
      prest.setString(12, smsqueue.getDept());
      prest.setInt(13, smsqueue.getDeptId());
      prest.setInt(14, smsqueue.getTotalSms());
      prest.setInt(15, smsqueue.getSmsType().getTblRefId());//TODO: channel id will be null for now until change of socket message format to include channel id parameter from client
      log.info("Inserting into db");
      int update = prest.executeUpdate();
      ResultSet rs = prest.getGeneratedKeys();
      if(rs.next()){
    	  id = rs.getLong(1);
      }
      prest.close();
      return id;
    }
    catch (SQLException e) {
      log.info("Failed to insert SMS. Reason: " + e.getMessage(), e);
      return 0;
    }

  }
  
  private String decrypt(String message) throws SgwException{
	  Matcher m = PIN_ENC_PATTERN.matcher(message);
	  String finalMsg = message;
		if(m.find()){
			try{
				String encryptedPin = m.group().replaceAll(PIN_ENC_ENCLOSURE, "");
				SecretKeySpec keyspec = new SecretKeySpec(Hex.decodeHex(this.ZPK.toCharArray()), ENC_ALGO);
				Cipher cipher = Cipher.getInstance(ENC_MODE_3DES_ECB_NOPADDING);
				cipher.init(Cipher.DECRYPT_MODE, keyspec);			
				byte[] clearPin = cipher.doFinal(Hex.decodeHex(encryptedPin.toCharArray()));
				String clearPinHex = Hex.encodeHexString(clearPin).replaceAll(PIN_ENC_PADDING, "");
				finalMsg = m.replaceAll(clearPinHex);
			}catch(Exception e){
		    	throw new SgwException("Failed to decrypting pin", e );				
			}
		}		
	  return finalMsg;
  }  
  
	private int getCharPerSms() throws Exception{
		if(this.charPerSms == 0){
			  PreparedStatement stmt = conn.prepareStatement("select char_per_sms, concatenate_sms,concatenate_sms_no from sms_cfg where version = ?");
			  stmt.setInt(1, Integer.parseInt(Constants.APPSVERSION));
			  ResultSet rs = stmt.executeQuery();
			  if(rs.next()){
				  this.charPerSms = CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, rs.getString(1), rs.getString(2), rs.getString(3));
			  }			
		}
		return this.charPerSms;
	}
	
	private boolean getApprovalRight(String userId) throws Exception{
		Boolean approval = false;
			  PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(rights_id) FROM user_rights WHERE user_id=? AND rights_id='"+Constants.SMS_APPROVAL+"' ");
			  stmt.setString(1,userId);
			  ResultSet rs = stmt.executeQuery();
			  if(rs.next()){
				   if((rs.getBigDecimal(1).intValue())>0){
					   approval = true;
				   }
			  }			
		
		return approval;
	}


  private TblRef getDefaultSmsType() throws Exception{
	  if(defaultSmsType == null){
		  defaultSmsType = new TblRef();
		  PreparedStatement stmt = conn.prepareStatement("select a.tbl_ref_id, a.online, a.encrypted from tbl_ref a, sms_cfg b where a.tbl_ref_id = b.default_sms_type and b.version = ?");
		  stmt.setInt(1, Integer.parseInt(Constants.APPSVERSION));
		  ResultSet rs = stmt.executeQuery();
		  if(rs.next()){
			  defaultSmsType.setTblRefId(rs.getInt(1));
			  defaultSmsType.setOnline(rs.getBoolean(2));
			  defaultSmsType.setEncrypted(rs.getBoolean(3));
		  }
		  rs.close();
		  stmt.close();		  
	  }
	  return this.defaultSmsType;
  }
  
  private TblRef selectSMSType(String userId, String channelId) throws Exception{
	  PreparedStatement stmt;
	  //if has channel id
	  if(StringUtils.isNotEmpty(channelId)){
		  stmt = conn.prepareStatement("select a.sms_type, b.online, b.encrypted from user_channel a, tbl_ref b where a.user_id = ? and a.channel_id = ? and a.sms_type=b.tbl_ref_id and a.status='Active' ");
		  stmt.setString(1, userId);
		  stmt.setString(2, channelId);
	  }else{//check if it's only one channel for user
		  stmt = conn.prepareStatement("select a.sms_type, b.online, b.encrypted from user_channel a, tbl_ref b where user_id = ? and a.sms_type=b.tbl_ref_id and a.status='Active' ");
		  stmt.setString(1, userId);
	  }
	  ResultSet rs = (ResultSet)stmt.executeQuery();
	  TblRef smsType = new TblRef();
	  if(rs.next()){
		  smsType.setTblRefId(rs.getInt(1));
		  smsType.setOnline(rs.getBoolean(2));
		  smsType.setEncrypted(rs.getBoolean(3));
	  }
	  //if record size not 1, lookup default sms type
	  if(smsType.getTblRefId() == 0 || rs.next()){
		  stmt.close();
		  rs.close();
		  smsType = getDefaultSmsType();
	  }
	  if(smsType.getTblRefId() == 0){
		  throw new RuntimeException("No Default Sms Type was set. Please configure in system setting.");
	  }
	  return smsType;
  }
  
  public void closeConnection() throws Exception{
	  this.conn.close();
  }
}