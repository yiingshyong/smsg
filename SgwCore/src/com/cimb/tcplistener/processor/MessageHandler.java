package com.cimb.tcplistener.processor;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;

import com.bcb.sgwcore.SgwException;

public class MessageHandler extends Thread
{
  private Socket connectionSocket;
  private CommonUtil commonUtil;
  Log log = LogManager.getLog();
  int portnum;

  public MessageHandler(Socket c)
    throws IOException
  {
    this.connectionSocket = c;

    int timeout = Integer.parseInt(PropertyUtil.getKey("TIMEOUT_IN_MINUTE"));
    c.setSoTimeout(60000 * timeout);
    c.setKeepAlive(true);
//    commonUtil = new CommonUtil();
  }

  public void run() 
  {
      long smsId = 0;
	  
    try
    {
      this.log.info("New client connection from " + this.connectionSocket.getInetAddress().getHostAddress() + " on port " + this.connectionSocket.getPort());

      this.portnum = this.connectionSocket.getPort();
      StringBuffer response = new StringBuffer();

      BufferedInputStream bis = new BufferedInputStream(this.connectionSocket.getInputStream());

      InputStreamReader isr = new InputStreamReader(bis, "US-ASCII");

      long startTime = 0;
      int j = 0;
      int messageLenght = Integer.parseInt(PropertyUtil.getKey("message.length"));
      	log.info("Reading incoming data ...");
        while (true) {
          int i = isr.read();
          if (i == -1) {
          	log.info("EOF detected, connection will be closed.");
            break;
          }

          if (j < 4) {//
          }
          else {
        	  response.append((char)i);
          }

          j++;
          if (j == messageLenght) {        	  
        	startTime = System.currentTimeMillis();        		
            this.log.info("Port: " + this.portnum + " - Incoming msg: '" + response.toString() + "'");
            if (response.length() > 38) {
              String tarNum = "";
              String tarMode = "";
              String tarmPriority = "";
              String userId = "";
              String tarMsg = "";


              tarNum = response.substring(0, 20);
              tarMode = response.substring(20, 27);
              tarmPriority = response.substring(27, 28);
              userId = response.substring(28, 38);
              tarMsg = response.substring(38);

              this.log.debug("tarNum: " + tarNum + " length: " + tarNum.length());
              this.log.debug("tarMode: " + tarMode + " length: " + tarMode.length());
              this.log.debug("tarmPriority: " + tarmPriority + " length: " + tarmPriority.length());
              this.log.debug("userId: " + userId + " length: " + userId.length());
              this.log.debug("tarMsg: " + tarMsg + " length: " + tarMsg.length());
              try{
            	  if(commonUtil == null){
            		  commonUtil = new CommonUtil();
            	  }
                  smsId = commonUtil.processMsg(removeSpaces(tarNum.trim()), removeSpaces(tarMode), removeSpaces(tarmPriority), removeSpaces(userId), tarMsg.trim(), this.connectionSocket.getInetAddress().getHostAddress());            	  
              }catch(SgwException ve){
            	  log.error("Error while processing incoming data. Due to: " + ve.getMessage(), ve);
              }

              response = new StringBuffer("");
            }
            if (PropertyUtil.getKey("REPLY").equals("Y")) {
                DataOutputStream output = new DataOutputStream(this.connectionSocket.getOutputStream());
              String msg = "-1";

              byte[] a = encodeInt(msg.length());
              output.write(a);
              output.writeBytes(new String(msg.getBytes(), "US-ASCII"));
              output.flush();
              output.close();
            }

            j = 0;
            log.info("SMS Id: " + smsId + " took " + (System.currentTimeMillis()-startTime) + " ms");
          }
        }
    }
    catch (Exception e) {
    	log.error("Unexpected error caught: " + e.getMessage(), e);
    }finally{
    	if(!connectionSocket.isClosed()){
    		try{
    			if(commonUtil != null){
    				commonUtil.closeConnection();
    			}
	   	    	connectionSocket.getInputStream().close();
	    		connectionSocket.close();
	    		log.info("Port " + portnum + " connection closed");
    		}catch(Exception e){
    			log.error("Error closing socket connection. Reason: " + e.getMessage(), e);
    		}
    	}
    }
  }

  private static byte[] encodeInt(int i)
  {
    byte[] b = new byte[4];
    b[0] = (byte)((i & 0xFF000000) >> 24);
    b[1] = (byte)((i & 0xFF0000) >> 16);
    b[2] = (byte)((i & 0xFF00) >> 8);
    b[3] = (byte)(i & 0xFF);
    return b;
  }

  private static String removeSpaces(String s) {
    StringTokenizer st = new StringTokenizer(s, " ", false);
    String t = "";
    while (st.hasMoreElements()) t = t + st.nextElement();
    return t;
  }
}
