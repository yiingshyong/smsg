package com.cimb.tcplistener.processor;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;


public class TcpClientSimulator {

	public static void main(String[] args) {
		new TcpClientSimulator().run();
	}
	
	public void run(){
		try{
			final Socket clientSocket = new Socket("10.17.49.122", 33302);
//			final Socket clientSocket = new Socket("10.104.21.12", 33301);		
			Thread responseThread = new Thread(new Runnable() {				
				@Override
				public void run() {
					try{
						InputStream is = clientSocket.getInputStream();
						int byteInt = 0;
						System.out.println("Waiting for server response ...");
						while((byteInt = is.read()) != -1){
							System.out.print((char)byteInt);
						}
						System.out.println();
						System.out.println("EOF from server. Existing...");
						
					}catch(Exception e){
						System.out.println("Error in getting input from server");
						e.printStackTrace();
					}
				}
			});			
			responseThread.start();
			
			while(true){
				String readLine = "";
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				while(!(readLine=reader.readLine()).equals("quit")){
					System.out.println(readLine);
					clientSocket.getOutputStream().write(readLine.getBytes());
					clientSocket.getOutputStream().flush();
				}
				System.out.println("Client exiting");
				clientSocket.close();
				break;
			}
			System.out.println("Closed");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
