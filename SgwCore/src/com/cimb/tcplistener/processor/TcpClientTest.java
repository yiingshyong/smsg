package com.cimb.tcplistener.processor;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

/**
 *
 *	0-20 : recipient mobile num
 *  20-27 : sms mode text/unicode
 *  27-28 : priority
 *  28-38 : sender userid
 *  38 : sms content
 *  
 * @author vincent
 *
 */
public class TcpClientTest {
	private String mobileNum;
	private String smsMode;
	private String priority;
	private String senderId;
	private String message;
	

	public TcpClientTest(String mobileNum, String smsMode, String priority,
			String senderId, String message) {
		super();
		this.mobileNum = mobileNum;
		this.smsMode = smsMode;
		this.priority = priority;
		this.senderId = senderId;
		this.message = message;
	}

	/**
	 * Sample arguements:
	 * 0126900868 1 1 vincent testtcp 1000 1 single abc - using single tcp connection, 1 message, 1000ms sleep
	 * 0126900868 1 1 vincent testtcp 1000 10 multiple abc - using concurrent tcp connection, 10 message, 1000ms sleep
	 * @param args
	 */
	public static void main(final String[] args) {
		try{
			for(int i=0; i<args.length; i++){
				System.out.println("param: " + i + ":" + args[i]);

			}
			String msg = "";
		if(args[7].equals("single")){
			if(args[8].equals("random")){
				msg = String.valueOf(RandomUtils.nextLong());
			}else{
				msg = args[4];
			}
			TcpClientTest client = new TcpClientTest(args[0], args[1], args[2], args[3], msg);
			String socketMsg = client.formatMsg();
			System.out.println("Message: '" + socketMsg);
			client.longTalk(socketMsg, Integer.parseInt(args[5]), Integer.parseInt(args[6]));						
		}else if(args[7].equals("multiple")){
			Runnable run = new Runnable() {			
				public void run() {
					String msg = null;
					if(args[8].equals("random")){
						msg = String.valueOf(RandomUtils.nextLong());
					}else{
						msg = args[4];
					}
					TcpClientTest client = new TcpClientTest(args[0], args[1], args[2], args[3], msg);
					String socketMsg = client.formatMsg();
					System.out.println("Message: '" + socketMsg);
					client.talk(socketMsg);
				}
			};
			for(int i=0; i<Integer.parseInt(args[5]); i++){
				System.out.println("Sending msg #" + i);
				try {
					Thread.sleep(Integer.parseInt(args[6]));
					new Thread(run).start();			
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private String formatMsg(){
		String padChar = " ";
		StringBuffer socketMsgBuf = new StringBuffer();
		socketMsgBuf.append(StringUtils.leftPad("", 4, padChar));		
		socketMsgBuf.append(StringUtils.rightPad(this.mobileNum, 20, padChar));		
		socketMsgBuf.append(StringUtils.rightPad(Integer.parseInt(this.smsMode)==1? "text" : "utf", 7, padChar));
		socketMsgBuf.append(StringUtils.rightPad(this.priority, 1, padChar));
		socketMsgBuf.append(StringUtils.rightPad(this.senderId, 10, padChar));		
		socketMsgBuf.append(StringUtils.rightPad(this.message,Integer.parseInt(PropertyUtil.getKey("message.length"))-42, padChar));		
		return socketMsgBuf.toString();
	}
	
	private void talk(String socketMsg) {
		try {
//			BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
			String host = PropertyUtil.getKey("host");
			int port = Integer.parseInt(PropertyUtil.getKey("PORT"));
			Socket socket = new Socket(host, port);
			System.out.println("Connecting to socket server " + host + ":" + port + " From local port: " + socket.getLocalPort());
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			System.out.println("Writing to socket: '" + socketMsg + "'");
			char[] chars = socketMsg.toCharArray();
			for(char a : chars){
				writer.write(a);				
			}
			writer.flush();
			writer.close();
			br.close();
			socket.close();
			System.out.println("Client socket closed.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void longTalk(String socketMsg, int sleep, int no) {
		try {
			System.out.println("Number of sms: " + no);
			long startTime = System.currentTimeMillis();
			String host = PropertyUtil.getKey("host");
			int port = Integer.parseInt(PropertyUtil.getKey("PORT"));
			Socket socket = new Socket(host, port);
			System.out.println("Connecting to socket server " + host + ":" + port + " From local port: " + socket.getLocalPort());
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			System.out.println("Writing to socket: '" + socketMsg + "'");
			
			
			char[] chars = socketMsg.toCharArray();
			for(int i=0; i<no; i++){
				for(char a : chars){
					writer.write(a);				
				}
				Thread.sleep(sleep);
			}

			writer.close();
			br.close();
			socket.close();			
			System.out.println("Client socket closed.");
			System.out.println("Time taken:" + (System.currentTimeMillis()-startTime));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getSmsMode() {
		return smsMode;
	}

	public void setSmsMode(String smsMode) {
		this.smsMode = smsMode;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}
