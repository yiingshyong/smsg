

package com.bcb.common.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.DeptForm;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;

public final class AddDept extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadocs
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			DeptForm editdeptform = (DeptForm) form;
			HttpSession session = request.getSession();

			// Pre-populating group status
			editdeptform.setSelected_deptstatus("Active");
			ArrayList status_label = new ArrayList();
			status_label.add("Active");
			status_label.add("Closed");
			editdeptform.setDeptstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add("Active");
			status_value.add("Closed");
			editdeptform.setDeptstatus_value(status_value);
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			List apps_result = hibernate_session.createQuery("from Apps order by appsId").list();
			request.setAttribute(Constants.APPS_KEY, apps_result);
			
			hibernate_session.getTransaction().commit();
			
		}
		catch (Exception E)
		{
			log4jUtil.error("AddDept System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("AddDept",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
