

package com.bcb.common.actions;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.Apps;
import com.bcb.common.modal.AppsRole;
import com.bcb.common.modal.PermRef;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.common.forms.RoleForm;
import com.bcb.common.util.Constants;
public final class AddRole extends Action {

    // ----------------------------------------------------- Instance Variables

    /**
     * The <code>Log</code> instance for this application.
     */
   // private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");

    // --------------------------------------------------------- Public Methods

        // See superclass for Javadoc
    public ActionForward execute(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        throws Exception {
 
try {
    	// Generate the dump ref no
	
	RoleForm editroleform = (RoleForm) form;
	
  	Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
    hibernate_session.beginTransaction();
	 
	   
    editroleform.setSelected_userstatus("Active");
			     ArrayList status_label = new ArrayList();
		     status_label.add("Active");
		     status_label.add("Closed");
		     editroleform.setUserstatus_label(status_label);
		     ArrayList status_value = new ArrayList();
		     status_value.add("Active");
		     status_value.add("Closed");
		     editroleform.setUserstatus_value(status_value);
		     
		

		// Select list of perm_ref
		     List result2 = hibernate_session.createQuery("from PermRef order by appsId, permId").list();

		     int t_appsId=0;

		     AppsRole anAppsRole =null;
		     ArrayList permref_result = new ArrayList();

		     for (int i = 0 ; i < result2.size(); i ++ )
		     {
		     	PermRef aPermRef = (PermRef) result2.get(i);
		     	if ( aPermRef.getAppsId() == t_appsId)
		     	{
		     		// Still the same
		     	}
		     	else
		     	{
		     		// Not the same
		     		anAppsRole = new AppsRole();
		     		Apps anApps = (Apps) hibernate_session.get(Apps.class,Integer.valueOf(aPermRef.getAppsId()));
		     		anAppsRole.setAppsName(anApps.getAppsName());
		     		permref_result.add(anAppsRole);
		     			anAppsRole.setPermList(new ArrayList());
		     			t_appsId = aPermRef.getAppsId();
		     	};
		     	anAppsRole.getPermList().add(aPermRef);
		     }
		     request.setAttribute(Constants.PERM_KEY, permref_result);

		     // select list of apps
		     
		     
	  
		     List apps_result = hibernate_session.createQuery("from Apps order by appsId").list();
	  request.setAttribute(Constants.APPS_KEY, apps_result);

	  
        	   hibernate_session.getTransaction().commit();
    }
    catch (Exception E)
    {
    	log4jUtil.error("AddRole System Error[" + E.getMessage()+ "]");
    	
    	ActionMessages messages = new ActionMessages();
    	messages.add("AddRole",new ActionMessage("errors.system"));
    	this.addErrors(request,messages);
    }
	   return (mapping.findForward("success"));
  
}
}
