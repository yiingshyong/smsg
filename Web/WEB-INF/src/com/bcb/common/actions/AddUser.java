

package com.bcb.common.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.UserForm;
import com.bcb.common.modal.Branch;
import com.bcb.common.modal.Department;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class AddUser extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			// Generate the dump ref no
			
			UserForm edituserform = (UserForm) form;
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			edituserform.setSelected_userstatus(Constants.STATUS_ACTIVE_LABEL);
			ArrayList status_label = new ArrayList();
			status_label.add(Constants.STATUS_ACTIVE_LABEL);
			status_label.add(Constants.STATUS_CLOSED_LABEL);
			status_label.add(Constants.STATUS_RESIGN_LABEL);
			edituserform.setUserstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add(Constants.STATUS_ACTIVE_LABEL);
			status_value.add(Constants.STATUS_CLOSED_LABEL);
			status_value.add(Constants.STATUS_RESIGN_LABEL);
			edituserform.setUserstatus_value(status_value);
			
			
//			------------------------------ FOR BRANCH -------------------------------
			//   edituserform.setSelected_branch(Integer.valueOf(0));
			List branch_result = hibernate_session.createQuery("from Branch where branch_status = 'Active' order by branch_code").list();
			ArrayList branch_label = new ArrayList();
			ArrayList branch_value = new ArrayList();
			for (int i =0 ; i < branch_result.size(); i ++)
			{
				Branch aBranch = (Branch) branch_result.get(i); 
				branch_label.add(aBranch.getBranchCode() + " - " +aBranch.getBranchName());
				branch_value.add(Integer.valueOf(aBranch.getBranchId()));
				
			}
			edituserform.setBranch_label(branch_label);
			edituserform.setBranch_value(branch_value);
//			------------------------------ FOR BRANCH -------------------------------
			
//			------------------------------ FOR Group -------------------------------
			//   edituserform.setSelected_group(Integer.valueOf(0));
			List group_result = hibernate_session.createQuery("from Department where deptStatus = 'Active' order by id").list();
			
			ArrayList group_label = new ArrayList();
			ArrayList group_value = new ArrayList();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				Department aGroup = (Department) group_result.get(i); 
				group_label.add(aGroup.getDeptName());
				group_value.add(Integer.valueOf(aGroup.getId()));
				
			}
			edituserform.setGroup_label(group_label);
			edituserform.setGroup_value(group_value);
//			------------------------------ FOR Group -------------------------------
			//	 ------------------------------ FOR Role -------------------------------
			//   edituserform.setSelected_role(Integer.valueOf(0));
			List role_result = hibernate_session.createQuery("from RoleRef where role_status = 'Active' order by role_name").list();
			ArrayList role_label = new ArrayList();
			ArrayList role_value = new ArrayList();
			for (int i =0 ; i < role_result.size(); i ++)
			{
				RoleRef aRole= (RoleRef) role_result.get(i); 
				role_label.add(aRole.getRoleName());
				role_value.add(Integer.valueOf(aRole.getRoleId()));
				
			}
			edituserform.setRole_label(role_label);
			edituserform.setRole_value(role_value);

			String selectedRoles =edituserform.getStrSelected_roles();
			
			if (selectedRoles !=null && selectedRoles.length()> 0){
				String[] rolesArray = selectedRoles.split(",");
				ArrayList selectedRoleLabel=new ArrayList();
				ArrayList selectedRoleValue=new ArrayList();

				for(int i=0;i<rolesArray.length;i++){
					Integer roleValue=Integer.valueOf(rolesArray[i].toString());

					selectedRoleValue.add(roleValue);
					RoleRef aRoleRef= (RoleRef)hibernate_session.get(RoleRef.class, roleValue);
					selectedRoleLabel.add(aRoleRef.getRoleName());
				}

				edituserform.setSelected_role_label(selectedRoleLabel);
				edituserform.setSelected_role_value(selectedRoleValue);
			}else{
				edituserform.setSelected_role_label(new ArrayList());
				edituserform.setSelected_role_value(new ArrayList());
			}

			hibernate_session.getTransaction().commit();
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error("AddUser System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("AddUser",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
