/**
 * Author		: Ong CheeLung
 * Department	: IT Development
 * Position		: System Analysis
 * Contacts		: ong.cheelung@cimb.com.my
 * Date			: 12/02/2007
 */
package com.bcb.common.actions;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.InvalidCancelException;
import org.apache.struts.config.ForwardConfig;


public class CustomRequestProcessor extends org.apache.struts.action.RequestProcessor {

	@Override
	protected boolean processValidate(HttpServletRequest request,
			HttpServletResponse response, ActionForm form, ActionMapping mapping)
			throws IOException, ServletException, InvalidCancelException {
		// TODO Auto-generated method stub
		boolean result = super.processValidate(request, response, form, mapping); 
//		ActionMessages errors = (ActionMessages) request.getAttribute(Globals.ERROR_KEY);
//		if(errors != null && !errors.isEmpty()){
//			Iterator ite = errors.get();
//			while(ite.hasNext()){
//				ActionMessage message = (ActionMessage) ite.next();
//				Object[] values = message.getValues();				
//				if(values != null && values.length > 0){
//					for(int i=0; i<values.length; i++){
//						if(StringUtils.isNotBlank(values[i].toString())){
//							values[i] = StringEscapeUtils.escapeHtml(values[i].toString()); 							
//						}
//					}
//				}
//			}
//		}
//		request.setAttribute(Globals.ERROR_KEY, errors);
		return result;
	}

	@Override
	protected boolean processForward(HttpServletRequest request,
			HttpServletResponse response, ActionMapping mapping)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		return super.processForward(request, response, mapping);
	}

	@Override
	protected void processForwardConfig(HttpServletRequest request,
			HttpServletResponse response, ForwardConfig forward)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		super.processForwardConfig(request, response, forward);
	}
	
//	private static final Log log = LogFactory.getLog(CustomRequestProcessor.class);
	
//	@Override
//	protected void processForwardConfig(HttpServletRequest request, HttpServletResponse response, ForwardConfig forward) throws IOException, ServletException {
//		if(AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.SMSG_AUTO_CLOSE_SESSION_END_REQUEST)){
//			log.debug("Closing unclosed hibernate session");
//			Session session = HibernateUtil.getOnlySessionFactory().getCurrentSession();
//			closeSession(session);
//			session = HibernateUtilCrsmsg.getOnlySessionFactory().getCurrentSession();
//			closeSession(session);		
////			session = DaoBeanFactory.getHibernateTemplate().getSessionFactory().getCurrentSession();
////			closeSession(session);
//		}
//		super.processForwardConfig(request, response, forward);
//	}
//	
//	private void closeSession(Session session){
//		if(session.getTransaction().isActive()){
//			session.getTransaction().commit();
//		}
//		if(session != null && session.isOpen()){
//			session.close();
//		}		
//	}

//	protected boolean processPreprocess (
//			
//            HttpServletRequest request,
//            HttpServletResponse response) {
//            
//		HttpSession session = request.getSession(false);
//		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
//		
//		if ( theLogonCredential == null)
//        {
//        	return true;
//        };
//        
//		String path = request.getServletPath();
//		if ( path.equals(Constants.WELCOME_PATH) ||
//				path.equals(Constants.LOGON_PATH) ||
//				path.equals(Constants.LOGON_PATH)) 
//		return true;
//
//	    int lastindex = path.lastIndexOf(Constants.STR_SLASH);
//	    int firstindex = path.indexOf(Constants.STR_SLASH);
//	        
//	        
//        Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
//        hibernate_session.beginTransaction();
//        List resultList = hibernate_session.createQuery(Constants.QUERY_APPS)
//        .setString(0, path.substring(firstindex, lastindex))
//		.list();
//
//        if (resultList.size() > 0){
//        	Apps apps = (Apps) resultList.get(0);
//        	session.setMaxInactiveInterval(apps.getTimeoutValue());
//        }else{
//        	session.setMaxInactiveInterval(Constants.DEFAULT_TIMEOUT);
//        }
//        hibernate_session.getTransaction().commit();
//        if(hibernate_session != null && hibernate_session.isOpen()){
//            hibernate_session.close();        	
//        }
//        return true;
//
//	}
	
	
    
}
