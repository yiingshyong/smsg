

package com.bcb.common.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.DeptSearchForm;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class DeptSearch extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try
		{
			// Extract attributes we will need
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			
			if ( theLogonCredential.allowed(com.bcb.common.util.Constants.APPSID,com.bcb.common.util.Constants.DEPTMGMT_PERM) == false)
			{
				
				return (mapping.findForward("accdeny"));
				
			};
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			hibernate_session.beginTransaction();
			DeptSearchForm gsform =  (DeptSearchForm) form;
			String orderBy = " order by deptName desc";
			List result = null; 
			if ( gsform == null)
			{
				
			}
			else if (gsform.getSelectedState().equals("Department Name"))
			{
				// By Customer Name
				result = hibernate_session.createQuery
				("from Department where deptName like ?" + orderBy).setString(0,gsform.getSearchkey() + "%").list();
				request.setAttribute(Constants.DEPT_KEY, result);
			}
			
			hibernate_session.getTransaction().commit();
			
		}
		catch (Exception E)
		{E.printStackTrace();
		log4jUtil.error("DeptSearch System Error[" + E.getMessage()+ "]");
		ActionMessages messages = new ActionMessages();
		messages.add("DeptSearch",new ActionMessage("errors.system"));
		this.addErrors(request,messages);
		};
		
		return (mapping.findForward("success"));
		
	}
	
}
