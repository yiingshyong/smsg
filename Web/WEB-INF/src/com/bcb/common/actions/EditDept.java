

package com.bcb.common.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.DeptForm;
import com.bcb.common.modal.Department;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class EditDept extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			
			String deptId = request.getParameter("deptpid");
			DeptForm editdeptform = (DeptForm) form;
			
			if (deptId == null || deptId.length() < 1)
				deptId = editdeptform.getDeptid().toString();
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			List dept_list=hibernate_session.createQuery("from Department where id = ?").setInteger(0, Integer.parseInt(deptId)).list();
			Department theDept= (Department) dept_list.get(0);
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat time_formatter = new SimpleDateFormat("hh:mm:ss");
			
			editdeptform.setDeptid(deptId.toString());
			editdeptform.setDeptname(theDept.getDeptName());
			editdeptform.setDeptdescription(theDept.getDeptDescription());
			
			editdeptform.setCreatedBy(theDept.getCreatedBy());
			if ( theDept.getCreatedDate() != null) 
				editdeptform.setCreatedDate(formatter.format(theDept.getCreatedDate()));
			if ( theDept.getCreatedTime() != null)
				editdeptform.setCreatedTime( (time_formatter.format(theDept.getCreatedTime())));
			
			editdeptform.setModifiedBy(theDept.getModifiedBy());
			if ( theDept.getModifiedDate() != null) 
				editdeptform.setModifiedDate(formatter.format(theDept.getModifiedDate()));
			
			if ( theDept.getModifiedTime() != null)
				editdeptform.setModifiedTime( (time_formatter.format(theDept.getModifiedTime())));

			editdeptform.setSelected_deptstatus(theDept.getDeptStatus());
			
			ArrayList status_label = new ArrayList();
			status_label.add("Active");
			status_label.add("Closed");
			editdeptform.setDeptstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add("Active");
			status_value.add("Closed");
			editdeptform.setDeptstatus_value(status_value);
			
//			List<TblRef> types = DaoBeanFactory.getTblRefDao().getSmsTypes();
//			types.remove(0);//remove express type
//			editdeptform.setSmsTypes(types);
//			editdeptform.setSelectedSmsType(String.valueOf(theDept.getSmsType()));
			hibernate_session.getTransaction().commit();
		}
		catch (Exception E)
		{
			log4jUtil.error("EditDept System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("EditDept",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
