

package com.bcb.common.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.RoleForm;
import com.bcb.common.modal.Apps;
import com.bcb.common.modal.AppsRole;
import com.bcb.common.modal.PermRef;
import com.bcb.common.modal.PermRole;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class EditRole extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			// Generate the dump ref no
			Integer roleId = Integer.valueOf(request.getParameter("roleid"));
			RoleForm editroleform = (RoleForm) form;
			if (roleId == null || roleId.intValue() == 0)
				roleId = editroleform.getRoleid();
			editroleform.setRoleid(roleId);
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			RoleRef theRole= (RoleRef) hibernate_session.get(RoleRef.class, roleId);
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat time_formatter = new SimpleDateFormat("hh:mm:ss");
			
			
			if ( theRole.getModifiedDate() != null) 
				editroleform.setModifieddate(formatter.format(theRole.getModifiedDate()));
			if ( theRole.getCreatedDate() != null) 
				editroleform.setCreateddate(formatter.format(theRole.getCreatedDate()));
			if ( theRole.getModifiedTime() != null)
				editroleform.setModifiedtime( (time_formatter.format(theRole.getModifiedTime())));
			if ( theRole.getCreatedTime() != null)
				editroleform.setCreatedtime( (time_formatter.format(theRole.getCreatedTime())));
			editroleform.setCreatedby(theRole.getCreatedBy());
			editroleform.setModifiedby(theRole.getModifiedBy());
			
			
			List apps_result = hibernate_session.createQuery("from Apps order by appsId").list();
			request.setAttribute(Constants.APPS_KEY, apps_result);
			
			List result2 = hibernate_session.createQuery("from PermRef order by appsId, permId").list();
			
			int t_appsId=0;
			
			AppsRole anAppsRole =null;
			ArrayList permref_result = new ArrayList();
			
			for (int i = 0 ; i < result2.size(); i ++ )
			{
				PermRef aPermRef = (PermRef) result2.get(i);
				if ( aPermRef.getAppsId() == t_appsId)
				{
					// Still the same
				}
				else
				{
					// Not the same
					anAppsRole = new AppsRole();
					Apps anApps = (Apps) hibernate_session.get(Apps.class,Integer.valueOf(aPermRef.getAppsId()));
					anAppsRole.setAppsName(anApps.getAppsName());
					permref_result.add(anAppsRole);
					anAppsRole.setPermList(new ArrayList());
					t_appsId = aPermRef.getAppsId();
				};
				anAppsRole.getPermList().add(aPermRef);
			}
			request.setAttribute(Constants.PERM_KEY, permref_result);
			
//			On top is whole list
//			Below is what the role has.
			// Apps
			List appsofrole_result = hibernate_session.createQuery("from RoleApps where roleId = ? order by appsId").setInteger(0,roleId.intValue()).list();
			request.setAttribute(Constants.APPSOFROLE_KEY, appsofrole_result);
//			Perm
			ArrayList permsofrole_result = new ArrayList();
			Set oldSet = theRole.getPermRoles();
			
			Iterator it = oldSet.iterator();
			while (it.hasNext()) {
				// Get element
				PermRole element = (PermRole) it.next();
				permsofrole_result.add(Integer.valueOf(element.getPermRef()));
			}
			request.setAttribute(Constants.PERMSOFROLE_KEY, permsofrole_result);
			editroleform.setRolename(theRole.getRoleName());
			editroleform.setRoledescription(theRole.getRoleDescription());
			
			editroleform.setSelected_userstatus(theRole.getRoleStatus());
			ArrayList status_label = new ArrayList();
			status_label.add("Active");
			status_label.add("Closed");
			editroleform.setUserstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add("Active");
			status_value.add("Closed");
			editroleform.setUserstatus_value(status_value);
			
			hibernate_session.getTransaction().commit();
		}
		catch (Exception E)
		{
			log4jUtil.error("EditUser System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("EditUser",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
