
package com.bcb.common.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.UserForm;
import com.bcb.common.modal.Branch;
import com.bcb.common.modal.Department;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.modal.User;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class EditUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			// Generate the dump ref no
			String userId = request.getParameter(Constants.PARAM_USERID);
			UserForm edituserform = (UserForm) form;
			
			if (userId == null || userId.length() < 1)
				userId = edituserform.getUserid();
			
			hibernate_session.beginTransaction();
			User theUser= (User) hibernate_session.get(User.class, userId);
			
			SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
			SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
			
			edituserform.setUserid(userId);
			if ( theUser.getModifiedDate() != null) 
				edituserform.setModifieddate(formatter.format(theUser.getModifiedDate()));
			if ( theUser.getCreatedDate() != null) 
				edituserform.setCreateddate(formatter.format(theUser.getCreatedDate()));
			if ( theUser.getModifiedTime() != null)
				edituserform.setModifiedtime( (time_formatter.format(theUser.getModifiedTime())));
			if ( theUser.getCreatedTime() != null)
				edituserform.setCreatedtime( (time_formatter.format(theUser.getCreatedTime())));
			if ( theUser.getLastLoginDate() != null)
				edituserform.setLastLoginDate((formatter.format(theUser.getLastLoginDate())));
			
			edituserform.setCreatedby(theUser.getCreatedBy());
			edituserform.setModifiedby(theUser.getModifiedBy());
			
			if ( theUser.getLastPwdchg() != null)
				edituserform.setLastpwdchg(formatter.format(theUser.getLastPwdchg()));
			if ( theUser.getFailedLogon() != null)
				edituserform.setFailedlogon(theUser.getFailedLogon().toString());
			else
				edituserform.setFailedlogon("0");
			edituserform.setBizphone(theUser.getBizPhone());
			edituserform.setEmailid(theUser.getEmailId());
			edituserform.setExt(theUser.getExt());
			edituserform.setHphone(theUser.getHPhone());
			edituserform.setUsername(theUser.getUserName());
			edituserform.setStaffid(theUser.getStaffId());
			edituserform.setSelected_userstatus(theUser.getUserStatus());
			ArrayList status_label = new ArrayList();
			status_label.add(Constants.STATUS_ACTIVE_LABEL);
			status_label.add(Constants.STATUS_CLOSED_LABEL);
			status_label.add(Constants.STATUS_RESIGN_LABEL);
			edituserform.setUserstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add(Constants.STATUS_ACTIVE_LABEL);
			status_value.add(Constants.STATUS_CLOSED_LABEL);
			status_value.add(Constants.STATUS_RESIGN_LABEL);
			edituserform.setUserstatus_value(status_value);
			
			
			// ------------------------------ FOR BRANCH -------------------------------
			edituserform.setSelected_branch(Integer.valueOf(theUser.getBranchDept()));
			List branch_result = hibernate_session.createQuery("from Branch where branch_status = 'Active' order by branch_code").list();
			ArrayList branch_label = new ArrayList();
			ArrayList branch_value = new ArrayList();
			for (int i =0 ; i < branch_result.size(); i ++)
			{
				Branch aBranch = (Branch) branch_result.get(i); 
				branch_label.add(aBranch.getBranchCode() + " - " +aBranch.getBranchName());
				branch_value.add(Integer.valueOf(aBranch.getBranchId()));
				
			}
			edituserform.setBranch_label(branch_label);
			edituserform.setBranch_value(branch_value);
			// ------------------------------ FOR BRANCH -------------------------------
			
			//		 ------------------------------ FOR Group -------------------------------
			edituserform.setSelected_group(Integer.valueOf(theUser.getUserGroup()));
			List group_result = hibernate_session.createQuery("from Department where deptStatus = 'Active' order by id").list();
			
			ArrayList group_label = new ArrayList();
			ArrayList group_value = new ArrayList();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				Department aGroup = (Department) group_result.get(i); 
				group_label.add(aGroup.getDeptName());
				group_value.add(Integer.valueOf(aGroup.getId()));
				
			}
			edituserform.setGroup_label(group_label);
			edituserform.setGroup_value(group_value);
			//	 ------------------------------ FOR Group -------------------------------
			//	 ------------------------------ FOR Role -------------------------------
			//edituserform.setSelected_role(Integer.valueOf(theUser.getRole()));
			List role_result = hibernate_session.createQuery("from RoleRef where role_status = 'Active' order by role_name").list();
			ArrayList role_label = new ArrayList();
			ArrayList role_value = new ArrayList();
			for (int i =0 ; i < role_result.size(); i ++)
			{
				RoleRef aRole= (RoleRef) role_result.get(i); 
				role_label.add(aRole.getRoleName());
				role_value.add(Integer.valueOf(aRole.getRoleId()));
				
			}
			edituserform.setRole_label(role_label);
			edituserform.setRole_value(role_value);
			//		 ------------------------------ FOR Role -------------------------------
			
//			------------------------------ FOR User Roles -------------------------------
			String selectedRoles =edituserform.getStrSelected_roles();
			
			if (selectedRoles !=null && selectedRoles.length()> 0){
				String[] rolesArray = selectedRoles.split(",");
				ArrayList selectedRoleLabel=new ArrayList();
				ArrayList selectedRoleValue=new ArrayList();
				
				for(int i=0;i<rolesArray.length;i++){
					Integer roleValue=Integer.valueOf(rolesArray[i].toString());
					
					selectedRoleValue.add(roleValue);
					RoleRef aRoleRef= (RoleRef)hibernate_session.get(RoleRef.class, roleValue);
					selectedRoleLabel.add(aRoleRef.getRoleName());
				}
				
				edituserform.setSelected_role_label(selectedRoleLabel);
				edituserform.setSelected_role_value(selectedRoleValue);
			}else{
				List userroles_result = hibernate_session.createQuery("select rr from RoleRef rr, UserRoles ur where ur.status = 'Active' and rr.roleStatus='Active' and ur.userId=? and ur.roleId=rr.roleId order by rr.roleId").setString(0, theUser.getUserId()).list();
				ArrayList selected_role_label = new ArrayList();
				ArrayList selected_role_value = new ArrayList();
				for (int i =0 ; i < userroles_result.size(); i ++)
				{
					RoleRef aRole= (RoleRef) userroles_result.get(i); 
					selected_role_label.add(aRole.getRoleName());
					selected_role_value.add(Integer.valueOf(aRole.getRoleId()));
					
				}
				
				edituserform.setSelected_role_label(selected_role_label);
				edituserform.setSelected_role_value(selected_role_value);
			}
			
			//		 ------------------------------ FOR User Role -------------------------------
			
			hibernate_session.getTransaction().commit();
		}
		catch (Exception E)
		{
			log4jUtil.error("EditUser System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_EDITUSERACTION,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
