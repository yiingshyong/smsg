

package com.bcb.common.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.LogonHist;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class Homecontent extends Action {
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			// Extract attributes we will need
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			
			// Select last successful
			// select last failed
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();

			List sresult = null;
			List fresult = null; 
			LogonHist slogon =null;
			LogonHist flogon =null;

			sresult = hibernate_session.createQuery
			("from LogonHist where userId = ? and logonStatus='S' order by logonKey desc").setString(0,theLogonCredential.getUserId()).setMaxResults(1).list();
			
			fresult = hibernate_session.createQuery
			("from LogonHist where userId = ? and logonStatus != 'S' order by logonKey desc").setString(0,theLogonCredential.getUserId()).setMaxResults(1).list();

			if (sresult.size() > 0 ){
				slogon = (LogonHist) sresult.get(0);
				request.setAttribute(com.bcb.common.util.Constants.SLOGON_KEY, slogon);
				
			}

			if (fresult.size() > 0 ){
				flogon = (LogonHist) fresult.get(0);
				request.setAttribute(com.bcb.common.util.Constants.FLOGON_KEY, flogon);
			}
			
			List result=DBUtilsCrsmsg.DBHQLCommand("from SmsQuota where ref=?", new Object[]{Integer.valueOf(theLogonCredential.getUserGroup())});
			
			if(result.size()==1){
				request.setAttribute(com.bcb.crsmsg.util.Constants.USER_QUOTA_KEY, result.get(0));
			}else{// error sms quota
				
			}
		
			hibernate_session.getTransaction().commit();
		}catch (Exception E){
			E.printStackTrace();
			log4jUtil.error("Homepage System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add("Homepage",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward("success"));
		
	}
	
}
