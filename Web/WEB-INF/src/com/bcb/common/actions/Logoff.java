/*
 * $Id: Logoff.java,v 1.1 2012/10/19 05:06:32 vincent Exp $ 
 *
 * Copyright 1999-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bcb.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.UserSessionListener;

/**
 * Implementation of <strong>Action</strong> that processes a
 * user logoff.
 *
 * @version $Rev: 54929 $ $Date: 2012/10/19 05:06:32 $
 */
public final class Logoff extends Action {

    // ----------------------------------------------------- Instance Variables

    
    // --------------------------------------------------------- Public Methods

        // See superclass for Javadoc
    public ActionForward execute(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response)
        throws Exception {

        // Extract attributes we will need
        HttpSession session = request.getSession();
     
      //  session.removeAttribute(Constants.SUBSCRIPTION_KEY);
      //  session.removeAttribute(Constants.USER_KEY);
      
 	   
        LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
        if ( theLogonCredential == null)
        {
        
        	ActionMessages messages = new ActionMessages();
    		messages.add("logon",new ActionMessage("errors.logon.notlogon"));
    		this.addErrors(request,messages);
    		return (mapping.findForward("notlogon"));
        };
       

        Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
        hibernate_session.beginTransaction();
        AuditTrail anAuditTrail = new AuditTrail();
        anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGOFF,"","S",theLogonCredential.getBranchCode());
    	hibernate_session.save(anAuditTrail);
        hibernate_session.getTransaction().commit();
        
        UserSessionListener.logoffSession(theLogonCredential.getUserId());
        UserSessionListener.removeSessionId(request.getSession().getId());
        
        session.invalidate();
        
          // Forward control to the specified success URI
        return (mapping.findForward("success"));

    }

}
