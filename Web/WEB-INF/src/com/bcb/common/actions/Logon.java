package com.bcb.common.actions;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.LogonForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.Branch;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.LogonHist;
import com.bcb.common.modal.PermRef;
import com.bcb.common.modal.PermRole;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.common.util.UserSessionListener;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;
public final class Logon extends Action {
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		Session hibernate_session = null;
		try{
//			StandardPBEStringEncryptor strongEncryptor = new StandardPBEStringEncryptor();
//			strongEncryptor.setAlgorithm("PBEWithMD5AndDES");
//			strongEncryptor.setPassword("mysql");
//			HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
//			registry.registerPBEStringEncryptor("hibernate.configuration.encryptor", strongEncryptor);
			LogonForm logform = (LogonForm) form;
			HttpSession session = request.getSession();
			hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			List result = hibernate_session.createQuery("from User where user_id = ?").setString(0,logform.getLogonuserid()).list();

			if ( result.isEmpty())
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.invalid.credential"));
				this.addErrors(request,messages);
				hibernate_session.getTransaction().commit();
				return (mapping.findForward("failure"));
			};
			User aUser = (User) result.get(0);
			
			LogonHist logonhist = new LogonHist ();
			logonhist.setFromIp(request.getRemoteAddr());
			logonhist.setLogonDate(new Date());
			logonhist.setLogonTime(new Date());
			logonhist.setUserId(aUser.getUserId());
			
			Branch theBranch = (Branch) hibernate_session.get(Branch.class, Integer.valueOf(aUser.getBranchDept()));
			if ( ! aUser.getUserStatus().equals("Active"))
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.disabled"));
				this.addErrors(request,messages);
				logonhist.setLogonStatus("FS");
				if (aUser.getFailedLogon() != null)
					aUser.setFailedLogon(Integer.valueOf(aUser.getFailedLogon().intValue() + 1));
				else
					aUser.setFailedLogon(new Integer(1) );
				hibernate_session.save(aUser);
				hibernate_session.save(logonhist);
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"NOT ACTIVE","F",theBranch.getBranchCode());
				hibernate_session.save(anAuditTrail);
				
				hibernate_session.getTransaction().commit();
				
				return (mapping.findForward("failure"));
			};
			if ( aUser.getFailedLogon() != null)
			{
				if ( aUser.getFailedLogon().intValue() > 3 - 1)
				{
					ActionMessages messages = new ActionMessages();
					messages.add("logon",new ActionMessage("errors.logon.suspend"));
					this.addErrors(request,messages);
					logonhist.setLogonStatus("FP");
					if (aUser.getFailedLogon() != null)
						aUser.setFailedLogon(Integer.valueOf(aUser.getFailedLogon().intValue() + 1));
					else
						aUser.setFailedLogon(new Integer(1) );
					hibernate_session.save(aUser);
					hibernate_session.save(logonhist);
					AuditTrail anAuditTrail = new AuditTrail();
					anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"ID SUSPENDED","F",theBranch.getBranchCode());
					hibernate_session.save(anAuditTrail);
					hibernate_session.getTransaction().commit();
					return (mapping.findForward("failure"));
					
				};
			};
			//Account Dormancy Check
			List sysCfg = hibernate_session.createQuery("from SmsCfg").setMaxResults(1).list();
			
			String accountDormancyCheckStr = null;
			int accountDormancyCheck = 0;
			Calendar dormancyCalendar =  Calendar.getInstance();
			//Check last login date, else check created date
			if ( aUser.getLastLoginDate() != null)
				dormancyCalendar.setTime(aUser.getLastLoginDate());
			else
				dormancyCalendar.setTime(aUser.getCreatedDate());

			//Account dormancy check not applied to admin id **added at 20131210**
			if(sysCfg.size() > 0 && !DaoBeanFactory.getUserDao().hasPermission(logform.getLogonuserid(), Integer.valueOf(Constants.PERM_ADMIN_ID))) {
				accountDormancyCheckStr = ((SmsCfg) sysCfg.get(0)).getAccountDormancyCheck();
				
				if(accountDormancyCheckStr != null && accountDormancyCheckStr.length()>0&&accountDormancyCheckStr.matches("[0-9]*")) {
					accountDormancyCheck = Integer.valueOf(accountDormancyCheckStr);
					
					if(accountDormancyCheck > 0) {
						dormancyCalendar.add(Calendar.DATE, accountDormancyCheck);
						
						if (new Date().after(dormancyCalendar.getTime()) == true)
						{
							ActionMessages messages = new ActionMessages();
							messages.add("logon",new ActionMessage("errors.logon.suspend"));
							this.addErrors(request,messages);
							logonhist.setLogonStatus("FD");
							
							aUser.setUserStatus(com.bcb.common.util.Constants.STATUS_DISABLED_LABEL);
							aUser.setModifiedBy("Logon");
							aUser.setModifiedDate(new Date());
							aUser.setModifiedTime(new Date());
							aUser.setLastLoginDate(new Date());
							hibernate_session.update(aUser);
							hibernate_session.save(logonhist);
							
							AuditTrail anAuditTrail = new AuditTrail();
							anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"ID SUSPENDED","F",theBranch.getBranchCode());
							hibernate_session.save(anAuditTrail);
							hibernate_session.getTransaction().commit();
							return (mapping.findForward("failure"));
						}
					}
				}
			}
			String passwrd = aUser.getPassword();
			String logonPassword = logform.getLogonpassword();
	        	
//			if(session.getAttribute(com.bcb.common.util.Constants.AUTO_LOGON)==null){
//				if (SecurityManager.getInstance().encryptSHA2(logonPassword).compareTo(passwrd) != 0 && SHAUtil.encrypt(logonPassword).compareTo(passwrd) != 0)
//				{
//					ActionMessages messages = new ActionMessages();
//					messages.add("logon",new ActionMessage("errors.logon.invalid.credential"));
//	
//					this.addErrors(request,messages);
//					logonhist.setLogonStatus("FP");
//					if (aUser.getFailedLogon() != null)
//						aUser.setFailedLogon(Integer.valueOf(aUser.getFailedLogon().intValue() + 1));
//					else
//						aUser.setFailedLogon(new Integer(1) );
//					hibernate_session.save(aUser);
//					hibernate_session.save(logonhist);
//					AuditTrail anAuditTrail = new AuditTrail();
//					anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"INVALID PWD","F",theBranch.getBranchCode());
//					hibernate_session.save(anAuditTrail);
//					hibernate_session.getTransaction().commit();
//					
//					return (mapping.findForward("failure"));
//				};
//			}
			LogonCredential aLogonCredential = new LogonCredential();
			aLogonCredential.setUserId(aUser.getUserId());
			aLogonCredential.setUserName(aUser.getUserName());
			aLogonCredential.setBranchCode(theBranch.getBranchCode());
			aLogonCredential.setUserGroup(Integer.toString(aUser.getUserGroup()));
			// get user id to map with role
			List roles_list = hibernate_session.createQuery("from UserRoles where userId = ? and status='Active'").setString(0,aUser.getUserId()).list();
			// get roles for user into array
			RoleRef aRef=null;
			StringBuffer menuHql=new StringBuffer();
			menuHql.append("select distinct (m) from Menu m, RoleApps u where m.appsId = u.appsId");
			menuHql.append(" and u.roleId in ( ");
			
			for(int i=0;i<roles_list.size();i++){
				aRef = (RoleRef) hibernate_session.get(RoleRef.class, Integer.valueOf(((UserRoles)roles_list.get(i)).getRoleId()));
				Set oldSet = aRef.getPermRoles();
				
				// Check if user is View Only Group
				if(aRef.getRoleName().equalsIgnoreCase(com.bcb.common.util.Constants.VIEW_ONLY_ROLE_NAME)){
					session.setAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY, com.bcb.common.util.Constants.VIEW_ONLY_ROLE_NAME);
				}
				if(aRef.getRoleName().equalsIgnoreCase(com.bcb.common.util.Constants.ROLE_OPERATOR)){
					session.setAttribute(com.bcb.common.util.Constants.ROLE_OPERATOR_KEY, com.bcb.common.util.Constants.ROLE_OPERATOR);
				}
				
				Iterator it = oldSet.iterator();
				
				while (it.hasNext()) {
					// Get element
					PermRole element = (PermRole) it.next();
					PermRef aPermRef = (PermRef) hibernate_session.get(PermRef.class,Integer.valueOf(element.getPermRef()));
					aLogonCredential.addCredential(Integer.toString(aPermRef.getAppsId()),Integer.toString(aPermRef.getPermValue()));
				}
				
				menuHql.append(Integer.valueOf(((UserRoles)roles_list.get(i)).getRoleId()));
				if(i+1<roles_list.size()){
					menuHql.append(", ");
				}
			}
			menuHql.append(" ) and m.Show = 'Y' order by m.showSequence,m.appsId,m.menuId");
			List menu_list=hibernate_session.createQuery(menuHql.toString()).list();
//			 get user id to map with role
			aLogonCredential.setRights((ArrayList<String>)DBUtilsCrsmsg.DBHQLCommand("select rightsId from UserRights where user.userId = ?", new Object[]{aUser.getUserId()}));
			session.setAttribute(com.bcb.common.util.Constants.MENU_KEY,menu_list);
			session.setAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY,aLogonCredential);
			logonhist.setLogonStatus("S");
			session.setMaxInactiveInterval(600);
			if(UserSessionListener.checkLoginSession(aUser.getUserId()))
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.alreadylogon"));
				this.addErrors(request,messages);
				hibernate_session.getTransaction().commit();
				return (mapping.findForward("failure"));
			}else{
		        session = request.getSession();
		        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
		        String storedToken = (String)session.getAttribute("csrfToken");
		        System.out.println("salt : "+salt);
	        	session.setAttribute("csrfToken", salt);
			}
			List sresult = hibernate_session.createQuery
			("from LogonHist where userId = ? and logonStatus='S' order by logonKey desc").setString(0,aUser.getUserId()).list();
			if ( sresult.size() == 0)
			{
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"FORCE CHG PWD","S",theBranch.getBranchCode());
				hibernate_session.save(anAuditTrail);
				hibernate_session.getTransaction().commit();
				return (mapping.findForward("forcechgpwd")); 
			};	
			aUser.setFailedLogon(new Integer(0));
			Calendar aCalendar =  Calendar.getInstance();
			// Backward compatibility - if it is null,set it to today
			if ( aUser.getLastPwdchg() == null)
				aUser.setLastPwdchg(new Date());
			hibernate_session.save(aUser);
			aCalendar.setTime(aUser.getLastPwdchg());
			/*
			Control theControl = (Control) hibernate_session.get(Control.class, Integer.valueOf(0));
			
			String pwdcycle = theControl.getPasswordCycle();
			
			String[] pwdcycle_com =  pwdcycle.split(",");
			int pwdcycle_int = Integer.valueOf(pwdcycle_com[0]).intValue();
			if ( pwdcycle_com[1].compareToIgnoreCase("M") == 0 )
			{
				
				aCalendar.add(Calendar.MONTH,pwdcycle_int);
				if (new Date().after(aCalendar.getTime()) == true)
				{
					AuditTrail anAuditTrail = new AuditTrail();
					anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"FORCE CHG PWD","S",theBranch.getBranchCode());
					hibernate_session.save(anAuditTrail);
					hibernate_session.getTransaction().commit();
					ActionMessages messages = new ActionMessages();
					messages.add("Logon",new ActionMessage("errors.user.password.expired"));
					this.addErrors(request,messages);
					return (mapping.findForward("forcechgpwd")); 
				};
			}
			else
				if ( pwdcycle_com[1].compareToIgnoreCase("D") == 0 )
				{
					aCalendar.add(Calendar.DATE,pwdcycle_int);
					if (new Date().after(aCalendar.getTime()) == true)
					{
						AuditTrail anAuditTrail = new AuditTrail();
						anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"FORCE CHG PWD","S",theBranch.getBranchCode());
						hibernate_session.save(anAuditTrail);
						hibernate_session.getTransaction().commit();
						ActionMessages messages = new ActionMessages();
						messages.add("Logon",new ActionMessage("errors.user.password.expired"));
						this.addErrors(request,messages);
						return (mapping.findForward("forcechgpwd")); 
					};
				};*/
			String passwordChangeDurationStr = null;
			int passwordChangeDuration = 0;
			
			if(sysCfg.size() > 0) {
				passwordChangeDurationStr = ((SmsCfg) sysCfg.get(0)).getPasswordChangeDuration();

				if(passwordChangeDurationStr != null && passwordChangeDurationStr.length()>0&&passwordChangeDurationStr.matches("[0-9]*")) {
					passwordChangeDuration = Integer.valueOf(passwordChangeDurationStr);
					
					if(passwordChangeDuration > 0) {
						aCalendar.add(Calendar.DATE, passwordChangeDuration);
						if (new Date().after(aCalendar.getTime()) == true)
						{
							AuditTrail anAuditTrail = new AuditTrail();
							anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"FORCE CHG PWD","S",theBranch.getBranchCode());
							hibernate_session.save(anAuditTrail);
							hibernate_session.getTransaction().commit();
							ActionMessages messages = new ActionMessages();
							messages.add("Logon",new ActionMessage("errors.user.password.expired"));
							this.addErrors(request,messages);
							return (mapping.findForward("forcechgpwd")); 
						}
					}
				}
			}			
				if(aUser.getResetStatus() != null && aUser.getResetStatus().equals("Y")){
					AuditTrail anAuditTrail = new AuditTrail();
					anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"FORCE CHG PWD","S",theBranch.getBranchCode());
					hibernate_session.save(anAuditTrail);
					hibernate_session.getTransaction().commit();
					ActionMessages messages = new ActionMessages();
					messages.add("Logon",new ActionMessage("errors.user.password.expired"));
					this.addErrors(request,messages);
					return (mapping.findForward("forcechgpwd")); 
				}
			//				Check if it is expired

			AuditTrail anAuditTrail = new AuditTrail();
			anAuditTrail.setAudit(aUser.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.SYS_LOGON,"","S",theBranch.getBranchCode());
			hibernate_session.save(anAuditTrail);

			aUser.setLastLoginDate(new Date());

			hibernate_session.save(aUser);					
			hibernate_session.save(logonhist);
			//				Should not need to save	hibernate_session.save(aUser);

			hibernate_session.getTransaction().commit();
			//smsgha-phase3: To renew session token after login
			Enumeration<String> attrNames = request.getSession().getAttributeNames();
			Map<String, Object> newSessionAttrs = new HashMap<String, Object>();
			while(attrNames.hasMoreElements()){
				String name = attrNames.nextElement();
				newSessionAttrs.put(name, request.getSession().getAttribute(name));
			}
			request.getSession().invalidate();
			request.getSession(true);
			for(String key : newSessionAttrs.keySet()){
				request.getSession().setAttribute(key, newSessionAttrs.get(key));
			}
			//end renew session token
			UserSessionListener.storeSessionId(aUser.getUserId(), request.getSession().getId());
		}
		catch (Exception e){
			log4jUtil.error("Unexpected error while login user. Reason:" + e.getMessage(), e);
			ActionMessages messages = new ActionMessages();
			messages.add("Logon",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
			return (mapping.findForward("failure"));
		}finally{
			if (hibernate_session != null){
				if (hibernate_session.isOpen()){
					hibernate_session.close();
				}
			}
		}
		return (mapping.findForward("success"));
	}
	
}
