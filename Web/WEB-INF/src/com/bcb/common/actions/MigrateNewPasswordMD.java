package com.bcb.common.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionRedirect;

import com.bcb.common.forms.MigrateUserPasswordForm;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class MigrateNewPasswordMD extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		MigrateUserPasswordForm pwform = (MigrateUserPasswordForm) form;
		User user = null;			
		if(!mapping.getParameter().equals("load") && StringUtils.isNotEmpty(pwform.getUserId())){
			user = DaoBeanFactory.getUserDao().findByUserId(pwform.getUserId());
			user.setPassword(SecurityManager.getInstance().encryptSHA2(pwform.getNewpassword1()));
			if(StringUtils.isEmpty(user.getPwdHistory())){
				user.setPwdHistory(user.getPassword());
			}else{
				user.setPwdHistory(user.getPassword() + com.bcb.common.util.Constants.PWD_HISTORY + user.getPwdHistory());
			}	
			ActionMessages messages = new ActionMessages();
            messages.add("Migrate Password",new ActionMessage("Password Successfully Changed", false));
            this.addErrors(request,messages);			
            DaoBeanFactory.getHibernateTemplate().update(user);
            
            ActionRedirect redirect = new ActionRedirect(mapping.findForward("login"));
            redirect.addParameter("logonuserid", pwform.getUserId());
            redirect.addParameter("logonpassword", pwform.getNewpassword1());
            return redirect;
		}		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}

	
}
