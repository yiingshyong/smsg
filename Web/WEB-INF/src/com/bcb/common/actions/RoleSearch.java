

package com.bcb.common.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.RoleSearchForm;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class RoleSearch extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try
		{
			// Extract attributes we will need
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			
			if ( theLogonCredential.allowed(com.bcb.common.util.Constants.APPSID,com.bcb.common.util.Constants.ROLEMGMT_PERM) == false)
			{
				return (mapping.findForward("accdeny"));
				
			};
			
			RoleSearchForm esform =  (RoleSearchForm) form;
			if ( esform == null)
			{
			}
			else
				if ( esform.getSelectedState().equals("RoleName"))
				{
					Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
					
					hibernate_session.beginTransaction();
					
					String orderBy = " order by roleName desc";
					List result = null; 
					
					// By Ref ID
					
					result = hibernate_session.createQuery
					("from RoleRef where roleName like ?" + orderBy).setString(0,esform.getSearchkey() + "%").list();
					request.setAttribute(Constants.ROLE_KEY, result);
					hibernate_session.getTransaction().commit();
				};
				
		}
		catch (Exception E)
		{
		log4jUtil.error("RoleSearch System Error[" + E.getMessage()+ "]");
		ActionMessages messages = new ActionMessages();
		messages.add("RoleSearch",new ActionMessage("errors.system"));
		this.addErrors(request,messages);
		};
		
		return (mapping.findForward("success"));
		
	}
	
}
