

package com.bcb.common.actions;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.DeptForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQuota;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public final class SaveAddDept extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		Session hibernate_session = null;

		try {
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			}
			
			//View Only User Role
			if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
				return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
			}
			
			hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			String logonuser=theLogonCredential.getUserId();
			DeptForm editdeptform = (DeptForm) form;
			
			Department theDept = new Department();
			theDept.setDeptName(editdeptform.getDeptname());
			theDept.setDeptDescription(editdeptform.getDeptdescription());
			theDept.setDeptLeader(editdeptform.getSelected_deptleader());
			theDept.setDeptStatus(editdeptform.getSelected_deptstatus());
			theDept.setCreatedBy(logonuser);
			theDept.setCreatedDate(new Date());
			theDept.setCreatedTime(new Date());
//			theDept.setSmsType(Integer.parseInt(editdeptform.getSelectedSmsType()));
			if(DBUtilsCrsmsg.saveOrUpdate(theDept, hibernate_session)){
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.DEPT_ADD,editdeptform.getDeptname() ,"S",theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
	
				List result=DBUtilsCrsmsg.DBHQLCommand("from SmsCfg", hibernate_session);
				if(result.size()==1){
					SmsCfg theCfg=(SmsCfg)result.get(0);
					
					SmsQuota aQuota = new SmsQuota();
					aQuota.setCreatedBy(theLogonCredential.getUserId());
					aQuota.setCreatedDatetime(new Date());
					if (theCfg.getSmsQuotaDefault().length() > 1){
						aQuota.setQuota(Integer.valueOf(theCfg.getSmsQuotaDefault()));
					}
					aQuota.setRef(theDept.getId());
					aQuota.setTotalQuota(aQuota.getQuota());
					
					DBUtilsCrsmsg.saveOrUpdate(aQuota, hibernate_session);
				}else{
				}
				
				ActionMessages messages = new ActionMessages();
				messages.add("SaveAddDept",new ActionMessage("errors.group.save.success"));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add("SaveAddDept",new ActionMessage("errors.group.save.fail"));
				this.addErrors(request,messages);
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error("SaveAddDept System Error[" + E.getMessage()+ "]");
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			ActionMessages messages = new ActionMessages();
			messages.add("SaveAddDept",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
