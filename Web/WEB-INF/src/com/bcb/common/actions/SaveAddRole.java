package com.bcb.common.actions;
import java.util.Date;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.RoleForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.PermRole;
import com.bcb.common.modal.RoleApps;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;

public final class SaveAddRole extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try{
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			
			//View Only User Role
			if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
				return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
			}
			
			String logonuser=theLogonCredential.getUserId();
			RoleForm roleform = (RoleForm) form;

			// ADD Comes here
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			RoleRef sm = new RoleRef();
			sm.setRoleName(roleform.getRolename());
			sm.setRoleDescription(roleform.getRoledescription());
			sm.setRoleStatus(roleform.getSelected_userstatus());
			sm.setCreatedBy(logonuser);
			sm.setCreatedDate(new Date());
			sm.setCreatedTime(new Date());
			
//			sm = (RoleRef) hibernate_session.load(RoleRef.class, );
			
			String[] checked = roleform.getPermroles();
			HashSet permroles = new HashSet();
			sm.setPermRoles(permroles);
			for ( int i = 0 ; checked!=null&&i < checked.length ;  i ++)
			{
				PermRole aPermRole = new PermRole();
				aPermRole.setPermRef(new Integer (checked[i]).intValue());
				aPermRole.setRoleRef(sm);
				
				sm.getPermRoles().add(aPermRole);
			}
			hibernate_session.saveOrUpdate(sm);
			hibernate_session.flush(); 
			
			hibernate_session.refresh(sm);
			roleform.setRoleid(Integer.valueOf(sm.getRoleId()));
			// flow thru the list lor
			String[] appsChecked = roleform.getUserapps();
			for ( int i = 0 ; appsChecked!=null&&i < appsChecked.length ;  i ++)
			{
				RoleApps aRoleApps  = new RoleApps();
				aRoleApps.setAppsId(new Integer (appsChecked[i]).intValue());
				aRoleApps.setRoleId(sm.getRoleId());
				hibernate_session.save(aRoleApps);
			}
			
			AuditTrail anAuditTrail = new AuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.ROLE_ADD,sm.getRoleName(),"S",theLogonCredential.getBranchCode());
			hibernate_session.save(anAuditTrail);
			hibernate_session.getTransaction().commit();
			
			ActionMessages messages = new ActionMessages();
			messages.add("SaveAddRole",new ActionMessage("errors.role.save.success"));
			this.addErrors(request,messages);
		}catch (Exception E){
			log4jUtil.error("SaveRole System Error[" + E.getMessage()+ "]"  );
			E.printStackTrace();
			
			ActionMessages messages = new ActionMessages();
			messages.add("SaveRole",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward("success"));
	}
}
