

package com.bcb.common.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.UserForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.util.SecurityManager;
public final class SaveAddUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		};
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			
			
			hibernate_session.beginTransaction();
			
			String logonuser=theLogonCredential.getUserId();
			UserForm edituserform = (UserForm) form;
			User aUser=null;
			aUser= (User) hibernate_session.get(User.class, edituserform.getUserid());
			if ( aUser != null )
			{
				ActionMessages messages = new ActionMessages();
				messages.add("usermgmt",new ActionMessage(Constants.ERRMSG_USERALREADYEXIST));
				this.addErrors(request,messages);
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ADD,edituserform.getUserid().trim() + " Exist","F",theLogonCredential.getBranchCode());
				hibernate_session.save(anAuditTrail);
				hibernate_session.getTransaction().commit();    	
			}
			else
			{
				User theUser = new User();
				theUser.setUserId(edituserform.getUserid());
				theUser.setBizPhone(edituserform.getBizphone());
				theUser.setBranchDept(1);//edituserform.getSelected_branch().intValue());
				theUser.setEmailId(edituserform.getEmailid());
				theUser.setExt(edituserform.getExt());
				theUser.setHPhone(edituserform.getHphone());
				
				//theUser.setRole(edituserform.getSelected_role().intValue());
				String selectedRoles =edituserform.getStrSelected_roles();
				
				if (selectedRoles !=null && selectedRoles.length()> 0){
					String[] rolesArray = selectedRoles.split(",");

					for(int i=0;i<rolesArray.length;i++){
						hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
						hibernate_session.beginTransaction();
						
						Integer roleValue=Integer.valueOf(rolesArray[i].toString());
						UserRoles aUserRole = new UserRoles();
						aUserRole.setUserId(edituserform.getUserid());
						aUserRole.setRoleId(roleValue);
						aUserRole.setStatus(Constants.STR_ACTIVE);
						aUserRole.setCreatedBy(logonuser);
						aUserRole.setCreatedDate(new Date());
						aUserRole.setCreatedTime(new Date());
						
						hibernate_session.save(aUserRole);
						
						AuditTrail anAuditTrail = new AuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ROLE_ADD,Integer.toString(aUserRole.getKeyCol()) , Constants.STR_ACTION_SUCCESS ,theLogonCredential.getBranchCode());
						hibernate_session.save(anAuditTrail);

						hibernate_session.getTransaction().commit();
					}
				}

				hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				hibernate_session.beginTransaction();
				
				theUser.setUserGroup(edituserform.getSelected_group().intValue());
				theUser.setUserName(edituserform.getUsername());
				theUser.setUserStatus(edituserform.getSelected_userstatus());
				theUser.setStaffId(edituserform.getStaffid());
				if ( edituserform.getPassword() != null && edituserform.getPassword().length() > 1 )
				{
					theUser.setPassword(SecurityManager.getInstance().encryptSHA2(edituserform.getPassword()));
				};
				
				theUser.setCreatedBy(logonuser);
				theUser.setCreatedDate(new Date());
				theUser.setCreatedTime(new Date());
				//theUser.setCicsGroupId(edituserform.getCicsGroupSelect().toString());
				
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ADD,edituserform.getUserid() ,"S",theLogonCredential.getBranchCode());
				hibernate_session.save(anAuditTrail);
				
				hibernate_session.save(theUser);
				hibernate_session.getTransaction().commit();
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SAVEADDUSERACTION,new ActionMessage(Constants.ERRMSG_USERSAVESUCCESS));
				this.addErrors(request,messages);
			};  
			
		}
		catch (Exception E)
		{
			log4jUtil.error("SaveAddUser System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SAVEADDUSERACTION,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
