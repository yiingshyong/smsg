

package com.bcb.common.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.DeptForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class SaveEditDept extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			
			//View Only User Role
			if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
				return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
			}
			
			String logonuser=theLogonCredential.getUserId();
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			DeptForm editgroupform = (DeptForm) form;
			List group_list=hibernate_session.createQuery("from Department where id = ?").setInteger(0, Integer.parseInt(editgroupform.getDeptid())).list();
			Department theGroup= (Department) group_list.get(0);
			
			theGroup.setDeptName(editgroupform.getDeptname());
			theGroup.setDeptDescription(editgroupform.getDeptdescription());
			theGroup.setDeptLeader(editgroupform.getSelected_deptleader());
			theGroup.setDeptStatus(editgroupform.getSelected_deptstatus());
			theGroup.setModifiedBy(logonuser);
			theGroup.setModifiedDate(new Date());
			theGroup.setModifiedTime(new Date());
//			theGroup.setSmsType(Integer.parseInt(editgroupform.getSelectedSmsType()));
			hibernate_session.saveOrUpdate(theGroup);

			ArrayList status_label = new ArrayList();
			status_label.add("Active");
			status_label.add("Closed");
			editgroupform.setDeptstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			status_value.add("Active");
			status_value.add("Closed");
			editgroupform.setDeptstatus_value(status_value);
			
			AuditTrail anAuditTrail = new AuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.DEPT_EDIT,theGroup.getDeptName() ,"S",theLogonCredential.getBranchCode());
			hibernate_session.save(anAuditTrail);

			hibernate_session.getTransaction().commit();
			
			ActionMessages messages = new ActionMessages();
			messages.add("SaveEditDept",new ActionMessage("errors.group.save.success"));
			this.addErrors(request,messages);
		}
		catch (Exception E)
		{
			log4jUtil.error("SaveEditDept System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("SaveEditDept",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
