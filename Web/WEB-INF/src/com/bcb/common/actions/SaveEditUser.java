

package com.bcb.common.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.UserForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.util.SecurityManager;
public final class SaveEditUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			
			String logonuser=theLogonCredential.getUserId();
			
			hibernate_session.beginTransaction();
			UserForm edituserform = (UserForm) form;
			User theUser= (User) hibernate_session.get(User.class, edituserform.getUserid());
			
			theUser.setStaffId(edituserform.getStaffid());
			theUser.setBizPhone(edituserform.getBizphone());
			theUser.setBranchDept(1);//edituserform.getSelected_branch().intValue());
			theUser.setEmailId(edituserform.getEmailid());
			theUser.setHPhone(edituserform.getHphone());
			theUser.setExt(edituserform.getExt());

			//theUser.setRole(edituserform.getSelected_role().intValue());
			String selectedRoles =edituserform.getStrSelected_roles();
			
			if (selectedRoles !=null && selectedRoles.length()> 0){
				ArrayList selectedRolesArray=new ArrayList();
				String[] rolesArray = selectedRoles.split(",");
				List rolesList=hibernate_session.createQuery("from UserRoles where userId=?").setString(0, edituserform.getUserid()).list();
				ArrayList roleIdList=new ArrayList();
				
				for(int i=0;i<rolesArray.length;i++){
					selectedRolesArray.add(Integer.parseInt(rolesArray[i]));
				}

				for(int i=0;i<rolesList.size();i++){
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
					hibernate_session.beginTransaction();
					
					UserRoles aUserRole = (UserRoles)rolesList.get(i);
					roleIdList.add(aUserRole.getRoleId());
					
					if(selectedRolesArray.contains(aUserRole.getRoleId())){
						aUserRole.setStatus(Constants.STR_ACTIVE);
					}else{
						aUserRole.setStatus(Constants.STR_CLOSED);
					}
					aUserRole.setModifiedBy(logonuser);
					aUserRole.setModifiedDate(new Date());
					aUserRole.setModifiedTime(new Date());
					hibernate_session.saveOrUpdate(aUserRole);
					
					AuditTrail anAuditTrail = new AuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ROLE_EDIT,Integer.toString(aUserRole.getKeyCol()) , Constants.STR_ACTION_SUCCESS ,theLogonCredential.getBranchCode());
					hibernate_session.save(anAuditTrail);
					
					hibernate_session.getTransaction().commit();
				}
				
				selectedRolesArray.removeAll(roleIdList); // new item
				
				for(int i=0;i<selectedRolesArray.size();i++){
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
					hibernate_session.beginTransaction();
					
					UserRoles aUserRole = new UserRoles();
					aUserRole.setUserId(edituserform.getUserid());
					aUserRole.setRoleId(Integer.valueOf(selectedRolesArray.get(i).toString()));
					aUserRole.setStatus(Constants.STR_ACTIVE);
					aUserRole.setCreatedBy(logonuser);
					aUserRole.setCreatedDate(new Date());
					aUserRole.setCreatedTime(new Date());

					hibernate_session.saveOrUpdate(aUserRole);
					
					AuditTrail anAuditTrail = new AuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ROLE_ADD,Integer.toString(aUserRole.getKeyCol()) , Constants.STR_ACTION_SUCCESS ,theLogonCredential.getBranchCode());
					hibernate_session.save(anAuditTrail);
					
					hibernate_session.getTransaction().commit();
				}
			}
			
			hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			theUser.setUserGroup(edituserform.getSelected_group().intValue());
			theUser.setUserName(edituserform.getUsername());
			theUser.setUserStatus(edituserform.getSelected_userstatus());
			//theUser.setCompanyId(Integer.valueOf(edituserform.getSelected_company()));
			
			theUser.setFailedLogon(Integer.valueOf(edituserform.getFailedlogon())); 
			
			if(edituserform.getPassword().length() > 0){
				 theUser.setResetStatus("Y");				 
			 }

			if ( edituserform.getPassword() != null && edituserform.getPassword().length() > 1 )
			{
				theUser.setPassword(SecurityManager.getInstance().encryptSHA2(edituserform.getPassword()));
			};
			
			theUser.setModifiedBy(logonuser);
			theUser.setModifiedDate(new Date());
			theUser.setModifiedTime(new Date());
			//theUser.setCicsGroupId(edituserform.getCicsGroupSelect().toString());
			//theUser.setSbu(edituserform.getSelected_sbu());
			
			hibernate_session.saveOrUpdate(theUser);
			
			AuditTrail anAuditTrail = new AuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_EDIT,theUser.getUserId() ,"S",theLogonCredential.getBranchCode());
			hibernate_session.save(anAuditTrail);
			hibernate_session.getTransaction().commit();
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SAVEEDITUSERACTION,new ActionMessage(Constants.ERRMSG_USERSAVESUCCESS));
			this.addErrors(request,messages);
			
			
		}
		catch (Exception E)
		{
			log4jUtil.error("SaveEditUser System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SAVEEDITUSERACTION,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
