

package com.bcb.common.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.ChangePasswordForm;
import com.bcb.common.modal.AuditTrail;
import com.bcb.common.modal.ChgPwdHist;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.LogonHist;
import com.bcb.common.modal.User;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.util.SecurityManager;
public final class SaveForceChangePassword extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try {
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				ActionMessages messages = new ActionMessages();
				messages.add("logon",new ActionMessage("errors.logon.notlogon"));
				this.addErrors(request,messages);
				return (mapping.findForward("notlogon"));
				
			};
			String logonuser=theLogonCredential.getUserId();
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			ChangePasswordForm pwform = (ChangePasswordForm) form;
			User theUser= (User) hibernate_session.get(User.class, logonuser);
			if ( SecurityManager.getInstance().encryptSHA2(pwform.getOldpassword()).compareTo(theUser.getPassword()) != 0 && 
					SHAUtil.encrypt(pwform.getOldpassword()).compareTo(theUser.getPassword()) != 0 		
			)
			{
				AuditTrail anAuditTrail = new AuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.PWD_CHG,theUser.getUserId().trim() + " WRONG" ,"F",theLogonCredential.getBranchCode());
				hibernate_session.save(anAuditTrail);
				ActionMessages messages = new ActionMessages();
				messages.add("forcechgpwd",new ActionMessage("errors.logon.password"));
				this.addErrors(request,messages);
				return (mapping.findForward("success"));
			};
			
			  String genHist = "";
			  if (theUser.getPwdHistory()!= null && theUser.getPwdHistory().length()>0){	
				  String getHistory = theUser.getPwdHistory(); 		
				  String[] splitData = getHistory.split("\\"+com.bcb.common.util.Constants.PWD_HISTORY);		  
				  if(splitData.length > 0){
					   for(int s=0; s < splitData.length ; s++ ){
						  String getData = splitData[s] ; //a|b|c|d|e 
						  if (SecurityManager.getInstance().encryptSHA2(pwform.getNewpassword1()).compareTo(getData) == 0){ 
							  	ActionMessages messages = new ActionMessages();
								messages.add("chgpwd",new ActionMessage("errors.chgpwd.passwordhistory"));
								this.addErrors(request,messages);
								return (mapping.findForward("success"));
						  }
						  if(s < 4){
							  genHist = genHist+"|"+getData; //|a|b|c|d|e						  
						  }					 
					  }	 genHist=genHist.substring(1, genHist.length());	
				  }	
				  theUser.setPwdHistory(SecurityManager.getInstance().encryptSHA2(pwform.getNewpassword1())+com.bcb.common.util.Constants.PWD_HISTORY+genHist);
			  }else{ 

				  theUser.setPwdHistory(SecurityManager.getInstance().encryptSHA2(pwform.getNewpassword1())+com.bcb.common.util.Constants.PWD_HISTORY+SecurityManager.getInstance().encryptSHA2(pwform.getOldpassword())); 
			  }	 
			  
			if(theUser.getResetStatus() != null){
				if(theUser.getResetStatus().equals("Y")){
					theUser.setResetStatus("");  
				}
			}
			
			theUser.setPassword(SecurityManager.getInstance().encryptSHA2(pwform.getNewpassword1()));   
			theUser.setModifiedBy(logonuser);
			theUser.setModifiedDate(new Date());
			theUser.setModifiedTime(new Date());
			theUser.setLastPwdchg(new Date());
			theUser.setLastLoginDate(new Date());
			
			ChgPwdHist aChgPwdHist = new ChgPwdHist();
			aChgPwdHist.setUserId(theUser.getUserId());
			aChgPwdHist.setChgpwdDate(new Date());
			aChgPwdHist.setChgpwdTime(new Date());
			aChgPwdHist.setFromIp(request.getRemoteAddr());
			hibernate_session.save(aChgPwdHist);
			
			hibernate_session.save(theUser);
			LogonHist logonhist = new LogonHist ();
			logonhist.setFromIp(request.getRemoteAddr());
			logonhist.setLogonDate(new Date());
			logonhist.setLogonTime(new Date());
			logonhist.setUserId(logonuser);
			logonhist.setLogonStatus("S");
			hibernate_session.save(logonhist);
			AuditTrail anAuditTrail = new AuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.PWD_CHG,theUser.getUserId() ,"S",theLogonCredential.getBranchCode());
			hibernate_session.save(anAuditTrail);
			
			hibernate_session.getTransaction().commit();
			ActionMessages messages = new ActionMessages();
			messages.add("SaveForceChangePassword",new ActionMessage("errors.chgpwd.save.success"));
			this.addErrors(request,messages);
			
			
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error("SaveForceChangePassword System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add("SaveForceChangePassword",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
		
	}
}
