

package com.bcb.common.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.forms.UserSearchForm;
import com.bcb.common.modal.Apps;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
public final class UserSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.common.util.Constants.APPSID,com.bcb.common.util.Constants.USERMGMT_PERM) == false)
		{
			
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
			
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		hibernate_session.beginTransaction();
		
		try
		{
			// Extract attributes we will need
			
			UserSearchForm esform =  (UserSearchForm) form;
			
			List result = null; 
			ArrayList properResult = new ArrayList();
			
			if(mapping.getParameter().equals("load")){
				
				if(esform.getByApps_label()==null){
					List appsList = hibernate_session.createQuery("from Apps order by appsName asc").list();
					ArrayList appsListValue = new ArrayList();
					ArrayList appsListLabel = new ArrayList();
					
					appsListValue.add(Integer.valueOf(Constants.STRING_ZERO).intValue());
					appsListLabel.add(Constants.STRING_ALL);
					
					if(!appsList.isEmpty()){
						for (int i =0 ; i < appsList.size(); i ++){
							appsListValue.add(((Apps)appsList.get(i)).getAppsId());
							appsListLabel.add(((Apps)appsList.get(i)).getAppsName());
							esform.setByApps_label(appsListLabel);
							esform.setByApps_value(appsListValue);
						}
					}
					
				}
				
			}else{
				
				Integer appsId = 0;
				
				if(!esform.getSelectedApps().equals(Constants.STR_EMPTYSTRING)){
					appsId = Integer.valueOf(esform.getSelectedApps());
				}
				
				if ( esform.getSelectedState().equals(Constants.STR_USERID))
				{
					// By Ref ID
					
					if(esform.getSelectedStatus().equals(Constants.STR_ALL)){
						if(appsId==0){
							result = hibernate_session.createQuery
							("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userId like ? " +
									"and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
							"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey() + Constants.STR_PERCENT)
							.list();
						}else{
							result = hibernate_session.createQuery
							("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userId like ? " +
									"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
							"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey() + Constants.STR_PERCENT)
							.setInteger(1,appsId).list();
						}
					}else{
						if(appsId==0){
							result = hibernate_session.createQuery
							("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userId like ? " +
									"and u.branchDept = b.branchId and ur.roleId = ra.roleId and u.userStatus = ? and ur.userId=u.userId " +
							"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode")
							.setString(0,esform.getSearchkey() + Constants.STR_PERCENT).setString(1, esform.getSelectedStatus()).list();
						}else{
							result = hibernate_session.createQuery
							("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userId like ? " +
									"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and u.userStatus = ? and ur.userId=u.userId " +
							"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode")
							.setString(0,esform.getSearchkey() + Constants.STR_PERCENT).setInteger(1,appsId).setString(2, esform.getSelectedStatus()).list();
						} 
					}
				}
				else
					if (esform.getSelectedState().equals(Constants.STR_USERNAME))
					{
						// By Customer Name
						
						if(esform.getSelectedStatus().equals(Constants.STR_ALL)){	
							if(appsId==0){
								result = hibernate_session.createQuery
								("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userName like ? " +
										"and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
								"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey() + Constants.STR_PERCENT)
								.list();
							}else{
								result = hibernate_session.createQuery
								("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userName like ? " +
										"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
								"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey() + Constants.STR_PERCENT)
								.setInteger(1,appsId).list();
							}
						}else{
							if(appsId==0){
								result = hibernate_session.createQuery
								("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userName like ? " +
										"and u.branchDept = b.branchId and ur.roleId = ra.role_id and u.userStatus = ? and ur.userId=u.userId " +
								"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode")
								.setString(0,esform.getSearchkey() + Constants.STR_PERCENT).setString(1, esform.getSelectedStatus()).list();
							}else{
								result = hibernate_session.createQuery
								("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where u.userName like ? " +
										"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and u.userStatus = ? and ur.userId=u.userId " +
								"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode")
								.setString(0,esform.getSearchkey() + Constants.STR_PERCENT).setInteger(1,appsId).setString(2, esform.getSelectedStatus()).list();
							}
						}
						
					}
					else
						if (esform.getSelectedState().equals(Constants.STR_BRANCHCODE))
						{
							// By Branch Code
							if(esform.getSelectedStatus().equals(Constants.STR_ALL)){	
								if(appsId==0){
									result = hibernate_session.createQuery
									("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where b.branchCode = ? " +
											"and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
									"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey())
									.list();
								}else{
									result = hibernate_session.createQuery
									("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where b.branchCode = ? " +
											"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and ur.userId=u.userId " +
									"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey())
									.setInteger(1,appsId).list();
								}
							}else{
								if(appsId==0){
									result = hibernate_session.createQuery
									("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where b.branchCode = ? " +
											"and u.branchDept = b.branchId and ur.roleId = ra.roleId and u.userStatus = ? and ur.userId=u.userId " +
									"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode")
									.setString(0,esform.getSearchkey()).setString(1, esform.getSelectedStatus()).list();
								}else{
									result = hibernate_session.createQuery
									("select u.userId,u.userName,u.ext,u.userStatus,u.lastLoginDate,b.branchCode, g.deptDescription from User u , Branch b, Apps a, RoleApps ra, UserRoles ur, Department g where b.branchCode = ? " +
											"and a.appsId = ? and u.branchDept = b.branchId and ur.roleId = ra.roleId and u.userStatus = ? and ur.userId=u.userId " +
									"and ra.appsId = a.appsId and g.id = u.userGroup group by u.userId,u.userName,u.branchDept,u.ext,u.userStatus,u.lastLoginDate,b.branchCode").setString(0,esform.getSearchkey())
									.setInteger(1,appsId).setString(2, esform.getSelectedStatus()).list();
								}             		  
							}
						}
				if (result != null){
					Iterator iterator = result.listIterator();   
					
					int count = 0;
					while(iterator.hasNext()){
						Object[] row = (Object[])iterator.next();
						User u = new User();
						
						String userId = (String) row[0];
						String userName = (String) row[1];
						String ext = (String) row[2];
						String userStatus = (String) row[3];
						Date userLastLoginDate = (Date) row[4];
						String branchCode = (String) row[6];
						u.setUserId(userId);
						u.setUserName(userName);
						u.setExt(ext);
						u.setUserStatus(userStatus);
						u.setLastLoginDate(userLastLoginDate);
						u.setBranchCode(branchCode);
						properResult.add(u);
						count++;
					}
				}
				request.setAttribute(Constants.USER_KEY, properResult);            
			}
			
			hibernate_session.getTransaction().commit();
			
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error("UserSearch System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USERSEARCHACTION,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
}
