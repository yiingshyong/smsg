package com.bcb.common.forms;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.util.Constants;
import com.bcb.common.util.PasswordUtil;
 
 

public final class ChangePasswordForm extends ActionForm {


    // --------------------------------------------------- Instance Variables


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String oldpassword;
	private String newpassword1;
	private String newpassword2;
    // --------------------------------------------------------- Public Methods


    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	oldpassword=null;
    	newpassword1=null;
    	newpassword2=null;


    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

	if ((oldpassword == null) || (oldpassword.length() < 1))
            errors.add("chgpwd",
                       new ActionMessage("errors.chgpwd.oldpassword.blank"));
	  
	if ((newpassword1 == null) || (newpassword1.length() < 1))
        errors.add("chgpwd",
                   new ActionMessage("errors.chgpwd.newpassword1.blank"));
	if ((newpassword2 == null) || (newpassword2.length() < 1))
        errors.add("chgpwd",
                   new ActionMessage("errors.chgpwd.newpassword2.blank"));
	
	if ( newpassword2.compareTo(newpassword1) != 0)
		errors.add("chgpwd",
                new ActionMessage("errors.chgpwd.notthesame"));
	if ( newpassword1 != null)
	{
		if ( newpassword1.length() < 7 || newpassword1.length() > 9)
			errors.add("chgpwd",
	                new ActionMessage("errors.chgpwd.passwordlength"));
			
	};
	if ( oldpassword != null && newpassword1 != null)
	{
		if ( oldpassword.compareTo(newpassword1) == 0 )
		{
			errors.add("chgpwd",
	                new ActionMessage("errors.chgpwd.recycle"));
		
		};
			
	};
	
	//Password Complexity Check
	if (newpassword1 != null)
	{
		String result = PasswordUtil.checkStrength(newpassword1);
		
		if(result != null && !result.equals(Constants.MAPPINGVALUE_SUCCESS))
			errors.add("chgpwd", new ActionMessage(result));
			
	};
	return (errors);

    }


	public String getNewpassword1() {
		return newpassword1;
	}


	public void setNewpassword1(String newpassword1) {
		this.newpassword1 = newpassword1;
	}


	public String getNewpassword2() {
		return newpassword2;
	}


	public void setNewpassword2(String newpassword2) {
		this.newpassword2 = newpassword2;
	}


	public String getOldpassword() {
		return oldpassword;
	}


	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}


	
}

