package com.bcb.common.forms;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.util.Constants;
import com.bcb.common.util.DBUtils;



public final class DeptForm extends ActionForm {
	
	
	// --------------------------------------------------- Instance Variables
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String action;
	private String deptid;
	private String deptname;
	private String deptdescription;
	
	private String selected_deptleader;
	private ArrayList deptleader_label=null;
	private ArrayList deptleader_value=null;
	
	private String selected_asistant_deptleader;
	private ArrayList asistant_deptleader_label=null;
	private ArrayList asistant_deptleader_value=null;
	
	private String selected_deptstatus;
	private ArrayList deptstatus_label=null;
	private ArrayList deptstatus_value=null;
	
	private String createdBy;
	private String createdDate;
	private String createdTime;
	private String modifiedBy;
	private String modifiedDate;
	private String modifiedTime;
//	private List<TblRef> smsTypes = new ArrayList<TblRef>();
//	private String selectedSmsType;
	
	private String[] userapps;
	
	// --------------------------------------------------------- Public Methods
	
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		action=null;
		deptid=null;
		deptname=null;
		deptdescription=null;
		
		selected_deptleader=null;
		deptleader_label=null;
		deptleader_value=null;
		
		selected_asistant_deptleader=null;
		asistant_deptleader_label=null;
		asistant_deptleader_value=null;
		
		selected_deptstatus=null;
		deptstatus_label=null;
		deptstatus_value=null;
		
		createdBy=null;
		createdDate=null;
		createdTime=null;
		modifiedBy=null;
		modifiedDate=null;
		modifiedTime=null;
		
		userapps=null;
//		smsTypes = DaoBeanFactory.getTblRefDao().getSmsTypes();
//		smsTypes.remove(0);
//		selectedSmsType = String.valueOf(smsTypes.get(smsTypes.size()-1).getTblRefId());
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if ((deptname == null) || (deptname.length() < 1))
			errors.add("deptmgmt",
					new ActionMessage("errors.group.groupname.blank"));
		if ((selected_deptstatus.equals(null)))
			errors.add("deptmgmt",
					new ActionMessage("errors.group.groupstatus.blank"));
		
		//Validation check
		if((deptid==null||deptid.length()==0)&&deptname!=null&&deptname.trim().length()>0){
			if(DBUtils.DBHQLCommand("from Department where deptName =?", new Object[]{deptname}).size()>0){
				errors.add("deptmgmt",
						new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, Constants.FORM_FIELD_DEPTNAME));
			}
		}
		return (errors);
		
	}
	
	
	public String getAction() {
		return action;
	}
	
	
	public void setAction(String action) {
		this.action = action;
	}
	
	
	public String getDeptdescription() {
		return deptdescription;
	}
	
	
	public void setDeptdescription(String groupdescription) {
		this.deptdescription = groupdescription;
	}
	
	
	public String getDeptid() {
		return deptid;
	}
	
	
	public void setDeptid(String groupid) {
		this.deptid = groupid;
	}
	
	public String getDeptname() {
		return deptname;
	}
	
	
	public void setDeptname(String groupname) {
		this.deptname = groupname;
	}


	/**
	 * @return Returns the asistant_deptleader_label.
	 */
	public ArrayList getAsistant_deptleader_label() {
		return asistant_deptleader_label;
	}


	/**
	 * @param asistant_deptleader_label The asistant_deptleader_label to set.
	 */
	public void setAsistant_deptleader_label(ArrayList asistant_groupleader_label) {
		this.asistant_deptleader_label = asistant_groupleader_label;
	}


	/**
	 * @return Returns the asistant_deptleader_value.
	 */
	public ArrayList getAsistant_deptleader_value() {
		return asistant_deptleader_value;
	}


	/**
	 * @param asistant_deptleader_value The asistant_deptleader_value to set.
	 */
	public void setAsistant_deptleader_value(ArrayList asistant_groupleader_value) {
		this.asistant_deptleader_value = asistant_groupleader_value;
	}


	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return createdBy;
	}


	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return createdDate;
	}


	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}


	/**
	 * @return Returns the createdTime.
	 */
	public String getCreatedTime() {
		return createdTime;
	}


	/**
	 * @param createdTime The createdTime to set.
	 */
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}


	/**
	 * @return Returns the deptleader_label.
	 */
	public ArrayList getDeptleader_label() {
		return deptleader_label;
	}


	/**
	 * @param deptleader_label The deptleader_label to set.
	 */
	public void setDeptleader_label(ArrayList groupleader_label) {
		this.deptleader_label = groupleader_label;
	}


	/**
	 * @return Returns the deptleader_value.
	 */
	public ArrayList getDeptleader_value() {
		return deptleader_value;
	}


	/**
	 * @param deptleader_value The deptleader_value to set.
	 */
	public void setDeptleader_value(ArrayList groupleader_value) {
		this.deptleader_value = groupleader_value;
	}


	/**
	 * @return Returns the deptstatus_label.
	 */
	public ArrayList getDeptstatus_label() {
		return deptstatus_label;
	}


	/**
	 * @param deptstatus_label The deptstatus_label to set.
	 */
	public void setDeptstatus_label(ArrayList groupstatus_label) {
		this.deptstatus_label = groupstatus_label;
	}


	/**
	 * @return Returns the deptstatus_value.
	 */
	public ArrayList getDeptstatus_value() {
		return deptstatus_value;
	}


	/**
	 * @param deptstatus_value The deptstatus_value to set.
	 */
	public void setDeptstatus_value(ArrayList groupstatus_value) {
		this.deptstatus_value = groupstatus_value;
	}


	/**
	 * @return Returns the modifiedBy.
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}


	/**
	 * @param modifiedBy The modifiedBy to set.
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	/**
	 * @return Returns the modifiedDate.
	 */
	public String getModifiedDate() {
		return modifiedDate;
	}


	/**
	 * @param modifiedDate The modifiedDate to set.
	 */
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	/**
	 * @return Returns the modifiedTime.
	 */
	public String getModifiedTime() {
		return modifiedTime;
	}


	/**
	 * @param modifiedTime The modifiedTime to set.
	 */
	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}


	/**
	 * @return Returns the selected_asistant_deptleader.
	 */
	public String getSelected_asistant_deptleader() {
		return selected_asistant_deptleader;
	}


	/**
	 * @param selected_asistant_deptleader The selected_asistant_deptleader to set.
	 */
	public void setSelected_asistant_deptleader(
			String selected_asistant_groupleader) {
		this.selected_asistant_deptleader = selected_asistant_groupleader;
	}


	/**
	 * @return Returns the selected_deptleader.
	 */
	public String getSelected_deptleader() {
		return selected_deptleader;
	}


	/**
	 * @param selected_deptleader The selected_deptleader to set.
	 */
	public void setSelected_deptleader(String selected_groupleader) {
		this.selected_deptleader = selected_groupleader;
	}


	/**
	 * @return Returns the selected_deptstatus.
	 */
	public String getSelected_deptstatus() {
		return selected_deptstatus;
	}


	/**
	 * @param selected_deptstatus The selected_deptstatus to set.
	 */
	public void setSelected_deptstatus(String selected_groupstatus) {
		this.selected_deptstatus = selected_groupstatus;
	}


	/**
	 * @return Returns the userapps.
	 */
	public String[] getUserapps() {
		return userapps;
	}


	/**
	 * @param userapps The userapps to set.
	 */
	public void setUserapps(String[] userapps) {
		this.userapps = userapps;
	}


//	public List<TblRef> getSmsTypes() {
//		return smsTypes;
//	}
//
//
//	public void setSmsTypes(List<TblRef> smsTypes) {
//		this.smsTypes = smsTypes;
//	}
//
//
//	public String getSelectedSmsType() {
//		return selectedSmsType;
//	}
//
//
//	public void setSelectedSmsType(String selectedSmsType) {
//		this.selectedSmsType = selectedSmsType;
//	}


	
}

