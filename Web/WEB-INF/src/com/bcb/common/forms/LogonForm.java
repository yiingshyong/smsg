package com.bcb.common.forms;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;



public final class LogonForm extends ActionForm {


    // --------------------------------------------------- Instance Variables


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String logonuserid=null;
    private String logonpassword = null;
    // --------------------------------------------------------- Public Methods


    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	this.logonuserid = null;
        this.logonpassword = null;


    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

	if ((logonuserid == null) || (logonuserid.length() < 1))
            errors.add("logon",
                       new ActionMessage("errors.logon.userid.blank"));
	  
	if ((logonpassword == null) || (logonpassword.length() < 1))
        errors.add("logon",
                   new ActionMessage("errors.logon.password.blank"));
	
	return (errors);

    }


	public String getLogonpassword() {
		return logonpassword;
	}


	public void setLogonpassword(String logonpassword) {
		this.logonpassword = logonpassword;
	}


	public String getLogonuserid() {
		return logonuserid;
	}


	public void setLogonuserid(String logonuserid) {
		this.logonuserid = logonuserid;
	}


}

