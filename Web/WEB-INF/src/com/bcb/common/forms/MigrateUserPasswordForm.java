package com.bcb.common.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.User;
import com.bcb.common.util.Constants;
import com.bcb.common.util.PasswordUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class MigrateUserPasswordForm extends ActionForm{

	private static final long serialVersionUID = -7676039614081476152L;
	private String userId;
	private String oldpassword;
	private String newpassword1;
	private String newpassword2;	

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// TODO Auto-generated method stub
    	oldpassword=null;
    	newpassword1=null;
    	newpassword2=null;
		this.userId = null;
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

    	if ((oldpassword == null) || (oldpassword.length() < 1))
                errors.add("chgpwd",
                           new ActionMessage("errors.chgpwd.oldpassword.blank"));
    	  
    	if ((newpassword1 == null) || (newpassword1.length() < 1))
            errors.add("chgpwd",
                       new ActionMessage("errors.chgpwd.newpassword1.blank"));
    	if ((newpassword2 == null) || (newpassword2.length() < 1))
            errors.add("chgpwd",
                       new ActionMessage("errors.chgpwd.newpassword2.blank"));
    	
    	if ( newpassword2.compareTo(newpassword1) != 0)
    		errors.add("chgpwd",
                    new ActionMessage("errors.chgpwd.notthesame"));
    	if ( newpassword1 != null)
    	{
    		if ( newpassword1.length() < 6 || newpassword1.length() > 9)
    			errors.add("chgpwd",
    	                new ActionMessage("errors.chgpwd.passwordlength"));
    			
    	};
    	if ( oldpassword != null && newpassword1 != null)
    	{
    		if ( oldpassword.compareTo(newpassword1) == 0 )
    		{
    			errors.add("chgpwd",
    	                new ActionMessage("errors.chgpwd.recycle"));
    		
    		};
    			
    	};
    	
    	//Password Complexity Check
    	if (newpassword1 != null)
    	{
    		String result = PasswordUtil.checkStrength(newpassword1);
    		
    		if(result != null && !result.equals(Constants.MAPPINGVALUE_SUCCESS))
    			errors.add("chgpwd", new ActionMessage(result));
    			
    	};
		
		if ( StringUtils.isEmpty(this.userId)){
				errors.add("chgpwd", new ActionMessage("User Id Is Mandatory",false));				
		}
		
		if(StringUtils.isNotEmpty(getOldpassword()) && StringUtils.isNotEmpty(this.userId)){
			User user = DaoBeanFactory.getUserDao().findByUserId(this.getUserId());
			if(user == null || !SHAUtil.encrypt(getOldpassword()).equalsIgnoreCase(user.getPassword())){
				errors.add("chgpwd", new ActionMessage("Invalid credential", false));
			}
		}
				
		return errors;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOldpassword() {
		return oldpassword;
	}

	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}

	public String getNewpassword1() {
		return newpassword1;
	}

	public void setNewpassword1(String newpassword1) {
		this.newpassword1 = newpassword1;
	}

	public String getNewpassword2() {
		return newpassword2;
	}

	public void setNewpassword2(String newpassword2) {
		this.newpassword2 = newpassword2;
	}	
}
