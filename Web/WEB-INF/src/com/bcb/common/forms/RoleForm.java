package com.bcb.common.forms;



import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.util.Constants;
import com.bcb.common.util.DBUtils;



public final class RoleForm extends ActionForm {


    // --------------------------------------------------- Instance Variables


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	
	private Integer roleid;
	private String rolename;
	private String roledescription;


	//private Set permroles;
	private String[] permroles;
	private String[] userapps;
    // --------------------------------------------------------- Public Methods
	private String selected_userstatus;
	private ArrayList userstatus_label=null;
	private ArrayList userstatus_value=null;
	
	private String modifieddate;
	private String modifiedtime;
	private String modifiedby;
	private String createddate;
	private String createdtime;
	private String createdby;
    /**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    
    	  roleid=null;
    	  rolename=null;
    	  roledescription=null;
    	selected_userstatus=null;
    		userstatus_label=null;
    	userstatus_value=null;
    	  permroles=null;
    	  userapps=null;
    	  modifieddate=null;
    	  modifiedtime=null;
    	  modifiedby=null;
    	  createddate=null;
    	  createdtime=null;
    	  createdby=null;
    	// How do i populate with all leh?


    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

	if ((rolename == null) || (rolename.length() < 1))
            errors.add("rolemgmt",
                       new ActionMessage("errors.role.rolename.blank"));
		
	if (permroles == null)
        errors.add("permroles",
                   new ActionMessage("errors.role.permroles.none"));
	
	if (userapps == null)
        errors.add("userapps",
                   new ActionMessage("errors.role.userapps.none"));
		
	if((roleid==null||roleid.toString().length()==0)&&rolename!=null&&rolename.trim().length()>0){
		if(DBUtils.DBHQLCommand("from RoleRef where roleName =?", new Object[]{rolename}).size()>0){
			errors.add("rolemgmt",
					new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, Constants.FORM_FIELD_ROLENAME));
		}
	}
	
		return (errors);

		
    }




/*
	public Set getPermroles() {
		return permroles;
	}


	public void setPermroles(Set permroles) {
		this.permroles = permroles;
	}
*/
	public String[] getPermroles() {
		return permroles;
	}


	public void setPermroles(String[] permroles) {
		this.permroles = permroles;
	}

	public String getRoledescription() {
		return roledescription;
	}


	public void setRoledescription(String roledescription) {
		this.roledescription = roledescription;
	}


	public Integer getRoleid() {
		return roleid;
	}


	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}


	public String getRolename() {
		return rolename;
	}


	public void setRolename(String rolename) {
		this.rolename = rolename;
	}



	public String[] getUserapps() {
		return userapps;
	}


	public void setUserapps(String[] userapps) {
		this.userapps = userapps;
	}


	public String getSelected_userstatus() {
		return selected_userstatus;
	}


	public void setSelected_userstatus(String selected_userstatus) {
		this.selected_userstatus = selected_userstatus;
	}


	public ArrayList getUserstatus_label() {
		return userstatus_label;
	}


	public void setUserstatus_label(ArrayList userstatus_label) {
		this.userstatus_label = userstatus_label;
	}


	public ArrayList getUserstatus_value() {
		return userstatus_value;
	}


	public void setUserstatus_value(ArrayList userstatus_value) {
		this.userstatus_value = userstatus_value;
	}


	public String getCreatedby() {
		return createdby;
	}


	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}


	public String getCreateddate() {
		return createddate;
	}


	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}


	public String getCreatedtime() {
		return createdtime;
	}


	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}


	public String getModifiedby() {
		return modifiedby;
	}


	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}


	public String getModifieddate() {
		return modifieddate;
	}


	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}


	public String getModifiedtime() {
		return modifiedtime;
	}


	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}


	
}

