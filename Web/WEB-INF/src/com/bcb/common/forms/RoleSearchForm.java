package com.bcb.common.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class RoleSearchForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String searchkey;
	private ArrayList searchby_label=new ArrayList();
	private ArrayList searchby_value=new ArrayList();
	private String selectedState;

 public void reset(ActionMapping mapping, HttpServletRequest request) {
	 searchby_label = new ArrayList();
	 searchby_label.add("Role Name");

	 searchby_value = new ArrayList();

	 searchby_value.add("RoleName");

	 selectedState = "";
	 searchkey="";
	 }


public ArrayList getSearchby_label() {
	return searchby_label;
}


public void setSearchby_label(ArrayList searchby_label) {
	this.searchby_label = searchby_label;
}


public ArrayList getSearchby_value() {
	return searchby_value;
}


public void setSearchby_value(ArrayList searchby_value) {
	this.searchby_value = searchby_value;
}


public String getSelectedState() {
	return selectedState;
}


public void setSelectedState(String selectedState) {
	this.selectedState = selectedState;
}


public String getSearchkey() {
	return searchkey;
}

public void setSearchkey(String searchkey) {
	this.searchkey = searchkey;
}
}
