package com.bcb.common.forms;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;



public final class UserForm extends ActionForm {

	private static final long serialVersionUID = -540214198180330369L;
	private String action;
	private String userid;
	private String username;

	private Integer selected_group;
	private ArrayList group_label=null;
	private ArrayList group_value=null;
	
	private Integer selected_branch;
	private ArrayList branch_label=null;
	private ArrayList branch_value=null;
	
	
	private Integer selected_role;
	private String[] selected_roles;
	private ArrayList role_label=null;
	private ArrayList role_value=null;
	private ArrayList selected_role_label=null;
	private ArrayList selected_role_value=null;
	private boolean selectedAllRoles;
	private String strSelected_roles;
	
	private String password;
	private String selected_userstatus;
	private ArrayList userstatus_label=null;
	private ArrayList userstatus_value=null;
	
	private String emailid;
	private String bizphone;
	private String ext;
	private String hphone;
	private String staffid;
	private String createdby;
	private String createddate;
	private String createdtime;
	private String modifiedby;
	private String modifieddate;
	private String modifiedtime;
	private String lastpwdchg;
	private String failedlogon;
	private String lastLoginDate;
	
    // --------------------------------------------------------- Public Methods

	/**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
   
    		action=null;
    		userid=null;
    		username=null;
    		selected_group=null;
    		group_label=null;
    		group_value=null;
    		
    		selected_branch=null;
    		branch_label=null;
    		branch_value=null;

    		selected_role=null;
    		role_label=null;
    		role_value=null;
    		selected_role_label=null;
    		selected_role_value=null;
    		selectedAllRoles=false;
    		selected_roles=null;
    		strSelected_roles=null;
    		
    		password=null;
    		selected_userstatus=null;
    		userstatus_label=null;
    		userstatus_value=null;
    		
    		emailid = null;
    		bizphone=null;
    		ext=null;
    		hphone=null;
    		staffid = null;
    		createdby=null;
    		createddate=null;
    		createdtime=null;
    		modifiedby=null;
    		modifieddate=null;
    		modifiedtime=null;
    		lastpwdchg=null;
    		failedlogon=null;
    		lastLoginDate=null;
    }


    /**
     * Validate the properties that have been set from this HTTP request,
     * and return an <code>ActionErrors</code> object that encapsulates any
     * validation errors that have been found.  If no errors are found, return
     * <code>null</code> or an <code>ActionErrors</code> object with no
     * recorded error messages.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public ActionErrors validate(ActionMapping mapping,
                                 HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
      
	
		
	if ((userid == null) || (userid.length() < 1))
            errors.add("usermgmt",
                       new ActionMessage("errors.user.userid.blank"));
	if ((username == null) || (username.length() < 1))
        errors.add("usermgmt",
                   new ActionMessage("errors.user.username.blank"));
	if ((staffid == null) || (staffid.length() < 1))
        errors.add("usermgmt",
                   new ActionMessage("errors.user.staffid.blank"));
	if ( selected_group.intValue() == 0 )
	    errors.add("usermgmt",
                new ActionMessage("errors.user.usergroup.blank"));
	if (strSelected_roles.length()==0 )
	    errors.add("usermgmt",
                new ActionMessage("errors.user.role.blank"));
	if (createdby == null){
		if(password != null && password.length() < 1){
		    errors.add("usermgmt",
	                new ActionMessage("errors.user.password.blank"));
		}
	}


//	if ( selected_branch.intValue() == 0 )
//	    errors.add("usermgmt",
//                new ActionMessage("errors.user.branchdept.blank"));
	if ( failedlogon != null)
	if (! failedlogon.matches("[0-9]+") )
	    errors.add("usermgmt",
                new ActionMessage("errors.user.failedlogon.type"));
	
	return (errors);

    }


	public String getAction() {
		return action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	

	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	
	public ArrayList getBranch_label() {
		return branch_label;
	}


	public void setBranch_label(ArrayList branch_label) {
		this.branch_label = branch_label;
	}


	public ArrayList getBranch_value() {
		return branch_value;
	}


	public void setBranch_value(ArrayList branch_value) {
		this.branch_value = branch_value;
	}




	public ArrayList getRole_value() {
		return role_value;
	}


	public void setRole_value(ArrayList role_value) {
		this.role_value = role_value;
	}


	public ArrayList getGroup_label() {
		return group_label;
	}


	public void setGroup_label(ArrayList group_label) {
		this.group_label = group_label;
	}


	public ArrayList getGroup_value() {
		return group_value;
	}


	public void setGroup_value(ArrayList group_value) {
		this.group_value = group_value;
	}


	public ArrayList getRole_label() {
		return role_label;
	}


	public void setRole_label(ArrayList role_label) {
		this.role_label = role_label;
	}


	public Integer getSelected_branch() {
		return selected_branch;
	}


	public void setSelected_branch(Integer selected_branch) {
		this.selected_branch = selected_branch;
	}


	public Integer getSelected_group() {
		return selected_group;
	}


	public void setSelected_group(Integer selected_group) {
		this.selected_group = selected_group;
	}


	public Integer getSelected_role() {
		return selected_role;
	}


	public void setSelected_role(Integer selected_role) {
		this.selected_role = selected_role;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}




	

	public String getEmailid() {
		return emailid;
	}


	public String getCreatedby() {
		return createdby;
	}


	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}


	public String getCreateddate() {
		return createddate;
	}


	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}


	public String getCreatedtime() {
		return createdtime;
	}


	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}


	public String getModifiedby() {
		return modifiedby;
	}


	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}


	public String getModifieddate() {
		return modifieddate;
	}


	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}


	public String getModifiedtime() {
		return modifiedtime;
	}


	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}


	public String getBizphone() {
		return bizphone;
	}


	public void setBizphone(String bizphone) {
		this.bizphone = bizphone;
	}


	public String getHphone() {
		return hphone;
	}


	public void setHphone(String hphone) {
		this.hphone = hphone;
	}


	public String getSelected_userstatus() {
		return selected_userstatus;
	}


	public void setSelected_userstatus(String selected_userstatus) {
		this.selected_userstatus = selected_userstatus;
	}


	public ArrayList getUserstatus_label() {
		return userstatus_label;
	}


	public void setUserstatus_label(ArrayList userstatus_label) {
		this.userstatus_label = userstatus_label;
	}


	public ArrayList getUserstatus_value() {
		return userstatus_value;
	}


	public void setUserstatus_value(ArrayList userstatus_value) {
		this.userstatus_value = userstatus_value;
	}


	public String getStaffid() {
		return staffid;
	}


	public void setStaffid(String staffid) {
		this.staffid = staffid;
	}


	public String getFailedlogon() {
		return failedlogon;
	}


	public void setFailedlogon(String failedlogon) {
		this.failedlogon = failedlogon;
	}


	public String getLastpwdchg() {
		return lastpwdchg;
	}


	public void setLastpwdchg(String lastpwdchg) {
		this.lastpwdchg = lastpwdchg;
	}


	public String getExt() {
		return ext;
	}


	public void setExt(String ext) {
		this.ext = ext;
	}


	public String getLastLoginDate() {
		return lastLoginDate;
	}


	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}


	/**
	 * @return Returns the selectedAllRoles.
	 */
	public boolean isSelectedAllRoles() {
		return selectedAllRoles;
	}


	/**
	 * @param selectedAllRoles The selectedAllRoles to set.
	 */
	public void setSelectedAllRoles(boolean selectedAllRoles) {
		this.selectedAllRoles = selectedAllRoles;
	}


	/**
	 * @return Returns the strSelected_roles.
	 */
	public String getStrSelected_roles() {
		return strSelected_roles;
	}


	/**
	 * @param strSelected_roles The strSelected_roles to set.
	 */
	public void setStrSelected_roles(String strSelected_roles) {
		this.strSelected_roles = strSelected_roles;
	}


	/**
	 * @return Returns the selected_roles.
	 */
	public String[] getSelected_roles() {
		return selected_roles;
	}


	/**
	 * @param selected_roles The selected_roles to set.
	 */
	public void setSelected_roles(String[] selected_roles) {
		this.selected_roles = selected_roles;
	}


	/**
	 * @return Returns the selected_role_label.
	 */
	public ArrayList getSelected_role_label() {
		return selected_role_label;
	}


	/**
	 * @param selected_role_label The selected_role_label to set.
	 */
	public void setSelected_role_label(ArrayList selected_role_label) {
		this.selected_role_label = selected_role_label;
	}


	/**
	 * @return Returns the selected_role_value.
	 */
	public ArrayList getSelected_role_value() {
		return selected_role_value;
	}


	/**
	 * @param selected_role_value The selected_role_value to set.
	 */
	public void setSelected_role_value(ArrayList selected_role_value) {
		this.selected_role_value = selected_role_value;
	}	

	
}

