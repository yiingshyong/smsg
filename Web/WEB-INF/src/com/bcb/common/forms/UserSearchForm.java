package com.bcb.common.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.util.Constants;

public class UserSearchForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	private String searchkey;
	private ArrayList searchby_label=new ArrayList();
	private ArrayList searchby_value=new ArrayList();
	private String selectedState;
	private ArrayList byApps_label=new ArrayList();
	private ArrayList byApps_value=new ArrayList();
	private String selectedApps;
	private ArrayList byStatus_label=new ArrayList();
	private ArrayList byStatus_value=new ArrayList();
	private String selectedStatus;
	

 public void reset(ActionMapping mapping, HttpServletRequest request) {
	 searchby_label = new ArrayList();
	 searchby_label.add(Constants.STR_USERID);
	 searchby_label.add(Constants.STR_USERNAME);
	 
	 searchby_value = new ArrayList();
	 searchby_value.add(Constants.STR_USERID);
	 searchby_value.add(Constants.STR_USERNAME);
	 
	 byStatus_label = new ArrayList();
	 byStatus_label.add(Constants.STR_ALL);
	 byStatus_label.add(Constants.STR_ACTIVE);
	 byStatus_label.add(Constants.STR_CLOSED);
	 byStatus_label.add(Constants.STR_RESIGN);
	 byStatus_value = new ArrayList();
	 byStatus_value.add(Constants.STR_ALL);
	 byStatus_value.add(Constants.STR_ACTIVE);
	 byStatus_value.add(Constants.STR_CLOSED);
	 byStatus_value.add(Constants.STR_RESIGN);
	 selectedState = "";
	 selectedStatus = "";
	 byApps_label = null;
	 byApps_value = null;
	 selectedApps = "";
	 searchkey="";
	 }

 public ActionErrors validate(ActionMapping mapping,
         HttpServletRequest request) {

	 ActionErrors errors = new ActionErrors();

	if (selectedState.equals(Constants.STR_BRANCHCODE)){
		if(searchkey.length() > 4){
			errors.add("branchmgmt",
					new ActionMessage("errors.common.maxlength", Constants.STR_BRANCHCODELABEL, Constants.STR_FOUR ));			
		}
	}
		
	return (errors);

}
 
public ArrayList getByApps_label() {
	return byApps_label;
}



public void setByApps_label(ArrayList byApps_label) {
	this.byApps_label = byApps_label;
}



public ArrayList getByApps_value() {
	return byApps_value;
}



public void setByApps_value(ArrayList byApps_value) {
	this.byApps_value = byApps_value;
}



public String getSelectedApps() {
	return selectedApps;
}



public void setSelectedApps(String selectedApps) {
	this.selectedApps = selectedApps;
}



public ArrayList getSearchby_label() {
	return searchby_label;
}


public void setSearchby_label(ArrayList searchby_label) {
	this.searchby_label = searchby_label;
}


public ArrayList getSearchby_value() {
	return searchby_value;
}


public void setSearchby_value(ArrayList searchby_value) {
	this.searchby_value = searchby_value;
}


public String getSelectedState() {
	return selectedState;
}


public void setSelectedState(String selectedState) {
	this.selectedState = selectedState;
}


public String getSearchkey() {
	return searchkey;
}

public void setSearchkey(String searchkey) {
	this.searchkey = searchkey;
}

public ArrayList getByStatus_label() {
	return byStatus_label;
}

public void setByStatus_label(ArrayList byStatus_label) {
	this.byStatus_label = byStatus_label;
}

public ArrayList getByStatus_value() {
	return byStatus_value;
}

public void setByStatus_value(ArrayList byStatus_value) {
	this.byStatus_value = byStatus_value;
}

public String getSelectedStatus() {
	return selectedStatus;
}

public void setSelectedStatus(String selectedStatus) {
	this.selectedStatus = selectedStatus;
}
}
