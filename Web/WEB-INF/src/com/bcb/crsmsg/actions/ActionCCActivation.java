package com.bcb.crsmsg.actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsReceived;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.creditcard.CCASMSGResponse;
import com.bcb.sgwcore.creditcard.CCActivationUtil;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ActionCCActivation implements ActionTemplate {

	private static final Log log = LogFactory.getLog(ActionCCActivation.class);
	private static final int CC_NO_LENGTH = 16;
	private static final int CARD_SUFFIX_ACTIVATION_INPUT_LENGTH = 4;
	private static final int CARD_HOLDER_PARAM_ID = 0;
	private static final int CARD_HOLDER_PARAM_CC_NO = 1;
	private static String RESPONSE_SMS_FAILED_TO_CREATE_MSG = "Generate response sms failed";
	private SmsCfg smsCfg;
	private String mobileNo;
	private String fullCCNo;
	private String fullCustId;
	private String normalizedSMS;
	private CreditCardActivation newCardRecord;
	
	
	@Override
	public String execute(Keyword theKeyword, ArrayList<String> paramArray) {
		long startTime = System.currentTimeMillis();
		StringBuilder logStr = new StringBuilder();
		CCASMSGResponse activationResult = null;
		mobileNo = paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO));
		SmsReceived smsReceived = theKeyword.getSmsReceive();		
		try{
			mobileNo = BusinessService.normalizeMobile(mobileNo);
//			if(mobileNo.startsWith("60")){//malaysia
//				mobileNo = mobileNo.substring(1);
//			}
			String incomingSms = paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE));
			smsCfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		
			//check svc window availability
			String[] activationSvcWindow = StringUtils.splitByWholeSeparator(smsCfg.getCcaSvcWindow(), Constants.CC_ACTIVATION_SVC_WINDOW_SEPARATOR);
			DateFormat df = new SimpleDateFormat("HHmm");
			Integer currentTime = Integer.parseInt(df.format(new Date()));
			if(currentTime < Integer.parseInt(activationSvcWindow[0]) || currentTime > Integer.parseInt(activationSvcWindow[1])){
				activationResult = CCASMSGResponse.getUnavailableResponse("SMS: Exceeded Card Activation Time Frame", false);
			}
			
			//Current time in the Card Activation Window Time. Now check for customer sms content validity
			if(activationResult == null){
				 normalizedSMS = normalizeIncomingSMS(incomingSms, theKeyword); //sms content now is without keyword and special characters including spaces
				if(normalizedSMS.length() > CC_NO_LENGTH){
					//customer supplied full card & id number
					activationResult = firstLvlCheckByFullInfo();					
				}else if(normalizedSMS.length() == CARD_SUFFIX_ACTIVATION_INPUT_LENGTH){
					//customer supplied simplified version
					activationResult = firstLvlCheckByCardSuffix();
//					smsReceived.setMessage(smsReceived.getMsgNoKeyword());//no encryption needed for simplified version as 1.input in not in full card number 2.To display in sms received 
				}else{
					//invalid input
					activationResult = CCASMSGResponse.getFailureResponse("SMS: Missing input or invalid input format", false);											
				}
			}
			
			//First level checking no error. Going to call server for card activation
			if(activationResult == null){
				activationResult = CCActivationUtil.activateCardViaSocket(newCardRecord.getMobileNo(), SecurityManager.getInstance().decrypt(newCardRecord.getCardNo()), newCardRecord.getCustomerId().toUpperCase());
			}
		}catch(Exception e){
			log.error("Unexpected Error Caught. SMS Received Id: " + smsReceived.getId() + ". Reason: " + e.getMessage(), e);
			activationResult = CCASMSGResponse.getFailureResponse("SMS: Unexpected Error", false);
		}finally{
			SmsQueue smsQueue = null;
			if(activationResult == null){//should not happen
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Unexpected Error", false);				
			}
			//save response sms
			try{
				smsQueue = createSMS(theKeyword, activationResult.getResponseSMS(), mobileNo);
				DaoBeanFactory.getHibernateTemplate().save(smsQueue);
			}catch(Exception e){
				log.error("Error create response SMS. SMS Received Id: " + smsReceived.getId() + ". Reason: " + e.getMessage(), e);
			}
			//update sms received
			smsReceived.setRemarks(activationResult.getActivationMessage());
			if(smsQueue == null){
				smsReceived.setRemarks(smsReceived.getRemarks() + com.bcb.common.util.Constants.LINE_SEPARATOR +  RESPONSE_SMS_FAILED_TO_CREATE_MSG);
			}
			DaoBeanFactory.getHibernateTemplate().update(smsReceived);
			
			//update credit card record
			if(newCardRecord != null){
				newCardRecord.setLastModifiedDatetime(new Date());
				newCardRecord.setFailureAttemptCount(activationResult.isIncrementAttemptCount()? newCardRecord.getFailureAttemptCount()+1 : newCardRecord.getFailureAttemptCount());
				newCardRecord.setStatus(activationResult.getResult());
				newCardRecord.setSmsReceivedId(smsReceived.getId());
				DaoBeanFactory.getHibernateTemplate().update(newCardRecord);
			}
			logStr.append(smsReceived.getId())
			.append(" ").append(mobileNo)
			.append(" ").append(activationResult.getActivationMessage())
			.append(" ").append((System.currentTimeMillis()-startTime));
			log.info(logStr.toString());
		}
		return "completed";
	}
		
	private CCASMSGResponse firstLvlCheckByFullInfo(){
		CCASMSGResponse activationResult = null;
		String[] cardHolderParams = getFullCardHolderParams(normalizedSMS);
		fullCCNo = cardHolderParams[CARD_HOLDER_PARAM_CC_NO];
		fullCustId = cardHolderParams[CARD_HOLDER_PARAM_ID];
		
		String normalizedMobileNo = BusinessService.normalizeMobile(this.mobileNo);  
				
		if(!StringUtils.isNumeric(fullCCNo)){
			activationResult = CCASMSGResponse.getFailureResponse("SMS: Missing input or invalid input format", false);						
		}
		
		if(activationResult == null){
			newCardRecord = DaoBeanFactory.getCCActivationDao().find(fullCCNo);
			if(newCardRecord == null){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Card No Not Exists", false);
			}else if(newCardRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED)){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Already Been Activated", false, CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED, null);
			}else if(newCardRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED) && newCardRecord.getFailureAttemptCount() >= smsCfg.getCcaMaxFailureAttempt()){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Exceeded Invalid Attempt Threshold", true);
			}else if(!fullCustId.trim().equalsIgnoreCase(newCardRecord.getCustomerId().trim())){
				activationResult = CCASMSGResponse.getFailureFirstLvCustIdUnmatch(fullCustId, newCardRecord.getCustomerId());					
			}else if(!newCardRecord.getMobileNo().trim().equals(normalizedMobileNo)){
				if(normalizedMobileNo.startsWith("60") && normalizedMobileNo.substring(1).equals(newCardRecord.getMobileNo().trim())){
					//in case cardlink record without MY country code
				}else{
					activationResult = CCASMSGResponse.getFailureFirstLvlMobileUnmatch(normalizedMobileNo, newCardRecord.getMobileNo());																				
				}
			}
		}
		
		return activationResult;
	}
	
	//Customer input only last 4 digit card number
	private CCASMSGResponse firstLvlCheckByCardSuffix(){
		Map<String, List<CreditCardActivation>> matchedCardGroup = new HashMap<String, List<CreditCardActivation>>();
		Map<String, List<CreditCardActivation>> unmatchedCardGroup = new HashMap<String, List<CreditCardActivation>>();
		CCASMSGResponse activationResult = null;
		String cardSuffix = normalizedSMS.trim();	
		int requiredCardSuffixLength = 4;
		if(cardSuffix.length() != requiredCardSuffixLength || !StringUtils.isNumeric(cardSuffix)){
			return activationResult = CCASMSGResponse.getFailureResponse("SMS: Missing input or invalid input format", false);									
		}
		//1. get from db by mobile no
		List<CreditCardActivation> records = DaoBeanFactory.getCCActivationDao().findByMobileNo(mobileNo);
		if(mobileNo.startsWith("60")){
			List<CreditCardActivation> recordWithoutCountryCode = DaoBeanFactory.getCCActivationDao().findByMobileNo(mobileNo.substring(1));
			if(recordWithoutCountryCode != null && !recordWithoutCountryCode.isEmpty()){
				if(records == null || records.isEmpty()){
					records = recordWithoutCountryCode;
				}else{
					records.addAll(recordWithoutCountryCode);
				}
			}
		}
		
		String theKey = null;
		if(records != null && !records.isEmpty()){
			for(CreditCardActivation record : records){
				String fullCardNo = SecurityManager.getInstance().decrypt(record.getCardNo()).trim();
//				theKey = fullCardNo + record.getCustomerId().trim();
				theKey = fullCardNo;
				if(fullCardNo.endsWith(cardSuffix)){
					if(!matchedCardGroup.containsKey(theKey)){
						matchedCardGroup.put(theKey, new ArrayList<CreditCardActivation>());
					}
					matchedCardGroup.get(theKey).add(record);
				}else{
					if(!unmatchedCardGroup.containsKey(theKey)){
						unmatchedCardGroup.put(theKey, new ArrayList<CreditCardActivation>());	
					}
					unmatchedCardGroup.get(theKey).add(record);
				}
			}			
		}
		
		if(matchedCardGroup.size() == 0){
			return activationResult = CCASMSGResponse.getFailureResponse("SMS: No record found", false);			
		}else if(matchedCardGroup.size() > 1){
			//multiple unique card no with same suffix
			return activationResult = CCASMSGResponse.getFailureResponse("SMS: Multiple records found", false);						
		}else{
			//single key found matches suffix provided
			boolean multipleRecord = false;
			theKey = matchedCardGroup.keySet().iterator().next();
			
//			when multiple record with same key defined above consider multiple 
			if(matchedCardGroup.get(theKey).size() > 1){
				multipleRecord = true;
			}
			
			if(multipleRecord){
				//same key has multiple records. Might due to duplicate record sent from cardlink 
				return activationResult = CCASMSGResponse.getFailureResponse("SMS: Multiple records found", false);										
			}else{
				newCardRecord = matchedCardGroup.get(theKey).get(matchedCardGroup.get(theKey).size() -1); //get the latest record 
				if(newCardRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED)){
					return activationResult = CCASMSGResponse.getFailureResponse("SMS: Already Been Activated", false, CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED, null); 				
				}else if(newCardRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED) && newCardRecord.getFailureAttemptCount() >= smsCfg.getCcaMaxFailureAttempt()){
					return activationResult = CCASMSGResponse.getFailureResponse("SMS: Exceeded Invalid Attempt Threshold", true);
				}else{
					return activationResult; 
				}
			}
		}
	}
	
	@Deprecated //earlier requirement - not used
	private CCASMSGResponse firstLvlCheckByShortInfo(){
		CCASMSGResponse activationResult = null;
		String[] custInfos = getSimplifiedCardHolderParams(normalizedSMS);
		String custIdSuffix = custInfos[0];
		String cardSuffix = custInfos[1];	
		if(!StringUtils.isNumeric(cardSuffix)){
			return activationResult = CCASMSGResponse.getFailureResponse("SMS: Missing input or invalid input format", false);						
		}
		//1. get from db by mobile no
		List<CreditCardActivation> records = DaoBeanFactory.getCCActivationDao().findUniqueRecordByMobileNoSuffix(mobileNo);
		//2. 
		if(records == null || records.isEmpty()){
			return activationResult = CCASMSGResponse.getFailureResponse("SMS: Mobile No Not Exists", false);
		}else{
			List<CreditCardActivation> matchedCustIdGroup = new ArrayList<CreditCardActivation>();
			List<CreditCardActivation> unmatchedCustIdGroup = new ArrayList<CreditCardActivation>();
			List<CreditCardActivation> matchedCardNoGroup = new ArrayList<CreditCardActivation>();
			List<CreditCardActivation> unmatchedCardNoGroup = new ArrayList<CreditCardActivation>();
			
			for(CreditCardActivation record : records){
				String custIdFull = record.getCustomerId().trim().toLowerCase();
				String cardNoFull = SecurityManager.getInstance().decrypt(record.getCardNo());
				
				if(custIdFull.endsWith(custIdSuffix.toLowerCase())){
					matchedCustIdGroup.add(record);
				}else{
					unmatchedCustIdGroup.add(record);					
				}	
				
				if(cardNoFull.endsWith(cardSuffix.toLowerCase())){
					matchedCardNoGroup.add(record);
				}else{
					unmatchedCardNoGroup.add(record);					
				}
				
			}
			
			if(matchedCardNoGroup.size() == 1 && matchedCustIdGroup.size() == 1){
								
				CreditCardActivation uniqueCustIdRecord = matchedCustIdGroup.get(0);
				CreditCardActivation uniqueCardNoRecord = matchedCardNoGroup.get(0);
				
				if(uniqueCustIdRecord.getId() != uniqueCardNoRecord.getId()){//record matching cust id & record matching card number is not the same record
					activationResult = CCASMSGResponse.getFailureResponse("SMS: Customer No Unmatch " + uniqueCardNoRecord.getCustomerId() + " (Sys) " + custIdSuffix + " (sms) " +
							"\nSMS: Card No Unmatch " + CCActivationUtil.maskCCNumber(SecurityManager.getInstance().decrypt(uniqueCustIdRecord.getCardNo())) + " (Sys) " + cardSuffix + " (sms) ", false);					
				}else if(uniqueCustIdRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED)){
					activationResult = CCASMSGResponse.getFailureResponse("SMS: Already Been Activated", false, CreditCardActivation.CC_ACTIVATION_STATUS_SUCCEED, null);					
				}else if(uniqueCustIdRecord.getStatus().equals(CreditCardActivation.CC_ACTIVATION_STATUS_FAILED) && uniqueCustIdRecord.getFailureAttemptCount() >= smsCfg.getCcaMaxFailureAttempt()){
					activationResult = CCASMSGResponse.getFailureResponse("SMS: Exceeded Invalid Attempt Threshold", true);
				}else if(uniqueCustIdRecord.getId() == uniqueCardNoRecord.getId()){
					activationResult = null;
					newCardRecord = uniqueCustIdRecord;
				}else{
					activationResult = CCASMSGResponse.getFailureResponse("SMS: Undefined Scenario", false);					
				}
			}else if(matchedCustIdGroup.size() == 0){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Customer Num No Record Found", false);
			}else if(matchedCustIdGroup.size() > 1 ){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Customer Num Multiple Record Found", false);				
			}else if(matchedCardNoGroup.size() == 0){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Card Num No Record Found", false);				
			}else if(matchedCardNoGroup.size() > 1){
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Card Num Multiple Record Found", false);				
			}else{
				log.error("Should not happen. Scenario not catered.");
				activationResult = CCASMSGResponse.getFailureResponse("SMS: Undefined Scenario", false);				
			}
		}
		return activationResult;
	}
	
	public static  String normalizeIncomingSMS(String incomingSMS, Keyword keyword){
		//step 1. change to lowercase
		String filteredIncomingSMS = incomingSMS.toLowerCase();
		//step 2. Remove special character
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, "-");
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, "&");
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, " ");
		
		if(keyword != null){
			//step 3. Remove keyword
			filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, keyword.getKeywordName().toLowerCase());			
		}		
		return filteredIncomingSMS;
	}
	
	public static String[] getFullCardHolderParams(String smsWithoutKeyword){
		String[] cardHolderParams = new String[2];
		cardHolderParams[CARD_HOLDER_PARAM_ID] = smsWithoutKeyword.substring(0, smsWithoutKeyword.length() - CC_NO_LENGTH);
		cardHolderParams[CARD_HOLDER_PARAM_CC_NO] = smsWithoutKeyword.substring(smsWithoutKeyword.length() - CC_NO_LENGTH);		
		return cardHolderParams;
	}
	
	public static String[] getSimplifiedCardHolderParams(String smsWithoutKeyword){
		String[] cardHolderParams = new String[2];
		cardHolderParams[CARD_HOLDER_PARAM_ID] = smsWithoutKeyword.substring(0, 4);
		cardHolderParams[CARD_HOLDER_PARAM_CC_NO] = smsWithoutKeyword.substring(4);		
		return cardHolderParams;
	}
	
	public static String[] getCardHolderParams(String incomingSMS, Keyword keyword){
		String filteredIncomingSMS = normalizeIncomingSMS(incomingSMS, keyword);
		
		//input not valid
		if(filteredIncomingSMS.length() <= CC_NO_LENGTH) return null;
		
		return getFullCardHolderParams(filteredIncomingSMS);
	}
	
	private SmsQueue createSMS(Keyword theKeyword, String message, String mobileNo) throws Exception{		
		int deptId=0;
		String dept=Constants.STR_EMPTY;
		User usermodal = (User)DBUtilsCrsmsg.get(User.class, theKeyword.getOwnership());
		if (usermodal == null){
			throw new SgwException(SgwException.USER_NOT_FOUND);
		}else{
			Department groupmodal = (Department)DBUtilsCrsmsg.get(Department.class, usermodal.getUserGroup());
			if (groupmodal != null){
				deptId=groupmodal.getId();
				dept=groupmodal.getDeptName();
			}
		}
		
		SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
		if(theCfg==null){
			throw new SgwException(SgwException.SMS_CFG_NOT_FOUND);
		}else{
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
			SmsQueue aSms=new SmsQueue();
			aSms.setCreatedBy(theKeyword.getOwnership());
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			aSms.setTotalSms(msgArray.size());
			aSms.setMobileNo(mobileNo);
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_4);
			aSms.setSmsStatusBy(theKeyword.getOwnership());
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(theKeyword.getOwnership());
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			aSms.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(aSms.getCreatedBy(), aSms.getChannelId()));
			return aSms;
		}
	}
	
	//To mask cc no in sms sent from customer
	public static String normalizeCCActivationSMSRequestWithMask(String msgNoKeyword){
		String[] params = ActionCCActivation.getCardHolderParams(msgNoKeyword, null);
		return params == null? msgNoKeyword : (params[ActionCCActivation.CARD_HOLDER_PARAM_ID] + " " + CCActivationUtil.maskCCNumber(params[ActionCCActivation.CARD_HOLDER_PARAM_CC_NO])); 
	}

}
