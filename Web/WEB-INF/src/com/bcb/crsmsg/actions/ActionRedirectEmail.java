package com.bcb.crsmsg.actions;

import java.util.ArrayList;

import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SendMailBean;
import com.bcb.crsmsg.util.log4jUtil;

public class ActionRedirectEmail extends ActionRedirect{
	public String execute(Keyword theKeyword, ArrayList<String> paramArray){
		log4jUtil.error("execute ActionRedirectEmail");
		
		if(CommonUtils.NullChecker(theKeyword.getRedirectEmail(), String.class).toString().length()>0){
			SendMailBean theMail=new SendMailBean();
			String subject="Redirect Income SMS: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO));
			String message=
				"Mobile No: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO))+Constants.SPECIAL_CHAR_NEWLINE+
				"Message: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE))+Constants.SPECIAL_CHAR_NEWLINE+
				"Time: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_RECEIVED_DATETIME))+Constants.SPECIAL_CHAR_NEWLINE;
			
			theMail.setP_to(theKeyword.getRedirectEmail());
			theMail.setP_from(Constants.APPSNAME);
			theMail.setP_subject(subject);
			theMail.setP_message(message);
			
			String mailStatus=theMail.send();
			if(mailStatus.equals(SendMailBean.MAIL_STATUS_SUCCESS)){
			}else{
				
			}
		}
		return "some string";
	}
	
}
