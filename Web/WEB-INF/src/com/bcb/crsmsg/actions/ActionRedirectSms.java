package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.Date;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ActionRedirectSms extends ActionRedirect{
	public String execute(Keyword theKeyword, ArrayList<String> paramArray){
		log4jUtil.error("execute ActionRedirectSms");
		System.out.println("inside ActionRedirectSms execute");
		if(CommonUtils.NullChecker(theKeyword.getRedirectMobileNo(), String.class).toString().length()>0){
			try{
				String message="Redirect Income SMS"+Constants.SPECIAL_CHAR_NEWLINE+
				"Mobile No: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO))+Constants.SPECIAL_CHAR_NEWLINE+
				"Message: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE))+Constants.SPECIAL_CHAR_NEWLINE+
				"Time: "+paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_RECEIVED_DATETIME))+Constants.SPECIAL_CHAR_NEWLINE;

				int deptId=0;
				String dept=Constants.STR_EMPTY;
				User usermodal = (User)DBUtilsCrsmsg.get(User.class, theKeyword.getOwnership());
				if (usermodal == null){
				}else{
					Department groupmodal = (Department)DBUtilsCrsmsg.get(Department.class, usermodal.getUserGroup());
					if (groupmodal != null){
						deptId=groupmodal.getId();
						dept=groupmodal.getDeptName();
					}
				}
				
				SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
				if(theCfg==null){// Error smscfg
				}else{
					int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
					ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
					
//					ArrayList<String> lastSmsIdList=new ArrayList<String>();
//					String lastSmsId=Constants.STR_EMPTY;
					
					String[] mobileNo=CommonUtils.NullChecker(theKeyword.getRedirectMobileNo(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()));
					for(int i=0;i<mobileNo.length;i++){
//						for(int j=0;j<msgArray.size();j++){
							SmsQueue aSms=new SmsQueue();
							aSms.setCreatedBy(theKeyword.getOwnership());
							aSms.setCreatedDatetime(new Date());
							aSms.setDeptId(deptId);
							aSms.setDept(dept);
							aSms.setMessage(message);
							aSms.setTotalSms(msgArray.size());
							aSms.setMobileNo(mobileNo[i]);
							aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
							aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
							aSms.setSmsStatus(Constants.SMS_STATUS_4);
							aSms.setSmsStatusBy("ActionReSms");
							aSms.setSmsStatusDatetime(new Date());
							aSms.setSentBy(theKeyword.getOwnership());
							aSms.setType(Constants.SMS_SOURCE_TYPE_5);
							aSms.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(aSms.getCreatedBy(), aSms.getChannelId()));//TODO: channel id implementation, but how for autoreply? Where should channel id come from?							
							DBUtilsCrsmsg.saveOrUpdate(aSms);
							
//							lastSmsIdList.add(CommonUtils.NullChecker(aSms.getId(), String.class).toString());
							
//							aSms.setConcatenateSms(theCfg.getConcatenateSms().equals(Constants.STATUS_YES)?(msgArray.size()==1?Constants.STR_EMPTY:j==0?CommonUtils.NullChecker(aSms.getId(), String.class).toString():j!=msgArray.size()-1?lastSmsId:CommonUtils.arrayToString(lastSmsIdList, theCfg.getTableListDelimiter())):Constants.STR_EMPTY);
//							DBUtilsCrsmsg.saveOrUpdate(aSms);
//							lastSmsId=CommonUtils.NullChecker(aSms.getId(), String.class).toString();
//						}
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
				
			}
		}
		
		return "some string";
	}
}
