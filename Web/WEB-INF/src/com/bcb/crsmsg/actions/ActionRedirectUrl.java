package com.bcb.crsmsg.actions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.crsmsg.util.SecurityManager;
import com.sun.net.ssl.internal.ssl.Provider;

public class ActionRedirectUrl extends ActionRedirect{
	
	private static final ResourceBundle ks = ResourceBundle.getBundle("keystore");
	
	String returnCode = null;
	public String execute(Keyword theKeyword, ArrayList<String> paramArray){
		log4jUtil.error("executing ActionRedirectUrl");
		
		if(CommonUtils.NullChecker(theKeyword.getRedirectUrl(), String.class).toString().length()>0){
			//start
			//returnCode = redirectUrl(theKeyword.getRedirectUrl(), CommonUtils.strToArray(theKeyword.getRedirectUrlParam(), Constants.SPECIAL_CHAR_PIPE), paramArray);
			String keywordDec = theKeyword.getRedirectUrl();
			String urlVal = SecurityManager.getInstance().decrypt(keywordDec);
			
			returnCode = redirectUrl(urlVal, CommonUtils.strToArray(theKeyword.getRedirectUrlParam(), Constants.SPECIAL_CHAR_PIPE), paramArray);
			//end
		}
		return returnCode;
	}
	
	public String redirectUrl(String url, List<String> paramNameList, ArrayList<String> paramArray) {
		//System.out.println(paramNameList);
		//System.out.println(paramArray);
		log4jUtil.error("executing redirectUrl");
		initSSL();
		//String returnCode = Constants.SPECIAL_DASH;
		
		//System.out.println("url is=="+url);
		try {
			for(int i=0;i<paramNameList.size()&&i<paramArray.size();i++){
				if(i!=0){url+="&";}
				url+=paramNameList.get(i)+"="+(paramArray.get(i)==null?Constants.STR_EMPTY:URLEncoder.encode(paramArray.get(i), "UTF-8").replace("+", "%20"));
				
			}
			//System.out.println("url after is =="+url);
			log4jUtil.info("Final redirect URL sent is=="+url);
			returnCode=sendMessage(url);			
		} catch (Exception e) {
			//System.out.println("exception while sending url");
			e.printStackTrace();
		}
		log4jUtil.error("returnCodes from redirectUrl method: "+returnCode);
		//System.out.println("returnCodes from redirectUrl method: "+returnCode);
		return returnCode;
	}	
	
	public String sendMessage(String queryString) throws Exception{
		URLConnection conn = null;
	
		URL objURL = new URL(queryString.toString());
        
		conn = objURL.openConnection();
		
		String strLine = Constants.STR_EMPTY;
		//String returnCode = Constants.SPECIAL_DASH;
		
		if(conn!=null){
			conn.setConnectTimeout(30000);
			conn.setDoOutput(true);
			 //System.out.println("inside conn");
			 try {
				 BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				 strLine = br.readLine();
				 br.close();
			 }//catch (FileNotFoundException e){
			 catch (Exception e){
				 //System.out.println("could not connect to the url");
				 log4jUtil.error("error getting input stream from connection");
				 e.printStackTrace();
			 }
			//BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			int i = 0;
			//System.out.println(br.readLine());
			//String code = (br.readLine().toString());
			//returnCode = returnCode+code;
			
			//strLine = br.readLine();
			//System.out.println("strLine before if clause=="+strLine);
			if (strLine != Constants.STR_EMPTY){
				//System.out.println("inside if");
				returnCode = strLine.toString();
			}else {
				//System.out.println("inside else");
				returnCode = Constants.SPECIAL_DASH;
			}
			
			/*while ((strLine = br.readLine().toString()) != null) {
				System.out.println("inside while loop");
				System.out.println("strLine=="+strLine);
				returnCode = strLine;
				System.out.println("return code in while loop=="+returnCode);
				if (i > 2000) {
					System.out.println("returnCode before break"+returnCode);
					break;
				}
				i++;
			}*/
			//br.close();
		}else{
			//System.out.println("Cannot establish connection with the redirect URL");
			returnCode="Cannot establish connection with the redirect URL";
			
		}
		//System.out.println("returnCode from send message method=="+returnCode);
		return returnCode;
	}
	
	public void initSSL(){
		log4jUtil.error("executing initSSL()");
		System.setProperty("java.protocol.handler.pkgs",
				"com.sun.net.ssl.internal.www.protocol");
		
		//edited -20191124
		Security.addProvider(new Provider());
		//java.security.Security.addProvider(Security.getProvider("SUNJSSE"));
		//end
		
		/*System.setProperty("javax.net.ssl.trustStore", "D:/Keystore/smsg.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");*/
		//System.out.println(ks.getString("keystoreLocation"));
		//System.out.println(ks.getString("password"));
		//getting values
		String keystore = ks.getString("keystoreLocation");
		String password = ks.getString("password");
		log4jUtil.info("KeystoreLocation=="+keystore);
		log4jUtil.info("password=="+password);
		//setting system properties
		//System.setProperty("javax.net.ssl.trustStore", keystore);
       //System.setProperty("javax.net.ssl.trustStorePassword", password);
		
	}

	}
