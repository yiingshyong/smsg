package com.bcb.crsmsg.actions;

import java.util.ArrayList;

import com.bcb.crsmsg.modal.Keyword;

public interface ActionTemplate{
	public String execute(Keyword theKeyword, ArrayList<String> paramArray);
}
