package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;

public class ActionUnsub implements ActionTemplate{
	public String execute(Keyword theKeyword, ArrayList<String> paramArray){
		
		List custList=DBUtilsCrsmsg.DBHQLCommand("from Customer where mobileNo=?", new Object[]{BusinessService.normalizeMobile(paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO)))});
		for(int i=0;i<custList.size();i++){
			Customer aCustomer=(Customer)custList.get(i);
			if(aCustomer.getSmsStatus().equals(Constants.STATUS_SUBSCRIPTION)){// Customer in subscription status, unsubscribe
				aCustomer.setSmsStatus(Constants.STATUS_UNSUBSCRIPTION);
				aCustomer.setSmsStatusBy("ActionUnsub");
				aCustomer.setSmsStatusDatetime(new Date());
				aCustomer.setModifiedBy("ActionUnsub");
				aCustomer.setModifiedDatetime(new Date());
				DBUtilsCrsmsg.saveOrUpdate(aCustomer);
			}else{
				// Customer already not in subscription status
			}
			
		}
		return "some string";
	}
}
