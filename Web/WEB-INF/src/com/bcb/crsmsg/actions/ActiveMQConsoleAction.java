package com.bcb.crsmsg.actions;

import java.text.MessageFormat;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
public final class ActiveMQConsoleAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,com.bcb.crsmsg.util.Constants.PERM_ACTIVEMQ_CONSOLE) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
//	    String postData = URLEncoder.encode(Constants.SSO_TOKEN_PARAM, Constants.CHARSET_UNICODE) + "=" + URLEncoder.encode(request.getSession().getId(), Constants.CHARSET_UNICODE);
//		
//	    URL url = new URL(com.bcb.crsmsg.util.Constants.ACTIVEMQ_CONSOLE_URL + "?" + postData);
//	    URLConnection conn = url.openConnection();
//	    conn.getInputStream();
//	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//	    wr.write(postData);
//	    wr.flush();
//	    wr.close();
		
		Cookie amqCookie = new Cookie(Constants.SSO_AMQ_COOKIE_NAME, request.getSession().getId());
		amqCookie.setPath("/");
		response.addCookie(amqCookie);		
		response.sendRedirect(MessageFormat.format(com.bcb.crsmsg.util.Constants.ACTIVEMQ_CONSOLE_URL, request.getServerName()));
		return null;
	}
}
