

package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.AutoActionForm;
import com.bcb.crsmsg.forms.TelcoForm;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
public final class AddAutoAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
		}else{	
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
			//hibernate_session.beginTransaction();
			AutoActionForm autoAction = (AutoActionForm) form;

			try{
			
			if(autoAction.getSubmitBtn()!=null && autoAction.getSubmitBtn().equals("Submit")){
					
										
					AutoAction auto = new AutoAction();
					auto.setKeyword(0);
					auto.setDescription(autoAction.getDescription());
					auto.setActionClassName(autoAction.getActionClassName());
					auto.setActionSequence(autoAction.getActionSequence());
					auto.setDefaultAction(autoAction.getDefaultAction());
					auto.setServerIP(autoAction.getServerIP());
					auto.setServerPort(Integer.valueOf(autoAction.getServerPort()));
					auto.setServerResponseTimeout(Integer.valueOf(autoAction.getServerResponseTimeout()));
					auto.setMaxFailureAttempt(Integer.valueOf(autoAction.getMaxFailureAttempt()));
					auto.setSvcWindow(autoAction.getSvcWindow());
					auto.setFailureCode(autoAction.getFailureCode());
					auto.setSuccessCode(autoAction.getSuccessCode());
					auto.setUnavailabilitySms(autoAction.getUnavailabilitySms());
					auto.setParameterKey(autoAction.getParameterKey());
					auto.setCreatedBy(theLogonCredential.getUserId());
					auto.setCreatedDatetime(new Date());
					DBUtilsCrsmsg.saveOrUpdate(auto, hibernate_session);
					
					SmsAuditTrail anAuditTrail = new SmsAuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_AUTOACTION_ADD, auto.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
					DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
					
				
				}
				ActionMessages messages = new ActionMessages();
				messages.add("AddAutoAction",new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				//hibernate_session.getTransaction().commit();
				DBUtilsCrsmsg.closeTransaction(hibernate_session);
			}catch (Exception E){
				
				E.printStackTrace();
				log4jUtil.error(Constants.ACTION_AUTOACTION_ADD+" System Error[" + E.getMessage()+ "]");
			
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ACTION_AUTOACTION_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
				
				DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
