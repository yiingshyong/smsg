

package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.FTPSetupForm;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class AddFTPSetup extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.FTP_SETUP) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		try {
			FTPSetupForm theForm = (FTPSetupForm)form;
			theForm.setSetupStatus(Constants.STATUS_ACTIVE);
			theForm.setFtpTimeMode(Constants.FORM_FIELD_TIME_MODE_WEEKLY);
			theForm.setFtpPort(Constants.DEFAULT_FTP_PORT);
			SmsCfg theCfg = (SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			if(theCfg!=null){
				theForm.setAdvanceDayLimit(CommonUtils.NullChecker(theCfg.getScheduledSmsControlDuration(), Integer.class).toString());
			}
			// Set system datetime
			SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_JAVACSRIPT);
			theForm.setSystemDateTime(theFormat.format(new Date()));
			
		}catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_FTPSETUP_ADD+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
