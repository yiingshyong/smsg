

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.KeywordForm;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class AddKeyword extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.KEYWORD_MGMT) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		try {
			KeywordForm theForm = (KeywordForm) form;
			
			List user_list = DBUtilsCrsmsg.DBHQLCommand("select distinct u from User u, UserRoles ur, RoleApps ra where u.userStatus = ? and ra.appsId=? and u.userId=ur.userId and ur.roleId=ra.roleId order by u.userName", new Object[]{Constants.STATUS_ACTIVE, Integer.valueOf(Constants.APPSID)});
			
			ArrayList<String> user_label = new ArrayList<String>();
			ArrayList<String> user_value = new ArrayList<String>();
			for (int i =0 ; i < user_list.size(); i ++){
				User aUser = (User) user_list.get(i); 
				user_label.add(aUser.getUserName());
				user_value.add(aUser.getUserId());
				
			}
			theForm.setOwnership_label(user_label);
			theForm.setOwnership_value(user_value);
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_KEYWORD_ADD+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_KEYWORD_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
