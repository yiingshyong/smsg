

package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.TelcoForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
public final class AddTelco extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
		}else{	
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
			//hibernate_session.beginTransaction();
			TelcoForm telco = (TelcoForm) form;

			try{
				String mobileList="";
				String homeList="";
				String nSList="";
				String incomingList="";
				String[] mobileNo=((String)CommonUtils.NullChecker(telco.getMobile_prefix(),String.class)).split(Constants.SPECIAL_CHAR_NEWLINE);			
//				String[] homeNo=telco.getHome_prefix().split(Constants.SPECIAL_CHAR_NEWLINE);
//				String[] nSNo=telco.getEmail_to().split(Constants.SPECIAL_CHAR_NEWLINE);
//				String[] incomingNo=telco.getIncoming_prefix().split(Constants.SPECIAL_CHAR_NEWLINE);
				
				//nSList=CommonUtils.arrayToString((ArrayList<String>)Arrays.asList(nSNo), Constants.SPECIAL_CHAR_PIPE);
				
				for(int i=0;i<mobileNo.length;i++){					
					if(i==0){
						mobileList=mobileNo[i];
					}else if(i==mobileNo.length-1){
						mobileList = mobileList + Constants.SPECIAL_CHAR_PIPE + mobileNo[i];
					}else{
						mobileList = mobileList + Constants.SPECIAL_CHAR_PIPE + mobileNo[i];
					}
				}
//				for(int i=0;i<homeNo.length;i++){					
//					if(i==0){
//						homeList=mobileNo[i];
//					}else if(i==mobileNo.length-1){
//						homeList = homeList + Constants.SPECIAL_CHAR_PIPE + mobileNo[i];
//					}else{
//						homeList = homeList + Constants.SPECIAL_CHAR_PIPE + mobileNo[i];
//					}
//				}
//				for(int i=0;i<incomingNo.length;i++){					
//					if(i==0){
//						incomingList=incomingNo[i];
//					}else if(i==incomingNo.length-1){
//						incomingList = incomingList + Constants.SPECIAL_CHAR_PIPE + incomingNo[i];
//					}else{
//						incomingList = incomingList + Constants.SPECIAL_CHAR_PIPE + incomingNo[i];
//					}
//				}
//				for(int i=0;i<nSNo.length;i++){					
//					if(i==0){
//						nSList=nSNo[i];
//					}else if(i==nSNo.length-1){
//						nSList = nSList + Constants.SPECIAL_CHAR_PIPE + nSNo[i];
//					}else{
//						nSList = nSList + Constants.SPECIAL_CHAR_PIPE + nSNo[i];
//					}
//				}
				
				if(telco.getSubmit()!=null && telco.getSubmit().equals("Save API")){
					
										
					Telco telcoMod = new Telco();
					telcoMod.setTelcoName(telco.getTelco_name());					
					telcoMod.setTelcoPrefix(BusinessService.normalizeMobile(mobileList));
//					telcoMod.setPrefixWithin(BusinessService.normalizeMobile(homeList));
					telcoMod.setUrl(telco.getUrl());
					telcoMod.setUserId(telco.getUsername());
					telcoMod.setPassword(telco.getPassword());
					telcoMod.setCostIncoming(telco.getIncoming_cost().length()==0?Constants.VALUE_0:telco.getIncoming_cost());
					telcoMod.setCreatedBy(theLogonCredential.getUserId());
					telcoMod.setCreatedDatetime(new Date());
					telcoMod.setUrlParam(telco.getUrlParam());
					telcoMod.setTelcoClassName(telco.getClassName());
					telcoMod.setSvcPvdId(telco.getSelectedSvcProvider());
					
//					telcoMod.setSmsQueueThreshold(telco.getEnabled_st());
//					telcoMod.setSmsQueueThresholdNo(telco.getQueue_threshold());
//					telcoMod.setThresholdNotificationSender(telco.getEmail_from());
//					telcoMod.setThresholdNotificationReceiver(nSList);
//					telcoMod.setThresholdNotification(telco.getEnable_notification());
					telcoMod.setStatus(Constants.STATUS_ACTIVE);
					telcoMod.setStatusDatetime(new Date());
					telcoMod.setProxyAddr(telco.getProxyAddr());
					telcoMod.setProxyPort(telco.getProxyPort());
					telcoMod.setEncryptionStatus(telco.getEncryption());
					
					telcoMod.setCostPerSmsWithin(telco.getWithin_cost().length()==0?Constants.VALUE_0:telco.getWithin_cost());
					telcoMod.setCostPerSmsInter(telco.getInter_cost().length()==0?Constants.VALUE_0:telco.getInter_cost());
					telcoMod.setShortCode(telco.getShortCode());
					telcoMod.setSuccessCode(telco.getSuccessCode());
					telcoMod.setRetryCode(telco.getRetryCode());
					telcoMod.setSmsOnHandLimit(telco.getSmsLimit());
					
					DBUtilsCrsmsg.saveOrUpdate(telcoMod, hibernate_session);
					
					SmsAuditTrail anAuditTrail = new SmsAuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_CONTACT_SAVE_ADD, telcoMod.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
					DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
					
					//hibernate_session.save(telcoMod);
				}
				ActionMessages messages = new ActionMessages();
				messages.add("AddTelco",new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				//hibernate_session.getTransaction().commit();
				DBUtilsCrsmsg.closeTransaction(hibernate_session);
			}catch (Exception E){
				
				E.printStackTrace();
				log4jUtil.error(Constants.ACTION_ADDRESSBOOK_SEARCH+" System Error[" + E.getMessage()+ "]");
			
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_ADDRESSBOOK_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
				
				DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
