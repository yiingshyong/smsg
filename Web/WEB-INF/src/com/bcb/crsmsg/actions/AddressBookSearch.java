

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.AddressBookSearchForm;
import com.bcb.crsmsg.modal.Record;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class AddressBookSearch extends Action {
	
	private static final Log log = LogFactory.getLog(AddressBookSearch.class);
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.ADDRESS_BOOK) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		
		
		try{
			List result = null; 
			AddressBookSearchForm esform =  (AddressBookSearchForm) form;
			esform.setAllowExport(theLogonCredential.getRights().contains(Integer.parseInt(com.bcb.crsmsg.util.Constants.ADDRESSBOOK_EXPORT.trim())));
			
			if(mapping.getParameter().equals("load")){
			}else if (mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				
				if(!esform.isAllowExport()) return null;
				
				//System.out.println("==================TRYING TO GENERATE CSV>>>>>>>>>>");	
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_ADDRESSBOOK );
				printHeader(ouputStream);
				
				Result resultList = (Result) esform.getResultList();
				for(int i=0;resultList != null&&i<resultList.getRecord().size();i++){
					Record aRecord=resultList.getRecord().get(i);
					for(int j=0;j<aRecord.getColumn().size();j++){
						ouputStream.print (println(aRecord.getColumn().get(j)));
					}
					ouputStream.println();
				}
				
				ouputStream.flush();
				
				ouputStream.close();
				
				return null;
			}else{
				ArrayList<String> mainQuery=new ArrayList<String>();
				ArrayList<String> conditionQuery=new ArrayList<String>();
				ArrayList<String> orderQuery=new ArrayList<String>();
				ArrayList<Object> queryParam=new ArrayList<Object>();
				
//				 Main query
				mainQuery.add(
						"select a.id, a.name, a.refNo, a.mobileNo, a.designation, d.deptName " +
						"from Contact a, Department d");
				
//				 Condition criteria
				conditionQuery.add("a.department=d.id");
				
				if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_NAME)){
					conditionQuery.add("a.name like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_MOBILE_NO)){
					conditionQuery.add("a.mobileNo like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_DESIGNATION)){
					conditionQuery.add("a.designation like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT_NAME)){
					conditionQuery.add("d.deptName like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_CUST_REF_NO)){
					conditionQuery.add("a.refNo like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else{// No matched search state, show all
					
				}
				
				if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL))){
					
				}else{
					if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT))){
						conditionQuery.add("a.department=?");
						queryParam.add(Integer.valueOf(CommonUtils.NullChecker(theLogonCredential.getUserGroup(), Integer.class).toString()));
					}else if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL))){
						conditionQuery.add("a.createdBy=?");
						queryParam.add(theLogonCredential.getUserId());
					}else{// Not allowed to perform any action
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
				}
//				for(String q : conditionQuery){
//					log.info("query: " + q);
//				}
//				
//				for(Object param : queryParam){
//					System.out.println(param);
//				}
				
//				 Order criteria
				orderQuery.add("a.name");
//				String query = DBUtilsCrsmsg.generateQuery(mainQuery, conditionQuery, orderQuery);
//				log.info("The final query: " + query);
				result = DBUtilsCrsmsg.DBHQLCommand(DBUtilsCrsmsg.generateQuery(mainQuery, conditionQuery, orderQuery),queryParam.toArray());
				esform.setResultList(CommonUtils.populateResult(result));  
				esform.setSearchkey("");
				esform.setSelectedState("");
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_ADDRESSBOOK_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_ADDRESSBOOK_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		
		ouputStream.print (println("No."));
		ouputStream.print (println("Name"));
		ouputStream.print (println("Cust Ref No."));
		ouputStream.print (println("Mobile Number"));
		ouputStream.print (println("Designation"));
		ouputStream.print (println("Department"));
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
}
