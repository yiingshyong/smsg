

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.CannedMessageSearchForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class CannedMessageSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
	
		try{
			List result = null; 
			
			if(mapping.getParameter().equals("load")){
			}else{// search
				CannedMessageSearchForm esform =  (CannedMessageSearchForm) form;
				ArrayList<String> mainQuery=new ArrayList<String>();
				ArrayList<String> conditionQuery=new ArrayList<String>();
				ArrayList<String> orderQuery=new ArrayList<String>();
				ArrayList<Object> queryParam=new ArrayList<Object>();
				
				// Main query
				mainQuery.add(
						"select m.id, m.templateCode, m.templateContent " +
						"from MsgTemplate m");
				
				// Condition criteria
				conditionQuery.add("m.status=?");
				queryParam.add(Constants.STATUS_ACTIVE);
				
				if (esform.getSelectedState().equals(Constants.FORM_FIELD_MESSAGE_CODE)){// By Message Code
					conditionQuery.add("m.templateCode like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_MESSAGE_TEXT)){// By Message Text
					conditionQuery.add("m.templateContent like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else{// No matched search state, show all
					
				}
				
				if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL))){
					
				}else{
					if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT))){
						conditionQuery.add("m.department=?");
						queryParam.add(Integer.valueOf(CommonUtils.NullChecker(theLogonCredential.getUserGroup(), Integer.class).toString()));
					}else if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL))){
						conditionQuery.add("m.createdBy=?");
						queryParam.add(theLogonCredential.getUserId());
					}else{// Not allowed to perform any action
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
				}

				// Order criteria
				orderQuery.add("m.templateCode");
				
				result = DBUtilsCrsmsg.DBHQLCommand(DBUtilsCrsmsg.generateQuery(mainQuery, conditionQuery, orderQuery), queryParam.toArray());
				esform.setResultList(CommonUtils.populateResult(result));   
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_CANNEDMESSAGE_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_CANNEDMESSAGE_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
}
