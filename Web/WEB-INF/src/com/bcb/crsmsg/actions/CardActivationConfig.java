package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.CardActivationConfigForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class CardActivationConfig extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.CARD_ACTIVATION_CONF) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session =  HibernateUtil.getSessionFactory().getCurrentSession();
		hibernate_session.beginTransaction();
		try {
			CardActivationConfigForm esform = (CardActivationConfigForm) form;
			SmsCfg theCfg= (SmsCfg) DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			
			if(mapping.getParameter().equals("save")){
				theCfg.setCcaFailureSms(esform.getCcaFailureSms());
				theCfg.setCcaMaxFailureAttempt(Integer.valueOf(esform.getCcaMaxFailureAttempt()));
				theCfg.setCcaServerIP(esform.getCcaServerIP());
				theCfg.setCcaServerPort(Integer.valueOf(esform.getCcaServerPort()));
				theCfg.setCcaServerResponseTimeout(Integer.valueOf(esform.getCcaServerResponseTimeout()));
				theCfg.setCcaSuccessSms(esform.getCcaSuccessSms());
				theCfg.setCcaSvcWindow(esform.getCcaSvcWindow());
				theCfg.setCcaUnavailabilitySms(esform.getCcaUnavailabilitySms());
				theCfg.setCcaHostUnavailabilitySms(esform.getCcaHostUnavailabilitySms());
				theCfg.setModifiedBy(theLogonCredential.getUserId());
				theCfg.setModifiedDatetime(new Date());	
				DBUtilsCrsmsg.saveOrUpdate(theCfg, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_CCCFG_SAVE_EDIT,theCfg.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				hibernate_session.getTransaction().commit();
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_CCCFG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
			}else{					
				esform.setCfgId(theCfg.getId());
				esform.setCcaFailureSms(theCfg.getCcaFailureSms());
				esform.setCcaMaxFailureAttempt(String.valueOf(theCfg.getCcaMaxFailureAttempt()));
				esform.setCcaServerIP(theCfg.getCcaServerIP());
				esform.setCcaServerPort(String.valueOf(theCfg.getCcaServerPort()));
				esform.setCcaServerResponseTimeout(String.valueOf(theCfg.getCcaServerResponseTimeout()));
				esform.setCcaSuccessSms(theCfg.getCcaSuccessSms());
				esform.setCcaUnavailabilitySms(theCfg.getCcaUnavailabilitySms());
				esform.setCcaHostUnavailabilitySms(theCfg.getCcaHostUnavailabilitySms());
				esform.setCcaSvcWindow(theCfg.getCcaSvcWindow());
			}
		}
		catch (Exception E){			
			log4jUtil.error(Constants.ACTION_CC_CONFIG+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_CC_CONFIG,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				
				hibernate_session.close();
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}


