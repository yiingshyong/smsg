package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Query;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.CardActivationDtlsForm;
import com.bcb.crsmsg.modal.CreditCardActivation;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.creditcard.CCActivationUtil;


public class CardActivationDtls extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));			
		};
			
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
		try{
			
			CardActivationDtlsForm  theForm =(CardActivationDtlsForm) form;
			
			String offset = request.getParameter(Constants.STR_PAGER_OFFSET);
			if (offset != null && !offset.equals(Constants.STR_EMPTY)) {	
				if (theForm.getButSearch()!= null){
					if (!theForm.getButSearch().equals("Search")){ 
						theForm.setButSearch(Constants.STR_PAGER_OFFSET);
					}else{
						offset = Constants.STR_ZERO;
					}
				}
			}
			
			if (offset == null || offset.equals(Constants.STR_EMPTY)) {
				offset = Constants.STR_ZERO;
			}
			
			int indexFirstResult= Integer.parseInt(offset);
		
			if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH) ||!mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){ 
				session.removeAttribute("dtldCSV");	
				String getFilterVal = (String) request.getParameter("status");
				if (getFilterVal!=null){
					theForm.setSelectedState("status");
					theForm.setSearchkey(getFilterVal);
				}

				String selState = theForm.getSelectedState(); 
				if(selState==null){ 
					selState = (String) session.getAttribute("selectedState");
				}
				
				String seaKey = theForm.getSearchkey();
				if(seaKey==null){ 
					seaKey = (String) session.getAttribute("searchkey");
				}
				
				String reportId = (String) request.getParameter("id");
				if(reportId==null){ 
				 reportId = (String) session.getAttribute("reportId");
				}

				String pageItem="20";
				StringBuffer strTotal = new StringBuffer();					
				
				if(theForm.getButDelete()!=null&& theForm.getButDelete().equals("Delete Record(s)")){						

					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null || theLogonCredential.allowed(Constants.APPSID,Constants.CARD_ACTIVATION_LOG_DELETE) == false) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					hibernate_session.beginTransaction();
					try{
						String[] genSelBox = theForm.getSelectedMsg();
						CreditCardActivation cca = null;
						String status=Constants.STR_EMPTY;
						
						for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){
							cca=(CreditCardActivation)DBUtilsCrsmsg.get(CreditCardActivation.class, Integer.parseInt(genSelBox[i]), hibernate_session);
							if (cca!=null){
								if(DBUtilsCrsmsg.delete(cca, hibernate_session)){ 											
									status=Constants.STATUS_SUCCESS;
								}else{// fail to delete record 
									status=Constants.STATUS_FAIL;
								}

								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "CardActivationLogDtls Delete", genSelBox[i] ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}
						}

						if(status.equals(Constants.STATUS_SUCCESS)){
							DBUtilsCrsmsg.closeTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}else{
							DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}
						
					}catch(Exception ex){
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}finally{
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
					}
				}
								
				List testList = null;
		    	Query dtlSrch = null;

		    	Session hibernate_session2 = HibernateUtil.getSessionFactory().getCurrentSession();	
		    	hibernate_session2.beginTransaction();
	    		StringBuffer sql = new StringBuffer();
	    		
	    		strTotal.append("select count(id) from CreditCardActivation where ftpReportId = "+reportId+" ");
	   			sql.append("from CreditCardActivation where ftpReportId = "+reportId+" ");
	    		if(seaKey != null  &&  seaKey.length() > 0){	    			
	    			if(selState.equals("cardNo")){
	    				String cardEnc = SecurityManager.getInstance().encrypt(seaKey);
	    				strTotal.append(" and cardNo = '"+cardEnc+"'");
	    				sql.append(" and cardNo = '"+cardEnc+"'");	    				
	    			}else{
	    				strTotal.append(" and "+selState+" like '%"+seaKey+"%'");
	    				sql.append(" and "+selState+" like '%"+seaKey+"%'");
	    			}
	    		}
	    		sql.append("order by id asc FETCH FIRST "+pageItem+" ROWS ONLY ");
	    		
	    		Integer totalItem  = new Integer("0"); 
	    		Integer tt = (Integer) hibernate_session2.createQuery(strTotal.toString()).list().get(0);
	    		totalItem = tt.intValue();
		    	dtlSrch = hibernate_session2.createQuery(sql.toString());
		    	testList =  dtlSrch.setFirstResult(indexFirstResult).setMaxResults(20).list();
				if(testList!=null){
					if(!testList.isEmpty()){
					int count = Integer.valueOf(offset);
					Iterator resultIter = testList.iterator();
					ArrayList dataResult = new ArrayList();
							
						while (resultIter.hasNext()){
							CreditCardActivation modal = (CreditCardActivation) resultIter.next();		
							CardActivationDtlsForm frm = new CardActivationDtlsForm();
							frm.setNo(++count);
							frm.setId(modal.getId());
							frm.setDate(DateUtil.getInstance().stringByDayMonthYear(modal.getCreatedDatetime()));
							frm.setMobileNo(modal.getMobileNo());
							String card = SecurityManager.getInstance().decrypt(modal.getCardNo());
							frm.setCardNumber(CCActivationUtil.maskCCNumber(card));
							frm.setIcNumber(modal.getCustomerId());
							frm.setStatus(modal.getStatus());
							frm.setFileName(getFileName(modal.getFtpReportId()));												 
							dataResult.add(frm);
						}
						
						int act = getCalc(Constants.Y,Integer.valueOf(reportId));
						int inAct = getCalc(Constants.P,Integer.valueOf(reportId));
						int failed = getCalc(Constants.F,Integer.valueOf(reportId));
						theForm.setFileName(getFileName(Integer.valueOf(reportId)));
						theForm.setActivated(act);
						theForm.setNotActivated(inAct);
						theForm.setFailedActivated(failed);
						theForm.setSelectedState(selState);
						theForm.setSearchkey(seaKey);
					

					request.setAttribute("calList",dataResult);
					session.setAttribute("dtldCSV",dataResult);
					session.setAttribute("calcCSV",theForm);
					request.setAttribute("countTotal", totalItem);
					session.setAttribute("reportId",reportId);					
					}					
				}	
				session.setAttribute("selectedState",theForm.getSelectedState());
				session.setAttribute("searchkey",theForm.getSearchkey());

				
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				// Export CSV
				
				String export = request.getParameter("butExport");
				
				try{
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					hibernate_session.beginTransaction();
					CardActivationDtlsForm getForm = (CardActivationDtlsForm) session.getAttribute("calcCSV");
					String getSearch = (String)session.getAttribute("searchkey");
					String getSelected = (String)session.getAttribute("selectedState");
					Query dtlsexp = null;
					List expList = null;
					StringBuffer sqlexp = new StringBuffer();
					sqlexp.append("from CreditCardActivation where ftpReportId = "+session.getAttribute("reportId"));

		    		if(getSearch != null  &&  getSearch.length() > 0){ 
		    			if(getSelected.equals("cardNo")){ 
		    				String cardEnc = SecurityManager.getInstance().encrypt(getSearch);
			    			sqlexp.append(" and "+getSelected+" = '"+cardEnc+"'");
		    			}else{ 
			    			sqlexp.append(" and "+getSelected+" like '%"+getSearch+"%'");
		    			}
		    		}
		    		
			    	dtlsexp = hibernate_session.createQuery(sqlexp.toString());
			    	expList = dtlsexp.list();
					SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
					if (export != null&&export.equals("generateCSV")){
						
						response.reset();
						
			            ServletOutputStream ouputStream = response.getOutputStream();
			                
			                 //--------------------Generate Report---------------
			     		response.setContentType(Constants.CONTENTTYPE_CSV);
			     		response.setHeader(Constants.HEADER,
			                     Constants.STR_FILENAME +
			                     Constants.CSVFILENAME_CARDACTIVATIONLOGDTLS);
			     		ouputStream.print (println("Filename:"+(String)getForm.getFileName()));
			     		ouputStream.println();
			     		ouputStream.println();
			     		ouputStream.print (println("Pending Card(s):"+String.valueOf(getForm.getNotActivated())));
			     		ouputStream.println();
			     		ouputStream.print (println("Failed Card(s):"+String.valueOf(getForm.getFailedActivated())));
			     		ouputStream.println();
			     		ouputStream.print (println("Card(s) Activated:"+String.valueOf(getForm.getActivated())));
			     		ouputStream.println();
			     		ouputStream.println();
			     		ouputStream.println();
			     		printHeader(ouputStream);
			     		
			     		if (expList != null){
				     		Iterator iter = expList.iterator();
				     		int countNo = 0;
				     		while (iter.hasNext()){			     			
				     			CreditCardActivation modalExp = (CreditCardActivation) iter.next();		
				     			ouputStream.print (println(String.valueOf(++countNo)));
		     					ouputStream.print (println(DateUtil.getInstance().stringByDayMonthYear(modalExp.getCreatedDatetime())));
	 							ouputStream.print (println(String.valueOf(modalExp.getMobileNo().toString())));
	 							String cardExp = SecurityManager.getInstance().decrypt(modalExp.getCardNo());					
								ouputStream.print (println(String.valueOf(CCActivationUtil.maskCCNumber(cardExp))));
								ouputStream.print (println(String.valueOf(modalExp.getCustomerId())));
				      			ouputStream.print (println(String.valueOf(modalExp.getStatus())));	
				     			ouputStream.println();
				     		}
			     		}
			     		ouputStream.flush();	                
		                ouputStream.close();
		                return null;
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}				
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("cardActivationLogDtls",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
		
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (println("No"));
		ouputStream.print (println("Date"));
		ouputStream.print (println("Mobile No"));
		ouputStream.print (println("Card Number"));
		ouputStream.print (println("IC Number"));
		ouputStream.print (println("Status"));	
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
	public static String getFileName(int id){
		String outString = Constants.STR_EMPTY;		
		try{			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select file_name from ftp_report where id = ? ")
			.setInteger(0, id).list();
			
			if (resultList.size() == 1){
				outString = resultList.get(0).toString();
			}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return outString;
	}
		
	public static int getCalc(String ind, int id){
		int outString = 0;		
		try{			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select count(status) from cc_activation where ftp_report_id = ? and status = ?")
			.setInteger(0, id).setParameter(1, ind).list();
			
			if (resultList.size() == 1){
				outString = Integer.valueOf(resultList.get(0).toString());
			}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return outString;
	}
	
	public static int getBadRecord(int id){
		
		int outString = 0;		
		try{			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			
			List resultList=hibernate_session.createSQLQuery("select count(ftp) from sms_invalid where ftp = ? ")
			.setInteger(0, id).list();
			
			if (resultList.size() == 1){
				outString = Integer.valueOf(resultList.get(0).toString());
			}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return outString;
	}
	
}
