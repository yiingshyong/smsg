package com.bcb.crsmsg.actions;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Query;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.dao.ICreditCardActivationDao;
import com.bcb.crsmsg.forms.CardActivationLogForm;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.util.SgwCoreHibernateUtil;

public class CardActivationLog extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
// //Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.CARD_ACTIVATION_LOG) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		
		try{	
	
			CardActivationLogForm theForm =(CardActivationLogForm) form;
		    List<Object> cardAct;
			
			String offset = request.getParameter(Constants.STR_PAGER_OFFSET);
			if (offset != null && !offset.equals(Constants.STR_EMPTY)) {	
				if (theForm.getButSearch()!= null){
					if (!theForm.getButSearch().equals("Search")){ 
						theForm.setButSearch(Constants.STR_PAGER_OFFSET);
					}else{
						offset = Constants.STR_ZERO;
					}
				}
			}
			
			if (offset == null || offset.equals(Constants.STR_EMPTY)) {
				offset = Constants.STR_ZERO;
			}
			
			int indexFirstResult= Integer.parseInt(offset);
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				session.removeAttribute("cardLog");
				session.removeAttribute("selectedStateLog");
				session.removeAttribute("searchkeyLog");
				session.removeAttribute("dtldCSV");
				session.removeAttribute("calcCSV");
				session.removeAttribute("selectedState");
				session.removeAttribute("searchkey");
				
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){	
				session.removeAttribute("cardLog");
				String selState = theForm.getSelectedState();
				if(selState==null){ 
					selState = (String) session.getAttribute("selectedStateLog");
				}
				
				String seaKey = theForm.getSearchkey();
				if(seaKey==null){ 
					seaKey = (String) session.getAttribute("searchkeyLog");
				}
				String pageItem="20";
				StringBuffer strTotal = new StringBuffer();	
				StringBuffer strCustTotal = new StringBuffer();
				
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_yyyy_MM_dd_hh_mm_ss);
				SimpleDateFormat sdf=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date():sdf.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():sdf.parse(theForm.getSelectedUntilDate());
				
				String fromDate = theFormatter.format(searchFromDate);
				String untilDate = theFormatter.format(searchUntilDate);
				
				if(theForm.getButDelete()!=null&& theForm.getButDelete().equals("Delete Record(s)")){
					
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null || theLogonCredential.allowed(Constants.APPSID,Constants.CARD_ACTIVATION_LOG_DELETE) == false) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					hibernate_session.beginTransaction();
					try{
						String[] genSelBox = theForm.getSelectedMsg();
						FtpReport fp = null;
						String status=Constants.STR_EMPTY;
						
						for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){
							fp=(FtpReport)DBUtilsCrsmsg.get(FtpReport.class, Integer.parseInt(genSelBox[i]), hibernate_session);
							if (fp!=null){ 
								ICreditCardActivationDao ccDao = (ICreditCardActivationDao) SgwCoreHibernateUtil.getBean("ccActivationDao");
								cardAct = ccDao.findByRptId(Integer.parseInt(genSelBox[i])); 
								if(DBUtilsCrsmsg.delete(fp, hibernate_session)){ 
									if(cardAct != null){
										ccDao.deleteByRptId(Integer.parseInt(genSelBox[i]));
									}
									status=Constants.STATUS_SUCCESS;
								}else{// fail to delete record
									status=Constants.STATUS_FAIL;
								}

								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "CardActivationLog Delete", genSelBox[i] ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}
						}

						if(status.equals(Constants.STATUS_SUCCESS)){
							DBUtilsCrsmsg.closeTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}else{
							DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}
					}catch(Exception ex){
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}finally{
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
					}
				}
				
				List testList = null;
		    	Query custSrch = null;
		    	StringBuffer sql = new StringBuffer();
		    	StringBuffer sqlCust = new StringBuffer();
		    	Integer totalItem  = new Integer("0");
		    	Integer tt = null;
		    	Session hibernate_session2 = HibernateUtil.getSessionFactory().getCurrentSession();	
		    	hibernate_session2.beginTransaction();
		    		    	
		    	if (fromDate != null & untilDate != null){
		    		
		    		strTotal.append("select count(f.id) "+
			    				"from FileConversion c, FtpSetup s, FtpReport f "+
			    				"where c.id=s.fileConversion "+
			    				"and c.conversionClassName = 'com.bcb.sgwcore.queuefileprocessor.CCARecordFTPConversion' "+
			    				"and c.status=1 "+
			    				"and s.id = f.ftpSetup "+
			    				"and (f.startDate between '"+fromDate+"' and '"+untilDate+"')") ;
		    		
			    	sql.append("select f.id, f.fileName, f.remark, f.startDate, s.ftpDesc "+
			    				"from FileConversion c, FtpSetup s, FtpReport f "+
			    				"where c.id=s.fileConversion "+
			    				"and c.conversionClassName = 'com.bcb.sgwcore.queuefileprocessor.CCARecordFTPConversion' "+
			    				"and c.status=1 "+
			    				"and s.id = f.ftpSetup "+
			    				"and (f.startDate between '"+fromDate+"' and '"+untilDate+"')") ;	

		    		if(seaKey != null && seaKey.length() > 0){
		    			if(selState.equals("customerId")){
		    				/*strCustTotal.append("select count(distinct(c.ftpReportId)) "+
	    						   	"from CreditCardActivation c, FtpReport f, FtpSetup s "+
	    						   	"where c.ftpReportId = f.id "+
	    						 	"and f.ftpSetup = s.id  "+
	    						   	"and (f.startDate between '"+fromDate+"' and '"+untilDate+"') " +
	    						   	"and c.customerId like '%"+seaKey+"%' ");*/
		    				
		    				strCustTotal.append("select count(distinct(c.ftp_report_id)) "+
	    						   	"from cc_activation c, ftp_report f, ftp_setup s "+
	    						   	"where c.ftp_report_id = f.id "+
	    						 	"and f.ftp_setup = s.ftp_id  "+
	    						   	"and (f.start_date between '"+fromDate+"' and '"+untilDate+"') " +
	    						   	"and c.customer_id like '%"+seaKey+"%' ");
			    				
		    				sqlCust.append("select distinct c.ftpReportId, f.fileName, f.remark, f.startDate, s.ftpDesc "+
		    						   	"from CreditCardActivation c, FtpReport f, FtpSetup s "+
		    						   	"where c.ftpReportId = f.id "+
		    						   	"and f.ftpSetup = s.id  "+
		    						   	"and (f.startDate between '"+fromDate+"' and '"+untilDate+"') " +
		    						   	"and c.customerId like '%"+seaKey+"%' "+
		    							"order by f.startDate asc FETCH FIRST "+pageItem+" ROWS ONLY");
		    			}else{
		    				strTotal.append(" and f."+selState+" like '%"+seaKey+"%'");
		    				sql.append(" and f."+selState+" like '%"+seaKey+"%'");
		    			}
		    		}
		    		
		    		sql.append(" order by f.startDate desc FETCH FIRST "+pageItem+" ROWS ONLY");
		    		
		    		if(seaKey != null && seaKey.length() > 0){
		    			if(selState.equals("customerId")){
//		    				tt = (Integer) hibernate_session2.createQuery(strCustTotal.toString()).list().get(0);
		    				tt = Integer.parseInt(String.valueOf((BigInteger) hibernate_session2.createSQLQuery(strCustTotal.toString()).list().get(0)));
		    				custSrch = hibernate_session2.createQuery(sqlCust.toString()); 
		    			}else{
		    				tt = (Integer) hibernate_session2.createQuery(strTotal.toString()).list().get(0); 
		    				custSrch = hibernate_session2.createQuery(sql.toString());  
		    			}
		    		}else{
		    			tt = (Integer) hibernate_session2.createQuery(strTotal.toString()).list().get(0);
		    			custSrch = hibernate_session2.createQuery(sql.toString()); 
		    		}
		    	}		    		
	    		 
	    		totalItem = tt.intValue();
		    	testList =  custSrch.setFirstResult(indexFirstResult).setMaxResults(20).list();   
				if(testList!=null){
					if(!testList.isEmpty()){
					int count = Integer.valueOf(offset); 
					Iterator resultIter = testList.iterator();
					ArrayList dataResult = new ArrayList();
							
						while (resultIter.hasNext()){									
							Object[] rowResult = (Object[]) resultIter.next();	
							CardActivationLogForm data = new CardActivationLogForm();
							data.setNo(++count);
							data.setId((Integer) rowResult[0]);
							data.setFtpReportId((Integer) rowResult[0]);							
							data.setDate(DateUtil.getInstance().stringByDayMonthYear((Date) rowResult[3]));
							data.setFileName((String)rowResult[1]);							
							int act = getCalc(Constants.Y,(Integer)rowResult[0]);
							int inAct = getCalc(Constants.P,(Integer)rowResult[0]);
							int failed = getCalc(Constants.F,(Integer)rowResult[0]);
							int badrec = getBadRecord((Integer)rowResult[0]);
							data.setActivated(act);
							data.setNotActivated(inAct);
							data.setFailedActivated(failed);
							data.setBadRecord(badrec);
							data.setTotalRecord(act + inAct + failed + badrec);
							data.setType((String) rowResult[4]);	
							data.setRemarks((String)rowResult[2]);
													 
							dataResult.add(data);
						}
						theForm.setSelectedState(selState);
						theForm.setSearchkey(seaKey);
						
					request.setAttribute("calList",dataResult);
					session.setAttribute("cardLog", dataResult);
					request.setAttribute("countTotalLog", totalItem);
					}
				}
				session.setAttribute("selectedStateLog",theForm.getSelectedState());
				session.setAttribute("searchkeyLog",theForm.getSearchkey());
				
				
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				// Export CSV
				String export = request.getParameter("butExport");

				SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
				if (export != null&&export.equals("generateCSV"))
				{
					ArrayList dataList = (ArrayList) session.getAttribute("cardLog");
					response.reset();
					
		            ServletOutputStream ouputStream = response.getOutputStream();
		                
		                 // --------------------Generate
							// Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     Constants.CSVFILENAME_CARDACTIVATIONLOG);
		     		printHeader(ouputStream);
		     		
		     		if (dataList != null){
			     		Iterator iter = dataList.iterator();
			     		while (iter.hasNext()){
			     			CardActivationLogForm model = (CardActivationLogForm) iter.next();
			     			
			      			ouputStream.print (println(String.valueOf(model.getNo())));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getDate(),String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getFileName(), String.class).toString()));
			      			ouputStream.print (println(String.valueOf(model.getTotalRecord())));
			      			ouputStream.print (println(String.valueOf(model.getActivated())));
			      			ouputStream.print (println(String.valueOf(model.getNotActivated())));
			      			ouputStream.print (println(String.valueOf(model.getFailedActivated())));
			      			ouputStream.print (println(String.valueOf(model.getBadRecord())));
			      			ouputStream.print (println(String.valueOf(model.getType())));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getRemarks(), String.class).toString()));
			     			ouputStream.println();
			     		}
		     		}
		     		ouputStream.flush();
	                
	                ouputStream.close();
	                return null;
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("cardActivationLog",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
		
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}

	private void printHeader(ServletOutputStream ouputStream) throws Exception {
		ouputStream.print(println("No"));
		ouputStream.print(println("Date"));
		ouputStream.print(println("File Name"));
		ouputStream.print(println("Total Records"));
		ouputStream.print(println("Activated"));
		ouputStream.print(println("Not Activated"));
		ouputStream.print(println("Failed Activated"));
		ouputStream.print(println("Bad Record"));
		ouputStream.print(println("Type"));
		ouputStream.print(println("Remarks"));
		ouputStream.println();

	}

	private String println(String printData) {
		if (printData != null) {
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}

	public static int getCalc(String ind, int id) {
		int outString = 0;
		try {
			Session hibernate_session = HibernateUtil.getSessionFactory()
					.getCurrentSession();
			hibernate_session.beginTransaction();

			List resultList = hibernate_session
					.createSQLQuery(
							"select count(status) from cc_activation d where ftp_report_id = ? and status = ?")
					.setInteger(0, id).setParameter(1, ind).list();

			if (resultList.size() == 1) {
				outString = Integer.valueOf(resultList.get(0).toString());
			}
			hibernate_session.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outString;
	}

	public static int getBadRecord(int id) {

		int outString = 0;
		try {
			Session hibernate_session = HibernateUtil.getSessionFactory()
					.getCurrentSession();
			hibernate_session.beginTransaction();

			List resultList = hibernate_session.createSQLQuery(
					"select count(ftp) from sms_invalid d where ftp = ? ")
					.setInteger(0, id).list();

			if (resultList.size() == 1) {
				outString = Integer.valueOf(resultList.get(0).toString());
			}
			hibernate_session.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return outString;
	}

}
