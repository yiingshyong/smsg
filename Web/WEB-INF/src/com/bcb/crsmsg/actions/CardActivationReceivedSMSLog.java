package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.CardActivationReceivedSMSForm;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsReceived;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.creditcard.CCActivationUtil;


public class CardActivationReceivedSMSLog extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
    	Session hibernate_session2 = null;
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
//		//Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.CARD_ACTIVATION_RECEIVED_SMS) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		
		try{
		
			CardActivationReceivedSMSForm theForm =(CardActivationReceivedSMSForm) form;
		    List<Object> cardAct;
		    String offset = request.getParameter(Constants.STR_PAGER_OFFSET);
			if (offset != null && !offset.equals(Constants.STR_EMPTY)) {	
				if (theForm.getButSearch()!= null){
					if (!theForm.getButSearch().equals("Search")){ 
						theForm.setButSearch(Constants.STR_PAGER_OFFSET);
					}else{
						offset = Constants.STR_ZERO;
					}
				}
			}
			
			if (offset == null || offset.equals(Constants.STR_EMPTY)) {
				offset = Constants.STR_ZERO;
			}
			
			int indexFirstResult= Integer.parseInt(offset);
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				session.removeAttribute("cardLog");
				session.removeAttribute("selectedStateSMS");
				session.removeAttribute("searchkeySMS");
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
			//	request.removeAttribute("calList");
				session.removeAttribute("cardLog");
				String selState = theForm.getSelectedState();
				if(selState==null){ 
					selState = (String) session.getAttribute("selectedStateSMS");
				}
				
				String seaKey = theForm.getSearchkey();
				if(seaKey==null){ 
					seaKey = (String) session.getAttribute("searchkeySMS");
				}
				String pageItem="20";
				StringBuffer strTotal = new StringBuffer();	
				
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATE_yyyy_MM_dd_hh_mm_ss);
				SimpleDateFormat sdf=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				
				Date searchFromDate = new Date();
				Date searchUntilDate= new Date();
				
				if(theForm.getSelectedFromDate()==null || theForm.getSelectedFromDate().length()==0)
					searchFromDate = new Date();
				else
					searchFromDate = sdf.parse(theForm.getSelectedFromDate());
				
				if (theForm.getSelectedUntilDate() == null || theForm.getSelectedUntilDate().length()==0)
					searchUntilDate = new Date();
				else
					searchUntilDate = sdf.parse(theForm.getSelectedUntilDate());
				
				String fromDate = theFormatter.format(searchFromDate);
				String untilDate = theFormatter.format(searchUntilDate);
				
				if(theForm.getButDelete()!=null&& theForm.getButDelete().equals("Delete Record(s)")){
					
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null || theLogonCredential.allowed(Constants.APPSID,Constants.CARD_ACTIVATION_LOG_DELETE) == false) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}

					Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					hibernate_session.beginTransaction();
					try{
						String[] genSelBox = theForm.getSelectedMsg();
						SmsReceived cca = null;
						String status=Constants.STR_EMPTY;
						
						for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){ 
							cca=(SmsReceived)DBUtilsCrsmsg.get(SmsReceived.class, Integer.parseInt(genSelBox[i]), hibernate_session);
							if (cca!=null){
								if(DBUtilsCrsmsg.delete(cca, hibernate_session)){ 										
									status=Constants.STATUS_SUCCESS;
								}else{// fail to delete record  
									status=Constants.STATUS_FAIL;
								}
								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "CardActivationLog Delete", genSelBox[i] ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}
						}

						if(status.equals(Constants.STATUS_SUCCESS)){
							DBUtilsCrsmsg.closeTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}else{
							DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}
					}catch(Exception ex){
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_CARD_LOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}finally{
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
					}
				}				
				
				List testList = null;
		    	Query custSrch = null;
		    	hibernate_session2 = HibernateUtil.getSessionFactory().getCurrentSession();	
		    	hibernate_session2.beginTransaction();
						    	
		    	if(fromDate != null & untilDate != null){
		    		StringBuffer sql = new StringBuffer();
		    		strTotal.append("select count(r.id) "+
		    				   "from SmsReceived r, AutoAction a "+
		    				   "where (r.receivedDatetime between '"+fromDate+"' and '"+untilDate+"') " +
		    				   "and r.keyword = a.keyword "+
		    				   "and a.actionClassName = 'com.bcb.crsmsg.actions.ActionCCActivation' ");
		    		
		    		sql.append("select r.id, r.receivedDatetime, r.mobileNo, r.remarks, r.message, r.keyword  "+
		    				   "from SmsReceived r, AutoAction a "+
		    				   "where (r.receivedDatetime between '"+fromDate+"' and '"+untilDate+"') " +
		    				   "and r.keyword = a.keyword "+
		    				   "and a.actionClassName = 'com.bcb.crsmsg.actions.ActionCCActivation' ");
		    		
		    		if(seaKey != null && seaKey.length() > 0 ){
		    			String getSearch = seaKey; 
		    			if (selState.equals(Constants.MESSAGE)){ 
		    			    String msg = StringUtils.remove(getSearch, "-");
		    			    msg = StringUtils.remove(msg, "&");
		    			    msg = StringUtils.remove(msg, " ");
		    				String searchFinal = SecurityManager.getInstance().encrypt(msg);
		    				strTotal.append(" and r.message like '"+searchFinal.trim()+"%'");
		    				sql.append(" and r.message like '"+searchFinal.trim()+"%'");
		    			}else{
		    				strTotal.append(" and r."+selState+" like '%"+getSearch+"%'");
		    				sql.append(" and r."+selState+" like '%"+getSearch+"%'");
		    			}
		    		}
		    		sql.append(" order by r.receivedDatetime desc");
		    		request.getSession().setAttribute(Constants.DBOBJ, sql.toString());
		    		sql.append(" FETCH FIRST "+pageItem+" ROWS ONLY");	
		    		custSrch = hibernate_session2.createQuery(sql.toString()); 
		    	}	
		    	
		    	Integer totalItem  = new Integer("0"); 
	    		Integer tt = (Integer) hibernate_session2.createQuery(strTotal.toString()).list().get(0); 
	    		totalItem = tt.intValue();
		    	testList =  custSrch.setFirstResult(indexFirstResult).setMaxResults(20).list();   
	    		hibernate_session2.getTransaction().commit();		    	
				if(testList!=null){
					if(!testList.isEmpty()){
					int count = Integer.valueOf(offset);
					Iterator resultIter = testList.iterator();
					ArrayList dataResult = new ArrayList();
							
						while (resultIter.hasNext()){
									
							Object[] rowResult = (Object[]) resultIter.next();	
							CardActivationReceivedSMSForm data = new CardActivationReceivedSMSForm();
							data.setNo(++count);
							data.setId((Integer) rowResult[0]);					
							data.setDate(DateUtil.getInstance().stringByDayMonthYear((Date) rowResult[1]));						
							data.setMobileNo((String) rowResult[2]);
							String getMsgOri = SecurityManager.getInstance().decrypt((String)rowResult[4].toString());
//							Keyword key = getKeyword ((Integer)rowResult[5]);
//							String[] cardHolderParams = null;							
//							cardHolderParams = ActionCCActivation.getCardHolderParams(getMsgOri);
//							if(cardHolderParams != null){ 
//								data.setMessage(cardHolderParams[0]+CCActivationUtil.maskCCNumber(cardHolderParams[1]));
//							}else{
//								data.setMessage(getMsgOri);
//							}
							data.setMessage(ActionCCActivation.normalizeCCActivationSMSRequestWithMask(getMsgOri));
							data.setRemarks((String)rowResult[3]);
							dataResult.add(data);
						}
						theForm.setSelectedState(selState);
						theForm.setSearchkey(seaKey);
						
					request.setAttribute("calList",dataResult);
					session.setAttribute("cardLog", dataResult);
					request.setAttribute("countSMS", totalItem);
					}
				}
				session.setAttribute("selectedStateSMS",theForm.getSelectedState());
				session.setAttribute("searchkeySMS",theForm.getSearchkey());
				
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				// Export CSV
				String export = request.getParameter("butExport");

				SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
				if (export != null&&export.equals("generateCSV"))
				{
					String sql = (String) session.getAttribute(Constants.DBOBJ);
					hibernate_session2 = HibernateUtil.getSessionFactory().getCurrentSession();
					ScrollableResults dataList = hibernate_session2.createQuery(sql).setReadOnly(true).setCacheable(false).setFetchSize(Integer.MIN_VALUE).scroll(ScrollMode.FORWARD_ONLY);
					
					
//					List dataList = (List) session.getAttribute(Constants.DBOBJ);
					response.reset();
					
		            ServletOutputStream ouputStream = response.getOutputStream();
		                
		                 //--------------------Generate Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     Constants.CSVFILENAME_CARDACTIVATIONSMSLOG);
		     		printHeader(ouputStream);
		     		
		     		if (dataList != null){
		     			int count=1;
			     		while (dataList.next()){
			     			Object[] fields =  dataList.get();
			      			ouputStream.print (println(String.valueOf(count++)));
			      			ouputStream.print (println(CommonUtils.NullChecker(DateUtil.getInstance().stringByDayMonthYear((Date) fields[1]),String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker((String)fields[2], String.class).toString()));
							String getMsgOri = SecurityManager.getInstance().decrypt((String)fields[4].toString());			      			
			      			ouputStream.print (println(String.valueOf(ActionCCActivation.normalizeCCActivationSMSRequestWithMask(getMsgOri))));
			      			ouputStream.print (println(CommonUtils.NullChecker((String)fields[3],String.class).toString()));
			     			ouputStream.println();
			     		}
		     		}
		     		ouputStream.flush();
	                
	                ouputStream.close();
	                return null;
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			hibernate_session2.getTransaction().rollback();
			ActionMessages messages = new ActionMessages();
			messages.add("cardActivationLog",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session2 != null && hibernate_session2.isOpen()){
				hibernate_session2.close();
			}
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
		
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (println("No"));
		ouputStream.print (println("Date"));
		ouputStream.print (println("Mobile No"));
		ouputStream.print (println("SMS Message"));
		ouputStream.print (println("Remarks"));		
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}		
	
	public static Keyword getKeyword(int id) {

		Keyword keyword = null;
		try {
			Session hibernate_session = HibernateUtil.getSessionFactory()
					.getCurrentSession();
			hibernate_session.beginTransaction();

			List resultList = hibernate_session.createQuery(
					"from Keyword d where id = ? ")
					.setInteger(0, id).list();

			if (resultList.size() == 1) {
				keyword = (Keyword) resultList.get(0);
			}
			hibernate_session.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return keyword;
	}
	

}
