package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.ChannelMaintenanceForm;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ChannelMaintananceAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		ChannelMaintenanceForm theForm = (ChannelMaintenanceForm) form;
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_LOAD)){
			if(StringUtils.isEmpty(theForm.getUserChannel().getUserChannelIdObj().getUserId()) && StringUtils.isNotEmpty(theForm.getUserId())){
				theForm.getUserChannel().getUserChannelIdObj().setUserId(theForm.getUserId());				
			}
			if(StringUtils.isNotEmpty(theForm.getChannelId())){
				theForm.getUserChannel().getUserChannelIdObj().setChannelId(theForm.getChannelId());
				theForm.setUserChannel((UserChannel) DaoBeanFactory.getHibernateTemplate().get(UserChannel.class, theForm.getUserChannel().getUserChannelIdObj()));
			}
		}
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			theForm.getUserChannel().setSmsType(
					(TblRef) DaoBeanFactory.getHibernateTemplate().get(TblRef.class, theForm.getUserChannel().getSmsType().getTblRefId()));
			DaoBeanFactory.getHibernateTemplate().saveOrUpdate(theForm.getUserChannel());
		}		

		theForm.setUserChannels(DaoBeanFactory.getUserChannelDao().getAllUserChannels(theForm.getUserChannel().getUserChannelIdObj().getUserId()));
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
}
