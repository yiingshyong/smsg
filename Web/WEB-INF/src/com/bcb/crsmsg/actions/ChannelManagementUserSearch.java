package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.ChannelMaintenanceForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ChannelManagementUserSearch extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LogonCredential theLogonCredential= (LogonCredential) request.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		if(mapping.getParameter().equals("search")){
			ChannelMaintenanceForm theForm = (ChannelMaintenanceForm) form;
			String searchStr = "%" + CommonUtils.NullChecker(theForm.getUserId(), String.class) + "%";
			theForm.setUsers(DaoBeanFactory.getUserDao().findUserByIdLike(searchStr));			
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));

	}

}
