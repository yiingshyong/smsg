package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.CommonInboxForm;
import com.bcb.crsmsg.modal.InboxModal;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public class CommonInbox extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.COMMON_INBOX) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
				CommonInboxForm theForm =(CommonInboxForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());

				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "r.mobile_no";						 
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_CUST_REF_NO)){
					queryParam = "c.cust_ref_no";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_KEYWORD)){
					queryParam = "k.keyword_name";
				}else{// Can't find match selected state
                }
				/* Define search selected state*/
				
				/* Get total no. of sms */
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(r.sms_id) from sms_received r left outer join customer c on r.mobile_no=c.mobile_no left outer join keyword k on r.keyword=k.keyword_id where (k.keyword_name is null or k.keyword_name ='') and (r.received_datetime between ? and ?)"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")", 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT}).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());

				sql.append("select r.sms_id, r.received_datetime, r.mobile_no, c.cust_ref_no, k.keyword_name, r.message, r.remarks ");
				sql.append("from sms_received r left outer join customer c on r.mobile_no=c.mobile_no left outer join keyword k " +
						   "on r.keyword=k.keyword_id where (k.keyword_name is null or k.keyword_name ='') and (r.received_datetime between ? and ?) "+
						   " and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+") ");
				sql.append("order by r.received_datetime desc LIMIT "+offset+", "+pageSize);
			
				resultList= DBUtilsCrsmsg.DBSQLCommand(
						sql.toString(),
						new String[]{"r.sms_id", "r.received_datetime", "r.mobile_no", "c.cust_ref_no", "k.keyword_name", "r.message", "r.remarks"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});
				/*while(offset>resultList.size()){
					offset-=20;
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				if(CommonUtils.NullChecker(theForm.getSearchkey(), String.class).toString().length()==0){// empty search key
					
				}else{// contain search key
					
				}
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<InboxModal> dataResult = new ArrayList<InboxModal>();
					int runningCounter = 0;

					while (resultIter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) resultIter.next();	
						InboxModal outBox= new InboxModal();
						outBox.setId(runningCounter);
						outBox.setSeq(String.valueOf(runningCounter));
						outBox.setReceivedDateTime( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						outBox.setMobileNo( (String) rowResult[2] );
						outBox.setCustRefNo( (String) rowResult[3] );
						outBox.setKeyword((String) rowResult[4]  );
						outBox.setMessage( (String) rowResult[5] );
						outBox.setRemarks((String) rowResult[6]  );
					    dataResult.add(outBox);
					}
					request.setAttribute(Constants.INBOX_RESULT_KEY, dataResult);
					session.setAttribute(Constants.INBOX_KEY, dataResult);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
				ArrayList dataList = (ArrayList) session.getAttribute(Constants.INBOX_KEY);
				response.reset();
				
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_COMMON_INBOXREPORT );
	     		printHeader(ouputStream);
	     		
	     		if (dataList != null){
		     		Iterator iter = dataList.iterator();
		     		
		     		while (iter.hasNext()){
		     			InboxModal model = (InboxModal) iter.next();
		     			 
		      			ouputStream.print (CommonUtils.println(model.getSeq()));
		      			ouputStream.print (CommonUtils.println(model.getReceivedDateTime()));
		      			ouputStream.print (CommonUtils.println(model.getMobileNo()));
		      			ouputStream.print (CommonUtils.println(model.getCustRefNo()));
		      			ouputStream.print (CommonUtils.println(model.getMessage()));
		      			ouputStream.print (CommonUtils.println(model.getRemarks()));

		     			ouputStream.println();
		     		}
	     		}
	     		ouputStream.flush();
                
                ouputStream.close();
                return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_COMMON_INBOX+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_COMMON_INBOX,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Received Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Cust Ref No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Remarks"));
		ouputStream.println();
	}
}