

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.CustomerSearchForm;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class CustomerSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.ADDRESS_BOOK) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
		
		try{
			CustomerSearchForm custform =  (CustomerSearchForm) form;
						
			if(mapping.getParameter().equals("load")){
			}else{			
				String custQuery = "";
				String allQuery = "";
				//ArrayList<String> conditionQuery=new ArrayList<String>();
				List custList=null;
				//Customer Query
				
				allQuery = "from Customer ";//custRefNo
								
				if (custform.getSelectedState().equals(Constants.FORM_FIELD_MOBILE_NO)){ 
					custQuery = "where mobileNo like ?";
				}else if (custform.getSelectedState().equals(Constants.FORM_LABEL_SMS_STATUS)){
					custQuery = "where smsStatus like ?";
				}else if (custform.getSelectedState().equals(Constants.FORM_FIELD_CUST_REF_NO)){
					custQuery = "where custRefNo like ?";
				}else{
					//do nothing
				}
				allQuery = allQuery + custQuery;
				custList = DBUtilsCrsmsg.DBHQLCommand(allQuery, new Object[]{Constants.STR_PERCENT +custform.getSearchkey()+ Constants.STR_PERCENT}, hibernate_session);
				 
				
				request.setAttribute("custList", custList);
				
				if(custform.getDeleteBtn()!=null&& custform.getDeleteBtn().equals("Delete Selected Customer(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					String[] genSelBox = custform.getSelectedMsg();
					
					int genCnt = 1;
					List genMsgList = null;
					Customer  cust = new Customer();					
					System.out.println("genSelBox.length: "+genSelBox.length);
					for(int i=0;i<genSelBox.length;i++){
						genMsgList=DBUtilsCrsmsg.DBHQLCommand("from Customer where id = ?", new Object[]{Integer.parseInt(genSelBox[i])}, hibernate_session);

						if (genMsgList.size()==1){
							cust = (Customer)genMsgList.get(0);		
							DBUtilsCrsmsg.delete(cust, hibernate_session);
							
							SmsAuditTrail anAuditTrail = new SmsAuditTrail();
							anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "ReceivedLog Delete",Integer.toString(cust.getId()) ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
							DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						}
						genCnt++;
					}
					
					ActionMessages messages = new ActionMessages();
					messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORDDELETE));
					this.addErrors(request,messages);
				}
			}	
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_CUSTOMER_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_CUSTOMER_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}	
	
}
