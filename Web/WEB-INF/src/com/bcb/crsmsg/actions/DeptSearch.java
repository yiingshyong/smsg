

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.DeptSearchForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class DeptSearch extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		try
		{
			// Extract attributes we will need
			HttpSession session = request.getSession();
			
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
				
			}
			
			if ( theLogonCredential.allowed(com.bcb.common.util.Constants.APPSID,com.bcb.common.util.Constants.DEPTMGMT_PERM) == false){
				return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
			}

			DeptSearchForm gsform =  (DeptSearchForm) form;
			
			if(gsform.getSearchBtn()==null){
				
			}else{
				List result = null; 
				ArrayList<String> mainQuery=new ArrayList<String>();
				ArrayList<String> conditionQuery=new ArrayList<String>();
				ArrayList<String> orderQuery=new ArrayList<String>();
				ArrayList<Object> queryParam=new ArrayList<Object>();
				
				mainQuery.add("select d.dept_id, d.dept_name, d.dept_description,d.dept_status, s.quota, s.total_quota from department d left outer join sms_quota s on d.dept_id=s.ref");
				
				if (gsform.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT_NAME)){
					conditionQuery.add("d.dept_name like ?");
					queryParam.add(gsform.getSearchkey() + Constants.STR_PERCENT);
				}
				
				// Order criteria
				orderQuery.add("d.dept_name");
				
				result = DBUtilsCrsmsg.DBSQLCommand(DBUtilsCrsmsg.generateQuery(mainQuery, conditionQuery, orderQuery), queryParam.toArray());
				gsform.setResultList(CommonUtils.populateResult(result));
			}
		}catch (Exception E){
			E.printStackTrace();
			log4jUtil.error("DeptSearch System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add("DeptSearch",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}
		return (mapping.findForward("success"));
	}
	
}
