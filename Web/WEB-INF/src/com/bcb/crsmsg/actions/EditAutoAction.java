
package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.AutoActionForm;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class EditAutoAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
		hibernate_session.beginTransaction();
		
		try{
									
			if(mapping.getParameter().equals("load")){
				String recId = request.getParameter("id");
				AutoActionForm autoForm = (AutoActionForm) form;
				AutoAction  autoActionMod = new AutoAction();
				System.out.println("recId---"+recId);
				List autoId = DBUtilsCrsmsg.DBHQLCommand("from AutoAction where id like ? ", new Object[]{Integer.valueOf(recId)});
				
				if(autoId.size()==1){
					autoActionMod = (AutoAction)autoId.get(0);
					
				}
				request.setAttribute("autoActionDetails", autoId);
				autoForm.setDescription(autoActionMod.getDescription());
				autoForm.setActionClassName(autoActionMod.getActionClassName());
				autoForm.setActionSequence(autoActionMod.getActionSequence());
				autoForm.setDefaultAction(autoActionMod.getDefaultAction());
				autoForm.setServerIP(autoActionMod.getServerIP());
				autoForm.setServerPort(String.valueOf(autoActionMod.getServerPort()));
				autoForm.setServerResponseTimeout(String.valueOf(autoActionMod.getServerResponseTimeout()));
				autoForm.setMaxFailureAttempt(String.valueOf(autoActionMod.getMaxFailureAttempt()));
				autoForm.setSvcWindow(autoActionMod.getSvcWindow());
				autoForm.setFailureCode(autoActionMod.getFailureCode());
				autoForm.setSuccessCode(autoActionMod.getSuccessCode());
				autoForm.setUnavailabilitySms(autoActionMod.getUnavailabilitySms());
				autoForm.setParameterKey(autoActionMod.getParameterKey());
				
			}else{
				
			}	
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_AUTOACTION_EDIT+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ACTION_AUTOACTION_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				
				hibernate_session.close();
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
