package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.ContactForm;
import com.bcb.crsmsg.modal.Contact;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditContact extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.ADDRESS_BOOK) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			ContactForm theForm = (ContactForm) form;
			Contact theContact= (Contact) DBUtilsCrsmsg.get(Contact.class, Integer.valueOf(theForm.getContactId()));
			
			theForm.setCustRefNo(theContact.getRefNo());
			theForm.setDesignation(theContact.getDesignation());
			theForm.setMobileNo(theContact.getMobileNo());
			theForm.setName(theContact.getName());
			theForm.setSelected_department(theContact.getDepartment());
			theForm.setSelected_type(theContact.getType());
			
			//		 ------------------------------ FOR Department -------------------------------
			theForm.setSelected_department(theContact.getDepartment());
			List group_result = DBUtilsCrsmsg.DBHQLCommand("from Department where deptStatus = ? order by id", new Object[]{Constants.STATUS_ACTIVE});
			
			ArrayList<String> group_label = new ArrayList<String>();
			ArrayList<Integer> group_value = new ArrayList<Integer>();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				Department aGroup = (Department) group_result.get(i); 
				group_label.add(aGroup.getDeptName());
				group_value.add(Integer.valueOf(aGroup.getId()));
				
			}
			theForm.setDepartment_label(group_label);
			theForm.setDepartment_value(group_value);
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_USER_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
