package com.bcb.crsmsg.actions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.FTPSetupForm;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditFTPSetup extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.FTP_SETUP) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			FTPSetupForm theForm = (FTPSetupForm) form;
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
			SmsCfg theCfg=((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION)));
			
			if(theCfg!=null){
				FtpSetup theSetup= (FtpSetup) DBUtilsCrsmsg.get(FtpSetup.class, Integer.valueOf(theForm.getFtpId()));
				
				if(theSetup.getFtpDay()!=null)
					theForm.setFtpDay(theSetup.getFtpDay().split(CommonUtils.escapeDelimiter(CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString())));
				theForm.setFtpDayOfMonth(theSetup.getFtpDayOfMonth());
				theForm.setFtpHour(theSetup.getFtpHour());
				theForm.setFtpHourFrom(theSetup.getFtpHourFrom());
				theForm.setFtpHourTo(theSetup.getFtpHourTo());
				if(theSetup.getFtpMonth()!=null)
					theForm.setFtpMonth(theSetup.getFtpMonth().split(CommonUtils.escapeDelimiter(CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString())));
				theForm.setFtpName(theSetup.getFtpName());
				theForm.setFtpPort(theSetup.getFtpPort());
				theForm.setFtpServer(theSetup.getFtpServer());
				
				String path=CommonUtils.NullChecker(theSetup.getFtpSourceDir(), String.class).toString().length()>0?theSetup.getFtpSourceDir()+File.separator:Constants.STR_EMPTY;
				theForm.setFtpTargetFile(path+theSetup.getFileName());
				theForm.setFtpDesc(theSetup.getFtpDesc());
				theForm.setFtpTime(theSetup.getFtpTime());
				theForm.setFtpTimeMode(theSetup.getFtpTimeMode());
				theForm.setFtpUserName(theSetup.getFtpUserName());
				theForm.setMessageTemplate(CommonUtils.NullChecker(theSetup.getMessageTemplate(), String.class).toString());
				theForm.setNotificationSetupReceiver(theSetup.getNotificationSetupReceiver());
				theForm.setNotificationSetupSender(theSetup.getNotificationSetupSender());
				theForm.setNotificationSetupStatus(theSetup.getNotificationSetupStatus());
				//theForm.setSecurityKey(theSetup.getSecurityKey());
				
				theForm.setSetupStatus(theSetup.getSetupStatus());
				theForm.setSmsAutoApproval(theSetup.getSmsAutoApproval());
				theForm.setSmsScheduled(theSetup.getSmsScheduled());
				if(theSetup.getSmsScheduledTime()!=null)
					theForm.setSmsScheduledTime(theFormatter.format(theSetup.getSmsScheduledTime()));
				theForm.setFileConversion(CommonUtils.NullChecker(theSetup.getFileConversion(), String.class).toString());
				theForm.setFtpMode(theSetup.getFtpMode());
				theForm.setFtpOwner(theSetup.getFtpOwner());
				theForm.setFtpRetryNo(theSetup.getFtpRetryNo());
				theForm.setFtpRetryPeriod(theSetup.getFtpRetryPeriod());
				theForm.setAdvanceDayLimit(CommonUtils.NullChecker(theCfg.getScheduledSmsControlDuration(), Integer.class).toString());

				// Set system datetime
				SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_JAVACSRIPT);
				theForm.setSystemDateTime(theFormat.format(new Date()));
			}else{
				
			}
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_FTPSETUP_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
