package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;

import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.IPForm;


import com.bcb.crsmsg.modal.HttpIpAddress;

import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;


public final class EditIPAddress extends Action {	
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		log4jUtil.info("inside EditIPAddress execute");
		System.out.println("inside editipaddress execute method");
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SECURITY_CONTROL) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
				IPForm theForm = (IPForm) form;
				//System.out.println("the id in editipadd is=="+theForm.getId());
				HttpIpAddress ipaddress = (HttpIpAddress) DBUtilsCrsmsg.get(HttpIpAddress.class, Integer.valueOf(theForm.getId()), hibernate_session);
				
				theForm.setIp(ipaddress.getIp());
				theForm.setDept(ipaddress.getDept());
				theForm.setSystem(ipaddress.getSystem());
				theForm.setRemarks(ipaddress.getRemarks());
				theForm.setSelected_status(ipaddress.getStatus());				
							
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_IPADDRESS_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ACTION_IPADDRESS_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}


}
