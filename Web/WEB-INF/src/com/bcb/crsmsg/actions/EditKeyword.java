package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.forms.KeywordForm;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;
public final class EditKeyword extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.KEYWORD_MGMT) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			
			if(theCfg==null){
			}else{
				KeywordForm theForm = (KeywordForm) form;
				Keyword theKeyword= (Keyword) DBUtilsCrsmsg.get(Keyword.class, Integer.valueOf(theForm.getKeywordId()));
				SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
				theForm.setAutoExpired(theKeyword.getAutoExpired());
				theForm.setAutoReply(theKeyword.getAutoReply());
				theForm.setAutoReplyMessage(theKeyword.getAutoReplyMsg());
				if(CommonUtils.NullChecker(theKeyword.getExpiredDate(), String.class).toString().length()>0)
					theForm.setExpiryDate(theFormat.format(theKeyword.getExpiredDate()));
				theForm.setKeyword(theKeyword.getKeywordName());
				theForm.setRedirectEmail(theKeyword.getRedirectEmail().replaceAll(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()), Constants.SPECIAL_CHAR_NEWLINE));
				theForm.setRedirectMobileNo(theKeyword.getRedirectMobileNo().replaceAll(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()), Constants.SPECIAL_CHAR_NEWLINE));
				theForm.setRedirectURL(theKeyword.getRedirectUrl());
				theForm.setRedirectURLParam(CommonUtils.NullChecker(theKeyword.getRedirectUrlParam(), String.class).toString().replaceAll(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter()), Constants.SPECIAL_CHAR_COMMA));
				theForm.setSelected_charset(theKeyword.getCharSet());
				
				theForm.setMaxRedirect_attempts(theKeyword.getMaxRedirect_attempts());
				theForm.setRedirectInterval(theKeyword.getRedirectInterval());
				
				theForm.setSelected_ownership(theKeyword.getOwnership());
				theForm.setSelected_status(theKeyword.getStatus());
				AutoAction autoAction = DaoBeanFactory.getAutoActionDao().getNonDefaultAutoActionByKeyword(theKeyword.getId());
				if(autoAction != null){
					theForm.setSelectedAutoAction(String.valueOf(autoAction.getId()));						
				}
	
				List user_list = DBUtilsCrsmsg.DBHQLCommand("select distinct u from User u, UserRoles ur, RoleApps ra where u.userStatus = ? and ra.appsId=? and u.userId=ur.userId and ur.roleId=ra.roleId order by u.userName", new Object[]{Constants.STATUS_ACTIVE, Integer.valueOf(Constants.APPSID)});
				
				ArrayList<String> user_label = new ArrayList<String>();
				ArrayList<String> user_value = new ArrayList<String>();
				for (int i =0 ; i < user_list.size(); i ++){
					User aUser = (User) user_list.get(i); 
					user_label.add(aUser.getUserName());
					user_value.add(aUser.getUserId());
					
				}
				theForm.setOwnership_label(user_label);
				theForm.setOwnership_value(user_value);
			}
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_KEYWORD_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_KEYWORD_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
