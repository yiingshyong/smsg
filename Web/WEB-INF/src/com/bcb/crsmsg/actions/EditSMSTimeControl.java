package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SystemCfgForm;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsTimeControl;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditSMSTimeControl extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			SystemCfgForm theForm = (SystemCfgForm) form;
			SmsCfg theCfg= (SmsCfg) DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			if(theCfg!=null){
				List controlList=DBUtilsCrsmsg.DBHQLCommand("from SmsTimeControl where dept=?", new Object[]{Integer.valueOf(CommonUtils.NullChecker(theForm.getDept(), Integer.class).toString())});
				
				if(controlList.size()==1){// Found time control cfg
					SmsTimeControl theControl=(SmsTimeControl)controlList.get(0);
					
					theForm.setDept(CommonUtils.NullChecker(theControl.getDept(), Integer.class).toString());
					theForm.setSmsTimeControlBatch(theControl.getSmsTimeControlBatch());
					theForm.setSmsTimeControlWeb(theControl.getSmsTimeControlWeb());
					System.out.println("Db selected: " + theControl.getAutoPatchToUnsent());
					theForm.setAutoPatchToUnsent(theControl.getAutoPatchToUnsent()? "Y" : "N");
					theForm.setSmsTimeControlDay(CommonUtils.NullChecker(theControl.getSmsTimeControlDay(), String.class).toString().split(CommonUtils.escapeDelimiter(CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString())));
					
					String[] smsTimeControlTimeFrom=CommonUtils.NullChecker(theControl.getSmsTimeControlTimeFrom(), String.class).toString().split(CommonUtils.escapeDelimiter(CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString()));
					String[] smsTimeControlTimeFromHour=new String[smsTimeControlTimeFrom.length];
					String[] smsTimeControlTimeFromMin=new String[smsTimeControlTimeFrom.length];
	
					for(int i=0;smsTimeControlTimeFrom!=null&&i<smsTimeControlTimeFrom.length;i++){
						if(smsTimeControlTimeFrom[i].length()>=4){
							smsTimeControlTimeFromHour[i]=smsTimeControlTimeFrom[i].substring(0, 2);
							smsTimeControlTimeFromMin[i]=smsTimeControlTimeFrom[i].substring(2);
						}
					}
					
					theForm.setSmsTimeControlTimeFromHour(smsTimeControlTimeFromHour);
					theForm.setSmsTimeControlTimeFromMin(smsTimeControlTimeFromMin);
					
					String[] smsTimeControlTimeTo=CommonUtils.NullChecker(theControl.getSmsTimeControlTimeTo(), String.class).toString().split(CommonUtils.escapeDelimiter(CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString()));
					String[] smsTimeControlTimeToHour=new String[smsTimeControlTimeTo.length];
					String[] smsTimeControlTimeToMin=new String[smsTimeControlTimeTo.length];
	
					for(int i=0;smsTimeControlTimeTo!=null&&i<smsTimeControlTimeTo.length;i++){
						if(smsTimeControlTimeTo[i].length()>=4){
							smsTimeControlTimeToHour[i]=smsTimeControlTimeTo[i].substring(0, 2);
							smsTimeControlTimeToMin[i]=smsTimeControlTimeTo[i].substring(2);
						}
					}
					
					theForm.setSmsTimeControlTimeToHour(smsTimeControlTimeToHour);
					theForm.setSmsTimeControlTimeToMin(smsTimeControlTimeToMin);
				}else{// Not found time control cfg
					theForm.setAutoPatchToUnsent("Y");
				}
			}else{
				
			}
		}
		catch (Exception E){	
			log4jUtil.error(Constants.ACTION_SMSTIMECONTROL_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMSTIMECONTROL_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
