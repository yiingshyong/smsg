package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SendSMSForm;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditSms extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		try {
			SendSMSForm theForm =(SendSMSForm)form;
			
			// Set system datetime
			SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_JAVACSRIPT);
			theForm.setSystemDateTime(theFormat.format(new Date()));
			
			Sms theSms=(Sms)DBUtilsCrsmsg.get(Sms.class, Integer.valueOf(CommonUtils.NullChecker(theForm.getSmsId(), Integer.class).toString()));
			
			if(theSms!=null){
				theForm.setMobileNo(theSms.getMobileNo());
				theForm.setScheduledSMS(theSms.getSmsScheduled());

				theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				theForm.setScheduledSMSDateTime(theSms.getSmsScheduledTime()!=null?theFormat.format(theSms.getSmsScheduledTime()):Constants.STR_EMPTY);
				
				theForm.setSelected_charset(theSms.getMsgFormatType());
				theForm.setSmsContent(theSms.getMessage());
				theForm.setSmsId(CommonUtils.NullChecker(theSms.getId(), Integer.class).toString());
				theForm.setSmsPriority(CommonUtils.NullChecker(theSms.getPriority(), String.class).toString());
				theForm.setSmsPriorityShow(theLogonCredential.getRights().contains(Integer.valueOf(Constants.SMS_PRIORITY_CHANGE))?Constants.STATUS_TRUE_1:null);
			}else{// No SMS found
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_MSG_EDIT,new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
				this.addErrors(request,messages);
			}
			// Set maximum SMS per message
			SmsCfg theCfg = (SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			
			if(theCfg!=null){
				theForm.setConcatenateSMS(theCfg.getConcatenateSms());
				theForm.setMaxNoOfSMS(CommonUtils.NullChecker(theCfg.getConcatenateSmsNo(), Integer.class).toString());
				theForm.setCharPerSms(CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString());
			}else{// Error, no sms cfg found
				theForm.setMaxNoOfSMS(Constants.VALUE_1);
				theForm.setCharPerSms(Constants.VALUE_0);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_MSG_EDIT,new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND_WITHREASON, "SmsCfg not exist!"));
				this.addErrors(request,messages);
			}
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_MSG_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_MSG_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
