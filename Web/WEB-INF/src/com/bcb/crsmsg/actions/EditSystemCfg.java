package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SystemCfgForm;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditSystemCfg extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			SystemCfgForm theForm = (SystemCfgForm) form;
			SmsCfg theCfg= (SmsCfg) DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			
			theForm.setAutoSmsFailure(theCfg.getAutoSmsFailure());
			theForm.setAutoSmsResend(theCfg.getAutoSmsResend());
			theForm.setAutoSmsResendInterval(CommonUtils.NullChecker(theCfg.getAutoSmsResendInterval(), String.class).toString());
			theForm.setAutoSmsResendLimit(theCfg.getAutoSmsResendLimit());
			theForm.setBulkSmsThreshold(theCfg.getBulkSmsThreshold());
			theForm.setBulkSmsThresholdNotification(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotification(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())));
			theForm.setBulkSmsThresholdNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setBulkSmsThresholdNotificationEmailSender(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationEmailSender(), String.class).toString());
			theForm.setBulkSmsThresholdNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setBulkSmsThresholdNotificationSmsSender(CommonUtils.NullChecker(theCfg.getBulkSmsThresholdNotificationSmsSender(), String.class).toString());
			theForm.setQueueSmsThreshold(theCfg.getQueueSmsThreshold());
			theForm.setQueueSmsThresholdNotification(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotification(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())));
			theForm.setQueueSmsThresholdNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setQueueSmsThresholdNotificationEmailSender(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationEmailSender(), String.class).toString());
			theForm.setQueueSmsThresholdNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setQueueSmsThresholdNotificationSmsSender(CommonUtils.NullChecker(theCfg.getQueueSmsThresholdNotificationSmsSender(), String.class).toString());
			theForm.setQueueSmsThresholdPeriod(theCfg.getQueueSmsThresholdPeriod());
			theForm.setQueueSmsThresholdTime(theCfg.getQueueSmsThresholdTime());
			theForm.setTotalQueuedSizeAlertCheckPeriod(String.valueOf(theCfg.getTotalQueuedSizeAlertCheckPeriod()));
			theForm.setTotalQueuedSizeAlertThreshold(String.valueOf(theCfg.getTotalQueuedSizeAlertThreshold()));
			theForm.setCfgId(theCfg.getId().toString());
			theForm.setConcatenateSms(theCfg.getConcatenateSms());
			theForm.setConcatenateSmsNo(theCfg.getConcatenateSmsNo());
			theForm.setEmailFormat(theCfg.getEmailFormat());
			theForm.setFtpFailureNotification(CommonUtils.NullChecker(theCfg.getFtpFailureNotification(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())));
			theForm.setFtpFailureNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getFtpFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setFtpFailureNotificationEmailSender(CommonUtils.NullChecker(theCfg.getFtpFailureNotificationEmailSender(), String.class).toString());
			theForm.setFtpFailureNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setFtpFailureNotificationSmsSender(CommonUtils.NullChecker(theCfg.getFtpFailureNotificationSmsSender(), String.class).toString());
			theForm.setHttpSmsService(theCfg.getHttpSmsService());
			theForm.setIpControl(theCfg.getIpControl());
			theForm.setIpSkip(theCfg.getIpSkip());
			theForm.setMobileCountryPrefix(theCfg.getMobileCountryPrefix());
			theForm.setScheduledSmsControlDuration(theCfg.getScheduledSmsControlDuration());
			theForm.setSmsFailureThreshold(theCfg.getSmsFailureThreshold());
			theForm.setSmsFailureThresholdPeriod(theCfg.getSmsFailureThresholdPeriod());
			theForm.setSmsFailureThresholdTime(theCfg.getSmsFailureThresholdTime());
			theForm.setSmsFailureNotification(CommonUtils.NullChecker(theCfg.getSmsFailureNotification(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())));
			theForm.setSmsFailureNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmail(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setSmsFailureNotificationEmailSender(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationEmailSender(), String.class).toString());
			theForm.setSmsFailureNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationSms(), String.class).toString().split(CommonUtils.escapeDelimiter(theCfg.getTableListDelimiter())), Constants.SPECIAL_CHAR_NEWLINE));
			theForm.setSmsFailureNotificationSmsSender(CommonUtils.NullChecker(theCfg.getSmsFailureNotificationSmsSender(), String.class).toString());
			theForm.setSmsPrioritize(theCfg.getSmsPrioritize());
			theForm.setSmsQuota(theCfg.getSmsQuota());
			theForm.setSmsQuotaDefault(CommonUtils.NullChecker(theCfg.getSmsQuotaDefault(), String.class).toString());
			theForm.setPasswordChangeDuration(theCfg.getPasswordChangeDuration());
			theForm.setAccountDormancyCheck(theCfg.getAccountDormancyCheck());
			theForm.setSystemLogDuration(theCfg.getSystemLogDuration());
			theForm.setUsageLogDuration(theCfg.getUsageLogDuration());
			theForm.setCcaDataAge(theCfg.getCcaDataAge());
			theForm.setDefaultSmsType(theCfg.getDefaultSMSType());
			theForm.setSystemSMSDefaultSender(theCfg.getSystemSMSSender());
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_SYSTEMCFG_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SYSTEMCFG_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}

