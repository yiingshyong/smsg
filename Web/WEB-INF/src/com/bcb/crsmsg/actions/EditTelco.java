
package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.TelcoForm;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
public final class EditTelco extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
		hibernate_session.beginTransaction();
		
		try{
									
			if(mapping.getParameter().equals("load")){
				
				TelcoForm telco = (TelcoForm) form;
				Telco  telcoMod = new Telco();
						
				List telcoId = DBUtilsCrsmsg.DBHQLCommand("from Telco where id like ? ", new Object[]{Integer.parseInt(telco.getId())});
				
				if(telcoId.size()==1){
					telcoMod = (Telco)telcoId.get(0);
					
				}
				request.setAttribute("telcoDetails", telcoId);
				telco.setTelco_name(telcoMod.getTelcoName());
				telco.setUrl(telcoMod.getUrl());
				telco.setUrlParam(telcoMod.getUrlParam());
				telco.setClassName(telcoMod.getTelcoClassName());
				
				telco.setInter_cost(CommonUtils.NullChecker(telcoMod.getCostPerSmsInter(),String.class).toString());
				telco.setWithin_cost(CommonUtils.NullChecker(telcoMod.getCostPerSmsWithin(),String.class).toString());
				
				telco.setIncoming_cost(CommonUtils.NullChecker(telcoMod.getCostIncoming(),String.class).toString());
				
				telco.setUsername(telcoMod.getUserId());
				telco.setPassword(telcoMod.getPassword());
				
				
				telco.setMobile_prefix(BusinessService.normalizeMobile(CommonUtils.NullChecker(telcoMod.getTelcoPrefix(),String.class).toString().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE)));
//				telco.setHome_prefix(BusinessService.normalizeMobile(CommonUtils.NullChecker(telcoMod.getPrefixWithin(),String.class).toString().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE)));
//				telco.setIncoming_prefix(BusinessService.normalizeMobile(CommonUtils.NullChecker(telcoMod.getPrefixIncoming(),String.class).toString().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE)));
				
				//telco.setMobile_prefix(telcoMod.getTelcoPrefix().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE));
				//telco.setHome_prefix(telcoMod.getPrefixWithin().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE));
				
				telco.setEnabled_st(CommonUtils.NullChecker(telcoMod.getSmsQueueThreshold(),String.class).toString());
				telco.setQueue_threshold(CommonUtils.NullChecker(telcoMod.getSmsQueueThresholdNo(),String.class).toString());
				telco.setEmail_from(CommonUtils.NullChecker(telcoMod.getThresholdNotificationSender(),String.class).toString());
				telco.setEmail_to(CommonUtils.NullChecker(telcoMod.getThresholdNotificationReceiver(),String.class).toString().replace(Constants.SPECIAL_CHAR_PIPE,Constants.SPECIAL_CHAR_NEWLINE));
				telco.setEnable_notification(CommonUtils.NullChecker(telcoMod.getThresholdNotification(),String.class).toString());
				telco.setSelected_status(telcoMod.getStatus());
				telco.setProxyAddr(telcoMod.getProxyAddr());
				telco.setProxyPort(telcoMod.getProxyPort());
				telco.setSelectedSvcProvider(telcoMod.getSvcPvdId());
				telco.setEncryption(telcoMod.getEncryptionStatus());
				
				telco.setWithin_cost(telcoMod.getCostPerSmsWithin());
				telco.setInter_cost(telcoMod.getCostPerSmsInter());
				telco.setShortCode(telcoMod.getShortCode());
				telco.setSuccessCode(telcoMod.getSuccessCode());
				telco.setRetryCode(telcoMod.getRetryCode());
				telco.setSmsLimit(telcoMod.getSmsOnHandLimit());
				
			}else{
				
			}	
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_ADDRESSBOOK_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_ADDRESSBOOK_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				
				hibernate_session.close();
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
