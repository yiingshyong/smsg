package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.TemplateMessageForm;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditTemplateMessage extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.TEMPLATE_MSG) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			TemplateMessageForm theForm = (TemplateMessageForm) form;
			MsgTemplate theMsg= (MsgTemplate) DBUtilsCrsmsg.get(MsgTemplate.class, Integer.valueOf(theForm.getTemplateId()));

			theForm.setDynamicFieldName(theMsg.getDynamicFieldName().replaceAll("["+((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION))).getTableListDelimiter()+"]", Constants.SPECIAL_CHAR_COMMA));
			theForm.setDynamicFieldNo(theMsg.getDynamicFieldNo());
			theForm.setMessageCode(theMsg.getTemplateCode());
			theForm.setMessageText(theMsg.getTemplateContent());
			theForm.setSelected_department(theMsg.getDepartment());
			theForm.setSelected_status(theMsg.getStatus());
			theForm.setSelected_type(theMsg.getType());
			theForm.setSelected_charset(theMsg.getMsgFormatType());
			
			//		 ------------------------------ FOR Department -------------------------------
			theForm.setSelected_department(theMsg.getDepartment());
			List group_result = null;
			
			if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL))){
				group_result = DBUtilsCrsmsg.DBHQLCommand("from Department where deptStatus = ? order by id", new Object[]{Constants.STATUS_ACTIVE});
			}else{
				group_result = DBUtilsCrsmsg.DBHQLCommand("from Department where deptStatus = ? and id=? order by id", new Object[]{Constants.STATUS_ACTIVE, Integer.valueOf(theLogonCredential.getUserGroup())});
			}
			
			ArrayList<String> group_label = new ArrayList<String>();
			ArrayList<Integer> group_value = new ArrayList<Integer>();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				Department aGroup = (Department) group_result.get(i); 
				group_label.add(aGroup.getDeptName());
				group_value.add(Integer.valueOf(aGroup.getId()));
				
			}
			theForm.setDepartment_label(group_label);
			theForm.setDepartment_value(group_value);
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_TEMPLATEMESSAGE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_TEMPLATEMESSAGE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
