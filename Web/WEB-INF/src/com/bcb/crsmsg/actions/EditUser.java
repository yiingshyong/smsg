
package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.RoleRef;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.forms.UserForm;
import com.bcb.crsmsg.modal.RightsRef;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class EditUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.USER_SEARCH) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		try {
			// Generate the dump ref no
			//String userId = request.getParameter(Constants.PARAM_USERID);
			UserForm edituserform = (UserForm) form;
			
			/*if (userId == null || userId.length() < 1)
			 userId = edituserform.getUserid();
			 */
			
			User theUser= (User) DBUtilsCrsmsg.get(User.class, edituserform.getUserid());
			
			SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
			SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
			
			if ( theUser.getModifiedDate() != null) 
				edituserform.setModifieddate(formatter.format(theUser.getModifiedDate()));
			if ( theUser.getCreatedDate() != null) 
				edituserform.setCreateddate(formatter.format(theUser.getCreatedDate()));
			if ( theUser.getModifiedTime() != null)
				edituserform.setModifiedtime( (time_formatter.format(theUser.getModifiedTime())));
			if ( theUser.getCreatedTime() != null)
				edituserform.setCreatedtime( (time_formatter.format(theUser.getCreatedTime())));
			if ( theUser.getLastLoginDate() != null)
				edituserform.setLastLoginDate((formatter.format(theUser.getLastLoginDate())));
			
			edituserform.setCreatedby(theUser.getCreatedBy());
			edituserform.setModifiedby(theUser.getModifiedBy());
			
			if ( theUser.getLastPwdchg() != null)
				edituserform.setLastpwdchg(formatter.format(theUser.getLastPwdchg()));
			if ( theUser.getFailedLogon() != null)
				edituserform.setFailedlogon(theUser.getFailedLogon().toString());
			else
				edituserform.setFailedlogon("0");
			edituserform.setBizphone(theUser.getBizPhone());
			edituserform.setEmailid(theUser.getEmailId());
			edituserform.setExt(theUser.getExt());
			edituserform.setHphone(theUser.getHPhone());
			edituserform.setUsername(theUser.getUserName());
			edituserform.setStaffid(theUser.getStaffId());
			edituserform.setSelected_userstatus(theUser.getUserStatus());
			ArrayList status_label = new ArrayList();
			 status_label.add(com.bcb.common.util.Constants.STATUS_ACTIVE_LABEL);
			 status_label.add(com.bcb.common.util.Constants.STATUS_DISABLED_LABEL);
			 status_label.add(com.bcb.common.util.Constants.STATUS_CLOSED_LABEL);
			edituserform.setUserstatus_label(status_label);
			ArrayList status_value = new ArrayList();
			 status_value.add(com.bcb.common.util.Constants.STATUS_ACTIVE_LABEL);
			 status_value.add(com.bcb.common.util.Constants.STATUS_DISABLED_LABEL);
			 status_value.add(com.bcb.common.util.Constants.STATUS_CLOSED_LABEL);
			edituserform.setUserstatus_value(status_value);
			
			/*
			 // ------------------------------ FOR BRANCH -------------------------------
			  edituserform.setSelected_branch(Integer.valueOf(theUser.getBranchDept()));
			  List branch_result = hibernate_session.createQuery("from Branch where branch_status = 'Active' order by branch_code").list();
			  ArrayList branch_label = new ArrayList();
			  ArrayList branch_value = new ArrayList();
			  for (int i =0 ; i < branch_result.size(); i ++)
			  {
			  Branch aBranch = (Branch) branch_result.get(i); 
			  branch_label.add(aBranch.getBranchCode() + " - " +aBranch.getBranchName());
			  branch_value.add(Integer.valueOf(aBranch.getBranchId()));
			  
			  }
			  edituserform.setBranch_label(branch_label);
			  edituserform.setBranch_value(branch_value);*/
			// ------------------------------ FOR BRANCH -------------------------------
			
			//		 ------------------------------ FOR Department -------------------------------
			edituserform.setSelected_department(Integer.valueOf(theUser.getUserGroup()));
			List group_result = DBUtilsCrsmsg.DBHQLCommand("from Department where deptStatus = ? order by id", new Object[]{Constants.STATUS_ACTIVE});
			
			ArrayList<String> group_label = new ArrayList<String>();
			ArrayList<Integer> group_value = new ArrayList<Integer>();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				Department aGroup = (Department) group_result.get(i); 
				group_label.add(aGroup.getDeptName());
				group_value.add(Integer.valueOf(aGroup.getId()));
				
			}
			edituserform.setDepartment_label(group_label);
			edituserform.setDepartment_value(group_value);
			//	 ------------------------------ FOR Group -------------------------------
			//	 ------------------------------ FOR Role -------------------------------
			//edituserform.setSelected_role(Integer.valueOf(theUser.getRole()));
			List role_result = DBUtilsCrsmsg.DBHQLCommand("from RoleRef where role_status = ? order by role_name", new Object[]{Constants.STATUS_ACTIVE});
			ArrayList<String> role_label = new ArrayList<String>();
			ArrayList<Integer> role_value = new ArrayList<Integer>();
			for (int i =0 ; i < role_result.size(); i ++)
			{
				RoleRef aRole= (RoleRef) role_result.get(i); 
				role_label.add(aRole.getRoleName());
				role_value.add(Integer.valueOf(aRole.getRoleId()));
				
			}
			edituserform.setRole_label(role_label);
			edituserform.setRole_value(role_value);
			//		 ------------------------------ FOR Role -------------------------------
			
//			------------------------------ FOR User Roles -------------------------------
			String selectedRoles =edituserform.getStrSelected_roles();
			
			if (selectedRoles !=null && selectedRoles.length()> 0){
				String[] rolesArray = selectedRoles.split(",");
				ArrayList<String> selectedRoleLabel=new ArrayList<String>();
				ArrayList<Integer> selectedRoleValue=new ArrayList<Integer>();
				
				for(int i=0;i<rolesArray.length;i++){
					Integer roleValue=Integer.valueOf(rolesArray[i].toString());
					
					selectedRoleValue.add(roleValue);
					RoleRef aRoleRef= (RoleRef)DBUtilsCrsmsg.get(RoleRef.class, roleValue);
					selectedRoleLabel.add(aRoleRef.getRoleName());
				}
				
				edituserform.setSelected_role_label(selectedRoleLabel);
				edituserform.setSelected_role_value(selectedRoleValue);
			}else{
				List userroles_result = DBUtilsCrsmsg.DBHQLCommand("select rr from RoleRef rr, UserRoles ur where ur.status = ? and rr.roleStatus=? and ur.userId=? and ur.roleId=rr.roleId order by rr.roleId",
						new Object[]{Constants.STATUS_ACTIVE, Constants.STATUS_ACTIVE, theUser.getUserId()});
				ArrayList<String> selected_role_label = new ArrayList<String>();
				ArrayList<Integer> selected_role_value = new ArrayList<Integer>();
				for (int i =0 ; i < userroles_result.size(); i ++){
					RoleRef aRole= (RoleRef) userroles_result.get(i); 
					selected_role_label.add(aRole.getRoleName());
					selected_role_value.add(Integer.valueOf(aRole.getRoleId()));
				}
				
				edituserform.setSelected_role_label(selected_role_label);
				edituserform.setSelected_role_value(selected_role_value);
			}
			
//			Populate Additional Rights
			List rights_list=DBUtilsCrsmsg.DBHQLCommand("from RightsRef order by rightsName");
			ArrayList<String> rights_label = new ArrayList<String>();
			ArrayList<Integer> rights_value = new ArrayList<Integer>();
			for (int i =0 ; i < rights_list.size(); i ++)
			{
				RightsRef aRight= (RightsRef) rights_list.get(i); 
				rights_label.add(aRight.getRightsName());
				rights_value.add(Integer.valueOf(aRight.getId()));
				
			}
			edituserform.setRights_label(rights_label);
			edituserform.setRights_value(rights_value);
			
			List<Integer> user_right_list=DBUtilsCrsmsg.DBHQLCommand("select rightsId from UserRights where user.userId=?", new Object[]{theUser.getUserId()});
			Integer selectedRightList[] = new Integer[user_right_list.size()];
			selectedRightList = user_right_list.toArray(selectedRightList);
			edituserform.setSelected_rights(selectedRightList);
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_USER_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
