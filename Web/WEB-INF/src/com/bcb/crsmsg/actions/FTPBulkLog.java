package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.FtpBulkLogForm;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.SmsDelete;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;


public class FTPBulkLog extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
//		//Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.FTP_REPORT) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try{			
			FtpBulkLogForm setupForm =(FtpBulkLogForm) form;

			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listFTPReport", null); 
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				
				if(setupForm.getButSubmit() != null && setupForm.getButSubmit().equals("Search")){
					hibernate_session.beginTransaction();
					String user ="";				
					
					boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
					boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
					boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
					
					if (isAdmin){
						
					}else if (isDept){
						user = getUserList(hibernate_session, theLogonCredential.getUserId());
					}else if(isPersonal){
						user="'"+theLogonCredential.getUserId()+"'";
					}
					
					SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
					String searchFromDate= CommonUtils.NullChecker(setupForm.getSelectedFromDate(), String.class).toString();
					String searchUntilDate= CommonUtils.NullChecker(setupForm.getSelectedUntilDate(), String.class).toString();
					List prList_result = null;
					
					String sql ="";
					if (searchFromDate.length() > 0 || searchUntilDate.length() > 0){
						Date fromDate=searchFromDate.length()>0?theFormatter.parse(searchFromDate):new Date(0);
						Date toDate=searchUntilDate.length()>0?theFormatter.parse(searchUntilDate):new Date();
						
						if (isAdmin){
							sql = "from FtpReport where (startDate between ? and ?) and type = 'F' order by startDate desc";
						}else{
							sql = "from FtpReport where type = 'F' and (startDate between ? and ?) and createdBy in(" + user + ") order by startDate desc";
						}
						prList_result = DBUtilsCrsmsg.DBHQLCommand(sql, new Object[]{fromDate, toDate});				
	
					}else{
						if (isAdmin){
							sql = "from FtpReport where type = 'F' order by startDate desc ";
						}else{
							sql = "from FtpReport where type = 'F' and createdBy in(" + user + ") order by startDate desc ";
						}
						prList_result = DBUtilsCrsmsg.DBHQLCommand(sql); 
					}
	
					ArrayList<FtpReport> dataResult = new ArrayList<FtpReport>();
					for(int i=0;i<prList_result.size();i++){
						FtpReport theReport=(FtpReport)prList_result.get(i);
						theReport.setSent(CommonUtils.getFtpSentCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setUnsent(CommonUtils.getFtpFailedCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setPendingApp(CommonUtils.getFtpPendingApprovalCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setQueue(CommonUtils.getFtpPendingCount(Integer.valueOf(theReport.getId().toString())));
						int total = theReport.getSent() + theReport.getUnsent() + theReport.getPendingApp() + theReport.getQueue();
						theReport.setTotal(total);
						dataResult.add(theReport);
					}
					
					//request.setAttribute("ftpBulkLog", dataResult);
					request.setAttribute("listFTPReport", dataResult); 
					session.setAttribute(Constants.FTPBULKLOG_KEY, dataResult);
				}
				String export = request.getParameter("butExport");
				if (export != null&&export.equals("generateCSV")){
					SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
					ArrayList dataList = (ArrayList) session.getAttribute(Constants.FTPBULKLOG_KEY);
					response.reset();
					
					ServletOutputStream ouputStream = response.getOutputStream();
					
					//--------------------Generate Report---------------
					response.setContentType(Constants.CONTENTTYPE_CSV);
					response.setHeader(Constants.HEADER,
							Constants.STR_FILENAME +
							Constants.CSVFILENAME_FTPBULKLOGREPORT );
					printHeader(ouputStream);
					
					if (dataList != null){
						Iterator iter = dataList.iterator();
						
						
						while (iter.hasNext()){
							FtpReport model = (FtpReport) iter.next();
							
							ouputStream.print (println(model.getId().toString()));
							ouputStream.print (println(model.getFileName()));
							ouputStream.print (println(DateUtil.getInstance().stringByDayMonthYear(model.getStartDate())));
							ouputStream.print (println(time_formatter.format(model.getStartTime())));
							ouputStream.print (println(time_formatter.format(model.getEndTime())));
							ouputStream.print (println(CommonUtils.NullChecker(model.getTotal(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getSent(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getUnsent(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getPendingApp(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getQueue(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getNoFailed(), Integer.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getCreatedBy(), String.class).toString()));
							ouputStream.print (println(CommonUtils.NullChecker(model.getDept(), String.class).toString()));
							ouputStream.print (println(model.getRemark()));
							ouputStream.println();
						}
					}
					ouputStream.flush();
					
					ouputStream.close();
					return null;
				}
				
				// Click on approve button
				if (setupForm.getButApprove() != null&&setupForm.getButApprove().equals("Approve Selected Record(s)")){
					String[] genSelBox = setupForm.getSelectedFtp();
					List genMsgList = null;
					SmsQueue  smsQ = new SmsQueue();
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
					boolean success=true;
					
					for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){// loop on each selected ftp record
						genMsgList=DBUtilsCrsmsg.DBHQLCommand("from SmsQueue where type = 'F' and smsStatus = 'A' and ftp = ? ", new Object[]{Integer.parseInt(genSelBox[i])}, hibernate_session);

						for (int q=0; q<genMsgList.size(); q++) {// loop on each sms within ftp record
							smsQ = (SmsQueue)genMsgList.get(q);						
							smsQ.setSmsStatus(Constants.SMS_STATUS_4);
							smsQ.setSmsStatusBy(theLogonCredential.getUserId());
							smsQ.setSmsStatusDatetime(new Date());
							smsQ.setApprovalBy(theLogonCredential.getUserId());
							smsQ.setApprovalDatetime(new Date());
							smsQ.setModifiedBy(theLogonCredential.getUserId());
							smsQ.setModifiedDatetime(new Date());
							if(DBUtilsCrsmsg.saveOrUpdate(smsQ, hibernate_session)){// successfully save
							}else{// fail to save
								success=false;
							}
						}
					}
					
					if(genSelBox!=null&&genSelBox.length>0){
						if(genMsgList.size()>0){
							String msg=Constants.STR_EMPTY;
							if(success){// success approve
								DBUtilsCrsmsg.closeTransaction(hibernate_session);
								msg=Constants.ERRMSG_ACTION_SUCCESS;
							}else{
								// failed to approve
								DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
								msg=Constants.ERRMSG_ACTION_FAIL;
							}
							ActionMessages messages = new ActionMessages();
							messages.add("ftpReport",new ActionMessage(msg, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_APPROVED));
							this.addErrors(request,messages);
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
							this.addErrors(request,messages);
						}
					}else{// no record selected
						ActionMessages messages = new ActionMessages();
						messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
						this.addErrors(request,messages);
					}
					DBUtilsCrsmsg.closeTransaction(hibernate_session);
				}
				
				if (setupForm.getButDelete() != null&&setupForm.getButDelete().equals("Delete Record(s)")){
					String[] genSelBox = setupForm.getSelectedFtp();
					List genMsgList = null;
					SmsQueue  smsQ = new SmsQueue();
					SmsDelete  smsDelete = new SmsDelete();
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
					boolean success=true;
					
					for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){
						genMsgList=DBUtilsCrsmsg.DBHQLCommand("from SmsQueue where type = 'F' and ftp = ? ", new Object[]{Integer.parseInt(genSelBox[i])}, hibernate_session);

						for (int q=0; q<genMsgList.size(); q++) {
							smsDelete = new SmsDelete();
							smsQ = (SmsQueue)genMsgList.get(q);						
							BeanUtilsBean utilBean = new BeanUtilsBean();
							utilBean.copyProperties((Object)smsDelete, (Object)smsQ);
							smsDelete.setId(null);// remove copied sms id, to save as new record
							
							if(DBUtilsCrsmsg.saveOrUpdate(smsDelete, hibernate_session)&&DBUtilsCrsmsg.delete(smsQ, hibernate_session)){
							}else{// fail to save
								success=false;
							}
						}
					}
					
					if(genSelBox!=null&&genSelBox.length>0){
						if(genMsgList.size()>0){
							String msg=Constants.STR_EMPTY;
							if(success){// success approve
								DBUtilsCrsmsg.closeTransaction(hibernate_session);
								msg=Constants.ERRMSG_ACTION_SUCCESS;
							}else{
								// failed to approve
								DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
								msg=Constants.ERRMSG_ACTION_FAIL;
							}
							ActionMessages messages = new ActionMessages();
							messages.add("ftpReport",new ActionMessage(msg, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
							this.addErrors(request,messages);
						}
					}else{// no record selected
						ActionMessages messages = new ActionMessages();
						messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
						this.addErrors(request,messages);
					}
					DBUtilsCrsmsg.closeTransaction(hibernate_session);
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session != null && hibernate_session.isOpen()){
				hibernate_session.close();
			}
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	public static String getUserList(Session hibernate_session, String u){
		StringBuffer  userId = new StringBuffer("");
		
		Integer group_id =(Integer) hibernate_session.createSQLQuery("select u.user_group from user u where u.user_id = ? ")
		.setString(0,u).list().get(0);
		
		
		List prList_result = hibernate_session.createSQLQuery("select u.user_id  from user u where  u.user_group = ? ")
		.setInteger(0,group_id).list();
		
		Iterator iter = prList_result.iterator();
		while (iter.hasNext()){
			String user =  (String) iter.next();
			userId.append("'");
			userId.append(user);
			userId.append("',");
		}
		
		return userId.substring(0, userId.length() -1);
		
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		
		
		ouputStream.print (println("Id."));
		ouputStream.print (println("File Name"));
		ouputStream.print (println("Process Date"));
		ouputStream.print (println("Process Start Time"));
		ouputStream.print (println("Process End Time"));
		ouputStream.print (println("Total"));
		ouputStream.print (println("Sent"));
		ouputStream.print (println("Unsent"));
		ouputStream.print (println("Pending Approval"));
		ouputStream.print (println("Queue"));
		ouputStream.print (println("Bad Record"));
		ouputStream.print (println("User"));
		ouputStream.print (println("Dept"));
		ouputStream.print (println("Remark"));
		
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
	
}
