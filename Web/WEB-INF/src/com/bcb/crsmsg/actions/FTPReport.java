package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.FtpReportForm;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;


public class FTPReport extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
//		//Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.FTP_LOG) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{
		
			FtpReportForm setupForm =(FtpReportForm) form;
		
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listFTPReport", null); 
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				hibernate_session.beginTransaction();
				String user ="";
				
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				
				if (isAdmin){
					
				}else if (isDept){
					user = getUserList(hibernate_session, theLogonCredential.getUserId());
				}else if(isPersonal){
					user="'"+theLogonCredential.getUserId()+"'";
				}
				
				String searchFromDate= setupForm.getSelectedFromDate();
				String searchUntilDate= setupForm.getSelectedUntilDate();
				String words1[];
				String words2[];
				String wordsSub1[];
				String wordsSub2[];
				
				String itu = "1 1";
				words1 = itu.split(Constants.STR_EMPTYSTRING);
				words1[0] = "01/01/0001";
				words1[1] = "00:00:01";
				words2 = itu.split(Constants.STR_EMPTYSTRING);
				words2[0] = "12/31/9999";
				words2[1] = "23:59:59";
	
				if (setupForm.getSelectedFromDate().length() > 0){
					words1 = searchFromDate.split(Constants.STR_EMPTYSTRING);
					wordsSub1 = words1[0].split("/");
					words1[0] = wordsSub1[2] + "-" + wordsSub1[1] + "-" + wordsSub1[0];
					words1[1] = words1[1] + ":00";
				}
				if ((setupForm.getSelectedUntilDate().length() > 0)){
					words2 = searchUntilDate.split(Constants.STR_EMPTYSTRING);
					wordsSub2 = words2[0].split("/");
					words2[0] = wordsSub2[2] + "-" + wordsSub2[1] + "-" + wordsSub2[0];
					words2[1] = words2[1] + ":00";
				}
				 List prList_result = null;
				
				String sql ="";
				 if (searchFromDate.length() > 1 & searchFromDate.length() > 1){
					 if (isAdmin){
						 sql = "from FtpReport where (startDate between ? and ?) and type = 'F' order by startDate desc";
					 }else{
						 sql = "from FtpReport where type = 'F' and (startDate between ? and ?) and createdBy in(" + user + ") order by startDate desc";
					 }
					 prList_result = hibernate_session.createQuery(sql)				
					.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
					.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
					.list();
				
				 }else{
					 if (isAdmin){
						 sql = "from FtpReport where type = 'F' order by startDate desc ";
					 }else{
						 sql = "from FtpReport where type = 'F' and createdBy in(" + user + ") order by startDate desc ";
					 }
					 prList_result = hibernate_session.createQuery(sql)				
					.list();	 
				 }
				request.setAttribute("listFTPReport", prList_result); 
				session.setAttribute(Constants.FTP_REPORT_KEY, prList_result); 
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				// Export CSV
				String export = request.getParameter("butExport");

				SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
				if (export != null&&export.equals("generateCSV"))
				{
					ArrayList dataList = (ArrayList) session.getAttribute(Constants.FTP_REPORT_KEY);
					response.reset();
					
		            ServletOutputStream ouputStream = response.getOutputStream();
		                
		                 //--------------------Generate Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     Constants.CSVFILENAME_FTPREPORT );
		     		printHeader(ouputStream);
		     		
		     		if (dataList != null){
			     		Iterator iter = dataList.iterator();
			     		while (iter.hasNext()){
			     			FtpReport model = (FtpReport) iter.next();
			     			
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getId(), String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getFileName(), String.class).toString()));
			      			ouputStream.print (println(DateUtil.getInstance().stringByDayMonthYear(model.getStartDate())));
			      			ouputStream.print (println(time_formatter.format(model.getStartTime())));
			      			ouputStream.print (println(time_formatter.format(model.getEndTime())));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getNoSuccess(), String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getNoFailed(), String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getCreatedBy(), String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getDept(), String.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getRemark(), String.class).toString()));
			     			ouputStream.println();
			     		}
		     		}
		     		ouputStream.flush();
	                
	                ouputStream.close();
	                return null;
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	public static String getUserList(Session hibernate_session, String u){
		StringBuffer  userId = new StringBuffer("");
		
		Integer group_id =(Integer) hibernate_session.createSQLQuery("select u.user_group from user u where u.user_id = ? ")
		.setString(0,u).list().get(0);
		
		
		List prList_result = hibernate_session.createSQLQuery("select u.user_id  from user u where  u.user_group = ? ")
		.setInteger(0,group_id).list();
		
		Iterator iter = prList_result.iterator();
		while (iter.hasNext()){
			String user =  (String) iter.next();
			userId.append("'");
			userId.append(user);
			userId.append("',");
		}
		
		return userId.substring(0, userId.length() -1);
		
	}
	
private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (println("Id."));
		ouputStream.print (println("File Name"));
		ouputStream.print (println("Date"));
		ouputStream.print (println("Start Time"));
		ouputStream.print (println("End Time"));
		ouputStream.print (println("Success"));
		ouputStream.print (println("Fail"));
		ouputStream.print (println("User"));
		ouputStream.print (println("Dept"));
		ouputStream.print (println("Remark"));
		
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
}
