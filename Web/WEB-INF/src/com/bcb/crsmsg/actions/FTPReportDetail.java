package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.FtpReportDetailForm;
import com.bcb.crsmsg.modal.FTPReportDetailModal;
import com.bcb.crsmsg.util.Constants;



public class FTPReportDetail extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
	
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try{
		
			FtpReportDetailForm reportForm =(FtpReportDetailForm) form;
		
			
			if(mapping.getParameter().equals("search")){
				String id = request.getParameter("refId");

				hibernate_session.beginTransaction();
				SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms_queue where ftp = ? and (sms_status=? or sms_status=? or sms_status=?)")

				.setString(0,id )
				.setString(1, Constants.SMS_STATUS_4)
				.setString(2, Constants.SMS_STATUS_5)
				.setString(3, Constants.SMS_STATUS_6);
				setMapping(q);
									   
				List prListQueue_result = q
				
//				//queue 
//				List prListQueue_result = hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms_queue where ftp = ?")
//				.setString(0,id )
				
				.list();
				
				Iterator resultIter_queue = prListQueue_result.iterator();
				ArrayList dataResult = new ArrayList();

				int runningCounter = 0;
				while (resultIter_queue.hasNext())
				{
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter_queue.next();	
					FTPReportDetailModal modal= new FTPReportDetailModal();
					modal.setSeq(String.valueOf(runningCounter));
					modal.setMobileNo( (String) rowResult[1] );
					modal.setMsg( (String) rowResult[2] );
					modal.setSentby( (String) rowResult[3] );
					modal.setRemark((String) rowResult[4]  );
					modal.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5]));
					modal.setStatus("queue");
					dataResult.add(modal);
				}
				reportForm.setTotalQueue(String.valueOf(prListQueue_result.size()));
				
				SQLQuery  qPApproval = (SQLQuery) hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms_queue where ftp = ? and sms_status=?")

				.setString(0,id )
				.setString(1, Constants.SMS_STATUS_3);
				setMapping(qPApproval);
									   
				List prListPApproval_result = qPApproval.list();
				
				Iterator resultIter_papproval = prListPApproval_result.iterator();

				while (resultIter_papproval.hasNext())
				{
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter_papproval.next();	
					FTPReportDetailModal modal= new FTPReportDetailModal();
					modal.setSeq(String.valueOf(runningCounter));
					modal.setMobileNo( (String) rowResult[1] );
					modal.setMsg( (String) rowResult[2] );
					modal.setSentby( (String) rowResult[3] );
					modal.setRemark((String) rowResult[4]  );
					modal.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5]));
					modal.setStatus("queue");
					dataResult.add(modal);
				}
				reportForm.setTotalPendingApproval(String.valueOf(prListPApproval_result.size()));
				
				SQLQuery  qPVerification = (SQLQuery) hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms_queue where ftp = ? and sms_status=?")

				.setString(0,id )
				.setString(1, Constants.SMS_STATUS_7);
				setMapping(qPVerification);
									   
				List prListPendingVerification_result = qPVerification
				
//				//queue 
//				List prListQueue_result = hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms_queue where ftp = ?")
//				.setString(0,id )
				
				.list();
				
				Iterator resultIter_pverification = prListPendingVerification_result.iterator();

				while (resultIter_pverification.hasNext())
				{
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter_pverification.next();	
					FTPReportDetailModal modal= new FTPReportDetailModal();
					modal.setSeq(String.valueOf(runningCounter));
					modal.setMobileNo( (String) rowResult[1] );
					modal.setMsg( (String) rowResult[2] );
					modal.setSentby( (String) rowResult[3] );
					modal.setRemark((String) rowResult[4]  );
					modal.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5]));
					modal.setStatus("queue");
					dataResult.add(modal);
				}
				reportForm.setTotalPendingVerification(String.valueOf(prListPendingVerification_result.size()));
				
				//sent log
				
				SQLQuery  qSent = (SQLQuery) hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms where ftp = ? and sms_status = 'S'")

				.setString(0,id );
				
				setMapping(qSent);
									   
				List prListSent_result = qSent
				
//				List prListSent_result = hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms where ftp = ? and sms_status = 'S' ")
//				.setString(0,id )
//				
				.list();
				
				Iterator resultIter_sent = prListSent_result.iterator();
				
				while (resultIter_sent.hasNext())
				{
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter_sent.next();	
					FTPReportDetailModal modal= new FTPReportDetailModal();
					modal.setSeq(String.valueOf(runningCounter));
					modal.setMobileNo( (String) rowResult[1] );
					modal.setMsg( (String) rowResult[2] );
					modal.setSentby( (String) rowResult[3] );
					modal.setRemark((String) rowResult[4]  );
					modal.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5]));
					modal.setStatus("sent");
					dataResult.add(modal);
				}
				reportForm.setTotalSent(String.valueOf(prListSent_result.size()));
				
				
				//unsent log
				SQLQuery  qUnSent = (SQLQuery) hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms where ftp = ? and sms_status = 'U'")

				.setString(0,id );
				
				setMapping(qUnSent);
									   
				List prList_result = qUnSent
				
//				List prList_result = hibernate_session.createSQLQuery("select sms_id, mobile_no, message, sent_by, remarks, sms_status_datetime from sms where ftp = ? and sms_status = 'U'")
//				.setString(0,id )
				
				.list();
				
				Iterator resultIter = prList_result.iterator();
				//ArrayList dataResult = new ArrayList();

				//int runningCounter = 0;
				while (resultIter.hasNext())
				{
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter.next();	
					FTPReportDetailModal modal= new FTPReportDetailModal();
					modal.setSeq(String.valueOf(runningCounter));
					modal.setMobileNo( (String) rowResult[1] );
					modal.setMsg( (String) rowResult[2] );
					modal.setSentby( (String) rowResult[3] );
					modal.setRemark((String) rowResult[4]  );
					modal.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5]));
					modal.setStatus("unsent");
					dataResult.add(modal);
				}
				reportForm.setTotalUnsent(String.valueOf(prList_result.size()));
				
				request.setAttribute("listFTPReportDetail", dataResult); 
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReportDetail",new ActionMessage("errors.system"));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward("success"));
		
		
	}
	
	
	private SQLQuery setMapping(SQLQuery q){

		q.addScalar("sms_id", Hibernate.INTEGER);
		q.addScalar("mobile_no", Hibernate.STRING);
		q.addScalar("message", Hibernate.STRING);
		q.addScalar("sent_by", Hibernate.STRING);
		q.addScalar("remarks", Hibernate.STRING);
		q.addScalar("sms_status_datetime", Hibernate.TIMESTAMP);
		
		
		return q;
	}
	
	
}
