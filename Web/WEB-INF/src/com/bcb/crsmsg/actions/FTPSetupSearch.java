

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.FTPSetupSearchForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class FTPSetupSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.FTP_SETUP) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		try{
			List result = null; 
			
			if(mapping.getParameter().equals("load")){
			}else{
				FTPSetupSearchForm esform =  (FTPSetupSearchForm) form;
				StringBuffer query=new StringBuffer();
				String queryField=Constants.STR_EMPTY;
				
				if (esform.getSelectedState().equals(Constants.FORM_FIELD_SFTP_ENTRY_NAME)){// By User Department
					queryField="ftpName";
				}else if ( esform.getSelectedState().equals(Constants.FORM_FIELD_SFTP_SERVER_HOST)){// By Message Code
					queryField="ftpServer";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_SFTP_USER_NAME)){// By Message Text
					queryField="ftpUserName";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_STATUS)){// By Status
					queryField="setupStatus";            		  
				}else{// No matched search state
					
				}
				
				query.append(
						"select id, ftpName, ftpServer, ftpUserName, createdBy, createdDatetime, setupStatus " +
						"from FtpSetup " +
						"where " +
						queryField+" like ? "+
						"order by ftpName"
						);
				result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
					Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT
					});
				esform.setResultList(CommonUtils.populateResult(result));   
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_FTPSETUP_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
}
