

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.KeywordSearchForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class KeywordSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.KEYWORD_MGMT) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		try{
			List result = null; 
			
			if(mapping.getParameter().equals("load")){
			}else{
				KeywordSearchForm esform =  (KeywordSearchForm) form;
				StringBuffer query=new StringBuffer();
				String queryField=Constants.STR_EMPTY;
				
				if (esform.getSelectedState().equals(Constants.FORM_FIELD_KEYWORD)){// By Keyword
					queryField="k.keywordName";
				}else if ( esform.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT)){// By User Department
					queryField="d.deptName";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_OWNERSHIP)){// By Ownership
					queryField="k.ownership";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_STATUS)){// By Status
					queryField="k.status";            		  
				}else{// No matched search state
					
				}
				
				query.append(
						"select k.id, k.keywordName, k.autoReply, k.createdBy, d.deptName, k.ownership, DATE(k.createdDatetime), k.expiredDate, k.autoExpired, k.status " +
						"from Keyword k, Department d, User u " +
						"where " +
						queryField+" like ? "+
						"and k.ownership=u.userId and u.userGroup=d.id " +
						"order by k.keywordName"
						);
				result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
					Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT
					});
				esform.setResultList(CommonUtils.populateResult(result));          
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_KEYWORD_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_KEYWORD_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
}
