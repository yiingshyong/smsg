package com.bcb.crsmsg.actions;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.crsmsg.util.AppPropertiesConfig;

public class MKSimulator extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9202997410760781458L;
	private static final Log log = LogFactory.getLog(MKSimulator.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.info("Incoming request ... ");
		log.info("The request uri:" + request.getQueryString());
		BufferedWriter writer = new BufferedWriter(response.getWriter());		
		if(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.MK_SIM_STATUS).equalsIgnoreCase("1")){			
			writer.write(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.MK_SIM_SUCCESS_CODE));
		}else{
			writer.write(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.MK_SIM_RETRY_CODE));
		}
		writer.flush();
		writer.close();
	}  	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}   
	
}