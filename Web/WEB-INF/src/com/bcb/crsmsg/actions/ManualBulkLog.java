package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.ManualBulkLogForm;
import com.bcb.crsmsg.modal.FtpReport;
import com.bcb.crsmsg.modal.SmsDelete;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;


public class ManualBulkLog extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
//		//Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.BULK_SEND_LOG) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try{
		
			ManualBulkLogForm setupForm =(ManualBulkLogForm) form;
		
			System.out.println("request param: " + mapping.getParameter());			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listManualReport", null); 
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				hibernate_session.beginTransaction();
				if(setupForm.getButSubmit() != null && setupForm.getButSubmit().equals("Search")){
					String user ="";
					
					boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
					boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
					boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
									
					if (isAdmin){
						
					}else if (isDept){
						user = getUserList(hibernate_session, theLogonCredential.getUserId());
					}else if(isPersonal){
						user="'"+theLogonCredential.getUserId()+"'";
					}
					
					String searchFromDate= setupForm.getSelectedFromDate();
					String searchUntilDate= setupForm.getSelectedUntilDate();
					String words1[];
					String words2[];
					String wordsSub1[];
					String wordsSub2[];
					
					String itu = "1 1";
					words1 = itu.split(Constants.STR_EMPTYSTRING);
					words1[0] = "01/01/0001";
					words1[1] = "00:00:01";
					words2 = itu.split(Constants.STR_EMPTYSTRING);
					words2[0] = "12/31/9999";
					words2[1] = "23:59:59";
		
					if (setupForm.getSelectedFromDate().length() > 0){
						words1 = searchFromDate.split(Constants.STR_EMPTYSTRING);
						wordsSub1 = words1[0].split("/");
						words1[0] = wordsSub1[2] + "-" + wordsSub1[1] + "-" + wordsSub1[0];
						words1[1] = words1[1] + ":00";
					}
					if ((setupForm.getSelectedUntilDate().length() > 0)){
						words2 = searchUntilDate.split(Constants.STR_EMPTYSTRING);
						wordsSub2 = words2[0].split("/");
						words2[0] = wordsSub2[2] + "-" + wordsSub2[1] + "-" + wordsSub2[0];
						words2[1] = words2[1] + ":00";
					}
					 List prList_result = null;
					
					String sql ="";
					 if (searchFromDate.length() > 1 & searchFromDate.length() > 1){
						 if (isAdmin){
							 sql = "from FtpReport where (startDate between ? and ?) and type = 'B' order by startDate desc";
						 }else{
							 sql = "from FtpReport where type = 'B' and (startDate between ? and ?) and createdBy in(" + user + ") order by startDate desc";
						 }
						 prList_result = hibernate_session.createQuery(sql)				
						.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
						.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
						.list();
					
					 }else{
						 if (isAdmin){
							 sql = "from FtpReport where type = 'B' order by startDate desc ";
						 }else{
							 sql = "from FtpReport where type = 'B' and createdBy in(" + user + ") order by startDate desc ";
						 }
						 prList_result = hibernate_session.createQuery(sql)				
						.list();	 
					 }
				
					ArrayList dataResult = new ArrayList();
					for(int i=0;i<prList_result.size();i++){
						FtpReport theReport=(FtpReport)prList_result.get(i);
						theReport.setSent(CommonUtils.getFtpSentCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setUnsent(CommonUtils.getFtpFailedCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setPendingApp(CommonUtils.getFtpPendingApprovalCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setPendingVer(CommonUtils.getFtpPendingVerificationCount(Integer.valueOf(theReport.getId().toString())));
						theReport.setQueue(CommonUtils.getFtpPendingCount(Integer.valueOf(theReport.getId().toString())));
						dataResult.add(theReport);
					}
					
					//request.setAttribute("ftpBulkLog", dataResult);
					request.setAttribute("listManualReport", dataResult); 
					session.setAttribute(Constants.MANUALBULKLOG_KEY, dataResult);
				}
				
				System.out.println("Finished searching");
				String export = request.getParameter("butExport");

				SimpleDateFormat time_formatter = new SimpleDateFormat(Constants.DATEFORMAT_HOURBYMINUTESECOND);
				if (export != null&&export.equals("generateCSV"))
				{
					ArrayList dataList = (ArrayList) session.getAttribute(Constants.MANUALBULKLOG_KEY);
					response.reset();
					
		            ServletOutputStream ouputStream = response.getOutputStream();
		                
		                 //--------------------Generate Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     Constants.CSVFILENAME_MANUALBULKLOGREPORT );
		     		printHeader(ouputStream);
		     		
		     		if (dataList != null){
			     		Iterator iter = dataList.iterator();
			     	
			     		
			     		while (iter.hasNext()){
			     			FtpReport model = (FtpReport) iter.next();
			     			
			      			ouputStream.print (println(model.getId().toString()));
			      			ouputStream.print (println(model.getFileName()));
			      			ouputStream.print (println(DateUtil.getInstance().stringByDayMonthYear(model.getStartDate())));
			      			ouputStream.print (println(time_formatter.format(model.getStartTime())));
			      			ouputStream.print (println(time_formatter.format(model.getEndTime())));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getSent(), Integer.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getUnsent(), Integer.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getPendingVer(), Integer.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getPendingApp(), Integer.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getQueue(), Integer.class).toString()));
			      			ouputStream.print (println(CommonUtils.NullChecker(model.getNoFailed(), Integer.class).toString()));
			      			ouputStream.print (println(model.getRemark()));

			     			ouputStream.println();
			     		}
		     		}
		     		ouputStream.flush();
	                
	                ouputStream.close();
	                return null;
				}
					
				if (setupForm.getButApprove() != null&&setupForm.getButApprove().equals("Approve Selected Record(s)"))
				{
					System.out.println("Start Approving process");
					String[] genSelBox = setupForm.getSelectedFtp();
					
					int genCnt = 1;
					List genMsgList = null;
					SmsQueue  smsQ = new SmsQueue();
					
					for(int i=0;i<genSelBox.length;i++){
						System.out.println("Searching record from db");
						//genItemList=hibernate_session.createQuery("from InvItemModal where invId = ? ").setString(0,genSelBox[i]).setDate(1,formatter.parse(todayDate)).list();			
						genMsgList=DBUtilsCrsmsg.DBHQLCommand("from SmsQueue where type = 'B' and smsStatus = 'V' and ftp = ? ", new Object[]{Integer.parseInt(genSelBox[i])});
						//.setString(0,genSelBox[i])
						
						for (int q=0; q<genMsgList.size(); q++) {
							smsQ = (SmsQueue)genMsgList.get(q);			
							if (CommonUtils.NullChecker(smsQ.getSmsScheduled(), String.class).toString().equals(Constants.STATUS_YES)&&smsQ.getSmsScheduledTime()!=null){
								smsQ.setSmsStatus(Constants.SMS_STATUS_6);								
							}else{
								smsQ.setSmsStatus("Q");
							}
							smsQ.setSmsStatusBy(theLogonCredential.getUserId());
							smsQ.setSmsStatusDatetime(new Date());
							smsQ.setModifiedBy(theLogonCredential.getUserId());
							smsQ.setModifiedDatetime(new Date());
							DBUtilsCrsmsg.saveOrUpdate(smsQ);
						}
											
						genCnt++;
					}
					System.out.println("Leaving");
					ActionMessages messages = new ActionMessages();
					messages.add("ManualBulkReport",new ActionMessage(Constants.ERRMSG_RECORDAPPROVED));
					this.addErrors(request,messages);
					
				}
				
				System.out.println("setupForm.getButDelete()manual>>>>>"+setupForm.getButDelete());
				if (setupForm.getButDelete() != null&&setupForm.getButDelete().equals("Delete Record(s)"))
				{
		
					String[] genSelBox = setupForm.getSelectedFtp();
					
					int genCnt = 1;
					List genMsgList = null;
					SmsQueue  smsQ = new SmsQueue();
					SmsDelete  smsDelete = new SmsDelete();
					
					for(int i=0;i<genSelBox.length;i++){
						
						//genItemList=hibernate_session.createQuery("from InvItemModal where invId = ? ").setString(0,genSelBox[i]).setDate(1,formatter.parse(todayDate)).list();			
						genMsgList=hibernate_session.createQuery("from SmsQueue where type = 'B' and ftp = ? ")
						.setInteger(0, Integer.parseInt(genSelBox[i]))
						.list();
						
						for (int q=0; q<genMsgList.size(); q++) 
						{
							
							smsDelete = new SmsDelete();
							smsQ = (SmsQueue)genMsgList.get(q);						
							BeanUtilsBean utilBean = new BeanUtilsBean();
							utilBean.copyProperties((Object)smsDelete, (Object)smsQ);
							
							hibernate_session.save(smsDelete);
							hibernate_session.delete(smsQ);
							
					
						}
											
						genCnt++;
					
					}
					
					ActionMessages messages = new ActionMessages();
					messages.add("ManualBulkReport",new ActionMessage(Constants.ERRMSG_RECORDAPPROVED));
					this.addErrors(request,messages);					
				}
				if(hibernate_session.isOpen() && hibernate_session.getTransaction().isActive()){
					hibernate_session.getTransaction().commit();
				}				
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("ManualBulkReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	public static String getUserList(Session hibernate_session, String u){
		StringBuffer  userId = new StringBuffer("");
		
		Integer group_id =(Integer) hibernate_session.createSQLQuery("select u.user_group from user u where u.user_id = ? ")
		.setString(0,u).list().get(0);
		
		
		List prList_result = hibernate_session.createSQLQuery("select u.user_id  from user u where  u.user_group = ? ")
		.setInteger(0,group_id).list();
		
		Iterator iter = prList_result.iterator();
		while (iter.hasNext()){
			String user =  (String) iter.next();
			userId.append("'");
			userId.append(user);
			userId.append("',");
		}
		
		return userId.substring(0, userId.length() -1);
		
	}
	
private void printHeader(ServletOutputStream ouputStream ) throws Exception{

		
    	ouputStream.print (println("Id."));
		ouputStream.print (println("File Name"));
		ouputStream.print (println("Date"));
		ouputStream.print (println("Start Time"));
		ouputStream.print (println("End Time"));
		ouputStream.print (println("Sent"));
		ouputStream.print (println("Unsent"));
		ouputStream.print (println("Pending Verification"));
		ouputStream.print (println("Pending Approval"));
		ouputStream.print (println("Queue"));
		ouputStream.print (println("Bad Record"));
		ouputStream.print (println("Remark"));
		
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
	
}
