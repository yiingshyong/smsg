

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.AddressBookSearchForm;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class NumberListing extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}

		try{
			List result = null; 
			
			if(mapping.getParameter().equals("load")){
			}else{
				AddressBookSearchForm esform =  (AddressBookSearchForm) form;
				StringBuffer query=new StringBuffer();
				String queryField=Constants.STR_EMPTY;
				String additionalQuery=Constants.STR_EMPTY;
				Object additionalParameter=Constants.STR_EMPTY;
				if (esform.getSelectedState().equals(Constants.FORM_FIELD_TYPE)){// By Type
					queryField="a.type";
					
					if(esform.getSearchkey().equals(Constants.FORM_FIELD_CONTACT_TYPE_PERSONAL)){
						additionalQuery="and a.createdBy=? ";
						additionalParameter=theLogonCredential.getUserId();
					}else if (esform.getSearchkey().equals(Constants.FORM_FIELD_CONTACT_TYPE_GROUP)){
						additionalQuery="and a.department=? ";
						additionalParameter=Integer.valueOf(theLogonCredential.getUserGroup());
					}
				}else{// No matched search state
					
				}
				
				query.append(
						"select a.id, a.name, a.refNo, a.mobileNo, a.designation " +
						"from Contact a " +
						"where " +
						queryField+" like ? "+
						additionalQuery+
						"order by a.name"
						);
				result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
					Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT,
					additionalParameter
					});
				esform.setResultList(CommonUtils.populateResult(result));          
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_NUMBER_LISTING+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_NUMBER_LISTING,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
}
