package com.bcb.crsmsg.actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.SmsReceived;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.SgwException;
import com.bcb.sgwcore.creditcard.OCMOptInUtil;
import com.bcb.sgwcore.creditcard.OPTINSMSGResponse;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class OCMOptInAction implements ActionTemplate {

	private static final Log log = LogFactory.getLog(OCMOptInAction.class);
	private static final int CC_NO_LENGTH = 16;
	private static final int CARD_SUFFIX_ACTIVATION_INPUT_LENGTH = 4;
	private static final int CARD_HOLDER_PARAM_ID = 0;
	private static final int CARD_HOLDER_PARAM_CC_NO = 1;
	
	private static String RESPONSE_SMS_FAILED_TO_CREATE_MSG = "Generate response sms failed";
	private String normalizedSMS;
	private String mobileNo;
	private AutoAction autoAction;
	
	public static final String OPTIN_STATUS_SUCCEED = "Y";
	public static final String OPTIN_STATUS_FAILED = "F";
	public static final String OPTIN_UNEXPECTED_ERROR = "P";	
	@Override
	public String execute(Keyword theKeyword, ArrayList<String> paramArray) {
		long startTime = System.currentTimeMillis();
		StringBuilder logStr = new StringBuilder();
		OPTINSMSGResponse optInResult = null;
		mobileNo = paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO));
		SmsReceived smsReceived = theKeyword.getSmsReceive();		
	
		try{
			
			
			mobileNo = BusinessService.normalizeMobile(mobileNo);

			String incomingSms = paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE));
			String parakey = "OCM";//PUT THIS IS PROPERTIES FILE
			autoAction = DaoBeanFactory.getAutoActionDao().findOCMActions(parakey);
		
			//check svc window availability
			String[] svcWindow = StringUtils.splitByWholeSeparator(autoAction.getSvcWindow(), Constants.AUTOACTION_SVC_WINDOW_SEPARATOR);
			DateFormat df = new SimpleDateFormat("HHmm");
			Integer currentTime = Integer.parseInt(df.format(new Date()));
			if(currentTime < Integer.parseInt(svcWindow[0]) || currentTime > Integer.parseInt(svcWindow[1])){
				optInResult = OPTINSMSGResponse.getUnavailableResponse("SMS: Exceeded OCM Opt-In Time Frame", false,parakey);
			}
			
			normalizedSMS = normalizeIncomingSMS(incomingSms, theKeyword);//remove the keyword
			String cardSuffix = normalizedSMS.trim();	
			int requiredCardSuffixLength = 4;
			if(cardSuffix.length() > requiredCardSuffixLength){
				cardSuffix = cardSuffix.substring(cardSuffix.length() -4);					
			}//else if(cardSuffix.length() != requiredCardSuffixLength && !StringUtils.isNumeric(cardSuffix)){
			 //	optInResult = OPTINSMSGResponse.getFailureResponse("SMS: Missing input or invalid input format", false);									
			 //}
			
			//First level checking no error. Going to call server for OPT-IN
			if(optInResult == null){
				optInResult = OCMOptInUtil.optInCardViaSocket(mobileNo, cardSuffix, 0);
			}
		}catch(Exception e){
			log.error("Unexpected Error Caught. SMS Received Id: " + smsReceived.getId() + ". Reason: " + e.getMessage(), e);
			optInResult = OPTINSMSGResponse.getFailureResponse("SMS: Unexpected Error", false);
			
			
		}finally{
			SmsQueue smsQueue = null;
			if(optInResult == null){//should not happen
				optInResult = OPTINSMSGResponse.getFailureResponse("SMS: Unexpected Error", false);				
			}
			//save response sms
			try{
				//send sms to cardholder if there's host related error
				if(optInResult.getResult().equals(OPTIN_STATUS_FAILED)){
					smsQueue = createSMS(theKeyword, optInResult.getResponseSMS(), mobileNo);
					DaoBeanFactory.getHibernateTemplate().save(smsQueue);
				}
			
			}catch(Exception e){
				log.error("Error create response SMS. SMS Received Id: " + smsReceived.getId() + ". Reason: " + e.getMessage(), e);
			}
			//update sms received
			smsReceived.setRemarks(optInResult.getActivationMessage());
			DaoBeanFactory.getHibernateTemplate().update(smsReceived);
			
		
			logStr.append(smsReceived.getId())
			.append(" ").append(mobileNo)
			.append(" ").append(optInResult.getActivationMessage())
			.append(" ").append((System.currentTimeMillis()-startTime));
			log.info(logStr.toString());
		}
		return "completed";
	}
			
	public static  String normalizeIncomingSMS(String incomingSMS, Keyword keyword){
		//step 1. change to lowercase
		String filteredIncomingSMS = incomingSMS.toLowerCase();
		//step 2. Remove special character
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, "-");
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, "&");
		filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, " ");
		
		if(keyword != null){
			//step 3. Remove keyword
			filteredIncomingSMS = StringUtils.remove(filteredIncomingSMS, keyword.getKeywordName().toLowerCase());			
		}		
		return filteredIncomingSMS;
	}
	
	public static String[] getFullCardHolderParams(String smsWithoutKeyword){
		String[] cardHolderParams = new String[2];
		cardHolderParams[CARD_HOLDER_PARAM_ID] = smsWithoutKeyword.substring(0, smsWithoutKeyword.length() - CC_NO_LENGTH);
		cardHolderParams[CARD_HOLDER_PARAM_CC_NO] = smsWithoutKeyword.substring(smsWithoutKeyword.length() - CC_NO_LENGTH);		
		return cardHolderParams;
	}
	
	public static String[] getSimplifiedCardHolderParams(String smsWithoutKeyword){
		String[] cardHolderParams = new String[2];
		cardHolderParams[CARD_HOLDER_PARAM_ID] = smsWithoutKeyword.substring(0, 4);
		cardHolderParams[CARD_HOLDER_PARAM_CC_NO] = smsWithoutKeyword.substring(4);		
		return cardHolderParams;
	}
	
	public static String[] getCardHolderParams(String incomingSMS, Keyword keyword){
		String filteredIncomingSMS = normalizeIncomingSMS(incomingSMS, keyword);
		
		//input not valid
		if(filteredIncomingSMS.length() <= CC_NO_LENGTH) return null;
		
		return getFullCardHolderParams(filteredIncomingSMS);
	}
	
	private SmsQueue createSMS(Keyword theKeyword, String message, String mobileNo) throws Exception{		
		int deptId=0;
		String dept=Constants.STR_EMPTY;
		User usermodal = (User)DBUtilsCrsmsg.get(User.class, theKeyword.getOwnership());
		if (usermodal == null){
			throw new SgwException(SgwException.USER_NOT_FOUND);
		}else{
			Department groupmodal = (Department)DBUtilsCrsmsg.get(Department.class, usermodal.getUserGroup());
			if (groupmodal != null){
				deptId=groupmodal.getId();
				dept=groupmodal.getDeptName();
			}
		}
		
		SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
		if(theCfg==null){
			throw new SgwException(SgwException.SMS_CFG_NOT_FOUND);
		}else{
			int charPerSms=CommonUtils.calculateCharPerSms(Constants.FORM_FIELD_CHARSET_ASCIITEXT, theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(message, charPerSms);
			
			SmsQueue aSms=new SmsQueue();
			aSms.setCreatedBy(theKeyword.getOwnership());
			aSms.setCreatedDatetime(new Date());
			aSms.setDeptId(deptId);
			aSms.setDept(dept);
			aSms.setMessage(message);
			aSms.setTotalSms(msgArray.size());
			aSms.setMobileNo(mobileNo);
			aSms.setMsgFormatType(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
			aSms.setPriority(Integer.valueOf(Constants.PRIORITY_LEVEL_9));
			aSms.setSmsStatus(Constants.SMS_STATUS_4);
			aSms.setSmsStatusBy(theKeyword.getOwnership());
			aSms.setSmsStatusDatetime(new Date());
			aSms.setSentBy(theKeyword.getOwnership());
			aSms.setType(Constants.SMS_SOURCE_TYPE_5);
			aSms.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(aSms.getCreatedBy(), aSms.getChannelId()));
			return aSms;
		}
	}
	

}
