package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.OutboxForm;
import com.bcb.crsmsg.modal.OutboxModal;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public class Outbox extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.OUTBOX) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
				// Set counts to 0
				request.setAttribute(Constants.PENDING_COUNT_KEY, Integer.valueOf(Constants.VALUE_0)); 
				request.setAttribute(Constants.APPROVAL_COUNT_KEY, Integer.valueOf(Constants.VALUE_0)); 
				request.setAttribute(Constants.FAILED_COUNT_KEY, Integer.valueOf(Constants.VALUE_0)); 
				request.setAttribute(Constants.SENT_COUNT_KEY, Integer.valueOf(Constants.VALUE_0)); 
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
				OutboxForm theForm =(OutboxForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());
				
				/* Define User Rights */
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				String user=Constants.STR_EMPTY;

				if (isAdmin){// not restricted by user, show all
					user=Constants.STR_EMPTY;
				}else if (isDept){// restricted by cases from users of same department
					user = getUserList(theLogonCredential.getUserId());
				}else if(isPersonal){// restricted by own cases
					user=Constants.SPECIAL_QUOTE_SINGLE+theLogonCredential.getUserId()+Constants.SPECIAL_QUOTE_SINGLE;
				}
				/* Define User Rights */
				
				/* Set total sms counters */
				int pending = CommonUtils.getPendingCount(user);
				request.setAttribute(Constants.PENDING_COUNT_KEY, pending); 

				int approval = CommonUtils.getPendingApprovalCount(user);
				request.setAttribute(Constants.APPROVAL_COUNT_KEY, approval); 

				int failed = CommonUtils.getFailedCount(user);
				request.setAttribute(Constants.FAILED_COUNT_KEY, failed); 

				int sent = CommonUtils.getSentCount(user);
				request.setAttribute(Constants.SENT_COUNT_KEY, sent); 
				/* Set total sms counters */
				
				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				String subQueryParam = Constants.STR_EMPTY;
                if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "r.mobile_no";	
					subQueryParam = "mobile_no";	
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SENDER)){
					queryParam = "r.sent_by";
					subQueryParam = "sent_by";	
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_DEPARTMENT)){
					queryParam = "r.dept";
					subQueryParam = "dept";	
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR)){
					List list = DBUtilsCrsmsg.DBSQLCommand("select telco_id from telco where UPPER(telco_name) like ? and status != 'Closed'", new String[]{"telco_id"}, new NullableType[]{Hibernate.INTEGER}, new Object[]{Constants.STR_PERCENT + theForm.getSearchkey().toUpperCase() + Constants.STR_PERCENT});
					System.out.println("size: " + list.size());
					if (list !=null && list.size() > 0){						
						theForm.setSearchkey(String.valueOf(list.get(0)));	
					}else{
						theForm.setSearchkey("9999");
					}
					queryParam = "t.telco_id";
					subQueryParam = "telco";	
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SEND_TYPE)){
					queryParam = "r.type";
					subQueryParam = "type";	
				}else{// Can't find match selected state
                }
				/* Define search selected state*/
				
				/* Get total no. of sms */
/*						String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(r.sms_id) from (select sms_id, mobile_no, sent_by, dept, telco, type, sms_status_datetime from sms union select sms_id, mobile_no, sent_by, dept, telco, type, sms_status_datetime from sms_queue) r left outer join telco t on r.telco=t.telco_id where r.sms_status_datetime between ? and ?"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and r.sent_by in (" + user + ")":Constants.STR_EMPTY), 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT}).get(0), Integer.class).toString();
*/
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand(
							"select count(r.sms_id) " +
							"from " +
							"(" +
							"select sms_id, mobile_no, sent_by, dept, telco, type, sms_status_datetime from sms " +
							"where sms_status_datetime between ? and ?"+
							" and (UPPER("+ subQueryParam +") like ?"
							+(theForm.getSearchkey().length()==0?" or "+subQueryParam+" is null":Constants.STR_EMPTY)+")"+
							(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY) +
							" union " +
							"select sms_id, mobile_no, sent_by, dept, telco, type, sms_status_datetime from sms_queue " +
							"where sms_status_datetime between ? and ?"+
							" and (UPPER("+ subQueryParam +") like ?"
							+(theForm.getSearchkey().length()==0?" or "+subQueryParam+" is null":Constants.STR_EMPTY)+")"+
							(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY) +
							") r " 
							, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT,searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT}).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());

//				sql.append("select r.sms_id, r.sms_status_datetime, r.mobile_no, r.message, r.sent_by, r.dept, r.sms_scheduled_time,  r.type, r.remarks, r.approval_by, r.priority, r.sms_status, t.telco_name, r.created_datetime,r.approval_datetime ");
//				sql.append("from (select sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, approval_datetime  from sms union select sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, approval_datetime from sms_queue) r left outer join telco t on r.telco=t.telco_id " +
//						   "where (r.sms_status_datetime between ? and ?) "+
//						   " and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
//						   (user.length()>0?" and r.sent_by in (" + user + ")":Constants.STR_EMPTY));
//				sql.append("order by r.sms_status_datetime desc LIMIT "+offset+", "+pageSize);
			
				sql.append("select r.sms_id, r.sms_status_datetime, r.mobile_no, r.message, r.sent_by, r.dept, r.sms_scheduled_time,  r.type, r.remarks, r.approval_by, r.priority, r.sms_status, t.telco_name, r.created_datetime,r.approval_datetime ");
				sql.append("from (" +
							"select sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, approval_datetime " +
							"from sms " +
							"where (sms_status_datetime between ? and ?) "+
							" and (UPPER("+ subQueryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+subQueryParam+" is null":Constants.STR_EMPTY)+")"+
							(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY) + 							
							"union " +
							"select sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, approval_datetime " +
							"from sms_queue " +
							"where (sms_status_datetime between ? and ?) "+
							" and (UPPER("+ subQueryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+subQueryParam+" is null":Constants.STR_EMPTY)+")"+
							(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY) + 							
						   ") r " +
							"left outer join telco t " +
							"on r.telco=t.telco_id "
						   );
				sql.append("order by r.sms_status_datetime desc LIMIT "+offset+", "+pageSize);

				resultList= DBUtilsCrsmsg.DBSQLCommand(
						sql.toString(), 
						new String[]{"r.sms_id", "r.sms_status_datetime", "r.mobile_no", "r.message", "r.sent_by", "r.dept", "r.sms_scheduled_time", "r.type", "r.remarks", "r.approval_by", "r.priority", "r.sms_status", "t.telco_name", "r.created_datetime", "r.approval_datetime"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.INTEGER, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.TIMESTAMP}, 
//						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT,searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});
				/*while(offset>=resultList.size()&&offset!=0){
					offset-=pageSize;// back one page
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<OutboxModal> dataResult = new ArrayList<OutboxModal>();
					int runningCounter = 0;

					while (resultIter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) resultIter.next();	
						OutboxModal outBox= new OutboxModal();
						outBox.setId(runningCounter);
						outBox.setSeq(String.valueOf(runningCounter));
						outBox.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						outBox.setMobileNo( (String) rowResult[2] );
						outBox.setMessage( (String) rowResult[3] );
						outBox.setSentBy( (String) rowResult[4] );
						outBox.setDept((String) rowResult[5]  );
						outBox.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
						outBox.setMode(Constants.STR_EMPTY);
						outBox.setType((String) rowResult[7]  );
						outBox.setRemarks((String) rowResult[8]  );
						outBox.setApproval((String) rowResult[9]  );
						outBox.setPriority(CommonUtils.NullChecker(rowResult[10], Integer.class).toString());
						//outBox.setStatus(((Integer)rowResult[11]).toString()  );
						outBox.setStatus(CommonUtils.getStatus(CommonUtils.NullChecker(rowResult[11], String.class).toString()));
						outBox.setOperator(CommonUtils.NullChecker(rowResult[12], String.class).toString());
						//outBox.setOperator(((Integer)rowResult[12]).toString());
						outBox.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[13]));
						outBox.setDeliveryReportDate(Constants.STR_EMPTY);
						if(rowResult[14]!=null){
							outBox.setApprovalDatetime(DateUtil.getInstance().stringByDayMonthYear((Timestamp)rowResult[14]));
						}else{
							outBox.setApprovalDatetime(Constants.STR_EMPTY);
						}
					    dataResult.add(outBox);
					}
					request.setAttribute(Constants.OUTBOX_RESULT_KEY, dataResult);
					session.setAttribute(Constants.OUTBOX_KEY, dataResult);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
				ArrayList dataList = (ArrayList) session.getAttribute(Constants.OUTBOX_KEY);
				response.reset();
				
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_OUTBOXREPORT );
	     		printHeader(ouputStream);
	     		
	     		if (dataList != null){
		     		Iterator iter = dataList.iterator();
		     		
		     		while (iter.hasNext()){
		     			OutboxModal model = (OutboxModal) iter.next();
		     			 
		      			ouputStream.print (CommonUtils.println(model.getSeq()));
		      			ouputStream.print (CommonUtils.println(model.getDate()));
		      			ouputStream.print (CommonUtils.println(model.getMobileNo()));
		      			ouputStream.print (CommonUtils.println(model.getMessage()));
		      			ouputStream.print (CommonUtils.println(model.getSentBy()));
		      			ouputStream.print (CommonUtils.println(model.getApproval()));
		      			ouputStream.print (CommonUtils.println(model.getApprovalDatetime()));
		      			ouputStream.print (CommonUtils.println(model.getDept()));
		      			ouputStream.print (CommonUtils.println(model.getOperator()));
		      			ouputStream.print (CommonUtils.println(model.getPriority()));
		      			ouputStream.print (CommonUtils.println(model.getStatus()));
		      			ouputStream.print (CommonUtils.println(model.getRemarks()));

		     			ouputStream.println();
		     		}
	     		}
	     		ouputStream.flush();
                
                ouputStream.close();
                return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_OUTBOX+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_OUTBOX,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private String getUserList(String userId){
		ArrayList<String> userIdList=new ArrayList<String>();
		List prList_result =DBUtilsCrsmsg.DBHQLCommand("select userId from User where userGroup in (select userGroup from User u where userId = ?) ", new Object[]{userId});

		for(int i=0;i<prList_result.size();i++){
			userIdList.add(Constants.SPECIAL_QUOTE_SINGLE+prList_result.get(i).toString()+Constants.SPECIAL_QUOTE_SINGLE);
		}
		
		return CommonUtils.arrayToString(userIdList, Constants.SPECIAL_CHAR_COMMA);	
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Status Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Sent By"));
		ouputStream.print (CommonUtils.println("Approval"));
		ouputStream.print (CommonUtils.println("Approval Date"));
		ouputStream.print (CommonUtils.println("Dept"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Priority"));
		ouputStream.print (CommonUtils.println("Status"));
		ouputStream.print (CommonUtils.println("Remarks"));
		ouputStream.println();
	}
}
