package com.bcb.crsmsg.actions;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.jfree.util.Log;

import com.bcb.common.modal.AvailableDB;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.BillingReportForm;
import com.bcb.crsmsg.modal.BillingReportDetailModal;
import com.bcb.crsmsg.modal.BillingReportGstModal;
import com.bcb.crsmsg.modal.BillingReportHeaderModal;
import com.bcb.crsmsg.modal.BillingReportSummaryByTelcoModal;
import com.bcb.crsmsg.modal.BillingReportSummaryModal;
import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.FormatUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;
public class OutgoingBillingReport extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		//Check if having appropriate permission
		if ( theLogonCredential.allowed(Constants.APPSID,Constants.BILLING_REPORT) == false){
			return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
		};
		
		
		BillingReportForm billingReportForm =(BillingReportForm) form;
		
		boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
		billingReportForm.setAdmin(isAdmin);
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD) || mapping.getParameter().equals(Constants.PARAMETER_LOADINCOMING)){
			if (mapping.getParameter().equals(Constants.PARAMETER_LOADINCOMING)){
				session.setAttribute("billingreportaction", "I"); 
				billingReportForm.setStatus("I");
				//Check if having appropriate permission
				if ( theLogonCredential.allowed(Constants.APPSID,Constants.BILLING_REPORT_INCOMING) == false){
					return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
				};
			}else{
				session.setAttribute("billingreportaction", "O");
				billingReportForm.setStatus("O");
				//Check if having appropriate permission
				if ( theLogonCredential.allowed(Constants.APPSID,Constants.BILLING_REPORT) == false){
					return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
				};
			}
			BillingReportForm theForm =(BillingReportForm) form;
			List group_result = DBUtilsCrsmsg.DBHQLCommand("from AvailableDB where availableDBStatus = ? ", new Object[]{Constants.STATUS_ACTIVE});
			ArrayList<String> group_label = new ArrayList<String>();
			ArrayList<String> group_value = new ArrayList<String>();
			for (int i =0 ; i < group_result.size(); i ++)
			{
				AvailableDB aGroup = (AvailableDB) group_result.get(i); 
				group_label.add(aGroup.getName());
				group_value.add(aGroup.getHibernateValue());		
			}
			theForm.setSearchbyserver_label(group_label);
			theForm.setSearchbyserver_value(group_value);
		}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATE_BILLINGDETAILSCVS)){
			
			Session hibernate_session = HibernateUtil.getSessionFactoryServer(billingReportForm.getSelectedServer()).getCurrentSession();	
			try{
				String searchFromDate= (String) request.getParameter("startDate");
				String searchUntilDate= (String) request.getParameter("endDate");
				String dept = (String) request.getParameter("dept");
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE_SECOND);
				 ServletOutputStream ouputStream = response.getOutputStream();
				 
				if (session.getAttribute("billingreportaction").equals("I")){

					StringBuffer sql = new StringBuffer
					("select s.sms_id, s.mobile_no, s.message, s.dept, s.received_datetime,  t.telco_name, s.sms_charge");
					sql.append(" from sms_received s, telco t where s.telco=t.telco_id and ");
					
					if (searchFromDate.length() > 2){
						sql.append(" (s.received_datetime between ? and ?) and ");
					}
						sql.append(" s.dept like ? order by s.received_datetime desc");
						
					hibernate_session.beginTransaction();
					
					SQLQuery q;
					
					SimpleDateFormat theFormatter2=new SimpleDateFormat(Constants.DATE_yyyy_MM_dd_hh_mm_ss);
					
					if (searchFromDate.length() > 2){
						q = (SQLQuery) hibernate_session.createSQLQuery(sql.toString())
						.setString(0,theFormatter2.format(theFormatter.parse(searchFromDate)))
						.setString(1,theFormatter2.format(theFormatter.parse(searchUntilDate)))
						.setString(2, Constants.STR_PERCENT+dept+ Constants.STR_PERCENT)
						;
					}else{
						q = (SQLQuery) hibernate_session.createSQLQuery(sql.toString())
						.setString(0, Constants.STR_PERCENT+dept+ Constants.STR_PERCENT);
					}
					setMappingIncoming(q);
					Iterator iter= q.list().iterator();
					response.reset();
					
//					--------------------Generate Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     "BillingReportDetails_Incoming.csv" );
		     		printDetailsHeader_Incoming(ouputStream);
		     		int runningCounter =0;
		     		//s.sms_id, s.mobile_no, s.message, s.dept, s.received_datetime,  t.telco_name, s.sms_charge
					while(iter.hasNext()){
						
						runningCounter += 1;
						Object[] rowResult = (Object[]) iter.next();	
						ouputStream.print(CommonUtils.println(String.valueOf(runningCounter)));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[4])));
						ouputStream.print(CommonUtils.println((String) rowResult[1] ));
						ouputStream.print(CommonUtils.println((String) rowResult[2] ));
						ouputStream.print(CommonUtils.println((String) rowResult[3] ));
						ouputStream.print(CommonUtils.println((String) rowResult[5] ));
						ouputStream.print(CommonUtils.println((String) rowResult[6] ));						
						ouputStream.println();
					}
					hibernate_session.getTransaction().commit();
				}else{
				 
					StringBuffer sql = new StringBuffer
						("select s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept,");
						sql.append(" s.created_datetime, s.approval_by, s.sms_charge, t.telco_name ");
						sql.append(" from sms s, telco t where s.telco=t.telco_id and ");
						
					if (searchFromDate.length() > 2){
						sql.append(" (s.sms_status_datetime between ? and ?) and ");
					}
						sql.append(" s.sms_status = 'S' and s.dept like ? order by s.sms_status_datetime desc");
						
					hibernate_session.beginTransaction();
					
					SQLQuery  q;
					
					SimpleDateFormat theFormatter2=new SimpleDateFormat(Constants.DATE_yyyy_MM_dd_hh_mm_ss);
		
					if (searchFromDate.length() > 2){
						q = (SQLQuery) hibernate_session.createSQLQuery(sql.toString())
						.setString(0,theFormatter2.format(theFormatter.parse(searchFromDate)))
						.setString(1,theFormatter2.format(theFormatter.parse(searchUntilDate)))
						.setString(2, Constants.STR_PERCENT+dept+ Constants.STR_PERCENT)
						;
					}else{
						q = (SQLQuery) hibernate_session.createSQLQuery(sql.toString())
						.setString(0, Constants.STR_PERCENT+dept+ Constants.STR_PERCENT);
						}
					setMapping_BillingDetailsCSV(q); 
			
					Iterator iter= q.list().iterator();
					response.reset();
					
		           
		                
		            //--------------------Generate Report---------------
		     		response.setContentType(Constants.CONTENTTYPE_CSV);
		     		response.setHeader(Constants.HEADER,
		                     Constants.STR_FILENAME +
		                     "BillingReportDetails.csv" );
		     		printDetailsHeader(ouputStream);
		     		int runningCounter =0;
		     		//s.sms_status_datetime, s.mobile_no, s.message, s.sent_by,s.dept,s.created_datetime, s.approval_by, s.sms_charge, t.telco_name
					while(iter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) iter.next();	
						ouputStream.print(CommonUtils.println(String.valueOf(runningCounter)));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[0])));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[5])));
						ouputStream.print(CommonUtils.println((String) rowResult[1] ));
						ouputStream.print(CommonUtils.println((String) rowResult[2] ));
						ouputStream.print(CommonUtils.println((String) rowResult[3] ));
						ouputStream.print(CommonUtils.println((String) rowResult[4] ));
						ouputStream.print(CommonUtils.println((String) rowResult[6] ));
						ouputStream.print(CommonUtils.println((String) rowResult[8] ));
						ouputStream.print(CommonUtils.println(FormatUtil.getInstance().formatNumber(Double.parseDouble((String) rowResult[7] )/100, 4)));
						
						ouputStream.println();
					}
					hibernate_session.getTransaction().commit();
				}
				ouputStream.flush();
	            
	            ouputStream.close();
	            
	            return null;
			}catch(Exception ex){
				ex.printStackTrace();
				hibernate_session.getTransaction().rollback();				
			}finally{
				if(hibernate_session != null && hibernate_session.isOpen()){
					hibernate_session.close();
				}				
			}
		}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
		
				
			ArrayList dataList = (ArrayList) session.getAttribute(Constants.BILLINGBYDEPT_KEY);
			ArrayList telcoList = (ArrayList) session.getAttribute(Constants.BILLINGBYDEPT_KEY_SVC_PROVIDER);
			List<SmsRate> rates =  (List<SmsRate>) session.getAttribute(Constants.BILLINGBYDEPT_KEY_RATES);
			response.reset();
			
            ServletOutputStream ouputStream = response.getOutputStream();
                
                 //--------------------Generate Report---------------
     		response.setContentType(Constants.CONTENTTYPE_CSV);
     		
     		if (session.getAttribute("billingreportaction").equals("I")){
     			printHeader_incoming(ouputStream, telcoList);
     			response.setHeader(Constants.HEADER,
                        Constants.STR_FILENAME +
                        "BillingReportIncoming.csv" );
     		}else{
     			printHeader(ouputStream, telcoList, rates);
     			response.setHeader(Constants.HEADER,
                        Constants.STR_FILENAME +
                        "BillingReport.csv" );
     		}
     		
     		if (dataList != null){
     			ouputStream.println();
	     		Iterator iter = dataList.iterator();
	     		BillingReportSummaryByTelcoModal chargeModal;
	     		while (iter.hasNext()){
	     			BillingReportSummaryModal model = (BillingReportSummaryModal) iter.next();
	     			 
	      			ouputStream.print (CommonUtils.println(String.valueOf(model.getSeq())));
	      			ouputStream.print (CommonUtils.println(model.getDept()));
	      			ouputStream.print (CommonUtils.println(model.getUserId()));
	      			if(model.getChannelId() != null && model.getChannelId().equals("null")){
		      			ouputStream.print(Constants.STR_EMPTY);	      				
	      			}else{
		      			ouputStream.print (CommonUtils.println(model.getChannelId()));	      				
	      			}
	      		 	
	      			ArrayList chargeList = model.getTelcoModal();
	      			
	      			Iterator chargeIter = chargeList.iterator();
	      			while (chargeIter.hasNext()){
	      				chargeModal = (BillingReportSummaryByTelcoModal) chargeIter.next();
	      				ouputStream.print (CommonUtils.println(chargeModal.getTotalCharge()));
	      			}
	     			ouputStream.println();
	     		}
     		}
     		ouputStream.flush();
            
            ouputStream.close();
            return null;
		}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
			Session hibernate_session = null;				
			try{
				String searchFromDate= billingReportForm.getSelectedFromDate();
				String searchUntilDate= billingReportForm.getSelectedUntilDate();
				
				String startDate = Constants.STR_EMPTY;
				String endDate = Constants.STR_EMPTY;
				
				String words1[];
				String words2[];
				String wordsSub1[];
				String wordsSub2[];
				
				String itu = "1 1";
				words1 = itu.split(Constants.STR_EMPTYSTRING);
				words1[0] = "0001-01-01";
				words1[1] = "00:00:01";
				words2 = itu.split(" ");
				words2[0] = "9999-12-31";
				words2[1] = "23:59:59";
				
				String smsType = (String) session.getAttribute("billingreportaction");
				
				
				if (billingReportForm.getSelectedFromDate().length() > 0){
					wordsSub1 = searchFromDate.split("/");
					startDate = wordsSub1[2] + "-" + wordsSub1[1] + "-" + wordsSub1[0]  ;
				}
				
				if ((billingReportForm.getSelectedUntilDate().length() > 0)){
					
					wordsSub2 = searchUntilDate.split("/");
					endDate = wordsSub2[2] + "-" + wordsSub2[1] + "-" + wordsSub2[0];
					
				}
				if (billingReportForm.getSelectedFromDate().length() > 2){
					billingReportForm.setStartDate(billingReportForm.getSelectedFromDate() + " 00:00:01");
				}else{
					billingReportForm.setStartDate(Constants.STR_EMPTY);
				}
				if (billingReportForm.getSelectedUntilDate().length() > 2){
					billingReportForm.setEndDate(billingReportForm.getSelectedUntilDate() + " 23:59:59");
				}else{
					billingReportForm.setEndDate(Constants.STR_EMPTY);
				}
				
	//			List telcoList = getTelcoList(startDate,endDate, smsType);
	//			if (telcoList == null){
				List<Object[]> groupColumns = getDistinctGroupColumn(billingReportForm.getSelectedServer());
				if(groupColumns == null){
					ActionMessages messages = new ActionMessages();
		  			messages.add("apps",new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
		  			this.addErrors(request,messages);
		  			return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
				}else{
					
	//			ArrayList<String> sql_sum = getSummarySQL_SUM(telcoList);
	//			ArrayList<String> sql_sum_cost = getSummarySQL_COST(telcoList);
	//			
	//			ArrayList<String> sql_case_sum = getSummarySQL_CASE_SUM(telcoList);
	//			ArrayList<String> sql_case_cost = getSummarySQL_CASE_COST(telcoList);
				
				String dept = Constants.STR_EMPTY;
				if (billingReportForm.getSearchkey() ==null )
					dept = Constants.STR_EMPTY;
				else{
					dept = billingReportForm.getSearchkey().trim();
					
				}
				if(!isAdmin){
					dept = CommonUtils.getDeptName(theLogonCredential.getUserGroup());
				}
	
				//new query 20130208 - smsha phase2
				String countStr = "x";
				String costStr = "y";
				String gstStr = "z";
				
				StringBuffer mainQuerySb = new StringBuffer();
				StringBuffer subQuerySb = new StringBuffer();			
				StringBuffer subQueryTotalChargeSb = new StringBuffer(); 
				StringBuffer subQueryTotalCountSb = new StringBuffer();
				StringBuffer subQueryTotalGstSb = new StringBuffer();
				StringBuffer mainQueryTotalChargeSb = new StringBuffer(); 
				StringBuffer mainQueryTotalCountSb = new StringBuffer();
				StringBuffer mainQueryTotalGstSb = new StringBuffer();
				
				List<ServiceProvider> svcProviderName = new ArrayList<ServiceProvider>();
				List<SmsRate> rateName = new ArrayList<SmsRate>();
				ArrayList costNameList = new ArrayList();
				BillingReportGstModal telcoGst;
				BillingReportGstModal telcoGst2;
				Integer currentSvcPvdId = null;
				Integer sp = 0;
				Integer preSp = 0;
				
				int index = 0;
				for(Object[] groupColumn : groupColumns){
					Integer svcPvdId = (Integer) groupColumn[0];
					Integer rateId = (Integer) groupColumn[1];
					sp = svcPvdId;
					
					if(currentSvcPvdId == null || !currentSvcPvdId.equals(svcPvdId)){
						currentSvcPvdId = svcPvdId;
						svcProviderName.add(((ServiceProvider)DaoBeanFactory.getHibernateTemplate().get(ServiceProvider.class, currentSvcPvdId)));					
						svcProviderName.get(svcProviderName.size()-1).setColspan(1);
						svcProviderName.get(svcProviderName.size()-1).setGstColspan(2);
					}else{
						svcProviderName.get(svcProviderName.size()-1).setColspan(svcProviderName.get(svcProviderName.size()-1).getColspan()+1);
						svcProviderName.get(svcProviderName.size()-1).setGstColspan(svcProviderName.get(svcProviderName.size()-1).getColspan()+1);
					}
	
					rateName.add(((SmsRate)DaoBeanFactory.getHibernateTemplate().get(SmsRate.class, rateId)));					
					
					mainQueryTotalCountSb.append(", sum(a." + countStr + index + ") as " + countStr + index);
					mainQueryTotalChargeSb.append(", sum(a." + costStr + index + ") as " + costStr + index);
					mainQueryTotalGstSb.append(", sum(a." + gstStr + index + ") as " + gstStr + index);
					subQueryTotalCountSb.append(", ( case when svc_provider_id=" + svcPvdId + " and sms_rate_id=" + rateId + " then total_count else 0 end )as " + countStr + index);
					subQueryTotalChargeSb.append(", ( case when svc_provider_id=" + svcPvdId + " and sms_rate_id=" + rateId + " then total_charge else 0 end ) as " + costStr + index);
					subQueryTotalGstSb.append(", ( case when svc_provider_id=" + svcPvdId + " and sms_rate_id=" + rateId + " then gst_charge else 0 end ) as " + gstStr + index);
					index++;
					
						if(sp > preSp && preSp > 0 ){
							telcoGst = new BillingReportGstModal();
							telcoGst.setName(Constants.GST);
							costNameList.add(telcoGst);
							
							preSp = sp;
						}
					
						telcoGst2 = new BillingReportGstModal();
						telcoGst2.setName(getRate(rateId,billingReportForm.getSelectedServer()));
						costNameList.add(telcoGst2);
						preSp = sp;
					}
	
				subQuerySb.append("select dept, user_id, channel_id")
				.append(subQueryTotalChargeSb.toString())
				.append(subQueryTotalCountSb.toString())
				.append(subQueryTotalGstSb.toString())
				.append(" from SMS_CHARGE_DAILY where sms='O'");
				
/*				if (searchFromDate.length() > 2 || searchUntilDate.length() > 2){
					subQuerySb.append(" and CHARGE_DATE between ? and ? ");
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						subQuerySb.append(" and dept = '"+ dept + "'");
					}
				}else{
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						subQuerySb.append(" and dept = '"+ dept + "'");
					}
				}*/
				
				if (searchFromDate.length() > 2 || searchUntilDate.length() > 2){
					subQuerySb.append(" and CHARGE_DATE between ? and ? ");
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						subQuerySb.append(" and dept = ? ");
					}
				}else{
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						subQuerySb.append(" and dept = ? ");
					}
				}
				
				mainQuerySb.append("select a.dept, a.user_id, a.channel_id")
				.append(mainQueryTotalCountSb)
				.append(mainQueryTotalChargeSb.toString())
				.append(mainQueryTotalGstSb.toString())
				.append(" from (")
				.append(subQuerySb.toString())
				.append(")a")
				.append(" group by a.dept, a.user_id, a.channel_id");
				
				System.out.println("Main Query: '" + mainQuerySb.toString() + "'");
				
				//end smsha phase 2 query
				
				hibernate_session = HibernateUtil.getSessionFactoryServer(billingReportForm.getSelectedServer()).getCurrentSession();	
				hibernate_session.beginTransaction();
				
				SQLQuery query=null;
				List resultList;
				if (searchFromDate.length() > 2 || searchUntilDate.length() > 2){
					
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						query=(SQLQuery)hibernate_session.createSQLQuery(mainQuerySb.toString())
								.setString(0,startDate)
								.setString(1,endDate)
								.setString(2,dept);
					}else{
						query=(SQLQuery)hibernate_session.createSQLQuery(mainQuerySb.toString())
								.setString(0,startDate)
								.setString(1,endDate);
						
					}
					
					
				}else{
					
					if (!isAdmin || billingReportForm.getSearchkey().trim().length() != 0){
						query=(SQLQuery)hibernate_session.createSQLQuery(mainQuerySb.toString())
								.setString(0,dept);
								
					}else{
						query=hibernate_session.createSQLQuery(mainQuerySb.toString());
					}
					
					
				}
							   
				index = 0;
				query.addScalar("dept", Hibernate.STRING);
				query.addScalar("user_id", Hibernate.STRING);
				query.addScalar("channel_id", Hibernate.STRING);
				for(Object[] groupColumn : groupColumns){
					query.addScalar(countStr + index, Hibernate.DOUBLE);
					index++;
				}
				index=0;
				for(Object[] groupColumn : groupColumns){
					query.addScalar(costStr + index, Hibernate.DOUBLE);
					index++;
				}
				index=0;
				for(Object[] groupColumn : groupColumns){
					query.addScalar(gstStr + index, Hibernate.DOUBLE);
					index++;
				}
				
				resultList=query.list();
				hibernate_session.getTransaction().commit();
				ArrayList dataResult = new ArrayList();
				Iterator resultIter = resultList.iterator();
				int runningCounter = 0;
				Double grand_total_smsSent = new Double(0);
				Double grand_total_cost = new Double(0);
				Double grand_total_gst = new Double(0);
				Double grand_total_all = new Double(0);
				Map<Integer, BillingReportHeaderModal> telcoDetailsList = getTelcoHeader(svcProviderName);
				
				BillingReportHeaderModal subTotal_SMSModal;
				BillingReportHeaderModal postsubTotal_SMSModal;
				BillingReportSummaryByTelcoModal telcoCharge;
				BillingReportSummaryByTelcoModal telcoChargeGst;
				while (resultIter.hasNext()){
					runningCounter += 1;
					Object[] rowResult = (Object[]) resultIter.next();	
					BillingReportSummaryModal summary= new BillingReportSummaryModal();
					summary.setDept( (String) rowResult[0]);
					summary.setSeq(runningCounter);
					summary.setUserId((String) rowResult[1]);				
					if(!StringUtils.isEmpty((String) rowResult[2] ) && ((String) rowResult[2] ).equals("null")){
						summary.setChannelId("");					
					}else{
						summary.setChannelId((String) rowResult[2]);					
					}
					
					//testing column : this is for SMS Sent column
					ArrayList telcoChargeList = new ArrayList();
					Double count = new Double(0);
					for (int i = 0; i < rateName.size(); i ++){
						Double smsSent = (Double) rowResult[i+3];
						telcoCharge = new BillingReportSummaryByTelcoModal();						
						//subTotal_SMSSentModal = new BillingReportSummaryByTelcoModal();
						telcoCharge.setTotalCharge(getFormat((Double) rowResult[i+3]));
						telcoChargeList.add(telcoCharge);
						
						count =count + (Double) smsSent;
						subTotal_SMSModal = (BillingReportHeaderModal) telcoDetailsList.get(groupColumns.get(i)[0]);
						Double runningSubTotal = Double.parseDouble(subTotal_SMSModal.getTotalCharge_SMSSent()) + smsSent;
						
						subTotal_SMSModal.setTotalCharge_SMSSent(getFormat(runningSubTotal));
						
					}
					
					grand_total_smsSent = grand_total_smsSent + count;
					telcoCharge = new BillingReportSummaryByTelcoModal();
					telcoCharge.setTotalCharge(getFormat(count));
					telcoChargeList.add(telcoCharge);
					
					
					//this is for Cost column
					int rowCount = rateName.size();					
					Double runningCharge = new Double(0);
					Double runningGst = new Double(0);
					Double runningAll = new Double(0);
					Double postGst = new Double(0);
					String currTelco = "";
					String postTelco = "";
					double calcGst = new Double(0);
					
					BillingReportSummaryByTelcoModal subTotal_CostModal;
					for (int i = 0; i < rateName.size(); i ++){
						
						telcoCharge = new BillingReportSummaryByTelcoModal();
						telcoChargeGst = new BillingReportSummaryByTelcoModal();
						subTotal_CostModal = new BillingReportSummaryByTelcoModal();
						double charges = (Double) rowResult[i+3+rowCount]; //rowcount=RateName
						telcoCharge.setTotalCharge(
								FormatUtil.getInstance().formatNumber(
										charges, 2));
						runningCharge = runningCharge + charges;
						runningAll = runningAll + charges;
						telcoChargeList.add(telcoCharge); //by rate
						
						subTotal_SMSModal = (BillingReportHeaderModal) telcoDetailsList.get(groupColumns.get(i)[0]);
						Double runningSubTotal = Double.parseDouble(subTotal_SMSModal.getTotalCharge_Cost().replace(",","")) + charges;
						subTotal_SMSModal.setTotalCharge_Cost(FormatUtil.getInstance().formatNumber(runningSubTotal, 2));
												
						//GST
						double gst = (Double) rowResult[i+3+rateName.size()+rowCount]; 
						calcGst = calcGst + gst; // to calc gst by telco
						
						Double runningGstSubTotal = new Double(0);
						Double runningAllSubTotal = new Double(0);
						runningGstSubTotal = Double.parseDouble(subTotal_SMSModal.getTotalCharge_Gst().replace(",",""));//+ gst;
						//subTotal_SMSModal.setTotalCharge_Gst(FormatUtil.getInstance().formatNumber(runningGstSubTotal, 4));
						runningAllSubTotal = Double.parseDouble(subTotal_SMSModal.getTotalCharge_All().replace(",","")) + charges ;//+ gst;
						//subTotal_SMSModal.setTotalCharge_All(FormatUtil.getInstance().formatNumber(runningAllSubTotal, 4));
						
						
						String getSvp = subTotal_SMSModal.getHeader();
						currTelco = getSvp;
						
						if(i+1 < rateName.size()){
							postsubTotal_SMSModal = (BillingReportHeaderModal) telcoDetailsList.get(groupColumns.get(i+1)[0]);
							postTelco = postsubTotal_SMSModal.getHeader(); 
						}	
						 
						if (!currTelco.equals(postTelco) || i == rateName.size()-1){				
							telcoChargeGst.setTotalCharge(FormatUtil.getInstance().formatNumber(calcGst, 4));
							telcoChargeList.add(telcoChargeGst);
							
							postGst = Double.valueOf(FormatUtil.getInstance().formatNumber(calcGst, 4).replace(",",""));
							runningGst = runningGst + postGst; // to calc total gst
							runningAll = runningAll + postGst;
							runningGstSubTotal = runningGstSubTotal + postGst;
							runningAllSubTotal = runningAllSubTotal + postGst;
							calcGst = new Double(0);
						}
												
						subTotal_SMSModal.setTotalCharge_Gst(FormatUtil.getInstance().formatNumber(runningGstSubTotal, 4));
						subTotal_SMSModal.setTotalCharge_All(FormatUtil.getInstance().formatNumber(runningAllSubTotal, 4));					
					}
					
					grand_total_cost = grand_total_cost + runningCharge; // grant total cost without gst
					grand_total_gst = grand_total_gst + runningGst; // grant total gst
					grand_total_all = grand_total_all + runningAll;
					
					telcoCharge = new BillingReportSummaryByTelcoModal();
					//telcoCharge.setTotalCharge(FormatUtil.getInstance().formatNumber(runningCharge,2));
					telcoCharge.setTotalCharge(FormatUtil.getInstance().formatNumber(runningAll,4));
					telcoChargeList.add(telcoCharge); //total
					
					summary.setTelcoModal(telcoChargeList);
					
					dataResult.add(summary);
				}
				
				billingReportForm.setGrandTotal_Cost(FormatUtil.getInstance().formatNumber(grand_total_cost,2));
				billingReportForm.setGrandTotal_SMSSent(getFormat(grand_total_smsSent));
				billingReportForm.setGrandTotal_Gst(FormatUtil.getInstance().formatNumber(grand_total_gst,4));
				billingReportForm.setGrandTotal_All(FormatUtil.getInstance().formatNumber(grand_total_all,4));
				request.setAttribute("headerList", new ArrayList(telcoDetailsList.values()));
				session.setAttribute(Constants.BILLINGBYDEPT_KEY, dataResult);
	//			request.setAttribute(Constants.STR_COUNT, telcoList.size() + 1); 
	//			session.setAttribute(Constants.TELCOLIST_KEY, telcoList);
				request.setAttribute("telcoSize", svcProviderName.size() + 1); 
				request.setAttribute("telcoList", svcProviderName); 
				request.setAttribute("rateSize", rateName.size()+1);	
				request.setAttribute("gstRateSize", rateName.size()+1+svcProviderName.size());
				request.setAttribute("rateNameList", rateName); 
				request.setAttribute("costNameList", costNameList); 
				request.setAttribute("billingReportSummary", dataResult);
				session.setAttribute(Constants.BILLINGBYDEPT_KEY_RATES, rateName);
				session.setAttribute(Constants.BILLINGBYDEPT_KEY_SVC_PROVIDER, svcProviderName);
				
				}
				
			}catch (Exception E){
				E.printStackTrace();
				ActionMessages messages = new ActionMessages();
				messages.add("billingReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
			}finally{
				if(hibernate_session != null && hibernate_session.isOpen())		
					
					hibernate_session.close();
			}
		}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCHDETAILS)){
			Session hibernate_session = HibernateUtil.getSessionFactoryServer(billingReportForm.getSelectedServer()).getCurrentSession();	
			hibernate_session.beginTransaction();
			try{
			
			
				List prList_result=null;
				List export_result=null;
				
				Integer totalItem=new Integer(0);
				StringBuffer str = new StringBuffer();
				StringBuffer strTotal = new StringBuffer();	
			
				String offset = request.getParameter(Constants.STR_PAGER_OFFSET);
				if(billingReportForm.getButSearch()!=null&&billingReportForm.getButSearch().equals(Constants.STR_SEARCH)){
					offset = Constants.STR_ZERO;
				}
				
				if (offset != null && !offset.equals(Constants.STR_EMPTY)) {
					billingReportForm.setButSearch(Constants.STR_SEARCH);	
				}
				
				if (offset == null || offset.equals(Constants.STR_EMPTY)) {
					offset = Constants.STR_ZERO;
				}
				
				String searchFromDate= (String) request.getParameter("startDate");
				String searchUntilDate= (String) request.getParameter("endDate");
				String dept = (String) request.getParameter("dept");
	
				billingReportForm.setStartDate(searchFromDate);
				billingReportForm.setEndDate(searchUntilDate);
				billingReportForm.setDept(dept);
				
				billingReportForm.setSearchkey(dept);
				
				billingReportForm.setSelectedState((Constants.STR_BY_DEPARTMENT));
				
				String words1[];
				String words2[];
				String wordsSub1[];
				String wordsSub2[];
				
				String itu = "1 1";
				words1 = itu.split(Constants.STR_EMPTYSTRING);
				words1[0] = "0001-01-01";
				words1[1] = "00:00:01";
				words2 = itu.split(" ");
				words2[0] = "9999-12-31";
				words2[1] = "23:59:59";
	
				if (billingReportForm.getSelectedFromDate().length() > 0){
					words1 = searchFromDate.split(Constants.STR_EMPTYSTRING);
					wordsSub1 = words1[0].split("/");
					words1[0] = wordsSub1[2] + "-" + wordsSub1[1] + "-" + wordsSub1[0]  ;
					words1[1] = words1[1] + ":00";
				}
				if ((billingReportForm.getSelectedUntilDate().length() > 0)){
					words2 = searchUntilDate.split(Constants.STR_EMPTYSTRING);
					wordsSub2 = words2[0].split("/");
					words2[0] = wordsSub2[2] + "-" + wordsSub2[1] + "-" + wordsSub2[0];
					words2[1] = words2[1] + ":00";
				}
				String user = Constants.STR_EMPTY;
				
				String sql = "";
				
				if (session.getAttribute("billingreportaction") == null){
					
				}else if (session.getAttribute("billingreportaction").equals("I")){
					billingReportForm.setStatus("I");
					SQLQuery  q;
					
					StringBuffer countQuerySb = new StringBuffer("select count(s.sms_id) from sms_received s, telco t where s.telco=t.telco_id "); 
					str.append("select s.sms_id, s.mobile_no, s.message, s.dept, s.received_datetime,  t.telco_name, s.sms_charge ");
					str.append(" from sms_received s, telco t where s.telco=t.telco_id ");
					
					
					if (billingReportForm.getSearchkey().length() > 1){
						
					}
					
					List<Object> params = new ArrayList<Object>();
					if(dept != null && dept.length()>0){
						countQuerySb.append(" and dept=?");					
						str.append(" and dept=?");
						params.add(dept);
					}
					
					if (searchFromDate != null || searchUntilDate != null){
						str.append(" and (s.received_datetime between ? and ?)");
						str.append("order by s.received_datetime desc ");
						countQuerySb.append(" and (s.received_datetime between ? and ?)");
						countQuerySb.append("order by s.received_datetime desc ");
						
						params.add(words1[0] + Constants.STR_EMPTYSTRING + words1[1]);
						params.add(words2[0] + Constants.STR_EMPTYSTRING + words2[1]);
					}else{
						str.append("order by s.received_datetime desc ");
					}
					System.out.println("Main Query str: '" + str.toString() + "'");
					q = (SQLQuery) hibernate_session.createSQLQuery(str.toString());
					Query countQuery = hibernate_session.createSQLQuery(countQuerySb.toString());
					
					for(int i=0; i<params.size(); i++){
						q.setParameter(i, params.get(i));
						countQuery.setParameter(i, params.get(i));
					}		
					
					totalItem = ((BigInteger)countQuery.uniqueResult()).intValue();
					setMappingIncoming(q);
		 
					prList_result = q
					
					.setFirstResult(Integer.parseInt(offset)).setMaxResults(20).list();
					export_result= q.list();	
					
					
	//				totalItem = Integer.parseInt(String.valueOf((BigInteger) 
	//					hibernate_session.createSQLQuery("select found_rows()")
	//					.list().get(0)));
					
					if (prList_result !=null && prList_result.size() > 0){
						Iterator resultIter = prList_result.iterator();
						ArrayList dataResult = new ArrayList();
						int runningCounter = 0;
	
						while (resultIter.hasNext())
						{
							runningCounter += 1;
							
							Object[] rowResult = (Object[]) resultIter.next();	
							BillingReportDetailModal reportDetail= new BillingReportDetailModal();
							reportDetail.setId(runningCounter);
							reportDetail.setSeq(String.valueOf(runningCounter));
							reportDetail.setMobileNo( (String) rowResult[1] );
							reportDetail.setMessage( (String) rowResult[2] );
							reportDetail.setDept((String) rowResult[3]  );
							reportDetail.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[4]));
							reportDetail.setOperator((String) rowResult[5]  );
							reportDetail.setCost(FormatUtil.getInstance().formatNumber(Double.parseDouble((String) rowResult[6])/100, 2));
							
						    dataResult.add(reportDetail);
						}
					
						request.setAttribute(Constants.STR_COUNT, totalItem); 
						request.setAttribute("billingReportDetails", dataResult);
					}
				}else{

	//				if (!isAdmin)
	//					user = getUserList(hibernate_session, theLogonCredential.getUserId());
	//				
					
					if(billingReportForm.getSearchkey()!=null&&(billingReportForm.getSearchkey()).length()<1){
						if (searchFromDate != null || searchUntilDate != null){
							if (isAdmin){
								sql = "select count(sms_id) from sms where (sms_status_datetime between ? and ?) and sms_status = 'S'";
							}else{
								sql = "select count(sms_id) from sms where (sms_status_datetime between ? and ?) and sent_by in(" + user + ") and sms_status = 'S'";
							}
							
							totalItem = Integer.parseInt(String.valueOf((BigInteger) 
									hibernate_session.createSQLQuery(sql)
									.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
									.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
									//.setString(2,theLogonCredential.getUserId())
									.list().get(0)));
						}else{
							if (isAdmin){
								sql = "select count(sms_id) from sms where sms_status = 'S'";
							}else{
								sql = "select count(sms_id) from sms where sent_by in(" + user + ") and sms_status = 'S'";
								
							}
							totalItem = Integer.parseInt(String.valueOf((BigInteger) hibernate_session.createSQLQuery(sql)
									.list().get(0)));
						}
	
						if(offset .equals(Constants.STR_ZERO)){
							if (searchFromDate != null || searchUntilDate != null){
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge ");
								str.append(" from sms s, telco t where s.telco=t.telco_id and (s.sms_status_datetime between ? and ?)");
								if (isAdmin){
									str.append(" and s.sms_status = 'S'");
								}else{
									//str.append(" and s.sent_by in(" + user + ") and s.sms_status = 'S' ");
									str.append(" and s.sms_status = 'S' ");
								}
								
								str.append("order by s.sms_status_datetime desc");
								
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString())
								
								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1]);
								
								setMapping(q);
								export_result= q.list();					   
								prList_result = q.setFirstResult(0).setMaxResults(20).list();
								
							}else{
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge ");
								str.append(" from sms s, telco t where s.telco=t.telco_id and s.sms_status = 'S' ");
//								if (!isAdmin){
//									str.append(" and s.sent_by in(" + user + ") ");		
//								}
								
								str.append(" order by s.sms_status_datetime desc");
								System.out.println("Main Query str2: '" + str.toString() + "'");
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString());
								
//								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
//								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1]);
//								
								setMapping(q); 

								export_result= q.list();					   
								prList_result = q.setFirstResult(0).setMaxResults(20).list();
								
							}
						}else{
							if (searchFromDate != null || searchUntilDate != null){
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime , s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge ");
								str.append(" from sms s, telco t where s.telco=t.telco_id and (s.sms_status_datetime between ? and ?) and s.sms_status = 'S'");
//								if (!isAdmin){
//									str.append(" and s.sent_by in(" + user + ") ");
//								}
								str.append("order by sms_status_datetime desc ");
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString())
								
								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1]);
								
								setMapping(q);

													   
								prList_result = q
								
//								prList_result = hibernate_session.createSQLQuery(str.toString())
//								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
//								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
								//.setString(2,theLogonCredential.getUserId())
								.setFirstResult(Integer.parseInt(offset)).setMaxResults(20).list();
								export_result= q.list();
							}else{
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge  ");
								str.append("from sms s, telco t where s.telco=t.telco_id and s.sms_status = 'S' ");
								if (!isAdmin){
									str.append(" and s.sent_by in(" + user + ") ");
								}
								str.append("order by s.sms_status_datetime desc ");
								
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString());
								
//								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
//								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1]);
								
								setMapping(q);
								export_result= q.list();					   
								prList_result = q
								
								//prList_result = hibernate_session.createSQLQuery(str.toString())
								//.setString(0,theLogonCredential.getUserId())
								.setFirstResult(Integer.parseInt(offset)).setMaxResults(20).list();
								
							}
						}
							
						}else{
							String  asID=CommonUtils.NullChecker(billingReportForm.getSearchkey(), String.class).toString() ;
			                String queryParam = "";
			                if ( billingReportForm.getSelectedState().equals(Constants.STR_BY_MOBILE))
							{
								queryParam = "s.mobile_no";						 
							}else if ( billingReportForm.getSelectedState().equals(Constants.STR_BY_SENDER))
							{
								queryParam = "s.sent_by";
							}else if ( billingReportForm.getSelectedState().equals(Constants.STR_BY_DEPARTMENT))
							{
								queryParam = "s.dept";
							}else if ( billingReportForm.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR))
							{
								queryParam = "t.telco_name";
							/*}else if ( sentLogForm.getSelectedState().equals(Constants.STR_BY_SEND_MODE))
							{
								queryParam = "mode";*/
							}else if ( billingReportForm.getSelectedState().equals(Constants.STR_BY_SEND_TYPE))
							{
								queryParam = "s.type";
							}
			                 else{
			                }
								
			                if (searchFromDate != null || searchUntilDate != null){
	//		                	if (isAdmin){
			                		strTotal.append("select count(s.sms_id) from sms s, telco t where s.telco=t.telco_id and UPPER("+ queryParam +") like ? and (s.sms_status_datetime between ? and ?) and s.sms_status = 'S'");
	//		                	}else{
	//		                		strTotal.append("select count(s.sms_id) from sms s, telco t where s.telco=t.telco_id and UPPER("+ queryParam +") like ? and (s.sms_status_datetime between ? and ?) and s.sent_by in(" + user + ") and s.sms_status = 'S'");
	//		                	}
								totalItem =Integer.parseInt(String.valueOf((BigInteger) hibernate_session.createSQLQuery(strTotal.toString())
								.setString(0, Constants.STR_PERCENT+asID.toUpperCase()+ Constants.STR_PERCENT)
								.setString(1,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
								.setString(2,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
								//.setString(3,theLogonCredential.getUserId())
								.list().get(0)));
			                }else{
	//		                	if (isAdmin){
			                		strTotal.append("select count(s.sms_id) from sms s, telco t where s.telco=t.telco_id and UPPER("+ queryParam +") like ? and s.sms_status = 'S'");
	//		                	}else{
	//		                		strTotal.append("select count(s.sms_id) from sms s, telco t where s.telco=t.telco_id and UPPER("+ queryParam +") like ? and s.sent_by in(" + user + ") and s.sms_status = 'S'");
	//		                	}
								totalItem =Integer.parseInt(String.valueOf((BigInteger) hibernate_session.createSQLQuery(strTotal.toString())
								.setString(0, Constants.STR_PERCENT+asID.toUpperCase()+ Constants.STR_PERCENT)
								.list().get(0)));
			                }
			                
							request.setAttribute(Constants.STR_COUNT, totalItem);
							
							if (searchFromDate != null || searchUntilDate != null){
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge ");
								str.append("from sms s, telco t where s.telco=t.telco_id and (s.sms_status_datetime between ? and ?) and s.sms_status = 'S' ");
								if(offset.equals(Constants.STR_ZERO)){
									
	//								if (isAdmin){
										str.append( " and "+ queryParam +" like :var  ");
	//								}else{
	//									str.append( " and s.sent_by in(" + user + ") and "+ queryParam +" like :var  ");
	//								}
									
								}else{
	//								if (isAdmin){
										str.append( " and "+ queryParam +" like :var  ");
	//								}else{
	//									str.append( " and s.sent_by in(" + user + ") and "+ queryParam +" like :var  ");
	//								}
								}
								
								str.append( " order by s.sms_status_datetime desc " );
								
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString())
								
								.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
								.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
								.setString(Constants.STR_VAR,Constants.STR_PERCENT+asID.toUpperCase()+Constants.STR_PERCENT);
								
								setMapping(q);
	
								export_result= q.list();					   
								prList_result = q
	//							prList_result = hibernate_session.createSQLQuery(str.toString())
	//							.setString(0,words1[0] + Constants.STR_EMPTYSTRING + words1[1])
	//							.setString(1,words2[0] + Constants.STR_EMPTYSTRING + words2[1])
								//.setString(2,theLogonCredential.getUserId())
								
								.setFirstResult(Integer.parseInt(offset)).setMaxResults(20).list();
							}else{
								str.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time, t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.sms_charge ");
								
								if(offset.equals(Constants.STR_ZERO)){
	//								if (isAdmin){
										str.append( "from sms s, telco t where s.telco=t.telco_id and s.sms_status = 'S' and "+ queryParam +" like :var ");
	//								}else{
	//									str.append( "from sms s, telco t where s.telco=t.telco_id and s.sent_by = ? and s.sms_status = 'S' and "+ queryParam +" like :var order by s.sms_status_datetime desc ");
	//								}
								}else{
	//								str.append( "from sms s, telco t where s.telco=t.telco_id and s.sent_by in(" + user + ") and s.sms_status = 'S' and "+ queryParam +" like :var ");
									str.append( "from sms s, telco t where s.telco=t.telco_id and s.sms_status = 'S' and "+ queryParam +" like :var ");
								}
								
								str.append( " order by s.sms_status_datetime desc " );
								
								SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString())
								
								.setString(Constants.STR_VAR,Constants.STR_PERCENT+asID.toUpperCase()+Constants.STR_PERCENT);
								
								setMapping(q); 
								export_result= q.list();					   
								prList_result = q
								
	//							prList_result = hibernate_session.createSQLQuery(str.toString())
	//							//.setString(0,theLogonCredential.getUserId())
	//							.setString(Constants.STR_VAR,Constants.STR_PERCENT+asID.toUpperCase()+Constants.STR_PERCENT)
								.setFirstResult(Integer.parseInt(offset)).setMaxResults(20).list();
								
							}
							
						}
					if (prList_result !=null && prList_result.size() > 0){
						Iterator resultIter = prList_result.iterator();
						ArrayList dataResult = new ArrayList();
						int runningCounter = 0;

						
						while (resultIter.hasNext())
						{
							runningCounter += 1;
							
							Object[] rowResult = (Object[]) resultIter.next();	
							BillingReportDetailModal reportDetail= new BillingReportDetailModal();
							reportDetail.setId(runningCounter);
							reportDetail.setSeq(String.valueOf(runningCounter));
							//sentLog.setDate( (String) rowResult[1] );
							reportDetail.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
							reportDetail.setMobileNo( (String) rowResult[2] );
							reportDetail.setMessage( (String) rowResult[3] );
							reportDetail.setSentBy( (String) rowResult[4] );
							reportDetail.setDept((String) rowResult[5]  );
							reportDetail.setScheduledStatus(CommonUtils.NullChecker(rowResult[12], String.class).toString() );
							if(reportDetail.getScheduledStatus().equals(Constants.STATUS_YES))
								reportDetail.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
							else
								reportDetail.setScheduled(Constants.STR_EMPTY);
							
							reportDetail.setOperator((String) rowResult[7]  );
							reportDetail.setType((String) rowResult[8]  );
							reportDetail.setRemarks((String) rowResult[9]  );
							reportDetail.setDeliveryReportDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[10]));
							reportDetail.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[11]));
							reportDetail.setApproval(CommonUtils.NullChecker(rowResult[13], String.class).toString());
							if( rowResult[14]!=null){
								reportDetail.setApprovalDatetime( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[14]));
							}else{
								reportDetail.setApprovalDatetime(Constants.STR_EMPTY);
							}
							reportDetail.setCost(FormatUtil.getInstance().formatNumber(Double.parseDouble((String) rowResult[15])/100, 2));
						    dataResult.add(reportDetail);
						}
					
						request.setAttribute(Constants.STR_COUNT, totalItem); 
						request.setAttribute("billingReportDetails", dataResult);
					}
				}
				hibernate_session.getTransaction().commit();
			}catch (Exception E){
				E.printStackTrace();
				ActionMessages messages = new ActionMessages();
				messages.add("billingReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
			}finally{
				if(hibernate_session !=null && hibernate_session.isOpen())		
					
					hibernate_session.close();
			}
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private List<Object[]> getDistinctGroupColumn(String selectedServer){
		Session session = null;
		try{
			session = HibernateUtil.getSessionFactoryServer(selectedServer).getCurrentSession();
			session.beginTransaction();
			Query query = session.createSQLQuery("select distinct svc_provider_id,sms_rate_id from SMS_CHARGE_DAILY where sms='O' order by svc_provider_id");
			List<Object[]>results = query.list();
			if(results == null || results.isEmpty()) return null;
			session.getTransaction().commit();
			return results;
		}catch(Exception e){
			Log.error("Error " + e.getMessage());
			session.getTransaction().rollback();
			return null;
		}finally{
			if(session != null && session.isOpen()){
				session.close();
			}
		}
		
	}
	
	private void printDetailsHeader(ServletOutputStream ouputStream) throws IOException {
		// TODO Auto-generated method stub
		ouputStream.print(CommonUtils.println("No."));
		ouputStream.print(CommonUtils.println("Sent Date"));
		ouputStream.print(CommonUtils.println("Created Date"));
		ouputStream.print(CommonUtils.println("Mobile Number"));
		ouputStream.print(CommonUtils.println("Message"));
		ouputStream.print(CommonUtils.println("Sent By"));
		ouputStream.print(CommonUtils.println("Department"));
		ouputStream.print(CommonUtils.println("Approval By"));
		ouputStream.print(CommonUtils.println("Telco"));
		ouputStream.print(CommonUtils.println("Cost (RM)"));
		ouputStream.println();
	}
	
	private void printDetailsHeader_Incoming(ServletOutputStream ouputStream) throws IOException {
		// TODO Auto-generated method stub
		ouputStream.print(CommonUtils.println("No."));
		ouputStream.print(CommonUtils.println("Received Date"));
		ouputStream.print(CommonUtils.println("Mobile Number"));
		ouputStream.print(CommonUtils.println("Message"));
		ouputStream.print(CommonUtils.println("Department"));
		ouputStream.print(CommonUtils.println("Telco"));
		ouputStream.print(CommonUtils.println("Cost (RM)"));
		ouputStream.println();
	}

	private ArrayList<String> getSummarySQL_CASE_SUM(List telcoList) {
		
		ArrayList<String> sql = new ArrayList<String>();
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			Integer telcoId = (Integer) telcoList.get(i);
			sql.add("(case TELCO_ID when "+ String.valueOf(telcoId) + " then TOTAL_COUNT ELSE 0 END) as a" + String.valueOf(telcoId));
		}
		
		// TODO Auto-generated method stub
		return sql;
	}
	
	private ArrayList<String> getSummarySQL_CASE_COST(List telcoList) {
		
		ArrayList<String> sql = new ArrayList<String>();
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			Integer telcoId = (Integer) telcoList.get(i);
			sql.add("(case TELCO_ID when "+ String.valueOf(telcoId) + " then (TOTAL_CHARGE/100) ELSE 0 END) as b" + String.valueOf(telcoId));
		}
		
		// TODO Auto-generated method stub
		return sql;
	}

	private ArrayList<String> getSummarySQL_COST(List telcoList) {
		
		ArrayList<String> sql = new ArrayList<String>();
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			Integer telcoId = (Integer) telcoList.get(i);
			sql.add("sum(a.b"+ String.valueOf(telcoId) + ") as b" + String.valueOf(telcoId));
		}
		
		// TODO Auto-generated method stub
		return sql;
	}
	
	private ArrayList<String> getSummarySQL_SUM(List telcoList) {
		
		ArrayList<String> sql = new ArrayList<String>();
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			Integer telcoId = (Integer) telcoList.get(i);
			sql.add("sum(a.a"+ String.valueOf(telcoId) + ") as a" + String.valueOf(telcoId));
		}
		
		// TODO Auto-generated method stub
		return sql;
	}

//	private String getUserList(Session hibernate_session, String u){
//		StringBuffer  userId = new StringBuffer(Constants.STR_EMPTY);
//		
//		Integer group_id =(Integer) hibernate_session.createSQLQuery("select u.user_group from crsmsg.user u where u.user_id = ? ")
//		.setString(0,u).list().get(0);
//		
//		List prList_result = hibernate_session.createSQLQuery("select u.user_id  from crsmsg.user u where  u.user_group = ? ")
//		.setInteger(0,group_id).list();
//		
//		Iterator iter = prList_result.iterator();
//		while (iter.hasNext()){
//			String user =  (String) iter.next();
//			userId.append("'");
//			userId.append(user);
//			userId.append("',");
//		}
//		
//		return userId.substring(0, userId.length() -1);
//		
//	}
	
	private SQLQuery setMapping(SQLQuery q){
		//sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time, type, remarks, delivery_report_date
		q.addScalar("sms_id", Hibernate.INTEGER);
		q.addScalar("sms_status_datetime", Hibernate.TIMESTAMP);
		q.addScalar("mobile_no", Hibernate.STRING); 
		q.addScalar("message", Hibernate.STRING);
		q.addScalar("sent_by", Hibernate.STRING);
		q.addScalar("dept", Hibernate.STRING);
		q.addScalar("sms_scheduled_time", Hibernate.TIMESTAMP);
		q.addScalar("telco_name", Hibernate.STRING);
		q.addScalar("type", Hibernate.STRING);
		q.addScalar("remarks", Hibernate.STRING);
		q.addScalar("delivery_report_date", Hibernate.TIMESTAMP);
		q.addScalar("created_datetime", Hibernate.TIMESTAMP);
		q.addScalar("sms_scheduled", Hibernate.STRING);
		q.addScalar("approval_by", Hibernate.STRING);
		q.addScalar("approval_datetime", Hibernate.TIMESTAMP);
		q.addScalar("sms_charge", Hibernate.STRING);
		return q;
	}
	
	private SQLQuery setMappingIncoming(SQLQuery q){
		
		q.addScalar("sms_id", Hibernate.INTEGER);
		q.addScalar("mobile_no", Hibernate.STRING); 
		q.addScalar("message", Hibernate.STRING);
		q.addScalar("dept", Hibernate.STRING);
		q.addScalar("received_datetime", Hibernate.TIMESTAMP);
		q.addScalar("telco_name", Hibernate.STRING);
		q.addScalar("sms_charge", Hibernate.STRING);  
		return q;
	}
	
	private SQLQuery setMapping1(SQLQuery q, int smsSize, int costSize, List telcoList){
		//sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time, type, remarks, delivery_report_date
		q.addScalar("dept", Hibernate.STRING);
		
		for(int i=0;i<telcoList.size();i++){
			Integer telcoId = (Integer) telcoList.get(i);
			q.addScalar("a"+Integer.toString(telcoId), Hibernate.DOUBLE);
		}
		
		for(int i=0;i<telcoList.size();i++){
			Integer telcoId = (Integer) telcoList.get(i);
			q.addScalar("b"+Integer.toString(telcoId), Hibernate.DOUBLE);
		}
		
		return q;
	}
	
	private SQLQuery setMapping_BillingDetailsCSV(SQLQuery q){
		//s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.created_datetime, s.approval_by, s.sms_charge, t.telco_name 
		
		q.addScalar("sms_status_datetime", Hibernate.TIMESTAMP);
		q.addScalar("mobile_no", Hibernate.STRING); 
		q.addScalar("message", Hibernate.STRING);
		q.addScalar("sent_by", Hibernate.STRING);
		q.addScalar("dept", Hibernate.STRING);
		q.addScalar("created_datetime", Hibernate.TIMESTAMP);
		q.addScalar("approval_by", Hibernate.STRING);
		q.addScalar("sms_charge", Hibernate.STRING);
		q.addScalar("telco_name", Hibernate.STRING);
		return q;
	}
	
	private void printHeader(ServletOutputStream ouputStream, List telcoList, List<SmsRate> rates ) throws Exception{
		
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Department"));
		ouputStream.print (CommonUtils.println("User Id"));
		ouputStream.print (CommonUtils.println("Channel Id"));
		if (rates == null){
			ouputStream.print (CommonUtils.println("SMS Sent"));
			ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
			ouputStream.print (CommonUtils.println("Cost (RM)"));
		}else {
			ouputStream.print (CommonUtils.println("SMS Sent"));
			for (int i = 0; i < rates.size(); i ++){
				ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
			}
//			ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));//for total column header
			ouputStream.print (CommonUtils.println("Cost (RM)"));
		}
		
		//2nd row
		ouputStream.println();		
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		
		
		if(telcoList == null || telcoList.isEmpty()) return;
				
		//for count columns
		for (int i = 0; i < telcoList.size(); i ++)
		{
			
			ServiceProvider svcPvd = (ServiceProvider) telcoList.get(i);
			ouputStream.print (CommonUtils.println(svcPvd.getName()));
			for(int x=1; x<svcPvd.getColspan(); x++){
				ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));				
			}
			
		}
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));//total header				

		//for charge columns
		for (int i = 0; i < telcoList.size(); i ++)
		{
			
			ServiceProvider svcPvd = (ServiceProvider) telcoList.get(i);
			ouputStream.print (CommonUtils.println(svcPvd.getName()));
			for(int x=1; x<svcPvd.getGstColspan(); x++){
				ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));				
			}
			
		}
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));//total header
		
		//3rd row
		ouputStream.println();		
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		for(SmsRate rate : rates){//count columns
			ouputStream.print (CommonUtils.println(rate.getName()));							
		}
		ouputStream.print (CommonUtils.println("Total"));//total header
		int j=0;
		for (int i = 0; i < telcoList.size(); i ++)
		{
			ServiceProvider svcPvd = (ServiceProvider) telcoList.get(i);
			for(int x=1; x<svcPvd.getGstColspan(); x++){ 
				SmsRate rate = (SmsRate) rates.get(j);
				ouputStream.print (CommonUtils.println(rate.getName()));
				j++;
			}
			ouputStream.print (CommonUtils.println(Constants.GST));
		}
		ouputStream.print (CommonUtils.println("Total include GST (RM)"));//total header
	}
	
	private void printHeader_incoming(ServletOutputStream ouputStream, List telcoList ) throws Exception{
		
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Department"));
		if (telcoList.size() == 1 ){
			ouputStream.print (CommonUtils.println("SMS Received"));
			ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
			ouputStream.print (CommonUtils.println("Cost (RM)"));
		}else if(telcoList.size() > 1 ){
			ouputStream.print (CommonUtils.println("SMS Received"));
			for (int i = 1; i < telcoList.size(); i ++){
				ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
			}
			ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
			ouputStream.print (CommonUtils.println("Cost (RM)"));
		}
		ouputStream.println();
		
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			
			Integer telcoId = (Integer) telcoList.get(i);
			ouputStream.print (CommonUtils.println(CommonUtils.getTelcoName(telcoId)));
			
		}
		ouputStream.print (CommonUtils.println("Total"));
		for (int i = 0; i < telcoList.size(); i ++)
		{
			
			Integer telcoId = (Integer) telcoList.get(i);
			ouputStream.print (CommonUtils.println(CommonUtils.getTelcoName(telcoId)));
			
		}
		ouputStream.print (CommonUtils.println("Cost (RM)"));
		ouputStream.println();
	}

	//should get list from SMS_CHARGE_DAILY because telco might disabled anytime
	private static List getTelcoList(String startDate, String endDate, String smsType)
	{
		try{
			
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			hibernate_session.beginTransaction();
			StringBuffer sql = new StringBuffer("select distinct(telco_id) from SMS_CHARGE_DAILY where sms ='" + smsType + "'");
			
			List resultList;
			if (startDate.length()> 2){
				resultList=hibernate_session.createSQLQuery(sql.toString() + " and CHARGE_DATE between ? and ? order by telco_id")
				.setString(0,startDate)
				.setString(1,endDate)
				.list();
			}else{
				resultList=hibernate_session.createSQLQuery(sql.toString()+" order by telco_id").list();
			}
			
			if (resultList.size() > 0){
				return resultList;
			}
			
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			//ex.printStackTrace();
		}
		
		
		return null;
	}
	
	private String getFormat(Double value){
		DecimalFormat format = new DecimalFormat("0");
		return format.format(value);
		
	}
	
	private Map<Integer, BillingReportHeaderModal> getTelcoHeader(List<ServiceProvider> telcoList){
		Map<Integer, BillingReportHeaderModal> telcoDetailsList = new HashMap<Integer, BillingReportHeaderModal>();
		
		BillingReportHeaderModal telcoDetailsModal;
		
		for (int i = 0; i < telcoList.size(); i ++)
		{
			telcoDetailsModal = new BillingReportHeaderModal();
			Integer telcoId =  telcoList.get(i).getId();
			telcoDetailsModal.setHeader( telcoList.get(i).getName());
			telcoDetailsModal.setTotalCharge_Cost("0");
			telcoDetailsModal.setTotalCharge_SMSSent("0");	
			telcoDetailsModal.setTotalCharge_Gst("0");
			telcoDetailsModal.setTotalCharge_All("0");
			telcoDetailsList.put(telcoId,telcoDetailsModal );
		}
		
		return telcoDetailsList;
	}
	
	private String getRate(int rateId,String selectedServer){	
		String rateName = "";
		try{			
			Session hibernate_session = HibernateUtil.getSessionFactoryServer(selectedServer).getCurrentSession();
			hibernate_session.beginTransaction();
			
			List getTelco = hibernate_session.createQuery("from SmsRate where id = ?").setInteger(0, Integer.valueOf(rateId)).list(); 						
			if (getTelco.size() > 0 )
    		{	
				SmsRate sp = (SmsRate)getTelco.get(0);				
				rateName = sp.getName();
    		}
			hibernate_session.getTransaction().commit();
		}
		catch (Exception ex)
		{
			//System.out.println("ERROR : " + ex);
			ex.printStackTrace();
		}
		return rateName;
	}	
	
	
}


