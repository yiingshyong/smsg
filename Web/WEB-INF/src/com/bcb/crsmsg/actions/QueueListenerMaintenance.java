package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.QueueListenerForm;
import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class QueueListenerMaintenance extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		QueueListenerForm theForm = (QueueListenerForm) form;
		if(theForm.getId() != 0){
			theForm.setMq((SmsMQ) DaoBeanFactory.getSmsMQDao().findById(theForm.getId()));
			if(theForm.getMq() == null) theForm.setMq(new SmsMQ());
		}
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			SmsMQ existingRecord = (SmsMQ) DaoBeanFactory.getHibernateTemplate().get(SmsMQ.class, theForm.getMq().getMqId());
			existingRecord.setModifiedBy(CommonUtils.getLogonCredential(request).getUserId());
			existingRecord.setModifiedDatetime(new Date());
			existingRecord.setNoOfThread(theForm.getMq().getNoOfThread());
			DaoBeanFactory.getHibernateTemplate().saveOrUpdate(existingRecord);
		}
		theForm.setMqs(DaoBeanFactory.getSmsMQDao().findAll());
		return mapping.findForward(Constants.MAPPINGVALUE_SUCCESS);
	}
}
