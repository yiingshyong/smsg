package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.AvailableDB;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.ReceiveLogForm;
import com.bcb.crsmsg.modal.InboxModal;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsReceived;
import com.bcb.crsmsg.modal.SmsReceivedDelete;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.HibernateUtilCrsmsg;

public class ReceiveLogSearch extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.RECEIVED_LOG) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
				ReceiveLogForm theForm =(ReceiveLogForm) form;
				List group_result = DBUtilsCrsmsg.DBHQLCommand("from AvailableDB where availableDBStatus = ? ", new Object[]{Constants.STATUS_ACTIVE});
				ArrayList<String> group_label = new ArrayList<String>();
				ArrayList<String> group_value = new ArrayList<String>();
				for (int i =0 ; i < group_result.size(); i ++)
				{
					AvailableDB aGroup = (AvailableDB) group_result.get(i); 
					group_label.add(aGroup.getName());
					group_value.add(aGroup.getHibernateValue());		
				}
				theForm.setSearchbyserver_label(group_label);
				theForm.setSearchbyserver_value(group_value);
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
				ReceiveLogForm theForm =(ReceiveLogForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());
				
				/* Define User Rights */
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				String user=Constants.STR_EMPTY;

				if (isAdmin){// not restricted by user, show all
					user=Constants.STR_EMPTY;
				}else if (isDept){// restricted by cases from users of same department
					user = getUserList(theLogonCredential.getUserId());
				}else if(isPersonal){// restricted by own cases
					user=Constants.SPECIAL_QUOTE_SINGLE+theLogonCredential.getUserId()+Constants.SPECIAL_QUOTE_SINGLE;
				}
				/* Define User Rights */

				/* Delete Records */
				if(theForm.getDelete()!=null&& theForm.getDelete().equals("Delete Record(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					String[] genSelBox = theForm.getSelectedMsg();
					SmsReceived  smsReceived = null;
					SmsReceivedDelete  smsReceivedDelete = new SmsReceivedDelete();
					BeanUtilsBean utilBean = new BeanUtilsBean();
					String status=Constants.STR_EMPTY;
					
					Session hibernate_session = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();	
					for(int i=0;i<genSelBox.length;i++){
						smsReceived=(SmsReceived)DBUtilsCrsmsg.get(SmsReceived.class, Integer.parseInt(genSelBox[i]), hibernate_session);
						if (smsReceived!=null){
							smsReceivedDelete = new SmsReceivedDelete();
							utilBean.copyProperties((Object)smsReceivedDelete, (Object)smsReceived);

							if(DBUtilsCrsmsg.saveOrUpdate(smsReceivedDelete, hibernate_session)){
								if(DBUtilsCrsmsg.delete(smsReceived, hibernate_session)){
									status=Constants.STATUS_SUCCESS;
								}else{// fail to delete smsReceived
									status=Constants.STATUS_FAIL;
								}
							}else{// fail to save smsReceivedDelete
								status=Constants.STATUS_FAIL;
							}
							
							SmsAuditTrail anAuditTrail = new SmsAuditTrail();
							anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "ReceivedLog Delete",smsReceivedDelete.getId().toString() ,status,theLogonCredential.getBranchCode());
							DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						}else{// Can't find the sms received
						}
					}
					
					if(status.equals(Constants.STATUS_SUCCESS)){
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_RECEIVEDLOG_SEARCH,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}else{
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_RECEIVEDLOG_SEARCH,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}
				}
				/* Delete Records */

				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "r.mobile_no";						 
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_CUST_REF_NO)){
					queryParam = "c.cust_ref_no";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_KEYWORD)){
					queryParam = "k.keyword_name";
				}else{// Can't find match selected state
                }
				/* Define search selected state*/
				
				/* Get total no. of sms */
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(r.sms_id) from sms_received r left outer join customer c on r.mobile_no=c.mobile_no left outer join keyword k on r.keyword=k.keyword_id where (r.received_datetime between ? and ?)"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and k.ownership in (" + user + ")":Constants.STR_EMPTY),
						new String[]{}, 
						new NullableType[]{}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT},
						theForm.getSelectedServer()
						).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());
				System.out.println("offset value=="+offset);
				sql.append("select r.sms_id, r.received_datetime, r.mobile_no, c.cust_ref_no, k.keyword_name, r.message, k.user_name, k.dept_name, r.remarks ");
				sql.append("from sms_received r left outer join customer c on r.mobile_no=c.mobile_no left outer join (select kw.keyword_id, kw.keyword_name, u.user_name, d.dept_name, kw.ownership from keyword kw, user u, department d where kw.ownership=u.user_id and u.user_group=d.dept_id) k on r.keyword=k.keyword_id " +
						   "where (r.received_datetime between ? and ?) "+
						   " and (UPPER("+ queryParam +") like ? "+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						   (user.length()>0?" and k.ownership in (" + user + ")":Constants.STR_EMPTY));
				//sql.append(" order by r.received_datetime desc LIMIT "+offset+", "+pageSize);
				
				DBUtilsCrsmsg dbObj = new DBUtilsCrsmsg(sql.toString(), 
						new String[]{"r.sms_id", "r.received_datetime", "r.mobile_no", "c.cust_ref_no", "k.keyword_name", "r.message", "k.user_name", "k.dept_name", "r.remarks"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});

				resultList= DBUtilsCrsmsg.DBSQLCommand(
						
						dbObj.getCommand() + "order by r.received_datetime desc LIMIT "+offset+", "+pageSize,
						dbObj.getCmdParamName(),
						dbObj.getCmdType(),
						dbObj.getCmdParamObj(),
						theForm.getSelectedServer()
						);
						//export to excel issue
						/*sql.toString(),
						new String[]{"r.sms_id", "r.received_datetime", "r.mobile_no", "c.cust_ref_no", "k.keyword_name", "r.message", "k.user_name", "k.dept_name", "r.remarks"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});*/
				/*while(offset>=resultList.size()&&offset!=0){
					offset-=pageSize;// back one page
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<InboxModal> dataResult = new ArrayList<InboxModal>();
					int runningCounter = 0;

					while (resultIter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) resultIter.next();	
						InboxModal inBox= new InboxModal();
						//inBox.setId(Integer.valueOf(CommonUtils.NullChecker(rowResult[0], Integer.class).toString()));
						inBox.setId(runningCounter);
						inBox.setSeq(String.valueOf(runningCounter));
						inBox.setReceivedDateTime( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						inBox.setMobileNo( (String) rowResult[2] );
						inBox.setCustRefNo( (String) rowResult[3] );
						inBox.setKeyword((String) rowResult[4]  );
						inBox.setMessage( (String) rowResult[5] );
						inBox.setOwnership((String) rowResult[6]);
						inBox.setDepartment((String) rowResult[7]);
						inBox.setRemarks((String) rowResult[8]  );
					    dataResult.add(inBox);
					}
					request.setAttribute(Constants.INBOX_RESULT_KEY, dataResult);
					session.setAttribute(Constants.RECEIVEDLOG_KEY, dataResult);
					session.setAttribute(Constants.DBOBJ, dbObj);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
				//ArrayList dataList = (ArrayList) session.getAttribute(Constants.RECEIVEDLOG_KEY);
				DBUtilsCrsmsg dbObj = (DBUtilsCrsmsg) session.getAttribute(Constants.DBOBJ);
				response.reset();
				ReceiveLogForm theForm =(ReceiveLogForm) form;
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_RECEIVEDLOGREPORT );
	     		printHeader(ouputStream);
	     		
	     		Session hibernate_session = HibernateUtilCrsmsg.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();
				ScrollableResults resultList = DBUtilsCrsmsg.DBSQLScrollCommand(dbObj.getCommand(), dbObj.getCmdParamName(), dbObj.getCmdType(), dbObj.getCmdParamObj(), hibernate_session);
	     		
				if(resultList != null){
					int runningCounter = 0;
					while (resultList.next()){
						runningCounter++;
						Object[] rowResult = resultList.get();	
						ouputStream.print (CommonUtils.println(String.valueOf(runningCounter)));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1])));
						ouputStream.print (CommonUtils.println((String) rowResult[2]));
						ouputStream.print (CommonUtils.println((String) rowResult[3]));
						ouputStream.print (CommonUtils.println((String) rowResult[4]));
						ouputStream.print (CommonUtils.println((String) rowResult[5] ));
						ouputStream.print (CommonUtils.println((String) rowResult[6]));
						ouputStream.print (CommonUtils.println((String) rowResult[7]));
						ouputStream.print (CommonUtils.println((String) rowResult[8]));
						
						ouputStream.println();
					}
					
					/*if (dataList != null){
		     		Iterator iter = dataList.iterator();
		     		
		     		while (iter.hasNext()){
		     			InboxModal model = (InboxModal) iter.next();
		     			 
		      			ouputStream.print (CommonUtils.println(model.getSeq()));
		      			ouputStream.print (CommonUtils.println(model.getReceivedDateTime()));
		      			ouputStream.print (CommonUtils.println(model.getMobileNo()));
		      			ouputStream.print (CommonUtils.println(model.getCustRefNo()));
		      			ouputStream.print (CommonUtils.println(model.getKeyword()));
		      			ouputStream.print (CommonUtils.println(model.getMessage()));
		      			ouputStream.print (CommonUtils.println(model.getOwnership()));
		      			ouputStream.print (CommonUtils.println(model.getDepartment()));
		      			ouputStream.print (CommonUtils.println(model.getRemarks()));

		     			ouputStream.println();
		     		}
	     		}*/
				}
	     		ouputStream.flush();
                
                ouputStream.close();
                return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_RECEIVEDLOG_SEARCH+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_RECEIVEDLOG_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private String getUserList(String userId){
		ArrayList<String> userIdList=new ArrayList<String>();
		List prList_result =DBUtilsCrsmsg.DBHQLCommand("select userId from User where userGroup in (select userGroup from User u where userId = ?) ", new Object[]{userId});

		for(int i=0;i<prList_result.size();i++){
			userIdList.add(Constants.SPECIAL_QUOTE_SINGLE+prList_result.get(i).toString()+Constants.SPECIAL_QUOTE_SINGLE);
		}
		
		return CommonUtils.arrayToString(userIdList, Constants.SPECIAL_CHAR_COMMA);	
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Received Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Cust Ref No."));
		ouputStream.print (CommonUtils.println("Keyword"));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("User Name"));
		ouputStream.print (CommonUtils.println("Dept Name"));
		ouputStream.print (CommonUtils.println("Remarks"));
		ouputStream.println();
	}
}
