package com.bcb.crsmsg.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsReceived;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.EncryptionManager;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ReceiveSMSAction extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9202997410760781458L;
	private static final Log log = LogFactory.getLog(ReceiveSMSAction.class);
	private static String APPLICATION_ERROR = "";
	int sms_id = 0;
	Session hibernate_session = null;
	int maxAttempts = 0;
	int interval = 0;
	SimpleDateFormat theFormat = new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();		
		
		SmsReceived aSms = null;
		String returnMsg = Constants.STR_EMPTY;
			
		Keyword theKeyword = null;		
		StringBuffer incomingDataBuffer = new StringBuffer(request.getRemoteAddr());
		ArrayList<String> paramArray=new ArrayList<String>();
		long startTime = System.currentTimeMillis();
		long stopTime = 0;
		Map<String, String> parameters = new HashMap<String, String>();
		
		try {
		
			//try to get shortcode as usual
			Boolean encrypted = false;
			String shortcode = null;
			shortcode = request.getParameter("shortcode");
			log.debug("getParameter shortcode:-----"+shortcode);
			
			//cannot get shortcode due to encryption
			if(shortcode == null )	{
				String queryString = request.getQueryString();
				log.debug("request.getQueryString():"+request.getQueryString());
			
				//decrypt the query string and populate into an array based on parameters
				String decodedEncryption = URLDecoder.decode(queryString, "UTF-8");
				String decryptedQueryString = EncryptionManager.getInstance().decrypt(decodedEncryption);
				String decoded = URLDecoder.decode(decryptedQueryString, "UTF-8");
				String[] pares = decoded.split("&");
				
				for(String pare : pares) {
				    String[] nameAndValue = pare.split("=");
				    parameters.put(nameAndValue[0], nameAndValue[1]);
				}
				
				shortcode = parameters.get("shortcode");
				log.debug("parameter shortcode:"+shortcode);
				
				if(shortcode != null && shortcode.length()>0 ){
					encrypted = true;
				}
			}
			
			if(shortcode == null )	{
				returnMsg = APPLICATION_ERROR;
			}
			
			if(shortcode != null){
				
				log.debug("shortcode not null:"+shortcode);
				incomingDataBuffer.append(" ").append(shortcode);
				//MacroKiosk = 
				String[][] paramList;
				if (AppPropertiesConfig.getInstance().getSettings().getList(AppPropertiesConfig.SMS_RECEIVED_MK_SHORTCODE).contains(shortcode)){
					paramList=new String[][]{{"Mobile","Message","Timestamp","ServiceNum","","","","", "", ""},{"from","text","","shortcode","msgid","telcoid", "","","", "",""}};
					returnMsg = Constants.STR_STATUS_MK;
					log.debug("mk returnMsg:"+returnMsg);
				}else{
					paramList=new String[][]{{"Mobile","Message","Timestamp","ServiceNum","","","","", "", ""},{"from","text","","shortcode","msgid","rkey","subsid","telcoid", "dnstatus", "reportid"}};
					returnMsg = Constants.STATUS_OK;
					log.debug("returnMsg:"+returnMsg);
				}
				
				String param=Constants.STR_EMPTY;
			
				String paramName = null;
				incomingDataBuffer.append(" *");				
				for(int i=0;i<paramList[0].length;i++){
					paramName = paramList[0][i];
					if(encrypted){
						param = parameters.get(paramName);		
						log.debug("param1:"+param);
					}else{
						
						param = request.getParameter(paramName);
					}				
					if(StringUtils.isEmpty(param)){
						paramName = paramList[1][i];
						if(encrypted){
							param = parameters.get(paramName);	
							log.debug("param2:"+param);
						}else{
							param = request.getParameter(paramName);
						}
					}
					if(!paramName.equalsIgnoreCase("message") && !paramName.equalsIgnoreCase("text")){
						incomingDataBuffer.append(paramName).append(":").append(param).append(",");						
					}
					
					paramArray.add(param);
					
				}
				incomingDataBuffer.append("*");
				
			
				SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
				String[] msgTokens = StringUtils.splitByWholeSeparator(paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE)), " ");	
							
				if(theCfg==null || msgTokens.length == 0 || (theKeyword=extractKeyword(msgTokens[0])) == null){
					returnMsg = APPLICATION_ERROR;
				}
			}
			if(!returnMsg.equals(APPLICATION_ERROR)){ 
				incomingDataBuffer.append(" keyword: " + theKeyword.getKeywordName());
				Telco telco = getTelco(shortcode);
				
				aSms = new SmsReceived();
				log.debug("theKeyword:"+theKeyword);
				aSms.setKeyword(theKeyword==null?-1:theKeyword.getId());
				String msgNoKeyword = removeKeyword(aSms.getKeyword(), paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MESSAGE)));
				
				if(theKeyword != null && theKeyword.isEncryption()){
					//encrypt message content
					String encryptedData = SecurityManager.getInstance().encrypt(msgNoKeyword.trim());
					aSms.setMessage(encryptedData);
				}else{
					aSms.setMessage(msgNoKeyword);
				}
				
				aSms.setMsgNoKeyword(msgNoKeyword);
				
				if(DaoBeanFactory.getAutoActionDao().isCreditCardActivationKeyword(aSms.getKeyword())){
					msgNoKeyword = ActionCCActivation.normalizeCCActivationSMSRequestWithMask(msgNoKeyword);					
				}
				incomingDataBuffer.append(" '").append(msgNoKeyword).append("'");
				
				aSms.setMobileNo(paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_MOBILE_NO)));				
				aSms.setDept(getDept(theKeyword.getOwnership()));
				
				if (telco != null){
					aSms.setTelco(String.valueOf(telco.getId()));
					aSms.setSmsCharge(telco.getCostIncoming());
				}else{
					throw new Exception("No Telco Found for shortcode: '" + shortcode + "'");
				}
			
				if(CommonUtils.NullChecker(paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_RECEIVED_DATETIME)), String.class).toString().length()==0){
					aSms.setReceivedDatetime(new Date());
				}else{
					aSms.setReceivedDatetime(theFormat.parse(paramArray.get(Integer.valueOf(Constants.SMS_INCOME_PARAM_RECEIVED_DATETIME))));
				}
				
				aSms.setRemarks(Constants.STR_EMPTY);
				aSms.setVersion(Integer.valueOf(Constants.APPSVERSION));
				
				hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				if(!DBUtilsCrsmsg.saveOrUpdate(aSms, hibernate_session)){
					returnMsg = APPLICATION_ERROR; log4jUtil.error("ReceiveSMS - save into DB");
				}
			
				if(theKeyword!=null){
				}
			}
			
			
			if(hibernate_session == null){
				hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				
			}
			String auditStatus = returnMsg.equals(APPLICATION_ERROR)? APPLICATION_ERROR : Constants.STATUS_SUCCESS;
			SmsAuditTrail anAuditTrail = new SmsAuditTrail();
			anAuditTrail.setAudit("HTTP Receive",request.getRemoteAddr(),
					com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SMS_ACTION_RECEIVE,aSms==null? "" : aSms.getId().toString() ,auditStatus,Constants.STR_EMPTY);			
			if(!DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
				throw new Exception("Save audit trail failed");
			}
			
			out.println(returnMsg);						
			out.close();
			stopTime = System.currentTimeMillis();
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);			
			if(!returnMsg.equals(APPLICATION_ERROR)){
				try{
					if(aSms != null){
						theKeyword.setSmsReceive(aSms);
					}
					performAutoAction(theKeyword, paramArray);
				}catch(Exception e){
					
					log.error("Error performing auto action. Reason: " + e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			
			log.error("Error in processing incoming sms: Reason:" + e.getMessage(), e);
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			returnMsg = APPLICATION_ERROR;
			out.println(returnMsg);								
			out.close();
		}finally{
			stopTime = System.currentTimeMillis();
			incomingDataBuffer.append(" ").append(returnMsg);
			incomingDataBuffer.append(" ").append(stopTime-startTime);
			log.debug(incomingDataBuffer.toString());
		}
	}  	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}   
	
	protected boolean validation(HttpServletRequest request){
		
		try{
			if((!request.getParameter("path").equals(Constants.STR_EMPTY)) && 
					(!request.getParameter("path").equals(null))){
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	protected String validateTime(String time){
		try{
			SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATE_YYYY_MM_DD_HHmmss);
			theFormat.parse(time);
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return "Invalid time format!";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private Keyword extractKeyword(String keyword){
		log4jUtil.error("inside extractKeyword method");
		List<Keyword> keywordList = DBUtilsCrsmsg.DBHQLCommand("from Keyword where lcase(keywordName) = ? and status='Active'", new Object[]{keyword.toLowerCase()});		
		if(keywordList != null && !keywordList.isEmpty()){
			return keywordList.get(keywordList.size()-1);
		}
		return null;
	}
	
	private String removeKeyword(int keyword, String message){
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		Keyword theKeyword=(Keyword)DBUtilsCrsmsg.get(Keyword.class, keyword, hibernate_session);
		if(theKeyword!=null){
			if(message.toUpperCase().contains(theKeyword.getKeywordName().toUpperCase())){
				int indexOf=message.toUpperCase().indexOf(theKeyword.getKeywordName().toUpperCase());
				int lastIndexOf=message.toUpperCase().indexOf(theKeyword.getKeywordName().toUpperCase())+theKeyword.getKeywordName().length();
				message=message.substring(0, indexOf).trim()+Constants.STR_SPACE+message.substring(lastIndexOf).trim();
			}
		}
		DBUtilsCrsmsg.closeTransaction(hibernate_session);
		return message;
	}
	
	@SuppressWarnings("unchecked")
	private void performAutoAction(Keyword theKeyword, ArrayList<String> paramArray) throws Exception{
		log4jUtil.error("inside performAutoAction");
		List actionList=DBUtilsCrsmsg.DBHQLCommand("from AutoAction where keyword=? order by actionSequence", new Object[]{theKeyword.getId()});
		final Keyword remarksKeyword = theKeyword;
		
		for(int i=0;i<actionList.size();i++){
			 ActionTemplate anAction =  (ActionTemplate) Class.forName(((AutoAction)actionList.get(i)).getActionClassName()).newInstance();
			//redirectURL starts here
			if (((AutoAction)actionList.get(i)).getActionClassName().equalsIgnoreCase("com.bcb.crsmsg.actions.ActionRedirectUrl")){
				
				String[] msgRedirect = StringUtils.splitByWholeSeparator(paramArray.get(1), "", 2);
				log.info("msgRedirect.length=="+msgRedirect.length);
				if(msgRedirect.length>=0){
					if (msgRedirect.length < 2){
						log.info("message empty");
						paramArray.set(1, "");	
						paramArray.set(4, msgRedirect[0]);
					}else{
						log.info("with message");
					paramArray.set(1, msgRedirect[1]);	
					paramArray.set(4, msgRedirect[0]);
					}
				}
				paramArray.set(2, theFormat.format(new Date()));
				
					
				/*log4jUtil.info("mobile number is=="+paramArray.get(0));
				log4jUtil.info("message is=="+paramArray.get(1));
				log4jUtil.info("timestamp is=="+paramArray.get(2));
				log4jUtil.info("shortcode is=="+paramArray.get(3));
				log4jUtil.info("keyword is=="+paramArray.get(4));*/
				
				log4jUtil.error("to redirectURL");
				
				try{			
					maxAttempts = Integer.parseInt(theKeyword.getMaxRedirect_attempts())+1;
					interval = (Integer.parseInt(theKeyword.getRedirectInterval()))*1000;
					
					int initialDelay = 0; // start after 30 seconds
					int period = interval;        //  150000 for2.5 minutes  
					final Keyword timerKeyword = theKeyword;
					final ArrayList<String> timerParamArray = paramArray;
					final ActionTemplate timerAction = anAction;
									
					final Timer timer = new Timer();
					TimerTask task = new TimerTask() {
						int noAttempts = 0;
						public void run() {
							if(noAttempts<=maxAttempts){
								noAttempts++;
								log4jUtil.error("noAttempts in timer task:"+noAttempts);
								
								String returnCode = timerAction.execute(timerKeyword, timerParamArray);							
								log4jUtil.error("returnCode:"+returnCode);
								System.out.println("return code in receivesms action=="+returnCode);
									if((noAttempts == maxAttempts)||(returnCode.equalsIgnoreCase(Constants.REDIRECT_SUCCESS_CODE))){//cancel if attempts equals 4 or repeat is false
																						
											timer.cancel();
											hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
											    											
											    //sms_id = 0;
												if(!(remarksKeyword.getSmsReceive().getId() == 0)){
													Query query3 = hibernate_session.createSQLQuery(
															"select * from sms_received sr where sr.sms_id = :sms_id")
															.addEntity(SmsReceived.class)
															.setParameter("sms_id", remarksKeyword.getSmsReceive().getId());
													List result = query3.list();
												
												SmsReceived bSms = (SmsReceived)result.get(0);
												
												if(returnCode.equalsIgnoreCase(Constants.REDIRECT_SUCCESS_CODE)){
													bSms.setRemarks(Constants.REDIRECT_SUCCESS_REMARK);
													}else{
														bSms.setRemarks(Constants.REDIRECT_FAILURE_REMARK);
													}
												DBUtilsCrsmsg.saveOrUpdate(bSms, hibernate_session);
											//	DBUtilsCrsmsg.closeTransaction(hibernate_session);		
												if(hibernate_session!=null&&hibernate_session.isOpen()&&hibernate_session.getTransaction().isActive()){
													try{
														hibernate_session.getTransaction().commit();				
														
													}catch(Exception ex){														
														ex.printStackTrace();														
													}
													/*finally{
														hibernate_session.close();
													}*/
												}
										}}
							}
						}							 
					};
					timer.scheduleAtFixedRate(task, initialDelay, period);
				    
					}catch (Exception ex){
						log.error("RedirectUrl Process Init Error: " + ex);
						}
					}
			else{
			anAction.execute(theKeyword, paramArray);
			log4jUtil.error("performAutoAction");
			
			}
		}
	}
	
	/*
	 * need to redo this
	 * 
	 * */
	@SuppressWarnings("unchecked")
	private String getDept(String ownership){
		
		StringBuffer query = new StringBuffer(Constants.STR_SPACE);
		query.append("select d.deptName " +
				"from Keyword k, Department d, User u " +
				"where u.userId like ? and u.userGroup=d.id " +
				"order by k.keywordName"
				);
		List result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
			Constants.STR_PERCENT + ownership + Constants.STR_PERCENT
			});
		
		if (result.size() > 0){
			return (String) result.get(0);
		}else {
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	private Telco getTelco(String shortcode){
		
		StringBuffer query = new StringBuffer("from Telco where shortCode like ? ");
		
		List result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
			Constants.STR_PERCENT + shortcode + Constants.STR_PERCENT
			});
		
		if (result.size() > 0){
			Telco telco = (Telco) result.get(0);
			return telco;
		}else {
			
			return null;
		}
	}
	
}