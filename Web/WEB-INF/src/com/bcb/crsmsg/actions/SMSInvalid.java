package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.SmsInvalid;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;


public class SMSInvalid extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listInvalidSMS", null); 
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				hibernate_session.beginTransaction();
				String id = request.getParameter("refId");
				
				 List prList_result = null;
				
				String sql ="";
				
				sql = "from SmsInvalid where ftp = ? order by createdDatetime desc";
				
				prList_result = hibernate_session.createQuery(sql)
				.setInteger(0,Integer.parseInt(id)).list();				

				session.setAttribute("listInvalidSMS", prList_result); 

			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				//System.out.println("==================TRYING TO GENERATE CSV>>>>>>>>>>");	
				ArrayList dataList = (ArrayList) session.getAttribute("listInvalidSMS");
				response.reset();
				
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_INVALIDREPORT );
	     		printHeader(ouputStream);
	     		
	     		for(int i=0;dataList!=null&&i<dataList.size();i++){
		     			SmsInvalid model = (SmsInvalid) dataList.get(i);
		      			ouputStream.print (CommonUtils.println(Integer.toString(i)));
		      			ouputStream.print (CommonUtils.println(model.getInvalidRecord()));
		      			ouputStream.print (CommonUtils.println(model.getRemarks()));
		      			ouputStream.print (CommonUtils.println(model.getType()));
		      			ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(model.getCreatedDatetime(), String.class).toString()));
		      			ouputStream.print (CommonUtils.println(model.getCreatedBy()));

		     			ouputStream.println();
	     		}
	     		ouputStream.flush();
                
                ouputStream.close();
                
                return null;
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Invalid Record"));
		ouputStream.print (CommonUtils.println("Remark"));
		ouputStream.print (CommonUtils.println("Type"));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Created By"));
		ouputStream.println();
		
	}
	
}
