package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.OutboxModal;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;


public class SMSSent extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{

			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listSMSSent", null); 
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS))
			{
				ArrayList dataList = (ArrayList) session.getAttribute(Constants.TOTALSMS_KEY);
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_SENTREPORT );
				printHeader(ouputStream);
				
				if (dataList != null){
					Iterator iter = dataList.iterator();
					
					Integer i=1;
					while (iter.hasNext()){
						OutboxModal model = (OutboxModal) iter.next();
						
						ouputStream.print (CommonUtils.println(model.getSeq()));
						ouputStream.print (CommonUtils.println(model.getCreated()));
						ouputStream.print (CommonUtils.println(model.getDate()));
						ouputStream.print (CommonUtils.println(model.getMobileNo()));
						ouputStream.print (CommonUtils.println(model.getMessage()));
						ouputStream.print (CommonUtils.println(model.getOperator()));
		      			ouputStream.print (CommonUtils.println(model.getScheduled()));
						ouputStream.print (CommonUtils.println(model.getApprovalDatetime()));
						
						ouputStream.println();
						i++;
					}
				}
				ouputStream.flush();
				
				ouputStream.close();
				return null;
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				hibernate_session.beginTransaction();
				String id = request.getParameter("refId");

				 List prList_result = null;
				
				String sql ="";
				
				sql="select id, smsStatusDatetime, mobileNo, message, sentBy, dept, smsScheduledTime,  type, remarks, approvalBy, priority, smsStatus, telco, createdDatetime, smsScheduled, approvalDatetime "+
					"from Sms "+
					"where ftp= ? and smsStatus=? order by createdDatetime desc";
				
				prList_result = hibernate_session.createQuery(sql)
				.setInteger(0,Integer.parseInt(id))
				.setString(1, Constants.SMS_STATUS_1).list();				

				if (prList_result !=null && prList_result.size() > 0){
					ArrayList<OutboxModal> dataResult=CommonUtils.populateOutboxModal(prList_result);
					
					request.setAttribute("listSMSSent", dataResult); 
					session.setAttribute(Constants.TOTALSMS_KEY, dataResult);
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("SmsSentReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Sent Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Scheduled Date"));
		ouputStream.print (CommonUtils.println("Approve Date"));
		ouputStream.println();
	}
}
