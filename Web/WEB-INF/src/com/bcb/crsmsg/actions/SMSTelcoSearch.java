

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.TelcoSearchForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class SMSTelcoSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
		hibernate_session.beginTransaction();
		
		try{
			TelcoSearchForm TelcoSearch = (TelcoSearchForm) form;
			
			if(mapping.getParameter().equals("load")){
			}else{
				
				String telcoQuery = "";
				String allQuery = "";
				//ArrayList<String> conditionQuery=new ArrayList<String>();
				List telcoList=null;
				//Customer Query
				
				allQuery = "from Telco ";//custRefNo
								
				if (TelcoSearch.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR)){ 
					telcoQuery = "where telcoName like ?";
				}else if (TelcoSearch.getSelectedState().equals(Constants.STR_BY_PREFIX)){
					telcoQuery = "where telcoPrefix like ?";
				}else{
					//do nothing
				}
				allQuery = allQuery + telcoQuery;
				telcoList = DBUtilsCrsmsg.DBHQLCommand(allQuery, new Object[]{Constants.STR_PERCENT +TelcoSearch.getSearchkey()+ Constants.STR_PERCENT});
				 
				
				request.setAttribute("telcoList", telcoList);
				
				
				if(TelcoSearch.getDeleteBtn()!=null&& TelcoSearch.getDeleteBtn().equals("Delete Selected Telco(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					String[] genSelBox = TelcoSearch.getSelectedMsg();
					
					int genCnt = 1;
					List genMsgList = null;
					Telco  telcoMod = new Telco();					
					
					hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					hibernate_session.beginTransaction();
					for(int i=0;i<genSelBox.length;i++){
						genMsgList=hibernate_session.createQuery("from Telco where id = ?")
						.setInteger(0, Integer.parseInt(genSelBox[i]))
						.list();
						if (genMsgList.size()==1){
							
							telcoMod = (Telco)genMsgList.get(0);						
							
							hibernate_session.delete(telcoMod);
							
							SmsAuditTrail anAuditTrail = new SmsAuditTrail();
							anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "ReceivedLog Delete",Integer.toString(telcoMod.getId()) ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
							DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						}
						genCnt++;
					}
					
					ActionMessages messages = new ActionMessages();
					messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_RECORDDELETE));
					this.addErrors(request,messages);
					
					hibernate_session.getTransaction().commit();
				}
			}	
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_ADDRESSBOOK_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_ADDRESSBOOK_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}	
	
}
