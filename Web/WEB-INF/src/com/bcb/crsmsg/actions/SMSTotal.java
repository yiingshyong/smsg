package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.modal.OutboxModal;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.log4jUtil;


public class SMSTotal extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add("logon",new ActionMessage("errors.logon.notlogon"));
			this.addErrors(request,messages);
			return (mapping.findForward("notlogon"));
			
		};
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{

			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
				request.setAttribute("listSMSTotal", null); 
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS))
			{
				ArrayList dataList = (ArrayList) session.getAttribute(Constants.TOTALSMS_KEY);
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_TOTALREPORT );
				printHeader(ouputStream);
				
				if (dataList != null){
					Iterator iter = dataList.iterator();
					
					Integer i=1;
					while (iter.hasNext()){
						OutboxModal model = (OutboxModal) iter.next();
						
						ouputStream.print (CommonUtils.println(model.getSeq()));
						ouputStream.print (CommonUtils.println(model.getCreated()));
						ouputStream.print (CommonUtils.println(model.getMobileNo()));
						ouputStream.print (CommonUtils.println(model.getMessage()));
						ouputStream.print (CommonUtils.println(model.getOperator()));
						ouputStream.print (CommonUtils.println(model.getStatus()));
		      			ouputStream.print (CommonUtils.println(model.getScheduled()));
						ouputStream.println();
						i++;
					}
				}
				ouputStream.flush();
				
				ouputStream.close();
				return null;
			}else if(mapping.getParameter().equals(Constants.STR_SEARCH)){
				hibernate_session.beginTransaction();
				String id = request.getParameter("refId");

				 List prList_result = null;
				
				StringBuffer str = new StringBuffer();
				str.append("select r.sms_id, r.sms_status_datetime, r.mobile_no, r.message, r.sent_by, r.dept, r.sms_scheduled_time,  r.type, r.remarks, r.approval_by, r.priority, r.sms_status, r.telco, r.created_datetime, r.sms_scheduled, r.approval_datetime ");
				str.append("from (select sms_id, ftp, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, sms_scheduled, approval_datetime from sms where ftp= ? union select sms_id, ftp, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks, approval_by, priority, sms_status, telco, created_datetime, sms_scheduled, approval_datetime from sms_queue where ftp= ?) r ");
				str.append("order by r.created_datetime desc");
				
				SQLQuery  q = (SQLQuery) hibernate_session.createSQLQuery(str.toString()).setInteger(0,Integer.parseInt(id)).setInteger(1,Integer.parseInt(id));
				log4jUtil.info("Generated query: '" + q.getQueryString() + "'");
				setMapping(q);
				   
				prList_result = q.list();

				if (prList_result !=null && prList_result.size() > 0){
					ArrayList<OutboxModal> dataResult=CommonUtils.populateOutboxModal(prList_result);
					
					request.setAttribute("listSMSTotal", dataResult);
					session.setAttribute(Constants.TOTALSMS_KEY, dataResult);
				}
			}
		}catch (Exception E){
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}finally{
			if(hibernate_session.isOpen())		
				hibernate_session.close();
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private SQLQuery setMapping(SQLQuery q){
		//sms_id, sms_status_datetime, mobile_no, message, sent_by, dept, sms_scheduled_time,  type, remarks
		//approval_by,priority,sms_status
		q.addScalar("sms_id", Hibernate.INTEGER);
		q.addScalar("sms_status_datetime", Hibernate.TIMESTAMP);
		q.addScalar("mobile_no", Hibernate.STRING); 
		q.addScalar("message", Hibernate.STRING);
		q.addScalar("sent_by", Hibernate.STRING);
		q.addScalar("dept", Hibernate.STRING);
		q.addScalar("sms_scheduled_time", Hibernate.TIMESTAMP);
		q.addScalar("type", Hibernate.STRING);
		q.addScalar("remarks", Hibernate.STRING);
		q.addScalar("approval_by", Hibernate.STRING);
		q.addScalar("priority", Hibernate.INTEGER);
		q.addScalar("sms_status", Hibernate.STRING);
		q.addScalar("telco", Hibernate.INTEGER);
		q.addScalar("created_datetime", Hibernate.TIMESTAMP);
		q.addScalar("sms_scheduled", Hibernate.STRING);
		q.addScalar("approval_datetime", Hibernate.TIMESTAMP);
		return q;
	}
	
private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Status"));
		ouputStream.print (CommonUtils.println("Scheduled Date"));
		ouputStream.println();
	}
}
