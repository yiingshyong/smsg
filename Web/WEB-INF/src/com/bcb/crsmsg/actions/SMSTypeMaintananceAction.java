package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SMSTypeMaintenanceForm;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SMSTypeMaintananceAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {

		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		SMSTypeMaintenanceForm theForm = (SMSTypeMaintenanceForm) form;
		if(theForm.getTblRefId() != 0){
			theForm.setSmsType((TblRef) DaoBeanFactory.getHibernateTemplate().get(TblRef.class, theForm.getTblRefId()));
		}
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			theForm.getSmsType().setTblName(Constants.DB_CONSTANTS_TBLREF_TBLNAME_SMSMQ);
			theForm.getSmsType().setColName(Constants.DB_CONSTANTS_TBLREF_COLNAME);
			theForm.getSmsType().setStatus(Constants.FORM_FIELD_STATUS_ACTIVE);
			DaoBeanFactory.getTblRefDao().updateSmsType(theForm.getSmsType());
		}
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			DaoBeanFactory.getTblRefDao().deleteSmsType(theForm.getTblRefId());
		}
		
		theForm.setSmsTypes(DaoBeanFactory.getTblRefDao().getAllSMSTypes());
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
}
