package com.bcb.crsmsg.actions;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.util.Constants;
import com.bcb.common.util.UserSessionListener;


public class SSOServlet extends HttpServlet implements Servlet{
	
	
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(SSOServlet.class);

	public void init(ServletConfig arg0) throws ServletException {
		log.info("Initialising " + this.getClass().getName());
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		log.info("SSO Authencating ...");
		if(UserSessionListener.isSessionValid(req.getParameter(Constants.SSO_TOKEN_PARAM))){
			resp.getWriter().write(Constants.SSO_TOKEN_VALID);
			resp.getWriter().flush();
			resp.getWriter().close();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}

}
