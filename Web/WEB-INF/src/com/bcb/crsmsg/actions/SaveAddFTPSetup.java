package com.bcb.crsmsg.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.crsmsg.forms.FTPSetupForm;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveAddFTPSetup extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.FTP_SETUP) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			FTPSetupForm theForm = (FTPSetupForm) form;
			SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
			String fileDelimiter=((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getDefaultFileDelimiter();
			String tableFieldDelimiter=((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getTableListDelimiter();
			
			FtpSetup aSetup=new FtpSetup();
			aSetup.setCreatedBy(theLogonCredential.getUserId());
			aSetup.setCreatedDatetime(new Date());
			aSetup.setFileConversion(Integer.valueOf(theForm.getFileConversion()));
			aSetup.setFileDelimiter(fileDelimiter);

			String sourceDir=Constants.STR_EMPTY;
			String filename=Constants.STR_EMPTY;
			if(theForm.getFtpTargetFile().lastIndexOf(File.separator)>0){
				sourceDir=theForm.getFtpTargetFile().substring(0, theForm.getFtpTargetFile().lastIndexOf(File.separator));
				filename=theForm.getFtpTargetFile().substring(theForm.getFtpTargetFile().lastIndexOf(File.separator)+1);
			}else{
				filename=theForm.getFtpTargetFile();
			}
			
			aSetup.setFileName(filename);
			aSetup.setFtpSourceDir(sourceDir);
			
			String ftpDay=Constants.STR_EMPTY;
			for(int i=0;theForm.getFtpDay()!=null&&i<theForm.getFtpDay().length;i++){
				ftpDay+=theForm.getFtpDay()[i]+tableFieldDelimiter;
			}
			aSetup.setFtpDay(ftpDay);
			aSetup.setFtpDesc(theForm.getFtpDesc());
			aSetup.setFtpDestDir(Constants.STR_EMPTY);
			aSetup.setFtpMode(theForm.getFtpMode());
			aSetup.setFtpOwner(theForm.getFtpOwner());
			
			String ftpMonth=Constants.STR_EMPTY;
			for(int i=0;theForm.getFtpMonth()!=null&&i<theForm.getFtpMonth().length;i++){
				ftpMonth+=theForm.getFtpMonth()[i]+tableFieldDelimiter;
			}
			aSetup.setFtpMonth(ftpMonth);
			aSetup.setFtpDayOfMonth(theForm.getFtpDayOfMonth());
			aSetup.setFtpHour(theForm.getFtpHour());
			aSetup.setFtpHourFrom(theForm.getFtpHourFrom());
			aSetup.setFtpHourTo(theForm.getFtpHourTo());
			aSetup.setFtpName(theForm.getFtpName());
			aSetup.setFtpPassword(SHAUtil.encrypt(theForm.getFtpPassword()));
			aSetup.setFtpPort(theForm.getFtpPort());
			aSetup.setFtpServer(theForm.getFtpServer());
			
			aSetup.setFtpTargetDir(Constants.STR_EMPTY);
			aSetup.setFtpTime(theForm.getFtpTime());
			aSetup.setFtpTimeMode(theForm.getFtpTimeMode());
			aSetup.setFtpUserName(theForm.getFtpUserName());
			aSetup.setFtpRetryNo(theForm.getFtpRetryNo());
			aSetup.setFtpRetryPeriod(theForm.getFtpRetryPeriod());
			if(theForm.getMessageTemplate()!=null&theForm.getMessageTemplate().length()>0)
				aSetup.setMessageTemplate(Integer.valueOf(theForm.getMessageTemplate()));
			else
				aSetup.setMessageTemplate(null);
			aSetup.setNotificationSetupReceiver(theForm.getNotificationSetupReceiver());
			aSetup.setNotificationSetupSender(theForm.getNotificationSetupSender());
			aSetup.setNotificationSetupStatus(theForm.getNotificationSetupStatus());
			//aSetup.setSecurityKey(theForm.getSecurityKey());
			aSetup.setSecurityKeyFolder(Constants.DEFAULT_SFTP_KEY_FOLDER);
			
			FormFile ftpFormFile=theForm.getSecurityKey();
			String uploadingFileName= ftpFormFile.getFileName();
			String uploadPath=aSetup.getSecurityKeyFolder();
			if(!uploadingFileName.equals(Constants.STR_EMPTY)){
				File uploadFileObject = new File(uploadPath, uploadingFileName);
				if(!uploadFileObject.getParentFile().exists()){
					uploadFileObject.mkdirs();
				}
				FileOutputStream fileOutStream = new FileOutputStream(uploadFileObject);
				fileOutStream.write(ftpFormFile.getFileData());
				fileOutStream.flush();
				fileOutStream.close();
				
				aSetup.setSecurityKey(uploadFileObject.getCanonicalPath());
			}else{
				
			}
			
			aSetup.setSetupStatus(theForm.getSetupStatus());
			aSetup.setSmsAutoApproval(theForm.getSmsAutoApproval());
			aSetup.setSmsScheduled(theForm.getSmsScheduled());
			if(theForm.getSmsScheduledTime().length()>0)
				aSetup.setSmsScheduledTime(theFormatter.parse(theForm.getSmsScheduledTime()));
	
			DBUtilsCrsmsg.saveOrUpdate(aSetup, hibernate_session);
			
			SmsAuditTrail anAuditTrail = new SmsAuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_FTPSETUP_SAVE_ADD,aSetup.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
			DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_FTPSETUP_SAVE_ADD+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
