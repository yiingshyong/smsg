package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.IPForm;

import com.bcb.crsmsg.forms.SecurityControlForm;

import com.bcb.crsmsg.modal.HttpIpAddress;

import com.bcb.crsmsg.modal.SmsAuditTrail;

import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;


public final class SaveAddIPAddress extends Action {
	

	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		log4jUtil.info("SaveAddIPAddress execute method");
		System.out.println("inside saveaddipaddress execute method");
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SECURITY_CONTROL) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
		
		try {
			
			IPForm theForm = (IPForm) form;
			
				HttpIpAddress ipaddress = new HttpIpAddress();
				
				ipaddress.setIp(theForm.getIp());
				ipaddress.setDept(theForm.getDept());
				ipaddress.setCreatedBy(theLogonCredential.getUserId());
				ipaddress.setCreatedDatetime(new Date());
				ipaddress.setRemarks(theForm.getRemarks());
				ipaddress.setSystem(theForm.getSystem());
				/*ipaddress.setStatus(theForm.getSelected_status());*/
				
				
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				ActionMessages messages = new ActionMessages();
				if(DBUtilsCrsmsg.saveOrUpdate(ipaddress, hibernate_session)){
					messages.add(Constants.ERROR_IP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
					this.addErrors(request,messages);
					
					
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SECURITY_CONTROL_SAVE_ADD,"IP Allowed",Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
					if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
						
					}else{
						messages.add(Constants.ERROR_IP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_AUDIT_TRAIL, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}
					
					
				}else{
					messages.add(Constants.ERROR_IP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
					this.addErrors(request,messages);
					
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SECURITY_CONTROL_SAVE_ADD,"IP Allowed" ,Constants.STATUS_FAIL,theLogonCredential.getBranchCode());
					if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
					}else{
						messages.add(Constants.ERROR_IP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_AUDIT_TRAIL, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}
				}
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E){
			log4jUtil.error(Constants.ERROR_IP_SAVE_ADD+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_IP_SAVE_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}


}
