package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.TemplateMessageForm;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveAddTemplateMessage extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.TEMPLATE_MSG) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			TemplateMessageForm theForm = (TemplateMessageForm) form;
			MsgTemplate aMsg=new MsgTemplate();
			aMsg.setCreatedBy(theLogonCredential.getUserId());
			aMsg.setCreatedDatetime(new Date());
			aMsg.setDepartment(theForm.getSelected_department());
			aMsg.setDynamicFieldName(theForm.getDynamicFieldName().replaceAll(Constants.SPECIAL_CHAR_COMMA, ((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getTableListDelimiter()));
			aMsg.setDynamicFieldNo(theForm.getDynamicFieldNo());
			aMsg.setStatus(theForm.getSelected_status());
			aMsg.setTemplateCode(theForm.getMessageCode());
			aMsg.setTemplateContent(theForm.getMessageText());
			aMsg.setType(theForm.getSelected_type());
			aMsg.setMsgFormatType(theForm.getSelected_charset());
			
			DBUtilsCrsmsg.saveOrUpdate(aMsg, hibernate_session);
			
			SmsAuditTrail anAuditTrail = new SmsAuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_TEMPLATEMESSAGE_SAVE_ADD,aMsg.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
			DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_TEMPLATEMESSAGE_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_TEMPLATEMESSAGE_SAVE_ADD+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_TEMPLATEMESSAGE_SAVE_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
