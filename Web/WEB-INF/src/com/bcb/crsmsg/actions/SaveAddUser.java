

package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.crsmsg.forms.UserForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.UserRights;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveAddUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.USER_SEARCH) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			String logonuser=theLogonCredential.getUserId();
			UserForm edituserform = (UserForm) form;
			User aUser=null;
			aUser= (User) DBUtilsCrsmsg.get(User.class, edituserform.getUserid(), hibernate_session);
			if ( aUser != null )
			{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_USER_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, aUser.getUserId()));
				this.addErrors(request,messages);
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_USER_SAVE_ADD,edituserform.getUserid().trim() + " Exist",Constants.STATUS_FAIL,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail);
			}else{
				User theUser = new User();
				theUser.setUserId(edituserform.getUserid());
				theUser.setBizPhone(edituserform.getBizphone());
				theUser.setBranchDept(1);//edituserform.getSelected_branch().intValue());
				theUser.setEmailId(edituserform.getEmailid());
				theUser.setExt(edituserform.getExt());
				theUser.setHPhone(edituserform.getHphone());
				
				//theUser.setRole(edituserform.getSelected_role().intValue());
				String selectedRoles =edituserform.getStrSelected_roles();
				
				if (selectedRoles !=null && selectedRoles.length()> 0){
					String[] rolesArray = selectedRoles.split(",");
					
					for(int i=0;i<rolesArray.length;i++){
						Integer roleValue=Integer.valueOf(rolesArray[i].toString());
						UserRoles aUserRole = new UserRoles();
						aUserRole.setUserId(edituserform.getUserid());
						aUserRole.setRoleId(roleValue);
						aUserRole.setStatus(Constants.STATUS_ACTIVE);
						aUserRole.setCreatedBy(logonuser);
						aUserRole.setCreatedDate(new Date());
						aUserRole.setCreatedTime(new Date());
						
						DBUtilsCrsmsg.saveOrUpdate(aUserRole, hibernate_session);
						
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_USER_SAVE_ADD,Integer.toString(aUserRole.getKeyCol()) , Constants.STATUS_SUCCESS ,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
					}
				}
				
				Integer[] selectedRights =edituserform.getSelected_rights();
				
				for(int i=0;selectedRights !=null&&i<selectedRights.length;i++){
					Integer rightsValue=selectedRights[i];
					UserRights aUserRights = new UserRights();
					aUserRights.setRightsId(rightsValue);
					aUserRights.setUser(theUser);
					
					DBUtilsCrsmsg.saveOrUpdate(aUserRights, hibernate_session);
					
					SmsAuditTrail anAuditTrail = new SmsAuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_USER_SAVE_ADD,Integer.toString(aUserRights.getId()) , Constants.STATUS_SUCCESS ,theLogonCredential.getBranchCode());
					DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				}
				
				theUser.setUserGroup(edituserform.getSelected_department().intValue());
				theUser.setUserName(edituserform.getUsername());
				theUser.setUserStatus(Constants.STATUS_ACTIVE);
				//theUser.setStaffId(edituserform.getStaffid());
				if ( edituserform.getPassword() != null && edituserform.getPassword().length() > 1 )
				{
					theUser.setPassword(SecurityManager.getInstance().encryptSHA2(edituserform.getPassword()));
				}
				
				/*List result=DBUtilsCrsmsg.DBHQLCommand("from SmsCfg where version=?", new Object[]{Integer.valueOf(Constants.APPSVERSION)}, hibernate_session);
				if(result.size()==1){
					SmsCfg theCfg=(SmsCfg)result.get(0);
					
					SmsQuota dQuota=new SmsQuota();
					
					if(theCfg.getSmsQuota().equals(Constants.STATUS_YES)){
						dQuota.setQuota(CommonUtils.NullChecker(theCfg.getSmsQuotaDefault(), Integer.class).toString().length()>0?Integer.valueOf(CommonUtils.NullChecker(theCfg.getSmsQuotaDefault(), Integer.class).toString()): Integer.valueOf(Constants.VALUE_0));
						dQuota.setTotalQuota(dQuota.getQuota());
					}else{
						dQuota.setQuota(Integer.valueOf(Constants.VALUE_0));
						dQuota.setTotalQuota(dQuota.getQuota());
					}
					dQuota.setRef(theUser.getUserId());
					DBUtilsCrsmsg.saveOrUpdate(dQuota, hibernate_session);
				}*/
				
				theUser.setCreatedBy(logonuser);
				theUser.setCreatedDate(new Date());
				theUser.setCreatedTime(new Date());
				//theUser.setCicsGroupId(edituserform.getCicsGroupSelect().toString());
				DBUtilsCrsmsg.saveOrUpdate(theUser, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_USER_SAVE_ADD,theUser.getUserId() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_USER_SAVE_ADD,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				DBUtilsCrsmsg.closeTransaction(hibernate_session);
			}
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_USER_SAVE_ADD+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_SAVE_ADD,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
