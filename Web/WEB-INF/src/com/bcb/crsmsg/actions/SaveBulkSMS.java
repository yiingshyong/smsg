package com.bcb.crsmsg.actions;

import java.io.File;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import org.hibernate.Session;


import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;

import com.bcb.crsmsg.forms.BulkSMSForm;
import com.bcb.crsmsg.modal.BulkSmsFile;
import com.bcb.crsmsg.modal.BulkSmsSetup;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.modal.SmsAuditTrail;

import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;

public final class SaveBulkSMS extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.BULK_SMS) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
		
		try {
			BulkSMSForm theForm = (BulkSMSForm) form;
			BulkSmsSetup theSetup = new BulkSmsSetup();
			theForm.setFileConversion("1");
			FormFile bulkFormFile=theForm.getBulkSMSFile();
			String uploadingFileName= bulkFormFile.getFileName();
			//adding time stamp to ensure uniqueness of file name
			uploadingFileName = new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()) + "." + uploadingFileName;
			String uploadPath=((FileConversion)DBUtilsCrsmsg.get(FileConversion.class, Integer.valueOf(theForm.getFileConversion()), hibernate_session)).getLocalFolder();
			if(!uploadingFileName.equals(Constants.STR_EMPTY)){
				File uploadFileObject = new File(uploadPath, uploadingFileName);
				if(!uploadFileObject.getParentFile().exists()){
					uploadFileObject.mkdirs();
				}
//				FileOutputStream fileOutStream = new FileOutputStream(uploadFileObject);
//				fileOutStream.write(bulkFormFile.getFileData());
//				fileOutStream.flush();
//				fileOutStream.close();
				
				theSetup.setBulkFile(uploadFileObject.getCanonicalPath());
			}else{
				
			}

			theSetup.setBulkFileMode(theForm.getBulkFileMode());
			theSetup.setFileName(uploadingFileName);
			theSetup.setBulkName(theForm.getBulkName());
			theSetup.setCreatedBy(theLogonCredential.getUserId());
			theSetup.setCreatedDatetime(new Date());
			theSetup.setDept(((Department)DBUtilsCrsmsg.get(Department.class, Integer.valueOf(theLogonCredential.getUserGroup()), hibernate_session)).getDeptName());
			theSetup.setFileConversion(Integer.valueOf(theForm.getFileConversion()));
			theSetup.setFileDelimiter(theForm.getFileDelimiter());
			theSetup.setMessage(theForm.getSmsContent());
			theSetup.setMsgFormatType(theForm.getSelected_charset());
			theSetup.setPriority(Integer.valueOf(theForm.getSmsPriority()));
			theSetup.setSentBy(theLogonCredential.getUserId());
			theSetup.setSetupStatus(Constants.STATUS_OPEN);
			theSetup.setSmsScheduled(theForm.getScheduledSMS());
			
			if(theForm.getScheduledSMSDateTime().length()>0)
				theSetup.setSmsScheduledTime(theFormat.parse(theForm.getScheduledSMSDateTime()));
			
			theSetup.setMsgVerification(theForm.getVerificationRequired());
			
			if(theForm.getVerificationRequired().equals(Constants.STATUS_YES)){
				theSetup.setSmsStatus(Constants.SMS_STATUS_7);
			}else{
				if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.SMS_APPROVAL))){
					theSetup.setSmsStatus(Constants.SMS_STATUS_3);
				}else if (CommonUtils.NullChecker(theForm.getScheduledSMS(), String.class).toString().equals(Constants.STATUS_YES)&&CommonUtils.NullChecker(theForm.getScheduledSMSDateTime(), String.class).toString().length()>0){
					theSetup.setSmsStatus(Constants.SMS_STATUS_6);
					theSetup.setApprovalBy("automated");
					theSetup.setApprovalDatetime(new Date());
				}else{
					theSetup.setSmsStatus(Constants.SMS_STATUS_4);
					theSetup.setApprovalBy("automated");
					theSetup.setApprovalDatetime(new Date());
				}
			}
			theSetup.setSmsStatusBy(theLogonCredential.getUserId());
			theSetup.setSmsStatusDatetime(new Date());
			theSetup.setTelco(0);
			theSetup.setType(Constants.SMS_SOURCE_TYPE_3);
			DBUtilsCrsmsg.saveOrUpdate(theSetup, hibernate_session);

//				String path = null;
//				path = uploadPath +"\\"+ uploadingFileName;
//				File optfile = new File(path);
//		        FileInputStream content = new FileInputStream(optfile);
//		        
			
			if(!uploadingFileName.equals(Constants.STR_EMPTY)){
				
				BulkSmsFile bulkSmsFile =new BulkSmsFile();
				bulkSmsFile.setBulkId(theSetup.getId());
				bulkSmsFile.setContent(bulkFormFile.getFileData());
				bulkSmsFile.setCreatedBy(theLogonCredential.getUserId());
				bulkSmsFile.setCreatedDatetime(new Date());
				bulkSmsFile.setBulkFileStatus(Constants.SMS_STATUS_4);
				bulkSmsFile.setFileName(uploadingFileName);
				bulkSmsFile.setUploadPath(uploadPath);
				DBUtilsCrsmsg.saveOrUpdate(bulkSmsFile, hibernate_session);

			}else{
				
			}

			
			//saveTheSetup(bulkFormFile.getFileData(),theSetup.getId(),uploadingFileName,theLogonCredential.getUserId());
			
			SmsAuditTrail anAuditTrail = new SmsAuditTrail();
			anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SMS_BULK_SAVE,theSetup.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
			DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);

			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMS_BULK_SAVE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
			this.addErrors(request,messages);

			DBUtilsCrsmsg.closeTransaction(hibernate_session);
			

		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_SMS_BULK_SAVE+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMS_BULK_SAVE,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
