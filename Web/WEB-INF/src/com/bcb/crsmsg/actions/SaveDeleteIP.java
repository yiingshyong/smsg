package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.IPForm;

import com.bcb.crsmsg.forms.SecurityControlForm;
import com.bcb.crsmsg.modal.HttpIpAddress;

import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;

public final class SaveDeleteIP extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		System.out.println("inside savedeleteip execute method");
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.KEYWORD_MGMT) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
				
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
		
		try {
			
			SecurityControlForm theForm = (SecurityControlForm) form;
			

			String templateId=Constants.STR_EMPTY;
			
			for(int i=0;theForm!=null&&theForm.getCheckedRecords()!=null&&i<theForm.getCheckedRecords().length;i++){
				templateId=theForm.getCheckedRecords()[i];
				System.out.println("templateId=="+templateId);
				HttpIpAddress ipaddress = (HttpIpAddress) DBUtilsCrsmsg.get(HttpIpAddress.class, Integer.valueOf(templateId), hibernate_session);
				DBUtilsCrsmsg.delete(ipaddress, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),Constants.APPSNAME,Constants.ACTION_TEMPLATEMESSAGE_DELETE,"IP Allowed",Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
			}
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_IPADDRESS_DELETE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_DELETED));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ERROR_IPADDRESS_DELETE+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_IPADDRESS_DELETE,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
				
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}

}
