

package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.DBUtils;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.UserSearchForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.UserRights;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveDeleteUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.USER_SEARCH) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			UserSearchForm usersearchform = (UserSearchForm) form;

			String userId=Constants.STR_EMPTY;
			for(int i=0;usersearchform!=null&&usersearchform.getCheckedRecords()!=null&&i<usersearchform.getCheckedRecords().length;i++){
				userId=usersearchform.getCheckedRecords()[i];
				User aUser=(User)DBUtilsCrsmsg.get(User.class, userId, hibernate_session);
				DBUtilsCrsmsg.delete(aUser, hibernate_session);
				
				List userRoles=DBUtils.DBHQLCommand("from UserRoles where userId=?", new Object[]{aUser.getUserId()});
				hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				for(int j=0;userRoles!=null&&j<userRoles.size();j++){
					UserRoles aUserRole=(UserRoles)userRoles.get(j);
					DBUtils.delete(aUserRole, hibernate_session);
				}
				
				List userRights=DBUtils.DBHQLCommand("from UserRights where user.userId=?", new Object[]{aUser.getUserId()});
				hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
				for(int j=0;userRights!=null&&j<userRights.size();j++){
					UserRights aUserRight=(UserRights)userRights.get(j);
					DBUtils.delete(aUserRight, hibernate_session);
				}
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),Constants.APPSNAME,Constants.ACTION_USER_DELETE,aUser.getUserId() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
			}
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_DELETE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_DELETED));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_USER_DELETE+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_DELETE,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
