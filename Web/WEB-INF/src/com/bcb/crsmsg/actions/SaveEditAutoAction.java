
package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.AutoActionForm;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class SaveEditAutoAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
		}else{				
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try{			
				AutoActionForm autoActionForm = (AutoActionForm) form;

				AutoAction autoModal = (AutoAction)DBUtilsCrsmsg.get(AutoAction.class, autoActionForm.getId(),hibernate_session);
				
				if(autoModal!=null){
					autoModal.setDescription(autoActionForm.getDescription());
					autoModal.setActionClassName(autoActionForm.getActionClassName());
					autoModal.setActionSequence(autoActionForm.getActionSequence());
					autoModal.setDefaultAction(autoActionForm.getDefaultAction());
					autoModal.setServerIP(autoActionForm.getServerIP());
					autoModal.setServerPort(Integer.valueOf(autoActionForm.getServerPort()));
					autoModal.setServerResponseTimeout(Integer.valueOf(autoActionForm.getServerResponseTimeout()));
					autoModal.setMaxFailureAttempt(Integer.valueOf(autoActionForm.getMaxFailureAttempt()));
					autoModal.setSvcWindow(autoActionForm.getSvcWindow());
					autoModal.setFailureCode(autoActionForm.getFailureCode());
					autoModal.setSuccessCode(autoActionForm.getSuccessCode());
					autoModal.setUnavailabilitySms(autoActionForm.getUnavailabilitySms());
					autoModal.setParameterKey(autoActionForm.getParameterKey());

					autoModal.setModifiedBy(theLogonCredential.getUserId());
					autoModal.setModifiedDatetime(new Date());

				
					if(DBUtilsCrsmsg.saveOrUpdate(autoModal, hibernate_session)){
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_AUTOACTION_SAVE_EDIT,autoModal.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
						if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_AUTOACTION_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_AUTOACTION_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}
					}else{
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_AUTOACTION_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}
					
				}
				
				DBUtilsCrsmsg.closeTransaction(hibernate_session);				
				
			}catch (Exception E){
				E.printStackTrace();
				log4jUtil.error(Constants.ACTION_AUTOACTION_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
				DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_TELCO_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
