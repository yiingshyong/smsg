package com.bcb.crsmsg.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.crsmsg.forms.FTPSetupForm;
import com.bcb.crsmsg.modal.FtpSetup;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditFTPSetup extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.FTP_SETUP) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			FTPSetupForm theForm = (FTPSetupForm) form;
			FtpSetup theSetup=(FtpSetup)DBUtilsCrsmsg.get(FtpSetup.class, Integer.valueOf(theForm.getFtpId()), hibernate_session);
			if(theSetup!=null){
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				String fileDelimiter=((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getDefaultFileDelimiter();
				String tableFieldDelimiter=((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getTableListDelimiter();
				
				theSetup.setModifiedBy(theLogonCredential.getUserId());
				theSetup.setModifiedDatetime(new Date());
				theSetup.setFileConversion(Integer.valueOf(theForm.getFileConversion()));
				theSetup.setFileDelimiter(fileDelimiter);
				
				String sourceDir=Constants.STR_EMPTY;
				String filename=Constants.STR_EMPTY;
				if(theForm.getFtpTargetFile().lastIndexOf(File.separator)>0){
					sourceDir=theForm.getFtpTargetFile().substring(0, theForm.getFtpTargetFile().lastIndexOf(File.separator));
					filename=theForm.getFtpTargetFile().substring(theForm.getFtpTargetFile().lastIndexOf(File.separator)+1);
				}else{
					filename=theForm.getFtpTargetFile();
				}
				
				theSetup.setFileName(filename);
				theSetup.setFtpSourceDir(sourceDir);
				
				String ftpDay=Constants.STR_EMPTY;
				for(int i=0;theForm.getFtpDay()!=null&&i<theForm.getFtpDay().length;i++){
					ftpDay+=theForm.getFtpDay()[i]+tableFieldDelimiter;
				}
				theSetup.setFtpDay(ftpDay);
				
				theSetup.setFtpDestDir(Constants.STR_EMPTY);
				theSetup.setFtpMode(theForm.getFtpMode());
				theSetup.setFtpOwner(theForm.getFtpOwner());
				theSetup.setFtpDesc(theForm.getFtpDesc());
				String ftpMonth=Constants.STR_EMPTY;
				for(int i=0;theForm.getFtpMonth()!=null&&i<theForm.getFtpMonth().length;i++){
					ftpMonth+=theForm.getFtpMonth()[i]+tableFieldDelimiter;
				}
				theSetup.setFtpMonth(ftpMonth);
				theSetup.setFtpDayOfMonth(theForm.getFtpDayOfMonth());
				theSetup.setFtpHour(theForm.getFtpHour());
				theSetup.setFtpHourFrom(theForm.getFtpHourFrom());
				theSetup.setFtpHourTo(theForm.getFtpHourTo());
				theSetup.setFtpName(theForm.getFtpName());
				
				if(theForm.getFtpPassword()!=null&&theForm.getFtpPassword().length()>0)
					theSetup.setFtpPassword(SHAUtil.encrypt(theForm.getFtpPassword()));
				
				theSetup.setFtpPort(theForm.getFtpPort());
				theSetup.setFtpServer(theForm.getFtpServer());
				
				theSetup.setFtpTargetDir(Constants.STR_EMPTY);
				theSetup.setFtpTime(theForm.getFtpTime());
				theSetup.setFtpTimeMode(theForm.getFtpTimeMode());
				theSetup.setFtpUserName(theForm.getFtpUserName());
				theSetup.setFtpRetryNo(theForm.getFtpRetryNo());
				theSetup.setFtpRetryPeriod(theForm.getFtpRetryPeriod());
				
				if(theForm.getMessageTemplate()!=null&theForm.getMessageTemplate().length()>0)
					theSetup.setMessageTemplate(Integer.valueOf(theForm.getMessageTemplate()));
				else
					theSetup.setMessageTemplate(null);
				theSetup.setNotificationSetupReceiver(theForm.getNotificationSetupReceiver());
				theSetup.setNotificationSetupSender(theForm.getNotificationSetupSender());
				theSetup.setNotificationSetupStatus(theForm.getNotificationSetupStatus());
				//theSetup.setSecurityKey(theForm.getSecurityKey());
				FormFile ftpFormFile=theForm.getSecurityKey();
				String uploadingFileName= ftpFormFile.getFileName();
				String uploadPath=theSetup.getSecurityKeyFolder();
				if(!uploadingFileName.equals(Constants.STR_EMPTY)){
					File uploadFileObject = new File(uploadPath, uploadingFileName);
					if(uploadFileObject!=null&&!uploadFileObject.getParentFile().exists()){
						uploadFileObject.mkdirs();
					}
					FileOutputStream fileOutStream = new FileOutputStream(uploadFileObject);
					fileOutStream.write(ftpFormFile.getFileData());
					fileOutStream.flush();
					fileOutStream.close();
					
					theSetup.setSecurityKey(uploadFileObject.getCanonicalPath());
				}else{
					
				}
				theSetup.setSetupStatus(theForm.getSetupStatus());
				theSetup.setSmsAutoApproval(theForm.getSmsAutoApproval());
				theSetup.setSmsScheduled(theForm.getSmsScheduled());
				if(theForm.getSmsScheduledTime().length()>0)
					theSetup.setSmsScheduledTime(theFormatter.parse(theForm.getSmsScheduledTime()));
		
				DBUtilsCrsmsg.saveOrUpdate(theSetup, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_FTPSETUP_SAVE_EDIT,theSetup.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_FTPSETUP_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_FTPSETUP_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_FTPSETUP_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_FTPSETUP_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
