package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.KeywordForm;
import com.bcb.crsmsg.modal.AutoAction;
import com.bcb.crsmsg.modal.Keyword;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.sgwcore.util.DaoBeanFactory;
public final class SaveEditKeyword extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.KEYWORD_MGMT) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR);
		
		try {
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
			
			if(theCfg==null){
			}else{
				KeywordForm theForm = (KeywordForm) form;
				Keyword theKeyword=(Keyword)DBUtilsCrsmsg.get(Keyword.class, Integer.valueOf(theForm.getKeywordId()), hibernate_session);
				if(theKeyword!=null){
					theKeyword.setAutoExpired(theForm.getAutoExpired());
					theKeyword.setAutoReply(theForm.getAutoReply());
					theKeyword.setAutoReplyMsg(theForm.getAutoReplyMessage());
					theKeyword.setModifiedBy(theLogonCredential.getUserId());
					theKeyword.setModifiedDatetime(new Date());
					if(theForm.getExpiryDate().length()>0)
						theKeyword.setExpiredDate(theFormat.parse(theForm.getExpiryDate()));
					theKeyword.setKeywordName(theForm.getKeyword());
					theKeyword.setOwnership(theForm.getSelected_ownership());
					theKeyword.setRedirectEmail(theForm.getRedirectEmail().replaceAll(Constants.SPECIAL_CHAR_NEWLINE, theCfg.getTableListDelimiter()));
					theKeyword.setRedirectMobileNo(theForm.getRedirectMobileNo().replaceAll(Constants.SPECIAL_CHAR_NEWLINE, theCfg.getTableListDelimiter()));
					//20190127 - encrypt url start				
					//aKeyword.setRedirectUrl(theForm.getRedirectURL());
					String keywordEnc = theForm.getRedirectURL();
					if(theForm.getSelected_encryptRedirectUrl().equals(Constants.FORM_FIELD_STATUS_YES))
							keywordEnc = SecurityManager.getInstance().encrypt(theForm.getRedirectURL());					
					theKeyword.setRedirectUrl(keywordEnc);							
					// end
					theKeyword.setRedirectUrlParam(theForm.getRedirectURLParam().replaceAll(Constants.SPECIAL_CHAR_COMMA, theCfg.getTableListDelimiter()));
					theKeyword.setStatus(theForm.getSelected_status());
					theKeyword.setCharSet(theForm.getSelected_charset());
					
					theKeyword.setMaxRedirect_attempts(theForm.getMaxRedirect_attempts());
					theKeyword.setRedirectInterval(theForm.getRedirectInterval());
					
					AutoAction autoAction = DaoBeanFactory.getAutoActionDao().getNonDefaultAutoActionByKeyword(theKeyword.getId());
					if(autoAction == null){
						autoAction = new AutoAction();
						autoAction.setId(0);
					}
					
					if(autoAction.getId() == Integer.parseInt(theForm.getSelectedAutoAction())){
						//nothing
					}else{
						if(autoAction.getId() != 0){
							AutoAction actionMethod = DaoBeanFactory.getAutoActionDao().getKeywordActionMethod(theKeyword.getId());
							if(actionMethod != null){
								DaoBeanFactory.getHibernateTemplate().delete(actionMethod);
							}
						}
						if(!theForm.getSelectedAutoAction().equals("0")){
							AutoAction selectedAutoAction = (AutoAction) DaoBeanFactory.getHibernateTemplate().get(AutoAction.class, Integer.parseInt(theForm.getSelectedAutoAction()));
							AutoAction anAction=new AutoAction();
							BeanUtils.copyProperties(anAction, selectedAutoAction);
							anAction.setId(null);
							anAction.setKeyword(theKeyword.getId());
							anAction.setDefaultAction(null);
							DBUtilsCrsmsg.saveOrUpdate(anAction, hibernate_session);													
						}
					}
					
					SmsAuditTrail anAuditTrail = new SmsAuditTrail();
					ActionMessages messages = new ActionMessages();
					if(DBUtilsCrsmsg.saveOrUpdate(theKeyword, hibernate_session)){
						messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
						
						List defaultActionList=DBUtilsCrsmsg.DBHQLCommand("from AutoAction where defaultAction=? order by actionSequence", new Object[]{Constants.STATUS_YES}, hibernate_session);
						List actionList=DBUtilsCrsmsg.DBHQLCommand("select actionClassName from AutoAction where keyword=? order by actionSequence", new Object[]{theKeyword.getId()}, hibernate_session);
						for(int i=0;i<defaultActionList.size();i++){
							AutoAction theAction=(AutoAction)defaultActionList.get(i);
							if(!actionList.contains(theAction.getActionClassName())){
								AutoAction anAction=new AutoAction();
								BeanUtils.copyProperties(anAction, theAction);
								anAction.setId(null);
								anAction.setKeyword(theKeyword.getId());
								anAction.setDefaultAction(null);
								DBUtilsCrsmsg.saveOrUpdate(anAction, hibernate_session);
							}
						}
						
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_KEYWORD_SAVE_EDIT,theKeyword.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
						if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
						}else{
							messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_AUDIT_TRAIL, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}
					}else{
						messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
						
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_KEYWORD_SAVE_EDIT,theKeyword.getId().toString() ,Constants.STATUS_FAIL,theLogonCredential.getBranchCode());
						if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
						}else{
							messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_AUDIT_TRAIL, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}
					}
				}else{
					ActionMessages messages = new ActionMessages();
					messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
					this.addErrors(request,messages);
					
					return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
				}
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_KEYWORD_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_KEYWORD_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
