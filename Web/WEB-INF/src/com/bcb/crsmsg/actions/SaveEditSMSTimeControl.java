package com.bcb.crsmsg.actions;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.SystemCfgForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsTimeControl;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditSMSTimeControl extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,com.bcb.common.util.Constants.DEPTMGMT_PERM) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();

		try {
			SystemCfgForm theForm = (SystemCfgForm) form;
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);

			if(theCfg!=null){
				List controlList=DBUtilsCrsmsg.DBHQLCommand("from SmsTimeControl where dept=?", new Object[]{Integer.valueOf(CommonUtils.NullChecker(theForm.getDept(), Integer.class).toString())}, hibernate_session);
				
				if(controlList.size()<=1){// Found time control cfg or new time control cfg
					SmsTimeControl theControl=controlList.size()==0?new SmsTimeControl():(SmsTimeControl)controlList.get(0);
					theControl.setDept(Integer.valueOf(CommonUtils.NullChecker(theForm.getDept(), Integer.class).toString()));
					theControl.setSmsTimeControlBatch(CommonUtils.NullChecker(theForm.getSmsTimeControlBatch(), String.class).toString());
					theControl.setSmsTimeControlWeb(CommonUtils.NullChecker(theForm.getSmsTimeControlWeb(), String.class).toString());
					System.out.println("form selected: " + theForm.getAutoPatchToUnsent());
					theControl.setAutoPatchToUnsent(StringUtils.isNotEmpty(theForm.getAutoPatchToUnsent()) && theForm.getAutoPatchToUnsent().equals("Y"));
					String smsTimeControlDay=Constants.STR_EMPTY;
					for(int i=0;theForm.getSmsTimeControlDay()!=null&&i<theForm.getSmsTimeControlDay().length;i++){
						smsTimeControlDay+=theForm.getSmsTimeControlDay()[i]+CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString();
					}
					theControl.setSmsTimeControlDay(smsTimeControlDay);
					
					String smsTimeControlTimeFrom=Constants.STR_EMPTY;
					for(int i=0;theForm.getSmsTimeControlTimeFromHour()!=null&&i<theForm.getSmsTimeControlTimeFromHour().length&&i<theForm.getSmsTimeControlTimeFromMin().length;i++){
						smsTimeControlTimeFrom+=theForm.getSmsTimeControlTimeFromHour()[i]+theForm.getSmsTimeControlTimeFromMin()[i]+CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString();
					}
					theControl.setSmsTimeControlTimeFrom(smsTimeControlTimeFrom);
					
					String smsTimeControlTimeTo=Constants.STR_EMPTY;
					for(int i=0;theForm.getSmsTimeControlTimeToHour()!=null&&i<theForm.getSmsTimeControlTimeToHour().length&&i<theForm.getSmsTimeControlTimeToMin().length;i++){
						smsTimeControlTimeTo+=theForm.getSmsTimeControlTimeToHour()[i]+theForm.getSmsTimeControlTimeToMin()[i]+CommonUtils.NullChecker(theCfg.getTableListDelimiter(), String.class).toString();
					}
					theControl.setSmsTimeControlTimeTo(smsTimeControlTimeTo);
					if(controlList.size()==0){
						theControl.setCreatedBy(theLogonCredential.getUserId());
						theControl.setCreatedDatetime(new Date());
					}else{
						theControl.setModifiedBy(theLogonCredential.getUserId());
						theControl.setModifiedDatetime(new Date());
					}
					DBUtilsCrsmsg.saveOrUpdate(theControl, hibernate_session);
				}else{// more then one cfg found
				}
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SMSTIMECONTROL_SAVE_EDIT,theCfg.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SMSTIMECONTROL_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SMSTIMECONTROL_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_SMSTIMECONTROL_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMSTIMECONTROL_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
