package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.SendSMSForm;
import com.bcb.crsmsg.modal.ADSSynch;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsHistory;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditSms extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));	
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
		
		try {
			SendSMSForm theForm = (SendSMSForm) form;
			
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			if(theCfg==null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				Sms aSMS=(Sms)DBUtilsCrsmsg.get(Sms.class, Integer.valueOf(CommonUtils.NullChecker(theForm.getSmsId(), Integer.class).toString()));
				
				if(aSMS!=null){
					if(!aSMS.getSmsStatus().equals(Constants.SMS_STATUS_5)){
						Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
						try{
							int charPerSms=CommonUtils.calculateCharPerSms(theForm.getSelected_charset(), theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
							ArrayList<String> msgArray=CommonUtils.strToArray(theForm.getSmsContent(), charPerSms);
							String status=Constants.STR_EMPTY;
							
								aSMS.setMobileNo(theForm.getMobileNo());
								aSMS.setSmsScheduled(theForm.getScheduledSMS());
								aSMS.setMsgFormatType(theForm.getSelected_charset());
								aSMS.setMessage(theForm.getSmsContent());
								aSMS.setTotalSms(msgArray.size());
								aSMS.setPriority(Integer.valueOf(theForm.getSmsPriority()));
								if(theForm.getScheduledSMSDateTime().length()>0)
									aSMS.setSmsScheduledTime(theFormat.parse(theForm.getScheduledSMSDateTime()));
								aSMS.setModifiedBy(theLogonCredential.getUserId());
								aSMS.setModifiedDatetime(new Date());
								
								if(DBUtilsCrsmsg.saveOrUpdate(aSMS, hibernate_session)){
									SmsHistory aSmsHistory=new SmsHistory();
									BeanUtils.copyProperties(aSmsHistory, aSMS);
									if(DBUtilsCrsmsg.saveOrUpdate(aSmsHistory, hibernate_session)){
										SmsAuditTrail anAuditTrail = new SmsAuditTrail();
										anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_MSG_SAVE_EDIT,aSMS.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
										if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
											if(theForm.getSendBtn()!=null){// perform send action when click on resend button
												aSMS.setSmsStatus(Constants.SMS_STATUS_4);
												aSMS.setSmsStatusBy(theLogonCredential.getUserId());
												aSMS.setSmsStatusDatetime(new Date());
												
												SmsQueue aSmsQueue=new SmsQueue();
												BeanUtils.copyProperties(aSmsQueue, aSMS);
												aSmsQueue.setId(null);// allow auto_increment generate new id
												
												if(DBUtilsCrsmsg.saveOrUpdate(aSmsQueue, hibernate_session)){// save as sms queue
													ADSSynch aSynch=(ADSSynch)DBUtilsCrsmsg.get(ADSSynch.class, aSMS.getId(), hibernate_session);
													if(aSynch!=null){// contain in ads synch, update id
														ADSSynch aNewSynch=new ADSSynch();
														BeanUtilsBean utilBean = new BeanUtilsBean();
														utilBean.copyProperties(aNewSynch, aSynch);
														aNewSynch.setSmsId(aSmsQueue.getId());
														if(DBUtilsCrsmsg.saveOrUpdate(aNewSynch, hibernate_session)){
															DBUtilsCrsmsg.delete(aSynch, hibernate_session);
														}
													}
													if(DBUtilsCrsmsg.delete(aSMS, hibernate_session)){// remove from sms
														anAuditTrail = new SmsAuditTrail();
														anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_MSG_SAVE_EDIT,aSMS.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
														if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
															status=Constants.STATUS_SUCCESS;
															theForm.reset(mapping, request);
														}else{
															status=Constants.STATUS_FAIL;
														}
													}else{
														status=Constants.STATUS_FAIL;
													}
												}else{
													status=Constants.STATUS_FAIL;
												}
											}else{
												status=Constants.STATUS_SUCCESS;
											}
										}else{
											status=Constants.STATUS_FAIL;
										}
									}else{
										status=Constants.STATUS_FAIL;
									}
								}else{
									status=Constants.STATUS_FAIL;
								}
							
							if(status.equals(Constants.STATUS_SUCCESS)){
								DBUtilsCrsmsg.closeTransaction(hibernate_session);
								ActionMessages messages = new ActionMessages();
								messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
								this.addErrors(request,messages);
							}else{
								DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
								ActionMessages messages = new ActionMessages();
								messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
								this.addErrors(request,messages);
							}
						}catch(Exception ex){
							DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}finally{
							DBUtilsCrsmsg.closeTransaction(hibernate_session);
						}
					}else{// SMS is ready to send, not allowed for amendment
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_DENIED));
						this.addErrors(request,messages);
					}
				}else{// No SMS found
					ActionMessages messages = new ActionMessages();
					messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
					this.addErrors(request,messages);
				}
			}
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_MSG_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
