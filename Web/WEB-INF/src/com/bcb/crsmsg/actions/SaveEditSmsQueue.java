package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.SendSMSForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsHistory;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditSmsQueue extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));	
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
		
		try {
			SendSMSForm theForm = (SendSMSForm) form;
			
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
			if(theCfg==null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				SmsQueue aSMS=(SmsQueue)DBUtilsCrsmsg.get(SmsQueue.class, Integer.valueOf(CommonUtils.NullChecker(theForm.getSmsId(), Integer.class).toString()), hibernate_session);
				
				if(aSMS!=null){
					if(!aSMS.getSmsStatus().equals(Constants.SMS_STATUS_5)){
						int charPerSms=CommonUtils.calculateCharPerSms(theForm.getSelected_charset(), theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
						ArrayList<String> msgArray=CommonUtils.strToArray(theForm.getSmsContent(), charPerSms);
						
//						ArrayList<String> lastSmsIdList=new ArrayList<String>();
//						String lastSmsId=Constants.STR_EMPTY;
//						for(int j=0;j<msgArray.size();j++){
							aSMS.setMobileNo(theForm.getMobileNo());
							aSMS.setSmsScheduled(theForm.getScheduledSMS());
							aSMS.setMsgFormatType(theForm.getSelected_charset());
							aSMS.setMessage(theForm.getSmsContent());
							aSMS.setTotalSms(msgArray.size());
							aSMS.setPriority(Integer.valueOf(theForm.getSmsPriority()));
							if(theForm.getScheduledSMSDateTime().length()>0)
								aSMS.setSmsScheduledTime(theFormat.parse(theForm.getScheduledSMSDateTime()));
							aSMS.setModifiedBy(theLogonCredential.getUserId());
							aSMS.setModifiedDatetime(new Date());
							DBUtilsCrsmsg.saveOrUpdate(aSMS, hibernate_session);
							
//							lastSmsIdList.add(CommonUtils.NullChecker(aSMS.getId(), String.class).toString());
//							
//							aSMS.setConcatenateSms(theCfg.getConcatenateSms().equals(Constants.STATUS_YES)?(msgArray.size()==1?Constants.STR_EMPTY:j==0?CommonUtils.NullChecker(aSMS.getId(), String.class).toString():j!=msgArray.size()-1?lastSmsId:CommonUtils.arrayToString(lastSmsIdList, theCfg.getTableListDelimiter())):Constants.STR_EMPTY);
//							DBUtilsCrsmsg.saveOrUpdate(aSMS, hibernate_session);
							
							SmsHistory aSmsHistory=new SmsHistory();
							BeanUtils.copyProperties(aSmsHistory, aSMS);
							DBUtilsCrsmsg.saveOrUpdate(aSmsHistory, hibernate_session);
							
							SmsAuditTrail anAuditTrail = new SmsAuditTrail();
							anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_MSG_SAVE_EDIT,aSMS.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
							DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							
//							lastSmsId=CommonUtils.NullChecker(aSMS.getId(), String.class).toString();
//						}
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}else{// SMS is ready to send, not allowed for amendment
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_DENIED));
						this.addErrors(request,messages);
					}
				}else{// No SMS found
					ActionMessages messages = new ActionMessages();
					messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND));
					this.addErrors(request,messages);
				}
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E){
			log4jUtil.error(Constants.ACTION_MSG_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_MSG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
