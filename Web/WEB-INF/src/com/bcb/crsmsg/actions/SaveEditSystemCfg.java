package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.SystemCfgForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditSystemCfg extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();

		try {
			SystemCfgForm theForm = (SystemCfgForm) form;
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(theForm.getCfgId()), hibernate_session);

			if(theCfg!=null){
				theCfg.setAutoSmsFailure(CommonUtils.NullChecker(theForm.getAutoSmsFailure(), String.class).toString());
				theCfg.setAutoSmsResend(CommonUtils.NullChecker(theForm.getAutoSmsResend(), String.class).toString());
				theCfg.setAutoSmsResendInterval(CommonUtils.NullChecker(theForm.getAutoSmsResendInterval(), String.class).toString());
				theCfg.setAutoSmsResendLimit(CommonUtils.NullChecker(theForm.getAutoSmsResendLimit(), String.class).toString());
				theCfg.setBulkSmsThreshold(CommonUtils.NullChecker(theForm.getBulkSmsThreshold(), String.class).toString());
				theCfg.setBulkSmsThresholdNotification(CommonUtils.arrayToString(theForm.getBulkSmsThresholdNotification(), theCfg.getTableListDelimiter()));
				theCfg.setBulkSmsThresholdNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getBulkSmsThresholdNotificationEmail(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setBulkSmsThresholdNotificationEmailSender(CommonUtils.NullChecker(theForm.getBulkSmsThresholdNotificationEmailSender(), String.class).toString());
				theCfg.setBulkSmsThresholdNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getBulkSmsThresholdNotificationSms(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setBulkSmsThresholdNotificationSmsSender(CommonUtils.NullChecker(theForm.getBulkSmsThresholdNotificationSmsSender(), String.class).toString());
				theCfg.setQueueSmsThreshold(CommonUtils.NullChecker(theForm.getQueueSmsThreshold(), String.class).toString());
				theCfg.setQueueSmsThresholdNotification(CommonUtils.arrayToString(theForm.getQueueSmsThresholdNotification(), theCfg.getTableListDelimiter()));
				theCfg.setTotalQueuedSizeAlertCheckPeriod(CommonUtils.NullChecker(theForm.getTotalQueuedSizeAlertCheckPeriod(), String.class).toString() == Constants.STR_EMPTY? 0 : Integer.parseInt(theForm.getTotalQueuedSizeAlertCheckPeriod()));
				theCfg.setTotalQueuedSizeAlertThreshold(CommonUtils.NullChecker(theForm.getTotalQueuedSizeAlertThreshold(), String.class).toString() == Constants.STR_EMPTY? 0 : Integer.parseInt(theForm.getTotalQueuedSizeAlertThreshold()));
				theCfg.setQueueSmsThresholdNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getQueueSmsThresholdNotificationEmail(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setQueueSmsThresholdNotificationEmailSender(CommonUtils.NullChecker(theForm.getQueueSmsThresholdNotificationEmailSender(), String.class).toString());
				theCfg.setQueueSmsThresholdNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getQueueSmsThresholdNotificationSms(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setQueueSmsThresholdNotificationSmsSender(CommonUtils.NullChecker(theForm.getQueueSmsThresholdNotificationSmsSender(), String.class).toString());
				theCfg.setQueueSmsThresholdPeriod(CommonUtils.NullChecker(theForm.getQueueSmsThresholdPeriod(), String.class).toString());
				theCfg.setQueueSmsThresholdTime(CommonUtils.NullChecker(theForm.getQueueSmsThresholdTime(), String.class).toString());
				theCfg.setConcatenateSms(CommonUtils.NullChecker(theForm.getConcatenateSms(), String.class).toString());
				if (theCfg.getConcatenateSms().equals("Y")){
					theCfg.setConcatenateSmsNo(CommonUtils.NullChecker(theForm.getConcatenateSmsNo(), String.class).toString());
				}else{
					theCfg.setConcatenateSmsNo("1");
				}
				theCfg.setEmailFormat(CommonUtils.NullChecker(theForm.getEmailFormat(), String.class).toString());
				theCfg.setFtpFailureNotification(CommonUtils.arrayToString(theForm.getFtpFailureNotification(), theCfg.getTableListDelimiter()));
				theCfg.setFtpFailureNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getFtpFailureNotificationEmail(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setFtpFailureNotificationEmailSender(CommonUtils.NullChecker(theForm.getFtpFailureNotificationEmailSender(), String.class).toString());
				theCfg.setFtpFailureNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getFtpFailureNotificationSms(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setFtpFailureNotificationSmsSender(CommonUtils.NullChecker(theForm.getFtpFailureNotificationSmsSender(), String.class).toString());
				theCfg.setHttpSmsService(CommonUtils.NullChecker(theForm.getHttpSmsService(), String.class).toString());
				theCfg.setIpControl(CommonUtils.NullChecker(theForm.getIpControl(), String.class).toString());
//				theCfg.setIpSkip(CommonUtils.NullChecker(theForm.getIpSkip(), String.class).toString());
				theCfg.setMobileCountryPrefix(CommonUtils.NullChecker(theForm.getMobileCountryPrefix(), String.class).toString());
//				theCfg.setScheduledSmsControlDuration(CommonUtils.NullChecker(theForm.getScheduledSmsControlDuration(), String.class).toString());
				theCfg.setSmsFailureThreshold(CommonUtils.NullChecker(theForm.getSmsFailureThreshold(), String.class).toString());
				theCfg.setSmsFailureThresholdPeriod(CommonUtils.NullChecker(theForm.getSmsFailureThresholdPeriod(), String.class).toString());
				theCfg.setSmsFailureThresholdTime(CommonUtils.NullChecker(theForm.getSmsFailureThresholdTime(), String.class).toString());
				theCfg.setSmsFailureNotification(CommonUtils.arrayToString(theForm.getSmsFailureNotification(), theCfg.getTableListDelimiter()));
				theCfg.setSmsFailureNotificationEmail(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getSmsFailureNotificationEmail(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setSmsFailureNotificationEmailSender(CommonUtils.NullChecker(theForm.getSmsFailureNotificationEmailSender(), String.class).toString());
				theCfg.setSmsFailureNotificationSms(CommonUtils.arrayToString(CommonUtils.NullChecker(theForm.getSmsFailureNotificationSms(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE), theCfg.getTableListDelimiter()));
				theCfg.setSmsFailureNotificationSmsSender(CommonUtils.NullChecker(theForm.getSmsFailureNotificationSmsSender(), String.class).toString());
				theCfg.setSmsPrioritize(CommonUtils.NullChecker(theForm.getSmsPrioritize(), String.class).toString());
				theCfg.setSmsQuota(CommonUtils.NullChecker(theForm.getSmsQuota(), String.class).toString());
				theCfg.setSmsQuotaDefault(CommonUtils.NullChecker(theForm.getSmsQuotaDefault(), String.class).toString());
				theCfg.setUsageLogDuration(CommonUtils.NullChecker(theForm.getUsageLogDuration(), String.class).toString());
				theCfg.setSystemLogDuration(CommonUtils.NullChecker(theForm.getSystemLogDuration(), String.class).toString());
				theCfg.setPasswordChangeDuration(CommonUtils.NullChecker(theForm.getPasswordChangeDuration(), String.class).toString());
				theCfg.setAccountDormancyCheck(CommonUtils.NullChecker(theForm.getAccountDormancyCheck(), String.class).toString());
				theCfg.setSystemSMSSender(theForm.getSystemSMSDefaultSender());
				theCfg.setDefaultSMSType(theForm.getDefaultSmsType());
				theCfg.setCcaDataAge(theForm.getCcaDataAge());
				
				DBUtilsCrsmsg.saveOrUpdate(theCfg, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SYSTEMCFG_SAVE_EDIT,theCfg.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SYSTEMCFG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SYSTEMCFG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_SYSTEMCFG_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SYSTEMCFG_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
