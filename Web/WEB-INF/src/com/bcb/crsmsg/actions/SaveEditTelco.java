
package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.TelcoForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.EncryptionManager;
public final class SaveEditTelco extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
		}else{				
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try{			
				TelcoForm telco = (TelcoForm) form;
				SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
				String delimiter=theCfg==null?Constants.SPECIAL_CHAR_PIPE:theCfg.getTableListDelimiter();
				String[] mobileNo=CommonUtils.NullChecker(telco.getMobile_prefix(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE);			
//				String[] homeNo=CommonUtils.NullChecker(telco.getHome_prefix(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE);
//				String[] nSNo=CommonUtils.NullChecker(telco.getEmail_to(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE);
//				String[] incomingNo =CommonUtils.NullChecker(telco.getIncoming_prefix(), String.class).toString().split(Constants.SPECIAL_CHAR_NEWLINE);
				String mobileList=CommonUtils.arrayToString(mobileNo, delimiter);
//				String homeList=CommonUtils.arrayToString(homeNo, delimiter);
//				String nSList=CommonUtils.arrayToString(nSNo, delimiter);
				
//				String incomingList = CommonUtils.arrayToString(incomingNo, delimiter);
				Telco telcoMod = (Telco)DBUtilsCrsmsg.get(Telco.class, Integer.parseInt(telco.getId()),hibernate_session);
				
				if(telcoMod!=null){
					telcoMod.setTelcoName(telco.getTelco_name());					
					telcoMod.setTelcoPrefix(mobileList);
//					telcoMod.setPrefixWithin(homeList);
					telcoMod.setUrl(telco.getUrl());
					telcoMod.setUrlParam(telco.getUrlParam());
					telcoMod.setTelcoClassName(telco.getClassName());
//					telcoMod.setPrefixIncoming(incomingList);
					telcoMod.setUserId(telco.getUsername());
					telcoMod.setPassword(telco.getPassword());
//					telcoMod.setCostPerSmsWithin(telco.getWithin_cost().length()==0?Constants.VALUE_0:telco.getWithin_cost());
//					telcoMod.setCostPerSmsInter(telco.getInter_cost().length()==0?Constants.VALUE_0:telco.getInter_cost());
					telcoMod.setModifiedBy(theLogonCredential.getUserId());
					telcoMod.setModifiedDatetime(new Date());
//					telcoMod.setSmsQueueThreshold(telco.getEnabled_st());
//					telcoMod.setSmsQueueThresholdNo(telco.getQueue_threshold());
//					telcoMod.setThresholdNotificationSender(telco.getEmail_from());
//					telcoMod.setThresholdNotificationReceiver(nSList);
//					telcoMod.setThresholdNotification(telco.getEnable_notification());
					telcoMod.setStatus(telco.getSelected_status());
					telcoMod.setStatusDatetime(new Date());
					
					telcoMod.setCostIncoming(telco.getIncoming_cost().length()==0?Constants.VALUE_0:telco.getIncoming_cost());
					telcoMod.setProxyAddr(telco.getProxyAddr());
					telcoMod.setProxyPort(telco.getProxyPort());
					telcoMod.setSvcPvdId(telco.getSelectedSvcProvider());
					telcoMod.setEncryptionStatus(telco.getEncryption());
					
					telcoMod.setCostPerSmsWithin(telco.getWithin_cost().length()==0?Constants.VALUE_0:telco.getWithin_cost());
					telcoMod.setCostPerSmsInter(telco.getInter_cost().length()==0?Constants.VALUE_0:telco.getInter_cost());
					telcoMod.setShortCode(telco.getShortCode());
					telcoMod.setSuccessCode(telco.getSuccessCode());
					telcoMod.setRetryCode(telco.getRetryCode());
					telcoMod.setSmsOnHandLimit(telco.getSmsLimit());
					
					if(DBUtilsCrsmsg.saveOrUpdate(telcoMod, hibernate_session)){
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_CONTACT_SAVE_EDIT,telco.getId() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
						if(DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session)){
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_TELCO_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_TELCO_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
							this.addErrors(request,messages);
						}
					}else{
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_TELCO_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}
					
				}
				
				DBUtilsCrsmsg.closeTransaction(hibernate_session);				
				
			}catch (Exception E){
				E.printStackTrace();
				log4jUtil.error(Constants.ACTION_TELCO_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
				DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_TELCO_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
