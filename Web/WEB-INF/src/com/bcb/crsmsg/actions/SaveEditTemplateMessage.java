package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.TemplateMessageForm;
import com.bcb.crsmsg.modal.MsgTemplate;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditTemplateMessage extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.TEMPLATE_MSG) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			TemplateMessageForm theForm = (TemplateMessageForm) form;
			MsgTemplate theMsg=(MsgTemplate)DBUtilsCrsmsg.get(MsgTemplate.class, Integer.valueOf(theForm.getTemplateId()), hibernate_session);
			if(theMsg!=null){
				theMsg.setDepartment(theForm.getSelected_department());
				theMsg.setDynamicFieldName(theForm.getDynamicFieldName().replaceAll(Constants.SPECIAL_CHAR_COMMA, ((SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session)).getTableListDelimiter()));
				theMsg.setDynamicFieldNo(theForm.getDynamicFieldNo());
				theMsg.setModifiedBy(theLogonCredential.getUserId());
				theMsg.setModifiedDatetime(new Date());
				theMsg.setStatus(theForm.getSelected_status());
				theMsg.setTemplateCode(theForm.getMessageCode());
				theMsg.setTemplateContent(theForm.getMessageText());
				theMsg.setType(theForm.getSelected_type());
				theMsg.setMsgFormatType(theForm.getSelected_charset());
				
				DBUtilsCrsmsg.saveOrUpdate(theMsg, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_TEMPLATEMESSAGE_SAVE_EDIT,theMsg.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_TEMPLATEMESSAGE_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_TEMPLATEMESSAGE_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_TEMPLATEMESSAGE_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_TEMPLATEMESSAGE_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
