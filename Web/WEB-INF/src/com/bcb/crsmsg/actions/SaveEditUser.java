

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.modal.User;
import com.bcb.common.modal.UserRoles;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.SHAUtil;
import com.bcb.crsmsg.forms.UserForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.UserRights;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.crsmsg.util.log4jUtil;
public final class SaveEditUser extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		};
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.USER_SEARCH) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			String logonuser=theLogonCredential.getUserId();
			
			UserForm edituserform = (UserForm) form;
			User theUser= (User) DBUtilsCrsmsg.get(User.class, edituserform.getUserid(), hibernate_session);
			
			if(theUser!=null){
				theUser.setStaffId(edituserform.getStaffid());
				theUser.setBizPhone(edituserform.getBizphone());
				theUser.setBranchDept(1);//edituserform.getSelected_branch().intValue());
				theUser.setEmailId(edituserform.getEmailid());
				theUser.setHPhone(edituserform.getHphone());
				theUser.setExt(edituserform.getExt());

				// Handling User Roles
				String selectedRoles =edituserform.getStrSelected_roles();
				
				if (selectedRoles !=null && selectedRoles.length()> 0){
					ArrayList<Integer> selectedRolesArray=new ArrayList<Integer>();
					String[] rolesArray = selectedRoles.split(",");
					List rolesList=DBUtilsCrsmsg.DBHQLCommand("from UserRoles where userId=?", new Object[]{edituserform.getUserid()}, hibernate_session);
					ArrayList<Integer> roleIdList=new ArrayList<Integer>();
					
					for(int i=0;i<rolesArray.length;i++){
						selectedRolesArray.add(Integer.parseInt(rolesArray[i]));
					}
	
					for(int i=0;i<rolesList.size();i++){
						UserRoles aUserRole = (UserRoles)rolesList.get(i);
						roleIdList.add(aUserRole.getRoleId());
						
						if(selectedRolesArray.contains(aUserRole.getRoleId())){
							aUserRole.setStatus(Constants.STATUS_ACTIVE);
						}else{
							aUserRole.setStatus(Constants.STATUS_CLOSED);
						}
						aUserRole.setModifiedBy(logonuser);
						aUserRole.setModifiedDate(new Date());
						aUserRole.setModifiedTime(new Date());
						DBUtilsCrsmsg.saveOrUpdate(aUserRole, hibernate_session);
						
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ROLE_EDIT,Integer.toString(aUserRole.getKeyCol()) , Constants.STATUS_SUCCESS ,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
					}
					
					selectedRolesArray.removeAll(roleIdList); // new item
					
					for(int i=0;i<selectedRolesArray.size();i++){
						UserRoles aUserRole=new UserRoles();
						aUserRole.setUserId(edituserform.getUserid());
						aUserRole.setRoleId(Integer.valueOf(selectedRolesArray.get(i).toString()));
						aUserRole.setStatus(Constants.STATUS_ACTIVE);
						aUserRole.setCreatedBy(logonuser);
						aUserRole.setCreatedDate(new Date());
						aUserRole.setCreatedTime(new Date());
	
						DBUtilsCrsmsg.saveOrUpdate(aUserRole, hibernate_session);
						
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_ROLE_ADD,Integer.toString(aUserRole.getKeyCol()) , Constants.STATUS_SUCCESS ,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
					}
				}
				
				// Handling User Rights
				List user_rights_list=DBUtilsCrsmsg.DBHQLCommand("from UserRights where user.userId=?", new Object[]{theUser.getUserId()}, hibernate_session);
				UserRights[] aUserRight=new UserRights[user_rights_list.size()];
				for(int i=0;i<user_rights_list.size();i++){// Delete existing rights
					aUserRight[i]=(UserRights)user_rights_list.get(i);
					DBUtilsCrsmsg.delete(aUserRight[i], hibernate_session);
				}
				
				Integer[] selectedRights =edituserform.getSelected_rights();
				
				for(int i=0;selectedRights!=null&&i<selectedRights.length;i++){
					Integer rightsValue=selectedRights[i];
					UserRights aUserRights = new UserRights();
					aUserRights.setRightsId(rightsValue);
					aUserRights.setUser(theUser);
					
					DBUtilsCrsmsg.saveOrUpdate(aUserRights, hibernate_session);
					
					SmsAuditTrail anAuditTrail = new SmsAuditTrail();
					anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_USER_SAVE_ADD,Integer.toString(aUserRights.getId()) , Constants.STATUS_SUCCESS ,theLogonCredential.getBranchCode());
					DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
				}
				
				theUser.setUserGroup(edituserform.getSelected_department().intValue());
				theUser.setUserName(edituserform.getUsername());
				theUser.setUserStatus(edituserform.getSelected_userstatus());
				//theUser.setCompanyId(Integer.valueOf(edituserform.getSelected_company()));
				
				theUser.setFailedLogon(Integer.valueOf(edituserform.getFailedlogon())); 
				
				if(edituserform.getPassword().length() > 0){
					 theUser.setResetStatus("Y");				 
				 }
	
				if ( edituserform.getPassword() != null && edituserform.getPassword().length() > 1 )
				{
					theUser.setPassword(SecurityManager.getInstance().encryptSHA2(edituserform.getPassword()));
				};
				
				theUser.setModifiedBy(logonuser);
				theUser.setModifiedDate(new Date());
				theUser.setModifiedTime(new Date());
				//theUser.setCicsGroupId(edituserform.getCicsGroupSelect().toString());
				//theUser.setSbu(edituserform.getSelected_sbu());
				
				DBUtilsCrsmsg.saveOrUpdate(theUser, hibernate_session);
				System.out.println("request.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY) : "+request.getSession().getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY).toString());
				if(theUser.getUserId().equals(theLogonCredential.getUserId())){
					System.out.println(""+request);
				}
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.common.util.Constants.APPSNAME,com.bcb.common.util.Constants.USER_EDIT,theUser.getUserId() ,"S",theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
	
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_USER_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_USER_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
				
				return (mapping.findForward(Constants.MAPPINGVALUE_FAILED));
			}
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}
		catch (Exception E)
		{
			log4jUtil.error(Constants.ACTION_USER_SAVE_EDIT+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_SAVE_EDIT,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
