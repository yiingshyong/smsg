package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.SendSMSForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsHistory;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;
public final class SaveSendSMS extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SEND_SMS) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
		
		try {
			SendSMSForm theForm = (SendSMSForm) form;
			
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
			if(theCfg==null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_SMS_SEND_SAVE,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
				this.addErrors(request,messages);
			}else{
				int charPerSms=CommonUtils.calculateCharPerSms(theForm.getSelected_charset(), theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
				ArrayList<String> msgArray=CommonUtils.strToArray(theForm.getSmsContent(), charPerSms);
				
				ArrayList<String> lastSmsIdList=new ArrayList<String>();
				String lastSmsId=Constants.STR_EMPTY;
				String[] mobileNo=StringUtils.split(theForm.getMobileNo(), Constants.SPECIAL_CHAR_NEWLINE);
//				System.out.println("total sms: " + mobileNo.length);
				for(int i=0;i<mobileNo.length;i++){
						SmsQueue aSMS=new SmsQueue();
						aSMS.setMobileNo(mobileNo[i]);
						aSMS.setSmsScheduled(theForm.getScheduledSMS());
						aSMS.setMsgFormatType(theForm.getSelected_charset());
						aSMS.setMessage(theForm.getSmsContent());
						aSMS.setTotalSms(msgArray.size());
						aSMS.setPriority(Integer.valueOf(theForm.getSmsPriority()));
						if(theForm.getScheduledSMSDateTime().length()>0)
							aSMS.setSmsScheduledTime(theFormat.parse(theForm.getScheduledSMSDateTime()));
						aSMS.setCreatedBy(theLogonCredential.getUserId());
						aSMS.setCreatedDatetime(new Date());
						aSMS.setSentBy(theLogonCredential.getUserId());
						
						if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.SMS_APPROVAL))){
							aSMS.setSmsStatus(Constants.SMS_STATUS_3);
						}else if (CommonUtils.NullChecker(theForm.getScheduledSMS(), String.class).toString().equals(Constants.STATUS_YES)&&CommonUtils.NullChecker(theForm.getScheduledSMSDateTime(), String.class).toString().length()>0){
							aSMS.setSmsStatus(Constants.SMS_STATUS_6);
							aSMS.setApprovalBy("automated");
							aSMS.setApprovalDatetime(new Date());
						}else{
							aSMS.setSmsStatus(Constants.SMS_STATUS_4);
							aSMS.setApprovalBy("automated");
							aSMS.setApprovalDatetime(new Date());
						}
						
						aSMS.setSmsStatusBy(theLogonCredential.getUserId());
						aSMS.setSmsStatusDatetime(new Date());
						aSMS.setTelco(0);
						aSMS.setType(Constants.SMS_SOURCE_TYPE_1);
						aSMS.setVersion(Integer.valueOf(Constants.APPSVERSION));
						aSMS.setDept(((Department)DBUtilsCrsmsg.get(Department.class, Integer.valueOf(theLogonCredential.getUserGroup()), hibernate_session)).getDeptName());
						aSMS.setDeptId(Integer.valueOf(theLogonCredential.getUserGroup()));
						aSMS.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(theLogonCredential.getUserId(), null));//TODO: null would be channel id to be selected by user from front end
						if(aSMS.getSmsType().isOnline()){
							BusinessService.processSMS(aSMS);
						}else{
							DBUtilsCrsmsg.saveOrUpdate(aSMS, hibernate_session);
						}
						
						SmsHistory aSmsHistory=new SmsHistory();
						BeanUtils.copyProperties(aSmsHistory, aSMS);
						DBUtilsCrsmsg.saveOrUpdate(aSmsHistory, hibernate_session);
						
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_SMS_SEND_SAVE,aSMS.getId().toString() ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						
				}
				
				if(CommonUtils.NullChecker(theCfg.getSmsQuota(), String.class).toString().equals(Constants.STATUS_YES)){
					if(CommonUtils.checkQuota(theLogonCredential.getUserGroup(), mobileNo.length*msgArray.size(), hibernate_session)){
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_SMS_SEND_SAVE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
						this.addErrors(request,messages);
					}else{
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_SMS_SEND_SAVE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS_WITHWARNING, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED, Constants.ACTION_PARA_REASON_QUOTA_EXCEED));
						this.addErrors(request,messages);
					}
				}else{// no need check quota
					ActionMessages messages = new ActionMessages();
					messages.add(Constants.ERROR_SMS_SEND_SAVE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
					this.addErrors(request,messages);
				}
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_SMS_SEND_SAVE+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMS_SEND_SAVE,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
