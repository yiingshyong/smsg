package com.bcb.crsmsg.actions;

import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.SecurityControlForm;
import com.bcb.crsmsg.modal.Record;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public final class SecurityControlSearch extends Action{
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		log4jUtil.info("inside SecurityControlSearch execute method");
		//System.out.println("inside action class");
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SECURITY_CONTROL) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		try{
			
			
			List result = null; 
			
			if(mapping.getParameter().equals("load")){
			}else if (mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				//System.out.println("==================TRYING TO GENERATE CSV>>>>>>>>>>");	
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_IPADDRESSLIST );
				printHeader(ouputStream);
				
				int counter = 0;
				Result resultList = (Result) session.getAttribute("ipList");
				for(int i=0;resultList != null&&i<resultList.getRecord().size();i++){
					Record aRecord=resultList.getRecord().get(i);
					counter++;
					for(int j=0;j<aRecord.getColumn().size();j++){
						//System.out.println(aRecord.getColumn().get(j));
						
						if (j == 0){
							//System.out.println("j=="+counter);
							ouputStream.print (println(String.valueOf(counter)));
						}else{
						ouputStream.print (println(aRecord.getColumn().get(j)));
						}
					}
					ouputStream.println();
				}
				
				ouputStream.flush();
				
				ouputStream.close();
				
				return null;
			}else{
				SecurityControlForm theForm = (SecurityControlForm) form;
				StringBuffer query=new StringBuffer();
				String queryField=Constants.STR_EMPTY;
				System.out.println("selected state=="+theForm.getSelectedState());
				if (theForm.getSelectedState().equals(Constants.FORM_FIELD_IPADDRESS)){// By IP Address
					queryField="h.ip";
				}else if ( theForm.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT)){// By Department
					System.out.println("search key =="+ theForm.getSearchkey());
					if(theForm.getSearchkey() == ""){
						System.out.println("inside department if clause");
						queryField="h.ip";//return all ipaddress
					}else {
					queryField="h.dept";
					}
				}else if (theForm.getSelectedState().equals(Constants.FORM_FIELD_SYSTEM)){// By System
					System.out.println("search key =="+ theForm.getSearchkey());
					if(theForm.getSearchkey() == ""){
						System.out.println("inside system if clause");
						queryField="h.ip";//return all ipaddress
					}else{
					queryField="h.system";
					}
				}else{// No matched search state
					
				}
				//System.out.println("search key =="+ theForm.getSearchkey());
				/*query.append(
						"select h.id, h.ip, h.createdBy, DATE(h.createdDatetime), h.modifiedBy, DATE(h.modifiedDatetime), h.dept, h.system, h.remarks, h.status " +
						"from HttpIpAddress h " +
						"where " +
						queryField+" like ? "+
						"order by h.id"
						);*/
				query.append(
						"select h.id, h.ip, h.createdBy, DATE(h.createdDatetime), h.modifiedBy, DATE(h.modifiedDatetime), h.dept, h.system, h.remarks " +
						"from HttpIpAddress h " +
						"where " +
						queryField+" like ? "+
						"order by h.id"
						);
				result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
					Constants.STR_PERCENT + theForm.getSearchkey() + Constants.STR_PERCENT
					});
				theForm.setResultList(CommonUtils.populateResult(result));
				session.setAttribute("ipList", CommonUtils.populateResult(result));
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_IPADDRESS_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ACTION_IPADDRESS_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}

private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		
		ouputStream.print (println("No."));
		ouputStream.print (println("IP Address"));
		ouputStream.print (println("Created By"));
		ouputStream.print (println("Created Date"));
		ouputStream.print (println("Modified By"));
		ouputStream.print (println("Modified Date"));
		ouputStream.print (println("Department"));
		ouputStream.print (println("System"));
		ouputStream.print (println("Remarks"));
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	

}
