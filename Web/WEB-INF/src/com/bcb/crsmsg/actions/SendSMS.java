

package com.bcb.crsmsg.actions;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.SendSMSForm;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class SendSMS extends Action {
	
	// ----------------------------------------------------- Instance Variables
	
	/**
	 * The <code>Log</code> instance for this application.
	 */
	// private Log log = LogFactory.getLog("org.apache.struts.webapp.Example");
	
	// --------------------------------------------------------- Public Methods
	
	// See superclass for Javadoc
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SEND_SMS) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		try {
			if(session.getAttribute(Constants.SESSION_KEY_MULTIPLE_SMS_MSG) == null){
				session.setAttribute(Constants.SESSION_KEY_MULTIPLE_SMS_MSG, AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.MULTIPLE_SMS_ALERT));
			}
			SendSMSForm theForm =(SendSMSForm)form;
			
			// Set system datetime
			SimpleDateFormat theFormat=new SimpleDateFormat(Constants.DATEFORMAT_JAVACSRIPT);
			theForm.setSystemDateTime(theFormat.format(new Date()));
			
			// Set maximum SMS per message
			SmsCfg theCfg = (SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION));
			
			if(theCfg!=null){
				theForm.setConcatenateSMS(theCfg.getConcatenateSms());
				theForm.setMaxNoOfSMS(CommonUtils.NullChecker(theCfg.getConcatenateSmsNo(), Integer.class).toString());
				theForm.setCharPerSms(CommonUtils.NullChecker(theCfg.getCharPerSms(), Integer.class).toString());
				theForm.setSmsPriority(DBUtilsCrsmsg.DBHQLCommand("select max(id) from Priority where status=?", new Object[]{Constants.STATUS_ACTIVE}).get(0).toString());
				theForm.setSmsPriorityShow(theLogonCredential.getRights().contains(Integer.valueOf(Constants.SMS_PRIORITY_CHANGE))?Constants.STATUS_TRUE_1:null);
				theForm.setAdvanceDayLimit(CommonUtils.NullChecker(theCfg.getScheduledSmsControlDuration(), Integer.class).toString());
			}else{
				theForm.setMaxNoOfSMS(Constants.VALUE_1);
				theForm.setCharPerSms(Constants.VALUE_0);
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_SMS_SEND+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SMS_SEND,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
}
