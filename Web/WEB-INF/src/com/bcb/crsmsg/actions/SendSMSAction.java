package com.bcb.crsmsg.actions;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.bean.HttpPostingBean;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.modal.UserChannelID;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;
import com.bcb.sgwcore.util.DaoBeanFactory;
import com.ibm.icu.text.NumberFormat;

public class SendSMSAction extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	
	private static final long serialVersionUID = 4122355461471104515L;
	private static final Log log = LogFactory.getLog(SendSMSAction.class);
	private static Cipher CIPHER;
	private static final String RETURN_CODE_QUEUED = "1";
	private static final String ERROR_CODE_SMSG_APP_ERROR = "0";
	private static final String ERROR_CODE_INSUFFICIENT_QUATA = "2";
	private static final String ERROR_CODE_INVALID_CLIENT_IP = "3";
	private static final String ERROR_CODE_MESSAGE_PARAM_NOT_FOUND = "4";
	private static final String ERROR_CODE_INVALID_MOBILE_NO = "5";
	private static final String ERROR_CODE_INVALID_SMSG_SETUP = "6";
	private static final String ERROR_CODE_SMSG_HTTP_SERVICE_DISABLED = "7";
	private static final String ERROR_CODE_INVALID_SCHEDULED_TIME_FORMAT = "8";
	private static final String ERROR_CODE_INVALID_REQUEST_MODE = "10";
	private static final String ERROR_CODE_INVALID_USER_ID = "11";
	private static final String ERROR_CODE_INVALID_CLIENT_TIMESTAMP_FORMAT = "13";
	private static final String ERROR_CODE_INVALID_CHANNEL_ID = "12";
	
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		long startTime = System.currentTimeMillis();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();		
		String returnMsg = null;
		SmsQueue smsQueue = new SmsQueue();
		HttpPostingBean inputBean = new HttpPostingBean();
		StringBuffer buffer = new StringBuffer(request.getRemoteAddr());
		String UUID="";
		try {
			UUID=startTime+request.getParameter("user");
			String DEFAULT_DATE_FORMAT = "dd-MM-yyyy hh:mm:ss:SSS";
			String startDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(startTime);
			String endDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date());
				System.out.println(request.getRemoteAddr());
				inputBean.setClientIp(request.getRemoteAddr());
				inputBean.setMobileNo(request.getParameter("tar_num"));
				inputBean.setMessage(request.getParameter("tar_msg"));
				inputBean.setRequestMode(request.getParameter("tar_mode"));
				inputBean.setPriority(request.getParameter("priority"));
				inputBean.setUserId(request.getParameter("user"));
				inputBean.setScheduledTimeStr(request.getParameter("scheduled_time"));
				inputBean.setChannelId(request.getParameter(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.HTTP_POSTING_PARAM_CHANNEL)));
				inputBean.setClientTimestampStr(request.getParameter(AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.HTTP_POSTING_PARAM_CLIENT_TIMESTAMP)));								
				
				Enumeration<String> names = request.getParameterNames();
				buffer.append(" *");
				while(names.hasMoreElements()){
					String name = names.nextElement();
					String[] values = request.getParameterValues(name);
					buffer.append(name).append(":");
					if(values != null && values.length>0){
						for(String value : values){
							buffer.append("'").append(value).append("'").append(",");
						}					
					}
				}
				buffer.append("*");
				log.info("Before ValidateInputs : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				System.out.println("Before ValidateInputs : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				returnMsg = validateInputs(inputBean, smsQueue);
				log.info("After ValidateInputs : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				System.out.println("After ValidateInputs : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				if(returnMsg == null){
					returnMsg = validateSMSGSetting(inputBean, smsQueue);
				}				
				
				if (returnMsg == null){
					
					smsQueue.setChannelId(inputBean.getChannelId());
					smsQueue.setCreatedBy(inputBean.getUserId());
					smsQueue.setCreatedDatetime(new Date());
					log.info("Before selectSMSType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before selectSMSType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					TblRef smsType = DaoBeanFactory.getTblRefDao().selectSMSType(inputBean.getUserId(), inputBean.getChannelId());
					log.info("After selectSMSType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After selectSMSType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					smsQueue.setSmsType(smsType);
					if(smsType.isEncrypted()){
//						smsQueue.setMessage(Base64.encodeBase64String(CIPHER.doFinal(inputBean.getMessage().getBytes())));
						smsQueue.setMessage(SecurityManager.getInstance().encrypt(inputBean.getMessage()));
					}else{
						smsQueue.setMessage(inputBean.getMessage());
					}
					log.info("Before normalizeMobile : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before normalizeMobile : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
										
					smsQueue.setMobileNo(BusinessService.normalizeMobile(inputBean.getMobileNo()));
					log.info("After normalizeMobile : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After normalizeMobile : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					if(inputBean.getRequestMode().equals("text")){
						smsQueue.setMsgFormatType("ASCII/Text");
					}else if(inputBean.getRequestMode().equals("utf") || inputBean.getRequestMode().equals("unicode")){
						smsQueue.setMsgFormatType("UTF-8");
					}	

					log.info("Before findLowestPriority : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before findLowestPriority : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					if(StringUtils.isNotEmpty(inputBean.getPriority())){
						smsQueue.setPriority(Integer.valueOf(inputBean.getPriority()));					
					}
					else{
//						smsQueue.setPriority(Integer.valueOf(DBUtilsCrsmsg.DBHQLCommand("select max(id) from Priority where status=?", new Object[]{Constants.STATUS_ACTIVE}, session).get(0).toString()));
						smsQueue.setPriority(DaoBeanFactory.getPriorityDao().findLowestPriority());
					}
					log.info("After findLowestPriority : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After findLowestPriority : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					smsQueue.setRemarks(Constants.STR_EMPTY);
					smsQueue.setSentBy(inputBean.getUserId());
					smsQueue.setTelco(0);
					smsQueue.setVersion(Integer.valueOf(Constants.APPSVERSION));
					smsQueue.setType(Constants.SMS_SOURCE_TYPE_2);
//					List rightsList=DBUtilsCrsmsg.DBHQLCommand("from UserRights where user.userId=? and rightsId=?", new Object[]{inputBean.getUserId(), Integer.valueOf(Constants.SMS_APPROVAL)}, session);
					log.info("Before hasRights : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before hasRights : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					if(!DaoBeanFactory.getUserDao().hasRights(inputBean.getUserId(), Integer.valueOf(Constants.SMS_APPROVAL))){
						if(StringUtils.isNotEmpty(smsQueue.getSmsScheduled()) && smsQueue.getSmsScheduled().equals(Constants.STATUS_YES)){
							smsQueue.setSmsStatus(Constants.SMS_STATUS_6);
						}else{
							smsQueue.setSmsStatus(Constants.SMS_STATUS_4);
						}
						smsQueue.setApprovalBy("automated");
						smsQueue.setApprovalDatetime(new Date());
					}else{
						smsQueue.setSmsStatus(Constants.SMS_STATUS_3);
					}
					log.info("After hasRights : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After hasRights : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					smsQueue.setSmsStatusBy("automated");
					smsQueue.setSmsStatusDatetime(new Date());
					log.info("Before smsQueue.setSmsType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before smsQueue.setSmsType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					smsQueue.setSmsType(DaoBeanFactory.getTblRefDao().selectSMSType(smsQueue.getCreatedBy(), smsQueue.getChannelId()));
					log.info("After smsQueue.setSmsType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After smsQueue.setSmsType : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					
					log.info("Before processSMS : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("Before processSMS : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					if(smsQueue.getSmsType().isOnline()){
							BusinessService.processSMS(smsQueue);	
							returnMsg = RETURN_CODE_QUEUED;
					}
					else{
						DaoBeanFactory.getHibernateTemplate().saveOrUpdate(smsQueue);
						returnMsg = RETURN_CODE_QUEUED;						
					}	
					log.info("After processSMS : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
					System.out.println("After processSMS : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				}
				
				log.info("Before AuditTrail : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				System.out.println("Before AuditTrail : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit("HTTP SMS",request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,
						com.bcb.crsmsg.util.Constants.ACTION_SMS_ACTION_SEND,
						"Response: " + returnMsg, 
						returnMsg.equals(RETURN_CODE_QUEUED)? Constants.STATUS_SUCCESS : Constants.STATUS_FAIL,
						Constants.STR_EMPTY);
				DaoBeanFactory.getHibernateTemplate().saveOrUpdate(anAuditTrail);	
				log.info("After AuditTrail : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				System.out.println("After AuditTrail : "+new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date())+" Different "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
				out.println(returnMsg);
		} catch (Exception e) {
			returnMsg = ERROR_CODE_SMSG_APP_ERROR;
//			DBUtilsCrsmsg.rollbackTransaction(session);
			log.error("Error in http posting sms. Reason: " + e.getMessage(), e);
			SmsAuditTrail anAuditTrail = new SmsAuditTrail();
			anAuditTrail.setAudit("HTTP SMS",request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,
					com.bcb.crsmsg.util.Constants.ACTION_SMS_ACTION_SEND,
					"Response: " + returnMsg, 
					Constants.STATUS_FAIL,
					Constants.STR_EMPTY);
			DaoBeanFactory.getHibernateTemplate().saveOrUpdate(anAuditTrail);				
			out.print(returnMsg);
		}finally{
			if(smsQueue != null && smsQueue.getId() != null){
				buffer.append(" ").append(smsQueue.getId());
			}			
			buffer.append(" ").append(returnMsg);
			buffer.append(" ").append((System.currentTimeMillis() - startTime));

			
			log.debug(buffer.toString());
			String DEFAULT_DATE_FORMAT = "dd-MM-yyyy hh:mm:ss:SSS";
			String startDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(startTime);
			String endDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(new Date());

			log.info("StartTime : "+startDate+" | End Time : "+endDate +" | Different : "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
			System.out.println("StartTime : "+startDate+" | End Time : "+endDate +" | Different : "+(System.currentTimeMillis() - startTime)+"  | UUID : "+UUID);
			
			
		}
	}  	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}   
	
	@SuppressWarnings("unchecked")
	private String validateSMSGSetting(HttpPostingBean inputBean, SmsQueue smsQueue){
		String returnMsg = null;
		SmsCfg theCfg = DaoBeanFactory.getSmsCfgDao().findByCfgId(Constants.APPSVERSION);
		if(theCfg == null){
			log.error("SMSG Config not found, http client request failed to process.");
			returnMsg = ERROR_CODE_INVALID_SMSG_SETUP;
		}else{
			int charPerSms = CommonUtils.calculateCharPerSms(converMsgType(inputBean.getRequestMode()), theCfg.getCharPerSms(), theCfg.getConcatenateSms(), theCfg.getConcatenateSmsNo());
			ArrayList<String> msgArray=CommonUtils.strToArray(inputBean.getMessage(), charPerSms);
			smsQueue.setTotalSms(msgArray.size());
			
			if (theCfg.getHttpSmsService().equals("0")){ //http post been disabled
				returnMsg = ERROR_CODE_SMSG_HTTP_SERVICE_DISABLED;				
			}
			
			//Check sms quota
			if(CommonUtils.NullChecker(theCfg.getSmsQuota(), String.class).toString().equals(Constants.STATUS_YES)){
				User user = DaoBeanFactory.getUserDao().findByUserId(inputBean.getUserId());
				if(user != null){
					List quota = DaoBeanFactory.getSmsQuataDao().findSmsQuota(user.getUserGroup(), msgArray.size());
					if( quota != null && !quota.isEmpty()){
						
					}else{
						returnMsg = ERROR_CODE_INSUFFICIENT_QUATA;
					}
				}
			}
		}		
		return null;
	}
	
	private String validateInputs(HttpPostingBean inputBean, SmsQueue smsQueue){
		
		if(!validateMobileNo(inputBean.getMobileNo())){
			return ERROR_CODE_INVALID_MOBILE_NO;
		}		
		if (!validateIp(inputBean.getClientIp()))
			return ERROR_CODE_INVALID_CLIENT_IP;
		
		if (!validateUser(smsQueue, inputBean.getUserId())){
			return ERROR_CODE_INVALID_USER_ID;
		}
		
		if (!validateRequestMode(inputBean.getRequestMode()))
			return ERROR_CODE_INVALID_REQUEST_MODE;
				
		if(StringUtils.isNotEmpty(inputBean.getChannelId())){
			UserChannel channel = (UserChannel) DaoBeanFactory.getHibernateTemplate().get(UserChannel.class, new UserChannelID(inputBean.getChannelId(), inputBean.getUserId()));
			if(channel == null){
				return ERROR_CODE_INVALID_CHANNEL_ID;
			}
		}
				
		if (StringUtils.isEmpty(inputBean.getMessage())){
			return ERROR_CODE_MESSAGE_PARAM_NOT_FOUND;			
		}
	
		if(StringUtils.isNotEmpty(inputBean.getScheduledTimeStr())){
			SimpleDateFormat theFormat = new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
			try {
				smsQueue.setSmsScheduledTime(theFormat.parse(inputBean.getScheduledTimeStr()));
				smsQueue.setSmsScheduled(Constants.STATUS_YES);
			} catch (ParseException e) {				
				return ERROR_CODE_INVALID_SCHEDULED_TIME_FORMAT;
			}			
		}
		
		if(StringUtils.isNotEmpty(inputBean.getClientTimestampStr())){
			String dateFormat = AppPropertiesConfig.getInstance().getSettings().getString(AppPropertiesConfig.HTTP_POSTING_PARAM_CLIENT_TIMESTAMP_FORMAT);
			SimpleDateFormat df = new SimpleDateFormat(dateFormat);
			try {
				smsQueue.setClientTimestamp(df.parse(inputBean.getClientTimestampStr()));						
			} catch (ParseException e) {				
				log.error("Parsing date exception -> '" + inputBean.getClientTimestampStr() + "' format: '" + dateFormat + "'");
				return ERROR_CODE_INVALID_CLIENT_TIMESTAMP_FORMAT;				
			}			
		}
		
		
		return null;		
	}
	
	private boolean validateMobileNo(String mobileNo){
		if (mobileNo ==null||mobileNo.trim().length()==0 ){
			return false;
		}else{
			mobileNo = BusinessService.normalizeMobile(mobileNo);
//			Pattern pattern = Pattern.compile("^[6][05][189]\\d{7,9}$");
//			/* mobile no. pattern accepted
//			 601NNNNNNNN
//			 658NNNNNNNN
//			 659NNNNNNNN
//			 */
//			Matcher l_matcher = pattern.matcher(mobileNo);
//			 
//			if( !l_matcher.matches() ){
//				return false;
//			}
			if(StringUtils.isEmpty(mobileNo) || !StringUtils.isNumeric(mobileNo)) {
				return false;
			}
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean validateIp(String ipAddress){
//		List prList_result =  DBUtilsCrsmsg.DBHQLCommand("from HttpIpAddress where ip = ?", new Object[]{ipAddress}, hibernate_session);		

		List prList_result =  DaoBeanFactory.getSmsCfgDao().findIP(ipAddress);
		return (prList_result != null && !prList_result.isEmpty()) ? true : false;
	}
	
	private boolean validateRequestMode(String requestMode){
		return (StringUtils.isNotEmpty(requestMode) && (requestMode.equals("utf") || requestMode.equals("text") || requestMode.equals("unicode"))) ? true : false;
	}
	
	@SuppressWarnings("unchecked")
	private boolean validateUser(SmsQueue sms, String user){
		if(StringUtils.isEmpty(user)){
			return false;
		}else{
			List result = DaoBeanFactory.getDeptDao().getDeptByUserId(user);
			if (result != null && result.size() == 1){
				Department theDept=(Department)result.get(0);
				sms.setDeptId(theDept.getId());
				sms.setDept(theDept.getDeptName());
				return true;
			}			
		}
		return false;
	}
	
	private String converMsgType(String msgType){
		if(msgType.equals("text")){
			return Constants.FORM_FIELD_CHARSET_ASCIITEXT;
		}else if(msgType.equals("utf")||msgType.equals("unicode")){
			return Constants.FORM_FIELD_CHARSET_UTF8;
		}else{
			return Constants.STR_EMPTY;
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
		try{
			log.info("Setting encryption key ...");
			SmsCfg config = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
			Key encKey = null;
			if(StringUtils.isEmpty(config.getSerData())){
				KeyGenerator gen = KeyGenerator.getInstance("AES");
				gen.init(128);
				encKey = gen.generateKey();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(encKey);
				oos.flush();
				oos.close();
				String objectEncodedText = Base64.encodeBase64String(baos.toByteArray());
				config.setSerData(objectEncodedText);
				DaoBeanFactory.getSmsCfgDao().update(config);
			}
			if(encKey == null){
				String objectEncodedText = config.getSerData();
				encKey = (Key)new ObjectInputStream(new ByteArrayInputStream(Base64.decodeBase64(objectEncodedText))).readObject();
			}
			CIPHER = Cipher.getInstance("AES");
			CIPHER.init(Cipher.ENCRYPT_MODE, encKey);
		}catch(Exception e){
			log.error("Error while setting https sms message encryption key. Reason: " + e.getMessage(), e);
		}
	}
	
}