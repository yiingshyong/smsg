package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.AvailableDB;
import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.SentLogForm;
import com.bcb.crsmsg.modal.SentLogModal;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public class SentLog extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		Session hibernateSession = null;		
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.SENT_LOG) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
				SentLogForm theForm =(SentLogForm) form;
				List group_result = DBUtilsCrsmsg.DBHQLCommand("from AvailableDB where availableDBStatus = ? ", new Object[]{Constants.STATUS_ACTIVE});
				ArrayList<String> group_label = new ArrayList<String>();
				ArrayList<String> group_value = new ArrayList<String>();
				for (int i =0 ; i < group_result.size(); i ++)
				{
					AvailableDB aGroup = (AvailableDB) group_result.get(i); 
					group_label.add(aGroup.getName());
					group_value.add(aGroup.getHibernateValue());		
				}
				theForm.setSearchbyserver_label(group_label);
				theForm.setSearchbyserver_value(group_value);
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)){
				SentLogForm theForm =(SentLogForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());
				
				/* Define User Rights */
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				String user=Constants.STR_EMPTY;
				System.out.println("is admin: " + isAdmin);
				System.out.println("is dept: " + isDept);
				System.out.println("is personal: " + isPersonal);
				if (isAdmin){// not restricted by user, show all
					user=Constants.STR_EMPTY;
				}else if (isDept){// restricted by cases from users of same department
					user = getUserList(theLogonCredential.getUserId());
				}else if(isPersonal){// restricted by own cases
					user=Constants.SPECIAL_QUOTE_SINGLE+theLogonCredential.getUserId()+Constants.SPECIAL_QUOTE_SINGLE;
				}
				System.out.println("Users: ***" + user + "***");
				/* Define User Rights */

				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				if (theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "s.mobile_no";						 
				}else if (theForm.getSelectedState().equals(Constants.STR_BY_SENDER)){
					queryParam = "s.sent_by";
				}else if (theForm.getSelectedState().equals(Constants.STR_BY_DEPARTMENT)){
					queryParam = "s.dept";
				}else if (theForm.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR)){
					queryParam = "t.telco_name";
				/*}else if ( sentLogForm.getSelectedState().equals(Constants.STR_BY_SEND_MODE)){
					queryParam = "mode";*/
				}else if (theForm.getSelectedState().equals(Constants.STR_BY_SEND_TYPE)){
					queryParam = "s.type";
				}else{// Can't find match selected state
                }
				/* Define search selected state*/
				
				/* Get total no. of sms */
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(s.sms_id) from sms s, telco t where s.telco=t.telco_id and (s.sms_status_datetime between ? and ?) and s.sms_status = 'S'"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and s.sent_by in (" + user + ")":Constants.STR_EMPTY),
						 new String[]{}, 
						 new NullableType[]{},
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT},
						theForm.getSelectedServer()						
						).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());

				sql.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time,  t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.sms_scheduled, s.approval_by, s.approval_datetime, s.channel_id");
				sql.append(" from sms s, telco t where s.telco=t.telco_id" +
						   " and (s.sms_status_datetime between ? and ?) and s.sms_status = 'S' "+
						   " and (UPPER("+ queryParam +") like ? "+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						   (user.length()>0?" and s.sent_by in (" + user + ")":Constants.STR_EMPTY));
//				sql.append(" order by s.sms_status_datetime desc LIMIT "+offset+", "+pageSize);
				
				DBUtilsCrsmsg dbObj = new DBUtilsCrsmsg(sql.toString(), 
						new String[]{"s.sms_id", "s.sms_status_datetime", "s.mobile_no", "s.message", "s.sent_by", "s.dept", "s.sms_scheduled_time", "t.telco_name", "s.type", "s.remarks", "s.delivery_report_date", "s.created_datetime", "s.sms_scheduled", "s.approval_by", "s.approval_datetime", "s.channel_id"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});

//				resultList= DBUtilsCrsmsg.DBSQLCommand(
//						dbObj.getCommand() + " order by s.sms_status_datetime desc LIMIT "+offset+", "+pageSize,
//						dbObj.getCmdParamName(),
//						dbObj.getCmdType(),
//						dbObj.getCmdParamObj()
//						);
				resultList= DBUtilsCrsmsg.DBSQLCommand(
						dbObj.getCommand() + " order by s.sms_status_datetime desc LIMIT "+offset+", "+pageSize,
						dbObj.getCmdParamName(),
						dbObj.getCmdType(),
						dbObj.getCmdParamObj(),
						theForm.getSelectedServer()
						);
				/*while(offset>=resultList.size()&&offset!=0){
					offset-=pageSize;// back one page
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<SentLogModal> dataResult = new ArrayList<SentLogModal>();
					int runningCounter = 0;
	
					while (resultIter.hasNext()){
						runningCounter += 1;
						
						Object[] rowResult = (Object[]) resultIter.next();	
						SentLogModal sentLog= new SentLogModal();
						sentLog.setId(runningCounter);
						sentLog.setSeq(String.valueOf(runningCounter));
						//sentLog.setDate( (String) rowResult[1] );
						sentLog.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						sentLog.setMobileNo( (String) rowResult[2] );
						sentLog.setMessage( (String) rowResult[3] );
						sentLog.setSentBy( (String) rowResult[4] );
						sentLog.setChannelId(CommonUtils.NullChecker(rowResult[15], String.class).toString() );
						sentLog.setDept((String) rowResult[5]  );
						sentLog.setScheduledStatus(CommonUtils.NullChecker(rowResult[12], String.class).toString() );
						if(sentLog.getScheduledStatus().equals(Constants.STATUS_YES))
							sentLog.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
						else
							sentLog.setScheduled(Constants.STR_EMPTY);
						sentLog.setMode(Constants.STR_EMPTY);
						sentLog.setOperator((String) rowResult[7]  );
						sentLog.setType((String) rowResult[8]  );
						sentLog.setRemarks((String) rowResult[9]  );
						sentLog.setDeliveryReportDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[10]));
						sentLog.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[11]));
						sentLog.setApproval(CommonUtils.NullChecker(rowResult[13], String.class).toString());
						if( rowResult[14]!=null){
							sentLog.setApprovalDatetime( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[14]));
						}else{
							sentLog.setApprovalDatetime(Constants.STR_EMPTY);
						}
					    dataResult.add(sentLog);
					}
					
					request.setAttribute(Constants.SENTLOG_RESULT_KEY, dataResult);
//					session.setAttribute(Constants.SENTLOG_KEY, dataResult);
					session.setAttribute(Constants.DBOBJ, dbObj);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
//				ArrayList dataList = (ArrayList) session.getAttribute(Constants.SENTLOG_KEY);
				DBUtilsCrsmsg dbObj = (DBUtilsCrsmsg) session.getAttribute(Constants.DBOBJ);
				SentLogForm theForm =(SentLogForm) form;
				response.reset();
				
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_SENTLOGREPORT );
	     		printHeader(ouputStream);
	     		
				hibernateSession = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();
				ScrollableResults resultList = DBUtilsCrsmsg.DBSQLScrollCommand(dbObj.getCommand(), dbObj.getCmdParamName(), dbObj.getCmdType(), dbObj.getCmdParamObj(), hibernateSession);				
				
				if(resultList != null){
					int runningCounter = 0;
					while (resultList.next()){
						runningCounter++;
						Object[] rowResult = resultList.get();		     			 
						ouputStream.print (CommonUtils.println(String.valueOf(runningCounter)));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1])));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[11])));
						ouputStream.print (CommonUtils.println((String) rowResult[2]));
						ouputStream.print (CommonUtils.println((String) rowResult[3]));
						ouputStream.print (CommonUtils.println((String) rowResult[4]));
						ouputStream.print (CommonUtils.println((String) rowResult[15]));
						ouputStream.print (CommonUtils.println((String) rowResult[5]));
						ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[7], String.class).toString()));
						ouputStream.print (CommonUtils.println((String) rowResult[8]));
						if(CommonUtils.NullChecker(rowResult[12], String.class).toString().equals(Constants.STATUS_YES) ){
							ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6])));							
						}else{
							ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
						}
						ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[13], String.class).toString()));
						if(rowResult[14]!=null){
							ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[14], String.class).toString()));
						}else{
							ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
						}						
						ouputStream.print (CommonUtils.println((String) rowResult[9]));												
		      			
		     			ouputStream.println();
		     		}
	     		}
	     		ouputStream.flush();
                ouputStream.close();
                hibernateSession.getTransaction().commit();
                return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_SENTLOG+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_SENTLOG,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			if(hibernateSession != null && hibernateSession.getTransaction().isActive()){
				hibernateSession.getTransaction().rollback();
			}
		}finally{
			if(hibernateSession != null && hibernateSession.isOpen()){
				hibernateSession.close();
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private String getUserList(String userId){
		System.out.println("Getting users user this group");
		ArrayList<String> userIdList=new ArrayList<String>();
		List prList_result =DBUtilsCrsmsg.DBHQLCommand("select userId from User where userGroup in (select userGroup from User u where userId = ?) ", new Object[]{userId});
		System.out.println("User size: " + prList_result.size());
		for(int i=0;i<prList_result.size();i++){
			userIdList.add(Constants.SPECIAL_QUOTE_SINGLE+prList_result.get(i).toString()+Constants.SPECIAL_QUOTE_SINGLE);
		}
		
		return CommonUtils.arrayToString(userIdList, Constants.SPECIAL_CHAR_COMMA);	
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Sent Date"));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Sent By"));
		ouputStream.print (CommonUtils.println("Channel Id"));
		ouputStream.print (CommonUtils.println("Dept"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Type"));
		ouputStream.print (CommonUtils.println("Scheduled Date"));
		ouputStream.print (CommonUtils.println("Approval By"));
		ouputStream.print (CommonUtils.println("Approval Date"));
		ouputStream.print (CommonUtils.println("Remarks"));
		ouputStream.println();
	}
}