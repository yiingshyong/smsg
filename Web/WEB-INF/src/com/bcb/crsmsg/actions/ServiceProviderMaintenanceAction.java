package com.bcb.crsmsg.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.ServiceProviderMaintenanceForm;
import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ServiceProviderMaintenanceAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		ServiceProviderMaintenanceForm theForm = (ServiceProviderMaintenanceForm) form;
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_LOAD)){
			if(theForm.getSvcProvider() != null && theForm.getSvcProvider().getId() != null && theForm.getSvcProvider().getId() >0){
				theForm.setSvcProvider((ServiceProvider) DaoBeanFactory.getHibernateTemplate().get(ServiceProvider.class, theForm.getSvcProvider().getId()));
			}
		}
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			DaoBeanFactory.getServiceProviderGroupDao().delete(theForm.getSvcProvider().getId());
			theForm.setSvcProviders(DaoBeanFactory.getServiceProviderGroupDao().getActiveServiceProviders());
		}
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			if(theForm.getSvcProvider().getId() != null && theForm.getSvcProvider().getId() == 0){
				theForm.getSvcProvider().setId(null);
			}
			DaoBeanFactory.getHibernateTemplate().saveOrUpdate(theForm.getSvcProvider());
			theForm.setSvcProviders(DaoBeanFactory.getServiceProviderGroupDao().getActiveServiceProviders());
			theForm.setSvcProvider(new ServiceProvider());
		}		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
}
