package com.bcb.crsmsg.actions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SmsRateMaintenanceForm;
import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SmsRateMaintenanceAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SYSTEM_SETTING) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		SmsRateMaintenanceForm theForm = (SmsRateMaintenanceForm) form;
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_LOAD)){
			if(theForm.getSmsRate() != null && theForm.getSmsRate().getId() != null && theForm.getSmsRate().getId()>0){
				theForm.setSmsRate((SmsRate) DaoBeanFactory.getHibernateTemplate().get(SmsRate.class, theForm.getSmsRate().getId()));
			}
		}
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			DaoBeanFactory.getSmsRateDao().delete(theForm.getSmsRate().getId());
			theForm.setSmsRates(DaoBeanFactory.getSmsRateDao().getActiveSmsRate());
		}
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			if(theForm.getSmsRate().getId() != null && theForm.getSmsRate().getId() == 0){
				theForm.getSmsRate().setId(null);
			}
			Map<String, String> uniqueCountryCodes = new HashMap<String, String>();
			for(String countryCode : StringUtils.splitByWholeSeparator(theForm.getSmsRate().getCountryCodes(), com.bcb.common.util.Constants.LINE_SEPARATOR)){
				String value = countryCode.trim();
				if(StringUtils.isNotEmpty(value)){
					uniqueCountryCodes.put(value, value);
				}
			}
			String[] sortedCountryCodes =uniqueCountryCodes.keySet().toArray(new String[uniqueCountryCodes.size()]); 
			Arrays.sort(sortedCountryCodes);			
			theForm.getSmsRate().setCountryCodes(StringUtils.join(sortedCountryCodes, com.bcb.common.util.Constants.LINE_SEPARATOR));
			DaoBeanFactory.getHibernateTemplate().saveOrUpdate(theForm.getSmsRate());
			theForm.setSmsRates(DaoBeanFactory.getSmsRateDao().getActiveSmsRate());
			theForm.setSmsRate(new SmsRate());
		}	
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
}
