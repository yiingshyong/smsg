package com.bcb.crsmsg.actions;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.crsmsg.forms.SocketEncryptionKeyForm;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.SecurityManager;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SocketEncryptionKeyAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		

		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.SOCKET_ENC_KEY) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_LOAD)){
			return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));			
		}
		
		SocketEncryptionKeyForm theForm = (SocketEncryptionKeyForm) form;
		SmsCfg smsCfg = DaoBeanFactory.getSmsCfgDao().findSmsCfg();
		if(StringUtils.isNotEmpty(theForm.getBtn1()) && StringUtils.isNotEmpty(theForm.getSmsCfg().getSocketEncComp1())){
			smsCfg.setSocketEncComp1(theForm.getSmsCfg().getSocketEncComp1());
			theForm.getSmsCfg().setSocketEncComp1("");
		}
		else if(StringUtils.isNotEmpty(theForm.getBtn2()) && StringUtils.isNotEmpty(theForm.getSmsCfg().getSocketEncComp2())){
			smsCfg.setSocketEncComp2(theForm.getSmsCfg().getSocketEncComp2());
			theForm.getSmsCfg().setSocketEncComp2("");
		}
		else if(StringUtils.isNotEmpty(theForm.getBtn3()) && StringUtils.isNotEmpty(theForm.getSmsCfg().getSocketEncComp3())){
			smsCfg.setSocketEncComp3(theForm.getSmsCfg().getSocketEncComp3());
			theForm.getSmsCfg().setSocketEncComp3("");
		}
		
		if(StringUtils.isNotEmpty(smsCfg.getSocketEncComp1()) && StringUtils.isNotEmpty(smsCfg.getSocketEncComp2()) && StringUtils.isNotEmpty(smsCfg.getSocketEncComp3())){
			BigInteger result = new BigInteger(smsCfg.getSocketEncComp1(), 16).xor(new BigInteger(smsCfg.getSocketEncComp2(), 16)).xor(new BigInteger(smsCfg.getSocketEncComp3(), 16));
			String hexString = result.toString(16);
			String encryptedKey = SecurityManager.getInstance().getJasyptEnc().encrypt(hexString + hexString.substring(0,16));			
			smsCfg.setClZPK(encryptedKey);					
		}
		DaoBeanFactory.getHibernateTemplate().saveOrUpdate(smsCfg);
		
		ActionMessages messages = new ActionMessages();
		messages.add(Constants.ERROR_LOGONACTION,new ActionMessage("errors.record.saved"));
		this.addErrors(request,messages);		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}

	
}
