

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.TemplateMessageSearchForm;
import com.bcb.crsmsg.modal.Record;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class TemplateMessageSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.TEMPLATE_MSG) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		try{
			List result = null; 
			TemplateMessageSearchForm esform =  (TemplateMessageSearchForm) form;
			
			if(mapping.getParameter().equals("load")){
			}else if (mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				//System.out.println("==================TRYING TO GENERATE CSV>>>>>>>>>>");	
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_TEMPLATEMESSAGE );
				printHeader(ouputStream);
				
				Result resultList = (Result) session.getAttribute("templateMsg");
				for(int i=0;resultList != null&&i<resultList.getRecord().size();i++){
					Record aRecord=resultList.getRecord().get(i);
					for(int j=0;j<aRecord.getColumn().size();j++){
						ouputStream.print (println(aRecord.getColumn().get(j)));
					}
					ouputStream.println();
				}
				
				ouputStream.flush();
				
				ouputStream.close();
				
				return null;
			}else{
				ArrayList<String> mainQuery=new ArrayList<String>();
				ArrayList<String> conditionQuery=new ArrayList<String>();
				ArrayList<String> orderQuery=new ArrayList<String>();
				ArrayList<Object> queryParam=new ArrayList<Object>();
				
//				 Main query
				mainQuery.add(
						"select m.id, m.templateCode, m.templateContent, m.dynamicFieldNo, m.dynamicFieldName, m.createdBy, d.deptName, m.status " +
						"from MsgTemplate m, Department d ");
				
				// Condition criteria
				conditionQuery.add("m.department=d.id");

				if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT)){// By Message Code
					conditionQuery.add("d.deptName like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_MESSAGE_CODE)){// By Message Code
					conditionQuery.add("m.templateCode like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_MESSAGE_TEXT)){// By Message Text
					conditionQuery.add("m.templateContent like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_STATUS)){// By status
					conditionQuery.add("m.status like ?");
					queryParam.add(Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT);
				}else{// No matched search state, show all
					
				}
				
				if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL))){
					
				}else{
					if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT))){
						conditionQuery.add("m.department=?");
						queryParam.add(Integer.valueOf(CommonUtils.NullChecker(theLogonCredential.getUserGroup(), Integer.class).toString()));
					}else if(theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL))){
						conditionQuery.add("m.createdBy=?");
						queryParam.add(theLogonCredential.getUserId());
					}else{// Not allowed to perform any action
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
				}

				// Order criteria
				orderQuery.add("m.templateCode");
				result = DBUtilsCrsmsg.DBHQLCommand(DBUtilsCrsmsg.generateQuery(mainQuery, conditionQuery, orderQuery), queryParam.toArray());
				esform.setResultList(CommonUtils.populateResult(result));    
				session.setAttribute("templateMsg", CommonUtils.populateResult(result));
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_TEMPLATEMESSAGE_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_TEMPLATEMESSAGE_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		
		ouputStream.print (println("No."));
		ouputStream.print (println("Message Code"));
		ouputStream.print (println("Message Text"));
		ouputStream.print (println("Dynamic Fields"));
		ouputStream.print (println("Name of Fields"));
		ouputStream.print (println("Created By"));
		ouputStream.print (println("Department"));
		ouputStream.print (println("Status"));
		ouputStream.println();
		
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
	
}
