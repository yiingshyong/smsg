package com.bcb.crsmsg.actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.AvailableDB;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.UnSentLogForm;
import com.bcb.crsmsg.modal.ADSSynch;
import com.bcb.crsmsg.modal.SentLogModal;
import com.bcb.crsmsg.modal.Sms;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsDelete;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public class UnSentLog extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.UNSENT_LOG) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
				UnSentLogForm theForm =(UnSentLogForm) form;
				List group_result = DBUtilsCrsmsg.DBHQLCommand("from AvailableDB where availableDBStatus = ? ", new Object[]{Constants.STATUS_ACTIVE});
				ArrayList<String> group_label = new ArrayList<String>();
				ArrayList<String> group_value = new ArrayList<String>();
				for (int i =0 ; i < group_result.size(); i ++)
				{
					AvailableDB aGroup = (AvailableDB) group_result.get(i); 
					group_label.add(aGroup.getName());
					group_value.add(aGroup.getHibernateValue());		
				}
				theForm.setSearchbyserver_label(group_label);
				theForm.setSearchbyserver_value(group_value);
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)||
					mapping.getParameter().equals(Constants.PARAMETER_RESEND)){
				UnSentLogForm theForm =(UnSentLogForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());
				
				/* Define User Rights */
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				String user=Constants.STR_EMPTY;
				
				if (isAdmin){// not restricted by user, show all
					user=Constants.STR_EMPTY;
				}else if (isDept){// restricted by cases from users of same department
					user = getUserList(theLogonCredential.getUserId());
				}else if(isPersonal){// restricted by own cases
					user=Constants.SPECIAL_QUOTE_SINGLE+theLogonCredential.getUserId()+Constants.SPECIAL_QUOTE_SINGLE;
				}
				/* Define User Rights */
				
				/* Resend Records */
				if(theForm.getButApprove()!=null&&theForm.getButApprove().equals("Resend Selected Record(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					Session hibernate_session = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();
					
					try{
						String[] genSelBox = theForm.getSelectedMsg();
						
						SmsQueue  smsQ = new SmsQueue();
						BeanUtilsBean utilBean = new BeanUtilsBean();
						String status=Constants.STR_EMPTY;
						SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
						if(genSelBox != null && genSelBox.length != 0){
							List<Sms> smsList=DBUtilsCrsmsg.DBHQLCommand("from Sms where id in ("+CommonUtils.arrayToString(genSelBox, Constants.SPECIAL_CHAR_COMMA)+")", hibernate_session);
	//						smsList=checkConcatenateSms(smsList, theCfg, hibernate_session);
	//						smsList=CommonUtils.sortSms(smsList, theCfg);
							
							ArrayList<String> lastSmsIdList=new ArrayList<String>();
							String lastSmsId=Constants.STR_EMPTY;
							for(int i=0;i<smsList.size();i++){
								smsQ = new SmsQueue();
								smsList.get(i).setSmsStatus(Constants.SMS_STATUS_4);
								smsList.get(i).setSmsStatusBy(theLogonCredential.getUserId());
								smsList.get(i).setSmsStatusDatetime(new Date());
								utilBean.copyProperties((Object)smsQ, (Object)smsList.get(i));
								smsQ.setId(null);// let auto_increment generate new sms id to the sms
								
								if(DBUtilsCrsmsg.saveOrUpdate(smsQ, hibernate_session)){// save and get id first
									// Update concatenate sms indicator
									if(CommonUtils.NullChecker(smsQ.getConcatenateSms(), String.class).toString().length()==0){// is single sms
									}else{// is concatenate sms
										lastSmsIdList.add(CommonUtils.NullChecker(smsQ.getId(), String.class).toString());
										
										if(CommonUtils.NullChecker(smsQ.getConcatenateSms(), String.class).toString().equals(smsList.get(i).getId().toString())){// First concatenate sms
											smsQ.setConcatenateSms(smsQ.getId().toString());// assign new sms id
										}else if(CommonUtils.NullChecker(smsQ.getConcatenateSms(), String.class).toString().contains(theCfg.getTableListDelimiter())){// last concatenate sms
											smsQ.setConcatenateSms(CommonUtils.arrayToString(lastSmsIdList, theCfg.getTableListDelimiter()));
											
											lastSmsId=Constants.STR_EMPTY;// clear last sms id upon completed concatenate sms
											lastSmsIdList=new ArrayList<String>();// complete a concatenate sms, clear the list for next
										}else{// is middle concatenate sms
											smsQ.setConcatenateSms(lastSmsId);// assign the sms id before this sms
										}
									}
									
									if(DBUtilsCrsmsg.saveOrUpdate(smsQ, hibernate_session)){// update on the concatenate sms indicator
										ADSSynch aSynch=(ADSSynch)DBUtilsCrsmsg.get(ADSSynch.class, smsList.get(i).getId(), hibernate_session);
										if(aSynch!=null){// contain in ads synch, update id
											ADSSynch aNewSynch=new ADSSynch();
											utilBean.copyProperties(aNewSynch, aSynch);
											aNewSynch.setSmsId(smsQ.getId());
											if(DBUtilsCrsmsg.saveOrUpdate(aNewSynch, hibernate_session)){
												DBUtilsCrsmsg.delete(aSynch, hibernate_session);
											}
										}
										
										if(DBUtilsCrsmsg.delete(smsList.get(i), hibernate_session)){
											status=Constants.STATUS_SUCCESS;
											lastSmsId=CommonUtils.NullChecker(smsQ.getId(), String.class).toString();
										}else{// fail to delete sms
											status=Constants.STATUS_FAIL;
										}
									}else{
										status=Constants.STATUS_FAIL;
									}
								}else{// fail to save sms queue
									status=Constants.STATUS_FAIL;
								}
								
								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "UnSentLog Resend",smsQ.getId().toString() ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}
							if(status.equals(Constants.STATUS_SUCCESS)){
								DBUtilsCrsmsg.closeTransaction(hibernate_session);
								ActionMessages messages = new ActionMessages();
								messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_RESEND));
								this.addErrors(request,messages);
							}else{
								DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
								ActionMessages messages = new ActionMessages();
								messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_RESEND));
								this.addErrors(request,messages);
							}
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_NO_RECORD_SELECTED));
							this.addErrors(request,messages);							
						}
					}catch(Exception ex){
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_RESEND));
						this.addErrors(request,messages);
					}finally{
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
					}
				}
				/* Resend Records */
				
				/* Delete Records */
				if(theForm.getButDelete()!=null&& theForm.getButDelete().equals("Delete Record(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					Session hibernate_session = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();	
					
					try{
						String[] genSelBox = theForm.getSelectedMsg();
						Sms  sms = null;
						SmsDelete  smsDelete = new SmsDelete();
						BeanUtilsBean utilBean = new BeanUtilsBean();
						String status=Constants.STR_EMPTY;
						
						for(int i=0;genSelBox!=null&&i<genSelBox.length;i++){
							sms=(Sms)DBUtilsCrsmsg.get(Sms.class, Integer.parseInt(genSelBox[i]), hibernate_session);
							if (sms!=null){
								smsDelete = new SmsDelete();
								utilBean.copyProperties((Object)smsDelete, (Object)sms);
								
								if(DBUtilsCrsmsg.saveOrUpdate(smsDelete, hibernate_session)){
									if(DBUtilsCrsmsg.delete(sms, hibernate_session)){
										status=Constants.STATUS_SUCCESS;
									}else{// fail to delete smsReceived
										status=Constants.STATUS_FAIL;
									}
								}else{// fail to save smsReceivedDelete
									status=Constants.STATUS_FAIL;
								}
								
								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "UnSentLog Delete",smsDelete.getId().toString() ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}else{// Can't find the sms
							}
						}
						
						if(status.equals(Constants.STATUS_SUCCESS)){
							DBUtilsCrsmsg.closeTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}else{
							DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
							this.addErrors(request,messages);
						}
					}catch(Exception ex){
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}finally{
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
					}
				}
				/* Delete Records */
				
				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "mobile_no";						 
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SENDER)){
					queryParam = "sent_by";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_DEPARTMENT)){
					queryParam = "dept";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR)){
					queryParam = "telco_name";
					/*}else if ( sentLogForm.getSelectedState().equals(Constants.STR_BY_SEND_MODE)){
					 queryParam = "mode";*/
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SEND_TYPE)){
					queryParam = "type";
				}else{// Can't find match selected state
				}
				/* Define search selected state*/
				
				if(theForm.getButResendAll() != null && theForm.getButResendAll().equals("Resend All Record(s)")){
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					boolean resendSuccess = false;
					Session hibernateSession = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();
					Connection rawConn = hibernateSession.connection();
					try{
						rawConn.setAutoCommit(false);
						String whereClause = " where (s.sms_status_datetime between ? and ?) and s.sms_status = 'U'" +
						" and (UPPER("+ queryParam +") like ? "+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and s.sent_by in (" + user + ")":Constants.STR_EMPTY);
							
	
						StringBuffer sql = new StringBuffer();
						sql.append("update sms s set s.sms_status=?, sms_status_by=?, s.sms_status_datetime=?");
						sql.append(whereClause);
						
						log4jUtil.debug("Resend all record 1 stmt: " + sql.toString());
	
						String random = String.valueOf(new Random().nextInt(Integer.MAX_VALUE));
						Date currentDate = new Date();
						PreparedStatement stmt = rawConn.prepareStatement(sql.toString());
						int paramCount = 0;
						int updateCount = 0;
						int insertCount = 0;
						int deleteCount = 0;
						stmt.setString(++paramCount, Constants.SMS_STATUS_4);
						stmt.setString(++paramCount, random);						
						stmt.setTimestamp(++paramCount, new Timestamp(currentDate.getTime()));
						stmt.setTimestamp(++paramCount, new Timestamp(searchFromDate.getTime()));
						stmt.setTimestamp(++paramCount, new Timestamp(searchUntilDate.getTime()));
						stmt.setString(++paramCount, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT);
						if( (updateCount = stmt.executeUpdate()) > 0){
							
							sql = new StringBuffer();
							
							String columnNames = "sms_status_by, sms_id, version, mobile_no, message, sms_scheduled_time, remarks, sent_by, telco, priority, sms_status_datetime, type, ftp, created_by, created_datetime, approval_by, approval_datetime, sms_scheduled, msg_format_type, sms_status, dept, dept_id, modified_by, modified_datetime, sms_charge, sms_charge_mode, concatenate_sms, report_id, dnstatus, total_sms, channel_id, sms_type, client_timestamp";
							String valColumnNames = "'" + theLogonCredential.getUserId() + "', sms_id, version, mobile_no, message, sms_scheduled_time, remarks, sent_by, telco, priority, sms_status_datetime, type, ftp, created_by, created_datetime, approval_by, approval_datetime, sms_scheduled, msg_format_type, sms_status, dept, dept_id, modified_by, modified_datetime, sms_charge, sms_charge_mode, concatenate_sms, report_id, dnstatus, total_sms, channel_id, sms_type, client_timestamp";
							sql.append("insert into sms_queue (" + columnNames + ") select " + valColumnNames + " from sms where sms_status_datetime=? and sms_status_by=?");
							log4jUtil.debug("Resend all records insert stmt: " + sql.toString());
							stmt.close();
							stmt = rawConn.prepareStatement(sql.toString());
							paramCount = 0;
							stmt.setTimestamp(++paramCount, new Timestamp(currentDate.getTime()));
							stmt.setString(++paramCount, random);
							if( (insertCount = stmt.executeUpdate()) == updateCount){
								String deleteSql = "delete from sms where sms_status_datetime=? and sms_status_by=?";
								log4jUtil.debug("Resend all records delete stmt: " + deleteSql);
								paramCount = 0;
								stmt.close();
								stmt = rawConn.prepareStatement(deleteSql);
								stmt.setTimestamp(++paramCount, new Timestamp(currentDate.getTime()));
								stmt.setString(++paramCount, random);
								if( (deleteCount = stmt.executeUpdate()) == updateCount){
									stmt.close();
									rawConn.commit();	
									resendSuccess = true;									
								}else{
									log4jUtil.error("Deleted sms didn't match inserted sms. Inserted: " + insertCount + " Deleted: " + deleteCount);
								}
							}else{
								log4jUtil.error("Inserted sms didn't match updated sms. Updated: " + updateCount + " Inserted: " + insertCount);								
							}
						}else{
							log4jUtil.warn("no record updated");
						}
						if(!resendSuccess){
							rawConn.rollback();							
						}
						if(updateCount == 0){
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_RECORD_NOTFOUND_FOR, Constants.ACTION_PARA_RESEND));
							this.addErrors(request,messages);							
						}else{
							ActionMessages messages = new ActionMessages();
							messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(resendSuccess? Constants.ERRMSG_ACTION_SUCCESS : Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_RESEND));
							this.addErrors(request,messages);
						}
					}catch(Exception e){
						log4jUtil.error("Error while resend all records. Reason: " + e.getMessage(), e);
						rawConn.rollback();
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_RESEND));
						this.addErrors(request,messages);						
					}finally{
						if(hibernateSession.isOpen()){
							hibernateSession.close();
						}
					}

				}
				/* Get total no. of sms */
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(sms_id) from sms s left outer join telco t on s.telco=t.telco_id where (sms_status_datetime between ? and ?) and sms_status = 'U'"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY), 
						new String[]{}, 
						new NullableType[]{}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT},
						theForm.getSelectedServer()
						).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());
				
				sql.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time, t.telco_name, s.type, s.remarks, s.delivery_report_date, s.created_datetime, s.priority, s.sms_scheduled, s.approval_by, s.approval_datetime, s.channel_id");
				sql.append(" from sms s left outer join telco t on s.telco=t.telco_id" +
						" where (s.sms_status_datetime between ? and ?) and s.sms_status = 'U'"+
						" and (UPPER("+ queryParam +") like ? "+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and s.sent_by in (" + user + ")":Constants.STR_EMPTY));
//				sql.append(" order by s.mobile_no, s.created_datetime desc LIMIT "+offset+", "+pageSize);
				
				DBUtilsCrsmsg dbObj = new DBUtilsCrsmsg(sql.toString(), 
						new String[]{"s.sms_id", "s.sms_status_datetime", "s.mobile_no", "s.message", "s.sent_by", "s.dept", "s.sms_scheduled_time", "t.telco_name", "s.type", "s.remarks", "s.delivery_report_date", "s.created_datetime", "s.priority", "s.sms_scheduled", "s.approval_by", "s.approval_datetime", "s.channel_id"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.TIMESTAMP, Hibernate.INTEGER, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});
				
				resultList= DBUtilsCrsmsg.DBSQLCommand(
						dbObj.getCommand() + " order by s.created_datetime desc LIMIT "+offset+", "+pageSize,
						dbObj.getCmdParamName(), dbObj.getCmdType(), dbObj.getCmdParamObj(),
						theForm.getSelectedServer());
				
						
				/*while(offset>=resultList.size()&&offset!=0){
					offset-=pageSize;// back one page
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<SentLogModal> dataResult = new ArrayList<SentLogModal>();
					int runningCounter = 0;
					
					while (resultIter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) resultIter.next();	
						SentLogModal sentLog= new SentLogModal();
						sentLog.setId((Integer)rowResult[0]);
						sentLog.setSeq(String.valueOf(runningCounter));
						sentLog.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						sentLog.setMobileNo( (String) rowResult[2] );
						sentLog.setMessage( (String) rowResult[3] );
						sentLog.setSentBy( (String) rowResult[4] );
						sentLog.setDept((String) rowResult[5]  );
						sentLog.setScheduledStatus(CommonUtils.NullChecker(rowResult[13], String.class).toString() );
						if(sentLog.getScheduledStatus().equals(Constants.STATUS_YES))
							sentLog.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
						else
							sentLog.setScheduled(Constants.STR_EMPTY);
						sentLog.setMode(Constants.STR_EMPTY);
						sentLog.setOperator(CommonUtils.NullChecker(rowResult[7], String.class).toString());
						sentLog.setType((String) rowResult[8]  );
						sentLog.setRemarks((String) rowResult[9]  );
						sentLog.setDeliveryReportDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[10]));
						//sentLog.setDeliveryReportDate((String) rowResult[10]  );
						sentLog.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[11]));
						sentLog.setPriority(CommonUtils.NullChecker(rowResult[12], Integer.class).toString());
						sentLog.setApproval(CommonUtils.NullChecker(rowResult[14], String.class).toString());
						if(rowResult[15]!=null){
							sentLog.setApprovalDatetime(CommonUtils.NullChecker(rowResult[15], String.class).toString());
						}else{
							sentLog.setApprovalDatetime(Constants.STR_EMPTY);
						}
						sentLog.setChannelId(CommonUtils.NullChecker(rowResult[16], String.class).toString());
						dataResult.add(sentLog);
					}
					
					request.setAttribute(Constants.UNSENTLOG_RESULT_KEY, dataResult);
					session.setAttribute(Constants.DBOBJ, dbObj);
//					session.setAttribute(Constants.UNSENTLOG_KEY, dataResult);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
//				ArrayList dataList = (ArrayList) session.getAttribute(Constants.UNSENTLOG_KEY);
				DBUtilsCrsmsg dbObj = (DBUtilsCrsmsg) session.getAttribute(Constants.DBOBJ);
				response.reset();
				UnSentLogForm theForm =(UnSentLogForm) form;
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_UNSENTLOGREPORT );
				printHeader(ouputStream);
				Session hibernateSession = HibernateUtil.getSessionFactoryServer(theForm.getSelectedServer()).getCurrentSession();
				ScrollableResults resultList = DBUtilsCrsmsg.DBSQLScrollCommand(dbObj.getCommand(), dbObj.getCmdParamName(), dbObj.getCmdType(), dbObj.getCmdParamObj(), hibernateSession);				
				
				if(resultList != null){
					int runningCounter = 0;
					while (resultList.next()){
						runningCounter++;
						Object[] rowResult = resultList.get();
						ouputStream.print (CommonUtils.println(String.valueOf(runningCounter)));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1])));
						ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[11])));
						ouputStream.print (CommonUtils.println((String) rowResult[2]));
						ouputStream.print (CommonUtils.println((String) rowResult[3]));
						ouputStream.print (CommonUtils.println((String) rowResult[4]));
						ouputStream.print (CommonUtils.println((String) rowResult[16]));
						ouputStream.print (CommonUtils.println((String) rowResult[5]));
						ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[7], String.class).toString()));
						ouputStream.print (CommonUtils.println((String) rowResult[8]));
						ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[12], Integer.class).toString()));
						if(CommonUtils.NullChecker(rowResult[13], String.class).toString().equals(Constants.STATUS_YES) ){
							ouputStream.print (CommonUtils.println(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6])));							
						}else{
							ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
						}
						ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[14], String.class).toString()));
						if(rowResult[15]!=null){
							ouputStream.print (CommonUtils.println(CommonUtils.NullChecker(rowResult[15], String.class).toString()));
						}else{
							ouputStream.print (CommonUtils.println(Constants.STR_EMPTY));
						}						
						ouputStream.print (CommonUtils.println((String) rowResult[9]));												
						ouputStream.println();
					}
				}
				ouputStream.flush();
				
				ouputStream.close();
				return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_UNSENTLOG+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_UNSENTLOG,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	
	private String getUserList(String userId){
		ArrayList<String> userIdList=new ArrayList<String>();
		List prList_result =DBUtilsCrsmsg.DBHQLCommand("select userId from User where userGroup in (select userGroup from User u where userId = ?) ", new Object[]{userId});
		
		for(int i=0;i<prList_result.size();i++){
			userIdList.add(Constants.SPECIAL_QUOTE_SINGLE+prList_result.get(i).toString()+Constants.SPECIAL_QUOTE_SINGLE);
		}
		
		return CommonUtils.arrayToString(userIdList, Constants.SPECIAL_CHAR_COMMA);	
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
		ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Sent Date"));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Sent By"));
		ouputStream.print (CommonUtils.println("Channel Id"));
		ouputStream.print (CommonUtils.println("Dept"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Type"));
		ouputStream.print (CommonUtils.println("Priority"));
		ouputStream.print (CommonUtils.println("Scheduled Date"));
		ouputStream.print (CommonUtils.println("Approval By"));
		ouputStream.print (CommonUtils.println("Approval Date"));
		ouputStream.print (CommonUtils.println("Remarks"));
		ouputStream.println();
	}
	
	/**
	 * Filter out Sms which is concatenate sms and is not complete
	 * @param queueList
	 * @param theCfg
	 * @return queueList with non-concatenate and complete listing of concatenate Sms
	 */
	private List<Sms> checkConcatenateSms(List<Sms> queueList, SmsCfg theCfg, Session hibernate_session){
		List<Sms> newQueueList=new ArrayList<Sms>();
		List<String> concatenateList=new ArrayList<String>();
		List<String> concatenateCompleteList=new ArrayList<String>();
		List<String> smsIdList=new ArrayList<String>();
		Sms aSms=null;

		// populate smsIdList & concatenateList & concatenateCompleteList
		for(int i=0;i<queueList.size();i++){
			aSms=(Sms)queueList.get(i);
			
			if(CommonUtils.NullChecker(aSms.getConcatenateSms(), String.class).toString().length()>0){// is concatenate sms
				concatenateList.add(aSms.getId().toString());
				
				if(CommonUtils.NullChecker(aSms.getConcatenateSms(), String.class).toString().contains(theCfg.getTableListDelimiter())){// End of concatenate SMS
					concatenateCompleteList.add(aSms.getConcatenateSms());// collecting complete concatenate sms id				
				}else{// Start or mid of concatenate SMS
				}
			}else{
				// Normal sms, add into queue list
				smsIdList.add(aSms.getId().toString());
			}
		}

		for(int i=0;i<concatenateCompleteList.size();i++){
			List<String> completeConcatenate=CommonUtils.strToArray(concatenateCompleteList.get(i), theCfg.getTableListDelimiter());
			boolean complete=true;
			for(int j=0;j<completeConcatenate.size();j++){
				if(!concatenateList.contains(completeConcatenate.get(j))){
					complete=false;
				}
			}

			if(complete){// concatenate sms is complete, add into sms queue id list
				for(int j=0;j<completeConcatenate.size();j++){
					smsIdList.add(completeConcatenate.get(j));
				}
			}else{// not complete, skip
				
			}
		}

		// collecting single SMS or complete concatenate SMS
		for(int i=0;i<queueList.size();i++){
			aSms=(Sms)queueList.get(i);
			
			if(smsIdList.contains(aSms.getId().toString())){
				newQueueList.add(aSms);
			}else{// throw broken concatenate sms into unsent
				aSms.setSmsStatus(Constants.SMS_STATUS_2);
				aSms.setSmsStatusBy("checkConcatSms");
				aSms.setSmsStatusDatetime(new Date());
				aSms.setModifiedBy("checkConcatSms");
				aSms.setModifiedDatetime(new Date());
				aSms.setRemarks("Error: Broken concatenate sms");
				
				try{
					if(DBUtilsCrsmsg.saveOrUpdate(aSms, hibernate_session)){
					}else{
						
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}

		return newQueueList;
	}
}
