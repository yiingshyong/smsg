package com.bcb.crsmsg.actions;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.Department;
import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.forms.QuotaForm;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.modal.SmsQuota;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.crsmsg.util.log4jUtil;
public final class UpdateQuota extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null){
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.common.util.Constants.APPSID,com.bcb.common.util.Constants.DEPTMGMT_PERM) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			QuotaForm theForm = (QuotaForm) form;
			SmsCfg theCfg=(SmsCfg)DBUtilsCrsmsg.get(SmsCfg.class, Integer.valueOf(Constants.APPSVERSION), hibernate_session);
			String deptId=Constants.STR_EMPTY;
			for(int i=0;theForm.getCheckedRecords()!=null&&i<theForm.getCheckedRecords().length;i++){
				deptId=theForm.getCheckedRecords()[i];
				Department aDept=(Department)DBUtilsCrsmsg.get(Department.class, Integer.valueOf(deptId), hibernate_session);
				if(aDept==null){
					ActionMessages messages = new ActionMessages();
					messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_UPDATED));
					this.addErrors(request,messages);
				}else{
					List quotaList=DBUtilsCrsmsg.DBHQLCommand("from SmsQuota where ref=?", new Object[]{aDept.getId()}, hibernate_session);
					SmsQuota aQuota=new SmsQuota();
					if(quotaList.size()==0){
						aQuota.setRef(aDept.getId());
						aQuota.setCreatedBy(theLogonCredential.getUserId());
						aQuota.setCreatedDatetime(new Date());
					}else if(quotaList.size()==1){
						aQuota=(SmsQuota)quotaList.get(0);
						aQuota.setModifiedBy(theLogonCredential.getUserId());
						aQuota.setModifiedDatetime(new Date());
					}else{
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_UPDATED));
						this.addErrors(request,messages);
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
						return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
					}
					
					String quota=CommonUtils.NullChecker(theForm.getQuota(), Integer.class).toString().length()>0?theForm.getQuota():CommonUtils.NullChecker(theCfg.getSmsQuotaDefault(), Integer.class).toString().length()>0?theCfg.getSmsQuotaDefault():Constants.VALUE_0;
					if(theForm.getTopUpMode().equals(Constants.FORM_FIELD_QUOTA_TOPUP_MODE_TOPUP)){// topup
						aQuota.setQuota(Integer.valueOf(quota)+aQuota.getQuota());
						aQuota.setTotalQuota(Integer.valueOf(quota)+aQuota.getTotalQuota());
					}else if(theForm.getTopUpMode().equals(Constants.FORM_FIELD_QUOTA_TOPUP_MODE_RESET)){// reset
						aQuota.setTotalQuota(aQuota.getTotalQuota()-aQuota.getQuota()+Integer.valueOf(quota));
						aQuota.setQuota(Integer.valueOf(quota));
					}else{// no option selected
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_UPDATED));
						this.addErrors(request,messages);
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
						return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
					}
					
					if(DBUtilsCrsmsg.saveOrUpdate(aQuota, hibernate_session)){
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),Constants.APPSNAME,Constants.ACTION_QUOTA_UPDATE,aQuota.getId().toString(),Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_UPDATED));
						this.addErrors(request,messages);
					}else{
						SmsAuditTrail anAuditTrail = new SmsAuditTrail();
						anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),Constants.APPSNAME,Constants.ACTION_QUOTA_UPDATE,aQuota.getId().toString(),Constants.STATUS_FAIL,theLogonCredential.getBranchCode());
						DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ERRMSG_PARAMETERS_RECORD, Constants.ACTION_PARA_UPDATED));
						this.addErrors(request,messages);
					}
				}
			}
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
		}catch (Exception E){
			log4jUtil.error(Constants.ACTION_QUOTA_UPDATE+" System Error[" + E.getMessage()+ "]");
			E.printStackTrace();
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_QUOTA_UPDATE,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
}
