

package com.bcb.crsmsg.actions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.UserSearchForm;
import com.bcb.crsmsg.modal.Record;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class UserSearch extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.USER_SEARCH) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}

		try{
			List result = null; 
			UserSearchForm esform =  (UserSearchForm) form;
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){
				//System.out.println("==================TRYING TO GENERATE CSV>>>>>>>>>>");	
				response.reset();
				
				ServletOutputStream ouputStream = response.getOutputStream();
				
				//--------------------Generate Report---------------
				response.setContentType(Constants.CONTENTTYPE_CSV);
				response.setHeader(Constants.HEADER,
						Constants.STR_FILENAME +
						Constants.CSVFILENAME_USERREPORT );
				printHeader(ouputStream);
				
				Result resultList = (Result) session.getAttribute("userSearchData");
				
				for(int i=0;resultList != null&&i<resultList.getRecord().size();i++){
					Record aRecord=resultList.getRecord().get(i);
					for(int j=0;j<aRecord.getColumn().size();j++){
						//Print No for each record
						if(j == 0)
							ouputStream.print (println((i+1) + ""));
						ouputStream.print (println(aRecord.getColumn().get(j)));
					}
					ouputStream.println();
				}
				
				ouputStream.flush();
				
				ouputStream.close();
				
				return null;
			}else{
				Integer appsId = Integer.valueOf(Constants.APPSID);
				StringBuffer query=new StringBuffer();
				String queryField=Constants.STR_EMPTY;
				
				if ( esform.getSelectedState().equals(Constants.FORM_FIELD_USER_ID)){// By User ID
					queryField="u.userId";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_NAME)){// By User Name
					queryField="u.userName";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_ROLE)){// By Role
					queryField="r.roleName";
				}else if (esform.getSelectedState().equals(Constants.FORM_FIELD_USER_DEPARTMENT)){// By Department
					queryField="d.deptName";            		  
				}else{// No matched search state
					
				}
				
				query.append(
						"select u.userId,u.userName,r.roleName,d.deptName,u.createdDate,u.lastLoginDate,u.userStatus "+
						"from User u , Apps a, RoleApps ra, UserRoles ur, Department d, RoleRef r " +
						"where "+queryField+" like ? and a.appsId = ? and ur.status=? " +
						"and ur.roleId = ra.roleId and ur.userId=u.userId and ra.appsId = a.appsId and d.id = u.userGroup and r.roleId=ur.roleId "+
						"order by u.userId, u.userName, r.roleName, d.deptName");
				result = DBUtilsCrsmsg.DBHQLCommand(query.toString(), new Object[]{
					Constants.STR_PERCENT + esform.getSearchkey() + Constants.STR_PERCENT,
					appsId,
					Constants.STATUS_ACTIVE
					});
				
				ArrayList<Object[]> userIdList=new ArrayList<Object[]>();
				
				for(int i=0, j=0;i<result.size();i++){
					Object[] user=(Object[])result.get(i);
					if(userIdList.size()>0){
						if(userIdList.get(j)[0].equals(user[0])){// same user id
							userIdList.get(j)[2]=userIdList.get(j)[2]+Constants.SPECIAL_CHAR_COMMA+user[2];
						}else{// not same id, add into new list
							userIdList.add(user);
							j++;
						}
					}else{// insert initial user record entry
						userIdList.add((Object[])result.get(i));
					}
				}
				esform.setResultList(CommonUtils.populateResult(userIdList));  
				session.setAttribute("userSearchData", CommonUtils.populateResult(userIdList));
			}
		}
		catch (Exception E)
		{
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_USER_SEARCH+" System Error[" + E.getMessage()+ "]");
			
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_USER_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No"));
		ouputStream.print (CommonUtils.println("User"));
		ouputStream.print (CommonUtils.println("Name"));
		ouputStream.print (CommonUtils.println("Role"));
		ouputStream.print (CommonUtils.println("Department"));
		ouputStream.print (CommonUtils.println("Creation Date"));
		ouputStream.print (CommonUtils.println("Last Login Date"));
		ouputStream.print (CommonUtils.println("Status"));
		ouputStream.println();
	}
	
	private String println(String printData){
		if (printData != null){
			return Constants.CSV_STRSTART + printData + Constants.CSV_STREND;
		}
		return Constants.CSV_STREMPTY;
	}
}
