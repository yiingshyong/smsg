

package com.bcb.crsmsg.actions;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.CustomerForm;
import com.bcb.crsmsg.modal.Customer;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
public final class addCustomer extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
		if ( theLogonCredential == null)
		{
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
			this.addErrors(request,messages);
			return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			
		}
		
		if ( theLogonCredential.allowed(com.bcb.crsmsg.util.Constants.APPSID,Constants.CUSTOMER_MAINTENANCE) == false){
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		//View Only User Role
		if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
			return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
		}
		
		if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){
		}else{	
			Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					
			try{
			CustomerForm custForm = (CustomerForm) form;
			
			
			if(custForm.getSubmitBtn()!=null){
				
				Customer cust = new Customer();
				cust.setCustRefNo(custForm.getCustRefNo());
				cust.setMobileNo(custForm.getMobileNo());
				cust.setSmsStatus(custForm.getSelected_status());
				cust.setSmsStatusBy(theLogonCredential.getUserId());
				cust.setSmsStatusDatetime(new Date());
				cust.setCreatedBy(theLogonCredential.getUserId());
				cust.setCreatedDatetime(new Date());
				cust.setTransferStatus("0");
				
				DBUtilsCrsmsg.saveOrUpdate(cust, hibernate_session);
				
				SmsAuditTrail anAuditTrail = new SmsAuditTrail();
				anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME,com.bcb.crsmsg.util.Constants.ACTION_CONTACT_SAVE_ADD, String.valueOf(cust.getId()) ,Constants.STATUS_SUCCESS,theLogonCredential.getBranchCode());
				DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							
			}			
			
			ActionMessages messages = new ActionMessages();
			messages.add("ftpReport",new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_SAVED));
			this.addErrors(request,messages);
			
			DBUtilsCrsmsg.closeTransaction(hibernate_session);
			
			}catch (Exception E){
				
				E.printStackTrace();
				log4jUtil.error(Constants.ACTION_CUSTOMER_SEARCH+" System Error[" + E.getMessage()+ "]");
			
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_CUSTOMER_SEARCH,new ActionMessage(Constants.ERRMSG_SYSTEM));
				this.addErrors(request,messages);
				
				DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
			}
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
		
		
	}	
	
}
