package com.bcb.crsmsg.actions;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.type.NullableType;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.DateUtil;
import com.bcb.common.util.HibernateUtil;
import com.bcb.common.util.log4jUtil;
import com.bcb.crsmsg.forms.PendingApprovalForm;
import com.bcb.crsmsg.modal.SentLogModal;
import com.bcb.crsmsg.modal.SmsAuditTrail;
import com.bcb.crsmsg.modal.SmsDelete;
import com.bcb.crsmsg.modal.SmsQueue;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;

public class pendingApproval extends Action {
	
	public  ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response)
	throws Exception {
		try{
			HttpSession session = request.getSession();
			LogonCredential theLogonCredential= (LogonCredential) session.getAttribute(com.bcb.common.util.Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null){
				ActionMessages messages = new ActionMessages();
				messages.add(Constants.ERROR_LOGONACTION,new ActionMessage(Constants.ERRMSG_NOTLOGON));
				this.addErrors(request,messages);
				return (mapping.findForward(Constants.MAPPINGVALUE_NOTLOGON));
			}
			
			//Check if having appropriate permission
			if ( theLogonCredential.allowed(Constants.APPSID,Constants.PENDING_APPROVAL_LOG) == false){
				return (mapping.findForward(com.bcb.common.util.Constants.MAPPINGVALUE_ACCDENY));
			}
			
			if(mapping.getParameter().equals(Constants.PARAMETER_LOAD)){// initial load page
			}else if(mapping.getParameter().equals(Constants.PARAMETER_SEARCH)||
					mapping.getParameter().equals(Constants.PARAMETER_APPROVE)){
				PendingApprovalForm theForm =(PendingApprovalForm) form;
				SimpleDateFormat theFormatter=new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
				Date searchFromDate= theForm.getSelectedFromDate().length()==0?new Date(0):theFormatter.parse(theForm.getSelectedFromDate());
				Date searchUntilDate= theForm.getSelectedUntilDate().length()==0?new Date():theFormatter.parse(theForm.getSelectedUntilDate());
				
				/* Define User Rights */
				boolean isAdmin = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_ALL));
				boolean isDept = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_DEPARTMENT));
				boolean isPersonal = theLogonCredential.getRights().contains(Integer.valueOf(Constants.ACTION_PERSONAL));
				String user=Constants.STR_EMPTY;
				
				if (isAdmin){// not restricted by user, show all
					user=Constants.STR_EMPTY;
				}else if (isDept){// restricted by cases from users of same department
					user = getUserList(theLogonCredential.getUserId());
				}else if(isPersonal){// restricted by own cases
					user=Constants.SPECIAL_QUOTE_SINGLE+theLogonCredential.getUserId()+Constants.SPECIAL_QUOTE_SINGLE;
				}
				/* Define User Rights */
				
				/* Approve Records */
				if(theForm.getButApprove()!=null&&theForm.getButApprove().equals("Approve Record(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					String[] genSelBox = theForm.getSelectedMsg();
					SmsQueue  smsQ = null;
					String status=Constants.STR_EMPTY;
					
					Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					for(int i=0;i<genSelBox.length;i++){
						smsQ=(SmsQueue)DBUtilsCrsmsg.get(SmsQueue.class, Integer.parseInt(genSelBox[i]), hibernate_session);
						if (smsQ!=null){
							if(smsQ.getSmsStatus().equals(Constants.SMS_STATUS_3)){
								if(CommonUtils.NullChecker(smsQ.getSmsScheduled(), String.class).toString().equals(Constants.STATUS_YES)){
									smsQ.setSmsStatus(Constants.SMS_STATUS_6);
								}else{
									smsQ.setSmsStatus(Constants.SMS_STATUS_4);
								}
								smsQ.setSmsStatusBy(theLogonCredential.getUserId());
								smsQ.setSmsStatusDatetime(new Date());
								smsQ.setApprovalBy(theLogonCredential.getUserId());
								smsQ.setApprovalDatetime(new Date());
								smsQ.setModifiedBy(theLogonCredential.getUserId());
								smsQ.setModifiedDatetime(new Date());
								
								if(DBUtilsCrsmsg.saveOrUpdate(smsQ, hibernate_session)){
									status=Constants.STATUS_SUCCESS;
								}else{// fail to save sms queue
									status=Constants.STATUS_FAIL;
								}
								
								SmsAuditTrail anAuditTrail = new SmsAuditTrail();
								anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "pendingApproval Approve",smsQ.getId().toString() ,status,theLogonCredential.getBranchCode());
								DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
							}else{
							}
						}else{// Can't find the sms
						}
					}
					
					if(status.equals(Constants.STATUS_SUCCESS)){
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_PENDINGAPPROVAL,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_APPROVED));
						this.addErrors(request,messages);
					}else{
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_PENDINGAPPROVAL,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_APPROVED));
						this.addErrors(request,messages);
					}
				}
				/* Approve Records */
				
				/* Delete Records */
				if(theForm.getButDelete()!=null&& theForm.getButDelete().equals("Delete Record(s)")){
					//View Only User Role
					if(session.getAttribute(com.bcb.common.util.Constants.VIEW_ONLY_KEY) != null) {
						return (mapping.findForward(Constants.MAPPINGVALUE_ACCDENY));
					}
					
					String[] genSelBox = theForm.getSelectedMsg();
					SmsQueue  sms = null;
					SmsDelete  smsDelete = new SmsDelete();
					BeanUtilsBean utilBean = new BeanUtilsBean();
					String status=Constants.STR_EMPTY;
					
					Session hibernate_session = HibernateUtil.getSessionFactory().getCurrentSession();	
					for(int i=0;i<genSelBox.length;i++){
						sms=(SmsQueue)DBUtilsCrsmsg.get(SmsQueue.class, Integer.parseInt(genSelBox[i]), hibernate_session);
						if (sms!=null){
							smsDelete = new SmsDelete();
							utilBean.copyProperties((Object)smsDelete, (Object)sms);
							
							if(DBUtilsCrsmsg.saveOrUpdate(smsDelete, hibernate_session)){
								if(DBUtilsCrsmsg.delete(sms, hibernate_session)){
									status=Constants.STATUS_SUCCESS;
								}else{// fail to delete sms queue
									status=Constants.STATUS_FAIL;
								}
							}else{// fail to save sms queue
								status=Constants.STATUS_FAIL;
							}
							
							SmsAuditTrail anAuditTrail = new SmsAuditTrail();
							anAuditTrail.setAudit(theLogonCredential.getUserId(),request.getRemoteAddr(),com.bcb.crsmsg.util.Constants.APPSNAME, "UnSentLog Delete",smsDelete.getId().toString() ,status,theLogonCredential.getBranchCode());
							DBUtilsCrsmsg.saveOrUpdate(anAuditTrail, hibernate_session);
						}else{// Can't find the sms
						}
					}
					
					if(status.equals(Constants.STATUS_SUCCESS)){
						DBUtilsCrsmsg.closeTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_PENDINGAPPROVAL,new ActionMessage(Constants.ERRMSG_ACTION_SUCCESS, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}else{
						DBUtilsCrsmsg.rollbackTransaction(hibernate_session);
						ActionMessages messages = new ActionMessages();
						messages.add(Constants.ERROR_PENDINGAPPROVAL,new ActionMessage(Constants.ERRMSG_ACTION_FAIL, Constants.ACTION_PARA_RECORD, Constants.ACTION_PARA_DELETED));
						this.addErrors(request,messages);
					}
				}
				/* Delete Records */
				
				/* Define search selected state*/
				String queryParam = Constants.STR_EMPTY;
				if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE)){
					queryParam = "mobile_no";						 
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SENDER)){
					queryParam = "sent_by";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_DEPARTMENT)){
					queryParam = "dept";
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_MOBILE_OPERATOR)){
					queryParam = "telco_name";
					/*}else if ( sentLogForm.getSelectedState().equals(Constants.STR_BY_SEND_MODE)){
					 queryParam = "mode";*/
				}else if ( theForm.getSelectedState().equals(Constants.STR_BY_SEND_TYPE)){
					queryParam = "type";
				}else{// Can't find match selected state
				}
				/* Define search selected state*/
				
				/* Get total no. of sms */
				String smsCount=CommonUtils.NullChecker(DBUtilsCrsmsg.DBSQLCommand("select count(sms_id) from sms_queue s left outer join telco t on s.telco=t.telco_id where (sms_status_datetime between ? and ?) and sms_status = 'A'"+
						" and (UPPER("+ queryParam +") like ?"+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and sent_by in (" + user + ")":Constants.STR_EMPTY), 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT}).get(0), Integer.class).toString();
				request.setAttribute(Constants.STR_COUNT, Integer.valueOf(smsCount));
				/* Get total no. of sms */
				
				/* search record */
				StringBuffer sql = new StringBuffer();
				List resultList=new ArrayList();
				int pageSize=20;
				Integer offset = Integer.valueOf(CommonUtils.NullChecker(request.getParameter(Constants.STR_PAGER_OFFSET), Integer.class).toString());
				
				sql.append("select s.sms_id, s.sms_status_datetime, s.mobile_no, s.message, s.sent_by, s.dept, s.sms_scheduled_time, s.type, s.remarks, s.created_datetime, s.priority, t.telco_name, s.approval_by, s.sms_scheduled, s.approval_datetime");
				sql.append(" from sms_queue s left outer join telco t on s.telco=t.telco_id" +
						" where (s.sms_status_datetime between ? and ?) and s.sms_status = 'A'"+
						" and (UPPER("+ queryParam +") like ? "+(theForm.getSearchkey().length()==0?" or "+queryParam+" is null":Constants.STR_EMPTY)+")"+
						(user.length()>0?" and s.sent_by in (" + user + ")":Constants.STR_EMPTY));
				sql.append(" order by s.sms_status_datetime desc LIMIT "+offset+", "+pageSize);
				
				resultList= DBUtilsCrsmsg.DBSQLCommand(
						sql.toString(),
						new String[]{"s.sms_id", "s.sms_status_datetime", "s.mobile_no", "s.message", "s.sent_by", "s.dept", "s.sms_scheduled_time", "s.type", "s.remarks", "s.created_datetime", "s.priority", "t.telco_name", "s.approval_by", "s.sms_scheduled", "s.approval_datetime"}, 
						new NullableType[]{Hibernate.INTEGER, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP, Hibernate.INTEGER, Hibernate.STRING, Hibernate.STRING, Hibernate.STRING, Hibernate.TIMESTAMP}, 
						new Object[]{searchFromDate, searchUntilDate, Constants.STR_PERCENT+theForm.getSearchkey().toUpperCase()+Constants.STR_PERCENT});
				/*while(offset>=resultList.size()&&offset!=0){
					offset-=pageSize;// back one page
				}*/
				//resultList=resultList.subList(offset, (offset+pageSize)>resultList.size()?resultList.size():offset+pageSize);
				/* search record */
				
				// Populate results set into modal
				if (resultList !=null && resultList.size() > 0){
					Iterator resultIter = resultList.iterator();
					ArrayList<SentLogModal> dataResult = new ArrayList<SentLogModal>();
					int runningCounter = 0;
	
					while (resultIter.hasNext()){
						runningCounter += 1;
						Object[] rowResult = (Object[]) resultIter.next();	
						SentLogModal sentLog= new SentLogModal();
						sentLog.setId(((Integer)rowResult[0]));
						sentLog.setSeq(String.valueOf(runningCounter));
						sentLog.setDate( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[1]));
						sentLog.setMobileNo( (String) rowResult[2] );
						sentLog.setMessage( (String) rowResult[3] );
						sentLog.setSentBy( (String) rowResult[4] );
						sentLog.setDept((String) rowResult[5]  );
						sentLog.setScheduledStatus(CommonUtils.NullChecker(rowResult[13], String.class).toString() );
						if(sentLog.getScheduledStatus().equals(Constants.STATUS_YES))
							sentLog.setScheduled( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[6]));
						else
							sentLog.setScheduled(Constants.STR_EMPTY);
						sentLog.setMode(Constants.STR_EMPTY);
						sentLog.setType((String) rowResult[7]  );
						sentLog.setRemarks((String) rowResult[8]);
						sentLog.setDeliveryReportDate(Constants.STR_EMPTY);
						sentLog.setCreated( DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[9]));
						sentLog.setPriority(CommonUtils.NullChecker(rowResult[10], Integer.class).toString());
						sentLog.setOperator(CommonUtils.NullChecker(rowResult[11], String.class).toString());
						sentLog.setApproval((String) rowResult[12]  );
						if(rowResult[14]!=null)
							sentLog.setApprovalDatetime(DateUtil.getInstance().stringByDayMonthYear((Timestamp) rowResult[14]) );
						else
							sentLog.setApprovalDatetime(Constants.STR_EMPTY);
					    dataResult.add(sentLog);
					}
					
					request.setAttribute(Constants.PENDINGLOG_RESULT_KEY, dataResult);
					session.setAttribute(Constants.PENDINGAPPROVAL_KEY, dataResult);
				}
			}else if(mapping.getParameter().equals(Constants.PARAMETER_GENERATECVS)){// generate cvs file
				ArrayList dataList = (ArrayList) session.getAttribute(Constants.PENDINGAPPROVAL_KEY);
				response.reset();
				
	            ServletOutputStream ouputStream = response.getOutputStream();
	                
	                 //--------------------Generate Report---------------
	     		response.setContentType(Constants.CONTENTTYPE_CSV);
	     		response.setHeader(Constants.HEADER,
	                     Constants.STR_FILENAME +
	                     Constants.CSVFILENAME_PENDINGAPPLOGREPORT );
	     		printHeader(ouputStream);
	     		
	     		if (dataList != null){
		     		Iterator iter = dataList.iterator();
		     		
		     		while (iter.hasNext()){
		     			SentLogModal model = (SentLogModal) iter.next();
		     			 
		      			ouputStream.print (CommonUtils.println(model.getSeq()));
		      			ouputStream.print (CommonUtils.println(model.getDate()));
		      			ouputStream.print (CommonUtils.println(model.getCreated()));
		      			ouputStream.print (CommonUtils.println(model.getMobileNo()));
		      			ouputStream.print (CommonUtils.println(model.getMessage()));
		      			ouputStream.print (CommonUtils.println(model.getSentBy()));
		      			ouputStream.print (CommonUtils.println(model.getApproval()));
		      			ouputStream.print (CommonUtils.println(model.getApprovalDatetime()));
		      			ouputStream.print (CommonUtils.println(model.getDept()));
		      			ouputStream.print (CommonUtils.println(model.getOperator()));
		      			ouputStream.print (CommonUtils.println(model.getPriority()));
		      			ouputStream.print (CommonUtils.println(model.getType()));
		      			ouputStream.print (CommonUtils.println(model.getScheduled()));
		     			ouputStream.println();
		     		}
	     		}
	     		ouputStream.flush();
                
                ouputStream.close();
                return null;
			}else{// no parameters found
			}
		}catch(Exception E){
			E.printStackTrace();
			log4jUtil.error(Constants.ACTION_PENDINGAPPROVAL+" System Error[" + E.getMessage()+ "]");
			ActionMessages messages = new ActionMessages();
			messages.add(Constants.ERROR_PENDINGAPPROVAL,new ActionMessage(Constants.ERRMSG_SYSTEM));
			this.addErrors(request,messages);
		}
		return (mapping.findForward(Constants.MAPPINGVALUE_SUCCESS));
	}
	
	private String getUserList(String userId){
		ArrayList<String> userIdList=new ArrayList<String>();
		List prList_result =DBUtilsCrsmsg.DBHQLCommand("select userId from User where userGroup in (select userGroup from User u where userId = ?) ", new Object[]{userId});
		
		for(int i=0;i<prList_result.size();i++){
			userIdList.add(Constants.SPECIAL_QUOTE_SINGLE+prList_result.get(i).toString()+Constants.SPECIAL_QUOTE_SINGLE);
		}
		
		return CommonUtils.arrayToString(userIdList, Constants.SPECIAL_CHAR_COMMA);	
	}
	
	private void printHeader(ServletOutputStream ouputStream ) throws Exception{
    	ouputStream.print (CommonUtils.println("No."));
		ouputStream.print (CommonUtils.println("Sent Date"));
		ouputStream.print (CommonUtils.println("Created Date"));
		ouputStream.print (CommonUtils.println("Mobile No."));
		ouputStream.print (CommonUtils.println("Message"));
		ouputStream.print (CommonUtils.println("Sent By"));
		ouputStream.print (CommonUtils.println("Approval"));
		ouputStream.print (CommonUtils.println("Approval Date"));
		ouputStream.print (CommonUtils.println("Dept"));
		ouputStream.print (CommonUtils.println("Telco"));
		ouputStream.print (CommonUtils.println("Priority"));
		ouputStream.print (CommonUtils.println("Type"));
		ouputStream.print (CommonUtils.println("Scheduled Date"));
		ouputStream.println();
	}
}
