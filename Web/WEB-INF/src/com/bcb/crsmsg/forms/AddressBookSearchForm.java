package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.Constants;

public class AddressBookSearchForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8523188947066140410L;
	private String searchkey;
	private ArrayList<String> searchby_label=new ArrayList<String>();
	private ArrayList<String> searchby_value=new ArrayList<String>();
	private String selectedState;	
	private Result resultList;
	private String[] checkedRecords;
	private boolean allowExport;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		searchby_label = new ArrayList<String>();
		searchby_label.add(Constants.FORM_FIELD_USER_NAME);
		searchby_label.add(Constants.FORM_FIELD_MOBILE_NO);
		searchby_label.add(Constants.FORM_FIELD_DESIGNATION);
		searchby_label.add(Constants.FORM_FIELD_USER_DEPARTMENT_NAME);
		searchby_label.add(Constants.FORM_FIELD_CUST_REF_NO);
//		searchby_label.add(Constants.FORM_FIELD_TYPE);
		
		searchby_value = new ArrayList<String>();
		searchby_value.add(Constants.FORM_FIELD_USER_NAME);
		searchby_value.add(Constants.FORM_FIELD_MOBILE_NO);
		searchby_value.add(Constants.FORM_FIELD_DESIGNATION);
		searchby_value.add(Constants.FORM_FIELD_USER_DEPARTMENT_NAME);
		searchby_value.add(Constants.FORM_FIELD_CUST_REF_NO);
//		searchby_value.add(Constants.FORM_FIELD_TYPE);
		
		selectedState = Constants.STR_EMPTY;
		searchkey=Constants.STR_EMPTY;
		//resultList=null;
		checkedRecords=null;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(selectedState.equals(Constants.FORM_FIELD_USER_NAME)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_100)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USER_NAME, Constants.LENGTH_100 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_MOBILE_NO)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MOBILE_NO, Constants.LENGTH_20 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_DESIGNATION)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_DESIGNATION, Constants.LENGTH_50 ));			
			}
		}else if (selectedState.equals(Constants.FORM_FIELD_USER_DEPARTMENT_NAME)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USER_DEPARTMENT_NAME, Constants.LENGTH_50 ));			
			}
		}else if (selectedState.equals(Constants.FORM_FIELD_CUST_REF_NO)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_16)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_CUST_REF_NO, Constants.LENGTH_16 ));			
			}
		}else if (selectedState.equals(Constants.FORM_FIELD_TYPE)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_20)){
				errors.add(Constants.FORM_USER_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TYPE, Constants.LENGTH_20 ));			
			}
		}
		
		return (errors);
		
	}

	/**
	 * @return Returns the searchby_label.
	 */
	public ArrayList<String> getSearchby_label() {
		return searchby_label;
	}

	/**
	 * @param searchby_label The searchby_label to set.
	 */
	public void setSearchby_label(ArrayList<String> searchby_label) {
		this.searchby_label = searchby_label;
	}

	/**
	 * @return Returns the searchby_value.
	 */
	public ArrayList<String> getSearchby_value() {
		return searchby_value;
	}

	/**
	 * @param searchby_value The searchby_value to set.
	 */
	public void setSearchby_value(ArrayList<String> searchby_value) {
		this.searchby_value = searchby_value;
	}

	/**
	 * @return Returns the searchkey.
	 */
	public String getSearchkey() {
		return searchkey;
	}

	/**
	 * @param searchkey The searchkey to set.
	 */
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	/**
	 * @return Returns the selectedState.
	 */
	public String getSelectedState() {
		return selectedState;
	}

	/**
	 * @param selectedState The selectedState to set.
	 */
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	/**
	 * @return Returns the resultList.
	 */
	public Result getResultList() {
		return resultList;
	}

	/**
	 * @param resultList The resultList to set.
	 */
	public void setResultList(Result resultList) {
		this.resultList = resultList;
	}

	/**
	 * @return Returns the checkedRecords.
	 */
	public String[] getCheckedRecords() {
		return checkedRecords;
	}

	/**
	 * @param checkedRecords The checkedRecords to set.
	 */
	public void setCheckedRecords(String[] checkedRecords) {
		this.checkedRecords = checkedRecords;
	}

	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public boolean isAllowExport() {
		return allowExport;
	}

	public void setAllowExport(boolean allowExport) {
		this.allowExport = allowExport;
	}
}
