package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;



public final class AutoActionForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4851048155675524960L;
	
	private Integer id;
	private String submitBtn;
	private String actionClassName;
	private Integer actionSequence;
	private Integer keyword;
	private String defaultAction;
	private String serverIP;
	private String serverPort;
	private String serverResponseTimeout;
	private String maxFailureAttempt;
	private String svcWindow;
	private String successCode;
	private String failureCode;
	private String dataAge; 
	private String description; 
	private String unavailabilitySms;
	private String parameterKey;
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;

	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
				
		id=null;
		unavailabilitySms=null;
		parameterKey=null;
		actionClassName=null;
		actionSequence=null;
		keyword=null;
		serverIP=null;
		serverPort=null;
		serverResponseTimeout=null;
		maxFailureAttempt=null;
		svcWindow=null;
		successCode=null;
		failureCode=null;
		dataAge=null; 
		description = null;
		unavailabilitySms=null;
		defaultAction=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_NO);
		status_label.add(Constants.FORM_FIELD_STATUS_YES);

		status_value = new ArrayList<String>();
		status_value.add("N");
		status_value.add("Y");
		

	}


	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if ( actionClassName!=null&&actionClassName.length() == 0 )
			errors.add(Constants.FORM_AUTOACTION,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Action Class"));
		
		if ( actionSequence!=null&&actionSequence <0 )
			errors.add(Constants.FORM_AUTOACTION,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Action Sequence"));
		
		if (serverPort!=null&&serverPort.length()>0){
			if(!serverPort.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_AUTOACTION,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SERVER_PORT, serverPort));
			}
		}else{
			serverPort="0";
		}
		
		if (serverResponseTimeout!=null&&serverResponseTimeout.length()>0){
			if(!serverResponseTimeout.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_AUTOACTION,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SERVER_TIMEOUT, serverResponseTimeout));
			}
		}else{
			serverResponseTimeout="0";
		}
		
		if (maxFailureAttempt!=null&&maxFailureAttempt.length()>0){
			if(!maxFailureAttempt.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_AUTOACTION,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_FAILURE_ATTEMPT, maxFailureAttempt));
			}
		}else{
			maxFailureAttempt="0";
		}

		if(svcWindow == null){
			svcWindow="0000:0000";
		}else if (svcWindow!=null&&svcWindow.length()>0){
			if(svcWindow.length()!= 9){ 
				errors.add(Constants.FORM_AUTOACTION,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, svcWindow));
			}else{ 
				String ccaSvcWindow1 = svcWindow.substring(0, 4);
				String ccaSvcWindowDot = svcWindow.substring(4, 5);
				String ccaSvcWindow2 = svcWindow.substring(5, 9);
				if (!ccaSvcWindow1.matches("^\\d{1,15}$")){ 
					errors.add(Constants.FORM_AUTOACTION,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, svcWindow));
				}else if (!ccaSvcWindow2.matches("^\\d{1,15}$")){ 
					errors.add(Constants.FORM_AUTOACTION,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, svcWindow));
				}else if(!ccaSvcWindowDot.equals(":"))	{
					errors.add(Constants.FORM_AUTOACTION,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, svcWindow));
				}
			}
		}
		
		return (errors);
		
	}


	public String getSubmitBtn() {
		return submitBtn;
	}


	public void setSubmitBtn(String submitBtn) {
		this.submitBtn = submitBtn;
	}


	public String getUnavailabilitySms() {
		return unavailabilitySms;
	}


	public void setUnavailabilitySms(String unavailabilitySms) {
		this.unavailabilitySms = unavailabilitySms;
	}


	public String getParameterKey() {
		return parameterKey;
	}


	public void setParameterKey(String parameterKey) {
		this.parameterKey = parameterKey;
	}


	public String getSelected_status() {
		return selected_status;
	}


	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	public ArrayList<String> getStatus_label() {
		return status_label;
	}


	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}


	public ArrayList<String> getStatus_value() {
		return status_value;
	}


	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getActionClassName() {
		return actionClassName;
	}


	public void setActionClassName(String actionClassName) {
		this.actionClassName = actionClassName;
	}


	public Integer getActionSequence() {
		return actionSequence;
	}


	public void setActionSequence(Integer actionSequence) {
		this.actionSequence = actionSequence;
	}


	public Integer getKeyword() {
		return keyword;
	}


	public void setKeyword(Integer keyword) {
		this.keyword = keyword;
	}


	public String getDefaultAction() {
		return defaultAction;
	}


	public void setDefaultAction(String defaultAction) {
		this.defaultAction = defaultAction;
	}


	public String getServerIP() {
		return serverIP;
	}


	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}


	public String getServerPort() {
		return serverPort;
	}


	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}


	public String getServerResponseTimeout() {
		return serverResponseTimeout;
	}


	public void setServerResponseTimeout(String serverResponseTimeout) {
		this.serverResponseTimeout = serverResponseTimeout;
	}


	public String getMaxFailureAttempt() {
		return maxFailureAttempt;
	}


	public void setMaxFailureAttempt(String maxFailureAttempt) {
		this.maxFailureAttempt = maxFailureAttempt;
	}


	public String getSvcWindow() {
		return svcWindow;
	}


	public void setSvcWindow(String svcWindow) {
		this.svcWindow = svcWindow;
	}


	public String getSuccessCode() {
		return successCode;
	}


	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}


	public String getFailureCode() {
		return failureCode;
	}


	public void setFailureCode(String failureCode) {
		this.failureCode = failureCode;
	}


	public String getDataAge() {
		return dataAge;
	}


	public void setDataAge(String dataAge) {
		this.dataAge = dataAge;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	

}

