package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.Constants;

public class AutoActionSearchForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8523188947066140410L;
	private String searchkey;
	private ArrayList<String> searchby_label=new ArrayList<String>();
	private ArrayList<String> searchby_value=new ArrayList<String>();
	private String selectedState;	
	private Result resultList;
	private String[] checkedRecords;
	private String deleteBtn;
	private String[] selectedMsg;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		searchby_label = new ArrayList<String>();
		searchby_label.add(Constants.STR_BY_AUTO_ACTION);
	
				
		searchby_value = new ArrayList<String>();
		searchby_value.add(Constants.STR_BY_AUTO_ACTION);
	
				
		selectedState = Constants.STR_EMPTY;
		searchkey=Constants.STR_EMPTY;
		//resultList=null;
		checkedRecords=null;
		deleteBtn=null;
	}
	
	
	/**
	 * @return Returns the searchby_label.
	 */
	public ArrayList<String> getSearchby_label() {
		return searchby_label;
	}

	/**
	 * @param searchby_label The searchby_label to set.
	 */
	public void setSearchby_label(ArrayList<String> searchby_label) {
		this.searchby_label = searchby_label;
	}

	/**
	 * @return Returns the searchby_value.
	 */
	public ArrayList<String> getSearchby_value() {
		return searchby_value;
	}

	/**
	 * @param searchby_value The searchby_value to set.
	 */
	public void setSearchby_value(ArrayList<String> searchby_value) {
		this.searchby_value = searchby_value;
	}

	/**
	 * @return Returns the searchkey.
	 */
	public String getSearchkey() {
		return searchkey;
	}

	/**
	 * @param searchkey The searchkey to set.
	 */
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	/**
	 * @return Returns the selectedState.
	 */
	public String getSelectedState() {
		return selectedState;
	}

	/**
	 * @param selectedState The selectedState to set.
	 */
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	/**
	 * @return Returns the resultList.
	 */
	public Result getResultList() {
		return resultList;
	}

	/**
	 * @param resultList The resultList to set.
	 */
	public void setResultList(Result resultList) {
		this.resultList = resultList;
	}

	/**
	 * @return Returns the checkedRecords.
	 */
	public String[] getCheckedRecords() {
		return checkedRecords;
	}

	/**
	 * @param checkedRecords The checkedRecords to set.
	 */
	public void setCheckedRecords(String[] checkedRecords) {
		this.checkedRecords = checkedRecords;
	}

	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getDeleteBtn() {
		return deleteBtn;
	}

	public void setDeleteBtn(String deleteBtn) {
		this.deleteBtn = deleteBtn;
	}

	public String[] getSelectedMsg() {
		return selectedMsg;
	}

	public void setSelectedMsg(String[] selectedMsg) {
		this.selectedMsg = selectedMsg;
	}
	
	
	
}
