package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.bcb.crsmsg.util.Constants;

public class BillingReportForm extends ActionForm {
	
	private static final long serialVersionUID = 66388637221683274L;
	
	private String prID;
	private String selPR;
	private String hiddenSel;
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private ArrayList searchbyserver_label;
	private ArrayList searchbyserver_value;
	private String searchkey;
	private String selectedState;
	private String selectedServer;
	private String delete;
	
	private String searchBy;
	private String butSearch;
	private String selectedUntilDate;
	private String selectedFromDate;
	
	private String startDate;
	private String endDate;
	private String dept;
	
	private String grandTotal_SMSSent;
	private String grandTotal_Cost;
	private String status;
	
	private boolean isAdmin;
	
	private String grandTotal_Gst;
	private String grandTotal_All;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
//		selAsset = null;
		
		butSearch= null;
//		selectedState = "";
		delete=null;
		searchby_label = new ArrayList();
		
		searchby_label.add(Constants.STR_BY_DEPARTMENT);
		searchby_value = new ArrayList();

		searchby_value.add(Constants.STR_BY_DEPARTMENT);

		grandTotal_Cost = "";
		grandTotal_SMSSent = "";
		grandTotal_Gst = "";
		grandTotal_All = "";
		
	}
	
	
	
	
	/**
	 * @return Returns the searchkey.
	 */
	public String getSearchkey() {
		return searchkey;
	}
	
	
	
	
	/**
	 * @param searchkey The searchkey to set.
	 */
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}
	
	
	
	
	public String getSelPR() {
		return selPR;
	}
	
	public void setSelPR(String selPR) {
		this.selPR = selPR;
	}
	
	/**
	 * @return Returns the prID.
	 */
	public String getPrID() {
		return prID;
	}
	
	/**
	 * @param prID The prID to set.
	 */
	public void setPrID(String prID) {
		this.prID = prID;
	}
	
	
	/**
	 * @return Returns the searchby_label.
	 */
	public ArrayList getSearchby_label() {
		return searchby_label;
	}
	
	
	/**
	 * @param searchby_label The searchby_label to set.
	 */
	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}
	
	
	/**
	 * @return Returns the searchby_value.
	 */
	public ArrayList getSearchby_value() {
		return searchby_value;
	}
	
	
	/**
	 * @param searchby_value The searchby_value to set.
	 */
	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}
	
	/**
	 * @return Returns the selectedState.
	 */
	public String getSelectedState() {
		return selectedState;
	}
	
	/**
	 * @param selectedState The selectedState to set.
	 */
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}




	/**
	 * @return Returns the hiddenSel.
	 */
	public String getHiddenSel() {
		return hiddenSel;
	}




	/**
	 * @param hiddenSel The hiddenSel to set.
	 */
	public void setHiddenSel(String hiddenSel) {
		this.hiddenSel = hiddenSel;
	}




	/**
	 * @return Returns the butSearch.
	 */
	public String getButSearch() {
		return butSearch;
	}




	/**
	 * @param butSearch The butSearch to set.
	 */
	public void setButSearch(String butSearch) {
		this.butSearch = butSearch;
	}




	/**
	 * @return Returns the delete.
	 */
	public String getDelete() {
		return delete;
	}




	/**
	 * @param delete The delete to set.
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}




	public String getSearchBy() {
		return searchBy;
	}




	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}




	public String getSelectedFromDate() {
		return selectedFromDate;
	}




	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}




	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}




	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}




	/**
	 * @return Returns the endDate.
	 */
	public String getEndDate() {
		return endDate;
	}




	/**
	 * @param endDate The endDate to set.
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}




	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate() {
		return startDate;
	}




	/**
	 * @param startDate The startDate to set.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}




	/**
	 * @return Returns the dept.
	 */
	public String getDept() {
		return dept;
	}




	/**
	 * @param dept The dept to set.
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}




	/**
	 * @return Returns the grandTotal_Cost.
	 */
	public String getGrandTotal_Cost() {
		return grandTotal_Cost;
	}




	/**
	 * @param grandTotal_Cost The grandTotal_Cost to set.
	 */
	public void setGrandTotal_Cost(String grandTotal_Cost) {
		this.grandTotal_Cost = grandTotal_Cost;
	}




	/**
	 * @return Returns the grandTotal_SMSSent.
	 */
	public String getGrandTotal_SMSSent() {
		return grandTotal_SMSSent;
	}




	/**
	 * @param grandTotal_SMSSent The grandTotal_SMSSent to set.
	 */
	public void setGrandTotal_SMSSent(String grandTotal_SMSSent) {
		this.grandTotal_SMSSent = grandTotal_SMSSent;
	}




	/**
	 * @return Returns the isAdmin.
	 */
	public boolean isAdmin() {
		return isAdmin;
	}




	/**
	 * @param isAdmin The isAdmin to set.
	 */
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public String getGrandTotal_Gst() {
		return grandTotal_Gst;
	}




	public void setGrandTotal_Gst(String grandTotal_Gst) {
		this.grandTotal_Gst = grandTotal_Gst;
	}




	public String getGrandTotal_All() {
		return grandTotal_All;
	}




	public void setGrandTotal_All(String grandTotal_All) {
		this.grandTotal_All = grandTotal_All;
	}




	public ArrayList getSearchbyserver_label() {
		return searchbyserver_label;
	}




	public void setSearchbyserver_label(ArrayList searchbyserver_label) {
		this.searchbyserver_label = searchbyserver_label;
	}




	public ArrayList getSearchbyserver_value() {
		return searchbyserver_value;
	}




	public void setSearchbyserver_value(ArrayList searchbyserver_value) {
		this.searchbyserver_value = searchbyserver_value;
	}




	public String getSelectedServer() {
		return selectedServer;
	}




	public void setSelectedServer(String selectedServer) {
		this.selectedServer = selectedServer;
	}	

}
