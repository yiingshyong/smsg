package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;



public final class BulkSMSForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5827543193749737475L;
	private String logId;
	private String bulkName;
	private String smsContent;
	private String scheduledSMS;
	private String scheduledSMSDateTime;
	private String systemDateTime;
	private String maxNoOfSMS;
	private String charPerSms;
	private String concatenateSMS;
	private FormFile bulkSMSFile;
	private String fileDelimiter;
	private String advanceDayLimit;
	private String bulkLimit;
	
	private String selected_charset;
	private ArrayList<String> charset_label=null;
	private ArrayList<String> charset_value=null;
	
	private String smsPriority;
	private String smsPriorityShow;
	private ArrayList<String> smsPriority_label=null;
	private ArrayList<String> smsPriority_value=null;
	
	private String verificationRequired;
	private ArrayList<String> verificationRequired_label=null;
	private ArrayList<String> verificationRequired_value=null;
	
	private String bulkFileMode;
	private ArrayList<String> bulkFileMode_label=null;
	private ArrayList<String> bulkFileMode_value=null;
	
	private String fileConversion;
	private ArrayList<String> fc_label=null;
	private ArrayList<String> fc_value=null;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		bulkName=null;
		verificationRequired=null;
		smsContent=null;
		scheduledSMS=null;
		scheduledSMSDateTime=null;
		systemDateTime=null;
		maxNoOfSMS=null;
		charPerSms=null;
		concatenateSMS=null;
		bulkSMSFile=null;
		fileDelimiter=null;
		advanceDayLimit=null;
		bulkLimit=null;
		
		smsPriority=null;
		smsPriorityShow=null;
		smsPriority_label = new ArrayList<String>();
		smsPriority_label.add(Constants.PRIORITY_LEVEL_1);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_2);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_3);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_4);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_5);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_6);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_7);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_8);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_9);
		
		smsPriority_value = new ArrayList<String>();
		smsPriority_value.add(Constants.PRIORITY_LEVEL_1);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_2);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_3);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_4);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_5);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_6);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_7);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_8);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_9);
		
		selected_charset=null;
		charset_label = new ArrayList<String>();
		charset_label.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_label.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		charset_value = new ArrayList<String>();
		charset_value.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_value.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		verificationRequired=null;
		verificationRequired_label = new ArrayList<String>();
		verificationRequired_label.add(Constants.FORM_FIELD_STATUS_YES);
		verificationRequired_label.add(Constants.FORM_FIELD_STATUS_NO);
		
		verificationRequired_value = new ArrayList<String>();
		verificationRequired_value.add(Constants.STATUS_YES);
		verificationRequired_value.add(Constants.STATUS_NO);
		
		bulkFileMode=null;
		bulkFileMode_label = new ArrayList<String>();
		bulkFileMode_label.add(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE);
		bulkFileMode_label.add(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE);
		
		bulkFileMode_value = new ArrayList<String>();
		bulkFileMode_value.add(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE);
		bulkFileMode_value.add(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE_TEMPLATE);
		
		fileConversion=null;
		List fcList=DBUtilsCrsmsg.DBHQLCommand("from FileConversion where batchSetupDao=? order by conversionName", new Object[]{"bulkSMSSetupDao"});
		fc_label = new ArrayList<String>();
		fc_value = new ArrayList<String>();
		for(int i=0;i<fcList.size();i++){
			FileConversion aFc=(FileConversion)fcList.get(i);
			fc_label.add(aFc.getConversionName());
			fc_value.add(aFc.getId().toString());
		}
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (selected_charset!=null&&selected_charset.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CHARSET));
		
		if (smsPriority!=null&&smsPriority.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_PRIORITY));
		
		if (bulkSMSFile!=null&&bulkSMSFile.getFileName().length()==0)
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_BULKFILE));
		
		if (bulkSMSFile!=null&&bulkSMSFile.getFileName().length()>30)
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FILE_LENGTH_EXCEED));
		
//		if (fileConversion!=null&&fileConversion.equals(Constants.VALUE_0))
//			errors.add(Constants.FORM_BULK_SMS,
//					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_FILE_CONVERSION));
		
		if ((smsContent!=null&&smsContent.trim().length()==Integer.valueOf(Constants.LENGTH_0))&&
				(bulkFileMode!=null&&bulkFileMode.equals(Constants.FORM_FIELD_BULK_FILE_MODE_PHONE)))
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MESSAGE_TEXT));
		
		// Size Limit
		if (smsContent!=null&&smsContent.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_BULK_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SMS_CONTENT, Constants.LENGTH_1000));
		
		return (errors);
		
	}


	/**
	 * @return Returns the charset_label.
	 */
	public ArrayList<String> getCharset_label() {
		return charset_label;
	}


	/**
	 * @param charset_label The charset_label to set.
	 */
	public void setCharset_label(ArrayList<String> charset_label) {
		this.charset_label = charset_label;
	}


	/**
	 * @return Returns the charset_value.
	 */
	public ArrayList<String> getCharset_value() {
		return charset_value;
	}


	/**
	 * @param charset_value The charset_value to set.
	 */
	public void setCharset_value(ArrayList<String> charset_value) {
		this.charset_value = charset_value;
	}

	/**
	 * @return Returns the scheduledSMS.
	 */
	public String getScheduledSMS() {
		return scheduledSMS;
	}


	/**
	 * @param scheduledSMS The scheduledSMS to set.
	 */
	public void setScheduledSMS(String scheduledSMS) {
		this.scheduledSMS = scheduledSMS;
	}


	/**
	 * @return Returns the scheduledSMSDateTime.
	 */
	public String getScheduledSMSDateTime() {
		return scheduledSMSDateTime;
	}


	/**
	 * @param scheduledSMSDateTime The scheduledSMSDateTime to set.
	 */
	public void setScheduledSMSDateTime(String scheduledSMSDateTime) {
		this.scheduledSMSDateTime = scheduledSMSDateTime;
	}


	/**
	 * @return Returns the selected_charset.
	 */
	public String getSelected_charset() {
		return selected_charset;
	}


	/**
	 * @param selected_charset The selected_charset to set.
	 */
	public void setSelected_charset(String selected_charset) {
		this.selected_charset = selected_charset;
	}


	/**
	 * @return Returns the smsContent.
	 */
	public String getSmsContent() {
		return smsContent;
	}


	/**
	 * @param smsContent The smsContent to set.
	 */
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}


	/**
	 * @return Returns the smsPriority.
	 */
	public String getSmsPriority() {
		return smsPriority;
	}


	/**
	 * @param smsPriority The smsPriority to set.
	 */
	public void setSmsPriority(String smsPriority) {
		this.smsPriority = smsPriority;
	}


	/**
	 * @return Returns the smsPriority_label.
	 */
	public ArrayList<String> getSmsPriority_label() {
		return smsPriority_label;
	}


	/**
	 * @param smsPriority_label The smsPriority_label to set.
	 */
	public void setSmsPriority_label(ArrayList<String> smsPriority_label) {
		this.smsPriority_label = smsPriority_label;
	}


	/**
	 * @return Returns the smsPriority_value.
	 */
	public ArrayList<String> getSmsPriority_value() {
		return smsPriority_value;
	}


	/**
	 * @param smsPriority_value The smsPriority_value to set.
	 */
	public void setSmsPriority_value(ArrayList<String> smsPriority_value) {
		this.smsPriority_value = smsPriority_value;
	}


	/**
	 * @return Returns the systemDateTime.
	 */
	public String getSystemDateTime() {
		return systemDateTime;
	}


	/**
	 * @param systemDateTime The systemDateTime to set.
	 */
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}

	/**
	 * @return Returns the maxNoOfSMS.
	 */
	public String getMaxNoOfSMS() {
		return maxNoOfSMS;
	}


	/**
	 * @param maxNoOfSMS The maxNoOfSMS to set.
	 */
	public void setMaxNoOfSMS(String maxNoOfSMS) {
		this.maxNoOfSMS = maxNoOfSMS;
	}


	/**
	 * @return Returns the charPerSms.
	 */
	public String getCharPerSms() {
		return charPerSms;
	}


	/**
	 * @param charPerSms The charPerSms to set.
	 */
	public void setCharPerSms(String charPerSms) {
		this.charPerSms = charPerSms;
	}


	/**
	 * @return Returns the concatenateSMS.
	 */
	public String getConcatenateSMS() {
		return concatenateSMS;
	}


	/**
	 * @param concatenateSMS The concatenateSMS to set.
	 */
	public void setConcatenateSMS(String concatenateSMS) {
		this.concatenateSMS = concatenateSMS;
	}


	/**
	 * @return Returns the logId.
	 */
	public String getLogId() {
		return logId;
	}


	/**
	 * @param logId The logId to set.
	 */
	public void setLogId(String logId) {
		this.logId = logId;
	}


	/**
	 * @return Returns the verificationRequired.
	 */
	public String getVerificationRequired() {
		return verificationRequired;
	}


	/**
	 * @param verificationRequired The verificationRequired to set.
	 */
	public void setVerificationRequired(String verificationRequired) {
		this.verificationRequired = verificationRequired;
	}


	/**
	 * @return Returns the bulkSMSFile.
	 */
	public FormFile getBulkSMSFile() {
		return bulkSMSFile;
	}


	/**
	 * @param bulkSMSFile The bulkSMSFile to set.
	 */
	public void setBulkSMSFile(FormFile bulkSMSFile) {
		this.bulkSMSFile = bulkSMSFile;
	}


	/**
	 * @return Returns the verificationRequired_label.
	 */
	public ArrayList<String> getVerificationRequired_label() {
		return verificationRequired_label;
	}


	/**
	 * @param verificationRequired_label The verificationRequired_label to set.
	 */
	public void setVerificationRequired_label(
			ArrayList<String> verificationRequired_label) {
		this.verificationRequired_label = verificationRequired_label;
	}


	/**
	 * @return Returns the verificationRequired_value.
	 */
	public ArrayList<String> getVerificationRequired_value() {
		return verificationRequired_value;
	}


	/**
	 * @param verificationRequired_value The verificationRequired_value to set.
	 */
	public void setVerificationRequired_value(
			ArrayList<String> verificationRequired_value) {
		this.verificationRequired_value = verificationRequired_value;
	}


	public String getFileConversion() {
		return fileConversion;
	}


	public void setFileConversion(String fileConversion) {
		this.fileConversion = fileConversion;
	}


	public String getBulkName() {
		return bulkName;
	}


	public void setBulkName(String bulkName) {
		this.bulkName = bulkName;
	}


	public String getFileDelimiter() {
		return fileDelimiter;
	}


	public void setFileDelimiter(String fileDelimiter) {
		this.fileDelimiter = fileDelimiter;
	}


	public String getBulkFileMode() {
		return bulkFileMode;
	}


	public void setBulkFileMode(String bulkFileMode) {
		this.bulkFileMode = bulkFileMode;
	}


	public ArrayList<String> getBulkFileMode_label() {
		return bulkFileMode_label;
	}


	public void setBulkFileMode_label(ArrayList<String> bulkFileMode_label) {
		this.bulkFileMode_label = bulkFileMode_label;
	}


	public ArrayList<String> getBulkFileMode_value() {
		return bulkFileMode_value;
	}


	public void setBulkFileMode_value(ArrayList<String> bulkFileMode_value) {
		this.bulkFileMode_value = bulkFileMode_value;
	}


	public ArrayList<String> getFc_label() {
		return fc_label;
	}


	public void setFc_label(ArrayList<String> fc_label) {
		this.fc_label = fc_label;
	}


	public ArrayList<String> getFc_value() {
		return fc_value;
	}


	public void setFc_value(ArrayList<String> fc_value) {
		this.fc_value = fc_value;
	}


	public String getSmsPriorityShow() {
		return smsPriorityShow;
	}


	public void setSmsPriorityShow(String smsPriorityShow) {
		this.smsPriorityShow = smsPriorityShow;
	}


	public String getAdvanceDayLimit() {
		return advanceDayLimit;
	}


	public void setAdvanceDayLimit(String advanceDayLimit) {
		this.advanceDayLimit = advanceDayLimit;
	}


	public String getBulkLimit() {
		return bulkLimit;
	}


	public void setBulkLimit(String bulkLimit) {
		this.bulkLimit = bulkLimit;
	}
}

