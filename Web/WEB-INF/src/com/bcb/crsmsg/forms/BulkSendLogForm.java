package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class BulkSendLogForm extends ActionForm {


	private static final long serialVersionUID = 7338188937827461681L;
	
	
	private String butSubmit;
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private String searchkey;
	private String selectedState;
	private String hiddenSel;
	 
	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String selectedUntilDate;
	private String selectedFromDate;
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		searchby_label = new ArrayList();
		searchby_label.add("Name");
		searchby_label.add("Dept");
		
		searchby_value = new ArrayList();
		searchby_value.add("Name");
		searchby_value.add("Dept");
		
		
	}	
	
//	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
//		
//		ActionErrors errors = new ActionErrors();
//		
//		if (subType!=null && subType.length() < 1 )
//			errors.add(Constants.APPSNAME,
//					new ActionMessage(Constants.ERRORS_ASM_MANDATORY,Constants.STR_SUB_CATEGORY));
//		
//		
//		return errors;
//	}
	
	
	/**
	 * @return Returns the butSubmit.
	 */
	public String getButSubmit() {
		return butSubmit;
	}
	
	/**
	 * @param butSubmit The butSubmit to set.
	 */
	public void setButSubmit(String butSubmit) {
		this.butSubmit = butSubmit;
	}

	

	

	public ArrayList getSearchby_label() {
		return searchby_label;
	}

	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}

	public ArrayList getSearchby_value() {
		return searchby_value;
	}

	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	public String getHiddenSel() {
		return hiddenSel;
	}

	public void setHiddenSel(String hiddenSel) {
		this.hiddenSel = hiddenSel;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getSelectedFromDate() {
		return selectedFromDate;
	}

	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}

	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}

	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}

	
}
