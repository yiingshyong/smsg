package com.bcb.crsmsg.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;

public class CardActivationConfigForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5541197078612139051L;
	private String ccaServerIP;
	private String ccaServerPort;
	private String ccaServerResponseTimeout;
	private String ccaMaxFailureAttempt;
	private String ccaSvcWindow;
	private String ccaSuccessSms;
	private String ccaFailureSms;
	private String ccaUnavailabilitySms;
	private String ccaHostUnavailabilitySms;
	private int cfgId;
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		ccaServerIP=null;
		ccaServerPort=null;
		ccaServerResponseTimeout=null;
		ccaMaxFailureAttempt=null;
		ccaSvcWindow=null;
		ccaSuccessSms=null;
		ccaFailureSms=null;
		ccaUnavailabilitySms=null;	  
		ccaHostUnavailabilitySms = null;
	}	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
				
		if (ccaServerPort!=null&&ccaServerPort.length()>0){
			if(!ccaServerPort.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_CARD_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SERVER_PORT, ccaServerPort));
			}
		}else{
			ccaServerPort="0";
		}
		
		if (ccaServerResponseTimeout!=null&&ccaServerResponseTimeout.length()>0){
			if(!ccaServerResponseTimeout.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_CARD_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SERVER_TIMEOUT, ccaServerResponseTimeout));
			}
		}else{
			ccaServerResponseTimeout="0";
		}
		
		if (ccaMaxFailureAttempt!=null&&ccaMaxFailureAttempt.length()>0){
			if(!ccaMaxFailureAttempt.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_CARD_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_FAILURE_ATTEMPT, ccaMaxFailureAttempt));
			}
		}else{
			ccaMaxFailureAttempt="0";
		}

		if(ccaSvcWindow == null){
			ccaSvcWindow="0000:0000";
		}else if (ccaSvcWindow!=null&&ccaSvcWindow.length()>0){
			if(ccaSvcWindow.length()!= 9){ 
				errors.add(Constants.FORM_CARD_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, ccaSvcWindow));
			}else{ 
				String ccaSvcWindow1 = ccaSvcWindow.substring(0, 4);
				String ccaSvcWindowDot = ccaSvcWindow.substring(4, 5);
				String ccaSvcWindow2 = ccaSvcWindow.substring(5, 9);
				if (!ccaSvcWindow1.matches("^\\d{1,15}$")){ 
					errors.add(Constants.FORM_CARD_CFG,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, ccaSvcWindow));
				}else if (!ccaSvcWindow2.matches("^\\d{1,15}$")){ 
					errors.add(Constants.FORM_CARD_CFG,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, ccaSvcWindow));
				}else if(!ccaSvcWindowDot.equals(":"))	{
					errors.add(Constants.FORM_CARD_CFG,
							new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_CCA_SVC_WINDOWS, ccaSvcWindow));
				}
			}
		}
		
		return (errors);
		
	}
	public String getCcaServerIP() {
		return ccaServerIP;
	}
	public void setCcaServerIP(String ccaServerIP) {
		this.ccaServerIP = ccaServerIP;
	}
	public String getCcaServerPort() {
		return ccaServerPort;
	}
	public void setCcaServerPort(String ccaServerPort) {
		this.ccaServerPort = ccaServerPort;
	}
	public String getCcaServerResponseTimeout() {
		return ccaServerResponseTimeout;
	}
	public void setCcaServerResponseTimeout(String ccaServerResponseTimeout) {
		this.ccaServerResponseTimeout = ccaServerResponseTimeout;
	}
	public String getCcaMaxFailureAttempt() {
		return ccaMaxFailureAttempt;
	}
	public void setCcaMaxFailureAttempt(String ccaMaxFailureAttempt) {
		this.ccaMaxFailureAttempt = ccaMaxFailureAttempt;
	}
	public String getCcaSvcWindow() {
		return ccaSvcWindow;
	}
	public void setCcaSvcWindow(String ccaSvcWindow) {
		this.ccaSvcWindow = ccaSvcWindow;
	}
	public String getCcaSuccessSms() {
		return ccaSuccessSms;
	}
	public void setCcaSuccessSms(String ccaSuccessSms) {
		this.ccaSuccessSms = ccaSuccessSms;
	}
	public String getCcaFailureSms() {
		return ccaFailureSms;
	}
	public void setCcaFailureSms(String ccaFailureSms) {
		this.ccaFailureSms = ccaFailureSms;
	}
	public String getCcaUnavailabilitySms() {
		return ccaUnavailabilitySms;
	}
	public void setCcaUnavailabilitySms(String ccaUnavailabilitySms) {
		this.ccaUnavailabilitySms = ccaUnavailabilitySms;
	}
	public int getCfgId() {
		return cfgId;
	}
	public void setCfgId(int cfgId) {
		this.cfgId = cfgId;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public String getCcaHostUnavailabilitySms() {
		return ccaHostUnavailabilitySms;
	}
	public void setCcaHostUnavailabilitySms(String ccaHostUnavailabilitySms) {
		this.ccaHostUnavailabilitySms = ccaHostUnavailabilitySms;
	}
	
	
}
