package com.bcb.crsmsg.forms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.User;
import com.bcb.common.util.DBUtils;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CardActivationDtlsForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5541197078612139051L;
	private int no;
	private int id;
	private String fileName;
	private String date;
	private String cardNumber;
	private String mobileNo;
	private String icNumber;
	private int notActivated;
	private int failedActivated;
	private int activated;
	private String status;	
	private int ftpReportId;	
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private String searchkey;
	private String selectedState;
	private String delete;	
	private String searchBy;	
	private String butSearch;	
	private String[] selectedMsg;
	private String butDelete;
	private String butExport;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		date=null;
		searchkey=null;
		butDelete=null;
		butSearch= null;
		butExport= null;
		delete=null;
		searchby_label = new ArrayList();
		searchby_label.add(Constants.STR_MOBILE_NO);
		searchby_label.add(Constants.STR_CARD_NO);
		searchby_label.add(Constants.STR_IC_NO);
		searchby_label.add(Constants.STR_STATUS);
		searchby_value = new ArrayList();
		searchby_value.add("mobileNo");		
		searchby_value.add("cardNo");		
		searchby_value.add("customerId");	
		searchby_value.add("status");		
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getIcNumber() {
		return icNumber;
	}

	public void setIcNumber(String icNumber) {
		this.icNumber = icNumber;
	}

	public int getNotActivated() {
		return notActivated;
	}

	public void setNotActivated(int notActivated) {
		this.notActivated = notActivated;
	}

	public int getFailedActivated() {
		return failedActivated;
	}

	public void setFailedActivated(int failedActivated) {
		this.failedActivated = failedActivated;
	}

	public int getActivated() {
		return activated;
	}

	public void setActivated(int activated) {
		this.activated = activated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getFtpReportId() {
		return ftpReportId;
	}

	public void setFtpReportId(int ftpReportId) {
		this.ftpReportId = ftpReportId;
	}

	public ArrayList getSearchby_label() {
		return searchby_label;
	}

	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}

	public ArrayList getSearchby_value() {
		return searchby_value;
	}

	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getButSearch() {
		return butSearch;
	}

	public void setButSearch(String butSearch) {
		this.butSearch = butSearch;
	}

	public String[] getSelectedMsg() {
		return selectedMsg;
	}

	public void setSelectedMsg(String[] selectedMsg) {
		this.selectedMsg = selectedMsg;
	}

	public String getButDelete() {
		return butDelete;
	}

	public void setButDelete(String butDelete) {
		this.butDelete = butDelete;
	}

	public String getButExport() {
		return butExport;
	}

	public void setButExport(String butExport) {
		this.butExport = butExport;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}		
}
