package com.bcb.crsmsg.forms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.User;
import com.bcb.common.util.DBUtils;
import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class CardActivationReceivedSMSForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5541197078612139051L;
	private int no;
	private int id;
	private String date;
	private String mobileNo;
	private String message;
	private String remarks;
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private String searchkey;
	private String selectedState;
	private String delete;	
	private String searchBy;	
	private String butSearch;	
	private String selectedUntilDate;
	private String selectedFromDate;
	private String[] selectedMsg;
	private String butApprove;
	private String butDelete;
	private String butExport;
	private String keyword;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		date=null;
		remarks=null;
		searchkey=null;
		butDelete=null;
		butApprove=null;
		butSearch= null;
		butExport= null;
		delete=null;
		searchby_label = new ArrayList();
		searchby_label.add(Constants.STR_MOBILE_NO);
		searchby_label.add("Message Content");
		searchby_value = new ArrayList();
		searchby_value.add("mobileNo");		
		searchby_value.add(Constants.MESSAGE);		

	}	
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (selectedFromDate!=null&&selectedFromDate.length()==Integer.valueOf(Constants.LENGTH_0)||
				selectedUntilDate!=null&&selectedUntilDate.trim().length()==Integer.valueOf(Constants.LENGTH_0)){
			errors.add(Constants.FORM_CARD_LOG,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_LABEL_DATE_RANGE));
		}else{
			SimpleDateFormat df = new SimpleDateFormat(Constants.DATEFORMAT_DAYBYMONTHYEAR_HOUR_MINUTE);
			Date startDate = null;
			Date endDate = null;
			try{
				startDate = df.parse(this.selectedFromDate);
				endDate = df.parse(this.selectedUntilDate);
				String daysDiff = DurationFormatUtils.formatPeriod(startDate.getTime(), endDate.getTime(), "d");
				if(StringUtils.isNumeric(daysDiff) && Integer.parseInt(daysDiff) > Integer.parseInt(Constants.REPORT_MAX_DATE_RANGE)){
					errors.add(Constants.FORM_CARD_LOG,
							new ActionMessage(Constants.ERRMSG_FIELD_SEARCH_DATE_RANGE_DAYS, Constants.REPORT_MAX_DATE_RANGE));												
				}
			}catch(ParseException pe){
				errors.add(Constants.FORM_CARD_LOG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_LABEL_DATE_RANGE));							
			}
		}

		return (errors);
		
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public ArrayList getSearchby_label() {
		return searchby_label;
	}
	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}
	public ArrayList getSearchby_value() {
		return searchby_value;
	}
	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}
	public String getSearchkey() {
		return searchkey;
	}
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}
	public String getSelectedState() {
		return selectedState;
	}
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}
	public String getDelete() {
		return delete;
	}
	public void setDelete(String delete) {
		this.delete = delete;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getButSearch() {
		return butSearch;
	}
	public void setButSearch(String butSearch) {
		this.butSearch = butSearch;
	}
	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}
	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}
	public String getSelectedFromDate() {
		return selectedFromDate;
	}
	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}
	public String[] getSelectedMsg() {
		return selectedMsg;
	}
	public void setSelectedMsg(String[] selectedMsg) {
		this.selectedMsg = selectedMsg;
	}
	public String getButApprove() {
		return butApprove;
	}
	public void setButApprove(String butApprove) {
		this.butApprove = butApprove;
	}
	public String getButDelete() {
		return butDelete;
	}
	public void setButDelete(String butDelete) {
		this.butDelete = butDelete;
	}

	public String getButExport() {
		return butExport;
	}

	public void setButExport(String butExport) {
		this.butExport = butExport;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
}
