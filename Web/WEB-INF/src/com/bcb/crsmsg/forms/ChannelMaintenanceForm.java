package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.UserChannel;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ChannelMaintenanceForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private UserChannel userChannel;
	private ArrayList<TblRef> smsTypes;
	private ArrayList<UserChannel> userChannels;
	private String userId;
	private String channelId;
	private ArrayList<Option> status = (ArrayList<Option>) CommonUtils.getStatusOption();
	private ArrayList<User> users;

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		smsTypes = (ArrayList<TblRef>) DaoBeanFactory.getTblRefDao().getSmsTypes();
		userChannel = new UserChannel();
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,	HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		if(StringUtils.isEmpty(this.userChannel.getUserChannelIdObj().getUserId())){
			errors.add(Constants.FORM_CHANNEL_FORM,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_USER_ID));
		}
		if(StringUtils.isEmpty(this.userChannel.getUserChannelIdObj().getChannelId())){
			errors.add(Constants.FORM_CHANNEL_FORM,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CHANNEL_ID));
		}
		if(this.userChannel.getSmsType().getTblRefId() == 0){
			errors.add(Constants.FORM_CHANNEL_FORM,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_TYPE));			
		}		
		return errors;
	}

	public UserChannel getUserChannel() {
		return userChannel;
	}

	public void setUserChannel(UserChannel userChannel) {
		this.userChannel = userChannel;
	}

	public List<TblRef> getSmsTypes() {
		return smsTypes;
	}

	public void setSmsTypes(List<TblRef> smsTypes) {
		this.smsTypes = (ArrayList<TblRef>) smsTypes;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<UserChannel> getUserChannels() {
		return userChannels;
	}

	public void setUserChannels(List<UserChannel> userChannels) {
		this.userChannels = (ArrayList<UserChannel>) userChannels;
	}

	public List<Option> getStatus() {
		return status;
	}

	public void setStatus(List<Option> status) {
		this.status = (ArrayList<Option>) status;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = (ArrayList<User>) users;
	}

}
