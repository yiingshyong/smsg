package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.bcb.crsmsg.util.Constants;

public class CommonInboxForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5673953131797725789L;
	private String prID;
	private String selPR;
	private String hiddenSel;
	private ArrayList<String> searchby_label;
	private ArrayList<String> searchby_value;
	private String searchkey;
	private String selectedState;
	private String delete;
	
	private String searchBy;
	
	private String butSearch;
	
	private String selectedUntilDate;
	private String selectedFromDate;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
//		selAsset = null;
		
		butSearch= null;
//		selectedState = "";
		delete=null;
		searchby_label = new ArrayList<String>();
		searchby_label.add(Constants.STR_BY_MOBILE);
		searchby_label.add(Constants.STR_BY_CUST_REF_NO);

		searchby_value = new ArrayList<String>();
		searchby_value.add(Constants.STR_BY_MOBILE);
		searchby_value.add(Constants.STR_BY_CUST_REF_NO);
	}
	
	
	
	
	/**
	 * @return Returns the searchkey.
	 */
	public String getSearchkey() {
		return searchkey;
	}
	
	
	
	
	/**
	 * @param searchkey The searchkey to set.
	 */
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}
	
	
	
	
	public String getSelPR() {
		return selPR;
	}
	
	public void setSelPR(String selPR) {
		this.selPR = selPR;
	}
	
	/**
	 * @return Returns the prID.
	 */
	public String getPrID() {
		return prID;
	}
	
	/**
	 * @param prID The prID to set.
	 */
	public void setPrID(String prID) {
		this.prID = prID;
	}
	
	
	/**
	 * @return Returns the searchby_label.
	 */
	public ArrayList getSearchby_label() {
		return searchby_label;
	}
	
	
	/**
	 * @param searchby_label The searchby_label to set.
	 */
	public void setSearchby_label(ArrayList<String> searchby_label) {
		this.searchby_label = searchby_label;
	}
	
	
	/**
	 * @return Returns the searchby_value.
	 */
	public ArrayList<String> getSearchby_value() {
		return searchby_value;
	}
	
	
	/**
	 * @param searchby_value The searchby_value to set.
	 */
	public void setSearchby_value(ArrayList<String> searchby_value) {
		this.searchby_value = searchby_value;
	}
	
	/**
	 * @return Returns the selectedState.
	 */
	public String getSelectedState() {
		return selectedState;
	}
	
	/**
	 * @param selectedState The selectedState to set.
	 */
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}




	/**
	 * @return Returns the hiddenSel.
	 */
	public String getHiddenSel() {
		return hiddenSel;
	}




	/**
	 * @param hiddenSel The hiddenSel to set.
	 */
	public void setHiddenSel(String hiddenSel) {
		this.hiddenSel = hiddenSel;
	}




	/**
	 * @return Returns the butSearch.
	 */
	public String getButSearch() {
		return butSearch;
	}




	/**
	 * @param butSearch The butSearch to set.
	 */
	public void setButSearch(String butSearch) {
		this.butSearch = butSearch;
	}




	/**
	 * @return Returns the delete.
	 */
	public String getDelete() {
		return delete;
	}




	/**
	 * @param delete The delete to set.
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}




	public String getSearchBy() {
		return searchBy;
	}




	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}




	public String getSelectedFromDate() {
		return selectedFromDate;
	}




	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}




	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}




	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}


	
	
}
