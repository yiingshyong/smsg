package com.bcb.crsmsg.forms;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;



public final class ContactForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4851048155675524960L;
	private String contactId;
	private String name;
	private String custRefNo;
	private String mobileNo;
	private String designation;
	
	private Integer selected_department;
	private ArrayList department_label=null;
	private ArrayList department_value=null;

	private String selected_type;
	private ArrayList<String> type_label=null;
	private ArrayList<String> type_value=null;

	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		selected_department=null;
		department_label=null;
		department_value=null;
		selected_type=null;
		type_label = new ArrayList<String>();
		type_label.add(Constants.FORM_FIELD_CONTACT_TYPE_PERSONAL);
		type_label.add(Constants.FORM_FIELD_CONTACT_TYPE_GROUP);
		
		type_value = new ArrayList<String>();
		type_value.add(Constants.FORM_FIELD_CONTACT_TYPE_PERSONAL);
		type_value.add(Constants.FORM_FIELD_CONTACT_TYPE_GROUP);
		contactId=null;
		name=null;
		custRefNo=null;
		mobileNo=null;
		designation=null;
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if ( name!=null&&name.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_USER_NAME));
		
		if ( mobileNo!=null&&mobileNo.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MOBILE_NO));
		
		if (selected_department!=null&&selected_department.intValue() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_USER_DEPARTMENT));
		
		if (selected_type!=null&&selected_type.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CONTACT_TYPE));
		
		// Size Limit
		if ( name!=null&&name.length() >Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USER_NAME, Constants.LENGTH_100));
		
		if ( custRefNo!=null&&custRefNo.length() >Integer.valueOf(Constants.LENGTH_20))
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_CUST_REF_NO, Constants.LENGTH_20));
		
		if ( mobileNo!=null&&mobileNo.length() >Integer.valueOf(Constants.LENGTH_20) )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MOBILE_NO, Constants.LENGTH_20));
		
		if ( designation!=null&&designation.length() >Integer.valueOf(Constants.LENGTH_50))
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_DESIGNATION, Constants.LENGTH_50));
		
		
		return (errors);
		
	}
	
	/**
	 * @return Returns the department_label.
	 */
	public ArrayList getDepartment_label() {
		return department_label;
	}
	
	
	/**
	 * @param department_label The department_label to set.
	 */
	public void setDepartment_label(ArrayList department_label) {
		this.department_label = department_label;
	}
	
	
	/**
	 * @return Returns the department_value.
	 */
	public ArrayList getDepartment_value() {
		return department_value;
	}
	
	
	/**
	 * @param department_value The department_value to set.
	 */
	public void setDepartment_value(ArrayList department_value) {
		this.department_value = department_value;
	}
	
	
	/**
	 * @return Returns the selected_department.
	 */
	public Integer getSelected_department() {
		return selected_department;
	}
	
	
	/**
	 * @param selected_department The selected_department to set.
	 */
	public void setSelected_department(Integer selected_department) {
		this.selected_department = selected_department;
	}


	/**
	 * @return Returns the custRefNo.
	 */
	public String getCustRefNo() {
		return custRefNo;
	}


	/**
	 * @param custRefNo The custRefNo to set.
	 */
	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}


	/**
	 * @return Returns the designation.
	 */
	public String getDesignation() {
		return designation;
	}


	/**
	 * @param designation The designation to set.
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}


	/**
	 * @return Returns the mobileNo.
	 */
	public String getMobileNo() {
		return mobileNo;
	}


	/**
	 * @param mobileNo The mobileNo to set.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return Returns the selected_type.
	 */
	public String getSelected_type() {
		return selected_type;
	}


	/**
	 * @param selected_type The selected_type to set.
	 */
	public void setSelected_type(String selected_type) {
		this.selected_type = selected_type;
	}


	/**
	 * @return Returns the type_label.
	 */
	public ArrayList<String> getType_label() {
		return type_label;
	}


	/**
	 * @param type_label The type_label to set.
	 */
	public void setType_label(ArrayList<String> type_label) {
		this.type_label = type_label;
	}


	/**
	 * @return Returns the type_value.
	 */
	public ArrayList<String> getType_value() {
		return type_value;
	}


	/**
	 * @param type_value The type_value to set.
	 */
	public void setType_value(ArrayList<String> type_value) {
		this.type_value = type_value;
	}


	/**
	 * @return Returns the contactId.
	 */
	public String getContactId() {
		return contactId;
	}


	/**
	 * @param contactId The contactId to set.
	 */
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}


	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}

