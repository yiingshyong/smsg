package com.bcb.crsmsg.forms;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;



public final class CustomerForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4851048155675524960L;
	
	private String custRefNo;
	private String mobileNo;
	
	private String selected_status;
	private ArrayList<String> type_label=null;
	private ArrayList<String> type_value=null;
	
	private String submitBtn;
	private String id;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		selected_status=null;
		type_label = new ArrayList<String>();
		type_label.add(Constants.FORM_FIELD_SUBSCRIBE);
		type_label.add(Constants.FORM_FIELD_UNSUBSCRIBE);
		
		type_value = new ArrayList<String>();
		type_value.add(Constants.FORM_LABEL_SUBSCRIBE);
		type_value.add(Constants.FORM_LABEL_UNSUBSCRIBE);
		
		custRefNo=null;
		mobileNo=null;
		
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if ( custRefNo!=null&&custRefNo.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CUST_REF_NO));
		
		if ( mobileNo!=null&&mobileNo.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MOBILE_NO));
		
				
		// Size Limit
		if ( custRefNo!=null&&custRefNo.length() >Integer.valueOf(Constants.LENGTH_20))
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_CUST_REF_NO, Constants.LENGTH_20));
		
		if ( mobileNo!=null&&mobileNo.length() >Integer.valueOf(Constants.LENGTH_20) )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MOBILE_NO, Constants.LENGTH_20));
		
		
		
		return (errors);
		
	}
	
	/**
	 * @return Returns the custRefNo.
	 */
	public String getCustRefNo() {
		return custRefNo;
	}


	/**
	 * @param custRefNo The custRefNo to set.
	 */
	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}

	/**
	 * @return Returns the mobileNo.
	 */
	public String getMobileNo() {
		return mobileNo;
	}


	/**
	 * @param mobileNo The mobileNo to set.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return Returns the selected_type.
	 */
	public String getSelected_status() {
		return selected_status;
	}


	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	/**
	 * @return Returns the type_label.
	 */
	public ArrayList<String> getType_label() {
		return type_label;
	}


	/**
	 * @param type_label The type_label to set.
	 */
	public void setType_label(ArrayList<String> type_label) {
		this.type_label = type_label;
	}


	/**
	 * @return Returns the type_value.
	 */
	public ArrayList<String> getType_value() {
		return type_value;
	}


	/**
	 * @param type_value The type_value to set.
	 */
	public void setType_value(ArrayList<String> type_value) {
		this.type_value = type_value;
	}


	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}


	public String getSubmitBtn() {
		return submitBtn;
	}


	public void setSubmitBtn(String submitBtn) {
		this.submitBtn = submitBtn;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	

}

