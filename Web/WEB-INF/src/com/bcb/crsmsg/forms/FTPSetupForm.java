package com.bcb.crsmsg.forms;


import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.bcb.common.modal.User;
import com.bcb.crsmsg.modal.FileConversion;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;



public final class FTPSetupForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6146148435285325576L;
	private String ftpId;
	private String ftpName;
	private String ftpServer;
	private String ftpPort;
	private String ftpTargetFile;
	private String messageTemplate;
	private String setupStatus;
	private String ftpTimeMode;
	private String ftpDayOfMonth;
	private String ftpHour;
	private String ftpHourFrom;
	private String ftpHourTo;
	private String ftpTime;
	private String ftpRetryNo;
	private String ftpRetryPeriod;
	private String notificationSetupSender;
	private String notificationSetupReceiver;
	private String notificationSetupStatus;
	private String ftpUserName;
	private String ftpPassword;
	private String ftpPasswordConfirm;
	//private String securityKey;
	private FormFile securityKey;
	private String smsScheduled;
	private String smsScheduledTime;
	private String systemDateTime;
	private String advanceDayLimit;
	
	private String smsAutoApproval;
	private ArrayList<String> approval_label=null;
	private ArrayList<String> approval_value=null;
	
	private String fileConversion;
	private ArrayList<String> fc_label=null;
	private ArrayList<String> fc_value=null;
	
	private String[] ftpDay;
	private ArrayList<String> day_label=null;
	private ArrayList<String> day_value=null;
	
	private String[] ftpMonth;
	private ArrayList<String> month_label=null;
	private ArrayList<String> month_value=null;
	
	private String ftpMode;
	private ArrayList<String> ftpMode_label=null;
	private ArrayList<String> ftpMode_value=null;
	
	private String ftpOwner;
	private ArrayList<String> ftpOwner_label=null;
	private ArrayList<String> ftpOwner_value=null;
	
	private String ftpDesc;
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		ftpName=null;
		ftpServer=null;
		ftpPort=null;
		ftpTargetFile=null;
		messageTemplate=null;
		setupStatus=null;
		ftpTimeMode=null;
		ftpMonth=null;
		ftpDayOfMonth=null;
		ftpDay=null;
		ftpTime=null;
		ftpRetryNo=null;
		ftpRetryPeriod=null;
		notificationSetupSender=null;
		notificationSetupReceiver=null;
		notificationSetupStatus=null;
		ftpUserName=null;
		ftpPassword=null;
		ftpPasswordConfirm=null;
		securityKey=null;
		smsScheduled=null;
		smsScheduledTime=null;
		systemDateTime=null;
		advanceDayLimit=null;
		
		smsAutoApproval=null;
		approval_label = new ArrayList<String>();
		approval_label.add(Constants.FORM_FIELD_STATUS_YES);
		approval_label.add(Constants.FORM_FIELD_STATUS_NO);
		
		approval_value = new ArrayList<String>();
		approval_value.add(Constants.STATUS_YES);
		approval_value.add(Constants.STATUS_NO);
		
		fileConversion=null;
		List fcList=DBUtilsCrsmsg.DBHQLCommand("from FileConversion where batchSetupDao=? order by conversionName", new Object[]{"ftpSetupDao"});
		fc_label = new ArrayList<String>();
		fc_value = new ArrayList<String>();
		for(int i=0;i<fcList.size();i++){
			FileConversion aFc=(FileConversion)fcList.get(i);
			fc_label.add(aFc.getConversionName());
			fc_value.add(aFc.getId().toString());
		}
		
		day_label = new ArrayList<String>();
		day_label.add(Constants.FORM_FIELD_LABEL_SUNDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_MONDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_TUESDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_WEDNESDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_THURSDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_FRIDAY);
		day_label.add(Constants.FORM_FIELD_LABEL_SATURDAY);
		
		day_value = new ArrayList<String>();
		day_value.add(Integer.toString(Calendar.SUNDAY));
		day_value.add(Integer.toString(Calendar.MONDAY));
		day_value.add(Integer.toString(Calendar.TUESDAY));
		day_value.add(Integer.toString(Calendar.WEDNESDAY));
		day_value.add(Integer.toString(Calendar.THURSDAY));
		day_value.add(Integer.toString(Calendar.FRIDAY));
		day_value.add(Integer.toString(Calendar.SATURDAY));
		
		month_label = new ArrayList<String>();
		month_label.add(Constants.FORM_FIELD_LABEL_JANUARY);
		month_label.add(Constants.FORM_FIELD_LABEL_FEBRUARY);
		month_label.add(Constants.FORM_FIELD_LABEL_MARCH);
		month_label.add(Constants.FORM_FIELD_LABEL_APRIL);
		month_label.add(Constants.FORM_FIELD_LABEL_MAY);
		month_label.add(Constants.FORM_FIELD_LABEL_JUNE);
		month_label.add(Constants.FORM_FIELD_LABEL_JULY);
		month_label.add(Constants.FORM_FIELD_LABEL_AUGUST);
		month_label.add(Constants.FORM_FIELD_LABEL_SEPTEMBER);
		month_label.add(Constants.FORM_FIELD_LABEL_OCTOBER);
		month_label.add(Constants.FORM_FIELD_LABEL_NOVEMBER);
		month_label.add(Constants.FORM_FIELD_LABEL_DECEMBER);
		
		month_value = new ArrayList<String>();
		month_value.add(Integer.toString(Calendar.JANUARY));
		month_value.add(Integer.toString(Calendar.FEBRUARY));
		month_value.add(Integer.toString(Calendar.MARCH));
		month_value.add(Integer.toString(Calendar.APRIL));
		month_value.add(Integer.toString(Calendar.MAY));
		month_value.add(Integer.toString(Calendar.JUNE));
		month_value.add(Integer.toString(Calendar.JULY));
		month_value.add(Integer.toString(Calendar.AUGUST));
		month_value.add(Integer.toString(Calendar.SEPTEMBER));
		month_value.add(Integer.toString(Calendar.OCTOBER));
		month_value.add(Integer.toString(Calendar.NOVEMBER));
		month_value.add(Integer.toString(Calendar.DECEMBER)); 
		
		ftpMode=null;
		ftpMode_label = new ArrayList<String>();
		ftpMode_label.add(Constants.FORM_FIELD_LABEL_FTP_MODE_INBOUND);
		ftpMode_label.add(Constants.FORM_FIELD_LABEL_FTP_MODE_OUTBOUND);
		ftpMode_value = new ArrayList<String>();
		ftpMode_value.add(Constants.FORM_FIELD_FTP_MODE_INBOUND);
		ftpMode_value.add(Constants.FORM_FIELD_FTP_MODE_OUTBOUND);
		
		ftpOwner=null;
		List result=DBUtilsCrsmsg.DBHQLCommand("from User where userStatus=? order by userName", new Object[]{Constants.STATUS_ACTIVE});
		ftpOwner_label = new ArrayList<String>();
		ftpOwner_value = new ArrayList<String>();
		for(int i=0;i<result.size();i++){
			User aUser=(User)result.get(i);
			ftpOwner_label.add(aUser.getUserName());
			ftpOwner_value.add(aUser.getUserId());
		}
		
		ftpDesc=null;
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if ( ftpName!=null&&ftpName.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_ENTRY_NAME));

		if ( ftpServer!=null&&ftpServer.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_SERVER_HOST));

		if (ftpPort!=null&&ftpPort.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_PORT));
		
		if (ftpTargetFile!=null&&ftpTargetFile.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_TARGET_FILE));
		
		/*if (messageTemplate!=null&&messageTemplate.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TEMPLATE_MESSAGE));
		*/
		if (ftpUserName!=null&&ftpUserName.length() == 0 )
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_USER_NAME));
		
		/*if(ftpId!=null&&ftpId.length()==0){
			if (ftpPassword!=null&&ftpPassword.length() == 0 )
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_PASSWORD));
			
			if (ftpPasswordConfirm!=null&&ftpPasswordConfirm.length() == 0 )
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_PASSWORD_CONFIRMATION));
		}*/
		if (ftpTimeMode!=null&&ftpTimeMode.length()==0){
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TIME_MODE));
		}else if (ftpTimeMode!=null&&ftpTimeMode.equals(Constants.FORM_FIELD_TIME_MODE_MONTHLY)){
			if((ftpDayOfMonth!=null&&ftpDayOfMonth.length()==0)||(ftpMonth!=null&&ftpMonth.length==0)||(ftpTime!=null&&ftpTime.length()==0)){
				errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TIME_MODE));
			}
		}else if(ftpTimeMode!=null&&ftpTimeMode.equals(Constants.FORM_FIELD_TIME_MODE_WEEKLY)){
			if((ftpDay!=null&&ftpDay.length==0)||(ftpTime!=null&&ftpTime.length()==0)){
				errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TIME_MODE));
			}
		}else if(ftpTimeMode!=null&&ftpTimeMode.equals(Constants.FORM_FIELD_TIME_MODE_HOURLY)){
			if(ftpHour!=null&&ftpHour.length()==0){
				errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TIME_MODE));
			}
		}
		
		if (ftpMode!=null&&ftpMode.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_MODE));
		
		if (fileConversion!=null&&fileConversion.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_FILE_CONVERSION));
		
		// Size Limit
		if ( ftpName!=null&&ftpName.length() >Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_ENTRY_NAME, Constants.LENGTH_100));
		
		if ( ftpTargetFile!=null&&ftpTargetFile.length() >Integer.valueOf(Constants.LENGTH_255))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_TARGET_FILE, Constants.LENGTH_255));
		
		
		if ( ftpServer!=null&&ftpServer.length() >Integer.valueOf(Constants.LENGTH_50))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_SERVER_HOST, Constants.LENGTH_50));
		
		if ( ftpPort!=null&&ftpPort.length() >Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_PORT, Constants.LENGTH_10));
		
		if ( notificationSetupSender!=null&&notificationSetupSender.length() >Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_NOTIFICATION_SETUP_SENDER, Constants.LENGTH_100));
		
		if ( notificationSetupReceiver!=null&&notificationSetupReceiver.length() >Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_NOTIFICATION_SETUP_RECEIVER, Constants.LENGTH_100));
		
		if ( ftpUserName!=null&&ftpUserName.length() >Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_USER_NAME, Constants.LENGTH_100));
		
		if ( ftpPassword!=null&&ftpPassword.length() >Integer.valueOf(Constants.LENGTH_50))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_PASSWORD, Constants.LENGTH_50));
		
		if ( ftpPasswordConfirm!=null&&ftpPasswordConfirm.length() >Integer.valueOf(Constants.LENGTH_50))
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_PASSWORD_CONFIRMATION, Constants.LENGTH_50));
		
		// Validation
		if ((ftpPassword!=null&&ftpPassword.length() > 0 )
				&&(ftpPasswordConfirm!=null&&ftpPasswordConfirm.length() > 0 )
				&&!ftpPassword.equals(ftpPasswordConfirm)){
			errors.add(Constants.FORM_FTP_SETUP,
					new ActionMessage(Constants.ERRMSG_FIELD_COMPARE_NOTMATCH, Constants.FORM_FIELD_SFTP_PASSWORD, Constants.FORM_FIELD_SFTP_PASSWORD_CONFIRMATION));
		}
		
		if(ftpTargetFile!=null&&ftpTargetFile.length()>0){
			ftpTargetFile=ftpTargetFile.replace(Constants.SPECIAL_SLASH, File.separator).replace(Constants.SPECIAL_BACKSLASH, File.separator).trim();
			List setupList=DBUtilsCrsmsg.DBHQLCommand("from FtpSetup where fileName=?", new Object[]{ftpTargetFile});
			if(setupList.size()>0){
				new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, Constants.FORM_FIELD_SFTP_TARGET_FILE+": "+ftpTargetFile);
			}
		}
		
		if (ftpRetryNo!=null&&ftpRetryNo.length()>0){
			if(ftpRetryNo.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_RETRY_NO, Constants.LENGTH_15));
			}
			if(!ftpRetryNo.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_SFTP_RETRY_NO, ftpRetryNo));
			}
		}
		
		if (ftpRetryPeriod!=null&&ftpRetryPeriod.length()>0){
			if(ftpRetryPeriod.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SFTP_RETRY_PERIOD, Constants.LENGTH_15));
			}
			if(!ftpRetryPeriod.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_FTP_SETUP,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_SFTP_RETRY_PERIOD, ftpRetryPeriod));
			}
		}

		return (errors);
		
	}


	/**
	 * @return Returns the approval_label.
	 */
	public ArrayList<String> getApproval_label() {
		return approval_label;
	}


	/**
	 * @param approval_label The approval_label to set.
	 */
	public void setApproval_label(ArrayList<String> approval_label) {
		this.approval_label = approval_label;
	}


	/**
	 * @return Returns the approval_value.
	 */
	public ArrayList<String> getApproval_value() {
		return approval_value;
	}


	/**
	 * @param approval_value The approval_value to set.
	 */
	public void setApproval_value(ArrayList<String> approval_value) {
		this.approval_value = approval_value;
	}


	/**
	 * @return Returns the ftpDay.
	 */
	public String[] getFtpDay() {
		return ftpDay;
	}


	/**
	 * @param ftpDay The ftpDay to set.
	 */
	public void setFtpDay(String[] ftpDay) {
		this.ftpDay = ftpDay;
	}


	/**
	 * @return Returns the ftpId.
	 */
	public String getFtpId() {
		return ftpId;
	}


	/**
	 * @param ftpId The ftpId to set.
	 */
	public void setFtpId(String ftpId) {
		this.ftpId = ftpId;
	}


	/**
	 * @return Returns the ftpMonth.
	 */
	public String[] getFtpMonth() {
		return ftpMonth;
	}


	/**
	 * @param ftpMonth The ftpMonth to set.
	 */
	public void setFtpMonth(String[] ftpMonth) {
		this.ftpMonth = ftpMonth;
	}


	/**
	 * @return Returns the ftpName.
	 */
	public String getFtpName() {
		return ftpName;
	}


	/**
	 * @param ftpName The ftpName to set.
	 */
	public void setFtpName(String ftpName) {
		this.ftpName = ftpName;
	}


	/**
	 * @return Returns the ftpPassword.
	 */
	public String getFtpPassword() {
		return ftpPassword;
	}


	/**
	 * @param ftpPassword The ftpPassword to set.
	 */
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}


	/**
	 * @return Returns the ftpPort.
	 */
	public String getFtpPort() {
		return ftpPort;
	}


	/**
	 * @param ftpPort The ftpPort to set.
	 */
	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}


	/**
	 * @return Returns the ftpServer.
	 */
	public String getFtpServer() {
		return ftpServer;
	}


	/**
	 * @param ftpServer The ftpServer to set.
	 */
	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}


	/**
	 * @return Returns the ftpTargetFile.
	 */
	public String getFtpTargetFile() {
		return ftpTargetFile;
	}


	/**
	 * @param ftpTargetFile The ftpTargetFile to set.
	 */
	public void setFtpTargetFile(String ftpTargetFile) {
		this.ftpTargetFile = ftpTargetFile;
	}


	/**
	 * @return Returns the ftpTime.
	 */
	public String getFtpTime() {
		return ftpTime;
	}


	/**
	 * @param ftpTime The ftpTime to set.
	 */
	public void setFtpTime(String ftpTime) {
		this.ftpTime = ftpTime;
	}


	/**
	 * @return Returns the ftpTimeMode.
	 */
	public String getFtpTimeMode() {
		return ftpTimeMode;
	}


	/**
	 * @param ftpTimeMode The ftpTimeMode to set.
	 */
	public void setFtpTimeMode(String ftpTimeMode) {
		this.ftpTimeMode = ftpTimeMode;
	}


	/**
	 * @return Returns the ftpUserName.
	 */
	public String getFtpUserName() {
		return ftpUserName;
	}


	/**
	 * @param ftpUserName The ftpUserName to set.
	 */
	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}


	/**
	 * @return Returns the messageTemplate.
	 */
	public String getMessageTemplate() {
		return messageTemplate;
	}


	/**
	 * @param messageTemplate The messageTemplate to set.
	 */
	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}


	/**
	 * @return Returns the notificationSetupSender.
	 */
	public String getNotificationSetupSender() {
		return notificationSetupSender;
	}


	/**
	 * @param notificationSetupSender The notificationSetupSender to set.
	 */
	public void setNotificationSetupSender(String notificationSetupSender) {
		this.notificationSetupSender = notificationSetupSender;
	}


	/**
	 * @return Returns the notificationSetupStatus.
	 */
	public String getNotificationSetupStatus() {
		return notificationSetupStatus;
	}


	/**
	 * @param notificationSetupStatus The notificationSetupStatus to set.
	 */
	public void setNotificationSetupStatus(String notificationSetupStatus) {
		this.notificationSetupStatus = notificationSetupStatus;
	}
/*

	*//**
	 * @return Returns the securityKey.
	 *//*
	public String getSecurityKey() {
		return securityKey;
	}


	*//**
	 * @param securityKey The securityKey to set.
	 *//*
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
*/
	/**
	 * @return Returns the setupStatus.
	 */
	public String getSetupStatus() {
		return setupStatus;
	}


	/**
	 * @param setupStatus The setupStatus to set.
	 */
	public void setSetupStatus(String setupStatus) {
		this.setupStatus = setupStatus;
	}


	/**
	 * @return Returns the smsAutoApproval.
	 */
	public String getSmsAutoApproval() {
		return smsAutoApproval;
	}


	/**
	 * @param smsAutoApproval The smsAutoApproval to set.
	 */
	public void setSmsAutoApproval(String smsAutoApproval) {
		this.smsAutoApproval = smsAutoApproval;
	}


	/**
	 * @return Returns the smsScheduled.
	 */
	public String getSmsScheduled() {
		return smsScheduled;
	}


	/**
	 * @param smsScheduled The smsScheduled to set.
	 */
	public void setSmsScheduled(String smsScheduled) {
		this.smsScheduled = smsScheduled;
	}


	/**
	 * @return Returns the smsScheduledTime.
	 */
	public String getSmsScheduledTime() {
		return smsScheduledTime;
	}


	/**
	 * @param smsScheduledTime The smsScheduledTime to set.
	 */
	public void setSmsScheduledTime(String smsScheduledTime) {
		this.smsScheduledTime = smsScheduledTime;
	}


	/**
	 * @return Returns the notificationSetupReceiver.
	 */
	public String getNotificationSetupReceiver() {
		return notificationSetupReceiver;
	}


	/**
	 * @param notificationSetupReceiver The notificationSetupReceiver to set.
	 */
	public void setNotificationSetupReceiver(String notificationSetupReceiver) {
		this.notificationSetupReceiver = notificationSetupReceiver;
	}


	/**
	 * @return Returns the ftpHour.
	 */
	public String getFtpHour() {
		return ftpHour;
	}


	/**
	 * @param ftpHour The ftpHour to set.
	 */
	public void setFtpHour(String ftpHour) {
		this.ftpHour = ftpHour;
	}


	/**
	 * @return Returns the ftpHourFrom.
	 */
	public String getFtpHourFrom() {
		return ftpHourFrom;
	}


	/**
	 * @param ftpHourFrom The ftpHourFrom to set.
	 */
	public void setFtpHourFrom(String ftpHourFrom) {
		this.ftpHourFrom = ftpHourFrom;
	}


	/**
	 * @return Returns the ftpHourTo.
	 */
	public String getFtpHourTo() {
		return ftpHourTo;
	}


	/**
	 * @param ftpHourTo The ftpHourTo to set.
	 */
	public void setFtpHourTo(String ftpHourTo) {
		this.ftpHourTo = ftpHourTo;
	}


	/**
	 * @return Returns the systemDateTime.
	 */
	public String getSystemDateTime() {
		return systemDateTime;
	}


	/**
	 * @param systemDateTime The systemDateTime to set.
	 */
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}


	/**
	 * @return Returns the ftpDayOfMonth.
	 */
	public String getFtpDayOfMonth() {
		return ftpDayOfMonth;
	}


	/**
	 * @param ftpDayOfMonth The ftpDayOfMonth to set.
	 */
	public void setFtpDayOfMonth(String ftpDayOfMonth) {
		this.ftpDayOfMonth = ftpDayOfMonth;
	}


	/**
	 * @return Returns the ftpPasswordConfirm.
	 */
	public String getFtpPasswordConfirm() {
		return ftpPasswordConfirm;
	}


	/**
	 * @param ftpPasswordConfirm The ftpPasswordConfirm to set.
	 */
	public void setFtpPasswordConfirm(String ftpPasswordCOnfirm) {
		this.ftpPasswordConfirm = ftpPasswordCOnfirm;
	}


	/**
	 * @return Returns the fc_label.
	 */
	public ArrayList<String> getFc_label() {
		return fc_label;
	}


	/**
	 * @param fc_label The fc_label to set.
	 */
	public void setFc_label(ArrayList<String> fc_label) {
		this.fc_label = fc_label;
	}


	/**
	 * @return Returns the fc_value.
	 */
	public ArrayList<String> getFc_value() {
		return fc_value;
	}


	/**
	 * @param fc_value The fc_value to set.
	 */
	public void setFc_value(ArrayList<String> fc_value) {
		this.fc_value = fc_value;
	}


	/**
	 * @return Returns the fileConversion.
	 */
	public String getFileConversion() {
		return fileConversion;
	}


	/**
	 * @param fileConversion The fileConversion to set.
	 */
	public void setFileConversion(String fileConversion) {
		this.fileConversion = fileConversion;
	}


	public ArrayList<String> getDay_label() {
		return day_label;
	}


	public void setDay_label(ArrayList<String> day_label) {
		this.day_label = day_label;
	}


	public ArrayList<String> getDay_value() {
		return day_value;
	}


	public void setDay_value(ArrayList<String> day_value) {
		this.day_value = day_value;
	}


	public ArrayList<String> getMonth_label() {
		return month_label;
	}


	public void setMonth_label(ArrayList<String> month_label) {
		this.month_label = month_label;
	}


	public ArrayList<String> getMonth_value() {
		return month_value;
	}


	public void setMonth_value(ArrayList<String> month_value) {
		this.month_value = month_value;
	}


	public FormFile getSecurityKey() {
		return securityKey;
	}


	public void setSecurityKey(FormFile securityKey) {
		this.securityKey = securityKey;
	}


	public String getFtpMode() {
		return ftpMode;
	}


	public void setFtpMode(String ftpMode) {
		this.ftpMode = ftpMode;
	}


	public ArrayList<String> getFtpMode_label() {
		return ftpMode_label;
	}


	public void setFtpMode_label(ArrayList<String> ftpMode_label) {
		this.ftpMode_label = ftpMode_label;
	}


	public ArrayList<String> getFtpMode_value() {
		return ftpMode_value;
	}


	public void setFtpMode_value(ArrayList<String> ftpMode_value) {
		this.ftpMode_value = ftpMode_value;
	}


	public String getFtpOwner() {
		return ftpOwner;
	}


	public void setFtpOwner(String ftpOwner) {
		this.ftpOwner = ftpOwner;
	}


	public ArrayList<String> getFtpOwner_label() {
		return ftpOwner_label;
	}


	public void setFtpOwner_label(ArrayList<String> ftpOwner_label) {
		this.ftpOwner_label = ftpOwner_label;
	}


	public ArrayList<String> getFtpOwner_value() {
		return ftpOwner_value;
	}


	public void setFtpOwner_value(ArrayList<String> ftpOwner_value) {
		this.ftpOwner_value = ftpOwner_value;
	}


	public String getAdvanceDayLimit() {
		return advanceDayLimit;
	}


	public void setAdvanceDayLimit(String advanceDayLimit) {
		this.advanceDayLimit = advanceDayLimit;
	}


	public String getFtpRetryNo() {
		return ftpRetryNo;
	}


	public void setFtpRetryNo(String ftpRetryNo) {
		this.ftpRetryNo = ftpRetryNo;
	}


	public String getFtpRetryPeriod() {
		return ftpRetryPeriod;
	}


	public void setFtpRetryPeriod(String ftpRetryPeriod) {
		this.ftpRetryPeriod = ftpRetryPeriod;
	}


	public String getFtpDesc() {
		return ftpDesc;
	}


	public void setFtpDesc(String ftpDesc) {
		this.ftpDesc = ftpDesc;
	}	
	
}

