package com.bcb.crsmsg.forms;

import org.apache.struts.action.ActionForm;

public class FtpReportDetailForm extends ActionForm {
	
	private static final long serialVersionUID = -6753115354332872923L;
	private String totalUnsent;
	private String totalQueue;
	private String totalSent;
	private String totalPendingApproval;
	private String totalPendingVerification;
	
	public String getTotalQueue() {
		return totalQueue;
	}
	public void setTotalQueue(String totalQueue) {
		this.totalQueue = totalQueue;
	}
	public String getTotalSent() {
		return totalSent;
	}
	public void setTotalSent(String totalSent) {
		this.totalSent = totalSent;
	}
	public String getTotalUnsent() {
		return totalUnsent;
	}
	public void setTotalUnsent(String totalUnsent) {
		this.totalUnsent = totalUnsent;
	}
	public String getTotalPendingApproval() {
		return totalPendingApproval;
	}
	public void setTotalPendingApproval(String totalPendingApproval) {
		this.totalPendingApproval = totalPendingApproval;
	}
	public String getTotalPendingVerification() {
		return totalPendingVerification;
	}
	public void setTotalPendingVerification(String totalPendingVerification) {
		this.totalPendingVerification = totalPendingVerification;
	}
	
	

	
}
