package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;

public class FtpReportForm extends ActionForm {


	private static final long serialVersionUID = 7338188937827461681L;
	
	
	private String butSubmit;
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private String searchkey;
	private String selectedState;
	private String hiddenSel;
	 
	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String selectedUntilDate;
	private String selectedFromDate;
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		butSubmit=null;
		searchby_label = new ArrayList();
		searchby_label.add("Name");
		searchby_label.add("Dept");
		
		searchby_value = new ArrayList();
		searchby_value.add("Name");
		searchby_value.add("Dept");
		
		
	}	
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (butSubmit!=null&&selectedFromDate!=null&&selectedFromDate.length()==Integer.valueOf(Constants.LENGTH_0)||
				butSubmit!=null&&selectedUntilDate!=null&&selectedUntilDate.trim().length()==Integer.valueOf(Constants.LENGTH_0))
			errors.add(Constants.FORM_FTP_REPORT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_LABEL_DATE_RANGE));
		
		return (errors);
		
	}
	
	
	/**
	 * @return Returns the butSubmit.
	 */
	public String getButSubmit() {
		return butSubmit;
	}
	
	/**
	 * @param butSubmit The butSubmit to set.
	 */
	public void setButSubmit(String butSubmit) {
		this.butSubmit = butSubmit;
	}

	

	

	public ArrayList getSearchby_label() {
		return searchby_label;
	}

	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}

	public ArrayList getSearchby_value() {
		return searchby_value;
	}

	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	public String getHiddenSel() {
		return hiddenSel;
	}

	public void setHiddenSel(String hiddenSel) {
		this.hiddenSel = hiddenSel;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getSelectedFromDate() {
		return selectedFromDate;
	}

	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}

	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}

	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}

	
}
