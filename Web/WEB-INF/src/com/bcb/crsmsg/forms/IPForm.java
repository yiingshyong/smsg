package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;

public final class IPForm extends ActionForm {


	
	private String id;
	private String ip;
	private String dept;
	private String remarks;
	private String system;
	
	
	
	
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;

	
	

	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		System.out.println("inside IPForm reset");
		id=null;
		ip=null;
		dept=null;
		remarks=null;
		system=null;
		
		
		selected_status=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_label.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		status_value = new ArrayList<String>();
		status_value.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_value.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		
	}


	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		//if ip address field is empty
		if(ip == ""){
			errors.add(Constants.FORM_IP,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_IPADDRESS));
		}
		//Validate format
			if (ip!=null&&ip.length()>Integer.valueOf(Constants.LENGTH_0)){
					String[] ipList=ip.split(Constants.SPECIAL_CHAR_NEWLINE);
					Pattern p = Pattern.compile("^\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}$");
					
					String error=Constants.STR_EMPTY;
					
					for(int i=0;i<ipList.length;i++){
						Matcher m = p.matcher(ipList[i]);
					    if(!m.matches()){
					    	if(i!=0){
								error+=", ";
							}
					    	
					    	error+=ipList[i];
					    }
					}
					if(error.length()>0){
						errors.add(Constants.FORM_IP,
				    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_IPADDRESS, error));
					}
			}

		return (errors);
		
	}


	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}





	public String getIp() {
		return ip;
	}





	public void setIp(String ip) {
		this.ip = ip;
	}





	public String getDept() {
		return dept;
	}





	public void setDept(String dept) {
		this.dept = dept;
	}





	public String getRemarks() {
		return remarks;
	}





	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}





	public String getSystem() {
		return system;
	}





	public void setSystem(String system) {
		this.system = system;
	}





	public String getSelected_status() {
		return selected_status;
	}





	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}





	public ArrayList<String> getStatus_label() {
		return status_label;
	}





	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}





	public ArrayList<String> getStatus_value() {
		return status_value;
	}





	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	/*public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (keyword!=null&&keyword.length()==Integer.valueOf(Constants.LENGTH_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_KEYWORD));
		
		if (selected_ownership!=null&&selected_ownership.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_OWNERSHIP));
		
		if (selected_charset!=null&&selected_charset.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CHARSET));
		
		if (selected_status!=null&&selected_status.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_STATUS));
		
		// Size Limit
		if (keyword!=null&&keyword.length()>Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_KEYWORD, Constants.LENGTH_100));
		
		if (redirectURL!=null&&redirectURL.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_URL, Constants.LENGTH_1000));
		
		//maxRedirect_attempts is mandatory when a redirect url is entered
		if ((redirectURL!="") && (maxRedirect_attempts=="")){
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_LIMIT));
		}
		
		//redirectInterval is mandatory when a redirect url is entered
		if ((redirectURL!="") && (redirectInterval=="")){
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_INTERVAL));
		}
		
		if (redirectURLParam!=null){
			if(redirectURLParam.length()>Integer.valueOf(Constants.LENGTH_200))
				errors.add(Constants.FORM_KEYWORD,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_URL_PARAM, Constants.LENGTH_200));
		
			String[] paramList=redirectURLParam.split(Constants.SPECIAL_CHAR_COMMA);
			for(int i=0;i<paramList.length;i++){
				paramList[i]=paramList[i].trim();
			}
			redirectURLParam=CommonUtils.arrayToString(paramList, Constants.SPECIAL_CHAR_PIPE);
		}
		
		if (redirectEmail!=null&&redirectEmail.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_EMAIL, Constants.LENGTH_1000));
		
		if (redirectMobileNo!=null&&redirectMobileNo.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_MOBILENO, Constants.LENGTH_1000));
		
		if (autoReplyMessage!=null&&autoReplyMessage.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REPLY_MSG, Constants.LENGTH_500));
		
//		 Validation
		if (redirectMobileNo!=null&&redirectMobileNo.length()>Integer.valueOf(Constants.LENGTH_0)){
			String[] mobileNoList=redirectMobileNo.split(Constants.SPECIAL_CHAR_NEWLINE);
//			Pattern p = Pattern.compile("^[6][05][189]\\d{7,9}$");
			 mobile no. pattern accepted
			 601NNNNNNNN
			 658NNNNNNNN
			 659NNNNNNNN
			 
			String error=Constants.STR_EMPTY;
			
			for(int i=0;i<mobileNoList.length;i++){
//				Matcher m = p.matcher(BusinessService.normalizeMobile(mobileNoList[i]));
			    if(!BusinessService.validateMobileNo(mobileNoList[i])){
			    	if(i!=0){
						error+=", ";
					}
			    	
			    	error+=mobileNoList[i];
			    }
			}
			
			if(error.length()>0){
				errors.add(Constants.FORM_KEYWORD,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_REDIRECT_MOBILENO, error));
			}
		}
		
		if (maxRedirect_attempts!=null&&maxRedirect_attempts.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_LIMIT, Constants.LENGTH_10));
					
		if (redirectInterval!=null&&redirectInterval.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_INTERVAL, Constants.LENGTH_10));					
		
		
		return (errors);
		
	}*/


	
	
	

}
