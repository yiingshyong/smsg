package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;



public final class KeywordForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5117685801715630360L;
	private String keywordId;
	private String keyword;
	private String autoExpired;
	private String expiryDate;
	private String redirectURL;
	private String redirectURLParam;
	private String redirectEmail;
	private String redirectMobileNo;
	private String autoReply;
	private String autoReplyMessage;
	//keyword redirect
	private String maxRedirect_attempts;
	private String redirectInterval;
	
	private String selected_ownership;
	private ArrayList<String> ownership_label=null;
	private ArrayList<String> ownership_value=null;

	private String selected_charset;
	private ArrayList<String> charset_label=null;
	private ArrayList<String> charset_value=null;
	
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;

	private String selectedAutoAction;
	private ArrayList<Option> autoActions = null;
	
	private String encryptRedirectUrl;
	
	private String selected_encryptRedirectUrl;
	private ArrayList<String> encryptRedirectUrl_label=null;
	private ArrayList<String> encryptRedirectUrl_value=null;

	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		keywordId=null;
		keyword=null;
		autoExpired=null;
		expiryDate=null;
		redirectURL=null;
		redirectURLParam=null;
		redirectEmail=null;
		redirectMobileNo=null;
		autoReply=null;
		autoReplyMessage=null;
		selectedAutoAction=null;
		
		maxRedirect_attempts=null;
		redirectInterval=null;
		
		selected_ownership=null;
		ownership_label = new ArrayList<String>();
		ownership_value = new ArrayList<String>();

		selected_charset=null;
		charset_label = new ArrayList<String>();
		charset_label.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_label.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		charset_value = new ArrayList<String>();
		charset_value.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_value.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		selected_status=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_label.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		status_value = new ArrayList<String>();
		status_value.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_value.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		autoActions = (ArrayList<Option>) CommonUtils.getNonDefaultAutoActions();
		
		encryptRedirectUrl = null;
		
		selected_encryptRedirectUrl=null;
		encryptRedirectUrl_label = new ArrayList<String>();
		encryptRedirectUrl_label.add(Constants.FORM_FIELD_STATUS_NO);
		encryptRedirectUrl_label.add(Constants.FORM_FIELD_STATUS_YES);
		
		encryptRedirectUrl_value = new ArrayList<String>();
		encryptRedirectUrl_value.add(Constants.FORM_FIELD_STATUS_NO);
		encryptRedirectUrl_value.add(Constants.FORM_FIELD_STATUS_YES);
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (keyword!=null&&keyword.length()==Integer.valueOf(Constants.LENGTH_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_KEYWORD));
		
		if (selected_ownership!=null&&selected_ownership.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_OWNERSHIP));
		
		if (selected_charset!=null&&selected_charset.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CHARSET));
		
		if (selected_status!=null&&selected_status.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_STATUS));
		
		// Size Limit
		if (keyword!=null&&keyword.length()>Integer.valueOf(Constants.LENGTH_100))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_KEYWORD, Constants.LENGTH_100));
		
		if (redirectURL!=null&&redirectURL.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_URL, Constants.LENGTH_1000));
		
		//maxRedirect_attempts is mandatory when a redirect url is entered
		if ((redirectURL!="") && (maxRedirect_attempts=="")){
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_LIMIT));
		}
		
		//redirectInterval is mandatory when a redirect url is entered
		if ((redirectURL!="") && (redirectInterval=="")){
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_INTERVAL));
		}
		
		if (redirectURLParam!=null){
			if(redirectURLParam.length()>Integer.valueOf(Constants.LENGTH_200))
				errors.add(Constants.FORM_KEYWORD,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_URL_PARAM, Constants.LENGTH_200));
		
			String[] paramList=redirectURLParam.split(Constants.SPECIAL_CHAR_COMMA);
			for(int i=0;i<paramList.length;i++){
				paramList[i]=paramList[i].trim();
			}
			redirectURLParam=CommonUtils.arrayToString(paramList, Constants.SPECIAL_CHAR_PIPE);
		}
		
		if (redirectEmail!=null&&redirectEmail.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_EMAIL, Constants.LENGTH_1000));
		
		if (redirectMobileNo!=null&&redirectMobileNo.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_REDIRECT_MOBILENO, Constants.LENGTH_1000));
		
		if (autoReplyMessage!=null&&autoReplyMessage.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REPLY_MSG, Constants.LENGTH_500));
		
//		 Validation
		if (redirectMobileNo!=null&&redirectMobileNo.length()>Integer.valueOf(Constants.LENGTH_0)){
			String[] mobileNoList=redirectMobileNo.split(Constants.SPECIAL_CHAR_NEWLINE);
//			Pattern p = Pattern.compile("^[6][05][189]\\d{7,9}$");
			/* mobile no. pattern accepted
			 601NNNNNNNN
			 658NNNNNNNN
			 659NNNNNNNN
			 */
			String error=Constants.STR_EMPTY;
			
			for(int i=0;i<mobileNoList.length;i++){
//				Matcher m = p.matcher(BusinessService.normalizeMobile(mobileNoList[i]));
			    if(!BusinessService.validateMobileNo(mobileNoList[i])){
			    	if(i!=0){
						error+=", ";
					}
			    	
			    	error+=mobileNoList[i];
			    }
			}
			
			if(error.length()>0){
				errors.add(Constants.FORM_KEYWORD,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_REDIRECT_MOBILENO, error));
			}
		}
		
		if (maxRedirect_attempts!=null&&maxRedirect_attempts.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_LIMIT, Constants.LENGTH_10));
					
		if (redirectInterval!=null&&redirectInterval.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_KEYWORD,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_REDIRECT_SMS_INTERVAL, Constants.LENGTH_10));					
		
		
		return (errors);
		
	}


	/**
	 * @return Returns the autoExpired.
	 */
	public String getAutoExpired() {
		return autoExpired;
	}


	/**
	 * @param autoExpired The autoExpired to set.
	 */
	public void setAutoExpired(String autoExpired) {
		this.autoExpired = autoExpired;
	}


	/**
	 * @return Returns the autoReply.
	 */
	public String getAutoReply() {
		return autoReply;
	}


	/**
	 * @param autoReply The autoReply to set.
	 */
	public void setAutoReply(String autoReply) {
		this.autoReply = autoReply;
	}


	/**
	 * @return Returns the autoReplyMessage.
	 */
	public String getAutoReplyMessage() {
		return autoReplyMessage;
	}


	/**
	 * @param autoReplyMessage The autoReplyMessage to set.
	 */
	public void setAutoReplyMessage(String autoReplyMessage) {
		this.autoReplyMessage = autoReplyMessage;
	}


	/**
	 * @return Returns the charset_label.
	 */
	public ArrayList<String> getCharset_label() {
		return charset_label;
	}


	/**
	 * @param charset_label The charset_label to set.
	 */
	public void setCharset_label(ArrayList<String> charset_label) {
		this.charset_label = charset_label;
	}


	/**
	 * @return Returns the charset_value.
	 */
	public ArrayList<String> getCharset_value() {
		return charset_value;
	}


	/**
	 * @param charset_value The charset_value to set.
	 */
	public void setCharset_value(ArrayList<String> charset_value) {
		this.charset_value = charset_value;
	}


	/**
	 * @return Returns the expiryDate.
	 */
	public String getExpiryDate() {
		return expiryDate;
	}


	/**
	 * @param expiryDate The expiryDate to set.
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	/**
	 * @return Returns the keyword.
	 */
	public String getKeyword() {
		return keyword;
	}


	/**
	 * @param keyword The keyword to set.
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}


	/**
	 * @return Returns the ownership_label.
	 */
	public ArrayList<String> getOwnership_label() {
		return ownership_label;
	}


	/**
	 * @param ownership_label The ownership_label to set.
	 */
	public void setOwnership_label(ArrayList<String> ownership_label) {
		this.ownership_label = ownership_label;
	}


	/**
	 * @return Returns the ownership_value.
	 */
	public ArrayList<String> getOwnership_value() {
		return ownership_value;
	}


	/**
	 * @param ownership_value The ownership_value to set.
	 */
	public void setOwnership_value(ArrayList<String> ownership_value) {
		this.ownership_value = ownership_value;
	}


	/**
	 * @return Returns the redirectEmail.
	 */
	public String getRedirectEmail() {
		return redirectEmail;
	}


	/**
	 * @param redirectEmail The redirectEmail to set.
	 */
	public void setRedirectEmail(String redirectEmail) {
		this.redirectEmail = redirectEmail;
	}


	/**
	 * @return Returns the redirectMobileNo.
	 */
	public String getRedirectMobileNo() {
		return redirectMobileNo;
	}


	/**
	 * @param redirectMobileNo The redirectMobileNo to set.
	 */
	public void setRedirectMobileNo(String redirectMobileNo) {
		this.redirectMobileNo = redirectMobileNo;
	}


	/**
	 * @return Returns the redirectURL.
	 */
	public String getRedirectURL() {
		return redirectURL;
	}


	/**
	 * @param redirectURL The redirectURL to set.
	 */
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}


	/**
	 * @return Returns the selected_charset.
	 */
	public String getSelected_charset() {
		return selected_charset;
	}


	/**
	 * @param selected_charset The selected_charset to set.
	 */
	public void setSelected_charset(String selected_charset) {
		this.selected_charset = selected_charset;
	}


	/**
	 * @return Returns the selected_ownership.
	 */
	public String getSelected_ownership() {
		return selected_ownership;
	}


	/**
	 * @param selected_ownership The selected_ownership to set.
	 */
	public void setSelected_ownership(String selected_ownership) {
		this.selected_ownership = selected_ownership;
	}


	/**
	 * @return Returns the selected_status.
	 */
	public String getSelected_status() {
		return selected_status;
	}


	/**
	 * @param selected_status The selected_status to set.
	 */
	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	/**
	 * @return Returns the status_label.
	 */
	public ArrayList<String> getStatus_label() {
		return status_label;
	}


	/**
	 * @param status_label The status_label to set.
	 */
	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}


	/**
	 * @return Returns the status_value.
	 */
	public ArrayList<String> getStatus_value() {
		return status_value;
	}


	/**
	 * @param status_value The status_value to set.
	 */
	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}


	/**
	 * @return Returns the keywordId.
	 */
	public String getKeywordId() {
		return keywordId;
	}


	/**
	 * @param keywordId The keywordId to set.
	 */
	public void setKeywordId(String keywordId) {
		this.keywordId = keywordId;
	}


	public String getRedirectURLParam() {
		return redirectURLParam;
	}


	public void setRedirectURLParam(String redirectURLParam) {
		this.redirectURLParam = redirectURLParam;
	}


	public String getSelectedAutoAction() {
		return selectedAutoAction;
	}


	public void setSelectedAutoAction(String selectedAutoAction) {
		this.selectedAutoAction = selectedAutoAction;
	}


	public List<Option> getAutoActions() {
		return autoActions;
	}


	public void setAutoActions(List<Option> autoActions) {
		this.autoActions = (ArrayList<Option>) autoActions;
	}
	
	
	public String getMaxRedirect_attempts() {
		System.out.println("maxRedirectAttempts=="+maxRedirect_attempts);
		if (maxRedirect_attempts == ""){
			maxRedirect_attempts = null;
		}
		return maxRedirect_attempts;
	}


	public void setMaxRedirect_attempts(String maxRedirect_attempts) {
		this.maxRedirect_attempts = maxRedirect_attempts;
	}
	
	public String getRedirectInterval() {
		return redirectInterval;
	}


	
	public void setRedirectInterval(String redirectInterval) {
		this.redirectInterval = redirectInterval;
	}
	
	public String getSelected_encryptRedirectUrl() {
		return selected_encryptRedirectUrl;
	}


	public void setSelected_encryptRedirectUrl(String selected_encryptRedirectUrl) {
		this.selected_encryptRedirectUrl = selected_encryptRedirectUrl;
	}


	public ArrayList<String> getEncryptRedirectUrl_label() {
		return encryptRedirectUrl_label;
	}


	public void setEncryptRedirectUrl_label(
			ArrayList<String> encryptRedirectUrl_label) {
		this.encryptRedirectUrl_label = encryptRedirectUrl_label;
	}


	public ArrayList<String> getEncryptRedirectUrl_value() {
		return encryptRedirectUrl_value;
	}


	public void setEncryptRedirectUrl_value(
			ArrayList<String> encryptRedirectUrl_value) {
		this.encryptRedirectUrl_value = encryptRedirectUrl_value;
	}
	
	
	public String getEncryptRedirectUrl() {
	return encryptRedirectUrl;
	}


	public void setEncryptRedirectUrl(String encryptRedirectUrl) {
		this.encryptRedirectUrl = encryptRedirectUrl;
	}
}

