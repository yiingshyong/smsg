package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.SmsMQ;
import com.bcb.crsmsg.util.Constants;

public class QueueListenerForm extends ActionForm{

	private static final long serialVersionUID = 1L;
	private ArrayList<SmsMQ> mqs;
	private SmsMQ mq;
	private int id;

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.mq = new SmsMQ();
	}

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if(this.mq.getMqId() == 0){
			errors.add(Constants.FORM_QUEUE_LISTENER_CONFIG, new ActionMessage(Constants.ERRMSG_SELECT, "Record"));			
		}
		if(this.mq.getNoOfThread() == 0){
			errors.add(Constants.FORM_QUEUE_LISTENER_CONFIG, new ActionMessage(Constants.ERRMSG_FIELD_MUST_GREATER_THAN, 0));
		}
		return errors;
	}

	public SmsMQ getMq() {
		return mq;
	}

	public void setMq(SmsMQ mq) {
		this.mq = mq;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<SmsMQ> getMqs() {
		return mqs;
	}

	public void setMqs(List<SmsMQ> mqs) {
		this.mqs = (ArrayList<SmsMQ>) mqs;
	}
	
	

}
