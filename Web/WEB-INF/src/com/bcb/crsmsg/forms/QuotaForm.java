package com.bcb.crsmsg.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;

public class QuotaForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7011158716148213337L;
	private String quota;
	private String topUpMode;	
	private String[] checkedRecords;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		topUpMode = Constants.FORM_FIELD_QUOTA_TOPUP_MODE_TOPUP;
		quota=Constants.STR_EMPTY;
		checkedRecords=null;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(topUpMode!=null&&topUpMode.length()==Integer.valueOf(Constants.LENGTH_0)){
			errors.add(Constants.FORM_QUOTA, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_QUOTA_TOPUP_MODE));			
		}
		
		if(topUpMode!=null&&topUpMode.equals(Constants.FORM_FIELD_QUOTA_TOPUP_MODE_TOPUP)){
			if(quota!=null&&quota.length()==Integer.valueOf(Constants.LENGTH_0)){
				errors.add(Constants.FORM_QUOTA, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_QUOTA));			
			}
		}
		
		if(quota!=null&&quota.length()>Integer.valueOf(Constants.LENGTH_9)){
			errors.add(Constants.FORM_QUOTA, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUOTA, Constants.LENGTH_2 ));			
		}

		if(quota!=null&&quota.matches("^\\D{1,9}$")){
			errors.add(Constants.FORM_QUOTA, new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUOTA, quota ));			
		}
		
		return (errors);
		
	}

	/**
	 * @return Returns the quota.
	 */
	public String getQuota() {
		return quota;
	}

	/**
	 * @param quota The quota to set.
	 */
	public void setQuota(String searchkey) {
		this.quota = searchkey;
	}

	/**
	 * @return Returns the topUpMode.
	 */
	public String getTopUpMode() {
		return topUpMode;
	}

	/**
	 * @param topUpMode The topUpMode to set.
	 */
	public void setTopUpMode(String selectedState) {
		this.topUpMode = selectedState;
	}

	/**
	 * @return Returns the checkedRecords.
	 */
	public String[] getCheckedRecords() {
		return checkedRecords;
	}

	/**
	 * @param checkedRecords The checkedRecords to set.
	 */
	public void setCheckedRecords(String[] checkedRecords) {
		this.checkedRecords = checkedRecords;
	}

	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}
