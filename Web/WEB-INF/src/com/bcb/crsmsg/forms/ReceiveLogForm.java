package com.bcb.crsmsg.forms;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.bcb.crsmsg.util.Constants;



public final class ReceiveLogForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7830072404274120870L;
	private String prID;
	private String selPR;
	private String hiddenSel;
	private ArrayList searchby_label;
	private ArrayList searchby_value;
	private ArrayList searchbyserver_label;
	private ArrayList searchbyserver_value;
	private String searchkey;
	private String selectedState;
	private String selectedServer;
	private String delete;
	
	private String searchBy;
	private String[] selectedMsg;
	private String butSearch;
	
	private String selectedUntilDate;
	private String selectedFromDate;
		
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		butSearch= null;
//		selectedState = "";
		delete=null;

		searchby_label = new ArrayList();
		searchby_label.add(Constants.STR_BY_MOBILE);
		searchby_label.add(Constants.STR_BY_CUST_REF_NO);
		searchby_label.add(Constants.STR_BY_KEYWORD);

		searchby_value = new ArrayList();
		searchby_value.add(Constants.STR_BY_MOBILE);
		searchby_value.add(Constants.STR_BY_CUST_REF_NO);
		searchby_value.add(Constants.STR_BY_KEYWORD);
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
//		if ( ftpName!=null&&ftpName.length() == 0 )
//			errors.add(Constants.FORM_FTP_SETUP,
//					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SFTP_ENTRY_NAME));

		
		return (errors);
		
	}


	public String getButSearch() {
		return butSearch;
	}


	public void setButSearch(String butSearch) {
		this.butSearch = butSearch;
	}


	public String getDelete() {
		return delete;
	}


	public void setDelete(String delete) {
		this.delete = delete;
	}


	public String getHiddenSel() {
		return hiddenSel;
	}


	public void setHiddenSel(String hiddenSel) {
		this.hiddenSel = hiddenSel;
	}


	public String getPrID() {
		return prID;
	}


	public void setPrID(String prID) {
		this.prID = prID;
	}


	public String getSearchBy() {
		return searchBy;
	}


	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}


	public ArrayList getSearchby_label() {
		return searchby_label;
	}


	public void setSearchby_label(ArrayList searchby_label) {
		this.searchby_label = searchby_label;
	}


	public ArrayList getSearchby_value() {
		return searchby_value;
	}


	public void setSearchby_value(ArrayList searchby_value) {
		this.searchby_value = searchby_value;
	}


	public String getSearchkey() {
		return searchkey;
	}


	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}


	public String getSelectedFromDate() {
		return selectedFromDate;
	}


	public void setSelectedFromDate(String selectedFromDate) {
		this.selectedFromDate = selectedFromDate;
	}


	public String getSelectedState() {
		return selectedState;
	}


	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}


	public String getSelectedUntilDate() {
		return selectedUntilDate;
	}


	public void setSelectedUntilDate(String selectedUntilDate) {
		this.selectedUntilDate = selectedUntilDate;
	}


	public String getSelPR() {
		return selPR;
	}


	public void setSelPR(String selPR) {
		this.selPR = selPR;
	}


	public String[] getSelectedMsg() {
		return selectedMsg;
	}


	public void setSelectedMsg(String[] selectedMsg) {
		this.selectedMsg = selectedMsg;
	}


	public ArrayList getSearchbyserver_label() {
		return searchbyserver_label;
	}


	public void setSearchbyserver_label(ArrayList searchbyserver_label) {
		this.searchbyserver_label = searchbyserver_label;
	}


	public ArrayList getSearchbyserver_value() {
		return searchbyserver_value;
	}


	public void setSearchbyserver_value(ArrayList searchbyserver_value) {
		this.searchbyserver_value = searchbyserver_value;
	}


	public String getSelectedServer() {
		return selectedServer;
	}


	public void setSelectedServer(String selectedServer) {
		this.selectedServer = selectedServer;
	}

	
	


	
}

