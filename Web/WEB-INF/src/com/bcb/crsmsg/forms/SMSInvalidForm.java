package com.bcb.crsmsg.forms;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;



public final class SMSInvalidForm extends ActionForm {
	private static final long serialVersionUID = -320167851872235484L;
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
	}
}

