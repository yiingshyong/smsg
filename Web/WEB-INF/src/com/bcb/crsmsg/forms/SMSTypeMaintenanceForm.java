package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SMSTypeMaintenanceForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private ArrayList<TblRef> smsTypes;
	private TblRef smsType;
	private int tblRefId;
	private ArrayList<Option> routingRules;
	private ArrayList<Option> priorities;
//	private List<Option> svcProvider;
//	private List<Option> svcProvider;
	
	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.routingRules = (ArrayList<Option>) CommonUtils.getRoutingRulesOption();
		this.priorities = (ArrayList<Option>) CommonUtils.getPriorityOption();
		smsType = new TblRef();
		smsTypes = null;
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		
		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			if(this.tblRefId == 0){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_SELECT, Constants.FORM_FIELD_SMS_TYPE));
			}else{
				if(DaoBeanFactory.getTblRefDao().isSmsTypeInUsed(this.tblRefId)){
					errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_RECORD_IN_USED, Constants.FORM_FIELD_SMS_TYPE));					
				}
			}
		}
		
		else if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			if(StringUtils.isEmpty(smsType.getLabel())){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_TYPE));
			}else{
				if(smsType.getTblRefId() == 0 && DaoBeanFactory.getTblRefDao().getSize(smsType.getLabel())>0){
					errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_RECORD_EXIST, Constants.FORM_FIELD_SMS_TYPE));				
				}
			}
			
			if(StringUtils.isEmpty(smsType.getValue())){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_TYPE_PRIORITY));
			}
			if(StringUtils.isEmpty(smsType.getRoutingAction())){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_TYPE_ROUTING_RULES));
			}
	
			if(StringUtils.isEmpty(smsType.getRoutableTelco())){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_TYPE_SERVICE_PROVIDER));
			}else{
				if( !smsType.getRoutableTelco().matches("\\d{1,8}(\\|\\d{1,8})*") ){
					errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, "Service Provider", this.smsType.getRoutableTelco()));
				}else{
					String[] svcPvds = StringUtils.split(this.smsType.getRoutableTelco(), "|");
					List<Telco> telcos = DaoBeanFactory.getTelcoDao().findByIds(svcPvds);
					List<Integer> ids = new ArrayList<Integer>();
					for(Telco telco : telcos){
						ids.add(telco.getId());
					}
					for(String svcPvd : svcPvds){
						if(!ids.contains(Integer.parseInt(svcPvd))){
							errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, "Service Provider", svcPvd));							
						}
					}
				}
			}
		}
		return errors;
	}
	
	public List<TblRef> getSmsTypes() {
		return smsTypes;
	}
	public void setSmsTypes(List<TblRef> smsTypes) {
		this.smsTypes = (ArrayList<TblRef>) smsTypes;
	}
	public TblRef getSmsType() {
		return smsType;
	}
	public void setSmsType(TblRef smsType) {
		this.smsType = smsType;
	}
	public int getTblRefId() {
		return tblRefId;
	}
	public void setTblRefId(int tblRefId) {
		this.tblRefId = tblRefId;
	}

	public List<Option> getRoutingRules() {
		return routingRules;
	}

	public void setRoutingRules(List<Option> routingRules) {
		this.routingRules = (ArrayList<Option>) routingRules;
	}

	public List<Option> getPriorities() {
		return priorities;
	}

	public void setPriorities(List<Option> priorities) {
		this.priorities = (ArrayList<Option>) priorities;
	}
	
}
