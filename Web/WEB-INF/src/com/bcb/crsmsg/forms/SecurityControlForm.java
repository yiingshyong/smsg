package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.Constants;



public final class SecurityControlForm extends ActionForm {
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -4851048155675524960L;
	private String searchkey;
	private ArrayList<String> searchby_label=new ArrayList<String>();
	private ArrayList<String> searchby_value=new ArrayList<String>();
	private String selectedState;	
	private Result resultList;
	private String[] checkedRecords;

	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		System.out.println("inside form reset");
		
		searchby_label = new ArrayList<String>();
		searchby_label.add(Constants.FORM_FIELD_IPADDRESS);
		searchby_label.add(Constants.FORM_FIELD_USER_DEPARTMENT);
		searchby_label.add(Constants.FORM_FIELD_SYSTEM);
		
		
		searchby_value = new ArrayList<String>();
		searchby_value.add(Constants.FORM_FIELD_IPADDRESS);
		searchby_value.add(Constants.FORM_FIELD_USER_DEPARTMENT);
		searchby_value.add(Constants.FORM_FIELD_SYSTEM);
		
		
		selectedState = Constants.STR_EMPTY;
		searchkey=Constants.STR_EMPTY;
		resultList=null;
		checkedRecords=null;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(selectedState.equals(Constants.FORM_FIELD_IPADDRESS)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_16)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_IPADDRESS, Constants.LENGTH_16 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_SYSTEM)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SYSTEM, Constants.LENGTH_50 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_USER_DEPARTMENT)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USER_DEPARTMENT, Constants.LENGTH_50 ));			
			}
		}
		
		return (errors);
		
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	public ArrayList<String> getSearchby_label() {
		return searchby_label;
	}

	public void setSearchby_label(ArrayList<String> searchby_label) {
		this.searchby_label = searchby_label;
	}

	public ArrayList<String> getSearchby_value() {
		System.out.println("search value=="+searchby_value);
		return searchby_value;
	}

	public void setSearchby_value(ArrayList<String> searchby_value) {
		this.searchby_value = searchby_value;
	}

	public String getSelectedState() {
		return selectedState;
	}

	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	public Result getResultList() {
		return resultList;
	}

	public void setResultList(Result resultList) {
		this.resultList = resultList;
	}

	public String[] getCheckedRecords() {
		return checkedRecords;
	}

	public void setCheckedRecords(String[] checkedRecords) {
		this.checkedRecords = checkedRecords;
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	
	
	
}

