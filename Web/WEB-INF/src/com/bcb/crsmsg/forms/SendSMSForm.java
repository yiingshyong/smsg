package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.actions.SpecialCharaters;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.senderprocessor.business.BusinessService;



public final class SendSMSForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5827543193749737475L;
	private String smsId;
	private String mobileNo;
	private String smsContent;
	private String scheduledSMS;
	private String scheduledSMSDateTime;
	private String systemDateTime;
	private String maxNoOfSMS;
	private String charPerSms;
	private String concatenateSMS;
	private String advanceDayLimit;
	private String submitBtn;
	private String sendBtn;
	
	private String selected_charset;
	private ArrayList<String> charset_label=null;
	private ArrayList<String> charset_value=null;
	
	private String smsPriority;
	private String smsPriorityShow;
	private ArrayList<String> smsPriority_label=null;
	private ArrayList<String> smsPriority_value=null;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		smsId=null;
		mobileNo=null;
		smsContent=null;
		scheduledSMS=null;
		scheduledSMSDateTime=null;
		systemDateTime=null;
		maxNoOfSMS=null;
		charPerSms=null;
		concatenateSMS=null;
		advanceDayLimit=null;
		submitBtn=null;
		sendBtn=null;
		
		smsPriority=null;
		smsPriorityShow=null;
		smsPriority_label = new ArrayList<String>();
		smsPriority_label.add(Constants.PRIORITY_LEVEL_1);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_2);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_3);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_4);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_5);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_6);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_7);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_8);
		smsPriority_label.add(Constants.PRIORITY_LEVEL_9);
		
		smsPriority_value = new ArrayList<String>();
		smsPriority_value.add(Constants.PRIORITY_LEVEL_1);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_2);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_3);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_4);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_5);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_6);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_7);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_8);
		smsPriority_value.add(Constants.PRIORITY_LEVEL_9);
		
		selected_charset=null;
		charset_label = new ArrayList<String>();
		charset_label.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_label.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		charset_value = new ArrayList<String>();
		charset_value.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_value.add(Constants.FORM_FIELD_CHARSET_UTF8);
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if (mobileNo!=null&&mobileNo.length()==Integer.valueOf(Constants.LENGTH_0))
			errors.add(Constants.FORM_SEND_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MOBILE_NO));
		
		if (smsContent!=null&&smsContent.trim().length()==Integer.valueOf(Constants.LENGTH_0))
			errors.add(Constants.FORM_SEND_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MESSAGE_TEXT));
				
		if (selected_charset!=null&&selected_charset.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_SEND_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_CHARSET));
		
		if (smsPriority!=null&&smsPriority.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_SEND_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SMS_PRIORITY));
		
		// Size Limit
		if (smsContent!=null&&smsContent.length()>Integer.valueOf(Constants.LENGTH_1000))
			errors.add(Constants.FORM_SEND_SMS,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SMS_CONTENT, Constants.LENGTH_1000));
		
		SpecialCharaters.checkSpecialCharacter(smsContent,errors,Constants.ERRMSG_SPECIAL_CHARACTERS,Constants.FORM_FIELD_MESSAGE_TEXT);
		SpecialCharaters.checkSpecialCharacter(smsPriority,errors,Constants.ERRMSG_SPECIAL_CHARACTERS,Constants.FORM_FIELD_SMS_PRIORITY);

		// Validation
		if (mobileNo!=null&&mobileNo.length()>Integer.valueOf(Constants.LENGTH_0)){
			String[] mobileNoList=StringUtils.split(mobileNo, Constants.SPECIAL_CHAR_NEWLINE);
//			Pattern p = Pattern.compile("^[6][05][189]\\d{7,9}$");
			/* mobile no. pattern accepted
			 601NNNNNNNN
			 658NNNNNNNN
			 659NNNNNNNN
			 */
			String error=Constants.STR_EMPTY;
			
			for(int i=0;i<mobileNoList.length;i++){
//				Matcher m = p.matcher(BusinessService.normalizeMobile(mobileNoList[i]));
			    if(!BusinessService.validateMobileNo(mobileNoList[i])){
			    	if(i!=0){
						error+=", ";
					}
			    	
			    	error+=mobileNoList[i];
			    }
			}
			
			if(error.length()>0){
				errors.add(Constants.FORM_SEND_SMS,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_MOBILE_NO, error));
			}
		}
			
		
		return (errors);
		
	}



	/**
	 * @return Returns the charset_label.
	 */
	public ArrayList<String> getCharset_label() {
		return charset_label;
	}


	/**
	 * @param charset_label The charset_label to set.
	 */
	public void setCharset_label(ArrayList<String> charset_label) {
		this.charset_label = charset_label;
	}


	/**
	 * @return Returns the charset_value.
	 */
	public ArrayList<String> getCharset_value() {
		return charset_value;
	}


	/**
	 * @param charset_value The charset_value to set.
	 */
	public void setCharset_value(ArrayList<String> charset_value) {
		this.charset_value = charset_value;
	}


	/**
	 * @return Returns the mobileNo.
	 */
	public String getMobileNo() {
		return mobileNo;
	}


	/**
	 * @param mobileNo The mobileNo to set.
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	/**
	 * @return Returns the scheduledSMS.
	 */
	public String getScheduledSMS() {
		return scheduledSMS;
	}


	/**
	 * @param scheduledSMS The scheduledSMS to set.
	 */
	public void setScheduledSMS(String scheduledSMS) {
		this.scheduledSMS = scheduledSMS;
	}


	/**
	 * @return Returns the scheduledSMSDateTime.
	 */
	public String getScheduledSMSDateTime() {
		return scheduledSMSDateTime;
	}


	/**
	 * @param scheduledSMSDateTime The scheduledSMSDateTime to set.
	 */
	public void setScheduledSMSDateTime(String scheduledSMSDateTime) {
		this.scheduledSMSDateTime = scheduledSMSDateTime;
	}


	/**
	 * @return Returns the selected_charset.
	 */
	public String getSelected_charset() {
		return selected_charset;
	}


	/**
	 * @param selected_charset The selected_charset to set.
	 */
	public void setSelected_charset(String selected_charset) {
		this.selected_charset = selected_charset;
	}


	/**
	 * @return Returns the smsContent.
	 */
	public String getSmsContent() {
		return smsContent;
	}


	/**
	 * @param smsContent The smsContent to set.
	 */
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}


	/**
	 * @return Returns the smsPriority.
	 */
	public String getSmsPriority() {
		return smsPriority;
	}


	/**
	 * @param smsPriority The smsPriority to set.
	 */
	public void setSmsPriority(String smsPriority) {
		this.smsPriority = smsPriority;
	}


	/**
	 * @return Returns the smsPriority_label.
	 */
	public ArrayList<String> getSmsPriority_label() {
		return smsPriority_label;
	}


	/**
	 * @param smsPriority_label The smsPriority_label to set.
	 */
	public void setSmsPriority_label(ArrayList<String> smsPriority_label) {
		this.smsPriority_label = smsPriority_label;
	}


	/**
	 * @return Returns the smsPriority_value.
	 */
	public ArrayList<String> getSmsPriority_value() {
		return smsPriority_value;
	}


	/**
	 * @param smsPriority_value The smsPriority_value to set.
	 */
	public void setSmsPriority_value(ArrayList<String> smsPriority_value) {
		this.smsPriority_value = smsPriority_value;
	}


	/**
	 * @return Returns the systemDateTime.
	 */
	public String getSystemDateTime() {
		return systemDateTime;
	}


	/**
	 * @param systemDateTime The systemDateTime to set.
	 */
	public void setSystemDateTime(String systemDateTime) {
		this.systemDateTime = systemDateTime;
	}


	/**
	 * @return Returns the smsId.
	 */
	public String getSmsId() {
		return smsId;
	}


	/**
	 * @param smsId The smsId to set.
	 */
	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}


	/**
	 * @return Returns the maxNoOfSMS.
	 */
	public String getMaxNoOfSMS() {
		return maxNoOfSMS;
	}


	/**
	 * @param maxNoOfSMS The maxNoOfSMS to set.
	 */
	public void setMaxNoOfSMS(String maxNoOfSMS) {
		this.maxNoOfSMS = maxNoOfSMS;
	}


	/**
	 * @return Returns the charPerSms.
	 */
	public String getCharPerSms() {
		return charPerSms;
	}


	/**
	 * @param charPerSms The charPerSms to set.
	 */
	public void setCharPerSms(String charPerSms) {
		this.charPerSms = charPerSms;
	}


	/**
	 * @return Returns the concatenateSMS.
	 */
	public String getConcatenateSMS() {
		return concatenateSMS;
	}


	/**
	 * @param concatenateSMS The concatenateSMS to set.
	 */
	public void setConcatenateSMS(String concatenateSMS) {
		this.concatenateSMS = concatenateSMS;
	}


	public String getSendBtn() {
		return sendBtn;
	}


	public void setSendBtn(String sendBtn) {
		this.sendBtn = sendBtn;
	}


	public String getSubmitBtn() {
		return submitBtn;
	}


	public void setSubmitBtn(String submitBtn) {
		this.submitBtn = submitBtn;
	}


	public String getSmsPriorityShow() {
		return smsPriorityShow;
	}


	public void setSmsPriorityShow(String smsPriorityShow) {
		this.smsPriorityShow = smsPriorityShow;
	}


	public String getAdvanceDayLimit() {
		return advanceDayLimit;
	}


	public void setAdvanceDayLimit(String advanceDayLimit) {
		this.advanceDayLimit = advanceDayLimit;
	}
}

