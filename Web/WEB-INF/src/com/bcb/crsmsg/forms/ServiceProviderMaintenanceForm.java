package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.modal.ServiceProvider;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class ServiceProviderMaintenanceForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private ServiceProvider svcProvider;
	private ArrayList<Option> smsRates;
	private ArrayList<ServiceProvider> svcProviders;	

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		smsRates = (ArrayList<Option>) CommonUtils.getAllSmsRates();
		svcProviders = (ArrayList<ServiceProvider>) DaoBeanFactory.getServiceProviderGroupDao().getActiveServiceProviders();
		svcProvider = new ServiceProvider();
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,	HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			if(this.svcProvider != null && this.svcProvider.getId() == 0){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_SELECT, "Service Provider"));
			}else{
				if(DaoBeanFactory.getServiceProviderGroupDao().inUsed(this.svcProvider.getId())){
					errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_RECORD_IN_USED, "Service Provider"));					
				}
			}
		}
		
		else if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			if(StringUtils.isEmpty(this.svcProvider.getName())){
				errors.add(Constants.FORM_CHANNEL_FORM,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Name"));
			}else if(this.svcProvider.getId() != null && this.svcProvider.getId() == 0){
				for(ServiceProvider svc : svcProviders){
					if(svc.getName().equals(this.svcProvider.getName())){
						errors.add(Constants.FORM_CHANNEL_FORM,
								new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, this.svcProvider.getName()));					
					}
				}
			}
	 
			if(StringUtils.isEmpty(this.svcProvider.getSmsRateIds())){
				errors.add(Constants.FORM_CHANNEL_FORM,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "SMS Rates"));
			}
			if(svcProvider.getDefaultSmsRateId() != null && svcProvider.getDefaultSmsRateId() == 0){
				errors.add(Constants.FORM_CHANNEL_FORM,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Default SMS Rate"));
			}
		}
		return errors;
	}

	public ServiceProvider getSvcProvider() {
		return svcProvider;
	}

	public void setSvcProvider(ServiceProvider svcProvider) {
		this.svcProvider = svcProvider;
	}

	public List<Option> getSmsRates() {
		return smsRates;
	}

	public void setSmsRates(List<Option> smsRates) {
		this.smsRates = (ArrayList<Option>) smsRates;
	}

	public List<ServiceProvider> getSvcProviders() {
		return svcProviders;
	}

	public void setSvcProviders(List<ServiceProvider> svcProviders) {
		this.svcProviders = (ArrayList<ServiceProvider>) svcProviders;
	}

}
