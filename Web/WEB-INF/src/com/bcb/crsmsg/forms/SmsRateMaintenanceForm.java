package com.bcb.crsmsg.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.SmsRate;
import com.bcb.crsmsg.util.Constants;
import com.bcb.sgwcore.util.DaoBeanFactory;

public class SmsRateMaintenanceForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private SmsRate smsRate;
	private ArrayList<SmsRate> smsRates;
	private Integer selectedId;

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		smsRates = (ArrayList<SmsRate>) DaoBeanFactory.getSmsRateDao().getActiveSmsRate();
		smsRate = new SmsRate();
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,	HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_DELETE)){
			if(this.smsRate != null && this.smsRate.getId() == 0){
				errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_SELECT, "SMS Rate"));
			}else{
				if(DaoBeanFactory.getSmsRateDao().inUsed(this.smsRate.getId())){
					errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_RECORD_IN_USED, "SMS Rate"));					
				}
			}
		}
		
		else if(mapping.getParameter().equals(Constants.REQUEST_PARAMETER_SAVE)){
			if(StringUtils.isEmpty(this.smsRate.getName())){
				errors.add(Constants.FORM_CHANNEL_FORM,
						new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Name"));
			}else if(this.smsRate != null && this.smsRate.getId() == 0){
				for(SmsRate rate : smsRates){
					if(rate.getName().equals(this.smsRate.getName())){
						errors.add(Constants.FORM_CHANNEL_FORM,
								new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, this.smsRate.getName()));					
					}
				}
			}
		}
		return errors;
	}

	public SmsRate getSmsRate() {
		return smsRate;
	}

	public void setSmsRate(SmsRate smsRate) {
		this.smsRate = smsRate;
	}

	public List<SmsRate> getSmsRates() {
		return smsRates;
	}

	public void setSmsRates(List<SmsRate> smsRates) {
		this.smsRates = (ArrayList<SmsRate>) smsRates;
	}

	public Integer getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(Integer selectedId) {
		this.selectedId = selectedId;
	}

}
