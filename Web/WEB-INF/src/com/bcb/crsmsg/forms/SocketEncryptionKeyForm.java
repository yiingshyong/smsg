package com.bcb.crsmsg.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.SmsCfg;
import com.bcb.crsmsg.util.Constants;

public class SocketEncryptionKeyForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private SmsCfg smsCfg;
	private String btn1;
	private String btn2;
	private String btn3;

	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.smsCfg = new SmsCfg();
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		
		if(StringUtils.isNotEmpty(btn1) && (StringUtils.isEmpty(smsCfg.getSocketEncComp1()) || smsCfg.getSocketEncComp1().length() < 32)){
			errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_INCORRECT_KEY_LENGTH, "Key Component 1"));			
		}
		else if(StringUtils.isNotEmpty(btn2) && (StringUtils.isEmpty(smsCfg.getSocketEncComp2()) || smsCfg.getSocketEncComp2().length() < 32)){
			errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_INCORRECT_KEY_LENGTH, "Key Component 2"));			
		}
		else if(StringUtils.isNotEmpty(btn3) && (StringUtils.isEmpty(smsCfg.getSocketEncComp3()) || smsCfg.getSocketEncComp3().length() < 32)){
			errors.add(Constants.FORM_SMS_TYPE, new ActionMessage(Constants.ERRMSG_INCORRECT_KEY_LENGTH, "Key Component 3"));			
		}
		return errors;
	}

	public SmsCfg getSmsCfg() {
		return smsCfg;
	}

	public void setSmsCfg(SmsCfg smsCfg) {
		this.smsCfg = smsCfg;
	}

	public String getBtn1() {
		return btn1;
	}

	public void setBtn1(String btn1) {
		this.btn1 = btn1;
	}

	public String getBtn2() {
		return btn2;
	}

	public void setBtn2(String btn2) {
		this.btn2 = btn2;
	}

	public String getBtn3() {
		return btn3;
	}

	public void setBtn3(String btn3) {
		this.btn3 = btn3;
	}	
}
