package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.User;
import com.bcb.common.util.DBUtils;
import com.bcb.crsmsg.modal.TblRef;
import com.bcb.crsmsg.modal.Telco;
import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;
import com.bcb.sgwcore.util.DaoBeanFactory;



public final class SystemCfgForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4872020690345021184L;
	private String cfgId;
    private String autoSmsFailure;
    private String autoSmsResend;
    private String autoSmsResendInterval;
    private String autoSmsResendLimit;
    private String[] bulkSmsThresholdNotification;
    private String bulkSmsThresholdNotificationEmail;
    private String bulkSmsThresholdNotificationEmailSender;
    private String bulkSmsThresholdNotificationSms;
    private String bulkSmsThresholdNotificationSmsSender;
    private String bulkSmsThreshold;
    private String[] queueSmsThresholdNotification;
    private String queueSmsThresholdNotificationEmail;
    private String queueSmsThresholdNotificationEmailSender;
    private String queueSmsThresholdNotificationSms;
    private String queueSmsThresholdNotificationSmsSender;
    private String queueSmsThreshold;
    private String queueSmsThresholdPeriod;
    private String queueSmsThresholdTime;
    private String concatenateSms;
    private String concatenateSmsNo;
    private String[] ftpFailureNotification;
    private String ftpFailureNotificationEmail;
    private String ftpFailureNotificationEmailSender;
    private String ftpFailureNotificationSms;
    private String ftpFailureNotificationSmsSender;
    private String httpSmsService;
    private String ipControl;
    private String ipSkip;
    private String mobileCountryPrefix;
    private String scheduledSmsControlDuration;
    private String[] smsFailureNotification;
    private String smsFailureNotificationEmail;
    private String smsFailureNotificationEmailSender;
    private String smsFailureNotificationSms;
    private String smsFailureNotificationSmsSender;
    private String smsFailureThreshold;
    private String smsFailureThresholdPeriod;
    private String smsFailureThresholdTime;
    private String smsPrioritize;
    private String smsQuota;
    private String smsQuotaDefault;
    private String smsTimeControlBatch;
    private String smsTimeControlWeb;
    private String tableListDelimiter;
    private String systemDebug;
    private String emailFormat;
    private String version;
    private String passwordChangeDuration;
    private String dept;
    private String usageLogDuration;
    private String systemLogDuration;
    private String accountDormancyCheck;
    private String totalQueuedSizeAlertThreshold;
    private String totalQueuedSizeAlertCheckPeriod;
    private String autoPatchToUnsent;
    private String systemSMSDefaultSender;
    private int defaultSmsType;
    private String ccaDataAge;
    private ArrayList<TblRef> smsTypes;
    
	private String selected_ownership;
	private ArrayList<String> ownership_label=null;
	private ArrayList<String> ownership_value=null;

	private String selected_charset;
	private ArrayList<String> charset_label=null;
	private ArrayList<String> charset_value=null;
	
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;

	private String smsTelcoRouting;
	private ArrayList<String> routing_label=null;
	private ArrayList<String> routing_value=null;
	
	private String[] smsTelcoRoutingParam;
	private ArrayList<String> telco_label=null;
	private ArrayList<String> telco_value=null;
	
	private String[] smsTimeControlDay;
	private ArrayList<String> day_label=null;
	private ArrayList<String> day_value=null;
	
	private String[] smsTimeControlTimeFromHour;
    private String[] smsTimeControlTimeFromMin;
    private String[] smsTimeControlTimeToHour;
    private String[] smsTimeControlTimeToMin;
    
    private ArrayList<String> hour_label=null;
	private ArrayList<String> hour_value=null;
    private ArrayList<String> min_label=null;
	private ArrayList<String> min_value=null;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		autoPatchToUnsent = "N";
		cfgId=null;
	    autoSmsFailure=null;
	    autoSmsResend=null;
	    autoSmsResendInterval=null;
	    autoSmsResendLimit=null;
	    bulkSmsThresholdNotification=null;
	    bulkSmsThresholdNotificationEmail=null;
	    bulkSmsThresholdNotificationEmailSender=null;
	    bulkSmsThresholdNotificationSms=null;
	    bulkSmsThresholdNotificationSmsSender=null;
	    bulkSmsThreshold=null;
	    queueSmsThresholdNotification=null;
	    queueSmsThresholdNotificationEmail=null;
	    queueSmsThresholdNotificationEmailSender=null;
	    queueSmsThresholdNotificationSms=null;
	    queueSmsThresholdNotificationSmsSender=null;
	    queueSmsThreshold=null;
	    concatenateSms=null;
	    concatenateSmsNo=null;
	    ftpFailureNotificationEmail=null;
	    ftpFailureNotificationEmailSender=null;
	    ftpFailureNotificationSms=null;
	    ftpFailureNotificationSmsSender=null;
	    httpSmsService=null;
	    ipControl=null;
	    ipSkip=null;
	    mobileCountryPrefix=null;
	    scheduledSmsControlDuration=null;
	    smsFailureNotification=null;
	    smsFailureNotificationEmail=null;
	    smsFailureNotificationEmailSender=null;
	    smsFailureNotificationSms=null;
	    smsFailureNotificationSmsSender=null;
	    smsFailureThreshold=null;
	    smsFailureThresholdPeriod=null;
	    smsFailureThresholdTime=null;
	    smsPrioritize=null;
	    smsQuota=null;
	    smsQuotaDefault=null;
	    smsTimeControlBatch=null;
	    smsTimeControlWeb=null;
	    tableListDelimiter=null;
	    systemDebug=null;
	    emailFormat=null;
	    version=null;
	    passwordChangeDuration=null;
	    dept=null;
	    ftpFailureNotification=null;
	    usageLogDuration=null;
	    ccaDataAge=null;
	    systemLogDuration=null;
	    accountDormancyCheck=null;
	    
		selected_ownership=null;
		ownership_label = new ArrayList<String>();
		ownership_value = new ArrayList<String>();

		selected_charset=null;
		charset_label = new ArrayList<String>();
		charset_label.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_label.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		charset_value = new ArrayList<String>();
		charset_value.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_value.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		selected_status=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_label.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		status_value = new ArrayList<String>();
		status_value.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_value.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		smsTelcoRouting=null;
		routing_label= new ArrayList<String>();
		routing_label.add(Constants.FORM_FIELD_TELCO_ROUTING_PRIORITY);
		routing_label.add(Constants.FORM_FIELD_TELCO_ROUTING_COST);
		routing_label.add(Constants.FORM_FIELD_TELCO_ROUTING_CONGESTION);
		routing_label.add(Constants.FORM_FIELD_TELCO_ROUTING_PREFIX);
		
		routing_value= new ArrayList<String>();
		routing_value.add(Constants.FORM_FIELD_TELCO_ROUTING_PRIORITY);
		routing_value.add(Constants.FORM_FIELD_TELCO_ROUTING_COST);
		routing_value.add(Constants.FORM_FIELD_TELCO_ROUTING_CONGESTION);
		routing_value.add(Constants.FORM_FIELD_TELCO_ROUTING_PREFIX);
		
		smsTelcoRoutingParam=null;
		List telcoList=DBUtilsCrsmsg.DBHQLCommand("from Telco order by telcoName");
		telco_label= new ArrayList<String>();
		telco_value= new ArrayList<String>();
		Telco aTelco=null;
		for(int i=0;i<telcoList.size();i++){
			aTelco=(Telco)telcoList.get(i);
			telco_label.add(aTelco.getTelcoName());
			telco_value.add(aTelco.getId().toString());
		}
		
		 smsTimeControlDay=new String[]{
				 Integer.toString(Calendar.SUNDAY), 
				 Integer.toString(Calendar.MONDAY),
				 Integer.toString(Calendar.TUESDAY), 
				 Integer.toString(Calendar.WEDNESDAY),
				 Integer.toString(Calendar.THURSDAY),
				 Integer.toString(Calendar.FRIDAY),
				 Integer.toString(Calendar.SATURDAY)};
		smsTimeControlTimeFromHour=new String[]{"00","00","00","00","00","00","00"};
	    smsTimeControlTimeFromMin=new String[]{"00","00","00","00","00","00","00"};
	    smsTimeControlTimeToHour=new String[]{"23","23","23","23","23","23","23"};
	    smsTimeControlTimeToMin=new String[]{"59","59","59","59","59","59","59"};
	    
	    hour_label= new ArrayList<String>();
	    hour_value= new ArrayList<String>();
	    min_label= new ArrayList<String>();
	    min_value= new ArrayList<String>();
	    for(int i=0;i<24;i++){
	    	hour_label.add((Integer.toString(i).length()>1)?Integer.toString(i):Constants.VALUE_0+Integer.toString(i));
	    	hour_value.add((Integer.toString(i).length()>1)?Integer.toString(i):Constants.VALUE_0+Integer.toString(i));
	    }
	    for(int i=0;i<60;i++){
	    	min_label.add((Integer.toString(i).length()>1)?Integer.toString(i):Constants.VALUE_0+Integer.toString(i));
	    	min_value.add((Integer.toString(i).length()>1)?Integer.toString(i):Constants.VALUE_0+Integer.toString(i));
    	}
	    this.smsTypes = (ArrayList<TblRef>) DaoBeanFactory.getTblRefDao().getSmsTypes();
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if(smsTelcoRouting!=null&&smsTelcoRouting.equals(Constants.FORM_FIELD_TELCO_ROUTING_PRIORITY)){
			for(int i=0;smsTelcoRoutingParam!=null&&i<smsTelcoRoutingParam.length;i++){
				if(smsTelcoRoutingParam[i].equals(Constants.VALUE_0)){
					errors.add(Constants.FORM_SYSTEM_CFG,
							new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TELCO_ROUTING_PRIORITY_SEQUENCE));
				}
			}
		}
		
		// Size Limit
		if (ftpFailureNotificationEmail!=null&&ftpFailureNotificationEmail.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_FTP_EMAIL, Constants.LENGTH_500));
		
		if (ftpFailureNotificationEmailSender!=null){
			if(ftpFailureNotificationEmailSender.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_FTP_EMAIL_SENDER, Constants.LENGTH_50));
			}
		}
		
		if (ftpFailureNotificationSms!=null&&ftpFailureNotificationSms.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_FTP_SMS, Constants.LENGTH_500));
		
		if (ftpFailureNotificationSmsSender!=null&&ftpFailureNotificationSmsSender.length()>0){
			if(ftpFailureNotificationSmsSender.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_FTP_SMS_SENDER, Constants.LENGTH_15));
			}else{
				if(DBUtils.get(User.class, ftpFailureNotificationSmsSender)==null){
					errors.add(Constants.FORM_SYSTEM_CFG,
							new ActionMessage(Constants.ERRMSG_RECORD_NOT_EXIST, Constants.FORM_FIELD_FAILURE_FTP_SMS_SENDER));
				}
			}
		}
		
		if (smsFailureNotificationEmail!=null&&smsFailureNotificationEmail.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_EMAIL, Constants.LENGTH_500));
		
		if (smsFailureNotificationEmailSender!=null){
			if(smsFailureNotificationEmailSender.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_EMAIL_SENDER, Constants.LENGTH_50));
			}
		}
		
		if (smsFailureNotificationSms!=null&&smsFailureNotificationSms.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_SMS, Constants.LENGTH_500));
		
		if (smsFailureNotificationSmsSender!=null&&smsFailureNotificationSmsSender.length()>0){
			if(smsFailureNotificationSmsSender.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_SMS_SENDER, Constants.LENGTH_15));
			}else{
				if(DBUtils.get(User.class, smsFailureNotificationSmsSender)==null){
					errors.add(Constants.FORM_SYSTEM_CFG,
							new ActionMessage(Constants.ERRMSG_RECORD_NOT_EXIST, Constants.FORM_FIELD_FAILURE_SMS_SMS_SENDER));
				}
			}
		}
		
		if (smsFailureThreshold!=null&&smsFailureThreshold.length()>0){
			if(smsFailureThreshold.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD, Constants.LENGTH_15));
			}
			if(!smsFailureThreshold.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD, smsFailureThreshold));
			}
		}
		
		if (smsFailureThresholdPeriod!=null&&smsFailureThresholdPeriod.length()>0){
			if(smsFailureThresholdPeriod.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD_PERIOD, Constants.LENGTH_15));
			}
			if(!smsFailureThresholdPeriod.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD_PERIOD, smsFailureThresholdPeriod));
			}
		}
		
		if (smsFailureThresholdTime!=null&&smsFailureThresholdTime.length()>0){
			if(smsFailureThresholdTime.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD_TIME, Constants.LENGTH_15));
			}
			if(!smsFailureThresholdTime.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_FAILURE_SMS_THRESHOLD_TIME, smsFailureThresholdTime));
			}
		}
		
		if (mobileCountryPrefix!=null&&mobileCountryPrefix.length()>Integer.valueOf(Constants.LENGTH_5))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MOBILE_PREFIX, Constants.LENGTH_5));
		
		if (bulkSmsThresholdNotificationEmail!=null&&bulkSmsThresholdNotificationEmail.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_BULK_SMS_THRESHOLD_EMAIL, Constants.LENGTH_500));
		
		if (bulkSmsThresholdNotificationEmailSender!=null){
			if(bulkSmsThresholdNotificationEmailSender.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_BULK_SMS_THRESHOLD_EMAIL_SENDER, Constants.LENGTH_50));
			}
		}
		
		if (bulkSmsThresholdNotificationSms!=null&&bulkSmsThresholdNotificationSms.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_BULK_SMS_THRESHOLD_SMS, Constants.LENGTH_500));
		
		if (bulkSmsThresholdNotificationSmsSender!=null&&bulkSmsThresholdNotificationSmsSender.length()>0){
			if(bulkSmsThresholdNotificationSmsSender.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_BULK_SMS_THRESHOLD_SMS_SENDER, Constants.LENGTH_15));
			}else{
				if(DBUtils.get(User.class, bulkSmsThresholdNotificationSmsSender)==null){
					errors.add(Constants.FORM_SYSTEM_CFG,
							new ActionMessage(Constants.ERRMSG_RECORD_NOT_EXIST, Constants.FORM_FIELD_BULK_SMS_THRESHOLD_SMS_SENDER));
				}
			}
		}
		
		if (bulkSmsThreshold!=null&&bulkSmsThreshold.length()>0){
			if(bulkSmsThreshold.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_BULK_SMS_THRESHOLD, Constants.LENGTH_15));
			}
			if(!bulkSmsThreshold.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_BULK_SMS_THRESHOLD, bulkSmsThreshold));
			}
		}
		
		if (queueSmsThresholdNotificationEmail!=null&&queueSmsThresholdNotificationEmail.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_EMAIL, Constants.LENGTH_500));
		
		if (queueSmsThresholdNotificationEmailSender!=null){
			if(queueSmsThresholdNotificationEmailSender.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_EMAIL_SENDER, Constants.LENGTH_50));
			}
		}
		
		if (queueSmsThresholdNotificationSms!=null&&queueSmsThresholdNotificationSms.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_SMS, Constants.LENGTH_500));
		
		if (queueSmsThresholdNotificationSmsSender!=null&&queueSmsThresholdNotificationSmsSender.length()>0){
			if(queueSmsThresholdNotificationSmsSender.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_SMS_SENDER, Constants.LENGTH_15));
			}else{
				if(DBUtils.get(User.class, queueSmsThresholdNotificationSmsSender)==null){
					errors.add(Constants.FORM_SYSTEM_CFG,
							new ActionMessage(Constants.ERRMSG_RECORD_NOT_EXIST, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_SMS_SENDER));
				}
			}
		}
		
		if (queueSmsThreshold!=null&&queueSmsThreshold.length()>0){
			if(queueSmsThreshold.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD, Constants.LENGTH_15));
			}
			if(!queueSmsThreshold.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD, queueSmsThreshold));
			}
		}
		
		if (queueSmsThresholdPeriod!=null&&queueSmsThresholdPeriod.length()>0){
			if(queueSmsThresholdPeriod.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_PERIOD, Constants.LENGTH_15));
			}
			if(!queueSmsThresholdPeriod.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_PERIOD, queueSmsThresholdPeriod));
			}
		}
		
		if (totalQueuedSizeAlertThreshold!=null&&totalQueuedSizeAlertThreshold.length()>0){
			if(totalQueuedSizeAlertThreshold.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD, Constants.LENGTH_15));
			}
			if(!totalQueuedSizeAlertThreshold.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD, totalQueuedSizeAlertThreshold));
			}
		}
		
		if (totalQueuedSizeAlertCheckPeriod!=null&&totalQueuedSizeAlertCheckPeriod.length()>0){
			if(totalQueuedSizeAlertCheckPeriod.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD_PERIOD, Constants.LENGTH_15));
			}
			if(!totalQueuedSizeAlertCheckPeriod.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUEUE_TOTAL_SMS_THRESHOLD_PERIOD, totalQueuedSizeAlertCheckPeriod));
			}
		}
		
		if (queueSmsThresholdTime!=null&&queueSmsThresholdTime.length()>0){
			if(queueSmsThresholdTime.length()>Integer.valueOf(Constants.LENGTH_15)){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_TIME, Constants.LENGTH_15));
			}
			if(!queueSmsThresholdTime.matches("^\\d{1,15}$")){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_QUEUE_SMS_THRESHOLD_TIME, queueSmsThresholdTime));
			}
		}
		
		if (autoSmsFailure!=null&&autoSmsFailure.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_PENDING_SMS_AUTO_FAILURE, Constants.LENGTH_10));
		
		if (autoSmsResendLimit!=null&&autoSmsResendLimit.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_RESEND_SMS_LIMIT, Constants.LENGTH_10));
		
		if (autoSmsResendInterval!=null&&autoSmsResendInterval.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_AUTO_RESEND_SMS_INTERVAL, Constants.LENGTH_10));
		
		if (smsQuotaDefault!=null&&smsQuotaDefault.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SMS_QUOTA_DEFAULT, Constants.LENGTH_10));
		
		if (passwordChangeDuration!=null&&passwordChangeDuration.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_PASSWORD_RESET_POLICY, Constants.LENGTH_10));

		if (accountDormancyCheck!=null&&accountDormancyCheck.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_ACCOUNT_DORMANCY_CHECK, Constants.LENGTH_10));

		if (usageLogDuration!=null&&usageLogDuration.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USAGE_LOG_MAINTENANCE, Constants.LENGTH_10));
		
		if (ccaDataAge!=null&&ccaDataAge.length()>Integer.valueOf(Constants.LENGTH_5))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_CREDIT_CARD_ACTIVATION_LOG_MAINTENANCE, Constants.LENGTH_5));
		
		if (systemLogDuration!=null&&systemLogDuration.length()>Integer.valueOf(Constants.LENGTH_10))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SYSTEM_LOG_MAINTENANCE, Constants.LENGTH_10));
		
		if (scheduledSmsControlDuration!=null&&scheduledSmsControlDuration.length()>Integer.valueOf(Constants.LENGTH_5))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_SCHEDULED_SMS_CONTROL, Constants.LENGTH_5));
		
		if (ipSkip!=null&&ipSkip.length()>Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_IP_SKIP_CONTROL, Constants.LENGTH_500));
		
		if(StringUtils.isEmpty(this.systemSMSDefaultSender)){
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_SYS_SMS_DEFAULT_SENDER));			
		}else{
			if(DaoBeanFactory.getHibernateTemplate().get(User.class, this.systemSMSDefaultSender) == null){
				errors.add(Constants.FORM_SYSTEM_CFG,
						new ActionMessage(Constants.ERRMSG_RECORD_NOT_EXIST, Constants.FORM_FIELD_SYS_SMS_DEFAULT_SENDER));							
			}
		}
		
		if(this.defaultSmsType == 0){
			errors.add(Constants.FORM_SYSTEM_CFG,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_DEFAULT_SMS_TYPE));			
		}
		return (errors);
		
	}


	/**
	 * @return Returns the autoSmsFailure.
	 */
	public String getAutoSmsFailure() {
		return autoSmsFailure;
	}


	/**
	 * @param autoSmsFailure The autoSmsFailure to set.
	 */
	public void setAutoSmsFailure(String autoSmsFailure) {
		this.autoSmsFailure = autoSmsFailure;
	}


	/**
	 * @return Returns the autoSmsResend.
	 */
	public String getAutoSmsResend() {
		return autoSmsResend;
	}


	/**
	 * @param autoSmsResend The autoSmsResend to set.
	 */
	public void setAutoSmsResend(String autoSmsResend) {
		this.autoSmsResend = autoSmsResend;
	}


	/**
	 * @return Returns the autoSmsResendInterval.
	 */
	public String getAutoSmsResendInterval() {
		return autoSmsResendInterval;
	}


	/**
	 * @param autoSmsResendInterval The autoSmsResendInterval to set.
	 */
	public void setAutoSmsResendInterval(String autoSmsResendInterval) {
		this.autoSmsResendInterval = autoSmsResendInterval;
	}


	/**
	 * @return Returns the autoSmsResendLimit.
	 */
	public String getAutoSmsResendLimit() {
		return autoSmsResendLimit;
	}


	/**
	 * @param autoSmsResendLimit The autoSmsResendLimit to set.
	 */
	public void setAutoSmsResendLimit(String autoSmsResendLimit) {
		this.autoSmsResendLimit = autoSmsResendLimit;
	}


	/**
	 * @return Returns the bulkSmsThreshold.
	 */
	public String getBulkSmsThreshold() {
		return bulkSmsThreshold;
	}


	/**
	 * @param bulkSmsThreshold The bulkSmsThreshold to set.
	 */
	public void setBulkSmsThreshold(String bulkSmsThreshold) {
		this.bulkSmsThreshold = bulkSmsThreshold;
	}


	/**
	 * @return Returns the bulkSmsThresholdNotificationEmail.
	 */
	public String getBulkSmsThresholdNotificationEmail() {
		return bulkSmsThresholdNotificationEmail;
	}


	/**
	 * @param bulkSmsThresholdNotificationEmail The bulkSmsThresholdNotificationEmail to set.
	 */
	public void setBulkSmsThresholdNotificationEmail(
			String bulkSmsThresholdNotificationEmail) {
		this.bulkSmsThresholdNotificationEmail = bulkSmsThresholdNotificationEmail;
	}


	/**
	 * @return Returns the charset_label.
	 */
	public ArrayList<String> getCharset_label() {
		return charset_label;
	}


	/**
	 * @param charset_label The charset_label to set.
	 */
	public void setCharset_label(ArrayList<String> charset_label) {
		this.charset_label = charset_label;
	}


	/**
	 * @return Returns the charset_value.
	 */
	public ArrayList<String> getCharset_value() {
		return charset_value;
	}


	/**
	 * @param charset_value The charset_value to set.
	 */
	public void setCharset_value(ArrayList<String> charset_value) {
		this.charset_value = charset_value;
	}


	/**
	 * @return Returns the concatenateSms.
	 */
	public String getConcatenateSms() {
		return concatenateSms;
	}


	/**
	 * @param concatenateSms The concatenateSms to set.
	 */
	public void setConcatenateSms(String concatenateSms) {
		this.concatenateSms = concatenateSms;
	}


	/**
	 * @return Returns the concatenateSmsNo.
	 */
	public String getConcatenateSmsNo() {
		return concatenateSmsNo;
	}


	/**
	 * @param concatenateSmsNo The concatenateSmsNo to set.
	 */
	public void setConcatenateSmsNo(String concatenateSmsNo) {
		this.concatenateSmsNo = concatenateSmsNo;
	}


	/**
	 * @return Returns the emailFormat.
	 */
	public String getEmailFormat() {
		return emailFormat;
	}


	/**
	 * @param emailFormat The emailFormat to set.
	 */
	public void setEmailFormat(String emailFormat) {
		this.emailFormat = emailFormat;
	}


	/**
	 * @return Returns the ftpFailureNotificationEmail.
	 */
	public String getFtpFailureNotificationEmail() {
		return ftpFailureNotificationEmail;
	}


	/**
	 * @param ftpFailureNotificationEmail The ftpFailureNotificationEmail to set.
	 */
	public void setFtpFailureNotificationEmail(String ftpFailureNotificationEmail) {
		this.ftpFailureNotificationEmail = ftpFailureNotificationEmail;
	}


	/**
	 * @return Returns the httpSmsService.
	 */
	public String getHttpSmsService() {
		return httpSmsService;
	}


	/**
	 * @param httpSmsService The httpSmsService to set.
	 */
	public void setHttpSmsService(String httpSmsService) {
		this.httpSmsService = httpSmsService;
	}


	/**
	 * @return Returns the cfgId.
	 */
	public String getCfgId() {
		return cfgId;
	}


	/**
	 * @param id The id to set.
	 */
	public void setCfgId(String cfgId) {
		this.cfgId = cfgId;
	}


	/**
	 * @return Returns the ipControl.
	 */
	public String getIpControl() {
		return ipControl;
	}


	/**
	 * @param ipControl The ipControl to set.
	 */
	public void setIpControl(String ipControl) {
		this.ipControl = ipControl;
	}


	/**
	 * @return Returns the ipSkip.
	 */
	public String getIpSkip() {
		return ipSkip;
	}


	/**
	 * @param ipSkip The ipSkip to set.
	 */
	public void setIpSkip(String ipSkip) {
		this.ipSkip = ipSkip;
	}


	/**
	 * @return Returns the mobileCountryPrefix.
	 */
	public String getMobileCountryPrefix() {
		return mobileCountryPrefix;
	}


	/**
	 * @param mobileCountryPrefix The mobileCountryPrefix to set.
	 */
	public void setMobileCountryPrefix(String mobileCountryPrefix) {
		this.mobileCountryPrefix = mobileCountryPrefix;
	}


	/**
	 * @return Returns the ownership_label.
	 */
	public ArrayList<String> getOwnership_label() {
		return ownership_label;
	}


	/**
	 * @param ownership_label The ownership_label to set.
	 */
	public void setOwnership_label(ArrayList<String> ownership_label) {
		this.ownership_label = ownership_label;
	}


	/**
	 * @return Returns the ownership_value.
	 */
	public ArrayList<String> getOwnership_value() {
		return ownership_value;
	}


	/**
	 * @param ownership_value The ownership_value to set.
	 */
	public void setOwnership_value(ArrayList<String> ownership_value) {
		this.ownership_value = ownership_value;
	}


	/**
	 * @return Returns the scheduledSmsControlDuration.
	 */
	public String getScheduledSmsControlDuration() {
		return scheduledSmsControlDuration;
	}


	/**
	 * @param scheduledSmsControlDuration The scheduledSmsControlDuration to set.
	 */
	public void setScheduledSmsControlDuration(String scheduledSmsControlDuration) {
		this.scheduledSmsControlDuration = scheduledSmsControlDuration;
	}


	/**
	 * @return Returns the selected_charset.
	 */
	public String getSelected_charset() {
		return selected_charset;
	}


	/**
	 * @param selected_charset The selected_charset to set.
	 */
	public void setSelected_charset(String selected_charset) {
		this.selected_charset = selected_charset;
	}


	/**
	 * @return Returns the selected_ownership.
	 */
	public String getSelected_ownership() {
		return selected_ownership;
	}


	/**
	 * @param selected_ownership The selected_ownership to set.
	 */
	public void setSelected_ownership(String selected_ownership) {
		this.selected_ownership = selected_ownership;
	}


	/**
	 * @return Returns the selected_status.
	 */
	public String getSelected_status() {
		return selected_status;
	}


	/**
	 * @param selected_status The selected_status to set.
	 */
	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	/**
	 * @return Returns the smsFailureNotificationEmail.
	 */
	public String getSmsFailureNotificationEmail() {
		return smsFailureNotificationEmail;
	}


	/**
	 * @param smsFailureNotificationEmail The smsFailureNotificationEmail to set.
	 */
	public void setSmsFailureNotificationEmail(String smsFailureNotificationEmail) {
		this.smsFailureNotificationEmail = smsFailureNotificationEmail;
	}


	/**
	 * @return Returns the smsPrioritize.
	 */
	public String getSmsPrioritize() {
		return smsPrioritize;
	}


	/**
	 * @param smsPrioritize The smsPrioritize to set.
	 */
	public void setSmsPrioritize(String smsPrioritize) {
		this.smsPrioritize = smsPrioritize;
	}


	/**
	 * @return Returns the smsQuota.
	 */
	public String getSmsQuota() {
		return smsQuota;
	}


	/**
	 * @param smsQuota The smsQuota to set.
	 */
	public void setSmsQuota(String smsQuota) {
		this.smsQuota = smsQuota;
	}


	/**
	 * @return Returns the smsQuotaDefault.
	 */
	public String getSmsQuotaDefault() {
		return smsQuotaDefault;
	}


	/**
	 * @param smsQuotaDefault The smsQuotaDefault to set.
	 */
	public void setSmsQuotaDefault(String smsQuotaDefault) {
		this.smsQuotaDefault = smsQuotaDefault;
	}


	/**
	 * @return Returns the smsTelcoRouting.
	 */
	public String getSmsTelcoRouting() {
		return smsTelcoRouting;
	}


	/**
	 * @param smsTelcoRouting The smsTelcoRouting to set.
	 */
	public void setSmsTelcoRouting(String smsTelcoRouting) {
		this.smsTelcoRouting = smsTelcoRouting;
	}

	/**
	 * @return Returns the smsTimeControlBatch.
	 */
	public String getSmsTimeControlBatch() {
		return smsTimeControlBatch;
	}


	/**
	 * @param smsTimeControlBatch The smsTimeControlBatch to set.
	 */
	public void setSmsTimeControlBatch(String smsTimeControlBatch) {
		this.smsTimeControlBatch = smsTimeControlBatch;
	}


	/**
	 * @return Returns the smsTimeControlDay.
	 */
	public String[] getSmsTimeControlDay() {
		return smsTimeControlDay;
	}

	/**
	 * @return Returns the smsTimeControlWeb.
	 */
	public String getSmsTimeControlWeb() {
		return smsTimeControlWeb;
	}


	/**
	 * @param smsTimeControlWeb The smsTimeControlWeb to set.
	 */
	public void setSmsTimeControlWeb(String smsTimeControlWeb) {
		this.smsTimeControlWeb = smsTimeControlWeb;
	}


	/**
	 * @return Returns the status_label.
	 */
	public ArrayList<String> getStatus_label() {
		return status_label;
	}


	/**
	 * @param status_label The status_label to set.
	 */
	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}


	/**
	 * @return Returns the status_value.
	 */
	public ArrayList<String> getStatus_value() {
		return status_value;
	}


	/**
	 * @param status_value The status_value to set.
	 */
	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}


	/**
	 * @return Returns the tableListDelimiter.
	 */
	public String getTableListDelimiter() {
		return tableListDelimiter;
	}


	/**
	 * @param tableListDelimiter The tableListDelimiter to set.
	 */
	public void setTableListDelimiter(String tableListDelimiter) {
		this.tableListDelimiter = tableListDelimiter;
	}


	/**
	 * @return Returns the version.
	 */
	public String getVersion() {
		return version;
	}


	/**
	 * @param version The version to set.
	 */
	public void setVersion(String version) {
		this.version = version;
	}


	/**
	 * @return Returns the systemDebug.
	 */
	public String getSystemDebug() {
		return systemDebug;
	}


	/**
	 * @param systemDebug The systemDebug to set.
	 */
	public void setSystemDebug(String systemDebug) {
		this.systemDebug = systemDebug;
	}


	/**
	 * @return Returns the passwordChangeDuration.
	 */
	public String getPasswordChangeDuration() {
		return passwordChangeDuration;
	}


	/**
	 * @param passwordChangeDuration The passwordChangeDuration to set.
	 */
	public void setPasswordChangeDuration(String passwordChangeDuration) {
		this.passwordChangeDuration = passwordChangeDuration;
	}


	/**
	 * @return Returns the routing_label.
	 */
	public ArrayList<String> getRouting_label() {
		return routing_label;
	}


	/**
	 * @param routing_label The routing_label to set.
	 */
	public void setRouting_label(ArrayList<String> routing_label) {
		this.routing_label = routing_label;
	}


	/**
	 * @return Returns the routing_value.
	 */
	public ArrayList<String> getRouting_value() {
		return routing_value;
	}


	/**
	 * @param routing_value The routing_value to set.
	 */
	public void setRouting_value(ArrayList<String> routing_value) {
		this.routing_value = routing_value;
	}

	/**
	 * @return Returns the telco_label.
	 */
	public ArrayList<String> getTelco_label() {
		return telco_label;
	}


	/**
	 * @param telco_label The telco_label to set.
	 */
	public void setTelco_label(ArrayList<String> telco_label) {
		this.telco_label = telco_label;
	}


	/**
	 * @return Returns the telco_value.
	 */
	public ArrayList<String> getTelco_value() {
		return telco_value;
	}


	/**
	 * @param telco_value The telco_value to set.
	 */
	public void setTelco_value(ArrayList<String> telco_value) {
		this.telco_value = telco_value;
	}


	/**
	 * @param smsTelcoRoutingParam The smsTelcoRoutingParam to set.
	 */
	public void setSmsTelcoRoutingParam(String[] smsTelcoRoutingSequence) {
		this.smsTelcoRoutingParam = smsTelcoRoutingSequence;
	}


	/**
	 * @return Returns the smsTelcoRoutingParam.
	 */
	public String[] getSmsTelcoRoutingParam() {
		return smsTelcoRoutingParam;
	}


	/**
	 * @return Returns the day_label.
	 */
	public ArrayList<String> getDay_label() {
		return day_label;
	}


	/**
	 * @param day_label The day_label to set.
	 */
	public void setDay_label(ArrayList<String> day_label) {
		this.day_label = day_label;
	}


	/**
	 * @return Returns the day_value.
	 */
	public ArrayList<String> getDay_value() {
		return day_value;
	}


	/**
	 * @param day_value The day_value to set.
	 */
	public void setDay_value(ArrayList<String> day_value) {
		this.day_value = day_value;
	}


	/**
	 * @return Returns the hour_label.
	 */
	public ArrayList<String> getHour_label() {
		return hour_label;
	}


	/**
	 * @param hour_label The hour_label to set.
	 */
	public void setHour_label(ArrayList<String> hour_label) {
		this.hour_label = hour_label;
	}


	/**
	 * @return Returns the hour_value.
	 */
	public ArrayList<String> getHour_value() {
		return hour_value;
	}


	/**
	 * @param hour_value The hour_value to set.
	 */
	public void setHour_value(ArrayList<String> hour_value) {
		this.hour_value = hour_value;
	}


	/**
	 * @return Returns the min_label.
	 */
	public ArrayList<String> getMin_label() {
		return min_label;
	}


	/**
	 * @param min_label The min_label to set.
	 */
	public void setMin_label(ArrayList<String> min_label) {
		this.min_label = min_label;
	}


	/**
	 * @return Returns the min_value.
	 */
	public ArrayList<String> getMin_value() {
		return min_value;
	}


	/**
	 * @param min_value The min_value to set.
	 */
	public void setMin_value(ArrayList<String> min_value) {
		this.min_value = min_value;
	}


	/**
	 * @param smsTimeControlDay The smsTimeControlDay to set.
	 */
	public void setSmsTimeControlDay(String[] smsTimeControlDay) {
		this.smsTimeControlDay = smsTimeControlDay;
	}


	/**
	 * @return Returns the smsTimeControlTimeFromHour.
	 */
	public String[] getSmsTimeControlTimeFromHour() {
		return smsTimeControlTimeFromHour;
	}


	/**
	 * @param smsTimeControlTimeFromHour The smsTimeControlTimeFromHour to set.
	 */
	public void setSmsTimeControlTimeFromHour(String[] smsTimeControlTimeFromHour) {
		this.smsTimeControlTimeFromHour = smsTimeControlTimeFromHour;
	}
	
	/**
	 * @return Returns the smsTimeControlTimeFromHour.
	 */
	public String getSmsTimeControlTimeFromHour(int index) {
		return smsTimeControlTimeFromHour[index];
	}


	/**
	 * @param smsTimeControlTimeFromHour The smsTimeControlTimeFromHour to set.
	 */
	public void setSmsTimeControlTimeFromHour(String smsTimeControlTimeFromHour, int index) {
		this.smsTimeControlTimeFromHour[index] = smsTimeControlTimeFromHour;
	}


	/**
	 * @return Returns the smsTimeControlTimeFromMin.
	 */
	public String[] getSmsTimeControlTimeFromMin() {
		return smsTimeControlTimeFromMin;
	}


	/**
	 * @param smsTimeControlTimeFromMin The smsTimeControlTimeFromMin to set.
	 */
	public void setSmsTimeControlTimeFromMin(String[] smsTimeControlTimeFromMin) {
		this.smsTimeControlTimeFromMin = smsTimeControlTimeFromMin;
	}


	/**
	 * @return Returns the smsTimeControlTimeToHour.
	 */
	public String[] getSmsTimeControlTimeToHour() {
		return smsTimeControlTimeToHour;
	}

	/**
	 * @param smsTimeControlTimeToHour The smsTimeControlTimeToHour to set.
	 */
	public void setSmsTimeControlTimeToHour(String[] smsTimeControlTimeToHour) {
		this.smsTimeControlTimeToHour = smsTimeControlTimeToHour;
	}

	/**
	 * @return Returns the smsTimeControlTimeToMin.
	 */
	public String[] getSmsTimeControlTimeToMin() {
		return smsTimeControlTimeToMin;
	}


	/**
	 * @param smsTimeControlTimeToMin The smsTimeControlTimeToMin to set.
	 */
	public void setSmsTimeControlTimeToMin(String[] smsTimeControlTimeToMin) {
		this.smsTimeControlTimeToMin = smsTimeControlTimeToMin;
	}


	public String getDept() {
		return dept;
	}


	public void setDept(String dept) {
		this.dept = dept;
	}


	public String getFtpFailureNotificationSms() {
		return ftpFailureNotificationSms;
	}


	public void setFtpFailureNotificationSms(String smsFailureNotificationSms) {
		this.ftpFailureNotificationSms = smsFailureNotificationSms;
	}


	public String getFtpFailureNotificationSmsSender() {
		return ftpFailureNotificationSmsSender;
	}


	public void setFtpFailureNotificationSmsSender(
			String ftpFailureNotificationSmsSender) {
		this.ftpFailureNotificationSmsSender = ftpFailureNotificationSmsSender;
	}


	public String getSmsFailureThreshold() {
		return smsFailureThreshold;
	}


	public void setSmsFailureThreshold(String smsFailureThreshold) {
		this.smsFailureThreshold = smsFailureThreshold;
	}


	public String[] getFtpFailureNotification() {
		return ftpFailureNotification;
	}


	public void setFtpFailureNotification(String[] ftpFailureNotification) {
		this.ftpFailureNotification = ftpFailureNotification;
	}


	public String[] getSmsFailureNotification() {
		return smsFailureNotification;
	}


	public void setSmsFailureNotification(String[] smsFailureNotification) {
		this.smsFailureNotification = smsFailureNotification;
	}


	public String getSmsFailureNotificationSms() {
		return smsFailureNotificationSms;
	}


	public void setSmsFailureNotificationSms(String smsFailureNotificationSms) {
		this.smsFailureNotificationSms = smsFailureNotificationSms;
	}


	public String getSmsFailureNotificationSmsSender() {
		return smsFailureNotificationSmsSender;
	}


	public void setSmsFailureNotificationSmsSender(
			String smsFailureNotificationSmsSender) {
		this.smsFailureNotificationSmsSender = smsFailureNotificationSmsSender;
	}


	public String getSmsFailureThresholdTime() {
		return smsFailureThresholdTime;
	}


	public void setSmsFailureThresholdTime(String smsFailureThresholdTime) {
		this.smsFailureThresholdTime = smsFailureThresholdTime;
	}


	public String getFtpFailureNotificationEmailSender() {
		return ftpFailureNotificationEmailSender;
	}


	public void setFtpFailureNotificationEmailSender(
			String ftpFailureNotificationEmailSender) {
		this.ftpFailureNotificationEmailSender = ftpFailureNotificationEmailSender;
	}


	public String getSmsFailureNotificationEmailSender() {
		return smsFailureNotificationEmailSender;
	}


	public void setSmsFailureNotificationEmailSender(
			String smsFailureNotificationEmailSender) {
		this.smsFailureNotificationEmailSender = smsFailureNotificationEmailSender;
	}


	public String getSmsFailureThresholdPeriod() {
		return smsFailureThresholdPeriod;
	}


	public void setSmsFailureThresholdPeriod(String smsFailureThresholdPeriod) {
		this.smsFailureThresholdPeriod = smsFailureThresholdPeriod;
	}


	public String[] getBulkSmsThresholdNotification() {
		return bulkSmsThresholdNotification;
	}


	public void setBulkSmsThresholdNotification(
			String[] bulkSmsThresholdNotification) {
		this.bulkSmsThresholdNotification = bulkSmsThresholdNotification;
	}


	public String getBulkSmsThresholdNotificationEmailSender() {
		return bulkSmsThresholdNotificationEmailSender;
	}


	public void setBulkSmsThresholdNotificationEmailSender(
			String bulkSmsThresholdNotificationEmailSender) {
		this.bulkSmsThresholdNotificationEmailSender = bulkSmsThresholdNotificationEmailSender;
	}


	public String getBulkSmsThresholdNotificationSms() {
		return bulkSmsThresholdNotificationSms;
	}


	public void setBulkSmsThresholdNotificationSms(
			String bulkSmsThresholdNotificationSms) {
		this.bulkSmsThresholdNotificationSms = bulkSmsThresholdNotificationSms;
	}


	public String getBulkSmsThresholdNotificationSmsSender() {
		return bulkSmsThresholdNotificationSmsSender;
	}


	public void setBulkSmsThresholdNotificationSmsSender(
			String bulkSmsThresholdNotificationSmsSender) {
		this.bulkSmsThresholdNotificationSmsSender = bulkSmsThresholdNotificationSmsSender;
	}

	public String getUsageLogDuration() {
		return usageLogDuration;
	}


	public void setUsageLogDuration(String usageLogDuration) {
		this.usageLogDuration = usageLogDuration;
	}


	public String getSystemLogDuration() {
		return systemLogDuration;
	}


	public void setSystemLogDuration(String systemLogDuration) {
		this.systemLogDuration = systemLogDuration;
	}


	public String getAccountDormancyCheck() {
		return accountDormancyCheck;
	}


	public void setAccountDormancyCheck(String accountDormancyCheck) {
		this.accountDormancyCheck = accountDormancyCheck;
	}

	public String getQueueSmsThreshold() {
		return queueSmsThreshold;
	}


	public void setQueueSmsThreshold(String queueSmsThreshold) {
		this.queueSmsThreshold = queueSmsThreshold;
	}


	public String[] getQueueSmsThresholdNotification() {
		return queueSmsThresholdNotification;
	}


	public void setQueueSmsThresholdNotification(
			String[] queueSmsThresholdNotification) {
		this.queueSmsThresholdNotification = queueSmsThresholdNotification;
	}


	public String getQueueSmsThresholdNotificationEmail() {
		return queueSmsThresholdNotificationEmail;
	}


	public void setQueueSmsThresholdNotificationEmail(
			String queueSmsThresholdNotificationEmail) {
		this.queueSmsThresholdNotificationEmail = queueSmsThresholdNotificationEmail;
	}


	public String getQueueSmsThresholdNotificationEmailSender() {
		return queueSmsThresholdNotificationEmailSender;
	}


	public void setQueueSmsThresholdNotificationEmailSender(
			String queueSmsThresholdNotificationEmailSender) {
		this.queueSmsThresholdNotificationEmailSender = queueSmsThresholdNotificationEmailSender;
	}


	public String getQueueSmsThresholdNotificationSms() {
		return queueSmsThresholdNotificationSms;
	}


	public void setQueueSmsThresholdNotificationSms(
			String queueSmsThresholdNotificationSms) {
		this.queueSmsThresholdNotificationSms = queueSmsThresholdNotificationSms;
	}


	public String getQueueSmsThresholdNotificationSmsSender() {
		return queueSmsThresholdNotificationSmsSender;
	}


	public void setQueueSmsThresholdNotificationSmsSender(
			String queueSmsThresholdNotificationSmsSender) {
		this.queueSmsThresholdNotificationSmsSender = queueSmsThresholdNotificationSmsSender;
	}


	public String getQueueSmsThresholdPeriod() {
		return queueSmsThresholdPeriod;
	}


	public void setQueueSmsThresholdPeriod(String queueSmsThresholdPeriod) {
		this.queueSmsThresholdPeriod = queueSmsThresholdPeriod;
	}


	public String getQueueSmsThresholdTime() {
		return queueSmsThresholdTime;
	}


	public void setQueueSmsThresholdTime(String queueSmsThresholdTime) {
		this.queueSmsThresholdTime = queueSmsThresholdTime;
	}


	public String getTotalQueuedSizeAlertThreshold() {
		return totalQueuedSizeAlertThreshold;
	}


	public void setTotalQueuedSizeAlertThreshold(
			String totalQueuedSizeAlertThreshold) {
		this.totalQueuedSizeAlertThreshold = totalQueuedSizeAlertThreshold;
	}


	public String getTotalQueuedSizeAlertCheckPeriod() {
		return totalQueuedSizeAlertCheckPeriod;
	}


	public void setTotalQueuedSizeAlertCheckPeriod(
			String totalQueuedSizeAlertCheckPeriod) {
		this.totalQueuedSizeAlertCheckPeriod = totalQueuedSizeAlertCheckPeriod;
	}


	public String getAutoPatchToUnsent() {
		return autoPatchToUnsent;
	}


	public void setAutoPatchToUnsent(String autoPatchToUnsent) {
		this.autoPatchToUnsent = autoPatchToUnsent;
	}


	public String getSystemSMSDefaultSender() {
		return systemSMSDefaultSender;
	}


	public void setSystemSMSDefaultSender(String systemSMSDefaultSender) {
		this.systemSMSDefaultSender = systemSMSDefaultSender;
	}


	public List<TblRef> getSmsTypes() {
		return smsTypes;
	}


	public void setSmsTypes(List<TblRef> smsTypes) {
		this.smsTypes = (ArrayList<TblRef>) smsTypes;
	}


	public int getDefaultSmsType() {
		return defaultSmsType;
	}


	public void setDefaultSmsType(int defaultSmsType) {
		this.defaultSmsType = defaultSmsType;
	}


	public String getCcaDataAge() {
		return ccaDataAge;
	}


	public void setCcaDataAge(String ccaDataAge) {
		this.ccaDataAge = ccaDataAge;
	}


}

