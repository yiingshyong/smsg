package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.common.modal.Option;
import com.bcb.crsmsg.util.CommonUtils;
import com.bcb.crsmsg.util.Constants;



public final class TelcoForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4851048155675524960L;
	
	private String telco_name;
	private String mobile_prefix;
	private String enable_prefix;
	private String home_prefix;
	private String incoming_prefix; 
	private String url;
	private String urlParam;
	private String className;
	private String username;
	private String password;
	private String within_cost;
	private String inter_cost;
	private String incoming_cost;
	private String submit;
	private String id;
	private int proxyPort;
	private String proxyAddr;
	private Integer selectedSvcProvider;
	private ArrayList<Option> serviceProvider;
	
	//for threshold
	private String enabled_st;
	private String queue_threshold;
	private String email_from;
	private String email_to;
	private String enable_notification;
	
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;
	
	private String encryption;
	private ArrayList<String> encryption_label=null;
	private ArrayList<String> encryption_value=null;
	
	private String shortCode;
	private String successCode;
	private String smsLimit;
	private String retryCode;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
				
		telco_name=null;
		enable_prefix=null;
		mobile_prefix=null;
		home_prefix=null;
		url=null;
		username=null;
		password=null;
		within_cost=null;
		inter_cost=null;
		incoming_cost=null;
		submit=null;
		incoming_prefix=null;
		enabled_st=null;
		queue_threshold=null;
		email_from=null;
		email_to=null;
		enable_notification=null;
		urlParam = null;
		className = null;
		shortCode=null;
		successCode=null;
		smsLimit=null;
		retryCode=null;
		
		selected_status=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_label.add(Constants.FORM_FIELD_STATUS_DISABLED);
		status_label.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		status_value = new ArrayList<String>();
		status_value.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_value.add(Constants.FORM_FIELD_STATUS_DISABLED);
		status_value.add(Constants.FORM_FIELD_STATUS_CLOSED);
		serviceProvider = (ArrayList<Option>) CommonUtils.getSvcProviderOptions();
		
		encryption=null;
		encryption_label = new ArrayList<String>();
		encryption_label.add(Constants.STATUS_ACTIVE);
		encryption_label.add(Constants.STATUS_DISABLED);
		
		
		encryption_value = new ArrayList<String>();
		encryption_value.add(Constants.STATUS_TRUE);
		encryption_value.add(Constants.STATUS_FALSE);
		
	}


	public String getEncryption() {
		return encryption;
	}


	public void setEncryption(String encryption) {
		this.encryption = encryption;
	}


	public ArrayList<String> getEncryption_label() {
		return encryption_label;
	}


	public void setEncryption_label(ArrayList<String> encryption_label) {
		this.encryption_label = encryption_label;
	}


	public ArrayList<String> getEncryption_value() {
		return encryption_value;
	}


	public void setEncryption_value(ArrayList<String> encryption_value) {
		this.encryption_value = encryption_value;
	}


	public String getEnable_prefix() {
		return enable_prefix;
	}


	public void setEnable_prefix(String enable_prefix) {
		this.enable_prefix = enable_prefix;
	}


	public String getInter_cost() {
		return inter_cost;
	}


	public void setInter_cost(String inter_cost) {
		this.inter_cost = inter_cost;
	}

	public String getMobile_prefix() {
		return mobile_prefix;
	}


	public void setMobile_prefix(String mobile_prefix) {
		this.mobile_prefix = mobile_prefix;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getSubmit() {
		return submit;
	}


	public void setSubmit(String submit) {
		this.submit = submit;
	}


	public String getTelco_name() {
		return telco_name;
	}


	public void setTelco_name(String telco_name) {
		this.telco_name = telco_name;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getWithin_cost() {
		return within_cost;
	}


	public void setWithin_cost(String within_cost) {
		this.within_cost = within_cost;
	}
	
	public String getHome_prefix() {
		return home_prefix;
	}


	public void setHome_prefix(String home_prefix) {
		this.home_prefix = home_prefix;
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public String getEmail_from() {
		return email_from;
	}


	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}


	public String getEmail_to() {
		return email_to;
	}


	public void setEmail_to(String email_to) {
		this.email_to = email_to;
	}


	public String getEnable_notification() {
		return enable_notification;
	}


	public void setEnable_notification(String enable_notification) {
		this.enable_notification = enable_notification;
	}


	public String getQueue_threshold() {
		return queue_threshold;
	}


	public void setQueue_threshold(String queue_threshold) {
		this.queue_threshold = queue_threshold;
	}

	public String getEnabled_st() {
		return enabled_st;
	}


	public void setEnabled_st(String enabled_st) {
		this.enabled_st = enabled_st;
	}


	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(this.selectedSvcProvider == 0){
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Service Provider"));			
		}
		
		// Mandatory
		if ( telco_name!=null&&telco_name.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Telco Name"));
		
		if ( url!=null&&url.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "URL"));
				
		if ( urlParam!=null&&urlParam.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "URL Param"));
		
		if ( className!=null&&className.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Implementation Class"));
		
		if ( username!=null&&username.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Username"));
		
		if ( password!=null&&password.length() == 0 )
			errors.add(Constants.FORM_CONTACT,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, "Password"));
		
		// Length Validation
		if(telco_name!=null&&telco_name.length()>Integer.valueOf(Constants.LENGTH_100)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_NAME, Constants.LENGTH_100));
		}
		
		if(url!=null&&url.length()>Integer.valueOf(Constants.LENGTH_500)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_URL, Constants.LENGTH_500));
		}
		
		if(username!=null&&username.length()>Integer.valueOf(Constants.LENGTH_50)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_USERNAME, Constants.LENGTH_50));
		}
		
		if(password!=null&&password.length()>Integer.valueOf(Constants.LENGTH_50)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_PASSWORD, Constants.LENGTH_50));
		}
		
		if(mobile_prefix!=null&&mobile_prefix.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).length()>Integer.valueOf(Constants.LENGTH_100)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_PREFIX_ROUTING, Constants.LENGTH_100));
		}
		
		if(home_prefix!=null&&home_prefix.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).length()>Integer.valueOf(Constants.LENGTH_100)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_PREFIX_HOME, Constants.LENGTH_100));
		}
		
		if(incoming_prefix!=null&&incoming_prefix.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).length()>Integer.valueOf(Constants.LENGTH_100)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_PREFIX_INCOMING, Constants.LENGTH_100));
		}
		
		if(within_cost!=null&&within_cost.length()>Integer.valueOf(Constants.LENGTH_10)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_WITHIN_CHARGE, Constants.LENGTH_10));
		}
		
		if(inter_cost!=null&&inter_cost.length()>Integer.valueOf(Constants.LENGTH_10)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_INTER_CHARGE, Constants.LENGTH_10));
		}
		
		if(incoming_cost!=null&&incoming_cost.length()>Integer.valueOf(Constants.LENGTH_10)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_INCOMING_CHARGE, Constants.LENGTH_10));
		}
		
		if(queue_threshold!=null&&queue_threshold.length()>Integer.valueOf(Constants.LENGTH_10)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_QUEUE_THRESHOLD_NO, Constants.LENGTH_10));
		}
		
		if(email_from!=null&&email_from.length()>Integer.valueOf(Constants.LENGTH_100)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_NOTIFICATION_EMAILFROM, Constants.LENGTH_100));
		}
		
		if(email_to!=null&&email_to.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).length()>Integer.valueOf(Constants.LENGTH_500)){
			errors.add(Constants.FORM_TELCO,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_TELCO_NOTIFICATION_EMAILTO, Constants.LENGTH_500));
		}
		
		// Validation Input
		if(mobile_prefix!=null&&mobile_prefix.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).length()>0){
			String[] mobileNoList=mobile_prefix.split(Constants.SPECIAL_CHAR_NEWLINE);
			Pattern p = Pattern.compile("^[\\d]{1,5}$");
		
			String error=Constants.STR_EMPTY;
			
			for(int i=0;i<mobileNoList.length;i++){
				Matcher m = p.matcher(mobileNoList[i]);
			    if(!m.matches()){
			    	if(i!=0){
						error+=", ";
					}
			    	
			    	error+=mobileNoList[i];
			    }
			}
			
			if(error.length()>0){
				errors.add(Constants.FORM_TELCO,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_TELCO_PREFIX_ROUTING, error));
			}
		}
		
		if(home_prefix!=null&&home_prefix.replaceAll(Constants.SPECIAL_CHAR_NEWLINE, Constants.STR_EMPTY).contains("^[\\d]")){
			String[] mobileNoList=home_prefix.split(Constants.SPECIAL_CHAR_NEWLINE);
			Pattern p = Pattern.compile("^[\\d]{1,5}$");
		
			String error=Constants.STR_EMPTY;
			
			for(int i=0;i<mobileNoList.length;i++){
				Matcher m = p.matcher(mobileNoList[i]);
			    if(!m.matches()){
			    	if(i!=0){
						error+=", ";
					}
			    	
			    	error+=mobileNoList[i];
			    }
			}
			
			if(error.length()>0){
				errors.add(Constants.FORM_TELCO,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_TELCO_PREFIX_HOME, error));
			}
		}
		
		if(within_cost!=null&&within_cost.length()>0){
			Pattern p = Pattern.compile("^[\\d]{0,10}$");
			
			String error=Constants.STR_EMPTY;
			
			Matcher m = p.matcher(within_cost);
			    if(!m.matches()){
			    	error+=within_cost;
			    }

			if(error.length()>0){
				errors.add(Constants.FORM_TELCO,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_TELCO_WITHIN_CHARGE, error));
			}
		}
		
		if(inter_cost!=null&&inter_cost.length()>0){
			Pattern p = Pattern.compile("^[\\d]{0,10}$");
			
			String error=Constants.STR_EMPTY;
			
			Matcher m = p.matcher(inter_cost);
			    if(!m.matches()){
			    	error+=inter_cost;
			    }

			if(error.length()>0){
				errors.add(Constants.FORM_TELCO,
		    			new ActionMessage(Constants.ERRMSG_FIELD_FORMAT_INVALID, Constants.FORM_FIELD_TELCO_INTER_CHARGE, error));
			}
		}
		
		return (errors);
		
	}


	public String getSelected_status() {
		return selected_status;
	}


	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	public ArrayList<String> getStatus_label() {
		return status_label;
	}


	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}


	public ArrayList<String> getStatus_value() {
		return status_value;
	}


	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}

	public String getIncoming_cost() {
		return incoming_cost;
	}


	public void setIncoming_cost(String incoming_cost) {
		this.incoming_cost = incoming_cost;
	}


	public String getIncoming_prefix() {
		return incoming_prefix;
	}


	public void setIncoming_prefix(String incoming_prefix) {
		this.incoming_prefix = incoming_prefix;
	}


	public String getUrlParam() {
		return urlParam;
	}


	public void setUrlParam(String urlParam) {
		this.urlParam = urlParam;
	}


	public String getClassName() {
		return className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public int getProxyPort() {
		return proxyPort;
	}


	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}


	public String getProxyAddr() {
		return proxyAddr;
	}


	public void setProxyAddr(String proxyAddr) {
		this.proxyAddr = proxyAddr;
	}


	public List<Option> getServiceProvider() {
		return serviceProvider;
	}


	public void setServiceProvider(List<Option> serviceProvider) {
		this.serviceProvider = (ArrayList<Option>) serviceProvider;
	}


	public Integer getSelectedSvcProvider() {
		return selectedSvcProvider;
	}


	public void setSelectedSvcProvider(Integer selectedSvcProvider) {
		this.selectedSvcProvider = selectedSvcProvider;
	}


	public String getShortCode() {
		return shortCode;
	}


	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}


	public String getSuccessCode() {
		return successCode;
	}


	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}


	public String getSmsLimit() {
		return smsLimit;
	}


	public void setSmsLimit(String smsLimit) {
		this.smsLimit = smsLimit;
	}


	public String getRetryCode() {
		return retryCode;
	}


	public void setRetryCode(String retryCode) {
		this.retryCode = retryCode;
	}


	public void setServiceProvider(ArrayList<Option> serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	
	
	
	

}

