package com.bcb.crsmsg.forms;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.util.Constants;
import com.bcb.crsmsg.util.DBUtilsCrsmsg;



public final class TemplateMessageForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 677354015664240037L;
	private String templateId;
	private String messageCode;
	private String messageText;
	private String dynamicFieldNo;
	private String dynamicFieldName;
	
	private Integer selected_department;
	private ArrayList department_label=null;
	private ArrayList department_value=null;

	private String selected_type;
	private ArrayList<String> type_label=null;
	private ArrayList<String> type_value=null;
	
	private String selected_status;
	private ArrayList<String> status_label=null;
	private ArrayList<String> status_value=null;

	private String selected_charset;
	private ArrayList<String> charset_label=null;
	private ArrayList<String> charset_value=null;
	
	// --------------------------------------------------------- Public Methods
	
	/**
	 * Reset all properties to their default values.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		templateId=null;
		messageCode=null;
		messageText=null;
		dynamicFieldNo=null;
		dynamicFieldName=null;
		
		selected_department=null;
		department_label=null;
		department_value=null;
		
		selected_type=null;
		type_label = new ArrayList<String>();
		type_label.add(Constants.FORM_FIELD_CONTACT_TYPE_PERSONAL);
		type_label.add(Constants.FORM_FIELD_CONTACT_TYPE_GROUP);
		type_value = new ArrayList<String>();
		type_value.add(Constants.FORM_FIELD_CONTACT_TYPE_PERSONAL);
		type_value.add(Constants.FORM_FIELD_CONTACT_TYPE_GROUP);
		
		selected_status=null;
		status_label = new ArrayList<String>();
		status_label.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_label.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		status_value = new ArrayList<String>();
		status_value.add(Constants.FORM_FIELD_STATUS_ACTIVE);
		status_value.add(Constants.FORM_FIELD_STATUS_CLOSED);
		
		selected_charset=null;
		charset_label = new ArrayList<String>();
		charset_label.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_label.add(Constants.FORM_FIELD_CHARSET_UTF8);
		
		charset_value = new ArrayList<String>();
		charset_value.add(Constants.FORM_FIELD_CHARSET_ASCIITEXT);
		charset_value.add(Constants.FORM_FIELD_CHARSET_UTF8);
	}
	
	
	/**
	 * Validate the properties that have been set from this HTTP request,
	 * and return an <code>ActionErrors</code> object that encapsulates any
	 * validation errors that have been found.  If no errors are found, return
	 * <code>null</code> or an <code>ActionErrors</code> object with no
	 * recorded error messages.
	 *
	 * @param mapping The mapping used to select this instance
	 * @param request The servlet request we are processing
	 */
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		// Mandatory
		if ( messageCode!=null&&messageCode.length() == 0 )
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MESSAGE_CODE));

		if ( messageText!=null&&messageText.length() == 0)
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_MESSAGE_TEXT));

		if (selected_department!=null&&selected_department.intValue() == 0 )
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_USER_DEPARTMENT));
		
		if (selected_type!=null&&selected_type.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_TYPE));
		
		if (selected_status!=null&&selected_status.equals(Constants.VALUE_0))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_MANDATORY, Constants.FORM_FIELD_STATUS));
		
		// Size Limit
		if ( messageCode!=null&&messageCode.length() >Integer.valueOf(Constants.LENGTH_20))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MESSAGE_CODE, Constants.LENGTH_20));
		
		if ( messageText!=null&&messageText.length() >Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MESSAGE_TEXT, Constants.LENGTH_500));
		
		if ( dynamicFieldNo!=null&&dynamicFieldNo.length() >Integer.valueOf(Constants.LENGTH_2))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_DYNAMIC_FIELD_NO, Constants.LENGTH_2));
		
		if ( dynamicFieldName!=null&&dynamicFieldName.length() >Integer.valueOf(Constants.LENGTH_500))
			errors.add(Constants.FORM_TEMPLATEMESSAGE,
					new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_DYNAMIC_FIELD_NAME, Constants.LENGTH_500));
		
		//Validation
		if((templateId==null||templateId.length()==0)&&messageCode!=null&&messageCode.length()>0){
			List tempList=DBUtilsCrsmsg.DBHQLCommand("from MsgTemplate where templateCode=?", new Object[]{messageCode});
			if(tempList.size()>0){
				errors.add(Constants.FORM_TEMPLATEMESSAGE,
						new ActionMessage(Constants.ERRMSG_ACTION_FAIL_DUPLICATE, Constants.FORM_FIELD_MESSAGE_CODE));
			}
		}
		
		if(dynamicFieldNo!=null&&dynamicFieldNo.length()>0&&dynamicFieldName.length()>0){
			if(!Integer.valueOf(dynamicFieldNo).equals(dynamicFieldName.split(Constants.SPECIAL_CHAR_COMMA).length)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE,
						new ActionMessage(Constants.ERRMSG_FIELD_COMPARE_NOTMATCH, Constants.FORM_FIELD_DYNAMIC_FIELD_NO, Constants.FORM_FIELD_DYNAMIC_FIELD_NAME));
			}
		}
		return (errors);
		
	}


	/**
	 * @return Returns the department_label.
	 */
	public ArrayList getDepartment_label() {
		return department_label;
	}


	/**
	 * @param department_label The department_label to set.
	 */
	public void setDepartment_label(ArrayList department_label) {
		this.department_label = department_label;
	}


	/**
	 * @return Returns the department_value.
	 */
	public ArrayList getDepartment_value() {
		return department_value;
	}


	/**
	 * @param department_value The department_value to set.
	 */
	public void setDepartment_value(ArrayList department_value) {
		this.department_value = department_value;
	}


	/**
	 * @return Returns the dynamicFieldName.
	 */
	public String getDynamicFieldName() {
		return dynamicFieldName;
	}


	/**
	 * @param dynamicFieldName The dynamicFieldName to set.
	 */
	public void setDynamicFieldName(String dynamicFieldName) {
		this.dynamicFieldName = dynamicFieldName;
	}


	/**
	 * @return Returns the dynamicFieldNo.
	 */
	public String getDynamicFieldNo() {
		return dynamicFieldNo;
	}


	/**
	 * @param dynamicFieldNo The dynamicFieldNo to set.
	 */
	public void setDynamicFieldNo(String dynamicFieldNo) {
		this.dynamicFieldNo = dynamicFieldNo;
	}


	/**
	 * @return Returns the messageCode.
	 */
	public String getMessageCode() {
		return messageCode;
	}


	/**
	 * @param messageCode The messageCode to set.
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}


	/**
	 * @return Returns the messageText.
	 */
	public String getMessageText() {
		return messageText;
	}


	/**
	 * @param messageText The messageText to set.
	 */
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}


	/**
	 * @return Returns the selected_department.
	 */
	public Integer getSelected_department() {
		return selected_department;
	}


	/**
	 * @param selected_department The selected_department to set.
	 */
	public void setSelected_department(Integer selected_department) {
		this.selected_department = selected_department;
	}


	/**
	 * @return Returns the selected_status.
	 */
	public String getSelected_status() {
		return selected_status;
	}


	/**
	 * @param selected_status The selected_status to set.
	 */
	public void setSelected_status(String selected_status) {
		this.selected_status = selected_status;
	}


	/**
	 * @return Returns the selected_type.
	 */
	public String getSelected_type() {
		return selected_type;
	}


	/**
	 * @param selected_type The selected_type to set.
	 */
	public void setSelected_type(String selected_type) {
		this.selected_type = selected_type;
	}


	/**
	 * @return Returns the status_label.
	 */
	public ArrayList<String> getStatus_label() {
		return status_label;
	}


	/**
	 * @param status_label The status_label to set.
	 */
	public void setStatus_label(ArrayList<String> status_label) {
		this.status_label = status_label;
	}


	/**
	 * @return Returns the status_value.
	 */
	public ArrayList<String> getStatus_value() {
		return status_value;
	}


	/**
	 * @param status_value The status_value to set.
	 */
	public void setStatus_value(ArrayList<String> status_value) {
		this.status_value = status_value;
	}


	/**
	 * @return Returns the templateId.
	 */
	public String getTemplateId() {
		return templateId;
	}


	/**
	 * @param templateId The templateId to set.
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}


	/**
	 * @return Returns the type_label.
	 */
	public ArrayList<String> getType_label() {
		return type_label;
	}


	/**
	 * @param type_label The type_label to set.
	 */
	public void setType_label(ArrayList<String> type_label) {
		this.type_label = type_label;
	}


	/**
	 * @return Returns the type_value.
	 */
	public ArrayList<String> getType_value() {
		return type_value;
	}


	/**
	 * @param type_value The type_value to set.
	 */
	public void setType_value(ArrayList<String> type_value) {
		this.type_value = type_value;
	}


	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}


	/**
	 * @return Returns the charset_label.
	 */
	public ArrayList<String> getCharset_label() {
		return charset_label;
	}


	/**
	 * @param charset_label The charset_label to set.
	 */
	public void setCharset_label(ArrayList<String> charset_label) {
		this.charset_label = charset_label;
	}


	/**
	 * @return Returns the charset_value.
	 */
	public ArrayList<String> getCharset_value() {
		return charset_value;
	}


	/**
	 * @param charset_value The charset_value to set.
	 */
	public void setCharset_value(ArrayList<String> charset_value) {
		this.charset_value = charset_value;
	}


	/**
	 * @return Returns the selected_charset.
	 */
	public String getSelected_charset() {
		return selected_charset;
	}


	/**
	 * @param selected_charset The selected_charset to set.
	 */
	public void setSelected_charset(String selected_charset) {
		this.selected_charset = selected_charset;
	}
	
}

