package com.bcb.crsmsg.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.bcb.crsmsg.modal.Result;
import com.bcb.crsmsg.util.Constants;

public class TemplateMessageSearchForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5541197078612139051L;
	private String searchkey;
	private ArrayList<String> searchby_label=new ArrayList<String>();
	private ArrayList<String> searchby_value=new ArrayList<String>();
	private String selectedState;	
	private Result resultList;
	private String[] checkedRecords;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		searchby_label = new ArrayList<String>();
		searchby_label.add(Constants.FORM_FIELD_USER_DEPARTMENT);
		searchby_label.add(Constants.FORM_FIELD_MESSAGE_CODE);
		searchby_label.add(Constants.FORM_FIELD_MESSAGE_TEXT);
		searchby_label.add(Constants.FORM_FIELD_STATUS);
		
		searchby_value = new ArrayList<String>();
		searchby_value.add(Constants.FORM_FIELD_USER_DEPARTMENT);
		searchby_value.add(Constants.FORM_FIELD_MESSAGE_CODE);
		searchby_value.add(Constants.FORM_FIELD_MESSAGE_TEXT);
		searchby_value.add(Constants.FORM_FIELD_STATUS);
		
		selectedState = Constants.STR_EMPTY;
		searchkey=Constants.STR_EMPTY;
		//resultList=null;
		checkedRecords=null;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(selectedState.equals(Constants.FORM_FIELD_USER_DEPARTMENT)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_50)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_USER_DEPARTMENT, Constants.LENGTH_50 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_MESSAGE_CODE)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_20)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MESSAGE_CODE, Constants.LENGTH_20 ));			
			}
		}else if(selectedState.equals(Constants.FORM_FIELD_MESSAGE_TEXT)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_500)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_MESSAGE_TEXT, Constants.LENGTH_500 ));			
			}
		}else if (selectedState.equals(Constants.FORM_FIELD_STATUS)){
			if(searchkey.length()>Integer.valueOf(Constants.LENGTH_6)){
				errors.add(Constants.FORM_TEMPLATEMESSAGE_SEARCH, new ActionMessage(Constants.ERRMSG_FIELD_LIMIT_EXCEED, Constants.FORM_FIELD_STATUS, Constants.LENGTH_6 ));			
			}
		}
		
		return (errors);
		
	}

	/**
	 * @return Returns the searchby_label.
	 */
	public ArrayList<String> getSearchby_label() {
		return searchby_label;
	}

	/**
	 * @param searchby_label The searchby_label to set.
	 */
	public void setSearchby_label(ArrayList<String> searchby_label) {
		this.searchby_label = searchby_label;
	}

	/**
	 * @return Returns the searchby_value.
	 */
	public ArrayList<String> getSearchby_value() {
		return searchby_value;
	}

	/**
	 * @param searchby_value The searchby_value to set.
	 */
	public void setSearchby_value(ArrayList<String> searchby_value) {
		this.searchby_value = searchby_value;
	}

	/**
	 * @return Returns the searchkey.
	 */
	public String getSearchkey() {
		return searchkey;
	}

	/**
	 * @param searchkey The searchkey to set.
	 */
	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	/**
	 * @return Returns the selectedState.
	 */
	public String getSelectedState() {
		return selectedState;
	}

	/**
	 * @param selectedState The selectedState to set.
	 */
	public void setSelectedState(String selectedState) {
		this.selectedState = selectedState;
	}

	/**
	 * @return Returns the resultList.
	 */
	public Result getResultList() {
		return resultList;
	}

	/**
	 * @param resultList The resultList to set.
	 */
	public void setResultList(Result resultList) {
		this.resultList = resultList;
	}

	/**
	 * @return Returns the checkedRecords.
	 */
	public String[] getCheckedRecords() {
		return checkedRecords;
	}

	/**
	 * @param checkedRecords The checkedRecords to set.
	 */
	public void setCheckedRecords(String[] checkedRecords) {
		this.checkedRecords = checkedRecords;
	}

	/**
	 * @return Returns the serialVersionUID.
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}
