package com.bcb.crsmsg.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.Session;

import com.bcb.common.modal.LogonCredential;
import com.bcb.common.util.Constants;
import com.bcb.common.util.HibernateUtil;
import com.bcb.crsmsg.util.AppPropertiesConfig;
import com.bcb.crsmsg.util.HibernateUtilCrsmsg;

public class GenericFilter implements Filter {
	
	private static final Log log = LogFactory.getLog(GenericFilter.class);
	private boolean autoCloseSession;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if(!getPublicUrl().contains(StringUtils.removeStart(httpRequest.getRequestURI(), httpRequest.getContextPath()))){
			LogonCredential theLogonCredential= (LogonCredential) httpRequest.getSession().getAttribute(Constants.LOGONCREDENTIAL_KEY);
			if ( theLogonCredential == null)
			{
				((HttpServletResponse) response).sendRedirect(httpRequest.getContextPath() + "/common/Welcome.do");
				return;
			};
		}
		
		chain.doFilter(request, response);
		boolean oldAutoCloseSession = autoCloseSession; 
		autoCloseSession = AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.SMSG_AUTO_CLOSE_SESSION_END_REQUEST);
		if(oldAutoCloseSession != autoCloseSession){
			log.info("Auto close persistent session " + (autoCloseSession ? "Activated" : "Deactivated"));
		}
		if(autoCloseSession){
			try{
				Session session = HibernateUtil.getOnlySessionFactory().getCurrentSession();
				closeSession(session);
				session = HibernateUtilCrsmsg.getOnlySessionFactory().getCurrentSession();
				closeSession(session);		
	//			session = DaoBeanFactory.getHibernateTemplate().getSessionFactory().getCurrentSession();
	//			closeSession(session);
			}catch(Exception e){
				log.error("Error while closing hibernate session. Reason: " + e.getMessage(), e);
			}
		}
	}

	private List<String> getPublicUrl(){
//		List<String> urls = new ArrayList<String>();
//		urls.add("/common/Logoff.do");
//		urls.add("/common/Logon.do");
//		urls.add("/common/Welcome.do");
//		urls.add("/common/AccessDeny.do");
//		urls.add("/common/SysNotAvail.do");
//		urls.add("/pages/common/logon.jsp");
//		urls.add("/pages/common/notlogon.jsp");
//		urls.add("/sendsms.do");
//		urls.add("/receivesms.do");		
//		urls.add("/SSOLogin.do");
//		urls.add("/mksim");
//		urls.add("/ionnexsim");
//		urls.add("/genericsim");
		return AppPropertiesConfig.getInstance().getSettings().getList(AppPropertiesConfig.SMSG_PUBLIC_URL);
	}
	
	private void closeSession(Session session){
		try{
			if(session != null && session.getTransaction() != null && session.getTransaction().isActive()){
				log.info("Closing unclosed hibernate session");
				session.getTransaction().commit();
			}
		}catch(Exception e){
			log.error("Error while completing txn. Reason: " + e.getMessage(), e);
		}finally{
			if(session != null && session.isOpen()){
				session.close();
			}					
		}
	}
	
	@Override
	public void init(FilterConfig filterCfg) throws ServletException {
		this.autoCloseSession = AppPropertiesConfig.getInstance().getSettings().getBoolean(AppPropertiesConfig.SMSG_AUTO_CLOSE_SESSION_END_REQUEST);
		log.info("Initializing generic filter. Activate Auto close persistent session: " + this.autoCloseSession);		
	}

}
