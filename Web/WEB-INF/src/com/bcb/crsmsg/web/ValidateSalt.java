package com.bcb.crsmsg.web;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;

public class ValidateSalt implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {

        // Assume its HTTP
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpSession session = httpReq.getSession();
        // Get the salt sent with the request
        String storedToken = (String)session.getAttribute("csrfToken");
        String token = request.getParameter("token");
        System.out.println("storedToken : "+storedToken);
        System.out.println("token : "+token);
        // Validate that the salt is in the cache
        if(storedToken!=null){
	        if (storedToken.equals(token)){
	
	            // If the salt is in the cache, we move on

		        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
	        	session.setAttribute("csrfToken", salt);
	            chain.doFilter(request, response);
	        } else {
	            // Otherwise we throw an exception aborting the request flow
	        	System.out.println("INVALID CSRF TOKEN");
	        	((HttpServletResponse) response).sendRedirect(httpReq.getContextPath() + "/common/Welcome.do");
				return;
	        }
        } else {
            // Otherwise we throw an exception aborting the request flow
        	System.out.println("INVALID CSRF TOKEN");
        	((HttpServletResponse) response).sendRedirect(httpReq.getContextPath() + "/common/Welcome.do");
			return;
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
