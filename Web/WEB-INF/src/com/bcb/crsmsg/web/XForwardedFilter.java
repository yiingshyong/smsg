package com.bcb.crsmsg.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class XForwardedFilter implements Filter {
	
	private static final Log log = LogFactory.getLog(XForwardedFilter.class);
	private static final String FILTER_PARAM_REMOTE_IP_HEADER = "REMOTE_IP_HEADER_NAME";
	private static final String FILTER_PARAM_REMOTE_IP_HEADER_VALUE_SEPARATOR = "REMOTE_IP_HEADER_VALUE_SEP";
	private String remoteIpHeaderName = "x-forwarded-for";
	private String remoteIpHeaderValueSep = ",";
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest theRequest = (HttpServletRequest) request;
		String remoteIPFrHeader = theRequest.getHeader(remoteIpHeaderName);
		log.debug("HTTP Header " + this.remoteIpHeaderName + " value: '" + remoteIPFrHeader + "'");
		if(StringUtils.isNotEmpty(remoteIPFrHeader)){
			String[] remoteIPList = StringUtils.splitByWholeSeparator(remoteIPFrHeader, this.remoteIpHeaderValueSep);
			XForwardedRequestWrapper xRequest = new XForwardedRequestWrapper(theRequest);
			xRequest.setRemoteAddr(remoteIPList[0].trim());
			xRequest.setRemoteHost(xRequest.getRemoteAddr());	
			log.debug("Original Remote IP: " + theRequest.getRemoteAddr() + " changed to: " + xRequest.getRemoteAddr());
			chain.doFilter(xRequest, response);
		}else{
			chain.doFilter(request, response);
		}
		
	}

	@Override
	public void init(FilterConfig filterCfg) throws ServletException {
		String remoteIpHeaderName = filterCfg.getInitParameter(FILTER_PARAM_REMOTE_IP_HEADER);
		String remoteIpHeaderValueSep = filterCfg.getInitParameter(FILTER_PARAM_REMOTE_IP_HEADER_VALUE_SEPARATOR);
		log.info("Request Header Name for Remote IP Default to: " + this.remoteIpHeaderName);
		log.info("Remote IP Separator Default to: " + this.remoteIpHeaderValueSep);
		
		if(StringUtils.isNotEmpty(remoteIpHeaderName)){
			this.remoteIpHeaderName = remoteIpHeaderName;
			log.info("Request Header Name for Remote IP changed to :" + this.remoteIpHeaderName);
		}
		if(StringUtils.isNotEmpty(remoteIpHeaderValueSep)){
			this.remoteIpHeaderValueSep = remoteIpHeaderValueSep;
			log.info("Remote IP Separator Default to: " + this.remoteIpHeaderValueSep);
		}
	}

}
