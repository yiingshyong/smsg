package com.bcb.crsmsg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XForwardedRequestWrapper extends HttpServletRequestWrapper {
	
	protected String remoteAddr;    
    protected String remoteHost;
    
    public XForwardedRequestWrapper(HttpServletRequest request) {
		super(request);
		this.remoteAddr = request.getRemoteAddr();
		this.remoteHost = request.getRemoteHost();
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	
	
}
