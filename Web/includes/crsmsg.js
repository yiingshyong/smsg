
function showAll(object){
	var output = '';
	for (var i in object)
		output += i+ '<br>';
    
	var w=createWindow();  
	w.document.write(output);
	w.document.execCommand('SaveAs',true,".txt");
}

function createWindow(){
	var w = window.frames.w;
	w = document.createElement( 'iframe' );
	w.id = 'w';
	w.style.display = 'none';
	document.body.insertBefore( w );
	w = window.frames.w;
	
	if( !w ) {
		alert("open temp window");
	w = window.open( '', '_temp', 'width=100,height=100' );
		if( !w ) {
			window.alert( 'Sorry, could not create file.' ); return false;
		}
	}

	return w;
}

function checkAllCheckboxes(that){
	
	for(i=0;i<document.forms.length;i++){
		for(j=0;j<document.forms[i].elements.length;j++){
			if (document.forms[i].elements[j].type == "checkbox"){
			
				if (document.forms[i].elements[j].disabled != true)
				{
					if(that.checked ==true){
						document.forms[i].elements[j].checked=true;
					}else{
						document.forms[i].elements[j].checked=false;
					}
				}
			}
		}
	}
}

function uncheckAllCheckboxes(){
	for(i=0;i<document.forms.length;i++){
		for(j=0;j<document.forms[i].elements.length;j++){
			if (document.forms[i].elements[j].type == "checkbox"){
				document.forms[i].elements[j].checked=false;
			}
		}
	}
}

function toggleCheckAll(thisCheckBox, thatCheckBox){
	if(thisCheckBox!=null&&thatCheckBox!=null){
		if(thisCheckBox.length==null){
			if(thatCheckBox.length==null){
				thatCheckBox.checked=thisCheckBox.checked;
			}else{
				for(var i=0;i<thatCheckBox.length;i++){
					thatCheckBox[i].checked=thisCheckBox.checked;
				}
			}
		}else{
			checkAllStatus=true;
			for ( var i=0;i<thisCheckBox.length; i++){
				var e = thisCheckBox[i];
				if(!e.checked){
					checkAllStatus=false;
				}
			}
		
			if(thatCheckBox.length==null){
				thatCheckBox.checked=checkAllStatus;
			}else{
			}
		}
	}
}

function controlCheckAll(checkStatus, thatCheckBox){
	if(checkStatus!=null&&thatCheckBox!=null){
		if(thatCheckBox.length==null){
			thatCheckBox.checked=checkStatus;
		}else{
			for(var i=0;i<thatCheckBox.length;i++){
				thatCheckBox[i].checked=checkStatus;
			}
		}
	}
}

function confirmDeleteButton(deleteItem){
	return confirm("Are your sure to delete the selected "+deleteItem+"?");
}

function createFormElement(createElementName, name, type, value){
	theElement=document.createElement(createElementName);
	theElement.name=name;
	theElement.type=type;
	theElement.value=value;
	return theElement;
}

function copyFormElement(form, elementToCopy, createElementName, name, type){
	if(elementToCopy!=null){
		if(elementToCopy.length!=null){
			checked=false;
			for(i=0;elementToCopy!=null&&elementToCopy.length!=null&&i<elementToCopy.length;i++){
				if(elementToCopy[i].type=='checkbox'&&elementToCopy[i].checked){
					form.appendChild(createFormElement(createElementName, name, type, elementToCopy[i].value));
					checked=true;
				}
			}
			if(!checked){
				alert("No record(s) selected!");
				return false;
			}else{
				return true;
			}
		}else{
			if(elementToCopy.type=='checkbox'&&elementToCopy.checked){
				form.appendChild(createFormElement(createElementName, name, type, elementToCopy.value));
				return true;
			}else{
				alert("No record(s) selected!");
				return false;
			}
		}
	}else{
		alert("No record(s) selected!");
		return false;
	}
}

var theTarget;
var theIdTarget;

function setPopupTarget(theObject, id){
	theTarget=theObject;
	theIdTarget=id;
}

function getPopupTarget(){
	return theTarget;
}

function getPopupIdTarget(){
	return theIdTarget;
}


function updatePopupTarget(popupTarget, popupSource, id, idTarget){
	if(idTarget!=null){
		popupSource=id;
		idTarget=null;
	}

	if(popupSource!=null){
		if(popupSource instanceof String){
			if(!popupTarget.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar").match(popupSource.replace(/[^a-z^A-Z^0-9]/g, "escapeChar"))){
				popupTarget.value+=popupSource;
			}
		}else{
			if(popupSource.length!=null){
				for(i=0;i<popupSource.length;i++){
					if(popupSource[i].type=='checkbox'){
						if(popupSource[i].checked&&!popupTarget.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar").match(popupSource[i].value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar"))){
							popupTarget.value+=popupSource[i].value+"\r\n";
						}
					}else if(popupSource[i].type=='textbox'){
						if(!popupTarget.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar").match(popupSource[i].value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar"))){
							popupTarget.value+=popupSource[i].value+"\r\n";
						}
					}
				}
			}else{
				if(popupSource.type=='checkbox'){
					if(popupSource.checked&&!popupTarget.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar").match(popupSource.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar"))){
						popupTarget.value+=popupSource.value+"\r\n";
					}
				}else if(popupSource.type=='textbox'){
					if(!popupTarget.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar").match(popupSource.value.replace(/[^a-z^A-Z^0-9]/g, "escapeChar"))){
						popupTarget.value+=popupSource.value+"\r\n";
					}
				}
			}
		}
	}
}

function popUp(URL) {
  day = new Date();
  id = day.getTime();
  eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,status=1,menubar=0,resizable=1,left = 0,top = 0 height=500,width=850');");
}

function popUp2(URL) {
  day = new Date();
  id = day.getTime();
  eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,status=1,menubar=0,resizable=1,left = 0,top = 0,height=700,width=850');");
}

function toggleLayer(triggerObj, layer, showCondition){
	var divStyle = layer.style;
	
	if(triggerObj.type=='checkbox'){
		divStyle.display = ( triggerObj.checked ? "block" : "none" );
	}else if(triggerObj.type=='select-one'){
		divStyle.display = ( triggerObj.value==showCondition ? "block" : "none" );
	}
}

function toggleTimeMode(timeMode, divMonth, divDaily, divHour, divTime){
	if(timeMode=='1'){
		divMonth.style.display='block';
		divDaily.style.display='none';
		divHour.style.display='none';
		divTime.style.display='block';
	}else if(timeMode=='2'){
		divMonth.style.display='none';
		divDaily.style.display='block';
		divHour.style.display='none';
		divTime.style.display='block';
	}else if(timeMode=='3'){
		divMonth.style.display='none';
		divDaily.style.display='none';
		divHour.style.display='block';
		divTime.style.display='none';
	}else{
		alert("No time mode selected!");
	}
}

var long_sms;
var maxSMSVal=1;
var charPerSms=0;
var maxCharLimit=0;

function initCharCounter(long_sms, maxSMSVal, charPerSms){
	this.long_sms=long_sms;
	this.maxSMSVal=maxSMSVal;
	this.charPerSms=charPerSms;
}

function calculateDefaultNoChar(charSetOption){
	var optSelected = charSetOption.selectedIndex;
	var default_noChar=charPerSms;
	var header=40;// header 40 byes (assume)
	
	if ( optSelected == 0 ){// ACII/Text
		if(maxSMSVal!='1'&&long_sms == 'Y' ){
			default_noChar -=7;
		}
	}else if ( optSelected == 1 ){// UTF-8
		default_noChar=default_noChar*2-header;
		default_noChar /= 4;//4 hexadecimal char (2 bytes) into one chinese char
		if(maxSMSVal!='1'&&long_sms == 'Y' ){
			default_noChar -=3;
		}
	}
	
	return default_noChar;
}

function populateMaxCharLimit(charSetOption, maxCharField){
	maxCharField.value=maxCharLimit=calculateDefaultNoChar(charSetOption)*maxSMSVal;
	maxCharField.value=maxCharLimit=maxSMSVal>1? maxCharField.value-7 : maxCharField.value; 
}

function showCharCounter(charField, taField, cField, sField){
	var default_noChar=calculateDefaultNoChar(charField);
	populateMaxCharLimit(charField, cField);

	if ( taField.value.length > maxCharLimit ){
		taField.value = taField.value.substring(0, maxCharLimit);
		alert ('Message has exceeded the limit.');
	}
	
	cField.value = maxCharLimit - taField.value.length;
	
	
	sField.value = Math.ceil( (taField.value.length + (taField.value.length>default_noChar? 7:0))  / default_noChar );
}

function trimSelectName(selectObjName){
	for(i=0;document.getElementById(selectObjName+"["+i+"]")!=null;i++){
		document.getElementById(selectObjName+"["+i+"]").name=document.getElementById(selectObjName+"["+i+"]").name.split('[')[0];
	}
}