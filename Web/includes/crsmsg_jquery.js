$(document).ready(function(){
	greyOutReadOnlyFields();
	resetForm();
	integerKeyIn();
	numericKeyIn();
	integerAndPipeKeyIn();	
	smsNoAlert();
	hexOnlyKeyIn();
});

function smsNoAlert(){
	$(".checkNoSms").click(function(){
		if($(".smsContent").length && $(".smsContent").val().length > 153){
			return window.confirm($("#multiSmsAlertMsg").val());
		}
		return true;
	});
}

$.fn.clearForm = function() {
    return this.each(function() {
      var type = this.type, tag = this.tagName.toLowerCase();
//      console.log($(this).attr("name") + ":" + ($(this).attr("readonly")==true));
      if (tag == 'form')
        return $(':input',this).clearForm();
      if($(this) != null && $(this).attr("readonly") == true){
    	  ;
      }
      else if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'hidden')
        this.value = '';
      else if (type == 'checkbox' || type == 'radio')
        this.checked = false;
      else if (tag == 'select')
        this.selectedIndex = 0;
    });
  };


function greyOutReadOnlyFields(){
	$("input[readonly='true']").each(function(){
		$(this).css('background','lightgrey');
	});
	$("input[readonly='']").each(function(){
		$(this).css('background','lightgrey');
	});
	$("input[readonly='readonly']").each(function(){
		$(this).css('background','lightgrey');
	});
}

function resetForm(){

$("input[value='Reset']").click(function(){
	$(':input').clearForm()
});

}

function integerKeyIn()
{
	$(".limitInt").keydown(function(event){
		 if ( event.keyCode == 46 || event.keyCode == 8 || ($(this).is("textarea") && event.keyCode == 13) ) {
	            // let it happen, don't do anything
	      }
		  else {
	            // Ensure that it is a number and stop the keypress
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	});
}

function numericKeyIn()
{
	$(".limitNumeric").keydown(function(event){
		 if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190 || ($(this).is("textarea") && event.keyCode == 13) ) {
	            // let it happen, don't do anything
	      }
		  else {
	            // Ensure that it is a number and stop the keypress
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	});
}

function integerAndPipeKeyIn()
{
	$(".limitIntPipe").keydown(function(event){
		 if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 220) {
	            // let it happen, don't do anything
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	});
}

function hexOnlyKeyIn()
{
	$(".limitHex").keydown(function(event){
		//allow only hex character & backspace
		 if ( event.keyCode == 8 || event.keyCode == 37 ||  event.keyCode == 39 
				 || (event.keyCode>=48 && event.keyCode<=57) || (event.keyCode>=65 && event.keyCode<=70)) {
	            return;
	     }
        event.preventDefault(); 
	});
}


