// Current Server Time script (SSI or PHP)- By JavaScriptKit.com (http://www.javascriptkit.com)
// For this and over 400+ free scripts, visit JavaScript Kit- http://www.javascriptkit.com/
// This notice must stay intact for use.

//Depending on whether your page supports SSI (.shtml) or PHP (.php), UNCOMMENT the line below your page supports and COMMENT the one it does not:
//Default is that SSI method is uncommented, and PHP is commented:

var currenttime;
// = '<!--#config timefmt="%B %d, %Y %H:%M:%S"--><!--#echo var="DATE_LOCAL" -->' //SSI method of getting server date
//var currenttime = '<? print date("F d, Y H:i:s", time())?>' //PHP method of getting server date

///////////Stop editting here/////////////////////////////////

var montharray;
var serverdate;
var timerCounter=2;

function padlength(what){
	var output=(what.toString().length==1)? "0"+what : what
	return output
}

function displaytime(){
	serverdate.setSeconds(serverdate.getSeconds()+1);
	var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear();
	var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());
	
	document.getElementById("servertime").innerHTML=datestring+" "+timestring;
	
	// Extra timer
	for(i=1;i<timerCounter;i++){
		if(document.getElementById("servertime"+i)!=null)
			document.getElementById("servertime"+i).innerHTML=datestring+" "+timestring;
	}
}

function initTimer(sysDateTime){
	setCurrentTime(sysDateTime);
	montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	serverdate=new Date(currenttime);
	setInterval("displaytime()", 1000);
}

function setCurrentTime(serverTime){
	this.currenttime=serverTime;
}