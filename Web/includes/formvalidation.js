function validMobileNo(MobileNo) {
	// Check for correct phone number
    var error="";
    var duplicate="";
    var mobileNoArray=MobileNo.split("\r\n");
    var duplicateMobileControl=new Array();	
    phoneRe = /^\\d*$/;
	/* mobile no. pattern accepted
		601NNNNNNNN
		658NNNNNNNN
		659NNNNNNNN
	*/
    for(i=0;i<mobileNoArray.length;i++){
		if (mobileNoArray[i].length==0||isNan(normalizeMobile(mobileNoArray[i]))) {
		}else{
			if(error.length>0){
				error+=", ";
			}
			
			error+=mobileNoArray[i];
		}
		
		if(i==0){
			duplicateMobileControl[0]=mobileNoArray[i];
		}else{
			var j=0;
			while(j<duplicateMobileControl.length){
					if(duplicateMobileControl[j]==mobileNoArray[i]){
						if(duplicate.length>0){
							duplicate+=", ";
						}
						duplicate+=mobileNoArray[i];
						break;
					}else{
					}
					j++;
			}
			duplicateMobileControl[j]=mobileNoArray[i];
		}
		
	}
	if(error.length>0||duplicate.length>0){
		if(error.length>0)
			alert("Mobile number(s) not valid: "+error);
		if(duplicate.length>0)
			alert("Mobile number(s) duplicated: "+duplicate);
		return false;
	}else{
		return true;
	}
}

function normalizeMobile(mobileNo) {
	mobileNo = mobileNo.replace(/ /g, "").replace(/-/g, "").replace(/[+]/g, "").replace(/[(]/g, "").replace(/[)]/g, "");
	
//	if (mobileNo.length>0&&!mobileNo.startsWith("6")){
//		mobileNo="6"+mobileNo;
//	}

	return mobileNo;
}
	
function validIpAddress(ip) {
	// Check for ip address
    var error="";
    var ipArray=ip.split("\r\n");
   
    ipRe = /^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$/;
    for(i=0;i<ipArray.length;i++){
		if (ipArray[i].length==0||ipArray[i].match(ipRe)) {
		}else{
			if(i!=0){
				error+=", ";
			}
			
			error+=ipArray[i];
		}
	}

	if(error.length>0){
		alert("IP Address(s) not valid: "+error);
		return false;
	}else{
		return true;
	}
}

function validEmailAddress(){
	emailRe = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$/
	
}

function validMandatory(mandatoryField, name){
	if(mandatoryField.value.length==0){
		alert(name+" cannot be empty!");
		return false;
	}else{
		return true;
	}
}

String.prototype.startsWith = function(str) 
{return (this.match("^"+str)==str)}

String.prototype.endsWith = function(str) 
{return (this.match(str+"$")==str)}