$(document).ready(function(){
	alert('df');

	greyOutReadOnlyFields();
	resetForm();
});

$.fn.clearForm = function() {
    return this.each(function() {
      var type = this.type, tag = this.tagName.toLowerCase();
      if (tag == 'form')
        return $(':input',this).clearForm();
      if (type == 'text' || type == 'password' || tag == 'textarea')
        this.value = '';
      else if (type == 'checkbox' || type == 'radio')
        this.checked = false;
      else if (tag == 'select')
        this.selectedIndex = -1;
    });
  };


function greyOutReadOnlyFields(){
	$("input[readonly='true']").each(function(){
		$(this).css('background','lightgrey');
	});
	$("input[readonly='']").each(function(){
		$(this).css('background','lightgrey');
	});
	$("input[readonly='readonly']").each(function(){
		$(this).css('background','lightgrey');
	});
}

function resetForm(){

$('input[type=reset]').click(function(){
	$('form').clearForm();
});

});




