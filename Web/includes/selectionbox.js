var sourceSelect, destSelect, strDestSelect, selectAll;

function selectionBoxInit(sourceSelect, destSelect, strDestSelect, selectAll){
	this.sourceSelect=sourceSelect;
	this.destSelect=destSelect;
	this.strDestSelect=strDestSelect;
	this.selectAll=selectAll;
}

function DoAdd(){
	if(selectAll!=null){
		selectAll.value='false';
	}

	if(sourceSelect.selectedIndex>=0){
		AddItem(destSelect, sourceSelect.options[sourceSelect.selectedIndex].text, sourceSelect.options[sourceSelect.selectedIndex].value);
	}else{
		alert("You did not select any record!");
	}
}

function DoAddAll(){
	if(selectAll!=null){
		selectAll.value='true';
	}
	
	for(i=0;i<sourceSelect.options.length;i++){
		AddItem(destSelect, sourceSelect.options[i].text, sourceSelect.options[i].value);
	}
}

function AddItem(objListBox, strText, strValue){
	var path1 = destSelect;
	var found;
  	var newOpt;
	newOpt = document.createElement("OPTION");
	newOpt = new Option(strText,strValue);
											
    for (var i = 0; i < path1.options.length; i++){
  	    if( path1.options[i].value == strValue){
  		    found = '1';
  	    }
    }

    if (found != '1'){
  	    objListBox.add(newOpt);
    }else if(selectAll.value=='false'){
    	alert('This record has been selected.');
    }
}

function RemoveItem(){
	var path1 = destSelect;

	if(path1.selectedIndex>=0){
		path1.options[path1.selectedIndex]=null;
	}else{
		alert("You did not select any record!");
	}
	
	if(selectAll!=null){
		selectAll.value='false';
	}
}

function RemoveAllItem(){ alert('RemoveAllItem function');
	destSelect.options.length=0;
	
	if(selectAll!=null){
		selectAll.value='false';
	}
}

function generateRecordsList(){
	var path2 = destSelect;
	var te='';

	for (var i = 0; i < path2.options.length; i++){
  	   te += path2.options[i].value;
  	   if(i+1<path2.options.length){
  	   		te += ",";
  	   }
    }
    strDestSelect.value=te;
}