<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SMS</title>

</head>
<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Change Password&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors/>
</td>
</tr>								
<tr>
<td class="error_td" >
<html:messages id="msg"/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
		
	
<!-- Edit User Dialog -->
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/common/SaveNewPasswordMD.do">
<tr valign="top" >
<td class=form_non_input width=20%>User Id&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="userId" styleClass="form_input" size="15" maxlength="15"/></td>
</tr>	
<tr valign="top" >
<td class=form_non_input width=20%>Old Password&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:password property="oldpassword" styleClass="form_input" size="15" maxlength="15" /></td>
</tr>	
<tr class=form_non_input>		
<td>New Password&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td><html:password property="newpassword1" styleClass="form_input" size="15" maxlength="15" /></td>
</tr>
<tr class=form_non_input>		
<td>Confirm New Password&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td><html:password property="newpassword2" styleClass="form_input" size="15" maxlength="15" /></td>
</tr>


<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" name="actiontype" value="Submit" class="form_button">
&nbsp;&nbsp;
<input type="reset" value="Reset" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			

</body>
</html:html>