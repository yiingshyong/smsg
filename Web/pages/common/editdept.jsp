<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SMS</title>


<script type="text/javascript">
var err_ind ="<html:errors/>";
function buttonCheck()
{
	// Should i check for change also ??
	if (err_ind.indexOf("successfully")  != -1)
	{
	document.DeptFormAdd.actiontype.disabled = true;

	}
	else if (err_ind.indexOf("not allowed")  != -1)
	{
	document.DeptFormAdd.actiontype.disabled = true;
	
	}
	else
	{
	document.DeptFormAdd.actiontype.disabled = false;

	};
}
</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 >

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Edit Dept&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
		
	
<!-- Edit Dept Dialog -->
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/common/SaveEditDept.do">
<html:hidden property="action" value="EDIT"/>
<html:hidden property="deptid"/>

<tr valign="top" >
<td class=form_non_input width=20%>Dept Name&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="deptname" size="50" maxlength="50" styleClass="form_input" /></td>
</tr>		
<tr class=form_non_input>		
<td> Dept Description</td>
<td>:</td>
<td><html:text property="deptdescription" styleClass="form_input" size="100" maxlength="100"/></td>
</tr>
<!--
<tr class=form_non_input>		
<td>Dept Leader&nbsp;<span class="remarks">*</span></td>
<td>:</td>

<td><html:text property="selected_deptleader" styleClass="form_input" size="100" maxlength="100"/></td>

</tr>
-->


<tr class=form_non_input>		
<td>Dept Status</td>
<td>:</td>
<td>
<html:select property="selected_deptstatus" styleClass="form_input">
<html:options property="deptstatus_value" labelProperty="deptstatus_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	
<%--
<tr class=form_non_input>		
<td>Dept Sms Type</td>
<td>:</td>
<td>
<html:select property="selectedSmsType" styleClass="form_input">
<html:optionsCollection property="smsTypes" label="label" value="tblRefId" styleClass="form_input" /> 
</html:select>
</td>
</tr>	
 --%>


<tr class=form_non_input>		
<td>Created By</td>
<td>:</td>
<td><html:text readonly="true" property="createdBy" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Created</td>
<td>:</td>
<td><html:text readonly="true" property="createdDate"  styleClass="form_input" size="12" maxlength="12"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Created</td>
<td>:</td>
<td><html:text readonly="true" property="createdTime"  styleClass="form_input" size="10" maxlength="10"/></td>
</tr>
<tr class=form_non_input>		
<td>Modified By</td>
<td>:</td>
<td><html:text readonly="true" property="modifiedBy" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Modified</td>
<td>:</td>
<td><html:text readonly="true" property="modifiedDate"  styleClass="form_input" size="12" maxlength="12"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Modified</td>
<td>:</td>
<td><html:text readonly="true" property="modifiedTime" styleClass="form_input" size="10" maxlength="10"/></td>
</tr>


<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" value="Submit" class="form_button">
&nbsp;&nbsp;
<input type="reset" value="Reset" class="form_button">
&nbsp;&nbsp;
<input type=button value="Close Window" onClick="javascript:window.close();" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit Dept Dialog End -->
			

</body>
</html:html>