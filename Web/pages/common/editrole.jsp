<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ page import="com.bcb.common.modal.Apps" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>

<html:html>

<head>
<title>Welcome to CRMS</title>
<SCRIPT LANGUAGE="JavaScript" SRC="../includes/tab.js"></SCRIPT>
<SCRIPT TYPE="text/javascript">
<!--
var permList = new Array(1);
permList [0] = new Array(1);

var appList = new Array(1);
appList [0] = new Array(1);
// Build another array of its permission
<logic:present name="permsofrole">

<% int countjs1 = 0;%> 

<% countjs1 = 0;%> 

<logic:iterate id="permsofrole2" name="permsofrole" scope="request">

permList[<%=countjs1%>]= "<bean:write name="permsofrole2"/>"; 
<% countjs1++ ;%> 
permList.concat (new Array(1));
</logic:iterate>
</logic:present>

<logic:present name="appsofrole">
<% int countjs2 = 0;%> 

<% countjs2 = 0;%> 

<logic:iterate id="appsofrole2" name="appsofrole" scope="request">


appList[<%=countjs2%>]= "<bean:write name="appsofrole2" property="appsId"/>"; 
<% countjs2++ ;%> 
appList.concat (new Array(1));
</logic:iterate>
</logic:present>
var err_ind ="<html:errors/>";
function buttonCheck()
{
	// Should i check for change also ??
	if (err_ind.indexOf("successfully")  != -1)
	{
	document.RoleForm.actiontype.disabled = true;

	}
	else if (err_ind.indexOf("not allowed")  != -1)
	{
	document.RoleForm.actiontype.disabled = true;
	
	}
	else
	{
	document.RoleForm.actiontype.disabled = false;

	};
}

function check_perm_box (field)
{
for (i = 0; i < field.length; i++) 
{
field[i].checked = false;
for ( j = 0; j < permList.length ; j ++)
{
if ( permList[j] == field[i].value)
{
field[i].checked = true;
};

}

}
}
function check_app_box (field)
{
for (i = 0; i < field.length; i++) 
{
field[i].checked = false;
for ( j = 0; j < appList.length ; j ++)
{

if ( appList[j] == field[i].value)
{
field[i].checked = true;
};

}

}
}
function reset_add_form()
{
//document.RoleFormAdd.rolename.value = "";
//document.RoleFormAdd.roledescription.value = ""; 


}

function check(obj){
   var allow = '';
   for(i=0;i<document.RoleForm.elements.length;i++){
   		if(document.RoleForm.elements[i].type=='checkbox'){
   			
   			if(obj.checked == true){
   				if(document.RoleForm.elements[i].name=='apps'){
   					if(document.RoleForm.elements[i].value==obj.value 
   						&& obj.checked == true){
						allow = 'true';
					}else{
						allow = 'false';
					}
				}
				if(document.RoleForm.elements[i].name == 'permroles'){
					if(allow=='true'){
						document.RoleForm.elements[i].checked = true;
					}
				}
   			}else if(obj.checked == false){
   				if(document.RoleForm.elements[i].name=='apps'){
   					if(document.RoleForm.elements[i].value==obj.value){
						allow = 'false';
					}else{
						allow = 'true';
					}
				}
				if(document.RoleForm.elements[i].name == 'permroles'){
					if(allow=='false'){
						document.RoleForm.elements[i].checked = false;
					}  
				}   				
   				
   			}
   		}
   }
}

//-->
</SCRIPT>	
</head>
<link rel="stylesheet" href="../includes/crms.css">
<link rel="stylesheet" href="../includes/tab.css">

<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 onload="check_perm_box(document.RoleForm.permroles);check_app_box(document.RoleForm.userapps);buttonCheck();">

<div id='anc'>
</div>
<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Edit Role&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors/>
</td>
</tr>
</table>
<!-- Header table End -->	
	
				
		
	<br>
		
<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td></td><td class="form_non_input">			

<!-- Start Detail Table -->
<html:form action="/common/SaveEditRole.do">
<html:hidden property="roleid"/>

<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=600>


<tr valign="top">
<td class="form_non_input">			
<div class="tab-pane" id="tabPane1">


<div class="tab-page" id="tabPage1">
<h2 class="tab">General</h2>
<table>	


	<tr valign="top" >
	<td class=form_non_input width=20%>Role Name&nbsp;<span class="remarks">*</span></td>
					<td width=1%>:</td>
			<td><html:text property="rolename" size="50" maxlength="50" styleClass="form_input" />
</td>
			</tr>	
		<tr class=form_non_input>		
			<td>Role Description</td>
			<td>:</td>
<td><html:text property="roledescription" styleClass="form_input" size="100" maxlength="100"/></td>
		</tr>
		
<tr class=form_non_input>		
<td>Status</td>
<td>:</td>
<td>
<html:select property="selected_userstatus" styleClass="form_input">
<html:options property="userstatus_value" labelProperty="userstatus_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	

</table>
</div>


<div class="tab-page" id="tabPage2">
<h2 class="tab">Authorization</h2>
<table>	
<tr class=form_non_input>		
			<td valign=top>Permission</td>
			<td valign=top>:</td>
			<td></td>
			</tr>
<logic:present name="perm">
<logic:iterate id="appsrole" name="perm">
		<tr class=form_non_input>		
			<td valign=top></td>
			<td valign=top></td>
			<td class=index_row8>
			<input type="checkbox" name="apps" value="<bean:write name='appsrole' property='appsName'/>" onClick="check(this);"/>
			<bean:write name='appsrole' property='appsName'/>
			
			
			</td>
			</tr>
		<tr class=form_non_input>		
			<td valign=top></td>
			<td valign=top></td>
			<td>
<logic:iterate name="appsrole" id="permRoles" property="permList">

<input type="checkbox" name="permroles" value="<bean:write name='permRoles' property='permId'/>"><bean:write name='permRoles' property='permName'/>
</logic:iterate>
</td>
		</tr>
</logic:iterate>
</logic:present>

<tr class=form_non_input>		
<td valign=top>Application assigned</td>
<td valign=top>:</td>
<td></td>
</tr>
<tr class=form_non_input>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>
			<% 
				ArrayList compList = (ArrayList) request.getAttribute("apps");
				
				Iterator compIter = compList.iterator();
			    
			    int count = 1;
			    int listSize = compList.size();
			    
				Apps model;
					while (compIter.hasNext()){
						model = new Apps();
						model = (Apps) compIter.next();
						int modTest = 0;
						modTest = count % 3;
						
						if(count==1){
						%><table border=0><%
						}
									
						if(modTest == 1){
							if(listSize == count){
							%>
								<tr class="form_non_input">

								<td >
								<input type="checkbox" name="userapps" value="<%=model.getAppsId()%>"/>
								</td>
								<td >
								<%=model.getAppsName()%>
								</td>
								</tr>
							<%
							}else{
							%>
								<tr class="form_non_input">
								<td>
								<input type="checkbox" name="userapps" value="<%=model.getAppsId()%>"/>
								</td>
								<td >
								<%=model.getAppsName()%>
								</td>
							<%
							}
						}else if(modTest == 2){
							if(listSize == count){
							%>
								<td >
								<input type="checkbox" name="userapps" value="<%=model.getAppsId()%>"/>
								</td>
								<td >
								<%=model.getAppsName()%>
								</td>
								</tr>
							<%
							}else{
							%>
								<td>
								<input type="checkbox" name="userapps" value="<%=model.getAppsId()%>"/>
								</td>
								<td >
								<%=model.getAppsName()%>
								</td>
							<%
							}
						}else{
						   %>
						   		<td>
								<input type="checkbox" name="userapps" value="<%=model.getAppsId()%>"/>
								</td>
								<td >
								<%=model.getAppsName()%>
								</td>
								</tr>
							<%
						}						
						if(count==listSize){
						%></table><%
						}
						count++;
				}
			
			%>
</td>
</tr>
</table>
</div>


<div class="tab-page" id="tabPage3">
<h2 class="tab">Info</h2>
<table>	
<tr class=form_non_input>		
<td>Created By</td>
<td>:</td>
<td><html:text property="createdby" styleClass="form_input" size="25" maxlength="25" readonly="true"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Created</td>
<td>:</td>
<td><html:text property="createddate" styleClass="form_input" size="12" maxlength="12" readonly="true"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Created</td>
<td>:</td>
<td><html:text property="createdtime" styleClass="form_input" size="10" maxlength="10" readonly="true"/></td>
</tr>
<tr class=form_non_input>		
<td>Modified By</td>
<td>:</td>
<td><html:text property="modifiedby" styleClass="form_input" size="25" maxlength="25" readonly="true"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Modified</td>
<td>:</td>
<td><html:text property="modifieddate" styleClass="form_input" size="12" maxlength="12" readonly="true"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Modified</td>
<td>:</td>
<td><html:text property="modifiedtime"  styleClass="form_input" size="10" maxlength="10" readonly="true"/></td>
</tr>

</table>
</div>

</div>

<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" name="actiontype" value="Submit" class="form_button">
&nbsp;&nbsp;
<input type="reset" value="Reset" class="form_button">
&nbsp;&nbsp;
<input type=button value="Close Window" onClick="javascript:window.close();" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit Group Dialog End -->
			</body>
</html:html>