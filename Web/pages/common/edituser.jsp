<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SMS</title>
<SCRIPT LANGUAGE="JavaScript" SRC="../includes/selectionbox.js"></SCRIPT>
<script type="text/javascript">
function loadSelectionBox(){
	selectionBoxInit(document.getElementById("selected_role"), document.getElementById("selected_roles"), document.getElementById("strSelected_roles"), document.getElementById("selectedAllRoles"));
}
</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 onload="loadSelectionBox();">

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Edit User&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
		
	
<!-- Edit User Dialog -->
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/common/SaveEditUser.do">
<tr valign="top" >
<td class=form_non_input width=20%>User ID&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="userid" styleClass="form_input" size="15" maxlength="15" readonly="true"/></td>
</tr>	
<tr class=form_non_input>		
<td>User Name&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td><html:text property="username" styleClass="form_input" size="50" maxlength="50"/></td>
</tr>
<tr class=form_non_input>		
<td>Staff ID&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td><html:text property="staffid" styleClass="form_input" size="15" maxlength="15"/></td>
</tr>
<tr class=form_non_input>		
<td>Dept&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td>
<html:select property="selected_group" styleClass="form_input">
<option value="0">Please select.</option>
<html:options property="group_value" labelProperty="group_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	
<!--
<tr class=form_non_input>		
<td>Branch/Dept&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td>
<html:select property="selected_branch" styleClass="form_input">
<option value="0">Please select.</option>
<html:options property="branch_value" labelProperty="branch_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	
-->
<html:hidden property="strSelected_roles" />
<tr>
			<td>User Role&nbsp;<span class="remarks">*</span></td> 
			<td>:</td>
			<td>
			<html:select property="selected_role" styleClass="form_input" styleId="TxtContent" size="8">
				<html:options property="role_value" labelProperty="role_label" styleClass="form_input"/> 
			</html:select>
			</td><td align=center width=5%>
			<INPUT type="button" class="form_button" value=" >> " onclick="DoAdd()" ID="Button1" NAME="Button1">
			<A href="#" onclick="DoAddAll()">All</A>
			<br><br>
			<INPUT type="button" class="form_button" value=" << " onclick="RemoveItem()" ID="Button2" NAME="Button2" >
			<A href="#" onclick="RemoveAllItem()">All</A>
			<td valign="top">
			<html:hidden property="selectedAllRoles" />
			<html:select styleId="Select1" size="8" styleClass="form_input" property="selected_roles" style="HEIGHT: 102px">
				<html:options property="selected_role_value" labelProperty="selected_role_label" styleClass="form_input"/> 
			</html:select>
			</td>
		</tr>
<!--		
<tr class=form_non_input>		
<td>User Role&nbsp;<span class="remarks">*</span></td>
<td>:</td>
<td>
<html:select property="selected_role" styleClass="form_input">
<option value="0">Please select.</option>
<html:options property="role_value" labelProperty="role_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	
-->
<tr class=form_non_input>		
<td>User Status</td>
<td>:</td>
<td>
<html:select property="selected_userstatus" styleClass="form_input">
<option value="0">Please select.</option>
<html:options property="userstatus_value" labelProperty="userstatus_label" styleClass="form_input"/> 
</html:select>
</td>
</tr>	
<tr class=form_non_input>		
<td>No of Failed Logon</td>
<td>:</td>
<td>
<html:text property="failedlogon" styleClass="form_input" size="5" maxlength="5"/>
</td>
</tr>	
<tr class=form_non_input>		
<td>Email Id&nbsp;</td>
<td>:</td>
<td><html:text property="emailid" styleClass="form_input" size="50" maxlength="50"/></td>
</tr>
<tr class=form_non_input>		
<td>Business Phone&nbsp;</td>
<td>:</td>
<td><html:text property="bizphone" styleClass="form_input" size="15" maxlength="15"/></td>
</tr>
<tr class=form_non_input>		
<td>Office Extention&nbsp;</td>
<td>:</td>
<td><html:text property="ext" styleClass="form_input" size="5" maxlength="4"/></td>
</tr>
<tr class=form_non_input>		
<td>Hand Phone&nbsp;</td>
<td>:</td>
<td><html:text property="hphone" styleClass="form_input" size="15" maxlength="15"/></td>
</tr>
<tr class=form_non_input>		
<td>Password</td>
<td>:</td>
<td><html:password property="password" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>	
<tr class=form_non_input>		
<td>Last Password Change</td>
<td>:</td>
<td><html:text readonly="true" property="lastpwdchg" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>
<tr class=form_non_input>		
<td>Created By</td>
<td>:</td>
<td><html:text readonly="true" property="createdby" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Created</td>
<td>:</td>
<td><html:text readonly="true" property="createddate"  styleClass="form_input" size="12" maxlength="12"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Created</td>
<td>:</td>
<td><html:text readonly="true" property="createdtime"  styleClass="form_input" size="10" maxlength="10"/></td>
</tr>
<tr class=form_non_input>		
<td>Modified By</td>
<td>:</td>
<td><html:text readonly="true" property="modifiedby" styleClass="form_input" size="25" maxlength="25"/></td>
</tr>
<tr class=form_non_input>		
<td>Date Modified</td>
<td>:</td>
<td><html:text readonly="true" property="modifieddate"  styleClass="form_input" size="12" maxlength="12"/></td>
</tr>
<tr class=form_non_input>		
<td>Time Modified</td>
<td>:</td>
<td><html:text readonly="true" property="modifiedtime" styleClass="form_input" size="10" maxlength="10"/></td>
</tr>
<tr class=form_non_input>		
<td>Last Login Date</td>
<td>:</td>
<td><html:text readonly="true" property="lastLoginDate"  styleClass="form_input" size="12" maxlength="12"/></td>
</tr>

<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" value="Submit" class="form_button" onclick="generateRecordsList();">
&nbsp;&nbsp;
<input type="reset" value="Reset" class="form_button">
&nbsp;&nbsp;
<input type=button value="Close Window" onClick="javascript:window.close();" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			

</body>
</html:html>