<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SGW</title>
</head>
<!---------------------POPUP MENU-------------------------->
<link rel="stylesheet" href="../includes/crms.css">
<logic:present name="eSalesSession">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 onload="window.open('../eSales/mgmtCampaign.do','content');">
</logic:present>
<logic:present name="eSalesActSession">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 onload="window.open('../eSales/activityMgmt.do','content');">
</logic:present>
<logic:notPresent name="eSalesSession">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>
</logic:notPresent>
<table cellspacing=0 cellpadding=0 height="60%" width="100%" border=0 >
	<tr align=center valign=center>   
    <td valign=center>
      <table border=0 cellpadding=0 cellspacing=2 >		
	  <tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
				<tr>
				<td background="../images/b_leftline.gif"></td>
				<td><img src="../images/logon.JPG"></td>
				<td background="../images/subtitle2.gif">
					<table>
					<tr>
					<td colspan=2>
					Welcome <bean:write name="logoncredential" property="userName"/> !!
					</td>
					</tr>
					<tr>
					<td class=headers>
					<logic:present name="slogon">
					Your last successful logon is at </td>
					<td class=form_non_input><bean:write name="slogon" property="logonDate"/>&nbsp;-&nbsp;<bean:write name="slogon" property="logonTime"/> from <bean:write name="slogon" property="fromIp"/>
					</logic:present>
					</td>
					</tr>
					<tr>
					<td class=headers>
					<logic:present name="flogon">
					Your last unsuccessful logon is at </td><td class=form_non_input><bean:write name="flogon" property="logonDate"/>&nbsp;-&nbsp;<bean:write name="flogon" property="logonTime"/> from <bean:write name="flogon" property="fromIp"/>
					</logic:present>
					</td>
					</tr>
					</table>
				</td>
				<td background="../images/b_right.gif"></td>
				</tr>
				<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
			</table>	
		</td>
	</tr>						
</table>
<logic:present name="userquota">
<table class="summary_tbl" cellSpacing=0 cellPadding=1 border=0 align="right" style="border-collapse:collapse;">
<tr class="summary_title"><td align="center" colspan="2">Department Quota Information</td></tr>
<tr>
	<td>
		<table width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="right" style="border-collapse:collapse;">
		<tr class=index_row14>
			<td align=center>SMS Quota Remaining</td>
			<td align=center>Total SMS Quota Assigned</td>
		</tr>
		<tr class=index_row8>
			<td align=center><bean:write name="userquota" property="quota"/></td>
			<td align=center><bean:write name="userquota" property="totalQuota"/>
			<input id="token" name="token" type="hidden" value="<%= session.getAttribute("csrfToken") %>" />
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</logic:present>

</body>
</html:html>