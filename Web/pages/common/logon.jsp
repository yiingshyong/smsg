<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ page session="true" %>
<html>
<head>
<script>
function check_frame() {
	//window.frames["menu"].document.getElementById ("menu_tree");
     if( top != self ) { //in frame
    	 window.top.location.href = document.URL ;
     }
}
</script>
<title>SMS Gateway (SMSG)</title>
</head>
<link rel="stylesheet" href="../includes/crms.css" type="text/css">

<body onload="check_frame()" link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 >
<table border="0" cellpadding="0" cellspacing="0" align="top" width="100%">
	<tr align="top">
		<td ><img name="cms_logo2" src="../images/crms_logo2.JPG" border="0" width="100%"></td>
	</tr>
</table>
<table height=75% border=0 cellpadding="0" cellspacing="40" align="top" width="100%">
<html:form action="/common/Logon.do" focus="logonuserid">

	<tr>
		<td>
		<table width=100% height=100% border=1 bordercolor=#00ccFF cellpadding=0 cellspacing=0 align=center>				
		<tr>
			<td>
				<table border=0 cellpadding=0 cellspacing=0 width=100% height=100% align=center>
				
				<tr>
					<td  width=150 height=30></td>
					<td class="error_td"><html:errors/></td>
				</tr>
				
	            <tr>
					<td></td>
					<td class="login_font_annon">For those with valid User ID, please login below.
                    </td>
				</tr>
				
				<tr>
					<td></td>
					<td class="login_font">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<table border=0 height=70%>
						<tr>
							<td width=15>&nbsp;</td> 
							<td class="login_first_col">&nbsp;&nbsp;&nbsp;&nbsp;User ID</td> 
							<td class="login_sec_col"><html:text property="logonuserid" maxlength="15" size="30" value="" styleClass="login_input" />
							<input id="token" name="token" type="hidden" value="<%= session.getAttribute("csrfToken") %>" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td> 
							<td class="login_first_col">&nbsp;&nbsp;&nbsp;&nbsp;Password</td> 
							<td class="login_sec_col"><html:password property="logonpassword" maxlength="15" size="30" value="" styleClass="login_input"/></td>
						</tr>
						<tr>
							<td>&nbsp;</td> 
							<td></td> 
							<td><html:submit styleClass="form_button">Logon</html:submit>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<html:reset styleClass="form_button">Reset</html:reset>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				
                
				<tr>
					<td></td>
					<td align=right class="login_font_gray">All rights reserved. Copyright 2006 CIMB Bank Berhad&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
			<script>
 for (i=0; i<document.forms.length; i++) {
            document.forms[i].setAttribute("AutoComplete","off");
    }
</script>

				</html:form>
				</table>		
			</td>
		</tr>
		</table>	
		</td>
	</tr>
</table>		
</body>
</html>	
	