<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

<head>
	<link rel="StyleSheet" href="../../includes/dtree.css" type="text/css" />
	<script type="text/javascript" src="../../includes/dtree.js"></script>
</head>

<body>
<table width=100% height=100%>
<tr>
<td>
<div id="menu_tree" class="dtree">
	<script type="text/javascript">
		<!--
		d = new dTree('d');
		d.add(0,-1,'<bean:write name="logoncredential" property="userName"/>');

		<logic:present name="menu">
	<logic:iterate id="menu2" name="menu">
	if ( "<bean:write name="menu2" property="url"/>".length < 5)
	{
	d.add(<bean:write name="menu2" property="menuId"/>,<bean:write name="menu2" property="parentId"/>,"<bean:write name="menu2" property="menuName"/>", "<bean:write name="menu2" property="url"/>", "<bean:write name="menu2" property="menuDescription"/>","");
	}
	else
	{
	d.add(<bean:write name="menu2" property="menuId"/>,<bean:write name="menu2" property="parentId"/>,"<bean:write name="menu2" property="menuName"/>", "<bean:write name="menu2" property="url"/>", "<bean:write name="menu2" property="menuDescription"/>","content");
	}
	</logic:iterate>
	</logic:present>
	d.add(998,0, "Chg Password","../../common/ChangePassword.do","Change Password","content");
	d.add(999,0, "Logout","../../common/Logoff.do","Logoff","_top");
		document.write(d);
		//-->
	</script>
</div>
</td>
</tr>
</table>
</body>

</html>