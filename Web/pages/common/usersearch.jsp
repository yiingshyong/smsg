<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SMS</title>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,left = 0,top = 0');");
}
// End -->
</script>	
</head>

<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 >

			<table cellspacing=0 cellpadding=0 width="772" border=0>
			<tr>   
    			<td valign=top>
      				<table border=0 cellpadding=0 cellspacing=2>				
					<tr>
					<td><img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;User  Mgmt&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
			</td>
			</tr>
<tr>
<td class="error_td" >
<html:errors/>
</td>
</tr>								

			</table>	
<html:form  method="post" action="/common/GetUserSearch.do" focus="searchkey">
<table align=left class="summary_tbl" cellSpacing=2 cellPadding=2  border=1 align="center">
<tr class="summary_title">															
<td colspan=2>Search By</td>															
<td>
<html:text property="searchkey" styleClass="form_input"/> 
<html:select property="selectedState" styleClass="form_input">
<html:options property="searchby_value" labelProperty="searchby_label" styleClass="form_input"/> 
</html:select>
</td>
<td>
Application
<html:select property="selectedApps" styleClass="form_input">
<html:options property="byApps_value" labelProperty="byApps_label" styleClass="form_input"/> 
</html:select>
</td>
<td>
Status
<html:select property="selectedStatus" styleClass="form_input">
<html:options property="byStatus_value" labelProperty="byStatus_label" styleClass="form_input"/> 
</html:select>
</td>
<td>
<input type="submit" value="Submit" class="form_input">
</td>
</tr>
</table>
</html:form>


<br>
<br>
<table class="form_non_input" cellSpacing=0 cellPadding=2 width=100%  border=1 align="center">
<tr class="summary_title">															
<td colspan=2>Selected Users</td>															
</tr>
</table>



<!-- Detail Table -->
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center>User Id</td>
<td align=center>User Name</td>
<td align=center>Branch/Dept</td>
<td align=center>Office Ext</td>
<td align=center>User Status</td>
<td align=center>Last Login Date</td>
</tr>

<logic:present name="user">
<logic:iterate id="user2" name="user">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<a href="#" onclick="javascript:popUp('../common/EditUser.do?userid=<bean:write name="user2" property="userId"/>');return false;"><td align=left><bean:write name="user2" property="userId"/> </td>
<td align=left><bean:write name="user2" property="userName"/></td>
<td align=left><bean:write name="user2" property="branchCode"/></td>
<td align=left><bean:write name="user2" property="ext"/></td>
<td align=left><bean:write name="user2" property="userStatus"/></td>
<td align=left><bean:write name="user2" property="lastLoginDate"/></td>
</a>
</tr>
</logic:iterate>
</logic:present>
</table>																
<!-- End Detail Table -->
<table width=100%>
<tr >
<td align=center>
<input type="button" class="form_button" value="New User" onClick="javascript:popUp('../common/AddUser.do')">		</td>
</tr>
</table>
</body>
</html:html>
