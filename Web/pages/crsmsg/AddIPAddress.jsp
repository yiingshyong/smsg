<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Add New IP Address</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	
	
	<body>
		<br>	
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr align="right">
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
					<tr>
						<td>
							<table border="0" cellpadding="5" cellspacing="1" width="100%">
								<tr>
									<td class="info" colspan="1" align="center"><b>Add New IP Address</b></td>
								</tr>
								
								<tr>
									<td class="back">
										<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
											<tr>
												<td class="back">
													<html:form action="/crsmsg/SaveAddIP.do">
														<TABLE border=0 bgcolor="#666666" align="center">
										<TR>
											<TD class="info" align=middle colSpan=2>
												&nbsp;
												<BR>
												<B><font color="white">New IP Address</font></B>
												<BR>
												&nbsp;
												<BR>
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="50%">IP Address: </TD>
											<TD class="back" align=left width="50%"><html:text property="ip" size="15" maxlength="100" /></TD>
										</TR>
										
																				
										
										<TR bgcolor="#98AFC7">
										    <TD class="hf" vAlign=top align=right width="50%">Department: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:text property="dept" size="50" maxlength="1000" /><br>
										    </TD>
										</TR>
										
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right width="50%">Remarks: </TD>
											<TD class="back" align=left width="50%">
												<html:textarea property="remarks" rows="5" cols="30" /> 
											</TD>
										</TR>
										
										<TR bgcolor="#98AFC7">
										    <TD class="hf" vAlign=top align=right width="50%">System: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:text property="system" size="30" maxlength="200" />
										    </TD>
										</TR>		
										
										
										<%-- <TR bgcolor="#98AFC7">
										    <TD class="hf" align=right width="50%">Status: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:select property="selected_status">
													<html:options property="status_value" labelProperty="status_label" />
										    	</html:select> 
										    </TD>
										</TR> --%>
										
										
										<TR bgcolor="#98AFC7">
											<TD class="back" align=middle colSpan=2>
												<html:submit value="Submit" property="submitBtn" /> 
												<html:reset value="Reset" property="resetBtn" />
											</TD>
										</TR>
										</TABLE>
													</html:form>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>