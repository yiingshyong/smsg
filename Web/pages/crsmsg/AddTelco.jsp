<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Add New Telco</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>		
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body><br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  <tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>

 <input type="hidden" name="hiddenSel" value="">
  <tbody>
    <tr>
      <td>
        <table class="summary_tbl" align="right" border="0" cellpadding="2" cellspacing="2">
          <tbody>
          
            <tr class="summary_title">
            <td class="form_non_input"></td>
					<td> 
						</td>
			  </td>
			  <td class="form_non_input"></td>
				<td> 
					</td>
			  </td>
              <td>
              
			  </td>
            </tr>
          </tbody>
        </table>

      </td>    
    </tr>
    
    
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		 		

		
		  <tr>
		    <td class="back" align="center" >
		 

		    </td>
		  </tr>
		</table>
	

<br>
   
    <tr>
  
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>Add New API</b></td>
                  </tr>
                  <tr>
                    <td class="back">
                       
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                       
                          <tr>
                            <td>
                            </form>
                            <html:form action="/crsmsg/SaveAddTelco.do">
                              <table border="0" cellpadding="8" cellspacing="1" align="center">
                                <tbody>
                           

                                   <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Service Provider :</h4></td>
								    <td class="back" align="left">
								    <html:select property="selectedSvcProvider" styleClass="form_input">
											<html:optionsCollection  styleClass="form_input" property="serviceProvider" label="name" value="value"/>
									</html:select> 
								    </td>
					               </tr>
					               
                                   <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>API Name :</h4></td>
								    <td class="back" align="left"><html:text property="telco_name"/></td>
					               </tr>
<%--
					               <tr bgcolor="#929CF4">
								    <td class="hf" align="right" width="50%"><h4>Mobile Prefix Routing:</h4><b>Note:</b>Separate each prefix with a new line.</td>
								    <td class="back" align="left"><html:textarea property="mobile_prefix" cols="15" rows="10"/></td>
								    </tr>
								     --%>
								    <%--
								    <tr bgcolor="#929CF4">
								    <td class="hf" align="right" width="50%"><h4>Home Prefix:</h4><b>Note:</b>Separate each prefix with a new line.</td>
								    <td class="back" align="left"><html:textarea property="home_prefix" cols="15" rows="5"/></td>
								    </tr>
								     --%> 
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>HTTP URL :</h4></td>
								    <td class="back" align="left"><html:text property="url" size="50"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>URL PARAMETER :</h4></td>
								    <td class="back" align="left"><html:text property="urlParam" size="255"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Proxy Address :</h4></td>
								    <td class="back" align="left"><html:text property="proxyAddr" size="100"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Proxy Port :</h4></td>
								    <td class="back" align="left"><html:text property="proxyPort" size="5" styleClass="limitInt"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Implementation Class :</h4></td>
								    <td class="back" align="left"><html:text property="className" size="100"/></td>
					               </tr>
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Short Code :</h4></td>
								    <td class="back" align="left"><html:text property="shortCode" size="255"/></td>
					               </tr>	
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Success Code :</h4></td>
								    <td class="back" align="left"><html:text property="successCode" size="255"/></td>
					               </tr>	
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Retry Code :</h4></td>
								    <td class="back" align="left"><html:text property="retryCode" size="255"/></td>
					               </tr>	
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>SMS on Hand Limit :</h4></td>
								    <td class="back" align="left"><html:text property="urlParam" size="10"/></td>
					               </tr>		               
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>User Name :</h4></td>
								    <td class="back" align="left"><html:text property="username"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Password :</h4></td>
								    <td class="back" align="left"><html:password property="password"/></td>
					               </tr>
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Encryption :</h4></td>
								    <td class="back" align="left">
								     <html:select property="encryption">
						   			 <html:options property="encryption_value" labelProperty="encryption_label"/> 			
					     			 </html:select>
								    </td>
					               </tr>
					              
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Within Operator Charge:</h4></td>
								    <td class="back" align="left"><html:text property="within_cost" size="4"/>&nbsp;cents per SMS</td>
								    </tr>
								  
								    <tr bgcolor="#929CF4">
								    <td class="hf" align="right" width="50%"><h4>Inter Operator Charge:</h4></td>
								    <td class="back" align="left"><html:text property="inter_cost" size="4"/>&nbsp;cents per SMS</td>
								    </tr>
								  
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Incoming SMS Charge:</h4></td>
								    <td class="back" align="left"><html:text property="incoming_cost" size="4"/>&nbsp;cents per SMS</td>
								    </tr>
								      <tr bgcolor="#707DF4">
								    <td class="hf" align="right" width="50%"><h4>Encryption :</h4></td>
								    <td class="back" align="left">
								     <html:select property="encryption">
						   			 <html:options property="encryption_value" labelProperty="encryption_label"/> 			
					     			 </html:select>
								    </td>
					               </tr>	
								    <%--
								    <tr bgcolor="#929CF4">
								    <td class="hf" align="right" width="50%"><h4>Incoming Charge Prefix:</h4><b>Note:</b>Separate each prefix with a new line.</td>
								    <td class="back" align="left"><html:textarea property="incoming_prefix" cols="15" rows="5"/></td>
								    </tr>
								     --%>
								    <%--
								    <tr bgcolor="#929CF4">
								    <td class="hf" align="left" width="50%"><h4>Queue Threshold Status </h4>To enable or disable the queue threshold check. </td>
								    <td class="back" align="left"><html:checkbox property="enabled_st" value="Y"/></td>
					               </tr>
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="left" width="50%"><h4>Queue Threshold </h4>To set the threshold to be notified. </td>
								    <td class="back" align="left"><html:text property="queue_threshold"/>&nbsp;message(s)</td>
					               </tr>
					               <tr bgcolor="#929CF4">
								    <td class="hf" align="left" width="50%"><h4>Queue Threshold Notification</h4>To set the email addresses to be notified when threshold is met. </td>
								    <td class="back" align="left">
								    Email From: <html:text property="email_from" size="30"/><br>
									Email To: <br><font size="-1">Separate each email address with a new line. 
									<br><b>e.g.</b><br> abc@123.com<br>def@1234.com</font><br>
									<html:textarea property="email_to" cols="50" rows="10" />

											<br>
									&nbsp;&nbsp;&nbsp;&nbsp
									<html:checkbox property="enable_notification" value="Y"/>&nbsp;Enable Notification<br>
									</td>
					               </tr>
										 --%>						   
								</tbody>
                              </table>
                            
                            
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	


  	
				<p> &nbsp;</p>
				<table align="center">
			
				<input type="submit" name="submit" value="Save API" />
				<input type="reset" name="reset" value="Reset">
				
				</html:form>
                    </td>
                  </tr>
                </tbody>
              </table>
           
           
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  
 	<!-- <tr>
    <td>
    <br>	
      <input type="button" name="buttonView" class="form_button" value="View Detail" onclick="if (getCheckedValue(document.PrListForm.selPR)) popUp('../asm/prView.do?prID='+document.PrListForm.hiddenSel.value)">
	</td>
  </tr>   -->

</tbody>
</table>
<br>





<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>
	</body>
	<!-- body end -->
</html:html>