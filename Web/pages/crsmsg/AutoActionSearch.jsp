<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Action Method</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		
		
		
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
	            	<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Action Method List</b></td></tr>
						<tr>
							<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/SearchSMSAutoAction.do" focus="searchkey">
								<table border="0" width="90%">
								<tr>
									<td width="10%" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/AddAutoAction.do">Add Action Method</a></td>
									<td class="plain" width="80%" align="right">
										Search:
										<html:text property="searchkey" styleClass="search_input"/>
										&nbsp;By&nbsp;
										<html:select property="selectedState" styleClass="search_select">
											<html:options property="searchby_value" labelProperty="searchby_label" /> 
										</html:select>
										<html:submit property="submitBtn" value="Search" styleClass="search_btn" />
									</td>
								</tr>
								</table>
								<br>
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<tr>
											<td class="hf" align="center" rowspan="1" width="5%"><b>No.</b></td>
                                    		<td class="hf" align="center" rowspan="1" width="10%"><b>Action Method Name</b></td>
                                  			<td class="hf" align="center" rowspan="1" width="15%"><b>Default Action</b></td>
                                    		<td class="hf" align="center" rowspan="1" width="15%"><b>Created Date</b></td>
                                    		<td class="hf" align="center" rowspan="1" width="15%"><b>Modified Date</b></td>
                                    	
											<td class="hf" align="center" rowspan="1" width="5%"><b>Delete</b></td>
										</tr>
										<%--
										<tr>	
							   				<td class="hf" align="center" width="15%"><b>Within</b></td>   
                                    		<td class="hf" align="center" width="15%"><b>Inter</b></td>
                                    	<tr>
                                    	 --%>
										<logic:present name="autoActionList">
                                		<logic:iterate id="inBox1" name="autoActionList" indexId="i">
										<tr>													
											<td align="center" class="back" width="5%"><%=i+1%></td>
											<td align="center" class="back" width="20%"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditAutoAction.do?id=<bean:write name="inBox1" property="id"/>"><bean:write name="inBox1" property="description"/></a></td>
											<td align="center" class="back" width="15%"><bean:write name="inBox1" property="defaultAction"/></td>
											<td align="center" class="back" width="15%"><bean:write name="inBox1" property="createdDatetime"/></td>
											<td align="center" class="back" width="15%"><bean:write name="inBox1" property="modifiedDatetime"/></td>
											
											<td align="center" class="back" width="5%"><html:multibox property="selectedMsg"><bean:write name="inBox1" property="id"/></html:multibox></td>
										</tr>
										</logic:iterate>	
										</logic:present>
										</table>
									</td>
								</tr>
								</table>
								
								<table border=0 width="100%" cellpadding="10">
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="9" align="right">
										
											Select all:	
											<input type="checkbox" name="Check_ctr" value="yes"onClick="checkAllCheckboxes(this);">
											<html:submit property="deleteBtn" value="Delete Selected Action Method(s)" />
										</html:form>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>