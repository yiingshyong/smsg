<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Add Action Method</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>	
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr align="right">
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Action Method</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
								<tr>
									<td class="back">
										<html:form action="/crsmsg/SaveEditAutoAction.do">
										<html:hidden property="id" />
										<TABLE border=0 bgcolor="#666666" align="center">
										<TR>
											<TD class="info" align=middle colSpan=2>
												&nbsp;
												<BR>
												<B><font color="white">Edit Action Method</font></B>
												<BR>
												&nbsp;
												<BR>
											</TD>
										</TR>
										<tr bgcolor="#707DF4">
								    <td class="hf" align="right">Action Method Name :</td>
								    <td class="back" align="left"><html:text property="description" size="50"/></td>
					               </tr>
					               <tr bgcolor="#707DF4">
								    <td class="hf" align="right">Action Class :</td>
								    <td class="back" align="left"><html:text property="actionClassName" size="100"/></td>
					               </tr>
									<tr bgcolor="#707DF4">
								    <td class="hf" align="right">Action Sequence :</td>
								    <td class="back" align="left"><html:text property="actionSequence" maxlength="9" onkeypress="return isNumberKey(event, this.value)" />	</td>
					               </tr>
					             <tr bgcolor="#707DF4">
										    <TD class="hf" align=right>Default Action: </TD>
										    <TD class="back" align=left>
										    	<html:select property="defaultAction">
													<html:options property="status_value" labelProperty="status_label" />
										    	</html:select> 
										    </TD>
									 </tr>
  								<tr bgcolor="#707DF4">
										    <td class="hf" align="right">Server IP<br></td>
										    <td class="back" align="left">
										    	<html:text property="serverIP" maxlength="35"/>
										    </td>
										</tr>
									  <tr bgcolor="#707DF4">
										    <td class="hf" align="right">TCP Port No<br></td>
										    <td class="back" align="left">
										    	<html:text property="serverPort" maxlength="5" />
										    </td>
										</tr>
										  <tr bgcolor="#707DF4">
										    <td class="hf" align="right">Server Connection Timeout (Second)<br></td>
										    <td class="back" align="left">
										    	<html:text property="serverResponseTimeout" maxlength="9" />
										    </td>
										</tr>
										  <tr bgcolor="#707DF4">
										    <td class="hf" align="right">Maximum Number of Attempt<br>	
												(For failure attempt)</td>
										    <td class="back" align="left">
										    	<html:text property="maxFailureAttempt" maxlength="5" />
										    </td>
										</tr>
										  <tr bgcolor="#707DF4">
										    <td class="hf" align="right">Time<br></td>
										    <td class="back" align="left">
										    	<html:text property="svcWindow" maxlength="9" onkeypress="return isNumberKey(event, this.value)" />										    	
										    </td>
										</tr>
											 <tr bgcolor="#707DF4">
										    <td class="hf" align="right">Failed Response Code<br></td>
										    <td class="back" align="left">
										    	<html:text property="failureCode" maxlength="9" onkeypress="return isNumberKey(event, this.value)" />										    	
										    </td>
										</tr>
											 <tr bgcolor="#707DF4">
										    <td class="hf" align="right">Success Response Code<br></td>
										    <td class="back" align="left">
										    	<html:text property="successCode" maxlength="9" onkeypress="return isNumberKey(event, this.value)" />										    	
										    </td>
										</tr>
											 <tr bgcolor="#707DF4">
										    <td class="back" align="right">Service Unavailability SMS<br></td>
										    <td class="back" align="left">
										    	<html:textarea rows="7" property="unavailabilitySms"></html:textarea>
										    </td>
										</tr>
									    <tr bgcolor="#707DF4">
										    <td class="stats" align="left"><b>Parameter Key </b><br></td>
										    <td class="stats" align="left">
										    	<html:text property="parameterKey" maxlength="9" />										    	
										    </td>
										</tr>
										<TR bgcolor="#98AFC7">
											<TD class="back" align=middle colSpan=2>
												<html:submit value="Submit" property="submitBtn" /> 
												<html:reset value="Reset" property="resetBtn" />
											</TD>
										</TR>
										</TABLE>
										</html:form>
									</td>
								</tr>
								</table>      
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>