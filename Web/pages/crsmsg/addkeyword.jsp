<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Add New Keyword</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>	
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr align="right">
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Add New SMS Keyword</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="90%">
								<tr>
									<td class="back">
										<html:form action="/crsmsg/SaveAddKeyword.do">
										<TABLE border=0 bgcolor="#666666" align="center">
										<TR>
											<TD class="info" align=middle colSpan=2>
												&nbsp;
												<BR>
												<B><font color="white">New Keyword</font></B>
												<BR>
												&nbsp;
												<BR>
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="50%">Keyword: </TD>
											<TD class="back" align=left width="50%"><html:text property="keyword" size="15" maxlength="100" /></TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right width="50%">Ownership: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:select property="selected_ownership">
													<html:option value="0">Please select.</html:option>
													<html:options property="ownership_value" labelProperty="ownership_label" /> 
												</html:select>
										    </TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right width="50%">Action Method: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:select property="selectedAutoAction">
													<html:optionsCollection  styleClass="form_input" property="autoActions" label="name" value="value"/> 
												</html:select>
												Click here for [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SMSAutoAction.do">Action Method Setup</a>]
										    </TD>	
										 </TR>				
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right width="50%">Auto Expired: </TD>
										    <TD class="back" align=left width="50%"><html:checkbox property="autoExpired" value="Y" /></TD></TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="50%"><DIV id=leftExpDate>Expiry Date: </DIV></TD>
											<TD class="back" align=left width="50%">
												<html:text property="expiryDate" readonly="true" maxlength="20" size="20" /><a  ID="expiryDateAnchor" NAME="expiryDateAnchor" href="javascript:NewCal1('expiryDate','ddmmyyyy',true,24,'dropdown',true,'expiryDateAnchor')"><img src="<jsp:getProperty name='req' property='contextPath'/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" vAlign=top align=right width="50%">Redirect URL: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:text property="redirectURL" size="50" maxlength="1000" /><br>
										    </TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" vAlign=top align=right width="50%">Redirect URL Parameters: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:text property="redirectURLParam" size="30" maxlength="200" /><br>
										    	*Enter parameters name in sequence: <br>
										    	Mobile no., SMS message, received datetime
										    </TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right width="50%">Redirect Email: </TD>
											<TD class="back" align=left width="50%">
												<html:textarea property="redirectEmail" rows="5" cols="30" /> 
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right width="50%">Redirect Mobile Number: </TD>
											<TD class="back" align=left width="50%">
												<html:textarea property="redirectMobileNo" rows="5" cols="30" /> 
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right width="50%">Autoreply: </TD>
											<TD class="back" align=left width="50%"><html:checkbox property="autoReply" value="Y" /></TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right width="50%">Autoreply Message: </TD>
											<TD class="back" align=left width="50%">
												<html:textarea property="autoReplyMessage" rows="6" cols="24" />
												<BR>
												<br>
												<input type="button" onclick="setPopupTarget(this.form.autoReplyMessage);window.open('<jsp:getProperty name="req" property="contextPath"/>/crsmsg/CannedMessage.do', 'targetMsg', 'width=650,height=500,resizable=1,scrollbars=1');" value="Canned messages" />
												<BR>
												<br>
												&nbsp;Character Set:&nbsp; 
												<html:select property="selected_charset"> 
													<html:options property="charset_value" labelProperty="charset_label" />
												</html:select> 
										  		<BR>&nbsp; 
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right width="50%">Status: </TD>
										    <TD class="back" align=left width="50%">
										    	<html:select property="selected_status">
													<html:options property="status_value" labelProperty="status_label" />
										    	</html:select> 
										    </TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="50%">Maximum Redirect Attempts: </TD>
											<TD class="back" align=left width="50%"><html:text property="maxRedirect_attempts" size="10" maxlength="20" /></TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="50%">Redirect Interval: </TD>
											<TD class="back" align=left width="50%"><html:text property="redirectInterval" size="10" maxlength="20" />&nbsp;second(s)</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="back" align=middle colSpan=2>
												<html:submit value="Submit" property="submitBtn" /> 
												<html:reset value="Reset" property="resetBtn" />
											</TD>
										</TR>
										</TABLE>
										</html:form>
									</td>
								</tr>
								</table>      
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>