<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>User Management - Add User</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/selectionbox.js"></script>
		<script language="JavaScript">
			function loadSelectionBox(){
				selectionBoxInit(document.getElementById("selected_role"), document.getElementById("selected_roles"), document.getElementById("strSelected_roles"), document.getElementById("selectedAllRoles"));
			}
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body onload="loadSelectionBox();">
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%"> 
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>User Management</b></td></tr>
						<tr>
							<td class="back" align="center">
								<html:form action="/crsmsg/SaveAddUser.do">
								<table border="0" bgcolor="777777">
								<tr>
									<td class="info" colspan="2" align="center">&nbsp;<br/><b><font color="ffffff">Add New User</font></b><br/>&nbsp;<br/></td>
								</tr>
								
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right">User name</td>
									<td class="back" width="50%" align="left"><html:text property="userid" size="15" maxlength="15" /></td>
								</tr>
								
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right">Full name</td>
									<td class="back" width="50%" align="left"><html:text property="username" size="50" maxlength="50"/></td>
								</tr>
								
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right">Password</td>
									<td class="back" width="50%" align="left"><html:password property="password" size="25" maxlength="25"/></td>
								</tr>
								
								<!-- Role -->
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right">User Role&nbsp;</td> 
									<html:hidden property="strSelected_roles" />
									<td class="back" width="50%" align="left">
										<table>
										<tr>
											<td>
												<html:select property="selected_role" styleId="TxtContent" size="8">
													<html:options property="role_value" labelProperty="role_label" /> 
												</html:select>
											</td>
											<td width="1">
												<input type="button" value=" >> " onclick="DoAdd()" ID="Button1" NAME="Button1">
												<A href="#" onclick="DoAddAll()">All</A>
												<br><br>
												<INPUT type="button" value=" << " onclick="RemoveItem()" ID="Button2" NAME="Button2" >
												<A href="#" onclick="RemoveAllItem()">All</A>
											</td>
											<td>
												<html:hidden property="selectedAllRoles" />
												<html:select styleId="Select1" size="8" property="selected_roles">
													<html:options property="selected_role_value" labelProperty="selected_role_label" /> 
												</html:select>
											</td>
										</tr>
										</table>
									</td>
								</tr>
								
								<!-- Department to be assigned -->
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right" valign="top">Department</td>
									<td class="back" width="50%" align="left">
										<html:select property="selected_department" styleClass="form_input">
										<html:option value="0">Please select.</html:option>
										<html:options property="department_value" labelProperty="department_label" /> 
										</html:select>
									</td>
								</tr>
								
								<!-- Access Rights -->
								<tr bgcolor="#98AFC7">
									<td class="hf" width="50%" align="right" valign="top">Additional Rights</td>
									<td class="back">
										<table>
										<logic:iterate id="rights_value" name="CrsmsgUserFormAdd" property="rights_value" indexId="i">
										<tr>
											<td>
												<html:multibox property="selected_rights" onclick="toggleCheckAll(this.document.CrsmsgUserFormAdd.selected_rights, this.document.getElementById('allbox'))"><bean:write name="rights_value" /></html:multibox>
												<logic:iterate id="rights_label" name="CrsmsgUserFormAdd" property="rights_label" indexId="j">
													<logic:equal name="j" value="<%=Integer.valueOf(i).toString()%>">
														<bean:write name="rights_label" />
													</logic:equal>
												</logic:iterate>
											</td>
										</tr>
										</logic:iterate>
										<tr><td><br><br><input type="checkbox" name="allbox" id="allbox" value="" onClick="toggleCheckAll(this, this.document.CrsmsgUserFormAdd.selected_rights)">&nbsp;Select All</td></tr>
										</table>
									</td>
								</tr>
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="2" align="center">
									<html:submit property="submitBtn" value="Submit" onclick="generateRecordsList();" />
									<html:reset property="resetBtn" value="Reset" />
									</td>
								</tr>
								</table>
								</html:form>
		                    </td>
		                  </tr>
						</tbody>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>