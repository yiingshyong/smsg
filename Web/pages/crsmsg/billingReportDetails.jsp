<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>CRSMSG - Billing Report</title>
  <link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
  
</head>

<body>
<html:form action="/crsmsg/BillingReportSearch.do">
<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
	<tr align="right" bgcolor="ffffff">
 		<td>CSV Format : <a href="../crsmsg/BillingReportDetailsGenerateCSV.do?startDate=<bean:write name="BillingReportForm" property="startDate"/>&endDate=<bean:write name="BillingReportForm" property="endDate"/>&dept=<bean:write name="BillingReportForm" property="dept"/>">Export<br>
		</td>
	</tr>	
		
<tr> 

  <tbody>
    

    <logic:present name="count">
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<% Integer aa = (Integer) request.getAttribute("count") ;
			if (aa < 20 ){ %>
			
			<style type="text/css">
			<!--
			.resultInfo {
			   color:#f80;
			   background-color: transparent;
			   text-transform:Uppercase;
			   padding: 5px 5px 5px 0px;
			   margin: 0;
			   font-size: 1em;
			}
			.rnav {
			    padding: 0;
			    font-family: Verdana, Arial, Helvetica, Sans-serif;
			    font-size: 1em;
			    color:#333;
			    background-color:#fff;
			    font-weight:bold; 
			    font-size: 1em;
			}
			.rnavLabel {
			    text-transform: Uppercase;
			    color:#f80;
			    background-color: transparent;
			}
			a.rnavLink {
			    color: #415481;
			    background-color: transparent;
			}
			a:visited.rnavLink {
			    color: #8A9CBD;
			    background-color: transparent;
			}
			a:hover.rnavLink {
			    color: #f80;
			    text-decoration: none;
			    background-color: transparent;
			}
			-->
			</style>
			<tr>
		    <td class="back" align="center" >
			
			<div class="resultInfo">
			Displaying results: <strong><%
							out.print(aa);
						%></strong> 
			</div>
			
			</td>
			</tr>
			<%
			}
		%> 	
		  <tr>
		    <td class="back" align="center" >
	<pg:pager 
			      items="<%= aa.intValue() %>"
			      index="<%= index %>"
			      maxPageItems="<%= maxPageItems %>"
			      maxIndexPages="<%= maxIndexPages %>"
			      isOffset="<%= true %>"
			      export="offset,currentPageNumber=pageNumber"
			      scope="request"
			      url="../crsmsg/BillingReportDetailsSearch.do">
	  		<pg:param name="index"/>
			  <pg:param name="maxPageItems"/>
			  <pg:param name="maxIndexPages"/>
			  <pg:param name="startDate"/>
	   <pg:param name="endDate"/>
	    <pg:param name="dept"/>
			   <input type="hidden" name="pager.offset" value="<%= offset %>" />
		      <pg:index>
		        <jsp:include page="alltheweb.jsp" flush="true"/>
		      </pg:index>
		  
		      </pg:pager>
		
		    </td>
		  </tr>
		</table>
	</logic:present>

<br>
	


    <tr>
  
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center">
                    <b>
					<logic:equal name="BillingReportForm" property="status" value="I">                    
                    	Incoming Billing Report Details
                    </logic:equal>
					<logic:notEqual name="BillingReportForm" property="status" value="I">
						Billing Report Details
					</logic:notEqual>
                    </b></td>
                  </tr>
                  <tr>
                    <td class="back">

                       <logic:present name="billingReportDetails">
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                        <logic:equal name="BillingReportForm" property="status" value="I">
	                        <tr>
	                            <td>
	                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
	                                <tbody>
	                                  <tr>
	                                    <td class="hf" align="center"><b>No</b></td>
	                                    <td class="hf" align="center"><b>Received Date</b></td>
	                                    <td class="hf" align="center"><b>Mobile Number</b></td>
	                                    <td class="hf" align="center"><b>Message</b></td>
	                                    <td class="hf" align="center"><b>Department</b></td>
	                                    <td class="hf" align="center"><b>Telco</b></td>
	                                    <td class="hf" align="center"><b>Cost (RM)</b></td>
	                                  </tr>
	                                <logic:iterate id="reportDetails" name="billingReportDetails">
									  <tr>
									    <td class="back" align="left"><bean:write name="reportDetails" property="seq"/></td>
	                                  	<td class="back" align="left"><bean:write name="reportDetails" property="date"/></td> 
										<td class="back" align="left"><bean:write name="reportDetails" property="mobileNo"/></td>  
	                                    <td class="back" align="left"><bean:write name="reportDetails" property="message"/></td>  
	                                    <td class="back" align="left"><bean:write name="reportDetails" property="dept"/></td>  
	                                    <td class="back" align="left"><bean:write name="reportDetails" property="operator"/></td>
	                                    <td class="back" align="left"><bean:write name="reportDetails" property="cost"/></td>  
	                                   </tr>	
									</logic:iterate>	
							
									</tbody>
	                              </table>
	                            </td>
	                          </tr>
                        </logic:equal>
                        
                        <logic:notEqual name="BillingReportForm" property="status" value="I">
  						
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                    <td class="hf" align="center"><b>No</b></td>
                                    <td class="hf" align="center"><b>Sent Date</b></td>
                                    <td class="hf" align="center"><b>Created Date</b></td>                                   
                                    <td class="hf" align="center"><b>Mobile Number</b></td>
                                    <td class="hf" align="center"><b>Message</b></td>
                                    <td class="hf" align="center"><b>Sent By</b></td>
                                    <td class="hf" align="center"><b>Department</b></td>
                                    <td class="hf" align="center"><b>Approval By</b></td>
                                    <td class="hf" align="center"><b>Telco</b></td>
                                    <td class="hf" align="center"><b>Cost (RM)</b></td>
                                  </tr>
                                <logic:iterate id="reportDetails" name="billingReportDetails">
								  <tr>
								    <td class="back" align="left"><bean:write name="reportDetails" property="seq"/></td>
                                  	<td class="back" align="left"><bean:write name="reportDetails" property="date"/></td> 
                              		<td class="back" align="left"><bean:write name="reportDetails" property="created"/></td>                                   	 
									<td class="back" align="left"><bean:write name="reportDetails" property="mobileNo"/></td>  
                                    <td class="back" align="left"><bean:write name="reportDetails" property="message"/></td>  
                                    <td class="back" align="left"><bean:write name="reportDetails" property="sentBy"/></td>  
                                    <td class="back" align="left"><bean:write name="reportDetails" property="dept"/></td>  
                                    <td class="back" align="left"><bean:write name="reportDetails" property="approval"/></td>
                                    <td class="back" align="left"><bean:write name="reportDetails" property="operator"/></td>
                                    <td class="back" align="left"><bean:write name="reportDetails" property="cost"/></td>  
                                   </tr>	
								</logic:iterate>	
						
								</tbody>
                              </table>
                            </td>
                          </tr>
                          </logic:notEqual>
                        </tbody>
                        
                      </table>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


</tbody>
</table>

<logic:present name="count">
<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<% Integer aa = (Integer) request.getAttribute("count") ;

%> 		
  <tr>
    <td class="back" align="center" >
    
		<pg:pager 
	      items="<%= aa.intValue() %>"
	      index="<%= index %>"
	      maxPageItems="<%= maxPageItems %>"
	      maxIndexPages="<%= maxIndexPages %>"
	      isOffset="<%= true %>"
	      export="offset,currentPageNumber=pageNumber"
	      scope="request"
	 		      url="../crsmsg/BillingReportDetailsSearch.do">
	  	 <pg:param name="index"/>
	  <pg:param name="maxPageItems"/>
	  <pg:param name="maxIndexPages"/>
	  <pg:param name="startDate"/>
	   <pg:param name="endDate"/>
	    <pg:param name="dept"/>
	   <input type="hidden" name="pager.offset" value="<%= offset %>">
      <pg:index>
        <jsp:include page="alltheweb.jsp" flush="true"/>
      </pg:index>
  
      </pg:pager>

    </td>
  </tr>
</table>
</logic:present>

<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
