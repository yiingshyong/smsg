<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>sgw - FTP Report</title>
<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
</head>

<body>

	
<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  

  <tbody>
    <tr>
      <td>

      </td>    
    </tr>
    
    
   
    <tr align="right">
  
    <td>
 
    
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>Bulk Send Log</b></td>
                  </tr>
                  <tr>
                    <td class="back">
                    
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%" bgcolor="ffffff">
                                <tbody>
                              	 <tr align="center">
					              	<td><br>
					              	</td>
					              	<td><br>
					              	</td>
             				 	</tr>
                              	 <tr align="center">
					              	<td><br>
					              	</td>
					              	<td><br>
					              	</td>
             				 	</tr>   
             				 	       
                              	 <tr align="center">
                              	   <html:form action="/crsmsg/ManualBulkLog.do">
					              	<td>
					              	<input type="submit" value="Manual Bulk Send Log" name="butSubmit">
					              	</td>
					              </html:form>
					             <html:form action="/crsmsg/FtpBulkLog.do">
					              	<td>
					              	<input type="submit" value="FTP Bulk Send Log" name="butSubmit">
					              	</td>
					              </html:form>
             				 	</tr>
                              	 <tr align="center">
					              	<td><br>
					              	</td>
					              	<td><br>
					              	</td>
             				 	</tr>
                              	 <tr align="center">
					              	<td><br>
					              	</td>
					              	<td><br>
					              	</td>
             				 	</tr>						         
						        </tbody>
						      </table>
    </td>
  </tr>
  


</tbody>
</table>

<br>
</form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>