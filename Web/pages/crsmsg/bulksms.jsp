<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title></title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetime.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body onload="initTimer(document.getElementById('systemDateTime').value);initCharCounter('<bean:write name="CrsmsgBulkSMSFormAdd" property="concatenateSMS" />', '<bean:write name="CrsmsgBulkSMSFormAdd" property="maxNoOfSMS" />', '<bean:write name="CrsmsgBulkSMSFormAdd" property="charPerSms" />');populateMaxCharLimit(document.getElementById('selected_charset'), document.getElementById('charLeftField'));toggleLayer(document.getElementById('scheduledSMS'),document.getElementById('divSched'));">
		<br>	
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Bulk SMS</b></td></tr>
						<tr>
							<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/SaveBulkSMS.do" enctype="multipart/form-data">
								<html:hidden property="logId" />
								<html:hidden property="bulkName" />
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right width="40%">
												File: 
												<logic:notEmpty name="CrsmsgBulkSMSFormAdd" property="bulkLimit">
												<br>
												(*Bulk file records line limit: <bean:write name="CrsmsgBulkSMSFormAdd" property="bulkLimit" />, 
												<br>
												exceeded file record(s) will not be processed.)
												</logic:notEmpty>
											</TD>
											<TD class="back" align=left width="60%"><html:file property="bulkSMSFile" /></TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right>Character Set:</TD>
										    <TD class="back" align=left>
										    	<html:select property="selected_charset" onchange="showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));"> 
													<html:options property="charset_value" labelProperty="charset_label" />
												</html:select>
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" align=right>Action Method:</TD>
										    <TD class="back" align=left>
										    	<html:select property="fileConversion">
													<html:option value="0">Please select.</html:option>
													<html:options property="fc_value" labelProperty="fc_label" /> 
												</html:select>
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
											<TD class="hf" vAlign=top align=right>Format:</TD>
											<TD class="back" align=left>
												<html:select property="bulkFileMode" onchange="if(this.value=='Phone & Template'){document.getElementById('templateBtn').disabled=true;document.getElementById('divDelim').style.display='block';document.getElementById('smsContent').readOnly='true';}else{document.getElementById('templateBtn').disabled=false;document.getElementById('divDelim').style.display='none';document.getElementById('smsContent').readOnly=null;}showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));"> 
													<html:options property="bulkFileMode_value" labelProperty="bulkFileMode_label" />
												</html:select>
												<DIV id="divDelim" style="DISPLAY: none">
													<BR>
													Delimiter: 
													<html:select property="fileDelimiter"> 
														<html:option value="|">|</html:option>
														<html:option value=",">,</html:option>
													</html:select>
												</DIV>
										      	<BR>
										      	Text Message:
										      	<BR>
										      	<html:textarea styleClass="smsContent" property="smsContent" onkeydown="showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" onkeyup="javascript:showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" onmouseout="javascript:showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" rows="10" cols="50" />										      	
										    	<br>
										      	<FONT size="-1">Characters left:&nbsp;</FONT>
										      	<INPUT id="charLeftField" readOnly maxLength="3" size="3" name="charLeftField" /> 
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<FONT size=-1>No of SMS:&nbsp;
													<INPUT id="noOfSMS" readOnly maxLength="2" size="2" value="0" name="noOfSMS" />&nbsp;/&nbsp;<bean:write name="CrsmsgBulkSMSFormAdd" property="maxNoOfSMS" />
												</FONT> 
												<br>
												<br>
												<input id="templateBtn" type="button" onclick="setPopupTarget(this.form.smsContent);window.open('<jsp:getProperty name="req" property="contextPath"/>/crsmsg/CannedMessage.do', 'targetMsg', 'width=650,height=500,resizable=1,scrollbars=1');" value="Template Messages" onfocus="showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));"/> 
											</TD>
										</TR>
										<TR bgcolor="#98AFC7">
										    <TD class="hf" vAlign=top align=right>Message Verification Required:</TD>
										    <TD class="back" align=left>
										    	<html:select property="verificationRequired">
													<html:options property="verificationRequired_value" labelProperty="verificationRequired_label" />
												</html:select>
											</TD>
										</TR>
										<logic:present name="CrsmsgBulkSMSFormAdd" property="smsPriorityShow">
											<tr>
												<td class="hf" align=right>Priority:</td>
												<td class="back">
													<html:select property="smsPriority">
														<html:options property="smsPriority_value" labelProperty="smsPriority_label" />
													</html:select>
												</td>
											</tr>
										</logic:present>
										<logic:notPresent name="CrsmsgBulkSMSFormAdd" property="smsPriorityShow">
											<html:hidden property="smsPriority" />
										</logic:notPresent>
										<TR bgcolor="#98AFC7">
											<TD class="hf" align=right>Schedule SMS:</TD>
											<TD class="back" >
												<html:checkbox property="scheduledSMS" value="Y" onclick="javascript:toggleLayer(document.getElementById('scheduledSMS'),document.getElementById('divSched'));"/>YES
										    	<BR>
										      	<DIV id="divSched" style="DISPLAY: none">
										      		Date: <html:text property="scheduledSMSDateTime" maxlength="20" size="20" readonly="true" /> 
													<a  ID="scheduledSMSDateTimeAnchor" NAME="scheduledSMSDateTimeAnchor"  href="javascript:NewCal1('scheduledSMSDateTime','ddmmyyyy',true,24,'dropdown',true,'scheduledSMSDateTimeAnchor', '<bean:write name="CrsmsgBulkSMSFormAdd" property="advanceDayLimit" />')"><img src="<jsp:getProperty name='req' property='contextPath'/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
													<BR>
													<html:hidden property="systemDateTime" />
													System Date & Time : <span id="servertime"></span>
										      		<BR>
										      	</DIV>
											</TD>
										</TR>
										</table>
									</td>
								</tr>
								</table>
								<table width="100%" height="100%">
						  		<TR>
						  			<TD class="back" align="center">
						  			<html:submit property="submitBtn" value="Submit" styleClass="checkNoSms" /> 
						  			<html:reset property="resetBtn" value="Clear" />
						  			</TD>
						  		</TR>
						  		</TABLE>
								<input id="multiSmsAlertMsg" type="hidden" value="<bean:write name="multiSmsAlertMsg" scope="session"/>"/>								
						  		</html:form>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>	
	</body>
	<!-- body end -->
</html:html>