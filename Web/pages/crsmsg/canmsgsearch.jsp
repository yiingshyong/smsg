<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Canned Message</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<html:form action="/crsmsg/GetCannedMessageSearch.do" focus="searchkey">
		<table border="0" width="90%">
		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td width="15%" align="center"></td>
			<td width="100%" align="right" class="plain">
					Search:
					<html:text property="searchkey" styleClass="search_input"/>
					&nbsp;By&nbsp;
					<html:select property="selectedState" styleClass="search_select">
						<html:options property="searchby_value" labelProperty="searchby_label" /> 
					</html:select>
					<html:submit property="submitBtn" value="Search" styleClass="search_btn" />
			</td>
		</tr>
		</table>
		<br>
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table border="0" cellpadding="8" cellspacing="1" width="100%">
				<tr>
					<td align="center" class="hf" width="5%"><b>No</b></td>
					<td align="center" class="hf" width="15%"><b>Message Code</b></td>
					<td align="center" class="hf" width="15%"><b>Message Text</b></td>
				</tr>
				<logic:present name="CrsmsgCannedMessageSearchForm" property="resultList">
				<logic:notEmpty name="CrsmsgCannedMessageSearchForm" property="resultList">
				<bean:define id="result" name="CrsmsgCannedMessageSearchForm" property="resultList" />
				<logic:iterate id="aRecord" name="result" property="record" indexId="i">
				<tr bgcolor="#98AFC7">
					<input name="tempMsg[<%=i%>]" type="hidden" value="<bean:write name='aRecord' property='column[2]' />" />
					<td class="back" align="center"><a href="#" onclick="updatePopupTarget(opener.getPopupTarget(), new String(document.getElementById('tempMsg['+<%=i%>+']').value), new String('<bean:write name="aRecord" property="column[0]" />'), opener.getPopupIdTarget());window.close();"><%=i+1%></a></td>
					<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
						<logic:equal name="j" value="1">
							<td class="back" align="center"><a href="#" onclick="updatePopupTarget(opener.getPopupTarget(), new String(document.getElementById('tempMsg['+<%=i%>+']').value), new String('<bean:write name="aRecord" property="column[0]" />'), opener.getPopupIdTarget());window.close();"><bean:write name="aColumn"/></a></td>
						</logic:equal>
						<logic:equal name="j" value="2">
							<td class="back" align="center"><textarea rows=5 cols=20 readonly><bean:write name="aColumn"/></textarea></td>
						</logic:equal>											
					</logic:iterate>    
				</tr>
				</logic:iterate>
				</logic:notEmpty>
				</logic:present>
				</table>
			</td>
		</tr>
		</table>
		</html:form>
		<table width="90%">
		<tr width="100%"><td align="right" class="plain"><input type="button" onClick="window.close()" value="Cancel" /></td></tr>
		</table>
	</body>
	<!-- body end -->
</html:html>