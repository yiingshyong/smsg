<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title></title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script type = "text/javascript">
			function isNumberKey(evt, rec){ 
			var time = rec;   
		  	var len=time.length;
		  	var char="";
		  	var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode <48 || charCode >58){ 
				return false; 			
			}
			return true;		    
		  }
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<html:form action="/crsmsg/SaveEditCardConfig.do">
		<html:hidden property="cfgId" />
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="2" align="center"><b>Card Activation Configuration</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%" align="center">
										<tr>
										    <td class="stats" align="left"><b>Card Link Server IP</b><br></td>
										    <td class="stats" align="left">
										    	<html:text property="ccaServerIP" maxlength="35"/>
										    </td>
										</tr>
										<tr>
										    <td class="back" align="left"><b>Card Link server TCP Port No</b><br></td>
										    <td class="back" align="left">
										    	<html:text property="ccaServerPort" maxlength="5" />
										    </td>
										</tr>
										<tr>
										    <td class="stats" align="left"><b>Card Link Server Connection Timeout (Second)</b><br></td>
										    <td class="stats" align="left">
										    	<html:text property="ccaServerResponseTimeout" maxlength="9" />
										    </td>
										</tr>
										<tr>
										    <td class="back" align="left"><b>Maximum Number of Activation Attempt</b><br>	
												<b>(For failure attempt)</b></td>
										    <td class="back" align="left">
										    	<html:text property="ccaMaxFailureAttempt" maxlength="5" />
										    </td>
										</tr>
										<tr>
										    <td class="stats" align="left"><b>Time</b><br></td>
										    <td class="stats" align="left">
										    	<html:text property="ccaSvcWindow" maxlength="9" onkeypress="return isNumberKey(event, this.value)" />										    	
										    </td>
										</tr>
										<tr>
										    <td class="back" align="left"><b>Successful Activation SMS</b><br></td>
										    <td class="back" align="left">
										    	<html:textarea rows="7" property="ccaSuccessSms"></html:textarea>
										    </td>
										</tr>
										<tr>
										    <td class="stats" align="left"><b>Failure Activation SMS</b><br></td>
										    <td class="stats" align="left">
										    	<html:textarea rows="7" property="ccaFailureSms"></html:textarea>
										    </td>
										</tr>
										<tr>
										    <td class="back" align="left"><b>Service Unavailability SMS</b><br></td>
										    <td class="back" align="left">
										    	<html:textarea rows="7" property="ccaUnavailabilitySms"></html:textarea>
										    </td>
										</tr>
										<tr>
										    <td class="stats" align="left"><b>CardLink Unavailability SMS</b><br></td>
										    <td class="stats" align="left">
										    	<html:textarea rows="7" property="ccaHostUnavailabilitySms"></html:textarea>
										    </td>
										</tr>
										</table>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		<br>
		<table align="center">
		<tr>
			<td>
				<html:submit property="submitBtn" value="Save Setting" />
				<html:reset property="resetBtn" value="Reset" />
			</td>
		</tr>
		</table>
		</html:form>
	</body>
	<!-- body end -->
</html:html>