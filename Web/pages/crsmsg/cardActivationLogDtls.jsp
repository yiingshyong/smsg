<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>Card Activation Log</title>
<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
	<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>

</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
<logic:present name="dtldCSV">                  
<tr align="right" bgcolor="ffffff"> 			
	<td>CSV Format : <a href="../crsmsg/CardActDetailsCSV.do?butExport=generateCSV">Export<br></td>
</tr>	
		
<tr>
</logic:present> 		
	<tr>
		<td class="error_td" >
		<html:errors bundle="crsmsg" />
		</td>
	</tr> 
<html:form action="/crsmsg/CardActDetailsSearch.do">
  <tbody>
    <tr>
      <td>
         <table class="summary_tbl" align="right" border="0" cellpadding="2" cellspacing="2">
          <tbody>
            <tr class="summary_title">            
              <td>
               Search :<html:text property="searchkey" styleClass="form_input" /> 
	           <html:select property="selectedState" styleClass="form_input">
               <html:options property="searchby_value" labelProperty="searchby_label" styleClass="form_input"/> 
               </html:select>               
               <input type="submit" name="butSearch" value="Search" class="form_button"> 
               <input type="reset" value="Reset" class="form_button">
			  </td>
			 </tr>
			</tbody>
        </table>

      </td>    
    </tr>
    
   <logic:present name="countTotal">
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<% Integer aa = (Integer) request.getAttribute("countTotal") ;
			if (aa <= 20 ){ %>
			
			<style type="text/css">
			<!--
			.resultInfo {
			   color:#f80;
			   background-color: transparent;
			   text-transform:Uppercase;
			   padding: 5px 5px 5px 0px;
			   margin: 0;
			   font-size: 1em;
			}
			.rnav {
			    padding: 0;
			    font-family: Verdana, Arial, Helvetica, Sans-serif;
			    font-size: 1em;
			    color:#333;
			    background-color:#fff;
			    font-weight:bold; 
			    font-size: 1em;
			}
			.rnavLabel {
			    text-transform: Uppercase;
			    color:#f80;
			    background-color: transparent;
			}
			a.rnavLink {
			    color: #415481;
			    background-color: transparent;
			}
			a:visited.rnavLink {
			    color: #8A9CBD;
			    background-color: transparent;
			}
			a:hover.rnavLink {
			    color: #f80;
			    text-decoration: none;
			    background-color: transparent;
			}
			-->
			</style>
			<tr>
		    <td class="back" align="center" >
			
			<div class="resultInfo">
			Displaying results: <strong><%
							out.print(aa);
						%></strong> 
			</div>
			
			</td>
			</tr>
			<%
			}else{
		%> 		

		
		  <tr>
		    <td class="back" align="center" >
		 
			  <pg:pager 
			      items="<%= aa.intValue() %>"
			      index="<%= index %>"
			      maxPageItems="<%= maxPageItems %>"
			      maxIndexPages="<%= maxIndexPages %>"
			      isOffset="<%= true %>"
			      export="offset,currentPageNumber=pageNumber"
			      scope="request"
			      url="../crsmsg/CardActDetailsSearch.do">
			  <pg:param name="index"/>
			  <pg:param name="maxPageItems"/>
			  <pg:param name="maxIndexPages"/>
			  
			   <input type="hidden" name="pager.offset" value="<%= offset %>">
		 
		      <pg:index>
		        <jsp:include page="alltheweb.jsp" flush="true"/>
		      </pg:index>
		  
		      </pg:pager>
		
		    </td>
		  </tr>
		  <%}%>
		</table>
	</logic:present>
	<br>
    <tr>
  <TABLE width="90%" border=0>
  <TBODY>
		<TR align=left>
		    <Td width="18%" bgcolor="#98AFC7" align="center"><b>Pending Card(s)</b></Td>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="CardActivationDtlsForm" property="notActivated"/></b></TD>
		    <TD class=plain width=*></TD>
		</TR>
		<TR align=left>
		    <Td width="18%" bgcolor="#98AFC7" align="center"><b>Failed Card(s)</b></Td>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="CardActivationDtlsForm" property="failedActivated"/></b></TD>
		    <TD class=plain width=*></TD>
		</TR>
		<TR align=left>
		    <Td width="18%" bgcolor="#98AFC7" align="center"><b>Card(s) Activated</b></Td>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="CardActivationDtlsForm" property="activated"/></b></TD>
		    <TD class=plain width=*></TD>
		</TR>
		</TBODY></TABLE><BR>
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>Card Activation Log Details</b></td>
                  </tr>
                   <logic:present name="calList">
					<tr align="left" bgcolor="ffffff"> 			
						<td><input value="Delete Record(s)" name="butDelete" type="submit"><br>
						</td>
					</tr>
                  <tr>
                    <td class="back">
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                    <td class="hf" align="center"><input type=checkbox name="checkAll" onclick="checkAllCheckboxes(this);"/></td>  
   									<td align="center" class="hf"><b>No</b></td>
									<td align="center" class="hf"><b>Date</b></td>
									<td align="center" class="hf"><b>Mobile No</b></td>
									<td align="center" class="hf"><b>Card Number</b></td>
									<td align="center" class="hf"><b>IC Number</b></td>
									<td align="center" class="hf"><b>Status</b></td>                                 
                                  </tr>
                                <logic:iterate id="calListing" name="calList">
								  <tr>
								     <td class="back" align="center"><html:multibox property="selectedMsg"><bean:write name="calListing" property="id"/></html:multibox></td>
	                                 <td class="back" align="center"><bean:write name="calListing" property="no"/></td>
									 <td class="back" align="center"><bean:write name="calListing" property="date"/></td>
									 <td class="back" align="center"><bean:write name="calListing" property="mobileNo"/></td>
									 <td class="back" align="center"><bean:write name="calListing" property="cardNumber"/></td>
									 <td class="back" align="center"><bean:write name="calListing" property="icNumber"/></td>
									 <td class="back" align="center"><bean:write name="calListing" property="status"/></td>
                                  </tr>
								</logic:iterate>							
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>                        
                      </table>                    
                      	  <b>Status: P - Pending/Not Activated, Y - Succesfully, F - Failed/Error</b>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

</tbody>
</table>
<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
</table>
</body>
</html:html>