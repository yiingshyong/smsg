<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>
<html:html>
<head>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>
<title>Welcome to SMS</title>
<script type="text/javascript">
var err_ind ="<html:errors/>";

</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Channel Maintenance&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors bundle="crsmsg"/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
<!-- Edit Dept Dialog -->
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/crsmsg/ChannelSave.do" >

<tr valign="top" >
<td class=form_non_input width=20%>User ID&nbsp;</td>
<td width=1%>:</td>
<td><html:text property="userChannel.userChannelIdObj.userId" size="50" maxlength="50" styleClass="form_input_readonly" readonly="true"/></td>
</tr>
<tr valign="top" >
<td class=form_non_input width=20%>Channel Name&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="userChannel.userChannelIdObj.channelId" size="50" maxlength="15" styleClass="form_input" /></td>
</tr>	
	
<tr class=form_non_input>		
<td>SMS Type</td>
<td>:</td>
<td>
<html:select property="userChannel.smsType.tblRefId" styleClass="form_input">
<html:optionsCollection  styleClass="form_input" property="smsTypes" label="label" value="tblRefId"/> 
</html:select>
</td>
</tr>
	
<tr class=form_non_input>		
<td>Time Validity (seconds)</td>
<td>:</td>
<td><html:text property="userChannel.timeValidity" size="50" maxlength="5" styleClass="form_input limitInt" /></td>
</tr>
	
<tr class=form_non_input>		
<td>Channel Status</td>
<td>:</td>
<td>
<html:select property="userChannel.status" styleClass="form_input">
<html:optionsCollection  styleClass="form_input" property="status" label="name" value="value"/> 
</html:select>
</td>
</tr>	

<tr>
<td colspan="3">
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center>Channel Id</td>
<td align=center>SMS Type</td>
<td align=center>Time Validity</td>
<td align=center>Status</td>
</tr>

<logic:present name="ChannelForm" property="userChannels">
<logic:iterate id="channel" name="ChannelForm" property="userChannels">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<td><a href="../crsmsg/ChannelListing.do?userId=<bean:write name="channel" property="userChannelIdObj.userId"/>&channelId=<bean:write name="channel" property="userChannelIdObj.channelId"/>" ><bean:write name="channel" property="userChannelIdObj.channelId"/></a></td>
<td align=left><bean:write name="channel" property="smsType.label"/></td>
<td align=left><bean:write name="channel" property="timeValidity"/></td>
<td align=left><bean:write name="channel" property="status"/></td>
</tr>
</logic:iterate>
</logic:present>
</table>																
</td>
</tr>


<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" name="actiontype" value="Save" class="form_button">
&nbsp;&nbsp;
<input type="button" value="Reset" class="form_button">
&nbsp;&nbsp;
<%--
<input type=button value="Edit User" onClick="javascript:window.location.href='../crsmsg/EditUser.do?userid=<bean:write property="userChannel.userChannelIdObj.userId" name="ChannelForm"/>'" class="form_button">
 --%>
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			
 
</body>
</html:html>