<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Channel Maintenance - User Search</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">		

		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
				
		<!--
	    <tr class="csvexport">
			<td>CSV Format : <a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/DownloadCSV.do">Export</a></td>
		</tr>
		-->
	    <tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
	            	<td>
	              		<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Channel Maintenance - User Search</b></td></tr>
	                  	<tr>
	                  		<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/ChannelUserSearch.do" focus="searchkey">
								<table border="0" width="90%">
								<tr>
									<td class="plain" width="80%" align="right">
										User Id:
										<html:text property="userId" size="15" maxlength="15" styleClass="form_input" />
										<input type="submit" name="actiontype" value="Search" class="form_button">										 
									</td>
								</tr>
								</table>
								<br>
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<tr>
											<td align="center" class="hf" width="15%"><b>User</b></td>
											<td align="center" class="hf" width="15%"><b>Name</b></td>
											<td align="center" class="hf" width="7%"><b>Status</b></td>
										</tr>
										<logic:present name="ChannelForm" property="users">
										<logic:notEmpty name="ChannelForm" property="users">
												<logic:iterate id="userList" name="ChannelForm" property="users">
												<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'" style="background: none repeat scroll 0% 0% rgb(255, 247, 229);">
												<td><a href="../crsmsg/ChannelListing.do?userId=<bean:write name="userList" property="userId"/>"/><bean:write name="userList" property="userId"/></a></td>
												<td align=left><bean:write name="userList" property="userName"/></td>
												<td align=left><bean:write name="userList" property="userStatus"/></td>
												</tr>
												</logic:iterate>
										</logic:notEmpty>
										</logic:present>
										</table>
									</td>
								</tr>
								</table>
								</html:form>								
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>