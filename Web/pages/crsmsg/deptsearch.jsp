<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html>
<head>
<title>Welcome to SMS</title>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,left = 0,top = 0');");
}
// End -->
</script>	
<script language="JavaScript" src="../includes/crsmsg.js"></script>
</head>

<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0 >

			<table cellspacing=0 cellpadding=0 width="772" border=0>
			<tr>   
    			<td valign=top>
      				<table border=0 cellpadding=0 cellspacing=2>				
					<tr>
					<td><img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Dept  Mgmt&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
			</td>
			</tr>
<tr>
<td class="error_td" >
<html:errors bundle="crsmsg"/>
</td>
</tr>								

			</table>	
<html:form  method="post" action="/crsmsg/DeptSearch.do" focus="searchkey">
<table align=left class="summary_tbl" cellSpacing=2 cellPadding=2  border=1 align="center">
<tr class="summary_title">															
<td colspan=2>Search By</td>															
<td>
<html:text property="searchkey" styleClass="form_input"/> 
<html:select property="selectedState" styleClass="form_input">
<html:options property="searchby_value" labelProperty="searchby_label" styleClass="form_input"/> 

</html:select>
<html:submit property="searchBtn" value="Submit" styleClass="form_input" />
</td>
</tr>
</table>



<br>
<br>
<table class="form_non_input" cellSpacing=0 cellPadding=2 width=100%  border=1 align="center">
<tr class="summary_title">															
<td colspan=2>Selected Depts</td>															
</tr>
</table>



<!-- Detail Table -->
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center>Dept No.</td>
<td align=center>Dept Name</td>
<td align=center>Dept Description</td>
<td align=center>Dept Status</td>
<td align=center>Quota Remain</td>
<td align=center>Total Quota Assigned</td>
<td align=center>Time Control</td>
<td align="center" class="hf" width="5%"><input type="checkbox" name="checkAllTop" onClick="toggleCheckAll(this, this.document.CrsmsgDeptSearchForm.checkedRecords);" /></td>
</tr>

										<logic:present name="CrsmsgDeptSearchForm" property="resultList">
										<logic:notEmpty name="CrsmsgDeptSearchForm" property="resultList">
										<bean:define id="result" name="CrsmsgDeptSearchForm" property="resultList" />
										<logic:iterate id="aRecord" name="result" property="record" indexId="i">
										<tr class=index_row8>
											<td class="back" align="center"><a href="#" onclick="javascript:popUp('../common/EditDept.do?deptid=<bean:write name="aRecord" property="column[0]" />');return false;"><%=i+1%></a></td>
											<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
												<logic:equal name="j" value="1">
													<td class="back" align="center"><a href="#" onclick="javascript:popUp('../common/EditDept.do?deptid=<bean:write name="aRecord" property="column[0]" />');return false;"><bean:write name="aColumn"/></a></td>
												</logic:equal>
												<logic:greaterThan name="j" value="1">
													<td class="back" align="center"><bean:write name="aColumn"/></td>
												</logic:greaterThan>
											</logic:iterate>    
											<td align=left><a href="#" onclick="javascript:popUp('../crsmsg/SMSTimeControl.do?dept=<bean:write name="aRecord" property="column[0]" />');return false;">click here</a></td>
											<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgDeptSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));"><bean:write name='aRecord' property='column[0]' /></html:multibox></td>
										</tr>
										</logic:iterate>
										</logic:notEmpty>
										</logic:present>
<!--										
<logic:present name="dept">
<logic:iterate id="dept2" name="dept">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<a href="#" onclick="javascript:popUp('../common/EditDept.do?deptid=<bean:write name="dept2" property="id"/>');return false;"><td align=left><bean:write name="dept2" property="deptName"/></td>
<td align=left><bean:write name="dept2" property="deptDescription"/></td>

<td align=left><bean:write name="dept2" property="deptStatus"/></td>
</a>
<td align=left><a href="#" onclick="javascript:popUp('../crsmsg/SMSTimeControl.do?dept=<bean:write name="dept2" property="id"/>');return false;">click here</a></td>
<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgDeptSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));toggleCheckAll(this.document.CrsmsgDeptSearchForm.checkedRecords, this.document.getElementById('checkAllBtm'))"><bean:write name="dept2" property="id"/></html:multibox></td>
</tr>
</logic:iterate>
</logic:present>
-->
</table>		
</html:form>														
<!-- End Detail Table -->
<table width=100%>
<tr >
<td align=center>
<input type="button" class="form_button" value="New Dept" onClick="javascript:popUp('../common/AddDept.do')"></td>
</tr>
<tr>
									<td class="back" align="right">
										<html:form action="/crsmsg/UpdateQuota.do">
											<font class="form_non_input">Quota:</font><html:text property="quota" styleClass="form_input" size="10" maxlength="9"/>
											<html:radio property="topUpMode" styleClass="form_non_input" value="topup"><font class="form_non_input">&nbsp;Top-Up</font></html:radio>
											<html:radio property="topUpMode" styleClass="form_non_input" value="reset"><font class="form_non_input">&nbsp;Reset</font></html:radio>
											<html:submit styleClass="form_button" property="setQuotaBtn" value="Set Quota For Selected Department(s)" onclick="return copyFormElement(this.form, this.document.CrsmsgDeptSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden');" />
											<html:button styleClass="form_button" property="setQuotaAllBtn" value="Set Quota For All Departments" onclick="controlCheckAll(true, this.document.CrsmsgDeptSearchForm.checkedRecords);if(copyFormElement(this.form, this.document.CrsmsgDeptSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden')){this.form.submit();}else{}" />
										</html:form>
									</td>
								</tr>
</table>
</body>
</html:html>
