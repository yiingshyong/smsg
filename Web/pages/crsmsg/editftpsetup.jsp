<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Edit FTP Setup</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetime.js"></script>
		<script language="JavaScript">
		function formValidation(){
			if(document.getElementById("ftpPasswordConfirm").value!=document.getElementById("ftpPassword").value){
				alert("Password and Confirmation Password not matched! Please re-enter.")
				return false;
			}
		}
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body onload="initTimer(document.getElementById('systemDateTime').value);toggleTimeMode('<bean:write name="CrsmsgFTPSetupFormEdit" property="ftpTimeMode" />', document.getElementById('divMonth'), document.getElementById('divDaily'), document.getElementById('divHour'), document.getElementById('divTime'));toggleLayer(document.getElementById('smsScheduled'),document.getElementById('divSched'));">
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Edit FTP Setup</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td class="stats">
										<html:form action="/crsmsg/SaveEditFtpSetup.do" enctype="multipart/form-data">
										<html:hidden property="ftpId" />
										<table border="0" width="95%" cellpadding="5" cellspacing="1">
										<tr>
											<td width="40%">
												<b>SFTP Entry Name</b><br>
												<span class="smsapp_desc">This is the naming of configuration entry in SFTP.</span>
											</td>
											<td width="60%"><html:text property="ftpName" size="50" /></td>
										</tr>
										<tr>
											<td width="40%">
												<b>SFTP Description</b><br>
												<span class="smsapp_desc">This is the description of configuration entry in SFTP.</span>
											</td>
											<td width="60%"><html:text property="ftpDesc" size="20" /></td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>SFTP Server/Host: </b><br>
												<span class="smsapp_desc">This is the URL or IP address of SFTP server/host to be conected.</span>
											</td>
											<td width="60%"><html:text property="ftpServer" size="35" /></td>
										</tr>
										<tr class="fieldevenrow">
											<td width="40%">
												<b>SFTP Port: </b><br>
												<span class="smsapp_desc">This is the port number of SFTP server/host to be polled. Default port is 22.</span>
											</td>
											<td width="60%"><html:text property="ftpPort" size="6" maxlength="10" /></td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>Target File:</b><br>
												<span class="smsapp_desc">This is the target file on SFTP server/host to be searched for / created to.</span>
											</td>
											<td width="60%"><html:text property="ftpTargetFile" size="50" maxlength="255" /></td>
										</tr>
										<tr id="smsfile1">
											<td width="40%">
												<b>SFTP SMS Auto Approval:</b><br>
												<span class="smsapp_desc">Auto approve SMS from SFTP file</span>
											</td>
											<td width="60%">
												<html:select property="smsAutoApproval">
													<html:options property="approval_value" labelProperty="approval_label" /> 
												</html:select>
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>FTP Mode:</b><br>
												<span class="smsapp_desc">This is the FTP  I/O options.</span>
											</td>
											<td width="60%">
												<html:select property="ftpMode">
													<html:option value="0">Please select.</html:option>
													<html:options property="ftpMode_value" labelProperty="ftpMode_label" /> 
												</html:select>
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>FTP Owner:</b><br>
												<span class="smsapp_desc">This is the FTP  I/O File Owner.</span>
											</td>
											<td width="60%">
												<html:select property="ftpOwner">
													<html:option value="0">Please select.</html:option>
													<html:options property="ftpOwner_value" labelProperty="ftpOwner_label" /> 
												</html:select>
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>Action Method:</b><br>
												<span class="smsapp_desc">This is the action method used on the SFTP file.</span>
											</td>
											<td width="60%">
												<html:select property="fileConversion">
													<html:option value="0">Please select.</html:option>
													<html:options property="fc_value" labelProperty="fc_label" /> 
												</html:select>
											</td>
										</tr>
										<TR>
										    <TD width="40%" vAlign=top >
										    	<b>Format:</b><br>
												<span class="smsapp_desc">FTP file format and defaulted messages (For inbound FTP only)</span>
											</TD>
										    <TD width="60%">
										    	Template ID: <html:text property="messageTemplate" readonly="true" size="3"/>
										    	<br>
										    	<INPUT onclick="setPopupTarget(this.form.messageTemplate, '1');window.open('<jsp:getProperty name="req" property="contextPath"/>/crsmsg/CannedMessage.do', 'targetMsg', 'width=650,height=500,resizable=1,scrollbars=1');" type="button" value="Template Messages">
										    	<a href="#" onclick="document.getElementById('messageTemplate').value='';">Clear</a>
										    </TD>
										</TR>
										<tr class="fieldevenrow">
											<td width="40%">
												<b>SFTP Connection Availability Status: </b><br>
												<span class="smsapp_desc">This option enables Admin to ENABLE or DISABLE the connection to SFTP server.</span>
											</td>
											<td width="60%">
												<html:checkbox property="setupStatus" value="Active" />&nbsp;Enable SFTP&nbsp;
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%" valign="top"><b>SFTP Time: </b>
												<br>
												<span class="smsapp_desc">
												This option enables Admin to automate the download/upload of FTP file from/to SFTP server.</span>
											</td>
											<td width="60%">
												<html:radio property="ftpTimeMode" value="1" onclick="toggleTimeMode(this.value, document.getElementById('divMonth'), document.getElementById('divDaily'), document.getElementById('divHour'), document.getElementById('divTime'));" />Monthly
												<html:radio property="ftpTimeMode" value="2" onclick="toggleTimeMode(this.value, document.getElementById('divMonth'), document.getElementById('divDaily'), document.getElementById('divHour'), document.getElementById('divTime'));" />Weekly
												<html:radio property="ftpTimeMode" value="3" onclick="toggleTimeMode(this.value, document.getElementById('divMonth'), document.getElementById('divDaily'), document.getElementById('divHour'), document.getElementById('divTime'));" />Hourly
												<br><br>
												<div id="divMonth" style="display:block">
													Day <html:text property="ftpDayOfMonth" maxlength="2" size="2" /> of
													<table>
													<tr>
														<td>
														<logic:present name="CrsmsgFTPSetupFormEdit" property="month_value">
														<%String month_label;%>
														<logic:iterate id="month" name="CrsmsgFTPSetupFormEdit" property="month_value" indexId="i">
															<logic:lessEqual name="i" value="5">
																<html:multibox property="ftpMonth" onclick="toggleCheckAll(this.document.CrsmsgFTPSetupFormEdit.ftpMonth, document.getElementById('checkAllBtm2'));"><bean:write name="month"/></html:multibox>&nbsp;
																<%month_label="month_label["+i+"]";%>
																<bean:write name="CrsmsgFTPSetupFormEdit" property="<%=month_label%>"/><br>
															</logic:lessEqual>
														</logic:iterate>
														</logic:present>
														</td>
														<td>
														<logic:present name="CrsmsgFTPSetupFormEdit" property="month_value">
														<%String month_label;%>
														<logic:iterate id="month" name="CrsmsgFTPSetupFormEdit" property="month_value" indexId="i">
															<logic:greaterThan name="i" value="5">
																<html:multibox property="ftpMonth" onclick="toggleCheckAll(this.document.CrsmsgFTPSetupFormEdit.ftpMonth, document.getElementById('checkAllBtm2'));"><bean:write name="month"/></html:multibox>&nbsp;
																<%month_label="month_label["+i+"]";%>
																<bean:write name="CrsmsgFTPSetupFormEdit" property="<%=month_label%>"/><br>
															</logic:greaterThan>
														</logic:iterate>
														</logic:present>
														</td>
													</tr>
													<tr><td colspan=2><br><input type="checkbox" name="checkAllBtm2" onClick="toggleCheckAll(this, this.document.CrsmsgFTPSetupFormEdit.ftpMonth);" />&nbsp;Select All Months</td></tr>
													</table>
													<br>
												</div>
												<div id="divDaily" style="display:block">
													<div class="smsapp_desc">
														<logic:present name="CrsmsgFTPSetupFormEdit" property="day_value">
														<%String day_label;%>
														<logic:iterate id="day" name="CrsmsgFTPSetupFormEdit" property="day_value" indexId="i">
															<html:multibox property="ftpDay" onclick="toggleCheckAll(this.document.CrsmsgFTPSetupFormEdit.ftpDay, document.getElementById('checkAllBtm1'));"><bean:write name="day"/></html:multibox>&nbsp;
															<%day_label="day_label["+i+"]";%>
															<bean:write name="CrsmsgFTPSetupFormEdit" property="<%=day_label%>"/><br>
														</logic:iterate>
														</logic:present>
														<br>
														<input type="checkbox" name="checkAllBtm1" onClick="toggleCheckAll(this, this.document.CrsmsgFTPSetupFormEdit.ftpDay);" />&nbsp;Select All Days
													</div>
													<br>
												</div>
												<div id="divTime" style="display:block">
													<div class="smsapp_desc">Schedule time (HHMM): <html:text property="ftpTime" size="5" /></div>
												</div>
												<div id="divHour" style="display:block">
													Duration : <html:text property="ftpHour" maxlength="5" size="5" /> Hour(s) everyday from <html:text property="ftpHourFrom" maxlength="5" size="5" /> (HHMM) to <html:text property="ftpHourTo" maxlength="5" size="5" /> (HHMM)
												</div>
												System Date & Time : <span id="servertime"></span><br>
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%" valign="top"><b>SFTP Auto Retry: </b>
												<br>
												<span class="smsapp_desc">
												This option enables Admin to automate the retry of download/upload of FTP file from/to SFTP server in the event of failure.</span>
											</td>
											<td width="60%">
											Retry No.: <html:text property="ftpRetryNo" size="3" maxlength="15"/><br>
											Retry Period: <html:text property="ftpRetryPeriod" size="3" maxlength="15" /> minute(s)
											</td>
										</tr>
										<tr class="fieldevenrow">
											<td width="40%" valign="top">
												<b>SFTP Notification Setup</b>
												<br>
												<span class="smsapp_desc">
													This option is to set the Email Notification to Admin upon completing processing the file downloaded/uploaded.</span>
												</span>
											</td>
											<td width="60%">
											Email From: <html:text property="notificationSetupSender" size="30" /><br>
											Email To: <br><font size="-1">Separate each email address with a new line. <br><b>e.g.</b><br> abc@123.com<br>def@1234.com</font><br>
											<html:textarea property="notificationSetupReceiver" cols="50" rows="10" /><br>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<html:checkbox property="notificationSetupStatus" value="Y" />&nbsp;Enable Notification&nbsp;
											</td>
										</tr>
										<tr class="fieldoddrow">
											<td width="40%">
												<b>Username: </b><br>
												<span class="smsapp_desc">This is the username to connect to SFTP server.</span>
											</td>
											<td width="60%"><html:text property="ftpUserName" /></td>
										</tr>
										<tr>
											<td><b>SFTP Key:</b>
											<br>
											Key for SFTP file transfered.
											</td>
											<td><html:file property="securityKey" /></td>
										</tr>
										<TR>
											<TD align=left><b>Schedule SMS:</b></TD>
										    <TD>
										    	<html:checkbox property="smsScheduled" value="Y" onclick="javascript:toggleLayer(document.getElementById('smsScheduled'),document.getElementById('divSched'));"/>YES
										    	<BR>
										      	<DIV id="divSched" style="DISPLAY: none">
										      		Date: <html:text property="smsScheduledTime" maxlength="20" size="20" readonly="true" /> 
													<a  ID="smsScheTimeAnchor" NAME="smsScheTimeAnchor"  href="javascript:NewCal1('smsScheduledTime','ddmmyyyy',true,24,'dropdown',true,'smsScheTimeAnchor', '<bean:write name="CrsmsgFTPSetupFormEdit" property="advanceDayLimit" />')"><img src="<jsp:getProperty name='req' property='contextPath'/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
													<BR>
													System Date & Time : <span id="servertime1"></span>
										      		<BR>
										      	</DIV>
										      	<html:hidden property="systemDateTime" />
											</TD>
										</TR>
										<tr>
											<td colspan="2" align="center">
												<html:submit property="submitBtn" value="Submit" />
												<html:reset property="reset" value="Reset" />
											</td>
										</tr>
										</table>
										</html:form>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>