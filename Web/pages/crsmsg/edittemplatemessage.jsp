<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Edit Template Message</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr align="right">
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Message Template</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%" bgcolor="ffffff">
										<tr align="center">
							              	<td>
												<html:form action="/crsmsg/SaveEditTemplateMessage.do">
													<html:hidden property="templateId"/>
													<table bgcolor="#666666" border="0">
													<tr><td class="info" colspan="2" align="center">&nbsp;<br/><b><font color="white">Edit Message</font></b><br/>&nbsp;<br/></td></tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right">Message Code</td>
														<td class="back" width="60%" align="left"><html:text property="messageCode" size="10" maxlength="20" /></td>
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right">Message Text</td>
														<td class="back" width="60%" align="left"><html:textarea property="messageText" rows="10" cols="50" /></td>
													</tr>
													<TR bgcolor="#98AFC7">
													    <TD class="hf" width="40%" align=right>Character Set:</TD>
													    <TD class="back" width="60%" align=left>
													    	<html:select property="selected_charset"> 
																<html:options property="charset_value" labelProperty="charset_label" />
															</html:select> 
														</TD>
													</TR>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right">Dynamic Field No.</td>
														<td class="back" width="60%" align="left"><html:text property="dynamicFieldNo" size="2" maxlength="2" /></td>
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right">Format of Fields (Seperate by comma)</td>
														<td class="back" width="60%" align="left"><html:text property="dynamicFieldName" size="50" maxlength="500" /></td>
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right" valign="top">Department</td>
														<td class="back" width="60%" align="left">
															<html:select property="selected_department">
																<html:option value="0">Please select.</html:option>
																<html:options property="department_value" labelProperty="department_label" /> 
															</html:select>
														</td>
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right" valign="top">Type</td>	 
														<td class="back" width="60%" align="left">
															<html:select property="selected_type">
																<html:option value="0">Please select.</html:option>
																<html:options property="type_value" labelProperty="type_label" /> 
															</html:select>
														</td>		
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="hf" width="40%" align="right" valign="top">Status</td>	 
														<td class="back" width="60%" align="left">
															<html:select property="selected_status">
																<html:option value="0">Please select.</html:option>
																<html:options property="status_value" labelProperty="status_label" /> 
															</html:select>
														</td>		
													</tr>
													<tr bgcolor="#98AFC7">
														<td class="back" width="40%" align="right"> &nbsp;
														<td class="back" width="60%" align="left">
															<html:submit property="submitBtn" value="Submit" />
															<html:reset property="reset" value="Reset" />
														</td>
													</tr>
													</table>
												</html:form>
							              	</td>
		             				 	</tr>
										</table>
		    						</td>
		  						</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>