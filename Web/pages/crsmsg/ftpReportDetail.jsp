<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>sgw - FTP Report</title>
<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  
<html:form action="/crsmsg/searchFTPReportDetail.do">

  <tbody>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="35%">
    <tr>
     <td colspan="5" class="info" colspan="1" align="center"><b>
    	Summary Report
    </b></td>
    </tr>
     <tr>
     		<td class="hf" align="center">
     			Total Sent
     		</td>
            <td class="hf" align="center">
            	Total Queue
            </td>
            <td class="hf" align="center">
            	Total Unsent
            </td>
            <td class="hf" align="center">
            	Total Pending Approval
            </td>
            <td class="hf" align="center">
            	Total Pending Verification
            </td>
     <tr>
     <tr>
     		<td class="back" align="center">
     			<bean:write name= "FtpReportDetailForm" property="totalSent"/>
     		</td>
            <td class="back" align="center">
	     		<bean:write name= "FtpReportDetailForm" property="totalQueue"/>
            </td>
            <td class="back" align="center">
            	<bean:write name= "FtpReportDetailForm" property="totalUnsent"/>
            </td>
            <td class="back" align="center">
            	<bean:write name= "FtpReportDetailForm" property="totalPendingApproval"/>
            </td>
            <td class="back" align="center">
            	<bean:write name= "FtpReportDetailForm" property="totalPendingVerification"/>
            </td>
     <tr>
    </table>
    <br><br>
    <tr>
  
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
         <tr align="right" bgcolor="ffffff">
 			
		 	<td>CSV Format : <a href="../crsmsg/FtpReportDetailGenerateCSV.do?butExport=generateCSV">Export<br>
		
			
		 	</td>
		 	</tr>	
		 	 <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>FTP Report Details</b></td>
                  </tr>
                  <tr>
                    <td class="back">
                       <logic:present name="listFTPReportDetail">
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                   <td class="hf" align="center"><b>Id</b></td>
                                   <td class="hf" align="center"><b>Mobile No</b></td>
                                   <td class="hf" align="center"><b>Message</b></td>
                                   <td class="hf" align="center"><b>Sent By</b></td>
                                   <td class="hf" align="center"><b>Date</b></td>  
									<td class="hf" align="center"><b>Mode</b></td>                                                                      
                                   <td class="hf" align="center"><b>Remark</b></td>
                                   <td class="hf" align="center"><b>Status</b></td>
                                  </tr>
                                <logic:iterate id="listFTPReport1" name="listFTPReportDetail">
								  <tr>
								     <td class="back" align="center"><bean:write name="listFTPReport1" property="seq"/></td>
	                                 <td class="back" align="center"><bean:write name="listFTPReport1" property="mobileNo"/></td>
									 <td class="back" align="center"><bean:write name="listFTPReport1" property="msg"/></td>
									 <td class="back" align="center"><bean:write name="listFTPReport1" property="sentby"/></td>
									 <td class="back" align="center"><bean:write name="listFTPReport1" property="date"/></td>
									 <td class="back" align="center">D</td>
									 <td class="back" align="center"><bean:write name="listFTPReport1" property="remark"/></td>
									 <td class="back" align="center"><bean:write name="listFTPReport1" property="status"/></td>									
                                  </tr>	
                                  
								</logic:iterate>	
						
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

</tbody>
</table>

<logic:present name="count">
<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<% Integer aa = (Integer) request.getAttribute("count") ;
%> 		

  <tr>
    <td class="back" align="center" >
 
	  <pg:pager 
	      items="<%= aa.intValue() %>"
	      index="<%= index %>"
	      maxPageItems="<%= maxPageItems %>"
	      maxIndexPages="<%= maxIndexPages %>"
	      isOffset="<%= true %>"
	      export="offset,currentPageNumber=pageNumber"
	      scope="request"
	      url="../sgw/sentLog.do">
	  <pg:param name="index"/>
	  <pg:param name="maxPageItems"/>
	  <pg:param name="maxIndexPages"/>
	  
	   <input type="hidden" name="pager.offset" value="<%= offset %>">
 
      <pg:index>
        <jsp:include page="alltheweb.jsp" flush="true"/>
      </pg:index>
  
      </pg:pager>

    </td>
  </tr>
</table>
</logic:present>

<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
