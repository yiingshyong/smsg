<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>Bulk Log</title>
<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
<logic:present name="listManualReport">
 <tr align="right" bgcolor="ffffff">
 			
		 	<td>CSV Format : <a href="../crsmsg/BulkLogSearch.do?butExport=generateCSV">Export</a><br>
		
			
		 	</td>
		 	</tr>
</logic:present>	
 <tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr> 
<html:form action="/crsmsg/BulkLogSearch.do">
 <html:hidden property="hiddenSel" />
  <tbody>
    <tr>
      <td>
        <table class="summary_tbl" align="right" border="0" cellpadding="2" cellspacing="2">
          <tbody>
            <tr class="summary_title">
              <td class="form_non_input">From</td>
					<td> 
						<html:text property="selectedFromDate" readonly="true" value="" size="20" maxlength="20" styleClass="form_input"/> 
						<a  ID="test" NAME="test"  href="javascript:NewCal1('selectedFromDate','ddmmyyyy',true,24,'dropdown',true,'test')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
					</td>
			  </td>
			  <td class="form_non_input">To</td>
				<td> 
					<html:text property="selectedUntilDate" readonly="true" value="" size="20" maxlength="20" styleClass="form_input"/>
					<a ID="test1" NAME="test1" href="javascript:NewCal1('selectedUntilDate','ddmmyyyy',true,24,'dropdown',true,'test1')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
				</td>
			  </td>
			  <td>
			   <html:submit value="Search" property="butSubmit" /> 
			  </td>
            </tr>
          </tbody>
        </table>

      </td>    
    </tr>
    

    <tr>

    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
          
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>Bulk Send Log</b></td>
                  </tr>
                  <tr>
                    <td class="back">
                       <logic:present name="listManualReport">
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr align="right" bgcolor="ffffff">
 			
		 	<td><input value="Approve Selected Record(s)" name="butApprove" type="submit">&nbsp 
		 	<!--<input value="Delete Record(s)" name="butDelete" type="submit">-->
		
			
		 	</td>
		 	</tr>	
		 	 <tr align="right" bgcolor="ffffff">
 			
		 	<td>&nbsp
		
			
		 	</td>
		 	</tr>	
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                   <td class="hf" align="center"><b>Id</b></td>
                                   <td class="hf" align="center"><b>File Name</b></td>
                                   <td class="hf" align="center"><b>Date</b></td>
                                   <td class="hf" align="center"><b>Start Time</b></td>
                                   <td class="hf" align="center"><b>End Time</b></td>                                   
                                   <td class="hf" align="center"><b>Sent</b></td>
                                   <td class="hf" align="center"><b>Unsent</b></td>   
                                   <td class="hf" align="center"><b>Pending Verification</b></td>   
                                   <td class="hf" align="center"><b>Pending Approval</b></td>   
                                   <td class="hf" align="center"><b>Queue </b></td>   
                                   <td class="hf" align="center"><b>Bad Record</b></td>  
                                    <td class="hf" align="center"><b>Remark</b></td>    
                                   <td class="hf" align="center"><input type=checkbox name="checkAll" onclick="checkAllCheckboxes(this);"/></td>                               
                                  </tr>
                                <logic:iterate id="listManualReport1" name="listManualReport">
                                
                  
								  <tr>
								 
								     <td class="back" align="center"><bean:write name="listManualReport1" property="id"/></td>
	                                 <td class="back" align="center"><bean:write name="listManualReport1" property="filteredFileName"/></td>
									 <td class="back" align="center"><bean:write name="listManualReport1" property="startDate"/></td>
									 <td class="back" align="center"><bean:write name="listManualReport1" property="startTime"/></td>
									 <td class="back" align="center"><bean:write name="listManualReport1" property="endTime"/></td>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSSent.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">	
									 <td class="back" align="center"><bean:write name="listManualReport1" property="sent"/></td>
									 </a>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSUnsent.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">	
									 <td class="back" align="center"><bean:write name="listManualReport1" property="unsent"/></td>
									 </a>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSPendingVerification.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">	
									 <td class="back" align="center"><bean:write name="listManualReport1" property="pendingVer"/></td>
									 </a>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSPendingApproval.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">	
									 <td class="back" align="center"><bean:write name="listManualReport1" property="pendingApp"/></td>	
									 </a>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSQueue.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">	
									 <td class="back" align="center"><bean:write name="listManualReport1" property="queue"/></td>		
									 </a>
									 <a href="#" onclick="javascript:popUp('../crsmsg/searchSMSInvalid.do?refId=<bean:write name="listManualReport1" property="id"/>');return false;">
									 <td class="back" align="center"><bean:write name="listManualReport1" property="noFailed"/></td>
									 </a>						 									 
									 <td class="back" align="center"><textarea rows="5" cols="30"><bean:write name="listManualReport1" property="remark"/></textarea></td>
									 <td class="back" align="center"><html:multibox property="selectedFtp"><bean:write name="listManualReport1" property="id"/></html:multibox></td>
							      
                                  </tr>	
                                
								</logic:iterate>	
						
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

</tbody>
</table>

<logic:present name="count">
<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<% Integer aa = (Integer) request.getAttribute("count") ;

%> 		

  <tr>
    <td class="back" align="center" >
 
	  <pg:pager 
	      items="<%= aa.intValue() %>"
	      index="<%= index %>"
	      maxPageItems="<%= maxPageItems %>"
	      maxIndexPages="<%= maxIndexPages %>"
	      isOffset="<%= true %>"
	      export="offset,currentPageNumber=pageNumber"
	      scope="request"
	      url="../crsmsg/sentLog.do">
	  <pg:param name="index"/>
	  <pg:param name="maxPageItems"/>
	  <pg:param name="maxIndexPages"/>
	  
	   <input type="hidden" name="pager.offset" value="<%= offset %>">
 
      <pg:index>
        <jsp:include page="alltheweb.jsp" flush="true"/>
      </pg:index>
  
      </pg:pager>

    </td>
  </tr>
</table>
</logic:present>

<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
