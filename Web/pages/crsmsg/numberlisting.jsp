<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title></title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<table border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr><td align="center"  class="info" colspan="2"><b>Number Listing</b></td></tr>
		<!--
		<tr>
			<td width="30%" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/searchContacts.do">Search Contacts</a></td>
			<td class="plain">&nbsp;</td>
		</tr>
		-->
		</table>
		<hr/>
		<table border="0" width="80%">
		<tr>
			<td align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/NumberListing.do?searchkey=Personal&selectedState=Type">My List</font></a></td>
			<td align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/NumberListing.do?searchkey=Group&selectedState=Type">Preset List</a></td>
		</tr>
		</table>
		<br/>
		<html:form action="/crsmsg/GetAddBookSearch.do" focus="searchkey">
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table border="0" cellpadding="8" cellspacing="1" width="100%">
				<tr>
					<td class="hf" align="center"><b>No</b></td>
					<td class="hf" align="center"><b>Name</b></td>
					<td class="hf" align="center"><b>Cust Ref No.</b></td>
					<td class="hf" align="center"><b>Number</b></td>
					<td class="hf" align="center"><b>Designation</b></td>
					<td align="center" class="hf" width="5%"><input type="checkbox" name="checkAllTop" onClick="toggleCheckAll(this, this.document.CrsmsgAddressBookSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllBtm'))" /></td>
				</tr>
				<logic:present name="CrsmsgAddressBookSearchForm" property="resultList">
					<logic:notEmpty name="CrsmsgAddressBookSearchForm" property="resultList">
						<bean:define id="result" name="CrsmsgAddressBookSearchForm" property="resultList" />
						<logic:iterate id="aRecord" name="result" property="record" indexId="i">
							<tr bgcolor="#98AFC7">
								<td class="back" align="center"><%=i+1%></td>
								<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
									<logic:greaterThan name="j" value="0">
										<td class="back" align="center"><bean:write name="aColumn"/></td>
									</logic:greaterThan>												
								</logic:iterate>    
								<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgAddressBookSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));toggleCheckAll(this.document.CrsmsgAddressBookSearchForm.checkedRecords, this.document.getElementById('checkAllBtm'))"><bean:write name='aRecord' property='column[3]' /></html:multibox></td>
							</tr>
						</logic:iterate>
					</logic:notEmpty>
				</logic:present>
				</table>
			</td>
		</tr>
		</table>
		<table border="0" width="80%">
		<tr>
			<td align="right" class="plain">
			<font size="-1">Select all:</font>
			<input type="checkbox" name="checkAllBtm" value="Select All" onClick="toggleCheckAll(this, this.document.CrsmsgAddressBookSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllTop'))" />
			<input type="button" name="Done" value="Done" onClick="updatePopupTarget(opener.getPopupTarget(), this.document.CrsmsgAddressBookSearchForm.checkedRecords);window.close();">
			</td>
		</tr>
		</table>
		</html:form>
		<input type="button" onClick="window.close()" value="Cancel" />
	</body>
	<!-- body end -->
</html:html>