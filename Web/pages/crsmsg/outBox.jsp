<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>sgw - Sent Log</title>
  <link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
  
</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

 <tr align="right" bgcolor="ffffff">
 			
		 	<td>CSV Format : <a href="../crsmsg/outboxGenerateCSV.do">Export<br>
		
			
		 	</td>
		 	</tr>	
	 <tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr> 	
<tr> 
<html:form action="/crsmsg/outboxSearch.do">
 <html:hidden property="hiddenSel" />
  <tbody>
    <tr>
      <td>
        <table class="summary_tbl" align="right" border="0" cellpadding="2" cellspacing="2">
          <tbody>
            <tr class="summary_title">
            <td class="form_non_input">From</td>
					<td> 
						<html:text property="selectedFromDate" readonly="true" value="" size="20" maxlength="20" styleClass="form_input"/> 
						<a  ID="test" NAME="test"  href="javascript:NewCal1('selectedFromDate','ddmmyyyy',true,24,'dropdown',true,'test')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
					</td>
			  </td>
			  <td class="form_non_input">To</td>
				<td> 
					<html:text property="selectedUntilDate" readonly="true" value="" size="20" maxlength="20" styleClass="form_input"/>
					<a ID="test1" NAME="test1" href="javascript:NewCal1('selectedUntilDate','ddmmyyyy',true,24,'dropdown',true,'test1')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
				</td>
			  </td>
              <td>
               Search :<input type="text" name="searchkey" value="" class="form_input" > 
	           <html:select property="selectedState" styleClass="form_input">
               <html:options property="searchby_value" labelProperty="searchby_label" styleClass="form_input"/> 
               </html:select>  
               <input value="Search" name="butSearch" type="submit"> 
               <input type="reset" value="Reset" class="form_button">
			  </td>
            </tr>
          </tbody>
        </table>

      </td>    
    </tr>
    
    <logic:present name="count">
		<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<% Integer aa = (Integer) request.getAttribute("count") ;
			if (aa <= 20 ){ %>
			
			<style type="text/css">
			<!--
			.resultInfo {
			   color:#f80;
			   background-color: transparent;
			   text-transform:Uppercase;
			   padding: 5px 5px 5px 0px;
			   margin: 0;
			   font-size: 1em;
			}
			.rnav {
			    padding: 0;
			    font-family: Verdana, Arial, Helvetica, Sans-serif;
			    font-size: 1em;
			    color:#333;
			    background-color:#fff;
			    font-weight:bold; 
			    font-size: 1em;
			}
			.rnavLabel {
			    text-transform: Uppercase;
			    color:#f80;
			    background-color: transparent;
			}
			a.rnavLink {
			    color: #415481;
			    background-color: transparent;
			}
			a:visited.rnavLink {
			    color: #8A9CBD;
			    background-color: transparent;
			}
			a:hover.rnavLink {
			    color: #f80;
			    text-decoration: none;
			    background-color: transparent;
			}
			-->
			</style>
			<tr>
		    <td class="back" align="center" >
			
			<div class="resultInfo">
			Displaying results: <strong><%
							out.print(aa);
						%></strong> 
			</div>
			
			</td>
			</tr>
			<%
			}else{
		%> 		

		
		  <tr>
		    <td class="back" align="center" >
		 
			  <pg:pager 
			      items="<%= aa.intValue() %>"
			      index="<%= index %>"
			      maxPageItems="<%= maxPageItems %>"
			      maxIndexPages="<%= maxIndexPages %>"
			      isOffset="<%= true %>"
			      export="offset,currentPageNumber=pageNumber"
			      scope="request"
			      url="../crsmsg/outboxSearch.do">
			  <pg:param name="index"/>
			  <pg:param name="maxPageItems"/>
			  <pg:param name="maxIndexPages"/>
			  
			   <input type="hidden" name="pager.offset" value="<%= offset %>">
		 
		      <pg:index>
		        <jsp:include page="alltheweb.jsp" flush="true"/>
		      </pg:index>
		  
		      </pg:pager>
		
		    </td>
		  </tr>
		  <%}%>
		</table>
	</logic:present>
	<TABLE align="center" width="90%" border=0>
  <TBODY>

  <TR align=left>
 
		    <TH width="18%" bgcolor="#98AFC7">Pending Message(s):</TH>
		    <TD width="10%" bgcolor="#98AFC7"><% out.print(request.getAttribute("pendingCount"));%></TD>
		    <TD class=plain width=*></TD></TR>
		 <TR align=left>
		    <TH width="18%" bgcolor="#98AFC7">Pending Approval Message(s):</TH>
		    <TD width="10%" bgcolor="#98AFC7"><% out.print(request.getAttribute("approvalCount")) ;%></TD>
		    <TD class=plain width=*></TD></TR>
		  <TR align=left>
		    <TH width="18%" bgcolor="#98AFC7">Failed Message(s):</TH>
		    <TD width="10%" bgcolor="#98AFC7"><% out.print(request.getAttribute("failedCount")) ;%></TD>
		    <TD class=plain width=*></TD></TR>
		  <TR align=left>
		    <TH width="18%" bgcolor="#98AFC7">Sent Message(s):</TH>
		    <TD width="10%" bgcolor="#98AFC7"><% out.print(request.getAttribute("sentCount")) ;%></TD>
		    <TD class=plain width=*></TD></TR></TBODY>
    </TABLE><BR>
<br>
   
    <tr>
  
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>Outbox</b></td>
                  </tr>
                  <tr>
                    <td class="back">
                       
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                    <td class="hf" align="center"><b>No</b></td>
                                    <td class="hf" align="center"><b>Sent Date</b></td>
                                    <td class="hf" align="center"><b>Created Date</b></td>                                   
                                    <td class="hf" align="center"><b>Mobile Number</b></td>
                                    <td class="hf" align="center"><b>Message</b></td>
                                    <td class="hf" align="center"><b>Sent By</b></td>
                                     <td class="hf" align="center"><b>Approval</b></td>
                                     <td class="hf" align="center"><b>Approval Date</b></td>
                                    <td class="hf" align="center"><b>Department</b></td>
                                     <td class="hf" align="center"><b>Telco</b></td>
                                     <td class="hf" align="center"><b>Type</b></td>
                                     <td class="hf" align="center"><b>Priority</b></td>
                                    <td class="hf" align="center"><b>Status</b></td>
                                    <td class="hf" align="center"><b>Scheduled Date</b></td>
                                    <td class="hf" align="center"><b>Remark</b></td>
                                  </tr>
                                  <logic:present name="outBox">
                                <logic:iterate id="outBox1" name="outBox">
								  <tr>
								    <td class="back" align="left"><bean:write name="outBox1" property="seq"/></td>
                                  	<td class="back" align="left"><bean:write name="outBox1" property="date"/></td> 
                              		<td class="back" align="left"><bean:write name="outBox1" property="created"/></td>                                   	 
									<td class="back" align="left"><bean:write name="outBox1" property="mobileNo"/></td>  
                                    <td class="back" align="left"><bean:write name="outBox1" property="message"/></td>  
                                    <td class="back" align="left"><bean:write name="outBox1" property="sentBy"/></td>  
                                    <td class="back" align="left"><bean:write name="outBox1" property="approval"/></td>  
                                    <td class="back" align="left"><bean:write name="outBox1" property="approvalDatetime"/></td>
                                    <td class="back" align="left"><bean:write name="outBox1" property="dept"/></td>   
                                    <td class="back" align="left"><bean:write name="outBox1" property="operator"/></td>   
                                    <td class="back" align="left"><bean:write name="outBox1" property="type"/></td>
                                    <td class="back" align="left"><bean:write name="outBox1" property="priority"/></td>   
                                    <td class="back" align="left"><bean:write name="outBox1" property="status"/></td>
                                     <td class="back" align="left"><bean:write name="outBox1" property="scheduled"/></td>
                                    <td class="back" align="left"><bean:write name="outBox1" property="remarks"/></td>  
                                   </tr>	
								</logic:iterate>	
						</logic:present>
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
  

</tbody>
</table>

<logic:present name="count">
<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<% Integer aa = (Integer) request.getAttribute("count") ;
if(aa>20){
%> 		

  <tr>
    <td class="back" align="center" >
 
	  <pg:pager 
	      items="<%= aa.intValue() %>"
	      index="<%= index %>"
	      maxPageItems="<%= maxPageItems %>"
	      maxIndexPages="<%= maxIndexPages %>"
	      isOffset="<%= true %>"
	      export="offset,currentPageNumber=pageNumber"
	      scope="request"
	      url="../crsmsg/outboxSearch.do">
	  <pg:param name="index"/>
	  <pg:param name="maxPageItems"/>
	  <pg:param name="maxIndexPages"/>
	  
	   <input type="hidden" name="pager.offset" value="<%= offset %>">
 
      <pg:index>
        <jsp:include page="alltheweb.jsp" flush="true"/>
      </pg:index>
  
      </pg:pager>

    </td>
  </tr>
  <%}%>
</table>
</logic:present>

<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
