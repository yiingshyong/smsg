<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@ page import="javax.servlet.http.HttpServletRequest" %>
<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "'");
}
// End -->

function checkDate(form){

var startDate= form.selectedFromDate;
var endDate = form.selectedUntilDate;

if (endDate.value != "" || startDate.value != ""){

	if (startDate.value != "" && endDate.value == "") {
		alert ("Please provide End date!");
		return false;
	}
	
	if (startDate.value == "" && endDate.value != "") {
		alert ("Please provide From date!");
		return false;
	}
	
	var compares = compareDates(startDate.value, "dd/MM/yyyy", endDate.value, "dd/MM/yyyy");
	
	if (compares == 1){
		alert ("End date cannot be earlier than Start Date");
		return false;
	}
}

form.submit();
}

function todayStr() {
var today=new Date();
var currentMo = today.getMonth()+1;
var currentDay = today.getDate();
if (currentMo < 10) {
currentMo = '0' + currentMo;
}

if (currentDay < 10) {
currentDay = '0' + currentDay;
}


return currentDay+"/"+currentMo+"/"+(today.getYear() )
}
</script>
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>CRSMSG - Billing Report</title>
  <link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/date.js"></SCRIPT>
  
</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
	<tr><td>&nbsp;</td></tr>	
	<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
	
	<tr align="right" bgcolor="ffffff">	
		<td>CSV Format : <a href="../crsmsg/OutgoingBillingReportGenerateCSV.do">Export<br>
		</td>
	</tr>	

	<tr> 
<html:form action="/crsmsg/OutgoingBillingReportSearch.do">
 <html:hidden property="hiddenSel" />
  <tbody>
    <tr>
      <td>
        <table class="summary_tbl" align="right" border="0" cellpadding="2" cellspacing="2">
          <tbody>
            <tr class="summary_title">
            	<td class="form_non_input">From</td>
					<td> 
						<html:text property="selectedFromDate" readonly="true" value="" size="17" maxlength="20" styleClass="form_input"/> 
						<a  ID="test" NAME="test"  href="javascript:NewCal1('selectedFromDate','ddmmyyyy',false,24,'dropdown',true,'test')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
					</td>
			  	</td>
			  <td class="form_non_input">To</td>
				<td> 
					<html:text property="selectedUntilDate" readonly="true" value="" size="17" maxlength="20" styleClass="form_input"/>
					<a ID="test1" NAME="test1" href="javascript:NewCal1('selectedUntilDate','ddmmyyyy',false,24,'dropdown',true,'test1')"><img src="<jsp:getProperty name="req" property="contextPath"/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
				</td>
			  </td>
			   <td>
				  <logic:equal name="BillingReportForm" property="admin" value="true">
		               Search :<input type="text" name="searchkey" value="" class="form_input" > 
			           <html:select property="selectedState" styleClass="form_input">
		               <html:options property="searchby_value" labelProperty="searchby_label" styleClass="form_input"/> 
		               </html:select>  
		               <html:select property="selectedServer" styleClass="form_input">
       				   <html:options property="searchbyserver_value" labelProperty="searchbyserver_label" styleClass="form_input"/> 
               		   </html:select>
				  </logic:equal>	
				  <INPUT class=form_button type="button" value="Search" onClick="checkDate(this.form)" > 
	               <input type="reset" value="Reset" class="form_button">		
	            </td>  
            </tr>
          </tbody>
        </table>

      </td>    
    </tr>

    

<br>

    <tr>
  <TABLE width="90%" border=0>
  <TBODY>
   <TR align=left>
			<td></td>
			
			<logic:equal name="BillingReportForm" property="status" value="I">                    
            	<TD width="15%" bgcolor="#98AFC7" align="center"><B> Total SMS Receive</B></TD>
            </logic:equal>
			<logic:notEqual name="BillingReportForm" property="status" value="I">
				<TD width="15%" bgcolor="#98AFC7" align="center"><B> Total SMS Sent</B></TD>
			</logic:notEqual>	
		
			<TD width="15%" bgcolor="#98AFC7" align="center"><B>Total Cost (RM)</B></TD>
			<TD width="15%" bgcolor="#98AFC7" align="center"><B>Total GST (RM)</B></TD>
			<TD width="15%" bgcolor="#98AFC7" align="center"><B>Grand Total with GST (RM)</B></TD>
		</tr>
	 	<logic:present name="billingReportSummary">
	    	<logic:iterate id="header" name="headerList">
	    		<tr>
				  	<td width="18%" bgcolor="#98AFC7" align="center"><b>
				  		<bean:write name="header" property="header"/>
				  	</b></td>
					<td width="18%" bgcolor="#98AFC7" align="right"><b>
				  		<bean:write name="header" property="totalCharge_SMSSent"/>
					</b></td>
					<td width="18%" bgcolor="#98AFC7" align="right"><b>
				  		<bean:write name="header" property="totalCharge_Cost"/>
					</b></td>
					<td width="18%" bgcolor="#98AFC7" align="right"><b>
				  		<bean:write name="header" property="totalCharge_Gst"/>
					</b></td>
					<td width="18%" bgcolor="#98AFC7" align="right"><b>
				  		<bean:write name="header" property="totalCharge_All"/>
					</b></td>
				</tr>
			</logic:iterate>	
		</logic:present>	

		<TR align=left>
		    <Td width="18%" bgcolor="#98AFC7" align="center"><b>Grand Total</b></Td>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="BillingReportForm" property="grandTotal_SMSSent"/></b></TD>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="BillingReportForm" property="grandTotal_Cost"/></b></TD>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="BillingReportForm" property="grandTotal_Gst"/></b></TD>
			<TD width="15%" bgcolor="#98AFC7" align="right"><b><bean:write name="BillingReportForm" property="grandTotal_All"/></b></TD>
		    <TD class=plain width=*></TD>
		</TR>
		</TBODY></TABLE><BR>
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
       
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>
            	        <logic:equal name="BillingReportForm" property="status" value="I">                    
        	            	Incoming Billing Report Details
    	                </logic:equal>
						<logic:notEqual name="BillingReportForm" property="status" value="I">
							Billing Report Details
						</logic:notEqual>	
                    </b>
                    </td>
                  </tr>
                  <tr>
                    <td class="back">

                       <logic:present name="billingReportSummary">
                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                    <td class="hf" align="center" rowspan=3><b>No</b></td>
                                    <td class="hf" align="center" rowspan=3><b>Department</b></td>
                                    <td class="hf" align="center" rowspan=3><b>User Id</b></td>
                                    <td class="hf" align="center" rowspan=3><b>Channel Id</b></td>
                                    <td class="hf" align="center" colspan="<bean:write name="rateSize"/>">
                                    	<b>
                                    		<logic:equal name="BillingReportForm" property="status" value="I">                    
					        	            	SMS Received
					    	                </logic:equal>
											<logic:notEqual name="BillingReportForm" property="status" value="I">
												SMS Sent
											</logic:notEqual>	
                                    		
                                    	</b>
                                    </td>                                   
                                    <td class="hf" align="center" colspan="<bean:write name="gstRateSize"/>"><b>Cost (RM)</b></td>
                                  </tr>
                                  <tr>
        	                        <logic:iterate id="header" name="telcoList">
									  	<td class="hf" align="center" colspan="<bean:write name="header" property="colspan"/>"><b>
									  		<bean:write name="header" property="name"/>
									  	</b></td>
									</logic:iterate>
									<td class="hf" align="center" rowspan="2"><b>Total</b></td>
        	                        <logic:iterate id="header" name="telcoList">
									  	<td class="hf" align="center" colspan="<bean:write name="header" property="gstColspan"/>"><b>									  	
									  		<bean:write name="header" property="name"/>
									  	</b></td>
									</logic:iterate>
									<td class="hf" align="center" rowspan="2"><b>Total (include GST)</b></td>
									</tr>
									<tr>	
        	                        <logic:iterate id="header" name="rateNameList">
									  	<td class="hf" align="center"><b>
									  		<bean:write name="header" property="name"/>
									  	</b></td>
									</logic:iterate>
									<logic:iterate id="header" name="costNameList">
									  	<td class="hf" align="center"><b>
									  		<bean:write name="header" property="name"/>
									  	</b></td>
									</logic:iterate>	
									<td class="hf" align="center"><b>GST</b></td>
								  </tr>	
                                <logic:iterate id="summary" name="billingReportSummary">
								  <tr>
								  	<td class="back" align="left"><bean:write name="summary" property="seq"/></td>
								   <!-- <a href="#" onclick="javascript:popUp('<jsp:getProperty name="req" property="contextPath"/>/crsmsg/BillingReportDetailsSearch.do?startDate=<bean:write name="BillingReportForm" property="startDate"/>&endDate=<bean:write name="BillingReportForm" property="endDate"/>&dept=<bean:write name="summary" property="dept"/>');return false;"> -->
								    	<td class="back" align="left"><bean:write name="summary" property="dept"/></td>
								    	<td class="back" align="left"><bean:write name="summary" property="userId"/></td>
								    	<td class="back" align="left"><bean:write name="summary" property="channelId"/></td>
                                  	<!-- </a> -->
                                  	<logic:iterate id="charge" property="telcoModal" name="summary">
										<td class="back" align="center"><bean:write name="charge" property="totalCharge"/></td> 
									</logic:iterate>	
									
                                   </tr>	
								</logic:iterate>	
						
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>


</tbody>
</table>

<br>

</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
