<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
<head>
<title>Welcome to SMS</title>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>

<script type="text/javascript">
var err_ind ="<html:errors/>";

</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">

<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Queue Listener Maintenance&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors bundle="crsmsg"/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/crsmsg/QueueListenerSave.do" styleClass="test" >
<html:hidden property="mq.mqId"/>
<tr valign="top" >
<td class=form_non_input width=20%>SMS Type&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="mq.value" size="50" maxlength="20" styleClass="form_input" readonly="true"/></td>
</tr>
<tr valign="top" >
<td class=form_non_input width=20%>Service Provider&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="mq.telcoName" size="50" maxlength="1" styleClass="form_input" readonly="true"/></td>
</tr>	

<tr valign="top" >
<td class=form_non_input width=20%>Queue Name&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="mq.queueName" size="50" maxlength="1" styleClass="form_input" readonly="true"/></td>
</tr>														
	
<tr class=form_non_input>		
<td>Number of thread</td>
<td>:</td>
<td><html:text property="mq.noOfThread" size="50" maxlength="2" styleClass="form_input limitInt" /></td>
</tr>

<tr>
<td colspan="3">
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center>SMS Type</td>
<td align=center>Service Provider</td>
<td align=center>Queue Name</td>
<td align=center>Number of Thread</td>
</tr>

<logic:present name="QueueListenerCfgForm" property="mqs">
<logic:iterate id="obj" name="QueueListenerCfgForm" property="mqs">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<td><a href="../crsmsg/QueueListenerListing.do?id=<bean:write name="obj" property="mqId"/>" ><bean:write name="obj" property="value"/></a></td>
<td align=left><bean:write name="obj" property="telcoName"/></td>
<td align=left><bean:write name="obj" property="queueName"/></td>
<td align=left><bean:write name="obj" property="noOfThread"/></td>
</tr>
</logic:iterate>
</logic:present>
</table>																
</td>
</tr>


<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" name="actiontype" value="Save" class="form_button">
&nbsp;&nbsp;
<input type="button" value="Reset" class="form_button">
&nbsp;&nbsp;
<input type=button value="Back To System Setting" onClick="javascript:window.location.href='../crsmsg/SystemCfg.do?'" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			
 
</body>
</html:html>