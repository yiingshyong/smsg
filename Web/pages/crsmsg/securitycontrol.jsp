<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Security Control</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/formvalidation.js"></script>
		<script language="JavaScript">
			function ValidateForm(){
				var status=true;
				status=validIpAddress(document.getElementById("ip").value);
				return status;
			}
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		
		
		
		
		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				
				
				
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Security Control</b></td></tr>
						<tr align="right" bgcolor="ffffff">		
							<td>CSV Format : <a href="../crsmsg/IPAddressGenerateCSV.do">Export<br>		 	
						</tr>
						<tr>
							<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/GetSecurityControlSearch.do" focus="searchkey">
								<table border="0" width="90%">
								<tr>
									<td width="10%" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/AddIP.do">New IP</a></td>
									<td class="plain" width="80%" align="right">
										Search:
										<html:text property="searchkey" styleClass="search_input"/>
										&nbsp;By&nbsp;
										<html:select property="selectedState" styleClass="search_select">
											<html:options property="searchby_value" labelProperty="searchby_label" /> 
										</html:select>
										<html:submit property="submitBtn" value="Search" styleClass="search_btn" />
									</td>
								</tr>
								</table>
								<br>
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<tr>
											<td align="center" class="hf" width="5%"><b>No</b></td>
											<td align="center" class="hf" width="12%"><b>IP Address</b></td>
											<td align="center" class="hf" width="12%"><b>Created By</b></td>
											<td align="center" class="hf" width="12%"><b>Created Date</b></td>
											<td align="center" class="hf" width="10%"><b>Modified By</b></td>
											<td align="center" class="hf" width="12%"><b>Modified Date</b></td>
											<td align="center" class="hf" width="10%"><b>Department</b></td>
											<td align="center" class="hf" width="10%"><b>System</b></td>
											<td align="center" class="hf" width="15%"><b>Remarks</b></td>
											<!-- <td align="center" class="hf" width="7%"><b>Status</b></td> -->
											<td align="center" class="hf" width="5%"><input type="checkbox" name="checkAllTop" onClick="toggleCheckAll(this, this.document.CrsmsgSecurityControlSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllBtm'))" /></td>
										</tr>
										<logic:present name="CrsmsgSecurityControlSearchForm" property="resultList">
										<logic:notEmpty name="CrsmsgSecurityControlSearchForm" property="resultList">
										<bean:define id="result" name="CrsmsgSecurityControlSearchForm" property="resultList" />
										<logic:iterate id="aRecord" name="result" property="record" indexId="i">
										<tr bgcolor="#98AFC7">
											<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditIP.do?id=<bean:write name='aRecord' property='column[0]' />"><%=i+1%></a></td>
											<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
												<logic:equal name="j" value="1">
													<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditIP.do?id=<bean:write name='aRecord' property='column[0]' />"><bean:write name="aColumn"/></a></td>
												</logic:equal>
												<logic:greaterThan name="j" value="1">
													<td class="back" align="center"><bean:write name="aColumn"/></td>
												</logic:greaterThan>												
											</logic:iterate>    
											<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgSecurityControlSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));toggleCheckAll(this.document.CrsmsgSecurityControlSearchForm.checkedRecords, this.document.getElementById('checkAllBtm'))"><bean:write name='aRecord' property='column[0]' /></html:multibox></td>
										</tr>
										</logic:iterate>
										</logic:notEmpty>
										</logic:present>
										</table>
									</td>
								</tr>
								</table>
								</html:form>
								<table border=0 width="100%" cellpadding="10">
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="9" align="right">
										<html:form action="/crsmsg/DeleteIP.do">
											Select all:	
											<input type="checkbox" name="checkAllBtm" onClick="toggleCheckAll(this, this.document.CrsmsgSecurityControlSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllTop'))" />
											<html:submit property="deleteBtn" value="Delete Selected IP(s)" onclick="if(confirmDeleteButton('IP(s)')){return copyFormElement(this.form, this.document.CrsmsgSecurityControlSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden');}else{return false;}" />
										</html:form>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>