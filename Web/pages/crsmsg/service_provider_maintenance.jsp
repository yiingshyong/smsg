<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
<head>
<title>Welcome to SMS</title>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.smsRateLookups').click(function(){
		var delim = "|";
		var selectedId = $(this).text();
		var list = $('#smsRateList').val().split(delim);
		if($.inArray(selectedId, list) == -1){
			list.push(selectedId);
			list = list.sort().join(delim);
			if(list.indexOf(delim) == 0){
				list = list.substr(1);
			}
			$('#smsRateList').val(list);
		}
	});

	$('.clearField').click(function(){
		$('#smsRateList').val('');
	});
});
var err_ind ="<html:errors/>";
</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">

<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Service Provider Maintenance&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors bundle="crsmsg"/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/crsmsg/ServiceProviderMaintenanceSave.do" styleClass="test" >
<html:hidden property="svcProvider.id" styleClass="abc"/>
<tr valign="top" >
<td class=form_non_input width=20%>Name&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="svcProvider.name" size="50" maxlength="20" styleClass="form_input" /></td>
</tr>
<tr valign="top" >
<td class=form_non_input width=20%>Default SMS Rate&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td>
 <html:select property="svcProvider.defaultSmsRateId" styleClass="form_input">
		<html:optionsCollection  styleClass="form_input" property="smsRates" label="name" value="value"/>
</html:select> 
</td>
</tr>
<tr>
<td class=form_non_input width=20%>Outgoing SMS Rate List&nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="svcProvider.smsRateIds" size="50" maxlength="20" styleClass="form_input integerKeyIn" readonly="true" styleId="smsRateList"/>
<a class="clearField">Clear</a>
</td>
</tr>
<tr valign="top" >
<td class=form_non_input width=20%>Incoming GST Charges(%) &nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="svcProvider.incomingGstCharge" size="15" maxlength="20" styleClass="form_input" /></td>
</tr>
<tr valign="top" >
<td class=form_non_input width=20%>Outgoing GST Charges(%) &nbsp;<span class="remarks">*</span></td>
<td width=1%>:</td>
<td><html:text property="svcProvider.outgoingGstCharge" size="15" maxlength="20" styleClass="form_input" /></td>
</tr>
<tr>
<td colspan="3">
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center colspan="2">Available SMS Rates Listing</td>
</tr>
<tr class=index_row13>
<td align=center>Id</td>
<td align=center>Name</td>
</tr>

<logic:present name="ServiceProviderMaintenanceForm" property="smsRates">
<logic:iterate id="obj" name="ServiceProviderMaintenanceForm" property="smsRates">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<td><a href="#" onclick="" class="smsRateLookups"><bean:write name="obj" property="value"/></a></td>
<td align=left><bean:write name="obj" property="name"/></td>
</tr>
</logic:iterate>
</logic:present>
</table>																
</td>
</tr>

<tr>
<td colspan="3">
<table valign=top width="100%" border="0" cellpadding="2" cellspacing=0 class=table align="center" style="border-collapse:collapse;">
<tr class=index_row13>
<td align=center>Name</td>
<td align=center>Outgoing Rate List</td>
<td align=center>Default SMS Rate</td>
<td align=center>Incoming GST Charges(%)</td>
<td align=center>Outgoing GST Charges(%)</td>
</tr>

<logic:present name="ServiceProviderMaintenanceForm" property="svcProviders">
<logic:iterate id="obj" name="ServiceProviderMaintenanceForm" property="svcProviders">
<tr class=index_row8 align=center onMouseOver="this.style.background='#EEEEEE'"  onMouseOut="this.style.background='#FFF7E5'">
<td><a href="../crsmsg/ServiceProviderMaintenanceListing.do?svcProvider.id=<bean:write name="obj" property="id"/>" ><bean:write name="obj" property="name"/></a></td>
<td align=left><bean:write name="obj" property="smsRateIds"/></td>
<td align=left><bean:write name="obj" property="defaultSmsRateId"/></td>
<td align=left><bean:write name="obj" property="incomingGstCharge"/></td>
<td align=left><bean:write name="obj" property="outgoingGstCharge"/></td>
</tr>
</logic:iterate>
</logic:present>
</table>																
</td>
</tr>


<tr><td colspan=3 height=10></td></tr>
<tr class="form_non_input">				
<td colspan=3 align=center>				
<input type="submit" name="actiontype" value="Save" class="form_button">
&nbsp;&nbsp;
<input type=button value="Delete" onClick="javascript:window.location.href='../crsmsg/ServiceProviderMaintenanceDelete.do?svcProvider.id=<bean:write property="svcProvider.id" name="ServiceProviderMaintenanceForm"/>'" class="form_button">
&nbsp;&nbsp;
<input type="button" value="Reset" class="form_button">
&nbsp;&nbsp;
<input type=button value="Back To System Setting" onClick="javascript:window.location.href='../crsmsg/SystemCfg.do?'" class="form_button">
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			
 
</body>
</html:html>