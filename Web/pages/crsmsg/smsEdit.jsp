<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title></title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetimepicker.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/AnchorPosition.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/datetime.js"></script>
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/formvalidation.js"></script>
		<script language="JavaScript">
			function ValidateForm(){
				var status=true;
				status=validMobileNo(document.getElementById("mobileNo").value);
				return status;
			}
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body onload="initCharCounter('<bean:write name="EditSMSForm" property="concatenateSMS" />', '<bean:write name="EditSMSForm" property="maxNoOfSMS" />', '<bean:write name="EditSMSForm" property="charPerSms" />');populateMaxCharLimit(document.getElementById('selected_charset'), document.getElementById('charLeftField'));">
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<html:form action="/crsmsg/editSms.do" onsubmit="return ValidateForm()">
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>SMS Edit</b></td></tr>
						<tr>
							<td bgcolor=ffffff align="center">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<TR>
											<TD class="hf" align=right width="40%">Mobile Number:<BR></TD>
										    <TD class="back" align=left width="60%">
										    <html:hidden property="smsId"/> 
										    	<html:text property="mobileNo" />&nbsp;
										    	 
										    </TD>
										</TR>
										<TR>
										    <TD class="hf" align=right>Schedule SMS:</TD>
										    <td class="back">
										      		<html:text property="scheduledSMSDateTime" maxlength="20" size="20" readonly="true" /> 
													<a  ID="scheduledSMSDateTimeAnchor" NAME="scheduledSMSDateTimeAnchor"  href="javascript:NewCal1('scheduledSMSDateTime','ddmmyyyy',true,24,'dropdown',true,'scheduledSMSDateTimeAnchor')"><img src="<jsp:getProperty name='req' property='contextPath'/>/images/hobbes.gif" width="16" height="16" border="0" alt="Pick a date"></a>
													<BR>
										
											</TD>
										</TR>
										<TR>
										    <TD class="hf" align=right>Character Set:</TD>
										    <TD class="back" align=left>
										    	<html:select property="selected_charset" onchange="showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));"> 
													<html:options property="charset_value" labelProperty="charset_label" />
												</html:select> 
											</TD>
										</TR>
										<TR>
										    <TD class="hf" align="right">Message:</TD>
										    <TD class="back" align="left">
										    	<html:textarea property="smsContent" onkeydown="showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" onkeyup="javascript:showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" onmouseout="javascript:showCharCounter(document.getElementById('selected_charset'), document.getElementById('smsContent'), document.getElementById('charLeftField'), document.getElementById('noOfSMS'));" rows="10" cols="50" />										      	
										    	<br>
										      	<FONT size="-1">Characters left:&nbsp;</FONT>
										      	<INPUT id="charLeftField" readOnly maxLength="3" size="3" name="charLeftField" /> 
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<FONT size=-1>No of SMS:&nbsp;
													<INPUT id="noOfSMS" readOnly maxLength="2" size="2" value="0" name="noOfSMS" />&nbsp;/&nbsp;<bean:write name="EditSMSForm" property="maxNoOfSMS" />
												</FONT> 
												<br>
												<br>
												
											</TD>
										</TR>
										<tr>
											<td class="hf" align=right>Priority:</td>
											<td class="back">
												<html:select property="priority">
													<html:options property="smsPriority_value" labelProperty="smsPriority_label" />
												</html:select>
											</td>
										</tr>
										
										<TR>
										    <TD class="hf">&nbsp;</TD>
										    <TD class="back">
										    	<html:submit value="Submit" property="submitBtn" /> 
										    	<html:reset value="Clear" property="resetBtn" />
										    </TD>
										</TR>
										</TABLE>
									</td>
								</tr>
								</table>
								</html:form>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>