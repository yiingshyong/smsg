<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<req:request id="req"/>

<html:html>

<%
  
    String index ="center";
    int maxPageItems = 20;
    int maxIndexPages = 20;
    int item = 10;
  
%>	
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <title>CRSMSG - Invalid SMS Report</title>
<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/datetimepicker.js"></SCRIPT>
  <SCRIPT LANGUAGE="JavaScript" SRC="<jsp:getProperty name="req" property="contextPath"/>/includes/AnchorPosition.js"></SCRIPT>
</head>

<body>

<br>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
  
<html:form action="/crsmsg/searchSMSInvalid.do">
  <tbody>
     <tr align="right" bgcolor="ffffff">
 			
		 	<td>CSV Format : <a href="../crsmsg/searchSMSInvalidCSV.do">Export<br>
		
			
		 	</td>
		 	</tr>	
   
    <tr>
   
    <tr>
  
    <td>
      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="5" cellspacing="1" width="100%">
                <tbody>
                  <tr>
                    <td class="info" colspan="1" align="center"><b>SMS Invalid Report</b></td>
                  </tr>
                  <tr>
                    <td class="back">

                       <logic:present name="listInvalidSMS">

                      <table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                       
                          <tr>
                            <td>
                              <table border="0" cellpadding="8" cellspacing="1" width="100%">
                                <tbody>
                                  <tr>
                                   <td class="hf" align="center"><b>No</b></td>
                                   <td class="hf" align="center"><b>Invalid Record</b></td>
                                   <td class="hf" align="center"><b>Remark</b></td>
                                   <td class="hf" align="center"><b>Type</b></td>
                                   <td class="hf" align="center"><b>Date</b></td>                                   
                                   <td class="hf" align="center"><b>Created By</b></td>
                                   
                                  </tr>
                                <logic:iterate id="listSMSInvalid1" name="listInvalidSMS" indexId="i">
								  <tr>
								  
								     <td class="back" align="center"><%=i+1%></td>
	                                 <td class="back" align="center"><bean:write name="listSMSInvalid1" property="invalidRecord"/></td>
									 <td class="back" align="center"><bean:write name="listSMSInvalid1" property="remarks"/></td>
									 <td class="back" align="center"><bean:write name="listSMSInvalid1" property="type"/></td>
									 <td class="back" align="center"><bean:write name="listSMSInvalid1" property="createdDatetime"/></td>
									 <td class="back" align="center"><bean:write name="listSMSInvalid1" property="createdBy"/></td>
									
							      
                                  </tr>	
                                  
								</logic:iterate>	
						
								</tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                        
                      </table>
                      	</logic:present>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>

</tbody>
</table>

<logic:present name="count">
<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<% Integer aa = (Integer) request.getAttribute("count") ;

%> 		

  <tr>
    <td class="back" align="center" >
 
	  <pg:pager 
	      items="<%= aa.intValue() %>"
	      index="<%= index %>"
	      maxPageItems="<%= maxPageItems %>"
	      maxIndexPages="<%= maxIndexPages %>"
	      isOffset="<%= true %>"
	      export="offset,currentPageNumber=pageNumber"
	      scope="request"
	      url="../crsmsg/sentLog.do">
	  <pg:param name="index"/>
	  <pg:param name="maxPageItems"/>
	  <pg:param name="maxIndexPages"/>
	  
	   <input type="hidden" name="pager.offset" value="<%= offset %>">
 
      <pg:index>
        <jsp:include page="alltheweb.jsp" flush="true"/>
      </pg:index>
  
      </pg:pager>

    </td>
  </tr>
</table>
</logic:present>

<br>
</html:form>

<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

</table>


</body>
</html:html>
