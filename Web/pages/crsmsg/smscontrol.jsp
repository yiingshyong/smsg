<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>SMS Control</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body onload="toggleLayer(document.getElementById('smsTelcoRouting'),document.getElementById('div1') ,'By Telco Priority');">
		<br>	
		<br>
		<html:form action="/crsmsg/SaveEditSMSControl.do">
		<html:hidden property="cfgId" />
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
		            <td>
						<table border="0" cellpadding="5" cellspacing="1">
						<tr><td class="info" colspan="1" align="center"><b>SMS Control Management</b></td></tr>
						<tr>
							<td class="back" align="center" valign="top">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table border="0" cellSpacing="1" cellPadding="5" align="center">
										<%--
										<tr bgcolor="#929CF4">
											<td class="hf" align="center" valign="top" width="30%"><b>SMS Telco Routing Control:</b></td>
											<td class="back" align="left" width="70%">
												<html:select property="smsTelcoRouting" onchange="toggleLayer(document.getElementById('smsTelcoRouting'),document.getElementById('div1') ,'By Telco Priority');">
													<html:option value="0">Please select.</html:option>
													<html:options property="routing_value" labelProperty="routing_label" /> 
												</html:select>
												<br>
												<DIV id="div1" style="DISPLAY: none">
													<br>Telco Priority:<br>
													<html:select property="smsTelcoRoutingParam[0]">
														<html:option value="0">Please select.</html:option>
														<html:options property="telco_value" labelProperty="telco_label" /> 
													</html:select>
													>
													<html:select property="smsTelcoRoutingParam[1]">
														<html:option value="0">Please select.</html:option>
														<html:options property="telco_value" labelProperty="telco_label" /> 
												</html:select>
												</div>
											</td>
										</tr>
										 --%>
										<tr bgcolor="#707DF4">
											<td class="hf" align="center" valign="top" width="30%">
											<b>Scheduled SMS Control:</b><br>
											Control no. of days in advance for scheduled SMS (0 to disable)
											</td>
											<td class="back" align="left" width="70%"><html:text property="scheduledSmsControlDuration" size="5" maxlength="5" /> day(s) advanced</td>
										</tr>
										<!--
										<tr bgcolor="#929CF4">
											<td class="hf" align="center" rowspan="2" valign="top" width="30%">
												<b>SMS Time</b><br>
												<span class="smsapp_desc" style="text-align:left;">
														This option enables user to set the day, start and end time of sending SMS.
												No control is applicable by default. To control the SMS sending time, simply select the day, and time range.
												</span>
											</td>
											<td class="back" align="center" align="left" width="70%">
												<table width="90%" align="center">
												<tr>
													<td>Apply Time Control to: </td>
													<td><html:checkbox property="smsTimeControlBatch" value="Y" />&nbsp;Batch SMS</td>
													<td><html:checkbox property="smsTimeControlWeb" value="Y" />&nbsp;WebApp SMS</td>
												</tr>
												</table>
											</td>
										</tr>
										<tr bgcolor="#929CF4">
									        <td class="back" align="center" width="70%">
									        	<table width="90%" align="center">
									        	<tr>
									        		<td>
									        			<table>
														<tr>
															<td><html:multibox property="smsTimeControlDay" value="1" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Sunday&nbsp;</td>
														</tr>
					    								<tr>
											                <td><html:multibox property="smsTimeControlDay" value="2" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Monday&nbsp;</td>
											            </tr>
														<tr>
											                <td><html:multibox property="smsTimeControlDay" value="3" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Tuesday&nbsp;</td>
											            </tr>
														<tr>
				                							<td><html:multibox property="smsTimeControlDay" value="4" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Wednesday&nbsp;</td>
				                						</tr>
														<tr>
															<td><html:multibox property="smsTimeControlDay" value="5" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Thursday&nbsp;</td>
														</tr>
														<tr>
															<td><html:multibox property="smsTimeControlDay" value="6" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Friday&nbsp;</td>
														</tr>
														<tr>
															<td><html:multibox property="smsTimeControlDay" value="7" onclick="toggleCheckAll(this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay, this.document.getElementById('checkAllBtm'))"/>Saturday&nbsp;</td>
				                						</tr>
				                						</table>
				                					</td>
				                					<td>
				                						<table>
				                						<tr>
				                							<td>
				                								<table>
				                								<%String propertyName;%>
				                								<logic:present name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeFromHour">
				                								<logic:iterate id="num" name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeFromHour" indexId="i">
							                					<tr>
							                						<td>
							                							<%propertyName="smsTimeControlTimeFromHour["+i+"]";%>
																		<html:select property="<%=propertyName%>">
																			<html:options property="hour_value" labelProperty="hour_label" /> 
																		</html:select>
																	</td>
																</tr>
																</logic:iterate>
																</logic:present>
																</table>
															</td>
															<td>
																<table>
																<logic:present name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeFromMin">
																<logic:iterate id="num" name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeFromMin" indexId="i">
							                					<tr>
							                						<td>
							                							<%propertyName="smsTimeControlTimeFromMin["+i+"]";%>
																		:<html:select property="<%=propertyName%>">
																			<html:options property="min_value" labelProperty="min_label" /> 
																		</html:select>
																		&nbsp;to&nbsp;
																	</td>
																</tr>
																</logic:iterate>
																</logic:present>
																</table>
															</td>
															
															<td>
																<table>
																<logic:present name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeToHour">
																<logic:iterate id="num" name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeToHour" indexId="i">
							                					<tr>
							                						<td>
							                							<%propertyName="smsTimeControlTimeToHour["+i+"]";%>
																		<html:select property="<%=propertyName%>">
																			<html:options property="hour_value" labelProperty="hour_label" /> 
																		</html:select>
																	</td>
																</tr>
																</logic:iterate>
																</logic:present>
																</table>
															</td>
															<td>
																<table>
																<logic:present name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeToMin">
																<logic:iterate id="num" name="CrsmsgSystemCfgFormEdit" property="smsTimeControlTimeToMin" indexId="i">
							                					<tr>
							                						<td>
							                							<%propertyName="smsTimeControlTimeToMin["+i+"]";%>
																		:<html:select property="<%=propertyName%>">
																			<html:options property="min_value" labelProperty="min_label" /> 
																		</html:select>
																	</td>
																</tr>
																</logic:iterate>
																</logic:present>
																</table>
															</td>
				                						</tr>
														</table>
													</td>
				                				</tr>
												<tr>
													<td colspan="2" align="right">
														&nbsp;&nbsp;&nbsp;
														<input type="checkbox" name="checkAllBtm" onClick="toggleCheckAll(this, this.document.CrsmsgSystemCfgFormEdit.smsTimeControlDay);" />Select All Days
													</td>
												</tr>
												</table>
											</td>
										</tr>
										-->
										<tr bgcolor="#707DF4">
											<td class="hf" align="center" valign="top">
												<b>IP to skip control</b><br>
										    	<span class="smsapp_desc">Enter each IP address in a new line.</span>
											</td>
											<td class="back" align="center">
												<html:textarea property="ipSkip" cols="50" rows="20" />
											</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		<p> &nbsp;</p>
		<table align="center">
		<tr><td>
			<html:submit property="submitBtn" value="Save Setting" onclick="trimSelectName('smsTelcoRoutingParam');trimSelectName('smsTimeControlTimeFromHour');trimSelectName('smsTimeControlTimeFromMin');trimSelectName('smsTimeControlTimeToHour');trimSelectName('smsTimeControlTimeToMin');" />
			<html:reset property="resetBtn" value="Reset" />
		</td></tr>
		</table>
		</html:form>
	</body>
	<!-- body end -->
</html:html>