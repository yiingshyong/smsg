<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>
<html:html>
<head>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/jquery/jquery.js"></script>
<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg_jquery.js"></script>
<title>Welcome to SMS</title>
<script type="text/javascript">
var err_ind ="<html:errors/>";

</script>
</head>
<link rel="stylesheet" href="../includes/crms.css">
<body link="#3333FF" vlink="#3333FF" alink="#3333FF" leftmargin=0 rightmargin=0 topmargin=0 marginheight=0 marginwidth=0>

<!-- Header table -->	
	<table cellspacing=0 cellpadding=0 width="772" border=0>
	<tr>   
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=2>				
				<tr>
					<td><div id='anc'>
</div>
<img height="18" width="10" src="../images/box.gif"></td>
					<td>
					<table border=0 cellpadding=0 cellspacing=0>
					<tr><td><img src="../images/b_lefttop.gif" width="3" height="3"></td><td background="../images/b_topline.gif"></td><td><img src="../images/b_righttop.gif" width="3" height="3"></td></tr>
					<tr><td background="../images/b_leftline.gif"></td><td class="headers">&nbsp;&nbsp;&nbsp;Decryption keys for OTP SMS send from CardLink to SMSG via TCP&nbsp;&nbsp;&nbsp;</td><td background="../images/b_rightbottom.gif"></td></tr>
					<tr><td><img src="../images/b_leftbottom.gif" width="3" height="3"></td><td background="../images/b_bottomline.gif"></td><td><img src="../images/b_rightline.gif" width="3" height="3"></td></tr>
					</table>		
					</td>
				</tr>
	</table>	
	</td>
	</tr>	
<tr>
<td class="error_td" >
<html:errors bundle="crsmsg"/>
</td>
</tr>								
</table>
<!-- Header table End -->	
	
				
		
	<br>
<!-- Edit Dept Dialog -->
	<table width=700 border=0 cellpadding=0 cellspacing=0>
			<tr><td><img src="../images/b1_lefttop.gif" width="6" height="6"></td><td background="../images/b1_top.gif"></td><td><img src="../images/b1_righttop.gif" width="6" height="6"></td></tr>
			<tr>
				<td background="../images/b1_left.gif"></td><td class="form_non_input">			

<!-- Start Detail Table -->
<table border=0 cellpadding=0 cellspacing=10 class="form_non_input" width=500>						
<html:form action="/crsmsg/SocketEncryptionKeyAction.do" >

<tr valign="top" >
<td class=form_non_input width=25%>Key Component 1&nbsp;</td>
<td width=1%>:</td>
<td width=25%><html:text property="smsCfg.socketEncComp1" size="45" maxlength="32" styleClass="form_input limitHex"/></td>
<td><html:submit property="btn1" value="Save" styleClass="form_button"></html:submit></td>
</tr>

<tr valign="top" >
<td class=form_non_input width=25%>Key Component 2&nbsp;</td>
<td width=1%>:</td>
<td width=25%><html:text property="smsCfg.socketEncComp2" size="45" maxlength="32" styleClass="form_input limitHex"/></td>
<td><html:submit property="btn2" value="Save" styleClass="form_button"></html:submit></td>
</tr>

<tr valign="top" >
<td class=form_non_input width=25%>Key Component 3&nbsp;</td>
<td width=1%>:</td>
<td width=25%><html:text property="smsCfg.socketEncComp3" size="45" maxlength="32" styleClass="form_input limitHex"/></td>
<td><html:submit property="btn3" value="Save" styleClass="form_button"></html:submit></td>
</tr>

<tr><td colspan=4 height=10></td></tr>
<tr class="form_non_input">
<td colspan="4">				
<input type="button" value="Reset" class="form_button">
&nbsp;&nbsp;
</td>
</tr>		
</html:form>
</table>
<!-- End Detail Table -->
						</td>
						<td background="../images/b1_right.gif"></td></tr>
					<tr><td><img src="../images/b1_leftbottom.gif" width="6" height="6"></td><td background="../images/b1_bottom.gif"></td><td><img src="../images/b1_rightbottom.gif" width="6" height="6"></td></tr>
				</table>
<!-- Edit User Dialog End -->
			
 
</body>
</html:html>