<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title></title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<html:form action="/crsmsg/SaveEditSystemCfg.do">
		<html:hidden property="cfgId" />
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
			<html:errors bundle="crsmsg" />
			</td>
		</tr>
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>System Setting</b></td></tr>
						<tr>
							<td class="back">
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%" align="center">
										<tr>
											<td class="back" align="left"><h4>Total SMS per Message.</h4>This set the maximum number of SMS per message. If "long SMS" is enable, the system will attempt to send SMS parts as concatenated messages. Mobile phone that suppose concatenated messages will join all messages and displayed as single SMS. 
												<br>
												Note: 
												<ul>
											        <li>for concatenated SMS, maximum length for each part is 153 characters. </li>
											        <li>Some phone may not support concatenated SMS, thus such phone may discard the received SMS.</li>
										        </ul>
										        <br>
												Please be noted that concatenated SMS is supported in sending SMS via Direct Connection.
										       <td class="back" align="left">
										       <p>
										        Check:
										        
										        <html:checkbox property="concatenateSms" value="Y" />
										        
										        to enable long SMS.
										        </p>
										        <p>
											        Total: 
											        <html:select property="concatenateSmsNo">
														<html:option value="1">1</html:option>
														<html:option value="2">2</html:option>
														<html:option value="3">3</html:option>
														<html:option value="4">4</html:option>
														<html:option value="5">5</html:option>
													</html:select>
													 SMS per message.
											   	</p>
											</td>
										</tr>
										<tr>
										    <td class="stats" align="left"><h4>HTTP To SMS Service.</h4>This option enable or disable HTTP to SMS. If disabled, all HTTP to SMS request will be discarded. </td>
										    <td class="stats" align="left">
										    	<html:select property="httpSmsService">
													<html:option value="1">Enable</html:option>
													<html:option value="0">Disable</html:option>
												</html:select>
										    </td>
										</tr>
										<tr>
										    <td class="back" align="left"><h4>Default SMS Type</h4>Default SMS Type, used when Channel ID for user couldn't be matched</td>
										    <td class="back" align="left">
										    	<html:select property="defaultSmsType">
										    		<html:optionsCollection property="smsTypes" label="label" value="tblRefId"/>
												</html:select>
										    </td>
										</tr>
										<tr>
										    <td class="stats" align="left"><h4>Default System SMS Sender</h4>Sender for System SMS, Eg. Notification SMS, will be overridden by any notification sender below</td>
										    <td class="stats" align="left">
										    	<html:text property="systemSMSDefaultSender"/>
										    </td>
										</tr>
										<tr>
											<td class="back" align="left"><h4>Failure FTP Notice</h4>The system will send an email/SMS notification if the FTP files failed to send to FTP server.</td>
											<td class="back" align="left">
												<table>
												<tr>
													<td>
														<html:multibox property="ftpFailureNotification" value="E" />Email
													</td>
													<td>
														<html:multibox property="ftpFailureNotification" value="S" />SMS
													</td>
												</tr>
												<tr>
													<td>
														<table>
														<tr>
															<td>Sender Email: <html:text property="ftpFailureNotificationEmailSender" maxlength="50"/></td>
														</tr>
														<tr>
															<td><html:textarea rows="10" property="ftpFailureNotificationEmail"></html:textarea></td>
														</tr>
														</table>
													</td>
													<td>
														<table>
														<tr>
															<td>Sender ID: <html:text property="ftpFailureNotificationSmsSender" maxlength="15"/></td>
														</tr>
														<tr>
															<td><html:textarea rows="10" property="ftpFailureNotificationSms"></html:textarea></td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="stats" align="left"><h4>Failure SMS Notice.</h4>The system will send an email/SMS notifcation if the SMS failed to send.</td>
											<td class="stats" align="left">
												<table>
												<tr>
													<td>
														SMS Failure Threshold No.: <html:text property="smsFailureThreshold" size="3" maxlength="15"/>
														Recheck: <html:text property="smsFailureThresholdPeriod" size="3" maxlength="15"/> minute(s)
													</td>
												</tr>
												<tr>
													<td>
														<table>
														<tr>
															<td>
																<html:multibox property="smsFailureNotification" value="E" />Email
															</td>
															<td>
																<html:multibox property="smsFailureNotification" value="S" />SMS
															</td>
														</tr>
														<tr>
															<td>
																<table>
																<tr>
																	<td>Sender Email: <html:text property="smsFailureNotificationEmailSender" maxlength="50"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="smsFailureNotificationEmail"></html:textarea></td>
																</tr>
																</table>
															</td>
															<td>
																<table>
																<tr>
																	<td>Sender ID: <html:text property="smsFailureNotificationSmsSender" maxlength="15"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="smsFailureNotificationSms"></html:textarea></td>
																</tr>
																</table>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="back" align="left"><h4>Mobile Prefix Number </h4>
												<br>
												This is the handphone prefix number for different country code where people 
												normally don't put in front of the number they send<br>
												eg:Singapore is +65, Malaysia is +6
											</td>
											<td class="back" align="left"><html:text property="mobileCountryPrefix" size="10" maxlength="20" /></td>
									    </tr>
									    <tr>
										    <td class="stats" align="left">
										    	<h4>Bulk SMS Limits </h4>
										    	<br>
										    	This is to set the limit of numbers of bulk SMS to be processed before trigger alert email to defined email address/SMS.
											</td>
											<td class="stats" align="left">
												<table>
												<tr>
													<td>
														Bulk SMS Threshold: <html:text property="bulkSmsThreshold" size="3" maxlength="15"/>
													</td>
												</tr>
												<tr>
													<td>
														<table>
														<tr>
															<td>
																<html:multibox property="bulkSmsThresholdNotification" value="E" />Email
															</td>
															<td>
																<html:multibox property="bulkSmsThresholdNotification" value="S" />SMS
															</td>
														</tr>
														<tr>
															<td>
																<table>
																<tr>
																	<td>Sender Email: <html:text property="bulkSmsThresholdNotificationEmailSender" maxlength="50"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="bulkSmsThresholdNotificationEmail"></html:textarea></td>
																</tr>
																</table>
															</td>
															<td>
																<table>
																<tr>
																	<td>Sender ID: <html:text property="bulkSmsThresholdNotificationSmsSender" maxlength="15"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="bulkSmsThresholdNotificationSms"></html:textarea></td>
																</tr>
																</table>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
										    <td class="back" align="left">
										    	<h4>Queue SMS Limit</h4>
										    	<br>
										    	This is to set the limit of numbers of queued SMS before trigger alert email to defined email address/SMS.
											</td>
											<td class="back" align="left">
												<table>
												<tr>
													<td>
														<table>
															<tr>
																<td>
																	Queue Total SMS Threshold: <html:text property="totalQueuedSizeAlertThreshold" size="3" maxlength="15"/>
																	</td><td>
																	Recheck: <html:text property="totalQueuedSizeAlertCheckPeriod" size="3" maxlength="15"/> minute(s)
																</td>
															</tr>
															<tr>
																<td>
																	Queue SMS Threshold &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <html:text property="queueSmsThreshold" size="3" maxlength="15"/>
																	</td><td>
																	Recheck: <html:text property="queueSmsThresholdPeriod" size="3" maxlength="15"/> minute(s)
																</td>
															</tr>														
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table>
														<tr>
															<td>
																<html:multibox property="queueSmsThresholdNotification" value="E" />Email
															</td>
															<td>
																<html:multibox property="queueSmsThresholdNotification" value="S" />SMS
															</td>
														</tr>
														<tr>
															<td>
																<table>
																<tr>
																	<td>Sender Email: <html:text property="queueSmsThresholdNotificationEmailSender" maxlength="50"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="queueSmsThresholdNotificationEmail"></html:textarea></td>
																</tr>
																</table>
															</td>
															<td>
																<table>
																<tr>
																	<td>Sender ID: <html:text property="queueSmsThresholdNotificationSmsSender" maxlength="15"/></td>
																</tr>
																<tr>
																	<td><html:textarea rows="10" property="queueSmsThresholdNotificationSms"></html:textarea></td>
																</tr>
																</table>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
										    <td class="stats" align="left">
										    	<h4>Pending SMS Auto Failure </h4>
										    	<br>
												This is to auto change pending case to unsent status after the defined lapsed period of time from the SMS created time
											</td>
										    <td class="stats" align="left"><html:text property="autoSmsFailure" size="5" /> days (Set to 0 to disable).</td>
										</tr>
										<tr>
										    <td class="back" align="left"><h4>SMS Resend</h4>This option is to set the maximum attempt of resending failed SMS and interval of each resend. Set the Maximum Attempt to 0 to disable this feature. </td>
										    <td class="back" align="left">&nbsp;Maximum Attempt: <html:text property="autoSmsResendLimit" size="10" maxlength="20" /><br>
											<div style="height:10px;">&nbsp;</div>&nbsp;
											Resend Interval: <html:text property="autoSmsResendInterval" size="10" maxlength="20" />&nbsp;second(s)
										    </td>
										</tr>
										<!-- 
										<tr>
										    <td class="stats" align="left"><h4>Priority SMS</h4>This option is to enable or disable the system of sending SMS based on priority. The highest priority is 1, and 9 for the lowest. </td>
										    <td class="stats" align="left"><html:checkbox property="smsPrioritize" value="Y" />&nbsp;Enable Priority SMS</td>
										</tr>
										 -->
										<tr>
										    <td class=stats align="left"><h4>WebApp SMS Quota Control</h4>This option is to enable or disable SMS quota control on WebApp user, and the default quota for each newly created user. </td>
										    <td class="stats" align="left">
										    	<html:checkbox property="smsQuota" value="Y" />&nbsp;Enable Quota Contol
										    	<br>
										        Default Quota: 
												<html:text property="smsQuotaDefault" size="10" />&nbsp;SMS
											</td>
										</tr>
										<tr>
										    <td class="back" align="left"><h4>Password Reset Policy</h4>This option is to set the number of days to reset password for each WebApp user. Set to '0' to disable the password from expiring. </td>
										    <td class="back" align="left">
										    	Change Password After &nbsp;
												<html:text property="passwordChangeDuration" size="10" />&nbsp;day(s).
											</td>
										</tr>
										<tr>
										    <td class="stats" align="left"><h4>Account Dormancy Check</h4>This option is to set the number of days to disable the login for each inactive WebApp user. Set to '0' to disable the account dormancy checking. </td>
										    <td class="stats" align="left">
										    	Disable Login After &nbsp;
												<html:text property="accountDormancyCheck" size="10" />&nbsp;day(s).
											</td>
										</tr>										
										<tr>
										    <td class="back" align="left"><h4>SMS Usage Log Maintenance</h4>This option is to set the number of months the system will keep the outbox, inbox and the unsent messages, FTP Log and Action Log. Set to '0' to disable the feature. </td>
										    <td class="back" align="left">
										    	Archive After &nbsp;
												<html:text property="usageLogDuration" size="10" />&nbsp;month(s).
											</td>
										</tr>
										<tr>
										    <td class="stats" align="left"><h4>SMS System Log Maintenance</h4>This option is to set the number of months the system will keep the system log file. Set to '0' to disable the feature. </td>
										    <td class="stats" align="left">
										    	Archive After &nbsp;
												<html:text property="systemLogDuration" size="10" />&nbsp;month(s).
											</td>
										</tr>																				
										<tr>
										    <td class="stats" align="left"><h4>Credit Card Activation Log Maintenance</h4>This option is to set the number of months the system will keep the Credit Card Activation Log . Set to '0' to disable the feature. </td>
										    <td class="stats" align="left">
										    	Archive After &nbsp;
												<html:text property="ccaDataAge" size="5" />&nbsp;month(s).
											</td>
										</tr>
										<tr>
											<td colspan="2" class="back" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SMSControl.do?cfgId=<bean:write name='CrsmsgSystemCfgFormEdit' property='cfgId' />">here</a>] to set SMS Control Management.</td>
										</tr>
										<tr>
											<td colspan="2" class="stats" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/ServiceProviderMaintenanceListing.do?cfgId=<bean:write name='CrsmsgSystemCfgFormEdit' property='cfgId' />">here</a>] to set SMS Service Provider.</td>
										</tr>
										<tr>
											<td colspan="2" class="back" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SMSTelco.do?cfgId=<bean:write name='CrsmsgSystemCfgFormEdit' property='cfgId' />">here</a>] to set SMS Service Provider API settings.</td>
										</tr>
										<tr>
											<td colspan="2" class="stats" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SmsRateMaintenanceListing.do?cfgId=<bean:write name='CrsmsgSystemCfgFormEdit' property='cfgId' />">here</a>] to set SMS Rate.</td>
										</tr>
										<tr>
											<td colspan="2" class="back" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SMSTypeListing.do">here</a>] for SMS Type Maintenance</td>
										</tr>
										<tr>
											<td colspan="2" class="stats" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/QueueListenerListing.do">here</a>] for Message Queue Setting</td>
										</tr>
										<tr>
											<td colspan="2" class="stats" align="left">Click [<a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/SMSAutoAction.do">here</a>] for Auto Action Setup</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		<br>
		<table align="center">
		<tr>
			<td>
				<html:submit property="submitBtn" value="Save Setting" />
				<html:reset property="resetBtn" value="Reset" />
			</td>
		</tr>
		</table>
		</html:form>
	</body>
	<!-- body end -->
</html:html>