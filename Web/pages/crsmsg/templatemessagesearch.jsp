<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>Template Message Mgmt</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		
		<tr class="csvexport">
			<td>CSV Format : <a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/GetTemplateMessageCSV.do">Export</a></td>
		</tr>
		
		<tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>Template Message</b></td></tr>
						<tr>
							<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/GetTemplateMessageSearch.do" focus="searchkey">
								<table border="0" width="90%">
								<tr>
									<td width="10%" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/AddTemplateMessage.do">New Message</a></td>
									<td class="plain" width="80%" align="right">
										Search:
										<html:text property="searchkey" styleClass="search_input"/>
										&nbsp;By&nbsp;
										<html:select property="selectedState" styleClass="search_select">
											<html:options property="searchby_value" labelProperty="searchby_label" /> 
										</html:select>
										<html:submit property="submitBtn" value="Search" styleClass="search_btn" />
									</td>
								</tr>
								</table>
								<br>
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<tr>
											<td align="center" class="hf" width="5%"><b>No</b></td>
											<td align="center" class="hf" width="15%"><b>Message Code</b></td>
											<td align="center" class="hf" width="15%"><b>Message Text</b></td>
											<td align="center" class="hf" width="15%"><b>Dynamic Fields</b></td>
											<td align="center" class="hf" width="12%"><b>Name of Fields</b></td>
											<td align="center" class="hf" width="7%"><b>Created By</b></td>
											<td align="center" class="hf" width="7%"><b>Department</b></td>
											<td align="center" class="hf" width="7%"><b>Status</b></td>
											<td align="center" class="hf" width="5%"><input type="checkbox" name="checkAllTop" onClick="toggleCheckAll(this, this.document.CrsmsgTemplateMessageSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllBtm'))" /></td>
										</tr>
										<logic:present name="CrsmsgTemplateMessageSearchForm" property="resultList">
										<logic:notEmpty name="CrsmsgTemplateMessageSearchForm" property="resultList">
										<bean:define id="result" name="CrsmsgTemplateMessageSearchForm" property="resultList" />
										<logic:iterate id="aRecord" name="result" property="record" indexId="i">
										<tr bgcolor="#98AFC7">
											<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditTemplateMessage.do?templateId=<bean:write name='aRecord' property='column[0]' />"><%=i+1%></a></td>
											<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
												<logic:equal name="j" value="1">
													<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditTemplateMessage.do?templateId=<bean:write name='aRecord' property='column[0]' />"><bean:write name="aColumn"/></a></td>
												</logic:equal>
												<logic:equal name="j" value="2">
													<td class="back" align="center"><textarea rows=5 cols=20 readonly><bean:write name="aColumn"/></textarea></td>
												</logic:equal>
												<logic:greaterThan name="j" value="2">
													<td class="back" align="center"><bean:write name="aColumn"/></td>
												</logic:greaterThan>												
											</logic:iterate>    
											<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgTemplateMessageSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));toggleCheckAll(this.document.CrsmsgTemplateMessageSearchForm.checkedRecords, this.document.getElementById('checkAllBtm'))"><bean:write name='aRecord' property='column[0]' /></html:multibox></td>
										</tr>
										</logic:iterate>
										</logic:notEmpty>
										</logic:present>
										</table>
									</td>
								</tr>
								</table>
								</html:form>
								<table border=0 width="100%" cellpadding="10">
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="9" align="right">
										<html:form action="/crsmsg/DeleteTemplateMessage.do">
											Select all:	
											<input type="checkbox" name="checkAllBtm" onClick="toggleCheckAll(this, this.document.CrsmsgTemplateMessageSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllTop'))" />
											<html:submit property="deleteBtn" value="Delete Selected Message(s)" onclick="if(confirmDeleteButton('template message(s)')){return copyFormElement(this.form, this.document.CrsmsgTemplateMessageSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden');}else{return false;}" />
										</html:form>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>