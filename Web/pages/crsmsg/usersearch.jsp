<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>
<%@ taglib uri="/tags/request-1.0" prefix="req" %>

<req:request id="req"/>

<html:html>
	<!-- header start -->
	<head>
		<title>User Management</title>
		<link rel="stylesheet" href="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.css">
		<script language="JavaScript" src="<jsp:getProperty name='req' property='contextPath'/>/includes/crsmsg.js"></script>
		<script language="JavaScript">
		</script>
	</head>
	<!-- header end -->
	<!-- body start -->
	<body>
		<br>
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">		

		<tr>
			<td class="error_td" >
				<html:errors bundle="crsmsg" />
			</td>
		</tr>
		
		<tr class="csvexport">
			<td>CSV Format : <a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/UserSearchGenerateCSV.do">Export</a></td>
		</tr>
		
		<!--
	    <tr class="csvexport">
			<td>CSV Format : <a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/DownloadCSV.do">Export</a></td>
		</tr>
		-->
	    <tr>
			<td>
				<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
	            	<td>
	              		<table border="0" cellpadding="5" cellspacing="1" width="100%">
						<tr><td class="info" colspan="1" align="center"><b>User Management</b></td></tr>
	                  	<tr>
	                  		<td bgcolor=ffffff align="center">
								<html:form action="/crsmsg/GetUserSearch.do" focus="searchkey">
								<table border="0" width="90%">
								<tr>
									<td width="10%" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/AddUser.do">New User</a></td>
									<td class="plain" width="80%" align="right">
										Search:
										<html:text property="searchkey" styleClass="search_input"/>
										&nbsp;By&nbsp;
										<html:select property="selectedState" styleClass="search_select">
											<html:options property="searchby_value" labelProperty="searchby_label" /> 
										</html:select>
										<html:submit property="submitBtn" value="Search" styleClass="search_btn" />
									</td>
								</tr>
								</table>
								<br>
								<table class="border" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<table border="0" cellpadding="8" cellspacing="1" width="100%">
										<tr>
											<td align="center" class="hf" width="5%"><b>No</b></td>
											<td align="center" class="hf" width="15%"><b>User</b></td>
											<td align="center" class="hf" width="15%"><b>Name</b></td>
											<td align="center" class="hf" width="15%"><b>Role</b></td>
											<td align="center" class="hf" width="12%"><b>Department</b></td>
											<td align="center" class="hf" width="7%"><b>Creation Date</b></td>
											<td align="center" class="hf" width="7%"><b>Last Login Date</b></td>
											<td align="center" class="hf" width="7%"><b>Status</b></td>
											<!--<td align="center" class="hf" width="8%"><b>Quota</b></td>-->
											<td align="center" class="hf" width="5%"><input type="checkbox" name="checkAllTop" onClick="toggleCheckAll(this, this.document.CrsmsgUserSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllBtm'))" /></td>
										</tr>
										<logic:present name="CrsmsgUserSearchForm" property="resultList">
										<logic:notEmpty name="CrsmsgUserSearchForm" property="resultList">
										<bean:define id="result" name="CrsmsgUserSearchForm" property="resultList" />
										<logic:iterate id="aRecord" name="result" property="record" indexId="i">
										<tr bgcolor="#98AFC7">
											<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditUser.do?userid=<bean:write name='aRecord' property='column[0]' />"><%=i+1%></a></td>
											<logic:iterate id="aColumn" name="aRecord" property="column" indexId="j">
												<logic:equal name="j" value="0">
													<td class="back" align="center"><a href="<jsp:getProperty name='req' property='contextPath'/>/crsmsg/EditUser.do?userid=<bean:write name='aRecord' property='column[0]' />"><bean:write name="aColumn"/></a></td>
												</logic:equal>
												<logic:notEqual name="j" value="0">
													<td class="back" align="center"><bean:write name="aColumn"/></td>
												</logic:notEqual>												
											</logic:iterate>    
											<td class="back" align="center"><html:multibox property="checkedRecords" onclick="toggleCheckAll(this.document.CrsmsgUserSearchForm.checkedRecords, this.document.getElementById('checkAllTop'));toggleCheckAll(this.document.CrsmsgUserSearchForm.checkedRecords, this.document.getElementById('checkAllBtm'))"><bean:write name='aRecord' property='column[0]' /></html:multibox></td>
										</tr>
										</logic:iterate>
										</logic:notEmpty>
										</logic:present>
										</table>
									</td>
								</tr>
								</table>
								</html:form>
								
								<table border=0 width="100%" cellpadding="10">
								<!--
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="9" align="right">
										<html:form action="/crsmsg/UpdateQuota.do">
											Quota:<html:text property="quota" size="2" maxlength="2"/>
											<html:radio property="topUpMode" value="topup">&nbsp;Top-Up</html:radio>
											<html:radio property="topUpMode" value="reset">&nbsp;Reset</html:radio>
											<html:submit property="setQuotaBtn" value="Set Quota For Selected Users" onclick="return copyFormElement(this.form, this.document.CrsmsgUserSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden');" />
											<html:button property="setQuotaAllBtn" value="Set Quota For All Users" onclick="controlCheckAll(true, this.document.CrsmsgUserSearchForm.checkedRecords);if(copyFormElement(this.form, this.document.CrsmsgUserSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden')){this.form.submit();}else{alert();}" />
										</html:form>
									</td>
								</tr>
								-->
								<tr bgcolor="#98AFC7">
									<td class="back" colspan="9" align="right">
										<html:form action="/crsmsg/DeleteUser.do">
											Select all:	<input type="checkbox" name="checkAllBtm" onClick="toggleCheckAll(this, this.document.CrsmsgUserSearchForm.checkedRecords);toggleCheckAll(this, this.document.getElementById('checkAllTop'))" />
											<html:submit property="deleteBtn" value="Delete Selected User(s)" onclick="if(confirmDeleteButton('user(s)')){return copyFormElement(this.form, this.document.CrsmsgUserSearchForm.checkedRecords, 'input', 'checkedRecords', 'hidden');}else{return false;}" />
										</html:form>
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</body>
	<!-- body end -->
</html:html>