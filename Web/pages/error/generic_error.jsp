<%@page isErrorPage="true"%>

<html>
<head>
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
</head>
<body>
<%
long logTimestamp = 0;
if(exception != null){
	logTimestamp = System.currentTimeMillis();
	com.bcb.common.util.log4jUtil.error("Log timestamp: " + logTimestamp + " - Unexpected Error Caught. Reason:" + exception.getMessage(), exception);
%>
<h2>An unexpected error has occurred</h2>
<p>
There was a system error during your navigation. Please try again later.
</p>

<i style="font-size: 12 ">Log Id: 
<%=logTimestamp%>
</i>
<%}%>
</body>
</html>