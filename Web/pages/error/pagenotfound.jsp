<%@page isErrorPage="true"%>

<html>
<head>
<title>404 Page Not Found</title>
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
<!-- this comment to hack around and suppress IE user friendly message -->
</head>
<body>
<%
	long logTimestamp = 0;
	logTimestamp = System.currentTimeMillis();
	com.bcb.common.util.log4jUtil.error("Log timestamp: " + logTimestamp + " - Page Not Found - " + request.getAttribute("javax.servlet.forward.request_uri"));
%>
<p>
  You have reached a page that does not exist or has been moved : <b><%=request.getAttribute("javax.servlet.forward.request_uri")%></b> 
</p>
</body>
</html>